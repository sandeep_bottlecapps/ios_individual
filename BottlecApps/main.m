 //
//  main.m
//  BottlecApps
//
//  Created by imac9 on 8/3/15.
//  Copyright (c) 2015 Kuldeep Tyagi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
