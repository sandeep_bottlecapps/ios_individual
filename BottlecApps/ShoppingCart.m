//
//  ShoppingCart.m
//  liquorApp2
//
//  Created by classic on 10/1/12.
//
//

#import "ShoppingCart.h"
#import "Product.h"

@interface ShoppingCart() {
NSMutableArray *_shoppingBag;
}

@end

static ShoppingCart *_sharedInstance=nil;
@implementation ShoppingCart
@synthesize shoppingBag=_shoppingBag;



-(ShoppingCart*)init
{
    if(self=[super init])
    {
        _shoppingBag=[[NSMutableArray alloc] init];
    }
    return self;
}

+(ShoppingCart*)sharedInstace
{
    if(_sharedInstance==nil)
    {
        _sharedInstance=[[ShoppingCart alloc] init];
    }
    return _sharedInstance;
}

// reset the cart
-(void) resetShopingCart   {
    @synchronized(self) {
        _shoppingBag = [[NSMutableArray alloc] init];
    }
}

// load the shoping cart from archiver
-(void) loadShopingCartfromArchiver {
    @synchronized(self) {
        _shoppingBag = [self getCartFromArchiver];
        if(!_shoppingBag)
            _shoppingBag = [NSMutableArray array];
        
        if(_shoppingBag && [_shoppingBag count])    {
                [[NSUserDefaults standardUserDefaults]setObject: [NSString stringWithFormat:@"%ld",(unsigned long)[_shoppingBag count]] forKey:@"badgecount"];
        }
    }
}

-(NSString*)addProductToMyShoppingCart:(NSArray*)productList{
   // NSLog(@"Product List = %@",productList);
    NSUInteger productCount= [productList count] ;
    int addedProduct=0;
    int CHKStatusProduct=0;
    NSString *message;
    for (Product *prod in productList){
        if ([self shoppingCHKStatus:prod]){
            if(![self shoppingCartContains:prod]){
                prod.orderQuantity=1;
                [_shoppingBag addObject:prod];
                addedProduct++;
                NSArray *shoppingbagarray=[[ShoppingCart sharedInstace] fetchProductFromShoppingCart];

                NSString *badgeCountstring=[NSString stringWithFormat:@"%lu",(unsigned long)[shoppingbagarray count]];
                NSDictionary* badgecountDict = [[NSDictionary alloc] initWithObjectsAndKeys:badgeCountstring,@"badgecount", nil];
                NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:@"getBadgeCountNotification" object:self userInfo:badgecountDict];
            }
        }
        else{
            CHKStatusProduct++;
        }
    }
    
    [self archieveShoppingCart]; //archive for  future use
    
     if(CHKStatusProduct>0 && addedProduct>0)   {
         message=@"Some items are out of stock, rest of them have been added";
    }
   else if (CHKStatusProduct>0) {
        message=@"Product is out of stock";
    }
   else if(productCount==addedProduct){
        message=@"Product added to Shopping Cart ";
   }
    
    else
        if(_shoppingBag.count==1)
        {
             message=@"It was already added to shopping cart.";
            
        }
      else
      {
        message=@"It was already added to shopping cart.";
     }
    
    DLog(@"productList-------------------%@",productList);
   
   
       return message;
}


-(BOOL)shoppingCartContains:(Product*)product
{
    //NSLog(@"_shoppingBag--%@",product.productId);
    for (Product *pro in _shoppingBag)
    {
      //  NSLog(@"Pro = %@",pro);
        NSString *prodID=[NSString stringWithFormat:@"%@", product.productId];
        NSString *prodIDShoppingCart=[NSString stringWithFormat:@"%@", pro.productId];

        DLog(@"productId---%@",product.productId);
        
               if([prodIDShoppingCart isEqualToString: prodID])
            return TRUE;
    }
       return FALSE;
}


-(BOOL)shoppingCHKStatus:(Product*)product
{
    NSString *prodStatus=[NSString stringWithFormat:@"%@",product.productStatus];
    NSString*productstatus=[NSString stringWithFormat:@"%@",prodStatus];
    if([productstatus isEqualToString: @"1"])
        return TRUE;
    else
        return FALSE;
}



-(void)removeProductFromMyShoppingCart:(NSArray*)productList
{
    for (Product *prod in productList)
    {
        if([self shoppingCartContains:prod])
        {
            [_shoppingBag removeObject:prod];
            NSArray *arrAllProduct = _shoppingBag;
            
            for (Product *prdct  in arrAllProduct){
                NSString *prodId=[NSString stringWithFormat:@"%@",prod.productId];
                NSString *shoProId=[NSString stringWithFormat:@"%@",prdct.productId];
                if([shoProId isEqualToString:prodId]){
                    [_shoppingBag removeObject:prdct];
                    break;
                }
            }
            NSArray *shoppingbagarray=[[ShoppingCart sharedInstace] fetchProductFromShoppingCart];

            NSString *badgeCountstring=[NSString stringWithFormat:@"%lu",(unsigned long)[shoppingbagarray count]];
            NSDictionary* badgecountDict = [[NSDictionary alloc] initWithObjectsAndKeys:badgeCountstring,@"badgecount", nil];
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"getBadgeCountNotification" object:self userInfo:badgecountDict];
        }
    }
    
    [self archieveShoppingCart];  //archive for  future use
}

-(NSArray*)fetchProductFromShoppingCart {
    return _shoppingBag;
}

#pragma mark encode/decode the cart to archiver for future use.
//we need to add shopping bag to nsuserdefault so that if the user get logged out and again get login ,he/she will be able to get cart item.
-(void)archieveShoppingCart  {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:_shoppingBag];
    [userDefault setObject:encodedObject forKey:[NSString stringWithFormat:kShopingCartArray]];
    [userDefault synchronize];
    encodedObject = nil;
    userDefault = nil;
}

#pragma mark Get cart from archiver.
-(NSMutableArray *)getCartFromArchiver    {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *decodedObject = [userDefault objectForKey: kShopingCartArray];
    NSMutableArray *cartArray = [[NSKeyedUnarchiver unarchiveObjectWithData: decodedObject] mutableCopy];
    decodedObject = nil;
    userDefault = nil;
    return cartArray;
}

#pragma mark remove userInfo from archiver when user logout from app
-(void)removeUserInfoFromArchiver   {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:nil forKey:[NSString stringWithFormat:kShopingCartArray]];
    [userDefault synchronize];
    userDefault = nil;
}
@end