//
//  SlideNavigationContorllerAnimationScaleAndFade.h
//  SlideMenu
//
//  Created by Kuldeep Tyagi on 4/08/2015.
//  Copyright (c) 2013 Classic. All rights reserved.

#import <Foundation/Foundation.h>
#import "SlideNavigationContorllerAnimator.h"

@interface SlideNavigationContorllerAnimatorScaleAndFade : NSObject <SlideNavigationContorllerAnimator>

- (id)initWithMaximumFadeAlpha:(CGFloat)maximumFadeAlpha fadeColor:(UIColor *)fadeColor andMinimumScale:(CGFloat)minimumScale;

@end
