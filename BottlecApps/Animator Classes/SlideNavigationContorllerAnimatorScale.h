//
//  SlideNavigationContorllerAnimationScale.h
//  SlideMenu
//
//  Created by Kuldeep Tyagi on 4/08/2015.
//  Copyright (c) 2013 Classic. All rights reserved.

#import <Foundation/Foundation.h>
#import "SlideNavigationContorllerAnimator.h"

@interface SlideNavigationContorllerAnimatorScale : NSObject <SlideNavigationContorllerAnimator>

@property (nonatomic, assign) CGFloat minimumScale;

- (id)initWithMinimumScale:(CGFloat)minimumScale;

@end
