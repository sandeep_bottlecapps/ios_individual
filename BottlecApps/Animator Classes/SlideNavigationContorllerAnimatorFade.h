//
//  SlideNavigationContorllerAnimationFade.h
//  SlideMenu
//
//  Created by Kuldeep Tyagi on 4/08/2015.
//  Copyright (c) 2013 Classic. All rights reserved.

#import <Foundation/Foundation.h>
#import "SlideNavigationContorllerAnimator.h"

@interface SlideNavigationContorllerAnimatorFade : NSObject <SlideNavigationContorllerAnimator>

@property (nonatomic, assign) CGFloat maximumFadeAlpha;
@property (nonatomic, strong) UIColor *fadeColor;

- (id)initWithMaximumFadeAlpha:(CGFloat)maximumFadeAlpha andFadeColor:(UIColor *)fadeColor;

@end
