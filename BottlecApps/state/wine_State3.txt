{

    "Regions": [
        {
            "RegionId": 36,
            "RegionName": "Baden"
        },
        {
            "RegionId": 53,
            "RegionName": "Franken"
        },
        {
            "RegionId": 59,
            "RegionName": "Mittle Rhein"
        },
        {
            "RegionId": 20,
            "RegionName": "Mosel-Saar-Ruwer"
        },
        {
            "RegionId": 38,
            "RegionName": "Nahe"
        },
        {
            "RegionId": 49,
            "RegionName": "Pfalz"
        },
        {
            "RegionId": 44,
            "RegionName": "Rheingau"
        },
        {
            "RegionId": 46,
            "RegionName": "Rheinhessen"
        },
        {
            "RegionId": 40,
            "RegionName": "Württemberg"
        }
    ]

}