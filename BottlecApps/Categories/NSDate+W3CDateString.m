//
//  NSDate+W3CDateString.m

//
//  Created by sachin kumaram 
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "NSDate+W3CDateString.h"


@implementation NSDate (W3CDateString)

+ (NSDate *)dateFromW3CDateString:(NSString *)string {
	// NSDateFormatter doesn't correctly recognize Z as equivalent to UTC, so we need to parse that manually
	NSString *dateString = [string stringByReplacingOccurrencesOfString:@"Z" withString:@"UTC"];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	[dateFormatter setTimeZone:gmt];
	[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
	NSDate *date = [dateFormatter dateFromString:dateString];
	
	
	return date;
}

@end
