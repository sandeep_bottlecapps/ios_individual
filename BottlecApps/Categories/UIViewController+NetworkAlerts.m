//
//  UIViewController+NetworkAlerts.m
//  LiquorApp
//
//  Created by sachin kumaram on 9/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UIViewController+NetworkAlerts.h"


@implementation UIViewController (NetworkAlerts)


- (void)showAlertViewForHTTPStatusCode:(NSInteger)statusCode {
	NSString *title = nil;
	NSString *message = nil;
	
	switch (statusCode) {
		case 400:
		case 401:
		case 402:
		case 403:
		case 404:
		case 405:
		case 406:
		case 407:
		case 408:
		case 409:
		case 410:
		case 411:
		case 412:
		case 413:
		case 414:
		case 415:
		case 416:
		case 417:
			title = @"Request Error";
			message = @"There was an error with your request and the server could not process it.";
			break;
		case 500:
		case 501:
		case 502:
		case 503:
		case 504:
		case 505:
			title = @"Server Error";
			message = @"Your request could not be completed. An error has occurred on the server.";
			break;
		default:
			title = @"Unknown Error";
			message = @"Your request could not be completed at this time. An unkown error has occurred.";
			break;
	}
	
	[self showAlertViewWithTitle:title message:message];
}

- (void)showAlertViewForError:(NSError *)error
{
	NSString *title = @"Unknown Error";
	NSString *message = @"An unknown error has occurred.";
	
	[self showAlertViewWithTitle:title message:message];
}

- (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message {
	UIAlertView *alertView = alertView = [[UIAlertView alloc] initWithTitle:title
																	message:message
																   delegate:self
														  cancelButtonTitle:@"OK"
														  otherButtonTitles:nil];
	
	[alertView show];
}

@end
