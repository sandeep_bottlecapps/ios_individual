//
//  NSDate+W3CDateString.h

//
//  Created by sachin kumaram 
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSDate (W3CDateString)

+ (NSDate *)dateFromW3CDateString:(NSString *)dateString;

@end
