//
//  UIViewController+NetworkAlerts.h
//  LiquorApp
//
//  Created by sachin kumaram on  12/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIViewController (NetworkAlerts)<UIAlertViewDelegate>

- (void)showAlertViewForHTTPStatusCode:(NSInteger)statusCode;
- (void)showAlertViewForError:(NSError *)error;
- (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message;
//- (void)showConfirmViewWithTitle:(NSString *)title message:(NSString *)message otherButtonTitle:(NSString *)otherButtonName;

@end
