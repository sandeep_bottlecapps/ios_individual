//
//  terms&conditionOperation.h
//  liquor
//
//  Created by IMAC5 on 2/17/14.
//
//

#import <Foundation/Foundation.h>
@protocol TermsOperationDelegate ;

@interface terms_conditionOperation : NSOperation
@property (nonatomic, assign) id <TermsOperationDelegate> termsDelegate;
@property(nonatomic,copy)NSString *url;
@end


//protocol to perform server operations

@protocol TermsOperationDelegate <NSObject>

@optional

- (void)serverOperationdidFinishWithAuthenticationKey:(NSDictionary *)key;
- (void)serverOperationdidFailWithResponse:(NSHTTPURLResponse *)response;
- (void)serverOperationdidFailWithError:(NSError *)error;

@end
