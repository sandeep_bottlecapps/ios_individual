//
//  terms&conditionOperation.m
//  liquor
//
//  Created by IMAC5 on 2/17/14.
//
//

#import "terms&conditionOperation.h"

@implementation terms_conditionOperation
@synthesize termsDelegate,url;
- (void)main
{
    
	NSError *error = nil;
	NSHTTPURLResponse *response = nil;
	NSString *URL = nil;
	
    URL = @"http://staging.liquorapps.com/WhiskeyStoreService/Login.svc/GetTerms";
	URL = [URL stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
	NSURL *urlString = [NSURL URLWithString:URL];
    NSLog(@"%@",urlString);
    
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlString];
	NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    //  NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
	if(error)
    {
		[self performSelectorOnMainThread:@selector(operationDidFailWithError:) withObject:error waitUntilDone:NO];
	}
    else if([response statusCode] != 200)
    {
		[self performSelectorOnMainThread:@selector(operationDidFailWithResponse:) withObject:response waitUntilDone:NO];
	}
   
        if (data!= Nil)
        {
            NSDictionary *Dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            [self performSelectorOnMainThread:@selector(operationDidFinishWithAuthenticationKey:) withObject:Dict waitUntilDone:NO];
        }
}

- (void)operationDidFinishWithAuthenticationKey:(NSDictionary *)key
{
    [termsDelegate serverOperationdidFinishWithAuthenticationKey:key];
}

- (void)operationDidFailWithResponse:(NSHTTPURLResponse *)response
{
    [termsDelegate serverOperationdidFailWithResponse:response];
}

- (void)operationDidFailWithError:(NSError *)error
{
    [termsDelegate serverOperationdidFailWithError:error];
}


@end
