/////FROM previous code//////////
#define DEFAULT_LIMIT 20
#define DEFAULT_ORDER_LIMIT 5
#define ACCEPTABLE_CHARECTERS_number @"0123456789."
# define PHONELIMIT 14 // Or whatever you want
#define ALPHA                   @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define NUMERIC                 @"1234567890"
#define ALPHA_NUMERIC           ALPHA NUMERIC
#define GOOGLE_GEO_KEY @"eIpG7XuIxtytQiPg01CBxYnL"

#define DEVICEID                     @"deviceId"

#define EVENTSELECTED                 @"0"

#define BADGECOUNT                    @"0"


#define UserDefaults                        [NSUserDefaults standardUserDefaults]
//#define UserDefaultsSelectedDate                        [[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedDate"]
/////FROM previous code//////////
//#define IS_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)

#define kGetUserModel [[ModelManager modelManager] getuserModel]
#define kGetClosestStoreModel [[ModelManager modelManager] getClosestStoreModel]
#define kGetStoreDetailsModel [lClosestStoreModelObj.GetStoredetails objectAtIndex:0]

#define kGetProducts [[ModelManager modelManager] getProductsObj]

#define kGetProductModel [[ModelManager modelManager] getProductModel]
#define kgetUserProfileModel [[ModelManager modelManager] getUserProfileModel]

//
#define kTextFieldCustomViewWidth     50
#define kTextFieldCustomViewHeight    50
#define kCustomScrollViewHeight   self.CustomScrollView.frame.size.height
#define kCustomScrollViewWidth    self.CustomScrollView.frame.size.width
#define krowHeight (kLabelHeight*5)-10
#define krowHeightFavorite (kLabelHeight*4) - 2
//(kProductImageHeight+kLabelHeight+(kPaddingSmall*3))

//(kProductImageHeight+kLabelHeight+(kPaddingSmall*3))


//krowHeightLarge-8
#define kRowHeight_custom                         ((self.defaultTableView.frame.size.height)/4)
#define kRowHeight_custom_without_Beer            ((self.defaultTableView.frame.size.height)/4)+15.0
#define kIconImageHightAndWidth                                      23

#define krowHeightLarge  (IS_IPHONE_4_OR_LESS? 150:IS_IPHONE_6?150:150)
#define krowHeigtProductDetails (IS_IPHONE_4_OR_LESS? 435:IS_IPHONE_6?435:435)
#define kRowHeight_orderconfirmationSmall                               80
#define kRowHeight_orderconfirmationLarge                               320
#define kHeightAddaddressContainer                                      150
#define appdelegateSharedInstance           (AppDelegate *) [[UIApplication sharedApplication]delegate]


#define kSectionHeight                                76
#define kCustomCellImageViewWidth                          70
#define kCustomCellImageViewheight                         70
#define kCartLikeViewHeight                            krowHeight-1
#define kcellLabelwidth 90
#define kcellLabelheight 20


#define ksearchStripHeight                         60
#define kLogoImageWidth      100
#define kLogoImageHeight     100
#define kProductImageWidthHeight  50

#define kProductImageWidth  70
//(productImage.size.width)
#define kProductImageHeight 70
//(productImage.size.height)
#define kcartImageWidth      50
#define kcartImageHeight     50
#define kCellTextLabelLeftPadding           (IS_IPHONE_4_OR_LESS? 100:IS_IPHONE_6?120:100)

//104
#define kTableViewHeight                    (SCREEN_HEIGHT/2)-kinBetweenGaps-kBottomViewHeight
#define kTableViewYcorr                     (SCREEN_HEIGHT/2)+kinBetweenGaps

#define kStarRatingWidth 130

#pragma mark- Screen height

#define kCellPadding  (IS_IPHONE_4_OR_LESS? 50:IS_IPHONE_6?60:50)
//13
#define kPaddingcartAndFav 1

#define kPaddingSmall 5
#define kheartIconWidth  32
#define kHeartIconHeight 27
#define kiPhone4Height 480
#define kiPhone5Height 568
#define kiPhone6Height 667
#define kiPhone6PlusHeight 736
#define kDoneButtonWidth 50
#define kDoneButtonHeight 30

#define kNavigationheight 64
#define kRowHeight_customForEventDetailScreeen (IS_IPHONE_4_OR_LESS? 50:IS_IPHONE_6?80:70)

#define kContentViewHeight 300
#define kContentViewwidtht 250

#define k_containerViewWidth _containerView.frame.size.width
#define k_containerViewHeight _containerView.frame.size.width

#define IS_OS_5_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.0)
#define IS_OS_6_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
#define IS_OS_7_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.1)
#define IS_OS_9_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)
#define IS_OS_9_OR_Prior ([[[UIDevice currentDevice] systemVersion] floatValue] <= 9.0)

//104
//defining the threshold value to get menu button enabled
#define kThresholdDistance 300 // client ask to change to 300m

#pragma mark- chech for the IOS version
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

//#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
//#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
//#define HALF_SCREEN_HEIGHT (SCREEN_HEIGHT/2)
//#define HALF_SCREEN_WIDTH (SCREEN_WIDTH/2)

#define kHeightUserProfileLogo (IS_IPHONE_4_OR_LESS? 60:IS_IPHONE_6?104:60)
#define kWidthUserProfileLogo  (IS_IPHONE_4_OR_LESS? 60:IS_IPHONE_6?104:60)
#define kimageCamera  [UIImage imageNamed:kImage_cameraicon]

#define kHeightCamera    cameraImge.size.height
#define kWidthUserCamera  cameraImge.size.width
//[UIImage imageNamed:kImage_cameraicon]
#define kHeightLoyaltyCard 270
#define kWidthLoyaltyCard 112
#define keditReviewImageWidth              (IS_IPHONE_4_OR_LESS? 26:IS_IPHONE_6?26:26)
#define keditReviewImageHeight              (IS_IPHONE_4_OR_LESS? 26:IS_IPHONE_6?26:26)

#define kGreyBorderTextfieldWidth_small  ((SCREEN_WIDTH-(kLeftPadding*2))/2)-2
#define kGreyBorderTextfieldWidth_Large  (SCREEN_WIDTH-(kLeftPadding*2))

//(SCREEN_WIDTH-(kLeftPadding*2))/2
//(((_containerView.frame.size.width)/3)-(2*kPaddingSmall))
//#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
//#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

//#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
//#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
//#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH >= 667.0)
//#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define kSpaceSignup             (IS_IPHONE_4_OR_LESS? 20:IS_IPHONE_6?20:20)

#define kTEXTfieldInBetweenPadding                   (IS_IPHONE_4_OR_LESS? 5:IS_IPHONE_6?5:2)
#define kTEXTfieldHeight                            (IS_IPHONE_4_OR_LESS? 35:IS_IPHONE_6?40:35)
#define kTEXTfieldHeightSmall                       (IS_IPHONE_4_OR_LESS? 20:IS_IPHONE_6?30:25)

#define kTEXTViewHeightLarge                       (IS_IPHONE_4_OR_LESS? 100:IS_IPHONE_6?160:150)
#define kTEXTViewHeightSmall                       (IS_IPHONE_4_OR_LESS? 50:IS_IPHONE_6?80:50)
#define kdropdownButtonHeight                          (IS_IPHONE_4_OR_LESS? 20:IS_IPHONE_6?40:30)

//monika 16 Nov
#define kTextViewPadding                                (kLeftPadding-5)
#define kButtonHeight                          (IS_IPHONE_4_OR_LESS? 40:IS_IPHONE_6?40:40)
#define kButtonHeight2                          (IS_IPHONE_4_OR_LESS? 30:IS_IPHONE_6?40:40)

/////

#define kCustomButtonContainerHeight            (IS_IPHONE_4_OR_LESS? 60:IS_IPHONE_6?60:60)
#define kLabelHeight                          (IS_IPHONE_4_OR_LESS? 20:IS_IPHONE_6?25:20)

#define kLabelWidth                          (IS_IPHONE_4_OR_LESS? 100:IS_IPHONE_6?100:100)
#define kLabelWidthForCell                         kLabelWidth*3

#define kLabelWidthLarge                          (IS_IPHONE_4_OR_LESS? 250:IS_IPHONE_6?350:250)
#define kLabelHeightLarge                          (IS_IPHONE_4_OR_LESS? 40:IS_IPHONE_6?50:40)

#define kLabelWidthMax                         SCREEN_WIDTH-(2*kLeftPadding)
#define kLabelHeightSmall                       (IS_IPHONE_4_OR_LESS? 15:IS_IPHONE_6?15:20)
#define kLabelWidthSmall                          (IS_IPHONE_4_OR_LESS? 60:IS_IPHONE_6?70:60)
#define kLabelWidthOrderSummary                          (IS_IPHONE_4_OR_LESS? 120:IS_IPHONE_6?120:120)

#define kTEXTfieldWidth            (IS_IPHONE_4_OR_LESS? 150:IS_IPHONE_6?200:150)

#define kFontRegularSize            (IS_IPHONE_4_OR_LESS? 12:IS_IPHONE_6?14:12)
#define kFontBoldSize               (IS_IPHONE_4_OR_LESS? 14:IS_IPHONE_6?15:14)

#if kStoreBottleCapps == 3||kStoreBottleCapps == 4
#define kFontSemiBoldSize      (IS_IPHONE_4_OR_LESS? 14:IS_IPHONE_6?17:14)
#else
#define kFontSemiBoldSize      (IS_IPHONE_4_OR_LESS? 14:IS_IPHONE_6?14:14)
#endif

#define kFontDOB      (IS_IPHONE_4_OR_LESS? 15:IS_IPHONE_6?17:17)
#define kFontUserName      (IS_IPHONE_4_OR_LESS? 16:IS_IPHONE_6?18:18)

#define kFontBoldSizeExtraSize      (IS_IPHONE_4_OR_LESS? 18:IS_IPHONE_6?20:18)

#define kButtonwidthTranparentView              (kPriceRangeViewWidth/3)

#define kButtonwidth              150
#define kTapToDetailButtonwidth              (IS_IPHONE_4_OR_LESS? 60:IS_IPHONE_6?150:60)


#define kButtonNotificationHeight              (kPriceRangeViewWidth/9)

#define kcustomButtonwidth              (IS_IPHONE_4_OR_LESS? 200:IS_IPHONE_6?250:200)
//150
#define kCartAndLikeViewWidth                (IS_IPHONE_4_OR_LESS? 100:IS_IPHONE_6?150:100)

#define kBottomViewHeight              33
#define kimageIconTopPadding        (IS_IPHONE_4_OR_LESS? 10:IS_IPHONE_6?10:10)
#define kTopPadding_small                 5
#define kTopPadding_logo                 (IS_IPHONE_4_OR_LESS? 45:IS_IPHONE_6?40:25)



#define kLeftPadding_label                (IS_IPHONE_4_OR_LESS? 28:IS_IPHONE_6?30:28)

#define kTopPaddingForButton                 (IS_IPHONE_4_OR_LESS? 40:IS_IPHONE_6?60:55)
//#define kTopPadding                 kStoreBottleCapps?(IS_IPHONE_4_OR_LESS? 10:IS_IPHONE_6?30:25):(IS_IPHONE_4_OR_LESS? 50:IS_IPHONE_6?70:65)
//#define kTopPadding2                 kStoreBottleCapps?(IS_IPHONE_4_OR_LESS? 10:IS_IPHONE_6?30:25):(IS_IPHONE_4_OR_LESS? 100:IS_IPHONE_6?120:120)

//kStoreBottleCapps
#define kTopPadding                 (IS_IPHONE_4_OR_LESS? 10:IS_IPHONE_6?30:25)
#define kLeftPadding                (IS_IPHONE_4_OR_LESS? 40:IS_IPHONE_6?50:50)
#define kPaddingFromright                (IS_IPHONE_4_OR_LESS? 40:IS_IPHONE_6?80:50)

#define kRightPadding               (IS_IPHONE_4_OR_LESS? 20:IS_IPHONE_6?30:20)
#define kBottomPadding               (IS_IPHONE_4_OR_LESS? 10:IS_IPHONE_6?30:20)
#define kBottomPaddingViewMoreButton               (IS_IPHONE_4_OR_LESS? 100:IS_IPHONE_6?60:70)

#define kSpaceinBottombackAndLogo    (IS_IPHONE_4_OR_LESS? 50:IS_IPHONE_6?60:50)
#define kTermsAndConditionPadding    (IS_IPHONE_4_OR_LESS? 20:IS_IPHONE_6?20:20)
#define kRedBorderButtonWidth       (IS_IPHONE_4_OR_LESS? 235:IS_IPHONE_6?235:235)
#define kredBorderButtonHeight      (IS_IPHONE_4_OR_LESS? 55:IS_IPHONE_6?55:55)


#define kTermsAndConditionRightPadding    (IS_IPHONE_4_OR_LESS? 45:IS_IPHONE_6?50:45)
//255


#define kinBetweenGaps              (IS_IPHONE_4_OR_LESS? 20:IS_IPHONE_6?30:10)
#define krowPadding                 (IS_IPHONE_4_OR_LESS? 20:IS_IPHONE_6?30:5)

#define kinBetweenbuttons           (IS_IPHONE_4_OR_LESS? 10:IS_IPHONE_6?30:10)

#define kTableTextTopPadding        (IS_IPHONE_4_OR_LESS? 10:IS_IPHONE_6?30:20)
#define kNavigationTopPadding        2
#define ksidePadding                (IS_IPHONE_4_OR_LESS? 14:IS_IPHONE_6?14:14)
#define kLoginfooterHeight          (IS_IPHONE_4_OR_LESS? 65:IS_IPHONE_6?107:100)
#define kSigninButtonHeight          (IS_IPHONE_4_OR_LESS? (signinbuttonImage.size.width)-50:IS_IPHONE_6?(signinbuttonImage.size.width):(signinbuttonImage.size.width)-10)
#define kSigninButtonWidth          (IS_IPHONE_4_OR_LESS? (signinbuttonImage.size.height)-50:IS_IPHONE_6?(signinbuttonImage.size.height):(signinbuttonImage.size.height)-10)




#define kNavigationLogoImageFrame        (IS_IPHONE_4_OR_LESS? CGRectMake(0, kNavigationTopPadding, HeaderLogoImage.size.width-10, HeaderLogoImage.size.height-10):IS_IPHONE_6?CGRectMake(0, kNavigationTopPadding, HeaderLogoImage.size.width-10, HeaderLogoImage.size.height-10):CGRectMake(0, kNavigationTopPadding, HeaderLogoImage.size.width-10, HeaderLogoImage.size.height-10)
//CGRectMake(0, kNavigationTopPadding, HeaderLogoImage.size.width, HeaderLogoImage.size.height)
#define kUserEmailTextFieldFrame        (IS_IPHONE_4_OR_LESS? CGRectMake((SCREEN_WIDTH-140)/2, kTopPadding, 140, 137):IS_IPHONE_6?CGRectMake((SCREEN_WIDTH-202)/2, kTopPadding, 202, 213):CGRectMake((SCREEN_WIDTH-202)/2, kTopPadding, 202, 213))

#define kStoreAddressWithLogoViewHeight         StoreAddressWithLogoView.frame.size.height
#define kStoreAddressWithLogoViewWidth          StoreAddressWithLogoView.frame.size.width
#define kStoreHoursViewHeight                   StoreHoursView.frame.size.height
#define kStoreHoursViewWidth                    StoreHoursView.frame.size.width
#define kEmailAndPhoneViewHeight                EmailAndPhoneView.frame.size.height
#define kEmailAndPhoneViewWidth                 EmailAndPhoneView.frame.size.width



#pragma mark-CONTAINERVIEW WITH CLOSE BUTTON
#define kFilterContainerViewHeight SCREEN_HEIGHT-kNavigationheight-kBottomViewHeight-kBottomPadding
#define kFilterContainerViewWidth SCREEN_WIDTH-10
#define kcloseButtonWidth kFilterContainerViewWidth
#define kcloseButtonYcorr  kFilterContainerViewHeight-kButtonHeight





