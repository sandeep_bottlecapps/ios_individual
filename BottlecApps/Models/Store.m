//
//  Store.m
//  BOTTLECAPPS
//
//  Created by imac9 on 9/2/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "Store.h"

@implementation Store
@synthesize StoreId;
@synthesize StoreName;
-(id)initWithStoreDetails:(NSDictionary *)StoreDetails{
 self = [super init];
    if(self)    {
        DLog(@"StoreDetails---%@",StoreDetails);
        self.StoreId                     = [self ProperValue : [StoreDetails valueForKey : kModelStoreId]];
        self.StoreName                   = [self ProperValue : [StoreDetails valueForKey : kModelStoreName]];
       
    }
    return self;

}
@end
