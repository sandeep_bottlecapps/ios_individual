//
//  ViewMyOrderModel.m
//  BOTTLECAPPS
//
//  Created by imac9 on 9/8/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ViewMyOrderModel.h"
#import "ViewMyOrderProductListModel.h"
@implementation ViewMyOrderModel
@synthesize orderListArray;
-(id)initWithViewMyOrderProductsList:(NSArray *)ViewMyOrderProductsListArr    {
    self = [super init];
    if(self)
    {
        self.orderListArray=[[ NSMutableArray alloc] init];
           DLog(@"ViewMyOrderProductsListArr%@",ViewMyOrderProductsListArr);
        if(ViewMyOrderProductsListArr && ViewMyOrderProductsListArr != NULL)   {
            for (NSDictionary *ordersList in ViewMyOrderProductsListArr) {
                ViewMyOrderProductListModel *OrderObj = [[ViewMyOrderProductListModel alloc] initWithOrderList:ordersList];
                [self.orderListArray addObject:OrderObj];
                DLog(@"orderListArray---%@",ordersList);

            }

        }
    }
    return self;
}

-(void) dealloc {
    [self.orderListArray removeAllObjects];
    self.orderListArray         = nil;
}

-(NSString*)getTotalNumberOfRecords:(NSString *)totalNoOfRecords{
    self.TotalNumberOfRecords  = [self ProperValue : totalNoOfRecords];
    return self.TotalNumberOfRecords;
}


@end
