//
//  StoreAddress.h
//  BOTTLECAPPS
//
//  Created by imac9 on 9/2/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ModelBase.h"

@interface StoreAddress : ModelBase

@property (nonatomic, strong) NSString              *StateName;
@property (nonatomic, strong) NSString              *UserAddress1;
@property (nonatomic, strong) NSString              *UserAddress2;
@property (nonatomic, strong) NSString              *StoreName;
@property (nonatomic, strong) NSString              *UserCity;
@property (nonatomic, strong) NSString              *UserLatitude;
@property (nonatomic, strong) NSString              *UserLongitude;
@property (nonatomic, strong) NSString              *UserZipCode;

-(id)initWithStoreAddress:(NSMutableArray *)StoreAddress;

@end
