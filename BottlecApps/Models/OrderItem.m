//
//  OrderItem.m
//  BOTTLECAPPS
//
//  Created by IMAC5 on 11/19/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "OrderItem.h"

@implementation OrderItem
@synthesize phoneNumber;
@synthesize firstName;
@synthesize lastName;
@synthesize address;
@synthesize address2;
@synthesize city;
@synthesize state;
@synthesize zipCode;
@synthesize productId;
@synthesize productName;
@synthesize productPrice;
@synthesize productSalePrice;
@synthesize productUPCNumber;
@synthesize productIsOnSale;
@synthesize productOutOfStockStatus;
@synthesize productTaxRatio;
@synthesize orderSalePrice;
@synthesize orderStatus;
@synthesize orderProductQuantity;

-(id)initWithOrderItemDetails:(NSDictionary *)orderItemDetails  {
    self = [super init];
    @try {
        if(self)    {
            self.phoneNumber                = [self ProperValue : [orderItemDetails valueForKey : @"Contact"]];
            self.firstName                  = [self ProperValue : [orderItemDetails valueForKey : @"Fname"]];
            self.lastName                   = [self ProperValue : [orderItemDetails valueForKey : @"Lname"]];
            self.address                    = [self ProperValue : [orderItemDetails valueForKey : @"Address"]];
            self.address2                    = [self ProperValue : [orderItemDetails valueForKey : @"Address2"]];
            self.city                       = [self ProperValue : [orderItemDetails valueForKey : @"City"]];
            self.state                      = [self ProperValue : [orderItemDetails valueForKey : @"State"]];
            self.zipCode                    = [self ProperValue : [orderItemDetails valueForKey : @"ZipCode"]];
            self.productId                  = [self ProperValue : [orderItemDetails valueForKey : @"ProductId"]];
            self.productName                = [self ProperValue : [orderItemDetails valueForKey : @"ProductName"]];
            self.productPrice               = [self ProperValue : [orderItemDetails valueForKey : @"ProductPriceVal"]];
            self.productSalePrice           = [self ProperValue : [orderItemDetails valueForKey : @"ProductSalePrice"]];
            self.productUPCNumber           = [self ProperValue : [orderItemDetails valueForKey : @"ProductUPC"]];
            self.productIsOnSale            = [[orderItemDetails valueForKey : @"IsOnSaleItems"] intValue];
            self.productOutOfStockStatus    = [[orderItemDetails valueForKey : @"ProductStatus"] intValue];
            self.productTaxRatio            = [self ProperValue : [orderItemDetails valueForKey : @"ProductTax"]];
            self.orderSalePrice             = [self ProperValue : [orderItemDetails valueForKey : @"OrderedSalePrice"]];
            self.orderStatus                = [self ProperValue : [orderItemDetails valueForKey : @"OrderStatus"]];
            self.orderProductQuantity       = [[orderItemDetails valueForKey : @"Quantity"] intValue];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception.userInfo);
    }
    @finally {
        return self;
    }
}

//Address = "400 E Royal Lane";
//City = Irving;
//Contact = 9818727302;
//Fname = Abhishek;
//IsOnSaleItems = 1;
//Lname = Jain;
//OrderStatus = Pending;
//OrderedSalePrice = "10.49";
//ProductId = "";
//ProductName = "446 Single Chardonnay";
//ProductPriceVal = "10.49";
//ProductSalePrice = "0.00";
//ProductStatus = 1;
//ProductTax = "0.0800";
//ProductUPC = 82242562133;
//Quantity = 1;
//State = "";
//ZipCode = 75039;

@end
