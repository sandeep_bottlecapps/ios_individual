//
//  ClosestStoreModel.m
//  LiquorApp
//
//  Created by Anindya on 16/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ClosestStoreModel.h"
#import "StoreAddress.h"
#import "StoreDetails.h"
#import "StoreOpenCloseTime.h"

@implementation ClosestStoreModel
@synthesize GetStoreOpenclose,GetStoredetails,UserLatituteDetailStore;
-(id)initWithStoreInfo:(NSDictionary *)StoreInfo    {
    self = [super init];
    if(self)    {
        id obj = [StoreInfo valueForKey:@"GetStoreDetailResult"];
        StoreAddress *storeAddress = [[StoreAddress alloc] initWithStoreAddress:[obj valueForKey:@"UserLatituteDetailStore"]];
        self.UserLatituteDetailStore = [[NSMutableArray alloc] initWithObjects:storeAddress, nil];

        
        StoreDetails *storeDetails = [[StoreDetails alloc] initWithStoreDetails:[obj valueForKey:@"GetStoredetails"]];
        self.GetStoredetails  = [[NSMutableArray alloc] initWithObjects:storeDetails, nil];
        
        self.GetStoreOpenclose = [[NSMutableArray alloc] init];
        for (NSDictionary *storeTimeDict in [obj valueForKey:@"GetStoreOpenclose"]) {
            StoreOpenCloseTime *storeOpenCloseTime = [[StoreOpenCloseTime alloc] initWithStoreOpenCloseTime: storeTimeDict];
            [self.GetStoreOpenclose addObject:storeOpenCloseTime];
        }
    }
    return self;
}

-(void) dealloc {
    [self.GetStoredetails removeAllObjects];
    [self.GetStoredetails removeAllObjects];
    [self.UserLatituteDetailStore removeAllObjects];
    
    self.GetStoreOpenclose         = nil;
    self.GetStoredetails           = nil;
    self.UserLatituteDetailStore   = nil;
   }


@end
