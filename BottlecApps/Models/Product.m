//
//  Product.m
//  LiquorApp
//
//  Created by sachin kumaram on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Product.h"

#define PRODUCT_ID_ELEMENT @"ProductID"
#define PRODUCTPRICE_ID_ELEMENT @"ProductPriceID"
#define PRODUCTPRICE_ELEMENT @"ProductPrice"
#define PRODUCTDiscount_ELEMENT @"discount"
#define PRODUCT_NAME_ELEMENT @"ProductName"
#define PRODUCT_SIZE_ELEMENT @"ProductSize"
#define PRODUCT_IMAGEURL_ELEMENT @"ProductImg"
#define PRODUCT_IMAGEURL1_ELEMENT @"ProductImage"
#define PRODUCT_DESC @"ProductDesc"
#define PRODUCT_SUBCAT_NAME @"SubCatName"
#define PRODUCT_CAT_NAME @"CatName"
#define PRODUCT_UPC @"ProductUPC"
#define PRODUCT_STATUS @"ProductStatus"
#define PRODUCT_SALES_PRICE @"ProductSalePrice"
#define PRODUCT_FAV_STATUS @"IsFavorite"
#define PRODUCT_ADDLIST_STATUS @"AddTolstStatus"
#define PRODUCT_VOTE_COUNT @"NoOfVote"
#define PRODUCT_RATING @"StoreRating"
#define PRODUCT_SKU @"ProductSKU"

#define PRODUCT_SUBCATG @"SubCatName"
#define PRODUCT_FLAVOR @"Flavor"
#define PRODUCT_TAX @"ProductTax"
#define PRODUCT_ONSALEITEM @"IsOnSaleItems"

#define PRODUCT_COUNTRYNME @"CountryName"
#define PRODUCT_VARIETAL @"SubCatName2"
#define PRODUCT_QUANITY @"Quantity"

@implementation Product

@synthesize productId,productPriceId,productName,productSize,productImageURL,productImage,totalNoOfProducts,numberOfVote;
@synthesize productDesc,productRating,productCatName,productUpc;
@synthesize productFavStatus,productAddListStatus,productPrice,productStatus,productSalesPrice,ProductTax,discount;
@synthesize productSubCatg,productFlavor,productCountryNme,productVarietal,TEMPproductStatus,onSaleItem,TEMPproductQuanity;
@synthesize ProductSku;
@synthesize orderQuantity;
- (id)initWithDictionary:(NSDictionary *)dictionary { 
    if(self = [super init]) {
        if ([[dictionary allKeys]containsObject:PRODUCT_SKU]) {
            self.ProductSku = [self ProperValue:[dictionary valueForKey:PRODUCT_SKU]];
        }
        self.productId = [self ProperValue:[dictionary valueForKey:PRODUCT_ID_ELEMENT]];
        if ([[dictionary allKeys]containsObject:PRODUCTPRICE_ID_ELEMENT]) {
            self.productPriceId = [self ProperValue:[dictionary valueForKey:PRODUCTPRICE_ID_ELEMENT]];
        }
          self.discount     = [self ProperValue:[dictionary valueForKey:PRODUCTDiscount_ELEMENT]];
        self.productPrice   = [self ProperValue:[dictionary valueForKey:PRODUCTPRICE_ELEMENT]];
        self.ProductTax     = [self ProperValue:[dictionary valueForKey:PRODUCT_TAX]];
        self.productName    = [self ProperValue:[dictionary valueForKey:PRODUCT_NAME_ELEMENT]];
        self.productSize    = [self ProperValue:[dictionary valueForKey:PRODUCT_SIZE_ELEMENT]];
        id theProductImgURL = ([dictionary valueForKey:PRODUCT_IMAGEURL_ELEMENT]) ? [dictionary valueForKey:PRODUCT_IMAGEURL_ELEMENT] : [dictionary valueForKey:PRODUCT_IMAGEURL1_ELEMENT];
        self.productImageURL= [self ProperValue:theProductImgURL];
        if ([[dictionary allKeys]containsObject:PRODUCT_DESC]) {
        self.productDesc    = [self ProperValue:[dictionary valueForKey:PRODUCT_DESC]];
        }
        if ([[dictionary allKeys]containsObject:PRODUCT_RATING]) {
        self.productRating  = [self ProperValue:[dictionary valueForKey:PRODUCT_RATING]];
        }
        if ([[dictionary allKeys]containsObject:PRODUCT_CAT_NAME]) {
        self.productCatName = [self ProperValue:[dictionary valueForKey:PRODUCT_CAT_NAME]];
        }
        if ([[dictionary allKeys]containsObject:PRODUCT_UPC]) {
        self.productUpc     = [self ProperValue:[dictionary valueForKey:PRODUCT_UPC]];
        }
        self.productFavStatus = [self ProperValue:[dictionary valueForKey:PRODUCT_FAV_STATUS]];
        if ([[dictionary allKeys]containsObject:PRODUCT_ADDLIST_STATUS]) {
        self.productAddListStatus = [self ProperValue:[dictionary valueForKey:PRODUCT_ADDLIST_STATUS]];
        }
        self.productStatus = [self ProperValue:[dictionary valueForKey:PRODUCT_STATUS]];
        self.productSalesPrice = [self ProperValue:[dictionary valueForKey:PRODUCT_SALES_PRICE]];
        
        if ([[dictionary allKeys]containsObject:PRODUCT_VOTE_COUNT]) {
        self.numberOfVote = [self ProperValue:[dictionary valueForKey:PRODUCT_VOTE_COUNT]];
        }
        if ([[dictionary allKeys]containsObject:PRODUCT_SUBCATG]) {
        self.productSubCatg = [self ProperValue:[dictionary valueForKey:PRODUCT_SUBCATG]];
        }
        if ([[dictionary allKeys]containsObject:PRODUCT_FLAVOR]) {
        self.productFlavor = [self ProperValue:[dictionary valueForKey:PRODUCT_FLAVOR]];
        }
        if ([[dictionary allKeys]containsObject:PRODUCT_ONSALEITEM]) {
        self.onSaleItem = [self ProperValue:[dictionary valueForKey:PRODUCT_ONSALEITEM]];
        }
        if ([[dictionary allKeys]containsObject:PRODUCT_COUNTRYNME]) {
        self.productCountryNme = [self ProperValue:[dictionary valueForKey:PRODUCT_COUNTRYNME]];
        }
        if ([[dictionary allKeys]containsObject:PRODUCT_VARIETAL]) {
        self.productVarietal = [self ProperValue:[dictionary valueForKey:PRODUCT_VARIETAL]];
        }
        self.TEMPproductStatus = [self ProperValue:[dictionary valueForKey:PRODUCT_STATUS]];
        DLog(@"%@",[dictionary allKeys]);
        if ([[dictionary allKeys]containsObject:PRODUCT_QUANITY]) {
        id thetempProductQuanity = [dictionary objectForKey:PRODUCT_QUANITY];
        self.TEMPproductQuanity = [self ProperValue:thetempProductQuanity];
        }
             }
    return self;
}

-(void)dealloc{
    //    [productId release];
    //    [productPriceId release];
    //    [productName release];
    //    [productSize release];
    //    [productImageURL release];
    //    [productImage release];
    //    [productDesc release];
    //    [productSalesPrice release];
    //    [productCatName release];
    //    [productUpc release];
    //    [productFavStatus release];
    //    [productAddListStatus release];
    //    [totalNoOfProducts release];
    //    [productSubCatg release];
    //    [productFlavor release];
    //    [ProductTax release];
    //    [ProductSku release];
    //
    //    [onSaleItem release];
    //    [productCountryNme release];
    //    [productVarietal release];
    //    [TEMPproductStatus release];
}
#pragma mark- archiver and Unarchiver methods
//Encode properties, other class variables, etc
/*
 @property (nonatomic, copy) NSString *productId;
 @property (nonatomic, copy) NSString *productPriceId;
 @property (nonatomic, copy) NSString *productName;
 @property (nonatomic, copy) NSString *productSize;
 @property (nonatomic, copy) NSString *productImageURL;
 @property (nonatomic, retain) UIImage *productImage;
 @property (nonatomic, copy)NSString *productDesc;
 @property (nonatomic,copy) NSString *productCatName;
 @property (nonatomic,copy) NSString *productUpc;
 @property(nonatomic, copy)NSString *productRating;
 @property(nonatomic, copy)NSString *productFavStatus;
 @property(nonatomic, copy)NSString *productAddListStatus;
 @property (nonatomic, copy) NSNumber *totalNoOfProducts;
 @property (nonatomic,copy) NSString *productPrice;
 @property (nonatomic, copy) NSString *productStatus;
 @property (nonatomic, copy) NSString *productSalesPrice;
 @property (nonatomic ,copy) NSString *numberOfVote;
 @property (nonatomic ,copy) NSString *ProductTax;
 @property (nonatomic ,copy) NSString *ProductSku;
 @property (nonatomic ,copy) NSString *productSubCatg;
 @property (nonatomic ,copy) NSString *productFlavor;
 @property (nonatomic ,copy) NSString *productCountryNme;
 @property (nonatomic ,copy) NSString *productVarietal;
 @property (nonatomic, copy) NSString *TEMPproductStatus;
 @property (nonatomic, copy) NSString *onSaleItem;
 @property (nonatomic, copy) NSString *TEMPproductQuanity;
 @property (nonatomic, assign) int orderQuantity;
 */

- (void)encodeWithCoder:(NSCoder *)encoder  {
    [encoder encodeObject: self.productCountryNme             forKey: PRODUCT_COUNTRYNME];
    [encoder encodeObject: self.productAddListStatus          forKey: PRODUCT_ADDLIST_STATUS ];
    [encoder encodeObject: self.productCatName                forKey: PRODUCT_CAT_NAME];
    [encoder encodeObject: self.productDesc                   forKey: PRODUCT_DESC];
    [encoder encodeObject: self.productFavStatus              forKey:PRODUCT_FAV_STATUS];
    [encoder encodeObject: self.productFlavor                 forKey:PRODUCT_FLAVOR];
    [encoder encodeObject: self.productId                     forKey:PRODUCT_ID_ELEMENT];
    [encoder encodeObject: self.productImage                  forKey:@"ProductImage"];
    [encoder encodeObject: self.productImageURL               forKey:PRODUCT_IMAGEURL1_ELEMENT];
    [encoder encodeObject: self.productName                   forKey:PRODUCT_NAME_ELEMENT];
    [encoder encodeObject: self.productPrice                  forKey:PRODUCTPRICE_ELEMENT];
    [encoder encodeObject: self.productPriceId                forKey:PRODUCTPRICE_ID_ELEMENT];
    [encoder encodeObject: self.productRating                 forKey:PRODUCT_RATING];
    [encoder encodeObject: self.productSalesPrice             forKey:PRODUCT_SALES_PRICE];
    [encoder encodeObject: self.productSize                   forKey:PRODUCT_SIZE_ELEMENT];
    [encoder encodeObject: self.ProductSku                    forKey:PRODUCT_SKU];
    [encoder encodeObject: self.productStatus                 forKey:PRODUCT_STATUS];
    [encoder encodeObject: self.productSubCatg                forKey:PRODUCT_SUBCAT_NAME];
    [encoder encodeObject: self.ProductTax                    forKey:PRODUCT_TAX];
    [encoder encodeObject: self.productUpc                    forKey:PRODUCT_UPC];
    [encoder encodeObject: self.productVarietal               forKey:PRODUCT_VARIETAL];
    [encoder encodeObject: self.productImage                  forKey:@"ProductImage"];
    [encoder encodeObject: self.totalNoOfProducts             forKey:@"Product_TotalNoOfProduct"];
    [encoder encodeObject: self.numberOfVote                  forKey:@"NUMBEROFVOTES"];
    [encoder encodeObject: self.TEMPproductStatus             forKey:@"TEMPPRODUCTSTATUS"];
    [encoder encodeObject: self.TEMPproductQuanity            forKey:@"TEMPproductQuanity"];
    [encoder encodeInteger: self.orderQuantity                forKey:@"orderQuantity"];
}

- (id)initWithCoder:(NSCoder *)decoder  {
    self = [super init];
    if(self)    {
              self.productId            = [decoder decodeObjectForKey: PRODUCT_ID_ELEMENT];
        self.productPriceId             = [decoder decodeObjectForKey: PRODUCTPRICE_ELEMENT];
        self.productName                = [decoder decodeObjectForKey: PRODUCT_NAME_ELEMENT];
        self.productSize                = [decoder decodeObjectForKey: PRODUCT_SIZE_ELEMENT];
        self.productImageURL            = [decoder decodeObjectForKey: PRODUCT_IMAGEURL1_ELEMENT];
        self.productImage               = [decoder decodeObjectForKey: @"ProductImage"];
        self.productDesc                = [decoder decodeObjectForKey: PRODUCT_DESC];
        self.productCatName             = [decoder decodeObjectForKey: PRODUCT_CAT_NAME];
        self.productUpc                 = [decoder decodeObjectForKey: PRODUCT_UPC];
        self.productRating              = [decoder decodeObjectForKey: PRODUCT_RATING];
        self.productFavStatus           = [decoder decodeObjectForKey: PRODUCT_FAV_STATUS];
        self.productAddListStatus       = [decoder decodeObjectForKey: PRODUCT_ADDLIST_STATUS];
        self.totalNoOfProducts          = [decoder decodeObjectForKey: @"Product_TotalNoOfProduct"];
        self.productPrice               = [decoder decodeObjectForKey: PRODUCTPRICE_ELEMENT];
        self.productStatus              = [decoder decodeObjectForKey: PRODUCT_STATUS];
        self.productSalesPrice          = [decoder decodeObjectForKey: PRODUCT_SALES_PRICE];
        self.numberOfVote               = [decoder decodeObjectForKey: @"NUMBEROFVOTES"];
        self.ProductTax                 = [decoder decodeObjectForKey: PRODUCT_TAX];
        self.ProductSku                 = [decoder decodeObjectForKey: PRODUCT_SKU];
        self.productSubCatg             = [decoder decodeObjectForKey: PRODUCT_SUBCAT_NAME];
        self.productFlavor              = [decoder decodeObjectForKey: PRODUCT_FLAVOR];
        self.productCountryNme          = [decoder decodeObjectForKey: PRODUCT_COUNTRYNME];
        self.productVarietal            = [decoder decodeObjectForKey: PRODUCT_VARIETAL];
        self.TEMPproductStatus          = [decoder decodeObjectForKey: @"TEMPPRODUCTSTATUS"];
        self.onSaleItem                 = [decoder decodeObjectForKey: PRODUCT_ONSALEITEM];
        self.TEMPproductQuanity         = [decoder decodeObjectForKey: @"TEMPproductQuanity"];
        self.orderQuantity              = [decoder decodeIntForKey: @"orderQuantity"];
    }
    return self;
}
@end
