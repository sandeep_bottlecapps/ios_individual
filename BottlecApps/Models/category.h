//
//  category.h
//  liquor
//
//  Created by IMAC5 on 10/28/13.
//
//

#import <Foundation/Foundation.h>

@interface category : NSObject
{
    NSString  *categoryId;
    NSString  *categoryName;
    NSMutableArray *subCategoryArray;
}

@property( nonatomic, retain) NSString * categoryId;
@property( nonatomic, retain)  NSString *categoryName;
@property( nonatomic, retain)  NSMutableArray *subCategoryArray;

@end
