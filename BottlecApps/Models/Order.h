//
//  Order.h
//  liquorApp2
//
//  Created by classic on 10/5/12.
//
//

#import <Foundation/Foundation.h>

@interface Order : ModelBase
{
    
}
@property (nonatomic, retain) NSString *discount;
@property (nonatomic, retain) NSString *orderID;
@property (nonatomic , retain) NSString *orderDate;
@property (nonatomic , retain) NSString *orderNo;
@property (nonatomic , retain) NSString *orderStatus;
@property (nonatomic , retain) NSString *productName;
@property (nonatomic , retain) NSString *productPrice;
@property (nonatomic , retain) NSString *productQnty;
@property (nonatomic , retain) NSString *productTotalCost;
@property (nonatomic , retain) NSString *totalOrders;
@property (nonatomic , retain) NSString *productID;
@property (nonatomic , retain) NSString *orderNote;
@property (nonatomic , retain) NSString *productStatus;
@property (nonatomic , retain) NSString *productSalePrice;
@property (nonatomic,retain)NSString *productTax;
@property (nonatomic,retain)NSString *TranscationID;
@property (nonatomic,retain)NSString *onsaleitem;
@property (nonatomic,retain)NSString *IsPickUp;
@property (nonatomic,retain)NSString *ProductUPC;
@property (nonatomic,retain)NSString *Address;
@property (nonatomic,retain)NSString *Address2;
@property (nonatomic,retain)NSString *City;
@property (nonatomic,retain)NSString *State;
@property (nonatomic,retain)NSString *ZipCode;
@property (nonatomic,retain)NSString *totalBeforeTax;
@property (nonatomic,retain)NSString *OrderedSalePrice;


//OrderedSalePrice
- (id)initWithDictionary:(NSDictionary *)dictionary andDeliveryCharge:(NSString*)deliveryCharge;
@end
