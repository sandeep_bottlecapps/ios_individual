//
//  StoreDetails.m
//  BOTTLECAPPS
//
//  Created by imac9 on 9/2/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "StoreDetails.h"

@implementation StoreDetails
-(id)initWithStoreDetails:(NSMutableArray *)StoreDetails{
    self = [super init];
    DLog(@"StoreDetails---%@",StoreDetails);

    if(self)    {
        self.StoreStateName                                     = [self ProperValue : [[StoreDetails valueForKey : KModelStoreStateName] objectAtIndex:0]];
        self.StoreEmailId                                       = [self ProperValue : [[StoreDetails valueForKey : KModelStoreEmailId] objectAtIndex:0]];
        self.StoreImage                                         = [self ProperValue : [[StoreDetails valueForKey : KModelStoreImage] objectAtIndex:0]];
        self.StoreLargeImage                                    = [self ProperValue : [[StoreDetails valueForKey : KModelStoreLargeImage] objectAtIndex:0]];
        self.StoreLatitude                                      = [self ProperValue : [[StoreDetails valueForKey : KModelStoreLatitude] objectAtIndex:0]];
        self.StoreLongitude                                     = [self ProperValue : [[StoreDetails valueForKey : KModelStoreLongitude] objectAtIndex:0]];
        self.StoreName                                          = [self ProperValue : [[StoreDetails valueForKey : KModelStoreName] objectAtIndex:0]];
        self.StoreUserAddress1                                  = [self ProperValue : [[StoreDetails valueForKey: KModelStoreUserAddress1] objectAtIndex:0]];
        self.StoreUserCity                                      = [self ProperValue : [[StoreDetails valueForKey: KModelStoreUserCity] objectAtIndex:0]];
        self.StoreUserContact                                   = [self ProperValue : [[StoreDetails valueForKey: KModelStoreUserContact] objectAtIndex:0]];
        self.StoreUserZipCode                                   = [self ProperValue : [[StoreDetails valueForKey: KModelStoreUserZipCode] objectAtIndex:0]];

    }
    return self;
    
}

@end

