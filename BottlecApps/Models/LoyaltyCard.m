//
//  LoyaltyCard.m
//  BOTTLECAPPS
//
//  Created by imac9 on 9/3/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "LoyaltyCard.h"

@implementation LoyaltyCard
-(id)initWithLoyaltyCardInfo:(NSDictionary *)LoyaltyCardResult{
    self = [super init];
    if(self)    {
        self.ImageStatus                                     = [self ProperValue : [LoyaltyCardResult valueForKey : kapiResponseImageStatus]];
        self.UserLoyalityCardImage                                   = [self ProperValue : [LoyaltyCardResult valueForKey : kapiResponseUserLoyalityCardImage]];
        self.UserId                           = [self ProperValue : [LoyaltyCardResult valueForKey : kapiResponseUserId]];
        self.CurrentCreditDollars                                 = [self ProperValue : [LoyaltyCardResult valueForKey : kapiResponseCurrentCreditDollars]];
        self.UserLoyalityCardNo                                = [self ProperValue : [LoyaltyCardResult valueForKey : kapiResponseUserLoyalityCardNo]];
        self.CurrentFSPoints                                    = [self ProperValue : [LoyaltyCardResult valueForKey : kapiResponseCurrentFSPoints]];
        self.AmountCredit                                  = [self ProperValue : [LoyaltyCardResult valueForKey : kapiResponseAmountCredit]];
        self.StoreImage                                  = [self ProperValue : [LoyaltyCardResult valueForKey : kapiResponseStoreImage]];

       
    }
    DLog(@"LoyaltyCardResult---%@",LoyaltyCardResult);
    return self;
}

@end
