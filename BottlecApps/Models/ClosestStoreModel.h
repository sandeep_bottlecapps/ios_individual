//
//  ClosestStoreModel.h
//  LiquorApp
//
//  Created by Anindya on 16/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@class StoreAddress;
@class StoreDetails;
@class StoreOpenCloseTime;

@interface ClosestStoreModel: ModelBase
@property (nonatomic, strong) NSMutableArray        *GetStoreOpenclose;
@property (nonatomic, strong) NSMutableArray        *GetStoredetails;
@property (nonatomic, strong) NSMutableArray        *UserLatituteDetailStore;
-(id)initWithStoreInfo:(NSDictionary *)StoreInfo;
@end



