//
//  OrderDetails.h
//  BOTTLECAPPS
//
//  Created by IMAC5 on 11/19/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ModelBase.h"
#import "OrderItem.h"

@interface OrderDetails : ModelBase

@property (nonatomic, strong) NSString          *CouponDiscountAmount;
@property (nonatomic, strong) NSString          *orderConfirmationNumber;
@property (nonatomic, strong) NSString          *DeliveryCharges;                //This provide wrong value
@property (nonatomic, assign) int               isOrderPickUp;                  // 1 if user pickup the order
@property (nonatomic, strong) NSString          *orderDate;
@property (nonatomic, strong) NSString          *orderDateTime;
@property (nonatomic, strong) NSString          *orderId;
@property (nonatomic, strong) NSString          *orderNumber;
@property (nonatomic, strong) NSString          *orderNote;
@property (nonatomic, strong) NSString          *orderStatus;                    //Pending if not delivered
@property (nonatomic, strong) NSString          *orderStatusId;                  // Not is use
@property (nonatomic, strong) NSString          *paymentStatus;                  // Paid if user paid
@property (nonatomic, strong) NSString          *storeAddress;
@property (nonatomic,strong) NSString           *tax;
@property (nonatomic,strong) NSString           *orderTotal;
@property (nonatomic, strong) NSMutableArray    *orderItemStore;
//Added for cancel Order
@property (nonatomic,strong) NSString           *isCancelable;

-(id)initWithOrderDetails:(NSDictionary *)orderDetails storeAddress: (NSString *) lstoreAddress;
@end