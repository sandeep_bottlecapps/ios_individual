//
//  EventModel.m
//  BOTTLECAPPS
//
//  Created by imac9 on 9/7/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "EventModel.h"
#import "EventList.h"
@implementation EventModel
-(id)initWithEventList:(NSArray *)EventListArr   {
    self = [super init];
    if(self)
    {
        if(EventListArr && EventListArr != NULL)   {
            id obj = [NSArray arrayWithArray:EventListArr];
            for (NSDictionary *eventList in obj) {
                EventList *EventListObj = [[EventList alloc] initWithEventsList:eventList];
                [self.EventListArray addObject:EventListObj];
            }
        }
    }
    return self;
}

-(void) dealloc {
    [self.EventListArray removeAllObjects];
    self.EventListArray         = nil;
}


@end
