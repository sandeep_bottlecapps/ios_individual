//
//  SubCategory.m
//  LiquorApp
//
//  Created by sachin kumaram on 4/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SubCategory.h"

#define CAT_ID @"CatId"
#define SUB_CAT_ID @"SubCatId"
#define SUB_CAT_NAME @"SubCatName"


@implementation SubCategory

@synthesize catId,subCatId,subCatName;


- (id)initWithDictionary:(NSDictionary *)dictionary {
    
    if(self = [super init]) {
        
        id theCatId = [dictionary valueForKey:CAT_ID];
        if(theCatId == [NSNull null] || [theCatId length]==0) theCatId = nil;
        self.catId = theCatId;
        
        id theSubCatId = [dictionary valueForKey:SUB_CAT_ID];
        if(theSubCatId == [NSNull null] || [theSubCatId length]==0) theSubCatId = nil;
        self.subCatId = theSubCatId;
        
        id theSubCatName = [dictionary valueForKey:SUB_CAT_NAME];
        if(theSubCatName == [NSNull null] || [theSubCatName length]==0) theSubCatName = nil;
        self.subCatName = theSubCatName;
        
    }
    return self;
}

-(void)dealloc{
    
//    [catId release];
//    [subCatId release];
//    [subCatName release];
//    [super dealloc];
}

@end
