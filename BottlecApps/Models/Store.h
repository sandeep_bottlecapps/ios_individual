//
//  Store.h
//  BOTTLECAPPS
//
//  Created by imac9 on 9/2/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ModelBase.h"
#import "StoreAddress.h"
#import "StoreOpenCloseTime.h"
#import "StoreDetails.h"
@interface Store : ModelBase
@property (nonatomic, strong) NSString              *StoreId;
@property (nonatomic, strong) NSString              *StoreName;
-(id)initWithStoreDetails:(NSDictionary *)StoreDetails;
@end
