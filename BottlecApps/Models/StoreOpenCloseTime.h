//
//  StoreOpenCloseTime.h
//  BOTTLECAPPS
//
//  Created by imac9 on 9/2/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ModelBase.h"

@interface StoreOpenCloseTime : ModelBase
@property (nonatomic, strong) NSString              *DayID;
@property (nonatomic, strong) NSString              *StoreCloseTime;
@property (nonatomic, strong) NSString              *StoreOpenTime;

-(id)initWithStoreOpenCloseTime:(NSDictionary *)storeOpenCloseTimeDict;

@end
