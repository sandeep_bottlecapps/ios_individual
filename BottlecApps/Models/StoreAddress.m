//
//  StoreAddress.m
//  BOTTLECAPPS
//
//  Created by imac9 on 9/2/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "StoreAddress.h"

@implementation StoreAddress
@synthesize StateName,UserAddress1,UserAddress2,UserCity,UserLatitude,UserLongitude,UserZipCode;

-(id)initWithStoreAddress:(NSMutableArray *)StoreAddress{
    DLog(@"StoreAddress---%@",StoreAddress);
    self = [super init];
    if(self)    {
        self.StateName                                      = [self ProperValue : [StoreAddress valueForKey : KModelStateName]];
        self.UserAddress1                                   = [self ProperValue : [StoreAddress valueForKey : KModelUserAddress1]];
        self.UserAddress2                                   = [self ProperValue : [StoreAddress valueForKey : KModelUserAddress2]];
        self.UserCity                                       = [self ProperValue : [StoreAddress valueForKey : KModelUserCity]];
        self.UserLatitude                                   = [self ProperValue : [StoreAddress valueForKey : KModelUserLatitude]];
        self.UserLongitude                                  = [self ProperValue : [StoreAddress valueForKey : KModelUserLongitude]];
        self.UserZipCode                                    = [self ProperValue : [StoreAddress valueForKey : KModelUserZipCode]];
    }
    return self;
}

@end
