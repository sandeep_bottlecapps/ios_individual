//
//  Order.m
//  liquorApp2
//
//  Created by classic on 10/5/12.
//
//

#import "Order.h"

#define PRODUCT_NAME @"ProductName"
#define PRODUCT_PRICE @"ProductPriceVal"
#define PRODUCT_SALE_PRICE @"ProductSalePrice"
#define PRODUCT_QUANTITY @"Quantity"
#define ORDER_DATE @"OrderDate"
#define ORDER_NO @"OrderNo"
#define ORDER_STATUS @"OrderStatus"
#define ORDER_ID @"OrderId"
#define TOTAL_ORDER_KEY @"totalNoOfRecords"
#define PRODUCT_ID_KEY @"ProductId"
#define PRODUCT_STATUS @"ProductStatus"
#define PRODUCT_TAX @"ProductTax"
#define TRANSCATION_ID @"TransactionID"
#define ON_SALE @"IsOnSaleItems"
#define ORDER_NOTE @"OrderNote"
#define ORDER_IsPickUp @"IsPickUp"
#define ORDER_Address @"Address"
#define ORDER_Address2 @"Address2"
#define ORDER_City @"City"
#define ORDER_State @"State"
#define ORDER_ZipCode @"ZipCode"
#define ORDER_OrderedSalePrice @"OrderedSalePrice"
#define ORDER_IsPickUp @"IsPickUp"
#define ORDER_UPC @"ProductUPC"
#define PRODUCT_Discount @"discount"
//OrderedSalePrice

@implementation Order
@synthesize productName,productPrice,productQnty,productTotalCost,orderDate,orderNo,orderStatus,orderID,totalOrders,productID,productStatus,orderNote,productTax,TranscationID,productSalePrice,onsaleitem,ProductUPC,Address,Address2,City,State,ZipCode,totalBeforeTax,OrderedSalePrice,discount;


-(NSDictionary*)getAppSettingDict:(NSDictionary*)setDict{
    NSDictionary *dict=setDict;
    return dict;

}
- (id)initWithDictionary:(NSDictionary *)dictionary andDeliveryCharge:(NSString*)deliveryCharge
{
    if(self = [super init]) {
        
        id theOrderId = [dictionary valueForKey:ORDER_ID];
        if(theOrderId == [NSNull null] || [theOrderId length]==0) theOrderId = nil;
        self.orderID = [self ProperValue:theOrderId];
        
        id theProductName = [dictionary valueForKey:PRODUCT_NAME];
        if(theProductName == [NSNull null] || [theProductName length]==0) theProductName = nil;
        self.productName = [self ProperValue:theProductName];
        
        id theProductTax = [dictionary valueForKey:PRODUCT_TAX];
        if(theProductTax == [NSNull null] || [theProductTax length]==0) theProductTax = nil;
        NSString *tax = [NSString stringWithFormat:@"%.2f",[theProductTax floatValue]];
        self.productTax = [self ProperValue:tax];
        
        id theProductSalePrice = [dictionary valueForKey:PRODUCT_SALE_PRICE];
        if(theProductSalePrice == [NSNull null] || [theProductSalePrice length]==0) theProductSalePrice = nil;
        self.productSalePrice = [self ProperValue:theProductSalePrice];
        
        id theProductDiscount = [dictionary valueForKey:PRODUCT_Discount];
        if(theProductDiscount == [NSNull null] || [theProductDiscount length]==0) theProductDiscount = nil;
        self.discount = [self ProperValue:theProductSalePrice];
        
        id theOnSaleItem= [dictionary valueForKey:ON_SALE];
        if(theOnSaleItem == [NSNull null] ) theOnSaleItem = nil;
        self.onsaleitem = [self ProperValue:theOnSaleItem];
        
        id theProductPrice = [dictionary valueForKey:PRODUCT_PRICE];
        if(theProductPrice == [NSNull null] || [theProductPrice length]==0) theProductPrice = nil;
        self.productPrice = [self ProperValue:theProductPrice];
        
        id theProductQntity = [dictionary valueForKey:PRODUCT_QUANTITY];
        if(theProductQntity == [NSNull null] || [theProductQntity length]==0) theProductQntity = nil;
        self.productQnty = [self ProperValue:theProductQntity];
        
        id theOrderDate = [dictionary valueForKey:ORDER_DATE];
        if(theOrderDate == [NSNull null] || [theOrderDate length]==0) theOrderDate = nil;
        self.orderDate = [self ProperValue:theOrderDate];
        
        id theOrderNo = [dictionary valueForKey:ORDER_NO];
        if(theOrderNo == [NSNull null] ) theOrderNo = nil;
        self.orderNo = [self ProperValue:theOrderNo];
        
        id theOrderStatus = [dictionary valueForKey:ORDER_STATUS];
        if(theOrderStatus == [NSNull null] || [theOrderStatus length]==0) theOrderStatus = nil;
        self.orderStatus = [self ProperValue:theOrderStatus];
        
        id theOrderedSalePrice = [dictionary valueForKey:ORDER_OrderedSalePrice];
        if(theOrderedSalePrice == [NSNull null] || [theOrderedSalePrice length]==0) theOrderedSalePrice = nil;
        self.OrderedSalePrice = [self ProperValue:theOrderedSalePrice];
        
        float price = ([self.OrderedSalePrice floatValue] * [self.productQnty intValue]*[self.productTax intValue])+[deliveryCharge floatValue];
        NSString *totalPrice = [NSString stringWithFormat:@"%.2f",price];
        self.productTotalCost = [self ProperValue:totalPrice];
        
        
        
        float priceBeforetax = ([self.OrderedSalePrice floatValue] * [self.productQnty intValue])+[deliveryCharge floatValue];
        NSString *lpricebeforeTax = [NSString stringWithFormat:@"%.2f",priceBeforetax];
        self.totalBeforeTax = [self ProperValue:lpricebeforeTax];
        
        id thetotalOrders = [dictionary valueForKey:TOTAL_ORDER_KEY];
        if(thetotalOrders == [NSNull null] || [thetotalOrders length]==0) thetotalOrders = nil;
        self.totalOrders = [self ProperValue:thetotalOrders];
        
        id theproductId = [dictionary valueForKey:PRODUCT_ID_KEY];
        if(theproductId == [NSNull null] || [theproductId length]==0) theproductId = nil;
        self.productID = [self ProperValue:theproductId];
        
        id theproductStatus = [dictionary valueForKey:PRODUCT_STATUS];
        if(theproductStatus == [NSNull null] || [theproductStatus length]==0) theproductStatus = nil;
        self.productStatus = [self ProperValue:theproductStatus];
        
        id theorderNoteStatus = [dictionary valueForKey:ORDER_NOTE];
        if(theorderNoteStatus == [NSNull null] || [theorderNoteStatus length]==0) theorderNoteStatus = nil;
        self.orderNote = [self ProperValue:theorderNoteStatus];
        
        
        id theorderISPickUp = [dictionary valueForKey:ORDER_IsPickUp];
        if(theorderISPickUp == [NSNull null] || [theorderISPickUp length]==0) theorderISPickUp = nil;
        self.IsPickUp = [self ProperValue:theorderISPickUp];
        
        id theProductUPC = [dictionary valueForKey:ORDER_UPC];
        if(theProductUPC == [NSNull null] || [theProductUPC length]==0) theProductUPC = nil;
        self.ProductUPC = [self ProperValue:theProductUPC];
        
        id theAddress = [dictionary valueForKey:ORDER_Address];
        if(theAddress == [NSNull null] || [theAddress length]==0) theAddress = nil;
        self.Address = [self ProperValue:theAddress];
        
        id theAddress2 = [dictionary valueForKey:ORDER_Address2];
        if(theAddress2 == [NSNull null] || [theAddress2 length]==0) theAddress2 = nil;
        self.Address2 = [self ProperValue:theAddress2];
        
        id theCity = [dictionary valueForKey:ORDER_City];
        if(theCity == [NSNull null] || [theCity length]==0) theCity = nil;
        self.City = [self ProperValue:theProductUPC];
        
        id theState = [dictionary valueForKey:ORDER_State];
        if(theState == [NSNull null] || [theState length]==0) theState = nil;
        self.State = [self ProperValue:theState];
        
        
        //OrderedSalePrice
    }
    
    return self;
}


@end
