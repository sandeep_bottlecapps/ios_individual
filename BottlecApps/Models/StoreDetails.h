//
//  StoreDetails.h
//  BOTTLECAPPS
//
//  Created by imac9 on 9/2/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ModelBase.h"

@interface StoreDetails : ModelBase
@property (nonatomic, strong) NSString              *StoreStateName;
@property (nonatomic, strong) NSString              *StoreEmailId;
@property (nonatomic, strong) NSString              *StoreImage;
@property (nonatomic, strong) NSString              *StoreLargeImage;
@property (nonatomic, strong) NSString              *StoreLatitude;
@property (nonatomic, strong) NSString              *StoreLongitude;
@property (nonatomic, strong) NSString              *StoreName;
@property (nonatomic, strong) NSString              *StoreUserAddress1;
@property (nonatomic, strong) NSString              *StoreUserApartment;
@property (nonatomic, strong) NSString              *StoreUserCity;
@property (nonatomic, strong) NSString              *StoreUserContact;
@property (nonatomic, strong) NSString              *StoreUserZipCode;
-(id)initWithStoreDetails:(NSMutableArray *)StoreDetails;

@end
