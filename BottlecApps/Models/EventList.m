//
//  EventList.m
//  BOTTLECAPPS
//
//  Created by imac9 on 9/7/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "EventList.h"

@implementation EventList
@synthesize getEventID;
-(id)initWithEventsList:(NSDictionary *)EventsList{
    self = [super init];
    if(self)    {
        DLog(@"EventsList---%@",EventsList);
        self.EventId                     = [self ProperValue : [EventsList valueForKey : kModelProductID]];
        self.EventImage                  = [self ProperValue : [EventsList valueForKey : kModelProductImage]];
        self.EventName = [self ProperValue : [EventsList valueForKey : kModelProductName]];
        self.EventStartDt = [self ProperValue : [EventsList valueForKey : kModelProductPrice]];
        
        
        
    }
    return self;
    
}
-(void)dealloc{
    
    self.EventId=nil;
    self.EventImage=nil;
    self.EventName=nil;
    self.EventStartDt=nil;
    
}

@end
