//
//  ViewMyOrderModel.h
//  BOTTLECAPPS
//
//  Created by imac9 on 9/8/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ModelBase.h"

@interface ViewMyOrderModel : ModelBase
@property (nonatomic, strong) NSMutableArray        *orderListArray;
@property (nonatomic, strong) NSString        *TotalNumberOfRecords;

-(id)initWithViewMyOrderProductsList:(NSArray *)ViewMyOrderProductsListArr;
-(NSString*)getTotalNumberOfRecords:(NSString *)totalNoOfRecords;

@end
