//
//  ViewMyOrderOrderListModel.m
//  BOTTLECAPPS
//
//  Created by imac9 on 9/8/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ViewMyOrderProductListModel.h"

@implementation ViewMyOrderProductListModel
-(id)initWithOrderList:(NSDictionary *)OrderList{
    self = [super init];
    if(self)    {
        DLog(@"OrderList---%@",OrderList);
        self.OrderId                     = [self ProperValue : [[OrderList valueForKey : kModelProductID] objectAtIndex:0]];
        self.OrderDate                  = [self ProperValue : [[OrderList valueForKey : kModelProductImage] objectAtIndex:0]];
        self.OrderNo = [self ProperValue : [[OrderList valueForKey : kModelProductName] objectAtIndex:0]];
        self.OrderStatus = [self ProperValue : [[OrderList valueForKey : kModelProductPrice] objectAtIndex:0]];
        self.OrderStatusId = [self ProperValue : [[OrderList valueForKey : kModelProductSalePrice] objectAtIndex:0]];
          }
    return self;
    
}
-(void)dealloc{
    
    self.OrderId=nil;
    self.OrderDate=nil;
    self.OrderNo=nil;
    self.OrderStatus=nil;
    self.OrderStatusId=nil;
    
    
}

@end
