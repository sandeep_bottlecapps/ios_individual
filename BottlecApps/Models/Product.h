//
//  Product.h
//  LiquorApp
//
//  Created by sachin kumaram on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : ModelBase{
    
}


@property (nonatomic, copy) NSString *discount;
@property (nonatomic, copy) NSString *productId;
@property (nonatomic, copy) NSString *productPriceId;
@property (nonatomic, copy) NSString *productName;
@property (nonatomic, copy) NSString *productSize;
@property (nonatomic, copy) NSString *productImageURL;
@property (nonatomic, retain) UIImage *productImage;
@property (nonatomic, copy)NSString *productDesc;
@property (nonatomic,copy) NSString *productCatName;
@property (nonatomic,copy) NSString *productUpc;
@property(nonatomic, copy)NSString *productRating;
@property(nonatomic, copy)NSString *productFavStatus;
@property(nonatomic, copy)NSString *productAddListStatus;
@property (nonatomic, copy) NSNumber *totalNoOfProducts;
@property (nonatomic,copy) NSString *productPrice;
@property (nonatomic, copy) NSString *productStatus;
@property (nonatomic, copy) NSString *productSalesPrice;
@property (nonatomic ,copy) NSString *numberOfVote;
@property (nonatomic ,copy) NSString *ProductTax;
@property (nonatomic ,copy) NSString *ProductSku;


@property (nonatomic ,copy) NSString *productSubCatg;
@property (nonatomic ,copy) NSString *productFlavor;
@property (nonatomic ,copy) NSString *productCountryNme;
@property (nonatomic ,copy) NSString *productVarietal;
@property (nonatomic, copy) NSString *TEMPproductStatus;
@property (nonatomic, copy) NSString *onSaleItem;
@property (nonatomic, copy) NSString *TEMPproductQuanity;
@property (nonatomic, assign) int orderQuantity;

- (id)initWithDictionary:(NSDictionary *)dictionary;

@end


