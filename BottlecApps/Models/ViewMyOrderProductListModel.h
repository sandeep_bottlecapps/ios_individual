//
//  ViewMyOrderProductListModel.h
//  BOTTLECAPPS
//
//  Created by imac9 on 9/8/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ModelBase.h"

@interface ViewMyOrderProductListModel : ModelBase
@property (nonatomic, strong) NSString              *OrderId;
@property (nonatomic, strong) NSString              *OrderDate;
@property (nonatomic, strong) NSString              *OrderNo;
@property (nonatomic, strong) NSString              *OrderStatus;
@property (nonatomic, strong) NSString              *OrderStatusId;

-(id)initWithOrderList:(NSDictionary *)OrderList;

@end
