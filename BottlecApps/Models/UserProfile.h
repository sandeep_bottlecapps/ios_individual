//
//  UserProfile.h
//  BOTTLECAPPS
//
//  Created by imac9 on 9/3/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ModelBase.h"

@interface UserProfile : ModelBase
@property (retain, nonatomic)NSString * Address;
@property (retain, nonatomic)NSString * City;
@property (retain, nonatomic)NSString * ContactNo ;
@property (retain, nonatomic)NSString * CountryName;
@property (retain, nonatomic)NSString * DateOfBirth;
@property (retain, nonatomic)NSString * EmailID ;
@property (retain, nonatomic)NSString * FirstName ;
@property (retain, nonatomic)NSString * Gender;
@property (retain, nonatomic)NSString * LastName ;
@property (retain, nonatomic)NSString * Address2 ;
@property (retain, nonatomic)NSString * RegionName;
@property (retain, nonatomic)NSString * State;
@property (retain, nonatomic)NSString * UserImage;
@property (retain, nonatomic)NSString * UserLoyalityCardNo;
@property (retain, nonatomic)NSString * ZipCode;
@property (retain, nonatomic)NSString * userID ;
@property (retain, nonatomic)NSString * MemberSince ;
@property (retain, nonatomic)NSString * IsProfileUpdated ;
-(id)initWithUserProfileInfo:(NSDictionary *)UserAccountResult;
@end
