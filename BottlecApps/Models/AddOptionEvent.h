//
//  AddOptionEvent.h
//  liquor
//
//  Created by IMAC5 on 10/29/13.
//
//

#import <Foundation/Foundation.h>

@interface AddOptionEvent : NSObject{
    NSString *eventId;
    NSString *eventName;
    NSString *isSelected;
}
@property( nonatomic, retain) NSString* eventId;
@property( nonatomic, retain)  NSString *eventName;
@property( nonatomic, retain)  NSString *isSelected;

@end
