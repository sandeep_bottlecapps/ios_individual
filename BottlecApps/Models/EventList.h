//
//  EventList.h
//  BOTTLECAPPS
//
//  Created by imac9 on 9/7/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ModelBase.h"

@interface EventList : ModelBase
@property (nonatomic, strong) NSString              *EventId;
@property (nonatomic, strong) NSString              *EventImage;
@property (nonatomic, strong) NSString              *EventName;
@property (nonatomic, strong) NSString              *EventStartDt;
@property (nonatomic, strong) NSString              *getEventID;


-(id)initWithEventsList:(NSDictionary *)EventsList;


@end
