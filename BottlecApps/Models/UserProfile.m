//
//  UserProfile.m
//  BOTTLECAPPS
//
//  Created by imac9 on 9/3/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "UserProfile.h"

@implementation UserProfile
-(id)initWithUserProfileInfo:(NSDictionary *)UserAccountResult{
    self = [super init];
    if(self)    {
         self.Address2                    = [self ProperValue : [UserAccountResult valueForKey : kapiResponseAddress2]];
        self.Address                     = [self ProperValue : [UserAccountResult valueForKey : kapiResponseAddress]];
        self.City                        = [self ProperValue : [UserAccountResult valueForKey : kapiResponseCity]];
        self.ContactNo                   = [self ProperValue : [UserAccountResult valueForKey : kapiResponseContactNo]];
        self.CountryName                 = [self ProperValue : [UserAccountResult valueForKey : kapiResponseCountryName]];
        self.DateOfBirth                 = [self ProperValue : [UserAccountResult valueForKey : kapiResponseDateOfBirth]];
        self.EmailID                     = [self ProperValue : [UserAccountResult valueForKey : kapiResponseEmailID]];
        self.FirstName                   = [self ProperValue : [UserAccountResult valueForKey : kapiResponseFirstName]];
        self.Gender                      = [self ProperValue : [UserAccountResult valueForKey : kapiResponseGender]];
        self.LastName                    = [self ProperValue : [UserAccountResult valueForKey : kapiResponseLastName]];
        self.MemberSince                 = [self ProperValue : [UserAccountResult valueForKey : kapiResponseMemberSince]];
        self.RegionName                  = [self ProperValue : [UserAccountResult valueForKey : kapiResponseRegionName]];
        self.State                       = [self ProperValue : [UserAccountResult valueForKey : kapiResponseState]];
        self.UserImage                   = [self ProperValue : [UserAccountResult valueForKey : kapiResponseUserImage]];
        self.UserLoyalityCardNo          = [self ProperValue : [UserAccountResult valueForKey : kapiResponseUserLoyalityCardNo]];
        self.ZipCode                     = [self ProperValue : [UserAccountResult valueForKey : kapiResponseZipCode]];
        self.userID                      = [self ProperValue : [UserAccountResult valueForKey : kapiResponseUserId]];
        self.IsProfileUpdated            = [self ProperValue : [UserAccountResult valueForKey : kapiResponseIsProfileUpdated]];

        DLog(@"UserAccountResult---%@",UserAccountResult);
    }
    return self;
    
}

@end
