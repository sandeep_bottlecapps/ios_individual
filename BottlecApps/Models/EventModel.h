//
//  EventModel.h
//  BOTTLECAPPS
//
//  Created by imac9 on 9/7/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ModelBase.h"

@interface EventModel : ModelBase
@property (nonatomic, strong) NSMutableArray        *EventListArray;
-(id)initWithEventList:(NSArray *)EventListArr;


@end
