//
//  LoyaltyCard.h
//  BOTTLECAPPS
//
//  Created by imac9 on 9/3/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ModelBase.h"

@interface LoyaltyCard : ModelBase
@property (nonatomic, strong) NSString      *ImageStatus;

@property (nonatomic, strong) NSString      *UserLoyalityCardImage;

@property (nonatomic, strong) NSString      *UserId;

@property (nonatomic, strong) NSString      *CurrentCreditDollars;

@property (nonatomic, strong) NSString      *UserLoyalityCardNo;

@property (nonatomic, strong) NSString      *CurrentFSPoints;
@property (nonatomic, strong) NSString      *StoreImage;

@property (nonatomic, strong) NSString      *AmountCredit;
-(id)initWithLoyaltyCardInfo:(NSDictionary *)LoyaltyCardResult;
@end
