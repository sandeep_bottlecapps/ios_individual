//
//  OrderItem.h
//  BOTTLECAPPS
//
//  Created by IMAC5 on 11/19/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ModelBase.h"

@interface OrderItem : ModelBase

@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *address2;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *zipCode;

@property (nonatomic, strong) NSString *productId;
@property (nonatomic, strong) NSString *productName;
@property (nonatomic, strong) NSString *productPrice;
@property (nonatomic, strong) NSString *productSalePrice;
@property (nonatomic, strong) NSString *productUPCNumber;
@property (nonatomic, assign) int       productIsOnSale;            // 1 == sale , 0 = not in sale
@property (nonatomic, assign) int       productOutOfStockStatus;    // 1 == product available 0 == out of stock
@property (nonatomic, strong) NSString *productTaxRatio;            // "0.0800"

@property (nonatomic, strong) NSString *orderSalePrice;             // only Use this
@property (nonatomic, strong) NSString *orderStatus;                //Pending; whether product is deliverd or not/
@property (nonatomic, assign) int       orderProductQuantity;


-(id)initWithOrderItemDetails:(NSDictionary *)orderItemDetails;
@end