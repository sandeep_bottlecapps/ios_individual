//
//  SubCategory.h
//  LiquorApp
//
//  Created by sachin kumaram on 4/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface SubCategory : NSObject{
    
}

@property (nonatomic, copy) NSString *catId;
@property (nonatomic, copy) NSString *subCatId;
@property (nonatomic, copy) NSString *subCatName;


- (id)initWithDictionary:(NSDictionary *)dictionary;

@end