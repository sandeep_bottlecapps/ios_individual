//
//  OrderDetails.m
//  BOTTLECAPPS
//
//  Created by IMAC5 on 11/19/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "OrderDetails.h"

@implementation OrderDetails
@synthesize orderConfirmationNumber;
@synthesize DeliveryCharges;
@synthesize isOrderPickUp;
@synthesize orderDate;
@synthesize orderDateTime;
@synthesize orderId;
@synthesize orderNumber;
@synthesize orderNote;
@synthesize orderStatus;
@synthesize orderStatusId;
@synthesize paymentStatus;
@synthesize storeAddress;
@synthesize orderItemStore;
@synthesize CouponDiscountAmount;
@synthesize isCancelable;


-(id)initWithOrderDetails:(NSDictionary *)orderDetails storeAddress: (NSString *) lstoreAddress{
    self = [super init];
    if(self)    {
        @try {
            if(orderDetails)  {
                self.orderConfirmationNumber    = [self ProperValue : [orderDetails valueForKey : @"ConfirmationNo"]];
                self.DeliveryCharges            = [self ProperValue : [orderDetails valueForKey : @"DeliveryCharges"]];
                self.CouponDiscountAmount       = [self ProperValue : [orderDetails valueForKey : @"CouponDiscountAmount"]];
                self.isOrderPickUp              = [[orderDetails valueForKey : @"IsPickUp"] intValue];
                self.orderDate                  = [self ProperValue : [orderDetails valueForKey : @"OrderDate"]];
                self.orderDateTime              = [self ProperValue : [orderDetails valueForKey : @"OrderDateTime"]];
                self.orderId                    = [self ProperValue : [orderDetails valueForKey : @"OrderId"]];
                self.orderNumber                = [self ProperValue : [orderDetails valueForKey : @"OrderNo"]];
                self.orderNote                  = [self ProperValue : [orderDetails valueForKey : @"OrderNote"]];
                self.orderStatus                = [self ProperValue : [orderDetails valueForKey : @"OrderStatus"]];
                self.orderStatusId              = [self ProperValue : [orderDetails valueForKey : @"OrderStatusId"]];
                self.paymentStatus              = [self ProperValue : [orderDetails valueForKey : @"PaymentStatus"]];
                self.storeAddress               = lstoreAddress;
                self.tax                        = [self ProperValue : [orderDetails valueForKey : @"Tax"]];
                self.orderTotal                 = [self ProperValue:[orderDetails valueForKey:@"OrderTotal"]];
                self.isCancelable               = [self ProperValue:[orderDetails valueForKey:@"isCancellable"]];
                self.orderItemStore             = [NSMutableArray array];
                
                NSArray *orderItemArray = [orderDetails objectForKey:@"_clsOrderDetails"];
                for(NSDictionary* orderedItem in orderItemArray)    {
                    OrderItem *item = [[OrderItem alloc] initWithOrderItemDetails: orderedItem];
                    [self.orderItemStore addObject:item];
                }
                orderItemArray = nil;
            }
        }
        @catch (NSException *exception) {
            NSLog(@"%@", exception.userInfo);
        }
        @finally {
            return self;
        }
    }
    return self;
}

-(void)dealloc  {
    [self.orderItemStore removeAllObjects];
}

@end

//ConfirmationNo = 2245150956;
//DeliveryCharges = 0;
//IsPickUp = 0;
//
//OrderDate = "11.19.2015";
//OrderDateTime = "11/19/2015 4:57:56 AM";
//OrderId = 2560;
//OrderNo = W02462;
//OrderNote = "This a Botlecapps demo app. Email Us at info@bottlecapps.com for more information on how we can help you get your own bottlecapps retail liquor store app.";
//OrderStatus = Pending;
//OrderStatusId = 1;
//PaymentStatus = Paid;