//
//  StoreOpenCloseTime.m
//  BOTTLECAPPS
//
//  Created by imac9 on 9/2/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "StoreOpenCloseTime.h"

@implementation StoreOpenCloseTime
-(id)initWithStoreOpenCloseTime:(NSDictionary *)storeOpenCloseTimeDict{
    self = [super init];
    if(self)    {
        self.DayID                                                = [self ProperValue : [storeOpenCloseTimeDict valueForKey : KModelStoreDayID]];
        self.StoreCloseTime                                       = [self ProperValue : [storeOpenCloseTimeDict valueForKey : KModelStoreCloseTime]];
        self.StoreOpenTime                                        = [self ProperValue : [storeOpenCloseTimeDict valueForKey : KModelStoreOpenTime]];
    }
    DLog(@"%@ %@ %@",_DayID,_StoreOpenTime,_StoreCloseTime);
     return self;
}
@end
