//
//  Constants.h
//  LMS_iPhone
//
//  Created by Sudhakar Goverdhanam on 20/03/12.
//  Copyright (c) 2012 Prime Technology Group, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject


//Strings for OrderConfirmation
extern NSString *itemsCost_oc;
extern NSString *discount_oc;
extern NSString *deliveryCharge_oc;
extern NSString *totalBeforeTax_oc;
extern NSString *tax_oc;
extern NSString *orderTotal_oc;

//External String for Login
extern NSString *loyaltyEnable;

//External String for browse option
extern NSString *notloggedIn;

//External String for checkout for browse option
extern NSString *pickUpClicked;
extern NSString *deliverClicked;

//External String for check after favorites added to particular user id when user logs in.
extern NSString *favoritesAlreadyAdded;

//External String for favorites separated by commas and send to favorite api when user logas in
extern NSString *combinedString;

//External string for after signup, go to deliver and then come back.
extern NSString *signupBack;

//External string for click on cancel button
//extern NSString *cancelClicked;
extern NSString *cancelCombinedString;

//External string for privacy,cancel and Refund policy
extern NSString *privacyPolicyText;
@end
