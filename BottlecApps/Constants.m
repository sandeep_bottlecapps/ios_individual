//
//  Constants.m
//  LMS_iPhone
//
//  Created by Sudhakar Goverdhanam on 20/03/12.
//  Copyright (c) 2012 Prime Technology Group, Inc. All rights reserved.
//

#import "Constants.h"

@implementation Constants

 NSString *itemsCost_oc = @"";
 NSString *discount_oc =@"";
 NSString *deliveryCharge_oc = @"";
 NSString *totalBeforeTax_oc = @"";
 NSString *tax_oc = @"";
 NSString *orderTotal_oc = @"";

 NSString *loyaltyEnable = @"";

 NSString *notloggedIn = @"";
 NSString *pickUpClicked = @"";
 NSString *deliverClicked = @"";

NSString *favoritesAlreadyAdded=@"";

NSString *combinedString=@"";
NSString *signupBack=@"";

//NSString *cancelClicked = @"";
NSString *cancelCombinedString = @"";

NSString *privacyPolicyText = @"";
@end
