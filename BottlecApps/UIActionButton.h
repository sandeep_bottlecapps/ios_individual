//
//  UICallButton.h
//  @lacarte
//
//  Created by IMAC5 on 12/26/14.
//  Copyright (c) 2014 classic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"
@interface UIActionButton : UIButton  {
    int rowNumber;
}

@property (nonatomic, assign) int rowNumber;
@property (nonatomic, assign) int sectionNumber;
@property (nonatomic, retain) Product *productObj;

-(void)setRowNumber:(int)rowNumber;
-(void)setSectionNumber:(int)sectionNumber;
-(void)setProductObj:(Product *)lproductObj;
- (id)initWithFrame:(CGRect)frame type:(UIButtonType)buttonType;
@end