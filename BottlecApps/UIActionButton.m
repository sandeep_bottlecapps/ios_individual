//
//  UICallButton.m
//  @lacarte
//
//  Created by IMAC5 on 12/26/14.
//  Copyright (c) 2014 classic. All rights reserved.
//

#import "UIActionButton.h"

@implementation UIActionButton
@synthesize rowNumber,sectionNumber,productObj;

- (id)initWithFrame:(CGRect)frame type:(UIButtonType)buttonType   {
    self = [super initWithFrame: frame];
    return self;
}

-(void)setRowNumber:(int)lrowNumber  {
    rowNumber = lrowNumber;
}
-(void)setSectionNumber:(int)lsectionNumber{
    sectionNumber=lsectionNumber;
}
-(void)setProductObj:(Product *)lproductObj{
    productObj=lproductObj;
}

- (void)dealloc{
}
@end