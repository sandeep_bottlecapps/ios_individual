//
//  OrderDetailsTableViewCell.m
//  BOTTLECAPPS
//
//  Created by IMAC5 on 11/19/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "OrderDetailsTableViewCell.h"
#import "QALabel.h"
#import "OrderItem.h"
#import "AppDelegate.h"
#import "Constants.h"

#define kLeftPadding 15
#define kCellLabelwidth self.contentView.bounds.size.width - (kLeftPadding * 2)

#define kTitleLabelWidth 121
#define kTitleLabelHeight 20


static NSString * const kFontRegular                =   @"OpenSans";
static NSString * const kFontSemiBold               =   @"OpenSans-Semibold";
static NSString * const kFontSemiBoldItalic         =   @"OpenSans-SemiBoldIt";
static NSString * const kFontBold                   =   @"OpenSans-Bold";
static NSString * const kFontBoldExtra              =   @"OpenSans-Extrabold";

@interface OrderDetailsTableViewCell ()
@property (nonatomic, strong) QALabel *     discountLbl;
@property (nonatomic, strong) QALabel *     _totalBasicCostLbl;
@property (nonatomic, strong) QALabel *     _deliveryChargeLbl;
@property (nonatomic, strong) QALabel *     _totalBeforeTaxLbl;
@property (nonatomic, strong) QALabel *     _taxLbl;
@property (nonatomic, strong) QALabel *     _totalOrderCostLbl;

@property (nonatomic, strong) QALabel *     _orderAddressLbl;
@property (nonatomic, strong) QALabel *     _orderStatusLbl;
@property (nonatomic, strong) QALabel *     _orderConformationNumberLbl;
@property (nonatomic, strong) UITextView *  _noteTextView;

@property (nonatomic, strong) UIView *      _orderItemsBaseView;
@property (nonatomic, strong) UIView *      _orderSummarybaseView;
@end

@implementation OrderDetailsTableViewCell
@synthesize _totalBasicCostLbl;
@synthesize _deliveryChargeLbl;
@synthesize _totalBeforeTaxLbl;
@synthesize _taxLbl;
@synthesize _totalOrderCostLbl;
@synthesize _orderAddressLbl;
@synthesize _orderStatusLbl;
@synthesize _orderConformationNumberLbl;
@synthesize _noteTextView;
@synthesize _orderItemsBaseView;
@synthesize _orderSummarybaseView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor colorWithRed:0.9294 green:0.9294 blue:0.9294 alpha:1.0];
        
        /**************************Order Item List Header Titles*****************************/
        int yPosition = 5;
        [self.contentView addSubview: [self createHeaderLabelWithText: @"Order Details:" yPosition: yPosition]];
        
        yPosition += kTitleLabelHeight;
        [self.contentView addSubview: [self createTitleLabelWithText:@"Product Name"
                                                                    frame:CGRectMake(kLeftPadding, yPosition, kTitleLabelWidth, kTitleLabelHeight)]];
        
        QALabel *quantityTitleLbl = [self createTitleLabelWithText:@"Quantity"
                                                             frame:CGRectMake(kCellLabelwidth - (35 + 5)*2, yPosition, 60, kTitleLabelHeight)];
        [quantityTitleLbl setTextAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview: quantityTitleLbl];
        
        QALabel * priceTitleLbl = [self createTitleLabelWithText:@"Price"
                                                            frame:CGRectMake(kCellLabelwidth- 25, yPosition, 40, kTitleLabelHeight)];
        [priceTitleLbl setTextAlignment:NSTextAlignmentRight];
        [self.contentView addSubview: priceTitleLbl];
        /**************************Order Item List Header Titles*****************************/
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void) setupOrderSummarybaseView:(OrderDetails *) orderDetails totalItemCost:(double) totalItemCost
                         totalTax:(double) totalTax deliveryAddress:(NSString *) deliveryAddress
            navigationBtnSelector:(SEL) navigationBtnSelector  cancelButtonSelector:(SEL)cancelButtonSelector target:(id) target{
    int yPosition = 5;
  
    [_orderSummarybaseView addSubview: [self createHeaderLabelWithText: @"Order Summary:" yPosition: yPosition]];
    
    yPosition += kTitleLabelHeight;
    if([orderDetails.CouponDiscountAmount isEqualToString:@"0.0000" ]){
    [_orderSummarybaseView addSubview: [self createTitleLabelWithText:@"Item Cost:"
                                                                frame:CGRectMake(kLeftPadding, yPosition, kTitleLabelWidth, kTitleLabelHeight)]];
    _totalBasicCostLbl = [self createTextLabelWithText:[NSString stringWithFormat:@"$%.2f",totalItemCost]
                                                 frame:CGRectMake(kLeftPadding*2 + kTitleLabelWidth, yPosition, kTitleLabelWidth+ 30, kTitleLabelHeight)];
    [_orderSummarybaseView addSubview: _totalBasicCostLbl];
    
    double totalBeforeTax = totalItemCost;
    
    if(orderDetails.isOrderPickUp==0) {
        yPosition += kTitleLabelHeight;
        [_orderSummarybaseView addSubview: [self createTitleLabelWithText:@"Delivery Charge:"
                                                                    frame:CGRectMake(kLeftPadding, yPosition, kTitleLabelWidth, kTitleLabelHeight)]];
        _deliveryChargeLbl = [self createTextLabelWithText:[NSString stringWithFormat:@"$%.2f",[orderDetails.DeliveryCharges doubleValue]]
                                                     frame:CGRectMake(kLeftPadding*2 + kTitleLabelWidth, yPosition, kTitleLabelWidth+ 30, kTitleLabelHeight)];
        [_orderSummarybaseView addSubview: _deliveryChargeLbl];
        
        yPosition += kTitleLabelHeight;
        [_orderSummarybaseView addSubview: [self createTitleLabelWithText:@"Total before tax"
                                                                    frame:CGRectMake(kLeftPadding, yPosition, kTitleLabelWidth, kTitleLabelHeight)]];
        totalBeforeTax += [orderDetails.DeliveryCharges doubleValue];
        _totalBeforeTaxLbl = [self createTextLabelWithText:[NSString stringWithFormat:@"$%.2f", totalBeforeTax]
                                                     frame:CGRectMake(kLeftPadding*2 + kTitleLabelWidth, yPosition, kTitleLabelWidth+ 30, kTitleLabelHeight)];
        [_orderSummarybaseView addSubview: _totalBeforeTaxLbl];
    
    }
    
    yPosition += kTitleLabelHeight;
    [_orderSummarybaseView addSubview: [self createTitleLabelWithText:@"Tax:"
                                                                frame:CGRectMake(kLeftPadding, yPosition, kTitleLabelWidth, kTitleLabelHeight)]];
    _taxLbl = [self createTextLabelWithText:[NSString stringWithFormat:@"$%.2f",[orderDetails.tax floatValue]]
                                      frame:CGRectMake(kLeftPadding*2 + kTitleLabelWidth, yPosition, kTitleLabelWidth+ 30, kTitleLabelHeight)];
    [_orderSummarybaseView addSubview: _taxLbl];
    
    yPosition += kTitleLabelHeight;
    
        NSLog(@"%f-----%f",totalTax,totalBeforeTax);
    [_orderSummarybaseView addSubview: [self createTitleLabelWithText:@"Order Total:"
                                                                frame:CGRectMake(kLeftPadding, yPosition, kTitleLabelWidth, kTitleLabelHeight)]];
    _totalOrderCostLbl = [self createTextLabelWithText:[NSString stringWithFormat:@"$%.2f",([orderDetails.tax floatValue] + totalBeforeTax)]
                                                 frame:CGRectMake(kLeftPadding*2 + kTitleLabelWidth, yPosition, kTitleLabelWidth+ 30, kTitleLabelHeight)];
    [_orderSummarybaseView addSubview: _totalOrderCostLbl];

    }
    else{
        [_orderSummarybaseView addSubview: [self createTitleLabelWithText:@"Item Cost:"
                                                                    frame:CGRectMake(kLeftPadding, yPosition, kTitleLabelWidth, kTitleLabelHeight)]];
        _totalBasicCostLbl = [self createTextLabelWithText:[NSString stringWithFormat:@"$%.2f",totalItemCost]
                                                     frame:CGRectMake(kLeftPadding*2 + kTitleLabelWidth, yPosition, kTitleLabelWidth+ 30, kTitleLabelHeight)];
        [_orderSummarybaseView addSubview: _totalBasicCostLbl];
        
        double totalBeforeTax = totalItemCost-[orderDetails.CouponDiscountAmount floatValue];
        
        [_orderSummarybaseView addSubview: [self createTitleLabelWithText:@"Discount:"
                                                                    frame:CGRectMake(kLeftPadding, _totalBasicCostLbl.frame.size.height+ yPosition, kTitleLabelWidth, kTitleLabelHeight)]];
        NSLog(@"The discount is %f",[orderDetails.CouponDiscountAmount floatValue]);
        _discountLbl = [self createTextLabelWithText:[NSString stringWithFormat:@"$%.2f",[orderDetails.CouponDiscountAmount floatValue]]
                                                     frame:CGRectMake(kLeftPadding*2 + kTitleLabelWidth, _totalBasicCostLbl.frame.size.height+ yPosition, kTitleLabelWidth+ 30, kTitleLabelHeight)];
        [_orderSummarybaseView addSubview: _discountLbl];
        
    
        
        if(orderDetails.isOrderPickUp==0) {
            yPosition += kTitleLabelHeight;
            [_orderSummarybaseView addSubview: [self createTitleLabelWithText:@"Delivery Charge:"
                                                                        frame:CGRectMake(kLeftPadding, _discountLbl.frame.size.height+ yPosition, kTitleLabelWidth, kTitleLabelHeight)]];
            _deliveryChargeLbl = [self createTextLabelWithText:[NSString stringWithFormat:@"$%.2f",[orderDetails.DeliveryCharges doubleValue]]
                                                         frame:CGRectMake(kLeftPadding*2 + kTitleLabelWidth, _discountLbl.frame.size.height+ yPosition, kTitleLabelWidth+ 30, kTitleLabelHeight)];
            [_orderSummarybaseView addSubview: _deliveryChargeLbl];
            
            yPosition += kTitleLabelHeight;
            [_orderSummarybaseView addSubview: [self createTitleLabelWithText:@"Total before tax"
                                                                        frame:CGRectMake(kLeftPadding, _deliveryChargeLbl.frame.size.height+ yPosition, kTitleLabelWidth, kTitleLabelHeight)]];
            totalBeforeTax += [orderDetails.DeliveryCharges doubleValue];
            _totalBeforeTaxLbl = [self createTextLabelWithText:[NSString stringWithFormat:@"$%.2f", totalBeforeTax]
                                                         frame:CGRectMake(kLeftPadding*2 + kTitleLabelWidth, _deliveryChargeLbl.frame.size.height+ yPosition, kTitleLabelWidth+ 30, kTitleLabelHeight)];
            [_orderSummarybaseView addSubview: _totalBeforeTaxLbl];
        
        
        yPosition += kTitleLabelHeight;
        [_orderSummarybaseView addSubview: [self createTitleLabelWithText:@"Tax:"
                                                                    frame:CGRectMake(kLeftPadding, _totalBeforeTaxLbl.frame.size.height+ yPosition, kTitleLabelWidth, kTitleLabelHeight)]];
            _taxLbl = [self createTextLabelWithText:[NSString stringWithFormat:@"$%.2f",[orderDetails.tax  floatValue]]
                                          frame:CGRectMake(kLeftPadding*2 + kTitleLabelWidth, _totalBeforeTaxLbl.frame.size.height+ yPosition, kTitleLabelWidth+ 30, kTitleLabelHeight)];
        [_orderSummarybaseView addSubview: _taxLbl];
        
        yPosition += kTitleLabelHeight;
    }
        else{
            yPosition += kTitleLabelHeight;
            [_orderSummarybaseView addSubview: [self createTitleLabelWithText:@"Tax:"
                                                                        frame:CGRectMake(kLeftPadding, _discountLbl.frame.size.height+ yPosition, kTitleLabelWidth, kTitleLabelHeight)]];
            _taxLbl = [self createTextLabelWithText:[NSString stringWithFormat:@"$%.2f",[orderDetails.tax floatValue]]
                                              frame:CGRectMake(kLeftPadding*2 + kTitleLabelWidth, _discountLbl.frame.size.height+ yPosition, kTitleLabelWidth+ 30, kTitleLabelHeight)];
            [_orderSummarybaseView addSubview: _taxLbl];
            
            yPosition += kTitleLabelHeight;
        }
        [_orderSummarybaseView addSubview: [self createTitleLabelWithText:@"Order Total:"
                                                                    frame:CGRectMake(kLeftPadding, _taxLbl.frame.size.height+ yPosition, kTitleLabelWidth, kTitleLabelHeight)]];
        _totalOrderCostLbl = [self createTextLabelWithText:[NSString stringWithFormat:@"$%.2f",([orderDetails.tax floatValue] + totalBeforeTax)]
                                                     frame:CGRectMake(kLeftPadding*2 + kTitleLabelWidth, _taxLbl.frame.size.height+ yPosition, kTitleLabelWidth+ 30, kTitleLabelHeight)];
        [_orderSummarybaseView addSubview: _totalOrderCostLbl];
    
    }
    // Address fields start
    
    NSString *addressString = kEmptyAddress;
    yPosition += kTitleLabelHeight*2.0;
    if (!orderDetails.isOrderPickUp)    {
        [_orderSummarybaseView addSubview: [self createHeaderLabelWithText: @"Delivery Address:" yPosition: yPosition]];
        addressString = deliveryAddress;
    }
    else    {
        [_orderSummarybaseView addSubview: [self createHeaderLabelWithText: @"Pick Up Address:" yPosition: yPosition]];
        addressString = orderDetails.storeAddress;
        
        UIImage *LocationImage=[UIImage imageNamed:@"locationicon"];
        UIButton *getLocationButton = [UIButton buttonWithType:UIButtonTypeCustom];
        getLocationButton.frame = CGRectMake(kCellLabelwidth-LocationImage.size.width + 10, yPosition + 35, LocationImage.size.width, LocationImage.size.height);
        [getLocationButton setTitle:kEmptyString forState:UIControlStateNormal];
        getLocationButton.backgroundColor=[UIColor clearColor];
        [getLocationButton addTarget:target action:navigationBtnSelector forControlEvents:UIControlEventTouchUpInside];
        [getLocationButton setImage:LocationImage forState:UIControlStateNormal];
        [_orderSummarybaseView addSubview:getLocationButton];
    }
   
    
    yPosition += kTitleLabelHeight -2;
    _orderAddressLbl = [self createTextLabelWithText:addressString
                                                    frame:CGRectMake(kLeftPadding, yPosition, kCellLabelwidth , (kTitleLabelHeight*4)-5)];
    _orderAddressLbl.numberOfLines = 4;
    _orderAddressLbl.textAlignment = NSTextAlignmentLeft;
    _orderAddressLbl.verticalAlignment = UIControlContentVerticalAlignmentTop;
    [_orderSummarybaseView addSubview: _orderAddressLbl];
    
    // Payment Details start
    yPosition += kTitleLabelHeight*2.5 + kTitleLabelHeight*2;
    [_orderSummarybaseView addSubview: [self createHeaderLabelWithText: @"Payment Details:" yPosition: yPosition]];
    
    yPosition += kTitleLabelHeight;
    [_orderSummarybaseView addSubview: [self createTitleLabelWithText:@"Status:"
                                                                frame:CGRectMake(kLeftPadding, yPosition, kTitleLabelWidth, kTitleLabelHeight)]];
    _orderStatusLbl = [self createTextLabelWithText:orderDetails.paymentStatus
                                              frame:CGRectMake(kLeftPadding*2 + kTitleLabelWidth, yPosition, kTitleLabelWidth+ 30, kTitleLabelHeight)];
    [_orderSummarybaseView addSubview: _orderStatusLbl];
    
    if (orderDetails.orderConfirmationNumber && (![orderDetails.orderConfirmationNumber isEqualToString:kEmptyString]))    {
        yPosition += kTitleLabelHeight;
        [_orderSummarybaseView addSubview: [self createTitleLabelWithText:@"Confirmation No.:"
                                                                    frame:CGRectMake(kLeftPadding, yPosition, kTitleLabelWidth, kTitleLabelHeight)]];
        _orderConformationNumberLbl = [self createTextLabelWithText:orderDetails.orderConfirmationNumber
                                                              frame:CGRectMake(kLeftPadding*2 + kTitleLabelWidth, yPosition, kTitleLabelWidth+ 30, kTitleLabelHeight)];
        [_orderSummarybaseView addSubview: _orderConformationNumberLbl];
    }
    
    // Note Start:
     yPosition += kTitleLabelHeight * 1.2;
    [_orderSummarybaseView addSubview: [self createTitleLabelWithText:@"Note:"
                                                                frame:CGRectMake(kLeftPadding, yPosition, 40, kTitleLabelHeight)]];
    
    _noteTextView = [[UITextView alloc] initWithFrame:CGRectMake(15 + 34, yPosition + 4, kCellLabelwidth - 34, 90)];
    [_noteTextView setTextColor:[UIColor colorWithRed:0.4392 green:0.4392 blue:0.4431 alpha:1.0]];
    [_noteTextView setFont:[UIFont fontWithName:kFontRegular size:12.4]];
    [_noteTextView setTextAlignment:NSTextAlignmentLeft];
    [_noteTextView setEditable:NO];
    [_noteTextView setScrollEnabled:YES];
    [_noteTextView setUserInteractionEnabled:YES];
    [_noteTextView setDataDetectorTypes:UIDataDetectorTypeAll];
    _noteTextView.textAlignment = NSTextAlignmentJustified;
    _noteTextView.textContainer.lineBreakMode = NSLineBreakByCharWrapping;
    _noteTextView.textContainerInset = UIEdgeInsetsMake(-3, 0, 0, 0);
    _noteTextView.backgroundColor = [UIColor clearColor];
    _noteTextView.text = orderDetails.orderNote;
    [_orderSummarybaseView addSubview: _noteTextView];

    //Added cancel button in order detail for orders which are in pending status.
    
    NSLog(@"%@",orderDetails.isCancelable);
    if([orderDetails.isCancelable isEqualToString:@"1"])
    {
        cancelOrderId = orderDetails.orderId;
        cancelOrderStatusId = orderDetails.orderStatusId;
        cancelCombinedString = [NSString stringWithFormat:@"%@##%@",cancelOrderId,cancelOrderStatusId];
        
        
        
        UIImage *orderCancelImage = [UIImage imageNamed:@"order-cancel-icon"];
        UIButton *orderCancelButton = [UIButton buttonWithType:UIButtonTypeCustom]; //
        orderCancelButton.frame = CGRectMake(kCellLabelwidth-((orderCancelImage.size.width)*4.5), yPosition+95.0,123,40);
        [orderCancelButton setTitle:@"   Cancel order " forState:UIControlStateNormal];
        orderCancelButton.titleLabel.font =  [UIFont fontWithName:kFontBold size:12.4];
        orderCancelButton.backgroundColor=[UIColor colorWithRed:241.0/255.0 green:27.0/255.0 blue:46.0/255.0 alpha:1.0]; //241 27 46
        orderCancelButton.layer.cornerRadius = 8.0;
       // [orderCancelButton addTarget:self action:@selector(orderCancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
         [orderCancelButton addTarget:target action:cancelButtonSelector forControlEvents:UIControlEventTouchUpInside];
        [orderCancelButton setImage:orderCancelImage forState:UIControlStateNormal];
        [_orderSummarybaseView addSubview:orderCancelButton];

    }
    else 
    {}
    
    _orderSummarybaseView.frame =  CGRectMake(0, _orderSummarybaseView.frame.origin.y, self.contentView.bounds.size.width, yPosition + 70 + 100);
}
/********** Send service when cancel button clicked **************************************************/
-(void)orderCancelButtonClicked:(UIButton *)button
{
    NSLog(@"OrderId = %@",cancelOrderId);
}



/**************************** create label**************************/
-(QALabel *) createHeaderLabelWithText:(NSString *) text yPosition:(float) yPosition   {
    return [self createLblWithRect: CGRectMake(kLeftPadding, yPosition, kCellLabelwidth , kTitleLabelHeight)
                              font: [UIFont fontWithName:kFontSemiBold size:14]
                              text: text
                     textAlignment: NSTextAlignmentLeft
                         textColor: [UIColor colorWithRed:0.9098 green:0.1765 blue:0.2392 alpha:1.0]];
}

-(QALabel *) createTitleLabelWithText:(NSString *) text frame:(CGRect) frame   {
    return [self createLblWithRect: frame
                              font: [UIFont fontWithName:kFontSemiBold size: (14 - 1.3)]
                              text: text
                     textAlignment: NSTextAlignmentLeft
                         textColor: [UIColor colorWithRed:0.3765 green:0.3922 blue:0.4039 alpha:1.0]];
}

-(QALabel *) createTextLabelWithText:(NSString *) text frame:(CGRect) frame   {
    return [self createLblWithRect: frame
                              font:[UIFont fontWithName:kFontRegular size:12.4]
                              text: text
                     textAlignment: NSTextAlignmentRight
                         textColor: [UIColor colorWithRed:0.4392 green:0.4392 blue:0.4431 alpha:1.0]];
}

-(QALabel*) createLblWithRect:(CGRect) rect font:(UIFont*) font text:(NSString*)text textAlignment:(NSTextAlignment) textAlignment textColor:(UIColor*) textColor{
    QALabel *defaultLbl = [[QALabel alloc] initWithFrame: rect];
    defaultLbl.text = [NSString stringWithFormat:@"%@",text];
    [defaultLbl setFont:font];
    [defaultLbl setTextAlignment:textAlignment];
    defaultLbl.textColor = textColor;
    defaultLbl.backgroundColor = [UIColor clearColor];
    return defaultLbl;
}
/**************************** create label**************************/

-(void) setOrderDetailsWithOrderDetails:(OrderDetails *) orderDetails navigationBtnSelector:(SEL) navigationBtnSelector cancelButtonSelector:(SEL)cancelButtonSelector target:(id)target {
    if (_orderItemsBaseView)    {
        [_orderItemsBaseView removeFromSuperview];
        _orderItemsBaseView = nil;
    }
    
    _orderItemsBaseView = [[UIView alloc] initWithFrame:CGRectMake(0, kTitleLabelHeight*2 + 5, self.contentView.bounds.size.width, 0)];
    _orderItemsBaseView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_orderItemsBaseView];
    
    /*********************Add order Items*****************************/
    double totalItemCost = 0.0;
    double totalTax = 0.0;
    NSMutableString *deliveryAddressString = [NSMutableString string];
    
    __weak NSArray *itemStore = orderDetails.orderItemStore;
    __weak OrderItem *orderItem  = nil;
    int yPosition = 0;
    
    for (int counter = 0; counter < itemStore.count; counter++) {
        orderItem = [itemStore objectAtIndex: counter];
        double itemTotalPrice = [orderItem.orderSalePrice doubleValue] * orderItem.orderProductQuantity;
        [_orderItemsBaseView addSubview: [self createTextLabelWithText: [NSString stringWithFormat:@"$%.2f", itemTotalPrice]
                                                              frame:CGRectMake(kCellLabelwidth- 35, yPosition, 52, kTitleLabelHeight)]];
        
        QALabel *quantityTextLbl = [self createTextLabelWithText:[NSString stringWithFormat:@"%d", orderItem.orderProductQuantity]
                                                           frame:CGRectMake(kCellLabelwidth- 35 - 35 - 10, yPosition, 52, kTitleLabelHeight)];
        [quantityTextLbl setTextAlignment:NSTextAlignmentCenter];
        [_orderItemsBaseView addSubview: quantityTextLbl];
        
        QALabel *orderNameTextLbl = [self createTextLabelWithText:orderItem.productName
                                                            frame:CGRectMake(kLeftPadding, yPosition,
                                                            kCellLabelwidth - (kCellLabelwidth- 35 - (35 + 10)*3 - 23), kTitleLabelHeight)];
        
        [orderNameTextLbl setTextAlignment:NSTextAlignmentLeft];
        [_orderItemsBaseView addSubview: orderNameTextLbl];
        
        /*******************Calculation*************************/
        totalItemCost += (orderItem.orderProductQuantity * [orderItem.orderSalePrice doubleValue]);
        totalTax += (orderItem.orderProductQuantity * [orderItem.orderSalePrice doubleValue] * [orderItem.productTaxRatio doubleValue]);
        
        if(counter == 0)    {
            if (orderItem.firstName && (![orderItem.firstName isEqualToString:kEmptyString]))   {
                [deliveryAddressString appendFormat:@"%@ ", orderItem.firstName];
            }
            if (orderItem.lastName && (![orderItem.lastName isEqualToString:kEmptyString]))   {
                [deliveryAddressString appendFormat:@"%@\n", orderItem.lastName];
            }
            if (orderItem.address && (![orderItem.address isEqualToString:kEmptyString]))   {
                if (orderItem.address2 && (![orderItem.address2 isEqualToString:kEmptyString])){
                NSString* address=[NSString stringWithFormat:@"%@, %@ ",orderItem.address,orderItem.address2];
                [deliveryAddressString appendFormat:@"%@\n", address];
                }
                else{
                    NSString* address=[NSString stringWithFormat:@"%@ %@ ",orderItem.address,orderItem.address2];
                    [deliveryAddressString appendFormat:@"%@\n", address];
                }
            }
//            if (orderItem.address2 && (![orderItem.address2 isEqualToString:kEmptyString]))   {
//                [deliveryAddressString appendFormat:@"%@ \n", orderItem.address2];
//            }
            if (orderItem.city && (![orderItem.city isEqualToString:kEmptyString]))   {
                [deliveryAddressString appendFormat:@"%@, ", orderItem.city];
            }
            if (orderItem.state && (![orderItem.state isEqualToString:kEmptyString]))   {
                [deliveryAddressString appendFormat:@"%@ ", orderItem.state];
            }
            if (orderItem.zipCode && (![orderItem.zipCode isEqualToString:kEmptyString]))   {
                [deliveryAddressString appendFormat:@"%@\n", orderItem.zipCode];
            }
            if (orderItem.phoneNumber && (![orderItem.phoneNumber isEqualToString:kEmptyString]))   {
                NSString* phonenumberstring= [self formatPhoneNumber:orderItem.phoneNumber deleteLastChar:NO];
                NSMutableArray *array = [NSMutableArray array];
                for (int i = 0; i < [phonenumberstring length]; i++) {
                    NSString *ch = [phonenumberstring substringWithRange:NSMakeRange(i, 1)];
                    if(i==0){
                        [array addObject:@"("];
                    }
                    else if(i==3){
                        [array addObject:@")"];
                        [array addObject:@" "];
                    }
                    
                    else if(i==6){
                        [array addObject:@"-"];
                    }
                    
                    [array addObject:ch];
                    
                }
                phonenumberstring = @"";
                for(NSString *strNumber in array){
                    phonenumberstring = [NSString stringWithFormat:@"%@%@",phonenumberstring,strNumber];
                }
                
                orderItem.phoneNumber=phonenumberstring;
                [deliveryAddressString appendFormat:@"Phone: %@", orderItem.phoneNumber];
            }
        }
        /*******************Calculation*************************/
        
        yPosition += kTitleLabelHeight;
        orderItem = nil;
    
    
    }
      
    _orderItemsBaseView.frame = CGRectMake(0, kTitleLabelHeight*2 + 5, self.contentView.bounds.size.width, yPosition);
    /*********************Add order Items*****************************/
    
    /*********************Update order summary view*****************************/
    if(_orderSummarybaseView)   {
        [_orderSummarybaseView removeFromSuperview];
        _orderSummarybaseView = nil;
    }
    
    // Need to update the height of cell in tableview if height of _orderSummarybaseView change.
    _orderSummarybaseView = [[UIView alloc] initWithFrame:CGRectMake(0, (kTitleLabelHeight*2 + 5)*1.5 + yPosition, self.contentView.bounds.size.width, yPosition + 70 + 10)];
    _orderSummarybaseView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_orderSummarybaseView];
    
    [self setupOrderSummarybaseView: orderDetails totalItemCost:totalItemCost
                           totalTax: totalTax
                    deliveryAddress: deliveryAddressString
              navigationBtnSelector: navigationBtnSelector
               cancelButtonSelector: cancelButtonSelector
                             target:target];
    /*********************Update order summary view*****************************/
}
-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar{
    
    if(simpleNumber.length==0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    // check if the number is to long
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"($1) $2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"$1$2$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    return simpleNumber;
}
@end