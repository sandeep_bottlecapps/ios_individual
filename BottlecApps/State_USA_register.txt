{

    "GetStateResult": [
        {
            "CountryID": "6",
            "StateAbbreviation": "AK",
            "StateID": "1",
            "StateName": "ALASKA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "AL",
            "StateID": "2",
            "StateName": "ALABAMA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "AR",
            "StateID": "3",
            "StateName": "ARKANSAS"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "AZ",
            "StateID": "4",
            "StateName": "ARIZONA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "CA",
            "StateID": "5",
            "StateName": "CALIFORNIA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "CO",
            "StateID": "6",
            "StateName": "COLORADO"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "CT",
            "StateID": "7",
            "StateName": "CONNECTICUT"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "DE",
            "StateID": "8",
            "StateName": "DELAWARE"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "FL",
            "StateID": "9",
            "StateName": "FLORIDA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "GA",
            "StateID": "10",
            "StateName": "GEORGIA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "HI",
            "StateID": "11",
            "StateName": "HAWAII"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "IA",
            "StateID": "12",
            "StateName": "IOWA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "ID",
            "StateID": "13",
            "StateName": "IDAHO"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "IL",
            "StateID": "14",
            "StateName": "ILLINOIS"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "IN",
            "StateID": "15",
            "StateName": "INDIANA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "KS",
            "StateID": "16",
            "StateName": "KANSAS"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "KY",
            "StateID": "17",
            "StateName": "KENTUCKY"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "LA",
            "StateID": "18",
            "StateName": "LOUISIANA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "MA",
            "StateID": "19",
            "StateName": "MASSACHUSETTS"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "MD",
            "StateID": "20",
            "StateName": "MARYLAND"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "ME",
            "StateID": "21",
            "StateName": "MAINE"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "MI",
            "StateID": "22",
            "StateName": "MICHIGAN"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "MN",
            "StateID": "23",
            "StateName": "MINNESOTA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "MO",
            "StateID": "24",
            "StateName": "MISSOURI"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "MS",
            "StateID": "25",
            "StateName": "MISSISSIPPI"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "MT",
            "StateID": "26",
            "StateName": "MONTANA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "NE",
            "StateID": "27",
            "StateName": "NEBRASKA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "NC",
            "StateID": "28",
            "StateName": "NORTH CAROLINA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "ND",
            "StateID": "29",
            "StateName": "NORTH DAKOTA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "NH",
            "StateID": "30",
            "StateName": "NEW HAMPSHIRE"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "NJ",
            "StateID": "31",
            "StateName": "NEW JERSEY"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "NM",
            "StateID": "32",
            "StateName": "NEW MEXICO"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "NV",
            "StateID": "33",
            "StateName": "NEVADA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "NY",
            "StateID": "34",
            "StateName": "NEW YORK"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "OH",
            "StateID": "35",
            "StateName": "OHIO"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "OK",
            "StateID": "36",
            "StateName": "OKLAHOMA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "OR",
            "StateID": "37",
            "StateName": "OREGON"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "PA",
            "StateID": "38",
            "StateName": "PENNSYLVANIA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "RI",
            "StateID": "39",
            "StateName": "RHODE ISLAND"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "SC",
            "StateID": "40",
            "StateName": "SOUTH CAROLINA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "SD",
            "StateID": "41",
            "StateName": "SOUTH DAKOTA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "TN",
            "StateID": "42",
            "StateName": "TENNESSEE"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "TX",
            "StateID": "43",
            "StateName": "TEXAS"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "UT",
            "StateID": "44",
            "StateName": "UTAH"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "VA",
            "StateID": "45",
            "StateName": "VIRGINIA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "VT",
            "StateID": "46",
            "StateName": "VERMONT"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "WA",
            "StateID": "47",
            "StateName": "WASHINGTON"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "WI",
            "StateID": "48",
            "StateName": "WISCONSIN"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "WV",
            "StateID": "49",
            "StateName": "WEST VIRGINIA"
        },
        {
            "CountryID": "6",
            "StateAbbreviation": "WY",
            "StateID": "50",
            "StateName": "WYOMING"
        }
    ]

}