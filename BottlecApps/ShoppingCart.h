//
//  ShoppingCart.h
//  liquorApp2
//
//  Created by classic on 10/1/12.
//
//

#import <Foundation/Foundation.h>
#import "Product.h"

@interface ShoppingCart : NSObject
{
    
}
@property (nonatomic , retain) NSMutableArray *shoppingBag;

+(ShoppingCart*)sharedInstace;
-(NSString*)addProductToMyShoppingCart:(NSArray*)productList;
-(void)removeProductFromMyShoppingCart:(NSArray*)productList;
-(BOOL)shoppingCartContains:(Product*)product;
-(NSArray*)fetchProductFromShoppingCart;

// reset the cart
-(void) resetShopingCart;
// load the shoping cart from archiver
-(void) loadShopingCartfromArchiver;
@end
 