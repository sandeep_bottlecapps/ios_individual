//
//  Spinner.h
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/7/14.
//  Copyright (c) 2014 Kuldeep Tyagi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Spinner : UIView {

}

+(Spinner*)spinner;
-(void) showSpinner;
-(void) hideSpinner;
@end
