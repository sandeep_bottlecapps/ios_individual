//
//  Spinner.m
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/7/14.
//  Copyright (c) 2014 Kuldeep Tyagi. All rights reserved.
//

#import "Spinner.h"
#import "RTSpinKitView.h"

@interface Spinner () {
    RTSpinKitView *_RTSpinner;
}

@end

@implementation Spinner

static Spinner *_spinner;

+ (Spinner*)spinner {
    @synchronized([Spinner class])   {
        if (_spinner == nil) {
            _spinner = [[Spinner alloc] init];
        }
        return _spinner;
    }
    return nil;
}

- (id)init {
    if ((self = [super init])) {
        _RTSpinner = [self createLoadingView];
    }
    return self;
}

-(void)dealloc  {
    _RTSpinner = nil;
}

-(RTSpinKitView *)createLoadingView   {
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    RTSpinKitView *spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleCircle color:[UIColor whiteColor]];
    
    spinner.center = CGPointMake(CGRectGetMidX(screenBounds), CGRectGetMidY(screenBounds));
    spinner.alpha = 0.0;
    [[UIApplication sharedApplication].keyWindow addSubview:spinner];
    
    return spinner;
}

// Mathod to show the spinner
-(void) showSpinner    {
    [_RTSpinner startAnimating];
    [UIView animateWithDuration:0.8 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations: ^{
        self.alpha = 1;
    }completion:nil];
}

//Mathod to hide the spinner
-(void) hideSpinner   {
    [UIView animateWithDuration:0.8 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations: ^{
        self.alpha = 0;
    }completion:nil];
}

@end
