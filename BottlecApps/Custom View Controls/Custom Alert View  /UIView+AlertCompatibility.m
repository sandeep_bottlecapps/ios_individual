//
//  UIView+AlertCompatibility.m
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/4/14.
//  Copyright (c) 2014 Kuldeep Tyagi. All rights reserved.
//

#import "UIView+AlertCompatibility.h"

@implementation UIView (AlertCompatibility)

+( void )showSimpleAlertWithTitle:( NSString * )title
                          message:( NSString * )message
                cancelButtonTitle:( NSString * )cancelButtonTitle
{
    if(SYSTEM_VERSION_LESS_THAN(@"8.0") )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: title
                                                        message: message
                                                       delegate: nil
                                              cancelButtonTitle: cancelButtonTitle
                                              otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        // nil titles break alert interface on iOS 8.0, so we'll be using empty strings
        UIAlertController *alert = [UIAlertController alertControllerWithTitle: title == nil ? @"": title
                                                                       message: message
                                                                preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle: cancelButtonTitle
                                                                style: UIAlertActionStyleDefault
                                                              handler: nil];
        
        [alert addAction: defaultAction];
        
        UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        [[rootViewController presentedViewController] presentViewController: alert animated: YES completion: nil];
        rootViewController = nil;
    }
}

+( void )showMessageWithTitle:( NSString * )title
                      message:( NSString * )message showInterval:(double)interval {
    if(SYSTEM_VERSION_LESS_THAN(@"8.0") )   {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: title
                                                        message: message
                                                       delegate: nil
                                              cancelButtonTitle: nil
                                              otherButtonTitles: nil];
        [alert show];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert dismissWithClickedButtonIndex:0 animated:YES];
        });
    }
    else    {
        // nil titles break alert interface on iOS 8.0, so we'll be using empty strings
        UIAlertController *alert = [UIAlertController alertControllerWithTitle: title == nil ? @"": title
                                                                       message: message
                                                                preferredStyle: UIAlertControllerStyleAlert];
        
        UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        [[rootViewController presentedViewController] presentViewController: alert animated: YES completion: nil];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [[rootViewController presentedViewController] dismissViewControllerAnimated:YES completion:nil];
        });
    }
}

+( void )showMessageWithTitle:( NSString * )title
                      message:( NSString * )message showInterval:(double)interval viewController:(UIViewController*)controller{
    if(SYSTEM_VERSION_LESS_THAN(@"8.0") )   {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: title
                                                        message: message
                                                       delegate: nil
                                              cancelButtonTitle: nil
                                              otherButtonTitles: nil];
        [alert show];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert dismissWithClickedButtonIndex:0 animated:YES];
        });
    }
    else    {
        // nil titles break alert interface on iOS 8.0, so we'll be using empty strings
        UIAlertController *alert = [UIAlertController alertControllerWithTitle: title == nil ? @"": title
                                                                       message: message
                                                                preferredStyle: UIAlertControllerStyleAlert];
        if(controller)
            [controller presentViewController: alert animated: YES completion: nil];
        else
            [[UIApplication sharedApplication].keyWindow.rootViewController.presentedViewController presentViewController:alert animated:YES completion:nil];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            if(controller)
                [controller dismissViewControllerAnimated:YES completion:nil];
            else
                [[UIApplication sharedApplication].keyWindow.rootViewController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
        });
    }
}
@end