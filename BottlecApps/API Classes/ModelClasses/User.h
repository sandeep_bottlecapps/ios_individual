//
//  User.h
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/6/14.
//  Copyright (c) 2014 Kuldeep Tyagi. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "ModelBase.h"
#import "Store.h"
#import "UserLocationManager.h"
@interface User : ModelBase

@property (nonatomic, strong) NSString      *userId;

@property (nonatomic, strong) NSString      *deviceToken; // APNS Device Token
@property (nonatomic, strong) CLLocation    *currentLocation;
@property (nonatomic, strong) NSString      *LastCategoryModificationDate;
@property (nonatomic, strong) NSString      *IsDeliveryEnabled;

@property (nonatomic, strong) NSMutableArray*StoreArray;
@property (nonatomic, strong) NSString      *WeeklyFlyer;

-(id)initWithUserInfo:(NSDictionary *)userInfo  deviceToken:(NSString *) lDeviceToken StoreListArray:(NSMutableArray*)StoreListArray;
//-(NSString*)getIsExists:(NSString*)IsExists;
-(CLLocation*) getCurrentLocation;
- (BOOL) isLocationServiceEnabled;

@end