//
//  User.m
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/6/14.
//  Copyright (c) 2014 Monika kumari. All rights reserved.
//

#import "User.h"
#import "Store.h"

@implementation User
@synthesize userId;
@synthesize deviceToken;
@synthesize currentLocation;
@synthesize LastCategoryModificationDate,WeeklyFlyer,StoreArray;
-(id)initWithUserInfo:(NSDictionary *)userInfo  deviceToken:(NSString *) lDeviceToken StoreListArray:(NSMutableArray*)StoreListArray    {
    DLog(@"userInfo%@ lDeviceToken%@ StoreListArray-%@",userInfo,lDeviceToken,StoreListArray)
    self = [super init];
    if(self)    {
        self.userId                                     = [self ProperValue : [userInfo valueForKey : kAuthenticationId]];
        self.IsDeliveryEnabled                          = [self ProperValue : [userInfo valueForKey : kIsDeliveryEnabled]];
        self.deviceToken                                = [self ProperValue : lDeviceToken];
        self.currentLocation                            = [self getCurrentLocation]; // update the user's location
        self.LastCategoryModificationDate               = [self ProperValue : [userInfo valueForKey: kUserLastCategoryModificationDate]];
        self.WeeklyFlyer                                = [self ProperValue : [userInfo valueForKey: kWeeklyFlyer]];
        for(NSDictionary* storeDict in StoreListArray)    {
            Store *store = [[Store alloc] initWithStoreDetails:storeDict];
            [StoreArray addObject:store];
        }

        [self startUpdateLocation];
    }
    return self;
}

-(void) dealloc {
    self.userId             = nil;
    self.LastCategoryModificationDate   =nil;
    self.deviceToken                    = nil;
    self.currentLocation                = nil;
    self.WeeklyFlyer                    =nil;
    self.IsDeliveryEnabled              =nil;
}

#pragma mark- Location related methods
-(CLLocation*) getCurrentLocation   {
    return [[UserLocationManager defaultLocationManager] getUserCurrentLocation];
}

- (BOOL) isLocationServiceEnabled   {
    return [[UserLocationManager defaultLocationManager] isLocationServiceEnabled];
}

// callback method to get location update as iPhone's location change.
-(void)startUpdateLocation   {
    [[UserLocationManager defaultLocationManager] updateLocationWithCompletionHandler:^(CLLocation * newLocation) {
        if(self.currentLocation)
            [self setCurrentLocation: newLocation];
    }];
}

#pragma mark- archiver and Unarchiver methods
//Encode properties, other class variables, etc
- (void)encodeWithCoder:(NSCoder *)encoder  {
    [encoder encodeObject: self.userId              forKey: kAuthenticationId];
       [encoder encodeObject: self.deviceToken         forKey:kUserDeviceToken];
    [encoder encodeObject: self.LastCategoryModificationDate         forKey:kUserLastCategoryModificationDate];
    [encoder encodeObject: self.currentLocation     forKey:kUserCurrentLocation];
}

- (id)initWithCoder:(NSCoder *)decoder  {
    self = [super init];
    if(self)    {
        self.userId                 = [decoder decodeObjectForKey: kAuthenticationId];
        self.LastCategoryModificationDate               = [decoder decodeObjectForKey: kUserLastCategoryModificationDate];
        self.WeeklyFlyer            = [decoder decodeObjectForKey: kWeeklyFlyer];
        self.deviceToken            = [decoder decodeObjectForKey: kUserDeviceToken];
        self.currentLocation        = [decoder decodeObjectForKey: kUserCurrentLocation];
        [self startUpdateLocation]; // update the user's location
    }
    return self;
}

@end