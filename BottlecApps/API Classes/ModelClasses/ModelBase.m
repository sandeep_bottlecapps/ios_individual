//
//  ModelBase.m
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/6/14.
//  Copyright (c) 2014 Kuldeep Tyagi. All rights reserved.
//

#import "ModelBase.h"

@implementation ModelBase

-(id)init   {
    self = [super init];
    if(self)    {
        //initialize here
    }
    return self;
}

-(NSString*) ProperValue:(NSString*)value   {
    if(!value || [value isEqual:[NSNull null]] ||
       ([value respondsToSelector:@selector(isEqualToString:)] && [value isEqualToString:kEmptyString]== YES))
        return kEmptyString;
    else
        return value;
}

// convert date format
- (NSString*) convertDate: (NSString*)date currentFormat:(NSString*) currentFormat newFormat : (NSString*) newFormat    {
    NSDateFormatter * dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:currentFormat];
    [dateFormat setLocale:[NSLocale currentLocale]];
    NSDate *dateObject = [dateFormat dateFromString:date];
    [dateFormat setDateFormat:newFormat]; // set new date format
    return [dateFormat stringFromDate:dateObject];
}
@end
