//
//  ModelBase.h
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/6/14.
//  Copyright (c) 2014 Kuldeep Tyagi. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark- temp vaiable need to be remove
#define kTempCurrentDateTime    @"2014-12-11 12:12:14"
#define kTempGMTUnixDateTime    12345678900101010
#define kDefaultEmailAddress    @"Default@HostName.com"
#define kDefaultFacebookId      123456789000000000
#define kdefaultTempAuthToken   @"f2f1996ade8bd92c3ec819d59c081e16"
#define kTempDeviceLongitude    77.20053809734496 // Delhi
#define kTempDeviceLatitude     28.51963440287253 // Delhi,
static NSString * const     kEmptyCardNumber             =     @"Card No: N/A";

static NSString * const     kEmptytextfield             =     @"N/A";
static NSString * const     kEmptyAddress               =     @"No address found.";
static NSString * const     kEmptyApartment             =     @"No Apartment found.";
static NSString * const     kEmptyInstructions          =     @"Please enter notes here.";

static NSString * const     kEmptySalePrice             =     @"0.00";

static NSString * const     kEmptyString                =     @"";
static NSString * const     kIPhoneDeviceType           =     @"I";
#pragma mark-Api parameters

static NSString * const     kParamTxtjson                =     @"txtjson";
static NSString * const     kParamIsSuccessOrder                =     @"IsSuccessOrder";

//IsSuccessOrder
static NSString * const     kParamStoreId                =     @"StoreId";
static NSString * const     kParamDiscountValue                =     @"Discount";
static NSString * const     kParamOrderValue                =     @"OrderValue";
static NSString * const     kParamUserId                 =     @"UserId";
static NSString * const     kParamUserName               =     @"UserName";
static NSString * const     kParamEmailId                =     @"EmailId";
static NSString * const     kPageSize                    =     @"10";

//searchbeer
static NSString * const     kParamPageNumber            =     @"PageNumber";
static NSString * const     kParamPageSize              =     @"PageSize";
static NSString * const     kParamPhone                 =     @"Phone";

static NSString * const     kParamuserid                =     @"userid";
static NSString * const     kParamTypeName              =     @"TypeName";
static NSString * const     kParamSize                  =     @"Size";
static NSString * const     kParamPriceMin              =     @"PriceMin";
static NSString * const     kParamPriceMax              =     @"PriceMax";
static NSString * const     kParamUserImage             =     @"UserImage";
static NSString * const     kParamSearchText             =     @"SearchText";


static NSString * const     kParamsubCatId2             =     @"subCatId2";
static NSString * const     kParamCountry               =     @"Country";
static NSString * const     kParamRegion                =     @"Region";
static NSString * const     kParamRegionId              =     @"RegionId";

static NSString * const     kParamReviewTitle           =     @"ReviewTitle";
static NSString * const     kParamReviewTxt             =     @"ReviewTxt";
static NSString * const     kParamReviewRating          =     @"ReviewRating";
static NSString * const     kParamReviewId              =     @"ReviewId";


static NSString * const     kParamisDeliveryEnabled             =     @"IsDeliveryEnabled";
static NSString * const     kParamDeliveryLimit                 =     @"DeliveryLimit";
static NSString * const     kParamDeliverCharge                 =     @"DeliveryCharges";
static NSString * const     kParamDeliveryTaxRate               =     @"DeliveryTaxRate";
static NSString * const     kParamStoreCurrentTime              =     @"CurrentStoreTime";
static NSString * const     kParamPrivacyPolicyText             =     @"StorePolicy";
//CancelOrder
static NSString * const     kOrderId             =     @"OrderId";
static NSString *const      kCurrentStatusId     =     @"CurrentStatusId";

//edit
static NSString * const     kParamAuthenticationID =     @"authenticationID";


static NSString * const     kParamFirstName  =     @"FirstName";
static NSString * const     kParamCouponCode  =     @"CouponCode";


static NSString * const     kParamLastName  =     @"LastName";

static NSString * const     kParamContactNumber            =     @"ContactNumber";
static NSString * const     kParamDateofBirth              =     @"dateofBirth";
static NSString * const     kParamGender                   =     @"Gender";
static NSString * const     kParamAddress                  =     @"Address";
static NSString * const     kParamApartment                  =   @"Address2";
static NSString * const     kParamCountryId                =     @"CountryId";
static NSString * const     kParamState                    =     @"State";

static NSString * const     kParamStateName                =     @"StateName";
static NSString * const     kParamCity                     =     @"City";
static NSString * const     kParamZipCode                  =     @"ZipCode";
static NSString * const     kParamZip                      =     @"Zip";

static NSString * const     kParamUserLoyalityCardNo       =     @"UserLoyalityCardNo";
static NSString * const     kParamDeviceType               =     @"DeviceType";
static NSString * const     kParamNotification             =     @"Notification";

//addorder
static NSString * const     kParamOrderId                   =     @"OrderId";
static NSString * const     kParamPaymentDetails            =     @"PaymentDetails";
static NSString * const     kParamUserRemarks               =     @"UserRemarks";
static NSString * const     kParamTotalTax                  =     @"TotalTax";
static NSString * const     kParamtxtjson                   =     @"txtjson";
static NSString * const     kParamSuccess                   =     @"Success";
static NSString * const     kParamMessage                   =     @"Message";

static NSString * const     kValidBarcodeid                 =     @"303831778802649";

static NSString * const     kParamProductName               =     @"ProductName";
static NSString * const     kParamMinValue                  =     @"MinValue";
static NSString * const     kParamMaxvalue                  =     @"Maxvalue";
//ProductIds
static NSString * const     kParamProductIds                =     @"ProductIds";

static NSString * const     kParamEventId                           =     @"EventId";
static NSString * const     kParamProductID                         =     @"ProductID";
static NSString * const     kParamQuantity                          =     @"Quantity";
static NSString * const     kParamProductQuantities                 =     @"ProductQuantities";

static NSString * const     kParamTabName                           =     @"TabName";
//addorder
static NSString * const     kParamItems                             =     @"items";
static NSString * const     kParamUniqueId                          =     @"UniqueId";
static NSString * const     kParamDeviceTimeZoneOffset              =     @"DeviceTimeZoneOffset";
static NSString * const     kParamDeviceTimeZoneName                =     @"DeviceTimeZoneName";
static NSString * const     kParamDeviceTimeZoneAbbreviation        =     @"DeviceTimeZoneAbbreviation";
static NSString * const     kParamDeviceTime                        =     @"DeviceTime";


#pragma mark- api response myprofile
static NSString * const kapiResponseAddress2                 =@"Address2";
static NSString * const kapiResponseAddress                 =@"Address";
static NSString * const kapiResponseCity                    =@"City";
static NSString * const kapiResponseContactNo               =@"ContactNo";
static NSString * const kapiResponseCountryName             =@"CountryName";
static NSString * const kapiResponseDateOfBirth             =@"DateOfBirth";
static NSString * const kapiResponseEmailID                 =@"EmailID";
static NSString * const kapiResponseFirstName               =@"FirstName";
static NSString * const kapiResponseGender                  =@"Gender";
static NSString * const kapiResponseLastName                =@"LastName";
static NSString * const kapiResponseMemberSince             =@"MemberSince";
static NSString * const kapiResponseRegionName              =@"RegionName";
static NSString * const kapiResponseState                   =@"State";
static NSString * const kapiResponseUserImage               =@"UserImage";
static NSString * const kapiResponseUserLoyalityCardNo      =@"UserLoyalityCardNo";
static NSString * const kapiResponseZipCode                 =@"ZipCode";
static NSString * const kapiResponseuserID                  =@"userID";
static NSString * const kapiTotalNoOfRecords                =@"totalNoOfRecords";
//@"totalNoOfRecords"

//loyaltycard
static NSString * const kapiResponseImageStatus             =@"ImageStatus";

static NSString * const kapiResponseUserLoyalityCardImage   =@"UserLoyalityCardImage";

static NSString * const kapiResponseUserId                  =@"UserId";
static NSString * const kapiResponseIsProfileUpdated        =@"IsProfileUpdated";

static NSString * const kapiResponseCurrentCreditDollars    =@"CurrentCreditDollars";


static NSString * const kapiResponseCurrentFSPoints         =@"CurrentFSPoints";

static NSString * const kapiResponseAmountCredit            =@"AmountCredit";
static NSString * const kapiResponseStoreImage            =@"StoreImage";



#pragma mark- attribute keys for Login Model Start.
static NSString * const     kUserPassword               =     @"password";
static NSString * const     kUserStoreId                =     @"StoreId";
static NSString * const     kUserDeviceId               =     @"DeviceId";
static NSString * const     kUserDeviceType             =     @"DeviceType";
static NSString * const     kAuthenticationId           =     @"AuthenticationId";
static NSString * const     kCurrentLoggedInUser        =     @"CurrentLoggedInUser";
static NSString * const     kUserName                   =     @"UserName";
static NSString * const     kUserEmailAddress           =     @"userEmailId";
static NSString * const     kUserRegType                =     @"RegType";
static NSString * const     kUserFacebookId             =     @"FacebookId";
static NSString * const     kUserInstagramId            =     @"InstagramId";
static NSString * const     kUserAuthToken              =     @"AuthToken";
static NSString * const     kUserDeviceToken            =     @"DeviceToken";
static NSString * const     kUserCurrentDateTime        =     @"CurrentDateTime";
static NSString * const     kUserGMTUnixDateTime        =     @"GMTUnixDateTime";
static NSString * const     kUserAccessToken            =     @"AccessToken";
static NSString * const     kUserCurrentLocation        =     @"CurrentLocation";
static NSString * const     kIsDeliveryEnabled          =     @"IsDeliveryEnabled";
static NSString * const     kShopingCartArray           =     @"ShopingCartArray";              //To save the shoping cart array in archiver
static NSString * const     kUserLoggedIn               =     @"UserLoggedIn";              //To check the user is still logged in or not.



static NSString * const     kUserLastCategoryModificationDate        =     @"LastCategoryModificationDate";
static NSString * const     kWeeklyFlyer                             =     @"WeeklyFlyer";
static NSString * const     kStoresList                              =     @"StoresList";


static NSString * const     kModelStoreId                            =     @"StoreId";
static NSString * const     kModelStoreName                          =     @"StoreName";
static NSString * const     KModelStateName                          =     @"StateName";
static NSString * const     KModelUserAddress1                       =     @"UserAddress1";
static NSString * const     KModelUserAddress2                       =     @"UserAddress2";
static NSString * const     KModelUserCity                           =     @"UserCity";
static NSString * const     KModelUserLatitude                       =     @"UserLatitude";
static NSString * const     KModelUserLongitude                      =     @"UserLongitude";
static NSString * const     KModelUserZipCode                        =     @"UserZipCode";


static NSString * const     KModelStoreStateName                =     @"StateName";
static NSString * const     KModelStoreEmailId                  =     @"StoreEmailId";
static NSString * const     KModelStoreImage                    =     @"StoreImage";
static NSString * const     KModelStoreLargeImage               =     @"StoreLargeImage";
static NSString * const     KModelStoreLatitude                 =     @"StoreLatitude";
static NSString * const     KModelStoreLongitude                =     @"StoreLongitude";
static NSString * const     KModelStoreName                     =     @"StoreName";
static NSString * const     KModelStoreUserAddress1             =     @"UserAddress1";
static NSString * const     KModelStoreUserCity                 =     @"UserCity";
static NSString * const     KModelStoreUserContact              =     @"UserContact";
static NSString * const     KModelStoreUserZipCode              =     @"UserZipCode";

static NSString * const     KModelStoreDayID                 =      @"DayID";
static NSString * const     KModelStoreCloseTime              =     @"StoreCloseTime";
static NSString * const     KModelStoreOpenTime              =      @"StoreOpenTime";

//searchbeer
static NSString * const     kModelProductID =@"ProductID";

static NSString * const     kModelProductImage=@"ProductImage";
static NSString * const     kModelProductName=@"ProductName";
static NSString * const     kModelProductPrice=@"ProductPrice";
static NSString * const     kModelProductSalePrice=@"ProductSalePrice";
static NSString * const     kModelProductSize=@"ProductSize";
static NSString * const     kModelProductStatus=@"ProductStatus";
static NSString * const     kModelProductTax=@"ProductTax";
static NSString * const     kModelQuantity=@"Quantity";

//
//searchbeer
static NSString * const     kModelIsFavorite =@"IsFavorite";




//ViewMyOrder

static NSString * const     kModelOrderId =@"OrderId";
static NSString * const     kModelOrderDate=@"OrderDate";
static NSString * const     kModelOrderNo=@"OrderNo";
static NSString * const     kModelOrderStatus=@"OrderStatus";
static NSString * const     kModelOrderStatusId=@"OrderStatusId";
//
//signup StoresList
/*static NSString * const     kParamSignupEmailId               =     @"EmailId";
static NSString * const     kParamSignupEmailId               =     @"Password";
static NSString * const     kParamSignupEmailId               =     @"FirstName";
static NSString * const     kParamSignupEmailId               =     @"LastName";
static NSString * const     kParamSignupEmailId               =     @"ContactNumber";
static NSString * const     kParamSignupEmailId               =     @"dateofBirth";
static NSString * const     kParamSignupEmailId               =     @"Gender";
static NSString * const     kParamSignupEmailId               =     @"Address";
static NSString * const     kParamSignupEmailId               =     @"EmailId";
static NSString * const     kParamSignupEmailId               =     @"EmailId";
static NSString * const     kParamSignupEmailId               =     @"EmailId";
static NSString * const     kParamSignupEmailId               =     @"EmailId";
static NSString * const     kParamSignupEmailId               =     @"EmailId";
static NSString * const     kParamSignupEmailId               =     @"EmailId";
static NSString * const     kParamSignupEmailId               =     @"EmailId";
static NSString * const     kParamSignupEmailId               =     @"EmailId";
static NSString * const     kParamSignupEmailId               =     @"EmailId";
static NSString * const     kParamSignupEmailId               =     @"EmailId";*/




#pragma mark- attribute keys for ForgotPassword Model Start.
static NSString * const     kForgotPasswordEmail        =     @"Email";

@interface ModelBase : NSObject {
    
}

-(id)init;
-(NSString*) ProperValue:(NSString*)value;
- (NSString*) convertDate: (NSString*)date currentFormat:(NSString*) currentFormat newFormat : (NSString*) newFormat;
@end