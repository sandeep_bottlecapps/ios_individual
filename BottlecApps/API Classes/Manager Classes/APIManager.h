//
//  APIManager.h
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/6/14.
//  Copyright (c) 2014 Kuldeep Tyagi. All rights reserved.
//

/*********************************Base URL*************************/

/*********************************Base URL*************************/

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "CompletionBlocks.h"
#import "APIRequestHelper.h"

// define api key
#define APIKeyStatus @"Status"
#define APIKeyMessage @"Message"

#define APIResponseFail @"Fail"
#define APIResponseSuccess @"Success"

@interface APIManager : NSObject    {
    
    
}
@property (nonatomic,retain)NSString*baseUrl;
+(APIManager*)apiManager;
-(void)initializeNetworkObject;

-(BOOL)getNetworkStatus; // get network status

-(void) loginWithUserCredentials:(NSDictionary*)userCredential success:(successBlock) success failure:(failureBlock) failure;
//-(void) logoutWithUserCredentials:(NSDictionary*)userCredential success:(apiStatusBlock) successStatusBlock failure:(failureBlock) failure;
-(void) getTermsWithsuccess:(successBlock) success failure:(failureBlock) failure    ;
-(void) SignupWithUserCredentials:(NSDictionary*)userCredential success:(successBlock) success failure:(failureBlock) failure;
-(void) ForgotPasswordWithUserCredentials:(NSDictionary*)userCredential success:(successBlock) success failure:(failureBlock) failure ;
-(void) GetStoreDetailWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure ;
-(void) ViewProfileWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure ;
-(void) GetLoyalityCardImageWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure  ;
-(void) VerifyEmailIdWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure ;
-(void) SearchBeerWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure ;
-(void)getEventsWithEventsList:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure;
-(void)ViewMyOrderWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure  ;
-(void)GetWeeklyAdsWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure  ;
-(void)GetEventDetailsDetailsWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure ;
-(void) EditUserAccount:(NSDictionary*)userCredential success:(successBlock) success failure:(failureBlock) failure;
-(void)ViewFavoriteWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure  ;
-(void)GetProductDetailWithParam:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure   ;
-(void)GetReviewWithParam:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure ;
-(void) SearchLiquorWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure   ;
-(void) SearchWineWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure ;
-(void)AddReviewWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure;
-(void)AddFavoriteWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure ;
-(void)RemoveFavoriteWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure ;
-(void)AddOrderWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure;
-(void) getProductQuantityWithParameter:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure;
-(void)GetSelectedProductCategoryWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure;
-(void)InsertNotificationSubscriptionWithJsonString:(NSString*)jsonString success:(successBlock) success failure:(failureBlock) failure  ;
-(void)GetUserNotificationHistory:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure;
-(void)Uploadimage:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure;
-(void)GetProductDealsWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure;
-(void) SearchproductWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure ;
-(void) GetAuthorizeCredentialsEncrypt:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure;
-(void)addOrderWithJsonString:(NSString*)jsonString success:(successBlock) success failure:(failureBlock) failure;
-(void)GetCouponCodeValidator:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure;
-(void)GetAddressValidator:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure ;
-(void)GetAPPSettings:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure;

//Added for cancel Order
-(void)cancelBookedOrders:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure;

-(void)EditReviewWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure;
@end
