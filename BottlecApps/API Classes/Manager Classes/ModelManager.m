//
//  ModelManager.m
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/6/14.
//  Copyright (c) 2014 Kuldeep Tyagi. All rights reserved.
//

#import "ModelManager.h"
#import "User.h"
#import "ShoppingCart.h"
#import "ClosestStoreModel.h"
#import "Constants.h"

@interface ModelManager ()  {
    
}

@end

@implementation ModelManager
static ModelManager *_modelManager;

+ (ModelManager*)modelManager {
    @synchronized([ModelManager class])   {
        if (_modelManager == nil) {
            _modelManager = [[ModelManager alloc] init];
        }
        return _modelManager;
    }
    return nil;
}

+(ModelManager *)getModelManager    { // spatialized for swift access
    return _modelManager;
}

//mathod to just to intialize the manager class
+(void)initializeModelManager  {
    [ModelManager modelManager];
}

- (id)init {
    if ((self = [super init])) {
        // initialize the singleton object
        self.deviceToken                        = [[NSMutableString alloc] initWithString:kEmptyString];
        self.currentSignedInUser                = [self getCurrentSignedUserInfoFromArchiver];
        
    }
    return self;
}

-(void) dealloc {
    _modelManager                               = nil;
    self.deviceToken                            = nil;
    self.currentSignedInUser                    = nil;
    
}

#pragma mark- local methods
// process the notification
-(NSString *) processNotification:(NSDictionary *)userInfo  {
    return [NSString stringWithFormat:@"%@ Please check the My Bookings.", [[userInfo objectForKey:@"aps"] objectForKey:@"alert"]];;
}
#pragma mark Get current logged in information from archiver.
-(User*)getCurrentSignedUserInfoFromArchiver    {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *decodedObject = [userDefault objectForKey: kCurrentLoggedInUser];
    User* user = [NSKeyedUnarchiver unarchiveObjectWithData: decodedObject];
    decodedObject = nil;
    userDefault = nil;
    return user;
}

#pragma mark - device token getter and setter
-(void)setCurrentDeviceToken:(NSString *)deviceToken    {
    @synchronized(self) {
        [self.deviceToken setString:deviceToken];
    }
}

-(NSString *)getCurrentDeviceToken {
    return self.deviceToken;
}

#pragma mark - archive the kUserLoggedIn variable for future use    

-(BOOL) isUserAlreadyLoggedIn   {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault boolForKey: kUserLoggedIn];
}

-(void) archiveIsUserAlreadyLoggedInVariable:(BOOL) isUserLogin    {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:isUserLogin forKey:kUserLoggedIn];
    [userDefault synchronize];
}

#pragma mark- To get network status
-(BOOL)getNetworkStatus {
    return [[APIManager apiManager] getNetworkStatus];
}
#pragma mark- seter and getter methods for local variables
-(UserProfile*)getUserProfileModel{
    //
    @synchronized (self.userProfileObj)  {
        return self.userProfileObj;
    }
}
// should only take weak reference

-(User*) getuserModel   {
    @synchronized (self.currentSignedInUser)  {
        return self.currentSignedInUser;
    }
}
-(Store*) getStoreModel   {
    @synchronized (self.storeObj)  {
        return self.storeObj;
    }
}

-(ClosestStoreModel*) getClosestStoreModel   {
    @synchronized (self.closestStoreModelObj)  {
        return self.closestStoreModelObj;
    }
}
-(LoyaltyCard*)getLoyaltyCardModel   {
    @synchronized (self.LoyaltyCardObj)  {
        return self.LoyaltyCardObj;
    }
}
-(EventModel*)getEventModel   {
    @synchronized (self.EventModelObj)  {
        return self.EventModelObj;
    }
}
-(EventList*)getEventListModel   {
    @synchronized (self.EventListObj)  {
        return self.EventListObj;
    }
}
-(ViewMyOrderModel*)getViewMyOrderModel   {
    @synchronized (self.ViewMyOrderModelObj)  {
        return self.ViewMyOrderModelObj;
    }
}
-(ViewMyOrderProductListModel*)getViewMyOrderProductListModel   {
    @synchronized (self.ViewMyOrderProductListModelObj)  {
        return self.ViewMyOrderProductListModelObj;
    }
}
-(void)resetCurrentSignedUser   {
    @synchronized (self.currentSignedInUser)  {
        self.currentSignedInUser = nil;
    }
}


#pragma mark encode the user information and savwe it to archiver.
-(void)setCurrentSignedUserInfoToArchiver    {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:self.currentSignedInUser];
    [userDefault setObject:encodedObject forKey:[NSString stringWithFormat:kCurrentLoggedInUser]];
    encodedObject = nil;
    userDefault = nil;
}


#pragma mark- Model manager Mathods Implementation
#pragma mark login Model manager Implementation
-(void) loginWithUserCredentials:(NSDictionary*) userCredential  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] loginWithUserCredentials:userCredential success:^(APIResponseStatusCode statusCode, id userInfo) {
        [self setCurrentSignedInUser:[[User alloc] initWithUserInfo:userInfo  deviceToken:[self getCurrentDeviceToken] StoreListArray:[userInfo valueForKey:kStoresList]]];
        //Need to check for current signedinUser is same as last loggedin user, if both are same than we need to jsut update the cart. if not then we need to reset the cart
        User*lastLoggedInUser = [self getCurrentSignedUserInfoFromArchiver];
              if(lastLoggedInUser)    {
            if([lastLoggedInUser.userId isEqualToString: self.currentSignedInUser.userId])  {
                // get cart from archive and show it
                [[ShoppingCart sharedInstace] loadShopingCartfromArchiver];
            }
            else    {
                [[ShoppingCart sharedInstace] resetShopingCart];
                //User is not same
                //reset the cart
            }
        }
        else    {
            if([notloggedIn intValue]==1)
              [[ShoppingCart sharedInstace] loadShopingCartfromArchiver];
            else
            //reset the cart due first time login.
            [[ShoppingCart sharedInstace] resetShopingCart];
        }
        
        [self setCurrentSignedUserInfoToArchiver];
        [self archiveIsUserAlreadyLoggedInVariable:YES];  //TO archive the variable for user loggedin
        
        if(self.currentSignedInUser)
            successStatus(TRUE);
        else
            successStatus(FALSE);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
-(void)ForgotPasswordWithUserCredentials:(NSDictionary*) userCredential  successStatus:(successStatusBlockWithUserMessage) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] ForgotPasswordWithUserCredentials:userCredential success:^(APIResponseStatusCode statusCode, id userInfo) {
     
        successStatus(TRUE, [userInfo valueForKey:@"Message"]);
        
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
-(void) SignupWithUserCredentials:(NSDictionary*) userCredential  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] SignupWithUserCredentials:userCredential success:^(APIResponseStatusCode statusCode, id userInfo) {
        [self setCurrentSignedInUser:[[User alloc] initWithUserInfo:userInfo deviceToken:[self getCurrentDeviceToken] StoreListArray:[userInfo valueForKey:kStoresList]]];
        [[ShoppingCart sharedInstace] resetShopingCart]; // user login firt time
        [self setCurrentSignedUserInfoToArchiver];
        
        if(self.currentSignedInUser)
            successStatus(TRUE);
        else
            successStatus(FALSE);
        
        
        
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
-(void) EditUserAccount:(NSDictionary*) userCredential  successStatus:(successStatusBlockWithUserMessage) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] EditUserAccount:userCredential success:^(APIResponseStatusCode statusCode, id userInfo) {
        DLog(@"userInfo%@",userInfo);
        successStatus(TRUE, [userInfo valueForKey:@"Authentication"]);
        
        
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}

-(void) GetTermsAndConditionWithsuccessStatus:(successStatusBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager]getTermsWithsuccess:^(APIResponseStatusCode statusCode,NSDictionary *TermsDictionary)
     {
         DLog(@"statusCode--%d TermsDictionary----%@",statusCode,TermsDictionary);
     }
                                        failure:^(APIResponseStatusCode statusCode,NSString *errorMessage){
                                            DLog(@"statusCode--%d TermsDictionary----%@",statusCode,errorMessage);
                                            
                                            
                                        }];
    
    
}
//-(void)ViewProfileWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
//    [[APIManager apiManager] ViewProfileWithParam:param success:^(APIResponseStatusCode statusCode, id userInfo) {
//        successStatus(statusCode, userInfo);
//    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
//        failure(statusCode, errorMessage);
//    }];
//}

-(void)ViewProfileWithParam:(NSDictionary*) param  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure{
    [[APIManager apiManager] ViewProfileWithParam:param success:^(APIResponseStatusCode statusCode, id userProfileInfo) {
        DLog(@"GetUserAccountResult===%@",[userProfileInfo valueForKey:@"GetUserAccountResult"]);
        self.userProfileObj = [[UserProfile alloc]initWithUserProfileInfo:[userProfileInfo valueForKey:@"GetUserAccountResult"]];
        successStatus(TRUE);
        
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
    
}
-(void)UserAccotntDetailsWithParam:(NSDictionary*) param  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] loginWithUserCredentials:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        [self setCurrentSignedUserInfoToArchiver];
        
        if(self.currentSignedInUser)
            successStatus(TRUE);
        else
            successStatus(FALSE);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}

-(void)GetUserAccountDetailsWithParam:(NSDictionary*) param  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] loginWithUserCredentials:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        ;
        [self setCurrentSignedUserInfoToArchiver];
        
        if(self.currentSignedInUser)
            successStatus(TRUE);
        else
            successStatus(FALSE);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
// GetEventDetailsDetailsWithParam

-(void)GetEventDetailsDetailsWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] GetEventDetailsDetailsWithParam:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}



-(void)GetTermsAndConditionWithsuccessStatus:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager]getTermsWithsuccess:^(APIResponseStatusCode statusCode,NSDictionary *TermsDictionary){
        DLog(@"statusCode--%d TermsDictionary----%@",statusCode,TermsDictionary);
        successStatus(statusCode, TermsDictionary);

    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
// getEventsWithEventsList
-(void)getEventsWithEventsList:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] getEventsWithEventsList:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}

-(void) getProductQuantityWithParameter:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure  {
    [[APIManager apiManager] getProductQuantityWithParameter:param success:^(APIResponseStatusCode statusCode, id response) {
        success(statusCode, response);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}

-(void)addOrderWithJsonstring:(NSString*) jsonstring  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] addOrderWithJsonString:jsonstring success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
// getEventsWithEventsList

-(void)GetSelectedProductCategoryWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] GetSelectedProductCategoryWithParam:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}


/*-(void)getEventsWithEventsList:(NSDictionary*) param  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure   {
 [[APIManager apiManager] SearchBeerWithParam:param success:^(APIResponseStatusCode statusCode, id searchInfo) {
 //if([obj isKindOfClass: [NSNull null] ])
 
 @try {
 self.searchObj = [[Search alloc]initWithProductList:[searchInfo valueForKey:@"productCount"]];
 successStatus(TRUE);
 }
 @catch (NSException *exception) {
 failure(ParseError, kInternalInconsistencyErrorMessage);
 }
 @finally {
 
 
 }
 
 
 } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
 failure(statusCode, errorMessage);
 }];
 }*/

-(void)Uploadimage:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] Uploadimage:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}

-(void)GetProductDetailWithParam:(NSDictionary*) param   successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] GetProductDetailWithParam:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        @try {
            successStatus(statusCode, userInfo);
            
        }
        @catch (NSException *exception) {
            failure(ParseError, kInternalInconsistencyErrorMessage);
            
            
        }
        @finally {
        }
        
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
-(void)VerifyEmailIdWithParam:(NSDictionary*) param  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] VerifyEmailIdWithParam:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        if ([[userInfo valueForKey:@"IsExists"] intValue]==0) {
            successStatus(TRUE);
            
        }
        else{
            successStatus(FALSE);
            
        }
        
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
-(void)GetReviewWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] GetReviewWithParam:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        @try {
            successStatus(statusCode, userInfo);
            
        }
        @catch (NSException *exception) {
            failure(ParseError, kInternalInconsistencyErrorMessage);
            
        }
        @finally {
            
        }
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
//AddFavorite
-(void)AddFavoriteWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] AddFavoriteWithParam:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
        
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
//RemoveFavorite
-(void)RemoveFavoriteWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] RemoveFavoriteWithParam:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
        
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}

-(void)ViewFavoriteWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] ViewFavoriteWithParam:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        @try {
            successStatus(statusCode, userInfo);
            
        }
        @catch (NSException *exception) {
            failure(ParseError, kInternalInconsistencyErrorMessage);
            
        }
        @finally {
            
        }
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
-(void)SearchBeerWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] SearchBeerWithParam:param success:^(APIResponseStatusCode statusCode, id searchInfo) {
        //if([obj isKindOfClass: [NSNull null] ])
        
        @try {
            successStatus(statusCode, searchInfo);
        }
        @catch (NSException *exception) {
            failure(ParseError, kInternalInconsistencyErrorMessage);
        }
        @finally {
        }
        
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
-(void)SearchLiquorWithParam:(NSDictionary*) param   successStatus:(successBlock) successStatus failure:(failureBlock) failure     {
    [[APIManager apiManager] SearchLiquorWithParam:param success:^(APIResponseStatusCode statusCode, id searchInfo) {
        
        successStatus(statusCode, searchInfo);
        
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
//Searchproduct
-(void)SearchproductWithParam:(NSDictionary*) param   successStatus:(successBlock) successStatus failure:(failureBlock) failure    {
    [[APIManager apiManager] SearchproductWithParam:param success:^(APIResponseStatusCode statusCode, id searchInfo) {
        
        successStatus(statusCode, searchInfo);
        
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
-(void)SearchWineWithParam:(NSDictionary*) param   successStatus:(successBlock) successStatus failure:(failureBlock) failure    {
    [[APIManager apiManager] SearchWineWithParam:param success:^(APIResponseStatusCode statusCode, id searchInfo) {
        
        successStatus(statusCode, searchInfo);
        
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}


//-(void)EditUserAccountWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
//    [[APIManager apiManager] EditUserAccountWithParam:param success:^(APIResponseStatusCode statusCode, id userInfo) {
//
//     successStatus(statusCode, userInfo);
//
//    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
//        failure(statusCode, errorMessage);
//    }];
//}
//GetAuthorizeCredentialsEncrypt
-(void)GetAuthorizeCredentialsEncrypt:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    
    [[APIManager apiManager] GetAuthorizeCredentialsEncrypt:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}




-(void)GetLoyalityCardImageWithParam:(NSDictionary*) param  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure   {
    
    [[APIManager apiManager] GetLoyalityCardImageWithParam:param success:^(APIResponseStatusCode statusCode, id LoyaltycardInfo) {
        self.LoyaltyCardObj = [[LoyaltyCard alloc]initWithLoyaltyCardInfo:[[LoyaltycardInfo valueForKey:@"GetLoyalityCardImageResult"] objectAtIndex:0]];
        
        successStatus(TRUE);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
-(void)GetStoreDetailWithParam:(NSDictionary*) param  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] GetStoreDetailWithParam:param success:^(APIResponseStatusCode statusCode, id storeInfo) {
        
        self.closestStoreModelObj = [[ClosestStoreModel alloc]initWithStoreInfo:storeInfo];
        successStatus(TRUE);
        
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}

-(void)GetWeeklyAdsWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] GetWeeklyAdsWithParam:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
-(void)GetProductDealsWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] GetProductDealsWithParam:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
-(void)AddReviewWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] AddReviewWithParam:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}

-(void)EditReviewWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] EditReviewWithParam:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}


-(void)ViewMyOrderWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] ViewMyOrderWithParam:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
-(void)GetAPPSettings:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] GetAPPSettings:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}

-(void)cancelBookedOrders:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure
{
    [[APIManager apiManager] cancelBookedOrders:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}

-(void)GetAddressValidator:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] GetAddressValidator:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
-(void)GetCouponCode:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] GetCouponCodeValidator:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}

-(void)GetUserNotificationHistory:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] GetUserNotificationHistory:param success:^(APIResponseStatusCode statusCode, id userInfo)
     {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}

-(void)GetBranchesWithParam:(NSDictionary*) param  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] loginWithUserCredentials:param success:^(APIResponseStatusCode statusCode, id userInfo) {
        ;
        [self setCurrentSignedUserInfoToArchiver];
        
        if(self.currentSignedInUser)
            successStatus(TRUE);
        else
            successStatus(FALSE);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}
-(void)InsertNotificationSubscriptionJsonstring:(NSString*) jsonstring  successStatus:(successBlock) successStatus failure:(failureBlock) failure   {
    [[APIManager apiManager] InsertNotificationSubscriptionWithJsonString:jsonstring success:^(APIResponseStatusCode statusCode, id userInfo) {
        successStatus(statusCode, userInfo);
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        failure(statusCode, errorMessage);
    }];
}

@end