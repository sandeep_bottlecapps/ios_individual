//
//  UserLocationManager.m
//  @lacarte
//
//  Created by Kuldeep Tyagi on 12/15/14.
//  Copyright (c) 2014 classic. All rights reserved.
//

#import "UserLocationManager.h"

#pragma mark - Declare notification strings
NSString *kLocationChangedNotification = @"LocationChangedNotification";

@interface UserLocationManager    ()  {
}
@property (nonatomic, copy) UpdateLocationCompletionHandler _completionBlock;
@end

@implementation UserLocationManager
static CLLocationManager        *_defaultlocationManager;
static UserLocationManager      *_locationManager;
@synthesize _completionBlock;

+(UserLocationManager*)defaultLocationManager  {
    @synchronized([UserLocationManager class])   {
        if (_locationManager == nil) {
            _locationManager = [[UserLocationManager alloc] init];
        }
        return _locationManager;
    }
    return nil;
}

- (id)init {
    if ((self = [super init])) {
        // initialize the singleton object
#warning set Location service in appdelegate and show alertview for location request
        /*
        http://stackoverflow.com/questions/5905940/location-service-ios-alert-call-back
        http://stackoverflow.com/questions/21334780/ios-turn-on-location-services-with-settings-and-cancel-buttons-how-do-i-captu
        http://stackoverflow.com/questions/5998465/how-to-prompt-user-to-turn-on-location-services-again?rq=1
        uirequireddevicecapabilities info.plist location service
        http://stackoverflow.com/questions/3765837/how-to-properly-set-uirequireddevicecapabilities
        http://stackoverflow.com/questions/9521468/ios-cllocationmanager-displaying-the-reenable-location-services-popup
         */
         
        if (!_defaultlocationManager)   {
            _defaultlocationManager = [[CLLocationManager alloc] init];
            _defaultlocationManager.delegate = self;
            _defaultlocationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
            
            if ([_defaultlocationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [_defaultlocationManager requestWhenInUseAuthorization];
                [_defaultlocationManager requestAlwaysAuthorization];
            }
            
            [_defaultlocationManager startUpdatingLocation];
            //[_defaultlocationManager startMonitoringSignificantLocationChanges] // efficient usage
        }
    }
    return self;
}

// just initialize the object
-(void)initializeLocationManager  {
}

-(void)dealloc  {
    [_defaultlocationManager stopUpdatingLocation];
    _defaultlocationManager.delegate        = nil;
    _defaultlocationManager                 = nil;
    _locationManager                        = nil;
    self._completionBlock                   = nil;
}

#pragma mark- start/stop updating user's location
-(void) startUpdatingLocation  {
    [_defaultlocationManager startUpdatingLocation];
}

-(void)stopUpdatingLocation {
    [_defaultlocationManager stopUpdatingLocation];
}

// callback method to get location update as iPhone's location change.
-(void)updateLocationWithCompletionHandler: (UpdateLocationCompletionHandler) handler   {
    self._completionBlock = handler;
}

#pragma mark get status of location service Enabled/or Disabled
- (BOOL) isLocationServiceEnabled  {
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        return YES;
    }
    return NO;
}

#pragma mark get user's most recent location
-(CLLocation *)getUserCurrentLocation   {
    return [_defaultlocationManager location];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error  {
    if([error code] == kCLErrorDenied)  {
        [manager stopUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations    {
#warning Need to customize the code so that location manager only use in background and best use of servcie to improve the bettary performance.
    if(self._completionBlock)   {
        self._completionBlock([locations lastObject]);
        [[NSNotificationCenter defaultCenter] postNotificationName: kLocationChangedNotification object: [locations lastObject]];
        return;
    }
}

/********************************** Depricated Delegate**********************************/
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation   {
//#warning Need to customize the code so that location manager only use in background and best use of servcie to improve the bettary performance.
//    /********************************** To get the user location address**********************************/
//
//   http://stackoverflow.com/questions/20967496/google-maps-ios-sdk-getting-current-location-of-user
//    
//    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(28.51963440287253, 77.20053809734496) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
//        
//        
//        NSLog(@"reverse geocoding results: %@", response.firstResult);
//        
//    subLocality: Butterfly Park
//    locality: New Delhi
//    country: India
//    }];
//   
//    /********************************** To get the user location address**********************************/
//    
//    if(self._completionBlock)   {
//        self._completionBlock(newLocation);
//        return;
//    }
//}
/********************************** Depricated Delegate**********************************/

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status  {
    if(status == kCLAuthorizationStatusAuthorizedAlways || status ==  kCLAuthorizationStatusAuthorizedWhenInUse) {
        [manager startUpdatingLocation];
    }
    else    {
        [manager stopUpdatingLocation];
    }
}

@end