 //
//  APIManager.m
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/6/14.
//  Copyright (c) 2014 Kuldeep Tyagi. All rights reserved.
//

#import "APIManager.h"
#import "Reachability.h"


#define kContentType            @"Content-Type"
#define kContentTypeText        @"text/html"
#define kContentTypeJSON        @"application/json"
#define kContentTypeImage       @"image/jpeg"
#define kContentTypeFormData    @"application/x-www-form-urlencoded"
#define kDefaultCheckInImage    @"photo.jpg"



@implementation APIManager

static APIManager *_apiManager;
static AFHTTPRequestOperationManager *_AFManager;

+ (APIManager*)apiManager {
    @synchronized([APIManager class])   {
        if (_apiManager == nil) {
            _apiManager = [[APIManager alloc] init];
        }
        return _apiManager;
    }
    return nil;
}

- (id)init {
    if ((self = [super init])) {
        // initialize the singleton object
        if (!_AFManager)   {
            _AFManager = [AFHTTPRequestOperationManager manager];
            _AFManager.responseSerializer.acceptableContentTypes = [_AFManager.responseSerializer.acceptableContentTypes setByAddingObject:kContentTypeJSON];
            [_AFManager.requestSerializer setValue:kContentTypeFormData forHTTPHeaderField:kContentType];
        }
    }
    return self;
}

-(void)dealloc  {
    [[_AFManager reachabilityManager] stopMonitoring];
    _AFManager = nil;
    _apiManager = nil;
}

#pragma mark- local Mathods

-(NSString*)getlocalizedStringForStatusCode:(int)statusCode {
    return [NSHTTPURLResponse localizedStringForStatusCode: statusCode];;
}

-(void)checkStatus:(id) responseObject status:(apiStatusBlock)status  {
    if([[responseObject objectForKey:APIKeyStatus] integerValue] ==  HTTPSuccessStatus)  {
        status(Success, [responseObject objectForKey:APIKeyMessage]);
    }
    else    {
        status(APIStatusFail, [responseObject objectForKey:APIKeyMessage]);
    }
}

#pragma mark- API Request Mathods Implementation
// api request mathods

//mathod to intialize the manager class
-(void)initializeNetworkObject  {
    [[_AFManager reachabilityManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
    }];
    [[_AFManager reachabilityManager] startMonitoring];
}

-(BOOL)getNetworkStatus {
    if([_AFManager reachabilityManager].reachable)  {
        return YES;
    }
    else    {
        return NO;
    }
}
#pragma mark - Base method to get data from server
-(void)apiRequestWithURL:(NSString *)apiURL parameters:(NSDictionary *)parameters
                 success:(successBlock) success failure:(failureBlock) failure    {
    if([_AFManager reachabilityManager].reachable)  {
        [_AFManager POST:apiURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self checkStatus:responseObject status:^(APIResponseStatusCode statusCode, NSString *message){
                if(statusCode == Success)
                    success(Success, responseObject);
                else
                    failure(APIStatusFail, message);
            }];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    apiURL = nil;
}
#pragma mark login API manager Implementation
//ForgotPassword
-(void) ForgotPasswordWithUserCredentials:(NSDictionary*)userCredential success:(successBlock) success failure:(failureBlock) failure  {
    if([_AFManager reachabilityManager].reachable)   {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL], [[APIRequestHelper requestHelper] getAPIKeyword:kAPIGetpassword]];
        DLog(@"%@ parameter=%@",lBaseUrlString,userCredential);
        _AFManager.responseSerializer.acceptableContentTypes = [_AFManager.responseSerializer.acceptableContentTypes setByAddingObject:kContentTypeJSON];
        [_AFManager.requestSerializer setValue:kContentTypeFormData forHTTPHeaderField:kContentType];
        [_AFManager POST:lBaseUrlString parameters:userCredential success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"%@", responseObject);
            success(Success, responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
    }
    else{
        failure(NetworkError, kNetworkErrorMessage);
    }
}
-(void) loginWithUserCredentials:(NSDictionary*)userCredential success:(successBlock) success failure:(failureBlock) failure    {
    if([_AFManager reachabilityManager].reachable)   {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL], [[APIRequestHelper requestHelper] getAPIKeyword:kAPIKeyLogin]];
        NSLog(@"%@ parameter=%@",lBaseUrlString,userCredential);
        _AFManager.responseSerializer.acceptableContentTypes = [_AFManager.responseSerializer.acceptableContentTypes setByAddingObject:kContentTypeJSON];
        [_AFManager.requestSerializer setValue:kContentTypeFormData forHTTPHeaderField:kContentType];
        [_AFManager POST:lBaseUrlString parameters:userCredential success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"%@", responseObject);
            if ([[responseObject valueForKey:@"Authentication"] boolValue]==FALSE) {
                DLog(@"Message=%@",[responseObject valueForKey:@"Message"]);
                failure(APIStatusFail, [responseObject valueForKey:@"Message"]);
            }
            else    {
                DLog(@"Message=%@",[responseObject valueForKey:@"Message"]);
                success(Success, responseObject);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        
        
        
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
}

#pragma mark Signup API manager Implementation
//EditUserAccount
-(void) EditUserAccount:(NSDictionary*)userCredential success:(successBlock) success failure:(failureBlock) failure    {
    if([_AFManager reachabilityManager].reachable)   {
        
        _AFManager.responseSerializer.acceptableContentTypes = [_AFManager.responseSerializer.acceptableContentTypes setByAddingObject:kContentTypeJSON];
        [_AFManager.requestSerializer setValue:kContentTypeFormData forHTTPHeaderField:kContentType];
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL], [[APIRequestHelper requestHelper] getAPIKeyword:kAPIEditUserAccount]];
        DLog(@"%@ userCredential=%@",lBaseUrlString,userCredential);
        [_AFManager POST:lBaseUrlString parameters:userCredential success:^(AFHTTPRequestOperation *operation, id responseObject) {
            DLog(@"responseObject-%@",responseObject);
            if ([[responseObject valueForKey:@"Authentication"] boolValue]==FALSE) {
                failure(APIStatusFail, [responseObject valueForKey:@"Message"]);
            }
            else    {
                success(Success, responseObject);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        
    }
}

-(void) SignupWithUserCredentials:(NSDictionary*)userCredential success:(successBlock) success failure:(failureBlock) failure    {
    if([_AFManager reachabilityManager].reachable)   {
        
        _AFManager.responseSerializer.acceptableContentTypes = [_AFManager.responseSerializer.acceptableContentTypes setByAddingObject:kContentTypeJSON];
        [_AFManager.requestSerializer setValue:kContentTypeFormData forHTTPHeaderField:kContentType];
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL], [[APIRequestHelper requestHelper] getAPIKeyword:kAPISignUp]];
        DLog(@"%@ userCredential=%@",lBaseUrlString,userCredential);
        [_AFManager POST:lBaseUrlString parameters:userCredential success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([[responseObject valueForKey:@"Authentication"] boolValue]==FALSE) {
                DLog(@"Message=%@",[responseObject valueForKey:@"Message"]);
                failure(APIStatusFail, [responseObject valueForKey:@"Message"]);
            }
            else       {
                success(Success, responseObject);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        
    }
}
#pragma mark Logout API manager Implementation
#pragma mark Getterms API manager Implementation
-(void)getTermsWithsuccess:(successBlock) success failure:(failureBlock) failure {
    if([_AFManager reachabilityManager].reachable)  {
        _AFManager.responseSerializer.acceptableContentTypes = [_AFManager.responseSerializer.acceptableContentTypes setByAddingObject:kContentTypeFormData];
       // [_AFManager.requestSerializer setValue:kContentTypeFormData forHTTPHeaderField:kContentType];
        _AFManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIGetTerms]];

        DLog(@"lBaseUrlString%@",lBaseUrlString);
        [_AFManager POST:[lBaseUrlString stringByReplacingOccurrencesOfString:@" " withString:@"%20"] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self checkStatus:responseObject status:^(APIResponseStatusCode statusCode, NSString *message){
                success(Success, responseObject);
                
            }];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
}



#pragma mark GetStoreDetail API manager Implementation
-(void) GetStoreDetailWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure     {
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIGetStoreDetail]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}

#pragma mark GetStoreDetail API manager Implementation
-(void) ViewProfileWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure     {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIGetUserAccountDetails]];
        NSLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
}

#pragma mark Get product quantity API manager Implementation
-(void) getProductQuantityWithParameter:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure     {
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIGetProductQuantity]];
        DLog(@"From%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
}

#pragma mark GetLoyalityCardImageWithParam API manager Implementation
//GetAuthorizeCredentialsEncrypt
-(void) GetAuthorizeCredentialsEncrypt:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure     {
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIGetAuthorizeCredentialsEncrypt]];
        NSLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
}

-(void) GetLoyalityCardImageWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure     {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIGetLoyalityCardImage]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}
#pragma mark VerifyEmailIdWithParam API manager Implementation
-(void) VerifyEmailIdWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure     {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIVerifyEmailId]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}
#pragma mark SearchBeerWithParam API manager Implementation
-(void) SearchBeerWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure     {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPISearchBeer]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}


#pragma mark SearchLiquorWithParam API manager Implementation
-(void) SearchLiquorWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure     {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPISearchLiquor]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}

#pragma mark SearchWineWithParam API manager Implementation
//SearchproductWithParam
-(void) SearchproductWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure     {
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPISearchproduct]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}

-(void) SearchWineWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure     {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPISearchWine]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}

//GetEventDetailsDetailsWithParam

#pragma mark GetEventDetailsDetailsWithParam API manager Implementation
-(void)GetEventDetailsDetailsWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure    {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIGetEventDetails]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
      
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}
#pragma mark GetSelectedProductCategoryWithParam API manager Implementation
-(void)GetSelectedProductCategoryWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure    {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIGetSelectedProductCategory]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}
//GetProductDealsWithParam
-(void)GetProductDealsWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure    {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIGetProductDeals]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}
//EditReviewWithParam
#pragma mark EditReviewWithParam API manager Implementation
-(void)EditReviewWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure    {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIEditReview]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}

//AddReviewWithParam
#pragma mark AddReviewWithParam API manager Implementation
-(void)AddReviewWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure    {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIAddReview]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}
//
#pragma mark AddOrderWithParam API manager Implementation
-(void)AddOrderWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure    {
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIAddOrder]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}
#pragma mark AddOrderWithParam API manager Implementation
-(void)ViewMyOrderWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure    {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIViewMyOrder]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        NSLog(@"lBaseURLString = %@...Param=%@,viewMyOrder= %@",lBaseUrlString,param,kAPIViewMyOrder);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}
#pragma mark Events API manager Implementation
-(void)GetWeeklyAdsWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure    {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIGetWeeklyAds]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}
#pragma mark Events API manager Implementation
-(void)EditUserAccountWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure    {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIEditUserAccount]];
        NSLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}

//EditUserAccountWithParam

#pragma mark AddFavoriteWithParam API manager Implementation
-(void)AddFavoriteWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure    {
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIAddFavorite]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}
//RemoveFavoriteWithParam

#pragma mark RemoveFavoriteWithParam API manager Implementation
-(void)RemoveFavoriteWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure    {
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIRemoveFavorite]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}

//addorder
#pragma mark ViewFavoriteWithParam API manager Implementation
-(void)addOrderWithJsonString:(NSString*)jsonString success:(successBlock) success failure:(failureBlock) failure    {
    
    /**********************************By AfNetworking****************************************/
    
    [_AFManager.requestSerializer setValue:kContentTypeFormData forHTTPHeaderField:kContentType];
    
    NSDictionary * dicta = [NSDictionary dictionaryWithObjectsAndKeys:jsonString,@"txtjson", nil];
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIAddOrder]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,jsonString);
        [_AFManager POST:lBaseUrlString parameters:dicta success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"responseObject--%@", responseObject);
            if (![[[[responseObject valueForKey:@"GetOrderStatus"] objectAtIndex:0] valueForKey:@"Message"] isEqualToString:@"Inserted"]) {
                failure(APIStatusFail, [[[responseObject valueForKey:@"GetOrderStatus"] objectAtIndex:0] valueForKey:@"Message"]);
            }
            else    {
                success(Success, responseObject);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        
        /**********************************By AfNetworking****************************************/
    }
}

//getEventsWithEventsList
#pragma mark ViewFavoriteWithParam API manager Implementation
-(void)getEventsWithEventsList:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure    {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIEvent]];
        NSLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        _AFManager.responseSerializer.acceptableContentTypes = [_AFManager.responseSerializer.acceptableContentTypes setByAddingObject:kContentTypeJSON];
        [_AFManager.requestSerializer setValue:kContentTypeFormData forHTTPHeaderField:kContentType];
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}

#pragma mark ViewFavoriteWithParam API manager Implementation
-(void)ViewFavoriteWithParam:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure    {
    
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIViewFavorite]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
}
//Uploadimage
-(void)Uploadimage:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure {
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIUploadimage]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
    
    
}

-(void)GetProductDetailWithParam:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure {
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIGetProductDetail]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
    
    
}
//GetReviewWithParam
-(void)GetReviewWithParam:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure {
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIGetReview]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
    
    
}
//InsertNotificationSubscriptionWithParam
-(void)InsertNotificationSubscriptionWithJsonString:(NSString*)jsonString success:(successBlock) success failure:(failureBlock) failure    {
    if([_AFManager reachabilityManager].reachable)  {
        
//        NSArray *Array=@[@{@"StoreId":@"10066",@"UserId":@"72974",@"IsEventSelected":@"1" ,@"items":@[@{@"SubCatId":@"18",@"CatId":@"1"},@{@"SubCatId":@"1",@"CatId":@"3"},@{@"SubCatId":@"8",@"CatId":@"2"}]}];
////        NSDictionary *diction = [NSDictionary dictionaryWithObjectsAndKeys:dict,@"Notification", nil];
//        
//        
//        
//        
//        
//        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:Array
//                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
//                                                             error:nil];
//        
//        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//        
        
        
        
        NSDictionary * dicta = [NSDictionary dictionaryWithObjectsAndKeys:jsonString,@"Notification", nil];

        
        
        
        /**********************************By AfNetworking****************************************/
        
        [_AFManager.requestSerializer setValue:kContentTypeFormData forHTTPHeaderField:kContentType];
        
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIInsertNotificationSubscription]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,jsonString);
        [_AFManager POST:lBaseUrlString parameters:dicta success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
    
    
    
}
//GetAPPSettings
-(void)GetAPPSettings:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure {
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIGetAPPSettings]];
        NSLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
}

//Cancelled Booked Orders
-(void)cancelBookedOrders:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure
{
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPICancelOrder]];
        NSLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
}

//Get Coupon Code
-(void)GetCouponCodeValidator:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure {
    if([_AFManager reachabilityManager].reachable)  {
          NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],@"ValidateCoupon"];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
}
//GetAddressValidator
-(void)GetAddressValidator:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure {
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIAddressValidator]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
    }
}
//GetUserNotificationHistory
-(void)GetUserNotificationHistory:(NSDictionary*) param   success:(successBlock) success failure:(failureBlock) failure {
    if([_AFManager reachabilityManager].reachable)  {
        NSString* lBaseUrlString = [NSString stringWithFormat:@"%@%@", [[APIRequestHelper requestHelper] getBaseURL],[[APIRequestHelper requestHelper] getAPIKeyword:kAPIGetUserNotificationHistory]];
        DLog(@"lBaseUrlString%@ param-%@",lBaseUrlString,param);
        [_AFManager POST:lBaseUrlString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(Success, responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(ParseError, [error localizedDescription]);
        }];
        lBaseUrlString  = nil;
    }
    else    {
        failure(NetworkError, kNetworkErrorMessage);
        
    }
    


}

@end