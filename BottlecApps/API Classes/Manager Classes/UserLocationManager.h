//
//  UserLocationManager.h
//  @lacarte
//
//  Created by Kuldeep Tyagi on 12/15/14.
//  Copyright (c) 2014 classic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#pragma mark - Declare Block
typedef void (^UpdateLocationCompletionHandler)(CLLocation *);
typedef void (^SignificantLocationChanngedCompletionHandler)(CLLocation *);

#pragma mark - Declare notification strings
extern NSString *kLocationChangedNotification;

@interface UserLocationManager : NSObject <CLLocationManagerDelegate>

+(UserLocationManager*)defaultLocationManager;
-(void)initializeLocationManager;
-(void) startUpdatingLocation;
-(void)stopUpdatingLocation;
// callback method to get location update as iPhone's location change.
-(void)updateLocationWithCompletionHandler: (UpdateLocationCompletionHandler) handler;
- (BOOL) isLocationServiceEnabled;
-(CLLocation *)getUserCurrentLocation;

@end
