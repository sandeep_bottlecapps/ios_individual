//
//  ModelManager.h
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/6/14.
//  Copyright (c) 2014 Kuldeep Tyagi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIManager.h"
#import "CompletionBlocks.h"
#import "ModelBase.h"
#import "User.h"
#import "Store.h"
#import "StoreAddress.h"
#import "StoreDetails.h"
#import "StoreOpenCloseTime.h"
#import "ClosestStoreModel.h"
#import "UserProfile.h"
#import "Search.h"
#import "EventModel.h"
#import "EventList.h"
#import "ViewMyOrderModel.h"
#import "ViewMyOrderProductListModel.h"
@interface ModelManager : NSObject  {
    
}
@property(nonatomic, strong) User                                          *currentSignedInUser;
@property(nonatomic, strong) Store                                         *storeObj;
@property(nonatomic, strong) ClosestStoreModel                             *closestStoreModelObj;
@property(nonatomic, strong) UserProfile                                   *userProfileObj;
@property(nonatomic, strong) LoyaltyCard                                   *LoyaltyCardObj;
@property(nonatomic, strong) EventModel                                    *EventModelObj;
@property(nonatomic, strong) EventList                                     *EventListObj;
@property(nonatomic, strong) ViewMyOrderModel                              *ViewMyOrderModelObj;
@property(nonatomic, strong) ViewMyOrderProductListModel                   *ViewMyOrderProductListModelObj;


///
@property(nonatomic, strong) NSMutableString        *deviceToken;
-(NSString*) processNotification:(NSDictionary *)userInfo;

+(ModelManager*)modelManager;                       // obj method to access Model Manager obj
+(ModelManager *)getModelManager;                   // spatialized for swift access Model Manager obj
+(void)initializeModelManager;                      // to initialize the Model Manager


#pragma mark - device token getter and setter
-(void)setCurrentDeviceToken:(NSString *)deviceToken;
-(NSString *)getCurrentDeviceToken;

-(User*) getuserModel ;
-(UserProfile*)getUserProfileModel;
-(Store*)getStoreModel ;
-(ClosestStoreModel*)getClosestStoreModel ;
-(LoyaltyCard*)getLoyaltyCardModel ;
-(EventModel*)getEventModel ;
-(EventList*)getEventListModel ;
-(ViewMyOrderModel*)getViewMyOrderModel ;
-(ViewMyOrderProductListModel*)ViewMyOrderProductListModelObj ;

-(BOOL) isUserAlreadyLoggedIn;
-(void) archiveIsUserAlreadyLoggedInVariable:(BOOL) isUserLogin;

//network check
-(BOOL)getNetworkStatus;
#pragma mark-api calling
-(void) loginWithUserCredentials:(NSDictionary*) userCredential  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure;
//-(void) logoutUserWithSuccessStatus:(successStatusBlockWithUserMessage) successStatusWithMessage failure:(failureBlock) failure;
-(void)GetTermsAndConditionWithsuccessStatus:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure  ;
-(void) SignupWithUserCredentials:(NSDictionary*) userCredential  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure ;
-(void)ForgotPasswordWithUserCredentials:(NSDictionary*) userCredential  successStatus:(successStatusBlockWithUserMessage) successStatus failure:(failureBlock) failure;
-(void)UserAccotntDetailsWithParam:(NSDictionary*) param  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure;
-(void)GetUserAccountDetailsWithParam:(NSDictionary*) param  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure ;
-(void)GetEventDetailsDetailsWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure;
-(void)Uploadimage:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure ;
-(void)GetProductDetailWithParam:(NSDictionary*) param   successStatus:(successBlock) successStatus failure:(failureBlock) failure ;
-(void)VerifyEmailIdWithParam:(NSDictionary*) param  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure;
-(void)GetReviewWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure ;
-(void)ViewFavoriteWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure ;
-(void)SearchBeerWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure;
-(void)SearchLiquorWithParam:(NSDictionary*) param   successStatus:(successBlock) successStatus failure:(failureBlock) failure;
-(void)SearchWineWithParam:(NSDictionary*) param   successStatus:(successBlock) successStatus failure:(failureBlock) failure ;
-(void)GetAuthorizeCredentialsEncrypt:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure ;
-(void) EditUserAccount:(NSDictionary*) userCredential  successStatus:(successStatusBlockWithUserMessage) successStatus failure:(failureBlock) failure; 
-(void)GetLoyalityCardImageWithParam:(NSDictionary*) param successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure;
-(void)GetStoreDetailWithParam:(NSDictionary*) param  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure;
-(void)GetWeeklyAdsWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure;
-(void)AddReviewWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure ;
-(void)ViewMyOrderWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure ;
-(void)GetBranchesWithParam:(NSDictionary*) param  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure ;
-(void)InsertNotificationSubscriptionJsonstring:(NSString*) jsonstring  successStatus:(successBlock) successStatus failure:(failureBlock) failure ;
-(void)ViewProfileWithParam:(NSDictionary*) param  successStatus:(successStatusBlock) successStatus failure:(failureBlock) failure;
-(void)getEventsWithEventsList:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure   ;
-(void)AddFavoriteWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure  ;
-(void)RemoveFavoriteWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure ;
-(void) getProductQuantityWithParameter:(NSDictionary*)param success:(successBlock) success failure:(failureBlock) failure;
-(void)AddOrderWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure ;
-(void)GetSelectedProductCategoryWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure;
-(void)GetUserNotificationHistory:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure ;
-(void)GetProductDealsWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure ;
-(void)SearchproductWithParam:(NSDictionary*) param   successStatus:(successBlock) successStatus failure:(failureBlock) failure;
-(void)addOrderWithJsonstring:(NSString*) jsonstring  successStatus:(successBlock) successStatus failure:(failureBlock) failure;
-(void)GetCouponCode:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure;
-(void)GetAddressValidator:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure;
-(void)GetAPPSettings:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure;

//CancelOrder
-(void)cancelBookedOrders:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure;
-(void)EditReviewWithParam:(NSDictionary*) param  successStatus:(successBlock) successStatus failure:(failureBlock) failure;
@end
