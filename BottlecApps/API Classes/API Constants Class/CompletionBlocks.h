//
//  CompletionBlocks.h
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/6/14.
//  Copyright (c) 2014 Kuldeep Tyagi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    Success                 = 0,
    NetworkError            = 1,
    ServerError             = 2,
    ParseError              = 3,
    APIStatusFail           = 4,
    InternalInconsistency   = 5,
    ReachLimit              = 6,
    RecordNotFound          = 7,
    FacebookSharingFail     = 8,
} APIResponseStatusCode;

#pragma mark - Define Blocks
typedef void (^successBlock)(APIResponseStatusCode statusCode, id response);
typedef void (^failureBlock)(APIResponseStatusCode statusCode, NSString* errorMessage);
typedef void (^apiStatusBlock)(APIResponseStatusCode statusCode, NSString* message);

typedef void (^successStatusBlock)(BOOL status);
typedef void (^successStatusBlockWithUserMessage)(BOOL status, NSString* message);
typedef void (^successStatusBlockWithDictionary) (BOOL status, NSDictionary *responseDictionary);

typedef void(^completion)(BOOL finished);

#pragma mark - to define the model object
typedef id (^newModelBlock)(NSDictionary *modelDictionary);

// define Error Messages
static NSString * const kEmptyFieldErrorMessage                    = @"Please fill all the mandatory fields...";

static NSString * const kNetworkErrorMessage                    = @"Please check internet connection...";
static NSString * const kNetworkErrorTitle                      = @"Network Check!";
static NSString * const kInternalInconsistencyErrorMessage      = @"Some inconsistency occurred!\nPlease try again...";
static NSString * const kNoMoreRecordsErrorMessage              = @"No more records...";
#pragma mark- HTTP API response codes
static NSInteger const HTTPSuccessStatus = 200;

#pragma mark- response array result records key
static NSString * const kRecordkey              = @"Records";

#pragma mark- reset ListStatus
static BOOL const kResetList             = 0;
static BOOL const kContinuePaging        = 1;
static BOOL const kStartPageNumber       = 1;

#pragma mark- numeber of records per page
static NSInteger const kNumberOfRecordsPerPage = 4;

@interface CompletionBlocks : NSObject

@end
