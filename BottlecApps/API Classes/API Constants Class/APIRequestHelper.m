//
//  APIRequestHelper.m
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/6/14.
//  Copyright (c) 2014 Kuldeep Tyagi. All rights reserved.
//

/******************************BASE URLs**************************************/
// http://www.atlacarte.com/public/api
// http://atlacarte.demos.classicinformatics.com/public/api
/******************************BASE URLs*****************************************/

#import "APIRequestHelper.h"

#define APIKeyDictName @"API Keywords"
#define kBaseURLLive    @"BaseURLLive"
#define kBaseURLDemo    @"BaseURLDemo"
#import "BaseViewController.h"
@implementation APIRequestHelper
static APIRequestHelper* _APIHelper = nil;

+(APIRequestHelper*)requestHelper   {
    @synchronized([APIRequestHelper class])
    {
        if (!_APIHelper)   {
            _APIHelper= [[self alloc] init];
        }
        return _APIHelper;
    }
    return nil;
}

- (id)init {
    if ((self = [super init])) {
        [self initializeAPIDict];
    }
    return self;
}

-(void)dealloc  {
    _APIHelper = nil;
}

-(void) initializeAPIDict   {
    @synchronized([APIRequestHelper class]) {
        if(!apiDict)    {
            NSString *path = [[NSBundle mainBundle] pathForResource:
                              @"API" ofType:@"plist"];
            apiDict = [[NSDictionary alloc] initWithContentsOfFile:path];
            path = nil;
        }
    }
}

-(NSString*) getBaseURL {
    if(!apiDict)
        [self initializeAPIDict];
    if (kLiveApp) {
        return [apiDict valueForKey: kBaseURLLive];

    }
    else{
        return [apiDict valueForKey: kBaseURLDemo];

    
    }
}

-(NSString*)getAPIKeyword:(NSString*)name {
    if(!apiDict)
        [self initializeAPIDict];
    
    return [[apiDict objectForKey:APIKeyDictName] objectForKey:name];
}
@end