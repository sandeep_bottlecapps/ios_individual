//
//  APIRequestHelper.h
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/6/14.
//  Copyright (c) 2014 Kuldeep Tyagi. All rights reserved.
//

#import <Foundation/Foundation.h>
#pragma mark-API KEYWORDS
// API keywords
static NSString * const kAPIAction                                 =   @"Action";
static NSString * const kAPIKeyLogin                               =   @"Login";
static NSString * const kAPIKeyLogout                              =   @"Logout";
static NSString * const kAPIGetTerms                               =    @"GetTerms";
static NSString * const kAPISignUp                                 =    @"SignUp";
static NSString * const kAPIGetpassword                            =    @"Getpassword";

static NSString * const kAPIGetUserAccountDetails                  =    @"GetUserAccountDetails";
static NSString * const kAPIGetEventDetails                        =    @"GetEventDetails";
static NSString * const kAPIUploadimage                            =    @"Uploadimage";
static NSString * const kAPIGetProductDetail                       =    @"GetProductDetail";
static NSString * const kAPIGetProductDeals                        =    @"GetProductDeals";

static NSString * const kAPIVerifyEmailId                          =    @"VerifyEmailId";
static NSString * const kAPIGetReview                              =    @"GetReview";
static NSString * const kAPIViewFavorite                           =    @"ViewFavorite";
static NSString * const kAPISearchBeer                             =    @"SearchBeer";
static NSString * const kAPISearchLiquor                           =    @"SearchLiquor";
static NSString * const kAPISearchWine                             =    @"SearchWine";
static NSString * const kAPISearchproduct                          =    @"Searchproduct";

static NSString * const kAPIEvent                                  =    @"Event";
static NSString * const kAPIGetUserNotificationHistory             =    @"GetUserNotificationHistory";
static NSString * const kAPIAddressValidator                       =    @"AddressValidator";
static NSString * const kAPIGetAPPSettings                         =    @"GetAPPSettings";
static NSString * const kAPICancelOrder                            =    @"CancelOrder";

//GetAPPSettings
static NSString * const kAPIGetAuthorizeCredentialsEncrypt         =    @"GetAuthorizeCredentialsEncrypt";
static NSString * const kAPIEditUserAccount                        =    @"EditUserAccount";

static NSString * const kAPIEditReview                             =    @"EditReview";
static NSString * const kAPIGetLoyalityCardImage                   =    @" GetLoyalityCardImage";
static NSString * const kAPIGetStoreDetail                         =    @"GetStoreDetail";
static NSString * const kAPIGetWeeklyAds                           =    @"GetWeeklyAds";
static NSString * const kAPIAddReview                              =    @"AddReview";
static NSString * const kAPIAddOrder                               =    @"AddOrder";
static NSString * const kAPIGetProductQuantity                     =    @"GetProductQuantity";

static NSString * const kAPIViewMyOrder                            =    @"ViewMyOrder";
static NSString * const kAPIGetBranches                            =    @"GetBranches";
static NSString * const kAPIInsertNotificationSubscription         =    @"InsertNotificationSubscription";
static NSString * const kAPIAddFavorite                            =    @"AddFavorite";
static NSString * const kAPIRemoveFavorite                         =    @"RemoveFavorite";
static NSString * const kAPIGetSelectedProductCategory             =    @"GetSelectedProductCategory";


//ViewProfileWithParam
@interface APIRequestHelper : NSObject  {
    NSDictionary *apiDict;
}

+(APIRequestHelper*)requestHelper;
-(NSString*) getBaseURL;
-(NSString*)getAPIKeyword:(NSString*)name;

@end
