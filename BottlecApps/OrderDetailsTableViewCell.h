//
//  OrderDetailsTableViewCell.h
//  BOTTLECAPPS
//
//  Created by IMAC5 on 11/19/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetails.h"


@interface OrderDetailsTableViewCell : UITableViewCell
{
    NSString *cancelOrderId;
    NSString *cancelOrderStatusId;
}

-(void) setOrderDetailsWithOrderDetails:(OrderDetails *) orderDetails navigationBtnSelector:(SEL)navigationBtnSelector cancelButtonSelector:(SEL)cancelButtonSelector target:(id)target;
@end
