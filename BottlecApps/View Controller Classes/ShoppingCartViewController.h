//
//  ShoppingCartViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 8/11/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "BaseViewController.h"

@interface ShoppingCartViewController  :BaseViewController<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray         *searchResult;
    NSMutableArray         *arrayForQuantity;
    
}
@property (strong, nonatomic)  UILabel *ordersummaryLabel;
@property (strong, nonatomic)  UILabel *itemCostLabel;
@property (strong, nonatomic)  UILabel *taxLabel;
@property (strong, nonatomic)  UILabel *orderTotalLabel;
@property (strong, nonatomic)  UIButton *continueShoppingButton;
@property (strong, nonatomic)  UIButton *DeliverButton;
@property (strong, nonatomic)  UIButton *PickUpButton;
@property (nonatomic ,retain)  NSMutableDictionary *updatedQuantityDict;

-(void)pushToOrderViewControllerThroughPickUp;
-(void)pushToOrderViewControllerThroughDeliver;
@end
