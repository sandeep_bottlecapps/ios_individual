//
//  HomeViewController.h
//  BottlecApps
//
//  Created by Kuldeep Tyagi on 8/4/15.
//  Copyright (c) 2015 Kuldeep Tyagi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "PageControl.h"
#import "iCarousel.h"

@interface HomeViewController  :BaseViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,iCarouselDelegate,iCarouselDataSource>
{
    iCarousel *iCar;
    NSMutableArray *returnProductIDArray;
   
}
@property (retain, nonatomic)  UIButton                   *EventsAndPromotionButton;
@property (retain, nonatomic)  NSMutableArray                    *EventsAndPromotionArray;
@property (retain, nonatomic)  UITextView                 *EventsAndPromotionTextView;
@property (retain, nonatomic)  UIButton                   *teaserCopyAboutButton;
//@property (retain, nonatomic)  UIButton                   *viewmoreButton;
@property (retain, nonatomic) UILabel                     *tapHereLabel;
@property (retain, nonatomic)  NSArray                    *EventsArray;
@property (retain, nonatomic)  NSArray                    *Events_icons_Array;
@property (retain, nonatomic)  PageControl                *custompagecontrol;
@property ( nonatomic)  int                               currentImageNumber;
@property ( retain,nonatomic)  NSString                   *currentImageName;
@property(nonatomic,retain)NSMutableArray         *segueIdentifierArray;
//-(void)EventsAndPromotionButton:(NSInteger)index;
//-(void)getProductIdsFromLocalStorage:(NSArray *)localStorageArray;
//@"EVENTS & PROMOTIONS teaser copy about an upcoming event here"
//NSArray * UIView *bottomView
@end
