//
//  ContactUsViewController.m
//  BottlecApps
//
//  Created by Kuldeep Tyagi on 8/4/15.
//  Copyright (c) 2015 Kuldeep Tyagi. All rights reserved.
//

#import "ContactUsViewController.h"
#import "ClosestStoreModel.h"
#import "StoreAddress.h"
#import "StoreDetails.h"

#import <MessageUI/MessageUI.h>

@implementation ContactUsViewController
-(void)callApiStoreDetails{
 //   __weak User *userObj = [[ModelManager modelManager] getuserModel];

    NSMutableDictionary* StoreDetailDict = [[ NSMutableDictionary alloc] init];
    [StoreDetailDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    //[StoreDetailDict setObject:userObj.userId forKey: kParamUserId];

    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] GetStoreDetailWithParam:StoreDetailDict  successStatus:^(BOOL status) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            __weak ClosestStoreModel *     lClosestStoreModelObj = kGetClosestStoreModel;
            //    [[ModelManager modelManager] getStoreOpenCloseTime];
            _WeekDaysArray      =[NSMutableArray array];
            _WeekOpenTimeArray  =[NSMutableArray array];
            _WeekCloseTimeArray =[NSMutableArray array];
            
            
            for (StoreOpenCloseTime * storeOpenTime in lClosestStoreModelObj.GetStoreOpenclose) {
                [_WeekDaysArray addObject:storeOpenTime.DayID];
                [_WeekOpenTimeArray addObject:storeOpenTime.StoreOpenTime];
                [_WeekCloseTimeArray addObject: storeOpenTime.StoreCloseTime];
                
            }
            [self UpdateUI];
            
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}
#pragma mark - SlideNavigationController Methods -
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu  {
    return YES;
}

-(void)UpdateUI{
    if(self.CustomScrollView)
    [self.CustomScrollView removeFromSuperview];
    [self addScrollView];
    [self.CustomScrollView setBackgroundColor:[UIColor clearColor]];
    //[self.CustomScrollView setBackgroundColor:[UIColor blackColor]];
    self.CustomScrollView.contentSize=CGSizeMake(kCustomScrollViewWidth, kCustomScrollViewHeight+100);
    __weak ClosestStoreModel* lClosestStoreModelObj = kGetClosestStoreModel;
    __weak StoreDetails *storeDetails = kGetStoreDetailsModel;
    storeImageURL = [NSURL URLWithString:storeDetails.StoreImage];
    NSData *imageData = [NSData dataWithContentsOfURL:storeImageURL];
    storeImage = [UIImage imageWithData:imageData];
    
    DLog(@"%@", storeDetails.StoreUserAddress1);
    DLog(@"StoreName--------%f",(storeImage.size.height));
    
    UIImageView *StoreAddressImageView=[self createImageViewWithRect:CGRectMake(0,kPaddingSmall, SCREEN_WIDTH, krowHeight+((storeImage.size.height)/2)) image:nil];
    [StoreAddressImageView setBackgroundColor:[UIColor clearColor]];
    [self.CustomScrollView addSubview:StoreAddressImageView];

   
   AsyncImageView *logoImageImageView=[self createAsynImageViewWithRect:CGRectMake((SCREEN_WIDTH-((storeImage.size.width)/2))/2, 0, (storeImage.size.width)/2,(storeImage.size.height)/2) imageUrl:storeImageURL isThumnail:YES];
   [StoreAddressImageView addSubview:logoImageImageView];

    UILabel *storeAddressLabel=[self createLblWithRect:CGRectMake(kLeftPadding, (logoImageImageView.frame.origin.y)+(logoImageImageView.frame.size.height)+kPaddingSmall, kLabelWidthMax, kLabelHeight) font:[self addFontSemiBold] text:kLabelStreetAddress textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
    [StoreAddressImageView addSubview:storeAddressLabel];

    UILabel *StreetAddressLabel=[self createLblWithRect:CGRectMake(kLeftPadding, storeAddressLabel.frame.origin.y+ storeAddressLabel.frame.size.height, kLabelWidthMax, kLabelHeight) font:[self addFontRegular  ] text:storeDetails.StoreUserAddress1 textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
    [StoreAddressImageView addSubview:StreetAddressLabel];
    
    UILabel *StateAddressLabel=[self createLblWithRect:CGRectMake(kLeftPadding, StreetAddressLabel.frame.origin.y+ StreetAddressLabel.frame.size.height, kLabelWidthMax,kLabelHeight) font:[self addFontRegular  ] text:[NSString stringWithFormat:@"%@, %@ %@",storeDetails.StoreUserCity,storeDetails.StoreStateName,storeDetails.StoreUserZipCode] textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
    [StoreAddressImageView addSubview:StateAddressLabel];
    
  

    UIImageView *StoreHoursImageView=[self createImageViewWithRect:CGRectMake(0,StoreAddressImageView.frame.origin.y+ StoreAddressImageView.frame.size.height, SCREEN_WIDTH, kLabelHeight*9) image:nil];
    [StoreHoursImageView setBackgroundColor:[UIColor clearColor]];
    [self.CustomScrollView addSubview:StoreHoursImageView];
    
    UILabel *StoreHoursLabel=[self createLblWithRect:CGRectMake(kLeftPadding, kPaddingSmall, kLabelWidth*2,kLabelHeight) font:[self addFontSemiBold] text:kLabelStoreHours textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
    [StoreHoursImageView addSubview:StoreHoursLabel];

    for (int i=0; i<[_WeekDaysArray count]; i++) {
        NSString *weekdayString=[_WeekDaysArray objectAtIndex:i];
        UILabel *weekLabel=[self createLblWithRect:CGRectMake(kLeftPadding, StoreHoursLabel.frame.origin.y+ StoreHoursLabel.frame.size.height+(i*kLabelHeight),kLabelWidth-30, kLabelHeight) font:[self addFontRegular] text:[weekdayString stringByReplacingOccurrencesOfString:@"." withString:@""] textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
        [weekLabel setBackgroundColor:[UIColor clearColor]];
        [StoreHoursImageView addSubview:weekLabel];

        if (![[_WeekOpenTimeArray objectAtIndex:i]isEqualToString:[_WeekCloseTimeArray objectAtIndex:i]]) {
            
            UILabel *weekOpenTimeLabel=[self createLblWithRect:CGRectMake((weekLabel.frame.origin.x)+(weekLabel.frame.size.width) + 50, StoreHoursLabel.frame.origin.y+ StoreHoursLabel.frame.size.height+(i*kLabelHeight),130, kLabelHeight) font:[self addFontRegular] text:[_WeekOpenTimeArray objectAtIndex:i] textAlignment:NSTextAlignmentRight textColor:[self getColorForBlackText]];
            [weekOpenTimeLabel setBackgroundColor:[UIColor clearColor]];
                      weekOpenTimeLabel.text = [NSString stringWithFormat:@"%@ - %@", [[_WeekOpenTimeArray objectAtIndex:i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]], [[_WeekCloseTimeArray objectAtIndex:i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
            [weekOpenTimeLabel setBackgroundColor:[UIColor clearColor]];

            [StoreHoursImageView addSubview:weekOpenTimeLabel];
            
        }
        else{
        UILabel *closedLabel=[self createLblWithRect:CGRectMake((weekLabel.frame.origin.x)+(weekLabel.frame.size.width)+50, StoreHoursLabel.frame.origin.y+ StoreHoursLabel.frame.size.height+(i*kLabelHeight),130, kLabelHeight) font:[self addFontRegular] text:kLabelClosed textAlignment:NSTextAlignmentRight  textColor:[self getColorForBlackText]];
            [closedLabel setBackgroundColor:[UIColor clearColor]];
            [StoreHoursImageView addSubview:closedLabel];
        }
    }
    UIImageView *separatorIView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 1)];
    [separatorIView setBackgroundColor:[self getColorForLightGreyColor]];
    [StoreHoursImageView addSubview:separatorIView];

    UIImageView *separatorIView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, ( StoreHoursImageView.frame.size.height)-2, SCREEN_WIDTH, 2)];
    [separatorIView2 setBackgroundColor:[self getColorForLightGreyColor]];
    [StoreHoursImageView addSubview:separatorIView2];
    
    CGRect ContentainerViewFrame ;
    ContentainerViewFrame.origin.x=0;
    ContentainerViewFrame.origin.y=SCREEN_HEIGHT-kBottomViewHeight-kCustomButtonContainerHeight;
    ContentainerViewFrame.size.width=SCREEN_WIDTH;
    ContentainerViewFrame.size.height=kCustomButtonContainerHeight;
    
    
    self.CallUsButton=  [self AddCustomButtonInsideContentViewWithFrame:ContentainerViewFrame];
        [self.CallUsButton setTitle:kButtonTittleCALLUS forState:UIControlStateNormal];
    [self.CallUsButton addTarget:self action:@selector(CallUsButton:) forControlEvents:UIControlEventTouchUpInside];
    self.EmailUsButton=  [self AddCustomButtonInsideContentViewWithFrame:CGRectMake(0, ContentainerViewFrame.origin.y-kCustomButtonContainerHeight, SCREEN_WIDTH, kCustomButtonContainerHeight)];
    [self.EmailUsButton setTitle:kButtonTittleEMAILUS forState:UIControlStateNormal];
    [self.EmailUsButton addTarget:self action:@selector(EmailUsButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIImage *LocationImage=[UIImage imageNamed:kImage_locationicon];
    self. getLocationButton=[self createButtonWithFrame:CGRectMake(SCREEN_WIDTH-LocationImage.size.width-kRightPadding, storeAddressLabel.frame.origin.y+ storeAddressLabel.frame.size.height, LocationImage.size.width, LocationImage.size.height) withTitle:kEmptyString];
    
    [self .getLocationButton addTarget:self action:@selector(getLocationButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_getLocationButton setImage:LocationImage forState:UIControlStateNormal];
    [self.CustomScrollView addSubview:self. getLocationButton];
//placeholder

   }


#pragma mark-viewcontrollerlifecycle.
-(void)viewWillAppear:(BOOL)animated{
    [self refreshPage:nil];
}
-(void)refreshPage:(id)sender{
    [self callApiStoreDetails];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshPage:) name:kContactUsViewControllerStoryboardId object:nil];

   
   }
-(void)cancelButtonClicked:(UIButton*)sender{
    [_transparentView removeFromSuperview ];
    
}
-(void)CurrentLocationButtonClicked:(UIButton*)sender{
    [_transparentView removeFromSuperview ];
    __weak ClosestStoreModel* lClosestStoreModelObj = kGetClosestStoreModel;
    __weak StoreDetails *storeDetails = kGetStoreDetailsModel;
    [self ShowLocationWithLat:storeDetails.StoreLatitude longitude:storeDetails.StoreLongitude];
}

/* -(void)addTrantParentview{
   _transparentView=[self createContentViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [_transparentView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_transparentView];
    UIImageView *transparentImageView=[self createImageViewWithRect:CGRectMake(0, 0, SCREEN_WIDTH
                                                                               , SCREEN_HEIGHT) image:nil];
    [_transparentView addSubview:transparentImageView];
    [transparentImageView setBackgroundColor:[UIColor lightGrayColor]];
    [transparentImageView setAlpha:.75];
    
    UIView *containerV=[self createContentViewWithFrame:CGRectMake(kRightPadding, (SCREEN_HEIGHT-(SCREEN_HEIGHT/3))/2, SCREEN_WIDTH-(kRightPadding*2),SCREEN_HEIGHT/3)];
    [containerV setBackgroundColor:[UIColor whiteColor]];
    [_transparentView addSubview:containerV];
    UILabel *headerLabel=[self createLblWithRect:CGRectMake(0, kPaddingSmall, containerV.frame.size.width, kLabelHeight*2) font:[UIFont fontWithName:kFontBold size:kFontBoldSize+4] text:kMessageGetDirectionFrom textAlignment:NSTextAlignmentCenter textColor:[self getColorForBlackText]];
    [containerV addSubview:headerLabel];
    UIButton *CurrentLocationButton=[self createButtonWithFrame:CGRectMake(kLeftPadding, (headerLabel.frame.size.height)+(headerLabel.frame.size.height)+20, containerV.frame.size.width-(kLeftPadding*2), kButtonHeight) withTitle:kButtonTitleCURRENTLOCATION];
    [CurrentLocationButton .titleLabel setFont:[self addFontSemiBold]];
    [CurrentLocationButton setBackgroundColor:[self getColorForRedText]];
    [CurrentLocationButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [CurrentLocationButton addTarget:self action:@selector(CurrentLocationButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [containerV addSubview:CurrentLocationButton];
    
    UIImage *crossimage=[UIImage imageNamed:kImageCrossForLocation];
    UIButton *cancelButton=[self createButtonWithFrame:CGRectMake((containerV.frame.size.width)-(crossimage.size.width)-kPaddingSmall, kPaddingSmall, crossimage.size.width, crossimage.size.height) withTitle:nil];
    [cancelButton setImage:crossimage forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [containerV addSubview:cancelButton];
} */

#pragma-MARK BUTTON ACTION
-(void)getLocationButtonClicked:(UIButton*)sender{
  //  [self addTrantParentview];
    __weak ClosestStoreModel* lClosestStoreModelObj = kGetClosestStoreModel;
    __weak StoreDetails *storeDetails = kGetStoreDetailsModel;
    [self ShowLocationWithLat:storeDetails.StoreLatitude longitude:storeDetails.StoreLongitude];

}
-(void)EmailUsButton:(UIButton*)button{
    if([MFMailComposeViewController canSendMail])   {
        ClosestStoreModel *lClosestStoreModelObj=kGetClosestStoreModel;
        StoreDetails *storedetalObj=kGetStoreDetailsModel;

        @try {
            NSArray *toRecipents = [NSArray arrayWithObject:storedetalObj.StoreEmailId];
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            [mc setSubject:@"info required by bottlecapps user"];
            [mc setMessageBody:kEmptyString isHTML:NO];
            [mc setToRecipients:toRecipents];
            [self presentViewController:mc animated:YES completion:NULL];
        }
        @catch (NSException *exception) {
            [UIView showMessageWithTitle:kError message:@"Please configure the email." showInterval:1.5 viewController:self];
        }
    }
    else    {
        [UIView showMessageWithTitle:kError message:@"Please configure the email." showInterval:1.5 viewController:self];
    }
}
-(void)CallUsButton:(UIButton*)button{
    __weak ClosestStoreModel* lClosestStoreModelObj = [[ModelManager modelManager] getClosestStoreModel];
    
    __weak StoreDetails *storeDetails = [lClosestStoreModelObj.GetStoredetails objectAtIndex:0];
    DLog(@"%@", storeDetails.StoreUserAddress1);

    NSString  *contactNum=[NSString stringWithFormat:@"%@",storeDetails.StoreUserContact];
    NSString *phno=[NSString stringWithFormat:@"tel://%@",contactNum];
    UIActionSheet *actionSheet;

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phno]];
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];

}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
