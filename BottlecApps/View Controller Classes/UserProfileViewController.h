//
//  UserProfileViewController.h
//  BottlecApps
//
//  Created by Kuldeep Tyagi on 8/4/15.
//  Copyright (c) 2015 Kuldeep Tyagi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface UserProfileViewController  :BaseViewController<UITextFieldDelegate,UIScrollViewDelegate>
@property (retain, nonatomic)  UIImageView *UserProfileImageView;
@property (retain, nonatomic)  UIImageView *ProfileLogoImageView;

@property (retain, nonatomic)  UIImageView *CoverPhotoImageView;
@property (retain, nonatomic)  UILabel *UserNameLabel;
@property (retain, nonatomic)  UILabel *DateOfBirthLabel;
@property (retain, nonatomic)  UILabel *TitleLabel;

@property (retain, nonatomic)  UILabel *CardNumberLabel;
@property (retain, nonatomic)  UILabel *addressLabel;
@property (retain, nonatomic)  UILabel *contactLabel;
@property (retain, nonatomic)  UILabel *streetLabel;
@property (retain, nonatomic)  UILabel *apartment;
@property (retain, nonatomic)  UILabel *stateLabel;
@property (retain, nonatomic)  UILabel *pinCodeLabel;
@property (retain, nonatomic)  UILabel *phoneLabel;
@property (retain, nonatomic)  UILabel *emailLabel;
@property (retain, nonatomic)  UIButton     *EditButton;
@property (retain, nonatomic)  NSMutableDictionary     *apiResponseDictionary;


@end
