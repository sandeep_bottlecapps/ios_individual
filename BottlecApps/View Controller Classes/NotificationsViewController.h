//
//  NotificationsViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 8/11/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "BaseViewController.h"

@interface NotificationsViewController  :BaseViewController<UITextFieldDelegate,UIScrollViewDelegate>{
}
//
@property (strong, nonatomic)  UILabel *DescriptionLabel;

@property (strong, nonatomic)  UIButton *LiquorButton;
@property (strong, nonatomic)  UIButton *BeerButton;
@property (strong, nonatomic)  UIButton *EventsButton;
@property (strong, nonatomic)  UIButton *WineButton;
@property (retain, nonatomic) UIView *transparentView;
@property (retain, nonatomic) UIView *containerView;

@property (strong, nonatomic)  UILabel *BeerLabel;
@property (strong, nonatomic)  UILabel *WineLabel;
@property (strong, nonatomic)  UILabel *LiquorLabel;
@property (strong, nonatomic)  UILabel *EventsLabel;
@property (strong, nonatomic)  UIButton *saveButton;
@property (strong, nonatomic)  UIButton *skipButton;
//@property (strong, nonatomic) IBOutlet UIButton *DessertButton;
//@property (strong, nonatomic) IBOutlet UIButton *nonAlcoholicButton;
//@property (strong, nonatomic) IBOutlet UIButton *OthersButton;
//@property (strong, nonatomic) IBOutlet UIButton *PortSherryButton;
//@property (strong, nonatomic) IBOutlet UIButton *RedButton;
//@property (strong, nonatomic) IBOutlet UIButton *roseButton;
//@property (strong, nonatomic) IBOutlet UIButton *sparklingButton;
//@property (strong, nonatomic) IBOutlet UIButton *whiteButton;
//@property (strong, nonatomic) IBOutlet UIButton *closebutton;

- (IBAction)BeerButtonClicked:(id)sender;
- (IBAction)WineButtonClicked:(id)sender;
- (IBAction)saveButtonClicked:(id)sender;
//- (IBAction)skipButtonClicked:(id)sender;
//- (IBAction)dessertButtonClicked:(id)sender;
//- (IBAction)nonalcoholicButtonClicked:(id)sender;
//- (IBAction)othersButtonClicked:(id)sender;
//- (IBAction)portsherryButtonClicked:(id)sender;
//- (IBAction)redButtonClicked:(id)sender;
//- (IBAction)roseButtonClicked:(id)sender;
//- (IBAction)sparklingButtonClicked:(id)sender;
//- (IBAction)whiteButtonClicked:(id)sender;
//- (IBAction)closeButtonClicked:(id)sender;
- (IBAction)liquorButtonClicked:(id)sender;
- (IBAction)eventsButtonClicked:(id)sender;

@end