//
//  TermsAndConditionViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 8/12/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "BaseViewController.h"
#import "terms&conditionOperation.h"

@interface TermsAndConditionViewController  :BaseViewController<UITextFieldDelegate,TermsOperationDelegate>
{
    UIWebView *webView;
    NSOperationQueue *operationQueue;
   }
@end
