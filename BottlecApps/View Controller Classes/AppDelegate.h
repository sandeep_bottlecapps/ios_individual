//
//  AppDelegate.h
//  BottlecApps
//
//  Created by imac9 on 8/3/15.
//  Copyright (c) 2015 Kuldeep Tyagi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelManager.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import<CoreLocation/CoreLocation.h>
#import<MapKit/MapKit.h>
@class User;

@interface AppDelegate : UIResponder <UIApplicationDelegate,MKReverseGeocoderDelegate,CLLocationManagerDelegate, UIAlertViewDelegate>
{
User *user;
    CLLocationManager	*locationManager;//use for get use's current location latitude and longitutde....
    NSString *deviceToken;
    int badgeValue;


}
@property (strong, nonatomic) UIWindow *window;
@property(assign)	CGFloat chooselatitude;
@property(assign) CGFloat chooselongitude;
@property (nonatomic,retain )NSMutableArray *CurrentLocationData;
@property (nonatomic,retain) NSMutableArray *globalFavoriteOfflineArray;
@property (nonatomic,retain )NSString *selectCatVal;
@property (nonatomic,retain) NSString *deviceToken;
@property (nonatomic,retain )NSString *MyOrderStatus;
@property (nonatomic,retain )NSString *LastModifedStoreId;
@property (nonatomic,retain )NSString *uuidString;
@property (nonatomic,retain)NSString *IsDeliveryEnabled_Cart;
@property (nonatomic,retain)NSString *DeliveryLimit_Cart;
@property (nonatomic,retain)NSString *DeliveryCharges_Cart;
@property (nonatomic,retain) NSString *deliveryTaxRate;
@property (nonatomic,retain) NSString *currentTimeStore;
-(void)setAppSettingWithDict:(NSDictionary*)appSettingDict;
@end

