//
//  ReviewViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 9/3/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ReviewViewController.h"
#import "ReviewDetailsViewController.h"
#import "Constants.h"

#define kReviewSectionHeight                                150
#define kReviewRowHeight                                    65

@interface ReviewViewController ()<UITableViewDelegate,UITableViewDataSource,EDStarRatingProtocol>{
    NSMutableDictionary *apiResponseDictionary;
    UIImage *productImage;
    NSURL *productImageURL;
    NSInteger selectedIndex;
}
@end

@implementation ReviewViewController
@synthesize getProductID,getProductObj,getReviewArray;

-(void)CallApiGetreviewwithProductId:(NSString*)lgetProductID{
    
    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [paramDict setObject:@"1" forKey: kParamPageNumber];
    [paramDict setObject:kPageSize forKey: kParamPageSize];
    [paramDict setObject:lgetProductID forKey: kParamProductID];
    [self showSpinner];
    [[ModelManager modelManager] GetReviewWithParam:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
        NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            
            @try {
                getReviewArray=[response valueForKey:@"Getreview"];
                [self UpdateUI];
                
            }
            @catch (NSException *exception) {
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    
                    
                });
            }
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        
        
    }];
    
    
}


-(void)UpdateUI{
    __weak User *userObj=kGetUserModel;
       BOOL isReviewAlreadyGiven;
    isReviewAlreadyGiven=NO;
    CGRect ContentainerViewFrame ;
    ContentainerViewFrame.origin.x=0;
    ContentainerViewFrame.origin.y=SCREEN_HEIGHT-kBottomViewHeight-kCustomButtonContainerHeight-kPaddingSmall;
    ContentainerViewFrame.size.width=SCREEN_WIDTH;
    ContentainerViewFrame.size.height=kCustomButtonContainerHeight;
    if ([getReviewArray count]==0) {
        [self showAlertViewWithTitle:nil message:kAlertReviewBeFirst];
    }
    for (NSDictionary *dict in getReviewArray) {
        if ([[dict valueForKey:@"UserId"]isEqualToString:userObj.userId]) {
            isReviewAlreadyGiven=YES;
            break;
        }
    }
    CGRect defaultTableViewFrame;

    if (isReviewAlreadyGiven==NO) {
        UIButton *writeReviewButton;
        defaultTableViewFrame=CGRectMake(0,kNavigationheight
                                         , SCREEN_WIDTH, SCREEN_HEIGHT-((kBottomViewHeight)+kNavigationheight+kCustomButtonContainerHeight));

        writeReviewButton=  [self AddCustomButtonInsideContentViewWithFrame:ContentainerViewFrame];
        
        [writeReviewButton setTitle:kButtonTittleWRITEAREVIEW forState:UIControlStateNormal];
        [writeReviewButton addTarget:self action:@selector(writeReviewButton:) forControlEvents:UIControlEventTouchUpInside];
         }
    else{
        defaultTableViewFrame=CGRectMake(0,kNavigationheight
                                         , SCREEN_WIDTH, SCREEN_HEIGHT-((kBottomViewHeight)+kNavigationheight));
    }
       self.defaultTableView = [[UITableView alloc] initWithFrame:defaultTableViewFrame
                                                         style:UITableViewStylePlain];
    self.defaultTableView.pagingEnabled = NO;
        [self.defaultTableView setSeparatorColor: [UIColor lightGrayColor]];
    self.defaultTableView .separatorStyle=UITableViewCellSeparatorStyleNone;
    self.defaultTableView.alwaysBounceVertical = NO;
    [self.view addSubview:self.defaultTableView];
    [self.defaultTableView setDelegate:self];
    [self.defaultTableView setDataSource:self];
}
-(void)initializeAllArrayAndDict{
    getReviewArray=[NSMutableArray new];
}
-(void)viewWillAppear:(BOOL)animated{
    [self CallApiGetreviewwithProductId:getProductID];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeAllArrayAndDict];
    // getReviewArray;
    productImageURL = [NSURL URLWithString:getProductObj.productImageURL];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:productImageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            productImage = [UIImage imageWithData:imageData];
        });
    });
    // Do any additional setup after loading the view.
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:kGoToReviewDetailsViewControllerSegueId]){
        
        ReviewViewController *controller =[segue destinationViewController];
        controller.getProductObj = getProductObj;
        controller.getProductID = getProductID;
        
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of ections.
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    AsyncImageView  *productImageView;
    UILabel         *productNameLabel;
    UILabel         *reviewTitle;
    UILabel         *reviewDateLabel;
    UITextView      *reviewDescTextView;
    UIButton        *editButton;
    
    if (cell == nil)    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:kDefaultTableViewCell];
        cell.layer.masksToBounds = YES;
    }
    if (indexPath.section==0) {
        productImageView=[self createAsynImageViewWithRect:CGRectMake((SCREEN_WIDTH-kProductImageWidth)/2, (kReviewSectionHeight-50)/2, 50,50) imageUrl:productImageURL isThumnail:YES];
        productNameLabel=[self createLblWithRect:CGRectMake(0, productImageView.frame.origin.y+productImageView.frame.size.height+20, SCREEN_WIDTH, kLabelHeight) font:[self addFontSemiBold] text:getProductObj.productName textAlignment:NSTextAlignmentCenter textColor:[self getColorForRedText]];
        [productNameLabel setBackgroundColor:[UIColor clearColor]];
        [cell addSubview:productImageView];
        [cell addSubview:productNameLabel];
        UIImageView *separatorEnd = [[UIImageView alloc] initWithFrame:CGRectMake(0, kReviewSectionHeight-1, SCREEN_WIDTH, 1)];
        [separatorEnd setBackgroundColor:[self getColorForLightGreyColor]];
        [cell.contentView addSubview:separatorEnd];
    }
    else {
        self.RatingView=[self createContentViewWithFrame:CGRectMake(SCREEN_WIDTH - keditReviewImageWidth - kStarRatingWidth + 17, 2, kStarRatingWidth - 20, kLabelHeight)];
        self.starRating=[[EDStarRating alloc ]initWithFrame:CGRectMake(0, 0, kStarRatingWidth - 10, kLabelHeight) ];
        [self.RatingView addSubview:self.starRating];
        self.starRating.starImage = [UIImage imageNamed:kImagegreystar];
        self.starRating.starHighlightedImage = [UIImage imageNamed:kImageredstar];
        self.starRating.maxRating = 5.0;
        self.starRating.delegate = self;
        self.starRating.horizontalMargin = 22;
        self.starRating.editable=NO;
        self.starRating.tintColor = [self getColorForRedText];
        self.starRating.displayMode=EDStarRatingDisplayFull;
        self.starRating.rating= [[self ProperValue:[[ getReviewArray objectAtIndex:indexPath.row] valueForKey:@"ReviewRating"]] intValue];
        
        reviewTitle=[self createLblWithRect:CGRectMake(15,0, SCREEN_WIDTH-(kLeftPadding*2) - 35, kLabelHeight) font:[self addFontSemiBold] text:[self ProperValue:[[ getReviewArray objectAtIndex:indexPath.row] valueForKey:@"ReviewTitle"]] textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
        reviewTitle.tag=kTagReviewTitle;
        
        reviewDateLabel=[self createLblWithRect:CGRectMake(15, (reviewTitle.frame.size.height)+(reviewTitle.frame.origin.y), SCREEN_WIDTH-(kLeftPadding*2), kLabelHeight) font:[self addFontRegular] text:[self dateStringFromString:[[ getReviewArray objectAtIndex:indexPath.row] valueForKey:@"Reviewdate"]sourceFormat:@"MM/dd/yyyy" destinationFormat:kServerDateFormat2]textAlignment:NSTextAlignmentLeft textColor:[UIColor colorWithRed:0.5020 green:0.5020 blue:0.5137 alpha:1.0]];
        
        CGRect  rectTextV2=[self setDynamicViewsFrameFor:[[ getReviewArray objectAtIndex:indexPath.row] valueForKey:@"ReviewDescription"]];
        reviewDescTextView=[self createDefaultTxtViewWithRect:CGRectMake(10, (reviewDateLabel.frame.size.height)+(reviewDateLabel.frame.origin.y), SCREEN_WIDTH-keditReviewImageHeight, (rectTextV2.size.height)+20) text:[[ getReviewArray objectAtIndex:indexPath.row] valueForKey:@"ReviewDescription"]
                                                withTextColor:[UIColor colorWithRed:0.5020 green:0.5020 blue:0.5137 alpha:1.0] withfont:[UIFont fontWithName:kFontRegular size:kFontRegularSize+2]];
        [reviewDescTextView setEditable:NO];
        [reviewDescTextView setBackgroundColor:[UIColor clearColor]];
        reviewDescTextView.dataDetectorTypes = UIDataDetectorTypeAll;
        reviewDescTextView.textAlignment = NSTextAlignmentJustified;
        reviewDescTextView.textContainer.lineBreakMode = NSLineBreakByCharWrapping;
        
        editButton=[self createButtonWithFrame:CGRectMake(SCREEN_WIDTH - keditReviewImageHeight - 5, kPaddingSmall - 3, keditReviewImageWidth, keditReviewImageHeight) withTitle:kEmptyString];
        editButton.tag=indexPath.row;
        [editButton addTarget:self action:@selector(editButton:) forControlEvents:UIControlEventTouchUpInside];
        [editButton setImage:[UIImage imageNamed:kImageEditIcon] forState:UIControlStateNormal];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell addSubview:reviewTitle];
        [cell addSubview:reviewDescTextView];
        [cell addSubview:reviewDateLabel];
        [cell addSubview:reviewDescTextView];
        [cell addSubview:self.RatingView];
        User *userObj=kGetUserModel;
        if ([[[ getReviewArray objectAtIndex:indexPath.row] valueForKey:@"UserId"]isEqualToString:userObj.userId]){
            [cell addSubview:editButton];
        }

        UIImageView *separatorEnd = [[UIImageView alloc] initWithFrame:CGRectMake(0, kReviewRowHeight-1 + [self setDynamicViewsFrameFor:[[ getReviewArray objectAtIndex:indexPath.row] valueForKey:@"ReviewDescription"]].size.height, SCREEN_WIDTH, 1)];
        [separatorEnd setBackgroundColor:[self getColorForLightGreyColor]];
        [cell.contentView addSubview:separatorEnd];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return kReviewSectionHeight;
    }
   else  {
        return kReviewRowHeight + [self setDynamicViewsFrameFor:[[ getReviewArray objectAtIndex:indexPath.row] valueForKey:@"ReviewDescription"]].size.height;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==1){
        if ([ getReviewArray count]) {
            return [ getReviewArray count];
        }
    }
    else{
    return 1;
    }
    return 0;
}
/*- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    static NSString *CellIdentifier = @"customCell";
    
    ReviewTableViewCell *cell = (ReviewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.RatingView.frame=CGRectMake(0, kPaddingSmall, SCREEN_WIDTH, kTEXTfieldHeight);
    cell.RatingView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:kImageredstrip]];
   cell. starRating.frame=CGRectMake((SCREEN_WIDTH-kStarRatingWidth)/2, 0, kStarRatingWidth, kTEXTfieldHeight);
  cell.  starRatingLabel.frame=CGRectMake(kLeftPadding, cell.RatingView.frame.origin.y+cell.RatingView.frame.size.height+kPaddingSmall, SCREEN_WIDTH-(kLeftPadding*2), kTEXTfieldHeight);
 

//    cell.ProductImageview.frame = CGRectMake(kLeftPadding,
//                                             (SCREEN_HEIGHT-kCustomCellImageViewheight)/2,
//                                             kCustomCellImageViewWidth,
//                                             kCustomCellImageViewheight);
//    
//    cell.ProductNameLabel.frame = CGRectMake(cell.ProductImageview.frame.origin.x+cell.ProductImageview.frame.size.width+kTopPadding_small,
//                                             kTopPadding_small,
//                                             100,
//                                             20);
    //    cell.ProductQuantityLabel.frame = CGRectMake(cell.ProductNameLabel.frame.origin.x,
    //                                                 cell.ProductNameLabel.frame.origin.y+cell.ProductImageview.frame.size.height,
    //                                                 100,
    //                                                 20);
    //    cell.ProductPriceLabel.frame = CGRectMake(cell.ProductNameLabel.frame.origin.x,
    //                                              cell.ProductQuantityLabel.frame.origin.y+cell.ProductQuantityLabel.frame.size.height,
    //                                              100,
    //                                              20);
    //    cell.TapForDetailButton.frame = CGRectMake(cell.ProductNameLabel.frame.origin.x,
    //                                               cell.ProductPriceLabel.frame.origin.y+cell.ProductPriceLabel.frame.size.height,
    //                                               100,
    //                                               20);
    //
    //    cell.ContainerView.frame = CGRectMake(SCREEN_WIDTH-(kRightPadding*2),
    //                                          0,
    //                                          (kRightPadding*2),
    //                                          kRowHeight_custom);
    //
    //
    //
    //    cell.LikeButton.frame = CGRectMake(  (cell.ContainerView.frame.size.width)/2 ,
    //                                       0,
    //                                       (cell.ContainerView.frame.size.width)/2,
    //                                       (cell.ContainerView.frame.size.height));
    //
    //    cell.CartButton.frame = CGRectMake(  0,
    //                                       0,
    //                                       (cell.ContainerView.frame.size.width)/2,
    //                                       (cell.ContainerView.frame.size.height));
    //
    //
    
    
    
    if (cell == nil) {
        //cell = [[[exerciseListUITableCell alloc] init] autorelease];
        
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ReviewTableViewCell" owner:self options:nil];
        UIImageView *backgroundImageView=[self createImageViewWithRect:CGRectMake(0, 0, SCREEN_WIDTH, cell.frame.size.width) image:[UIImage imageNamed:kImageButtonStrip]];
        
        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (ReviewTableViewCell *)currentObject;
                [cell.contentView addSubview:[self getSeperatorV]];

                cell.starRating.starImage = [UIImage imageNamed:kImagegreystar];
                cell.starRating.starHighlightedImage = [UIImage imageNamed:kImagewhitestar];
                cell.starRating.maxRating = 5.0;
                cell.starRating.delegate = self;
                cell.starRating.horizontalMargin = 12;
                cell.starRating.editable=YES;
                cell.starRating.tintColor = [self getColorForRedText];
                cell.starRating.displayMode=EDStarRatingDisplayFull;
                cell. starRating.rating= 2.5;
              //  cell.ProductImageview.image=[UIImage imageNamed:kImageIcon];
                
                
                break;
            }
        }
    }
    
//    cell.ProductNameLabel.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize];
//    cell.ProductNameLabel.textColor=[self getColorForRedText];
//    
//    cell.Sku.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize];
//    cell.UPC.textColor=[self getColorForBlackText];
//    
//    
//    cell.productSize.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize];
//    cell.productSize.textColor=[self getColorForBlackText];
//    
    
    //    cell.TapForDetailButton .titleLabel.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize];
    //    [cell.TapForDetailButton setTitleColor:[self getColorForBlackText] forState:UIControlStateNormal];
    //
    //    [cell.CartButton setImage:[UIImage imageNamed:kImageCartIconwhite] forState:UIControlStateNormal];
    //    [cell.CartButton setBackgroundColor:[UIColor colorWithRed:255/255.f green:186/255.f blue:5/255.f alpha:1]];
    //
    //    [cell.LikeButton setImage:[UIImage imageNamed:kImageHeartIconwhite] forState:UIControlStateNormal];
    //    [cell.LikeButton setBackgroundColor:[self getColorForRedText]];
    
    //If this is the selected index then calculate the height of the cell based on the amount of text we have
    //Otherwise just return the minimum height for the label.
    
    
    
    
    
    
    return cell;
}*/
-(void)editButton:(UIButton*)button{
    selectedIndex=button.tag;
    ReviewDetailsViewController *vc=[self viewControllerWithIdentifier:kReviewDetailsViewControllerStoryboardId];
    vc.getProductObj = getProductObj;
    vc.getReviewDict=[ getReviewArray objectAtIndex:selectedIndex];
    vc.getProductID = getProductID;

    [self.navigationController pushViewController:vc animated:YES];
    //[self performSegueWithIdentifier:kGoToReviewDetailsViewControllerSegueId sender:nil];
}
-(void)writeReviewButton:(UIButton*)button{
    if([notloggedIn intValue]==1)
         [self showConfirmViewWithTitle:@"Confirm" message:@"In order to proceed further please Sign in or Sign Up for the app." otherButtonTitle:@"Sign In/ Sign Up"];
    else
      [self performSegueWithIdentifier:kGoToReviewDetailsViewControllerSegueId sender:nil];

}

@end
