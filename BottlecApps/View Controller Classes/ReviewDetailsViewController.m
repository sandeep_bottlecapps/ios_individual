//
//  ReviewDetailsViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 9/3/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ReviewDetailsViewController.h"
#import "EDStarRating.h"
#import "SAMTextView.h"
#define ImgeViewHeightAndWidth (IS_IPHONE_4_OR_LESS? 70:IS_IPHONE_6?70:70)
#define kLeftPaddingReview                (IS_IPHONE_4_OR_LESS? 15:IS_IPHONE_6?15:15)
#define kReviewbig                       (IS_IPHONE_4_OR_LESS? 120:IS_IPHONE_6?120:120)

@interface ReviewDetailsViewController ()<EDStarRatingProtocol,UITextViewDelegate>{
    NSMutableArray *ItemArray;
    NSMutableDictionary *apiResponseDictionary;
    UIImage *productImage;
    NSURL *productImageURL;
    NSInteger selectedIndex;
    UITextField *reviewTitle;
    SAMTextView *reviewTextView;
    NSString *ratingString ;
}
@end

@implementation ReviewDetailsViewController
@synthesize starRating;
@synthesize getProductID;
@synthesize getReviewDict;
@synthesize  getProductObj;

-(void)CreateUI{
    [self addScrollView];
    [self.CustomScrollView setBackgroundColor:[UIColor clearColor]];
    if (IS_IPHONE_5) {
        [self.CustomScrollView setContentSize:CGSizeMake(self.CustomScrollView.frame.size.width, self.CustomScrollView.frame.size.height+100)];
    }
    else    {
        (IS_IPHONE_4_OR_LESS? [self.CustomScrollView setContentSize:CGSizeMake(self.CustomScrollView.frame.size.width, self.CustomScrollView.frame.size.height+100)]:IS_IPHONE_6?[self.CustomScrollView setContentSize:CGSizeMake(self.CustomScrollView.frame.size.width, self.CustomScrollView.frame.size.height)]:[self.CustomScrollView setContentSize:CGSizeMake(self.CustomScrollView.frame.size.width, self.CustomScrollView.frame.size.height)]);
    }
    
    AsyncImageView *ProductSelectedImageView=[self createAsynImageViewWithRect:CGRectMake((SCREEN_WIDTH-ImgeViewHeightAndWidth)/2,50, ImgeViewHeightAndWidth, ImgeViewHeightAndWidth) imageUrl:[NSURL URLWithString:getProductObj.productImageURL]isThumnail:YES];
    [self.CustomScrollView addSubview:ProductSelectedImageView];    // Do any additional setup after loading the view.
    
    UILabel *productNameLabel=[self createLblWithRect:CGRectMake(0, ProductSelectedImageView.frame.origin.y+ProductSelectedImageView.frame.size.height+40, SCREEN_WIDTH, kLabelHeight) font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize - 1] text:[self ProperValue:getProductObj.productName] textAlignment:NSTextAlignmentCenter textColor:[self getColorForRedText]];
    [self.CustomScrollView addSubview:productNameLabel];
    
    self.RatingView=[self createContentViewWithFrame:CGRectMake(0,(productNameLabel.frame.origin.y)+(productNameLabel.frame.size.height)+kPaddingSmall, SCREEN_WIDTH, kButtonHeight)];
    self.RatingView.backgroundColor=[self getColorForRedText];

    self.starRating=[[EDStarRating alloc ]initWithFrame:CGRectMake((SCREEN_WIDTH-kStarRatingWidth)/2, 0, kStarRatingWidth, kButtonHeight) ];
    self.starRating.starImage = [UIImage imageNamed:kImageredstar];
    self.starRating.starHighlightedImage = [UIImage imageNamed:kImagewhitestar];
    self.starRating.maxRating = 5.0;
    self.starRating.delegate = self;
    self.starRating.horizontalMargin = 12;
    self.starRating.editable=YES;
    self.starRating.tintColor = [UIColor whiteColor];
    self.starRating.displayMode=EDStarRatingDisplayFull;
    if (getReviewDict!=nil) {
        self.starRating.rating=[[getReviewDict valueForKey:@"ReviewRating"] intValue];
    }
    else{
        self.starRating.rating=0;
    }
    [self.CustomScrollView addSubview:self.RatingView];
    [self.RatingView addSubview:self.starRating];

    UIImage *ImageReviewSmall=[UIImage imageNamed:kImageReviewSmall];
    reviewTitle=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPaddingReview, self.RatingView.frame.origin.y+self.RatingView.frame.size.height + 6 , SCREEN_WIDTH-(kLeftPaddingReview*2), kTEXTfieldHeight) text:kEmptyString withTag:kTextfieldTagReview withPlaceholder:kTextfieldPlaceHolder];
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:reviewTitle.frame] ;
    imageV.image = ImageReviewSmall;
    [self.CustomScrollView addSubview:imageV];
    [self.CustomScrollView bringSubviewToFront:reviewTitle];
   [self.CustomScrollView addSubview:reviewTitle];
    
    UIImage *ImageReviewBig=[UIImage imageNamed:kImageReviewBig];
    
    reviewTextView=[self createCustomTxtViewWithRect:CGRectMake(kLeftPaddingReview, reviewTitle.frame.origin.y+reviewTitle.frame.size.height+10, SCREEN_WIDTH-(kLeftPaddingReview*2), kReviewbig) text:kEmptyString withTextColor:[self getColorForRedText] withfont:[self addFontRegular]];
    [reviewTextView setDelegate:self];
    reviewTextView.placeholder=kPlaceHolderReviewDescription;
    [reviewTextView setBackgroundColor:[UIColor clearColor]];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:reviewTextView.frame] ;
        imageView.image = ImageReviewBig;
    [self.CustomScrollView addSubview:imageView];
    [self.CustomScrollView bringSubviewToFront:reviewTextView];
    [self.CustomScrollView addSubview:reviewTextView ];

    UIButton *SubmitButton;
    CGRect ContentainerViewFrame ;
    ContentainerViewFrame.origin.x=0;
    ContentainerViewFrame.origin.y=SCREEN_HEIGHT-kBottomViewHeight-kCustomButtonContainerHeight;
    ContentainerViewFrame.size.width=SCREEN_WIDTH;
    ContentainerViewFrame.size.height=kCustomButtonContainerHeight;
    SubmitButton=  [self AddCustomButtonInsideContentViewWithFrame:ContentainerViewFrame];
    [SubmitButton setTitle:kButtonTitleSUBMIT forState:UIControlStateNormal];
    [SubmitButton addTarget:self action:@selector(SubmitButton:) forControlEvents:UIControlEventTouchUpInside];
    [SubmitButton setBackgroundColor:[UIColor clearColor]];
    if (getReviewDict!=nil) {
        reviewTitle.text=[getReviewDict valueForKey:@"ReviewTitle"];
        reviewTextView.text=[getReviewDict valueForKey:@"ReviewDescription"];
    }
    
}
-(void)CallApiEditReview{
    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
    User *userObj=kGetUserModel;
    [paramDict setObject:userObj.userId forKey: kParamUserId];
    [paramDict setObject:[getReviewDict valueForKey:@"ReviewID"] forKey: kParamReviewId];
    [paramDict setObject:ratingString forKey: kParamReviewRating];
    [paramDict setObject:reviewTextView.text forKey: kParamReviewTxt];
    [paramDict setObject:reviewTitle.text forKey: kParamReviewTitle];
    [paramDict setObject:getProductID forKey: kParamProductID];
    [self showSpinner];
    [[ModelManager modelManager] EditReviewWithParam:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
        NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            
            @try {
                apiResponseDictionary=[[NSMutableDictionary alloc] initWithDictionary:response];
                if ([[apiResponseDictionary valueForKey:@"Authentication"] boolValue]==TRUE) {
                    [self showAlertViewWithTitle:nil message:[apiResponseDictionary valueForKey:@"Message"]];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        
                        [self.navigationController popViewControllerAnimated:YES];
                        
                    });
                }
                else{
                    [self showAlertViewWithTitle:nil message:[apiResponseDictionary valueForKey:@"Message"]];
                    
                }
                
            }
            @catch (NSException *exception) {
                [self showAlertViewWithTitle:kError message:kInternalInconsistencyErrorMessage];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                });
            }
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        
    }];
    
}
-(void)CallApiAddReview{
    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
    User *userObj=kGetUserModel;
    [paramDict setObject:userObj.userId forKey: kParamUserId];
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [paramDict setObject:ratingString forKey: kParamReviewRating];
    [paramDict setObject:reviewTextView.text forKey: kParamReviewTxt];
    [paramDict setObject:reviewTitle.text forKey: kParamReviewTitle];
    [paramDict setObject:getProductID forKey: kParamProductID];
       [self showSpinner];
    [[ModelManager modelManager] AddReviewWithParam:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
        NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            
            @try {
                apiResponseDictionary=[[NSMutableDictionary alloc] initWithDictionary:response];
                if ([[apiResponseDictionary valueForKey:@"Authentication"] boolValue]==TRUE) {
                    [self showAlertViewWithTitle:nil message:[apiResponseDictionary valueForKey:@"Message"]];

                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        
                        [self.navigationController popViewControllerAnimated:YES];
                        
                    });

                }
                else{
                    [self showAlertViewWithTitle:nil message:[apiResponseDictionary valueForKey:@"Message"]];

                }
                
            }
            @catch (NSException *exception) {
                [self showAlertViewWithTitle:kError message:kInternalInconsistencyErrorMessage];

                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                });
            }
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        
    }];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    ratingString=@"0";
    [self CreateUI];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-BUTTON CLICKED METHODS
-(BOOL)formValidate{
    if ([reviewTitle.text length]==0) {
        [self showAlertViewWithTitle:kAlertReviewTitleRequired message:nil];
        return NO;
    }
    else if ([reviewTextView.text length]==0){
        [self showAlertViewWithTitle:kAlertReviewDescriptionRequired message:nil];
        return NO;

    }
    else{
        return YES;

        
    }
    
}

-(void)SubmitButton:(id)sender{
    if ([self formValidate]==YES) {
        if (getReviewDict!=nil) {
            [self CallApiEditReview];
        }
        else{
            [self CallApiAddReview];
        }
    }
   
}

-(void)starsSelectionChanged:(EDStarRating *)control rating:(float)rating
{
  ratingString = [NSString stringWithFormat:@"%.1f", rating];
//    if( [control isEqual:starRating] )
//  //      starRatingLabel.text = ratingString;
//    else
//   //     starRatingLabel.text = ratingString;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if (  range.location==0&&[text isEqualToString: @" "] ){
        
        return NO;
    }
    return YES;
}
@end
