//
//  EventTableViewCell.h
//  BOTTLECAPPS
//
//  Created by NehaMishra on 06/01/16.
//  Copyright © 2016 Monika Kumari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblDetail1;
@property (weak, nonatomic) IBOutlet UIButton *btnMap;

@end
