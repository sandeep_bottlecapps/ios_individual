 //
//  OrderDetailViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 8/28/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "OrderDetailViewController.h"
#import "HomeViewController.h"
#import "OrderConfirmationViewController.h"
#import "CardIO.h"
#import "AuthNet.h"
#import "MobileDeviceLoginRequest.h"
#import "StringEncryption.h"
#import "Base64.h"
#import "CJSONDeserializer.h"
#import "CJSONDataSerializer.h"
#import "Order.h"
#import "AppDelegate.h"
#import "CardIO.h"
#import "Constants.h"

#define kRowHeight_orderconfirmation                    100

#define PRODUCT_KEY  @"GetOrderStatus"
#define appService [NSURL URLWithString:@"http://staging.liquorapps.com/WhiskeyStoreServiceV2/login.svc/AddOrder"]
#define PAYMENT_SUCCESSFUL @"Transaction successful."
#define PAYMENTNOTSUCCESSFUL @"Transaction not successful."

#define AUTH_ID_KEY @"AuthenticationId"
#define OREDER_ID_KEY @"OrderId"
#define ORDER_ARRAY_KEY @"items"
#define Auth_ARRAY_KEY @"PaymentDetails"
#define USER_REMARKS @"UserRemarks"
#define TOTAL_TAX @"TotalTax"
#define JSON_KEY @"txtjson"
#define AUTH @"Success"
#define MSG @"Message"
#define PRODUCT_KEY  @"GetOrderStatus"
#define FLOAT_COLOR_VALUE(n) (n)/255.0

#define kCreditCardLength 16
#define kCreditCardLengthPlusSpaces (kCreditCardLength + 3)
#define kExpirationLength 4
#define kExpirationLengthPlusSlash  kExpirationLength + 1
#define kCVV2Length 4
#define kZipLength 5
#define kCreditCardObscureLength (kCreditCardLength - 4)
#define kSpace @" "
#define kSlash @"/"
#define ERROR_MESSAGE @"Login error Please check your login details or your device is not registered."
#define kCardNumberErrorAlert 1001
#define kCardExpirationErrorAlert 1002
#define kCvvErrorAlert 1003
#define kZipAlert 1004
#define krowHeightDeliveryDateAndTime 130
#define outofstock_alert_tag    1005
#define PAYMENT_DETAILS @"PaymentDetails"
#define deliveryTimeFormat @"hh:mm a"
#define deliveryDateTimeFormat @"MM-dd-yyyy hh:mm a "
#define deliveryDateFormat @"MM-dd-yyyy"

//@"MM-dd-yyyy hh:mm a "
@interface OrderDetailViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,CardIOPaymentViewControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    NSDate *dd;
    BOOL isCouponApplied;
     UIButton *couponCodeButton;
    NSString* textfieldData;
    double itemCost;
    double discount ;
    NSMutableDictionary                 *orderTotalArray;

    NSMutableArray                 *orderSummaryItems;
    NSMutableArray                 *orderSummaryValues;
    BOOL                    couponCode;
    BOOL                    enterNewAddress;
    int                     productAvailable;
    UITextField             *cardnumberTextfd;
    UITextField             *monthTextfd;
    UITextField             *cvvTextfd;
    NSString                *orderId;
    NSString                *payTypeString;
    UITextField             *addressTxtFd;
    UITextField             *couponCodeTxtFd;
    UITextField             *apartmentTxtFd;
    UITextField             *cityTxtFd;
    UITextField             *stateTxtFd;
    UITextField             *zipTxtFd;
    UILabel                 *SaveAddressLabel;
    NSMutableDictionary     * AdressValidatorDict;
    NSMutableDictionary     * CouponCodeValidatorDict;
    NSString                *CouponCodeValidatorString;
    NSString                *AdressValidatorString;
    UITextField             *firstNameTxtfd;
    UITextField             *lastNameTxtfd;
    UITextField             *phoneNumberTxtfd;
    NSDictionary            *setDeliveryAddressDict;
    UIButton                *applyButton;
    UIButton                *payNowButton;
    UIButton                *payAtStoreButton;
    UIButton                *cameraButton;
    NSMutableArray          *AuthArr;
    NSString                *specialInstructionString;
    UIButton                *DeliveryDateButton;
    UIButton                *DeliveryTimeFromButton;
    UIButton                *DeliveryTimeToButton;
    NSString                *deliveryDateString;
    NSString                *deliveryTimeFromString;
    NSString                *deliveryTimeToString;
    NSMutableArray          *pickerArray;
    NSMutableArray          *cardDeatailArray;
    NSMutableArray          *deliveryDateAndTimeArray;
    BOOL                    stoploop;
    BOOL isrestrict;
    BOOL isclosed;
   }
@end
NSString *addresstotal3;
NSString *addresstotal;
NSString *orderAddress;
NSString *orderstate;
NSString *ordercity;
NSString *ordercountry;
NSString *orderzipcode;
NSInteger numberofLine;
CGRect textRect2;
CGFloat labelHeight;
@implementation OrderDetailViewController
@synthesize OrderTypeString;
@synthesize getCartArray;
@synthesize creditCardBuf;
@synthesize totaltax;
@synthesize orderID,oType;
@synthesize expirationBuf;
@synthesize TotalAmount;
@synthesize orderArray;
@synthesize zipTextField;



@synthesize productID,onSaleId;
-(void)GoToShoppingCartWithUpdatedQuantity:(NSArray*)responseArray{
    [self GoBack];
  
    NSDictionary *theInfo =
    [NSDictionary dictionaryWithObjectsAndKeys:responseArray,@"responseArray", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CheckForOutOfStock"
                                                        object:self
                                                      userInfo:theInfo];

}



/*first we derive the weekday of selected delivery date and then set the picker array for delivery time accordingly*/
-(NSString*)GetWeekdayNameFromGivenDate:(NSDate*)selectedDate{
    if (selectedDate==nil) {
        selectedDate=[NSDate date];
    }
    NSString *weekdayName;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    weekdayName = [dateFormatter stringFromDate:selectedDate];
   // NSLog(@"weekday Name = %@",weekdayName);
    //delivery open and close time logic goes here
    

       return weekdayName;
}
/*this api given us the day and  open close time of stote */
-(void)lcallApiStoreDetails{
    __weak User *userObj = kGetUserModel;
  
    NSMutableDictionary* StoreDetailDict = [[ NSMutableDictionary alloc] init];
    [StoreDetailDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [StoreDetailDict setObject:userObj.userId forKey: kParamUserId];
    
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] GetStoreDetailWithParam:StoreDetailDict  successStatus:^(BOOL status) {
            [self hideSpinner];
            __weak ClosestStoreModel *     lClosestStoreModelObj = kGetClosestStoreModel;
            //    [[ModelManager modelManager] getStoreOpenCloseTime];
                     for (StoreOpenCloseTime * storeOpenTime in lClosestStoreModelObj.GetStoreOpenclose) {
              //  NSLog(@"DayID=%@",storeOpenTime.DayID);
              //  NSLog(@"StoreOpenTime=%@",storeOpenTime.StoreOpenTime);
              //  NSLog(@"StoreCloseTime=%@",storeOpenTime.StoreCloseTime);
                         
            }
            
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}

-(void)callApiGetRequestProductWiseQuantity{
    DLog(@"");
    
    [orderArray enumerateObjectsUsingBlock:^(id order, NSUInteger idx, BOOL *stop){
        [dict_productQuantityToPurchaseWithID setObject:[order valueForKey:@"Quantity"] forKey:[order valueForKey:@"ProductId"]];
        DLog(@"dict_productQuantityToPurchaseWithID-%@",dict_productQuantityToPurchaseWithID);
        
    }];
    NSLog(@"dict_productQuantityToPurchaseWithID---%@",dict_productQuantityToPurchaseWithID);

    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:kSTOREIDbottlecappsString, kParamStoreId,
                                [[dict_productQuantityToPurchaseWithID allKeys] componentsJoinedByString:@","], kParamProductIds,
                                [[dict_productQuantityToPurchaseWithID allValues] componentsJoinedByString:@","], kParamProductQuantities,
                                [self GetUUID], kParamUniqueId,
                                nil];
    
    NSLog(@"Parameters = %@",parameters);
    
    [self showSpinner1];
    [[ModelManager modelManager] getProductQuantityWithParameter: parameters success:^(APIResponseStatusCode statusCode, id response) {
  [self hideSpinner];
        __block BOOL isOUTofstock =NO;
        
        NSLog(@"Response  = %@",response);
        
       /*  NSError *error;
       id jsonObject = [NSJSONSerialization JSONObjectWithData:response options:0 error:&error];
        NSDictionary *dict = (NSDictionary *)jsonObject; */
    if ([OrderTypeString isEqualToString:kStringDeliver])
    {
        NSString *nowCurrentTimeStore = [response objectForKey:@"CurrentStoreTime"];
        NSArray *dateAndTimeSepartorArray = [nowCurrentTimeStore componentsSeparatedByString:@" "];
        NSString *beforeDdeliveryDateString = [dateAndTimeSepartorArray objectAtIndex:0];
        beforeDdeliveryDateString = [beforeDdeliveryDateString stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
        beforeDdeliveryDateString = [self addZeroBeforeMonth:[self getDateFromDateString:beforeDdeliveryDateString]];
     
        
           NSString *nowCurrentTimeString = [self formatServerDateAndTime:nowCurrentTimeStore];
           NSDate *nowCurrentDate = [self getDateFromString:nowCurrentTimeString];
           NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
           
           NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:nowCurrentDate];
           NSInteger hour = [components hour];
           //NSLog(@"%ld",hour);
           
           NSDateComponents *components1 = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:oneHourExtraServerTime];
           NSInteger hour1 = [components1 hour];
           //NSLog(@"%ld",hour1);

           
           if(hour>=hour1)
           {
               if([deliveryDateString isEqualToString:[self getCurrentDate]])
               {
                 [self showAlertViewWithTitle:@"Delivery Time" message:@"Selected time has been passed! We have reset the next available time."];
                 return;
               }
               else {}
           }

       }

        
        NSArray *qtyListArray = [response objectForKey:@"QtyList"];
        for(int i=0;i<[qtyListArray count];i++)
        {
            NSDictionary *innerDict = [qtyListArray objectAtIndex:0];
            int isValid = [[innerDict objectForKey:@"IsValid"] intValue];
            if(isValid == 1)
            {
               
            }
           else
           {
               NSString *str=[innerDict valueForKey:@"ProductId"];
               [ProductOutofStockArray addObject:str];
              isOUTofstock=YES;
               isrestrict=YES;
           }
            
        }
        
        
        /* [response enumerateObjectsUsingBlock:^(id selectedProduct, NSUInteger idx1, BOOL *stop1){
            [orderArray enumerateObjectsUsingBlock:^(id order, NSUInteger idx ,BOOL *stop){
                if ([[order valueForKey:@"ProductId"] intValue]==[[selectedProduct valueForKey:@"ProductId"] intValue]) {
                    int maxProductQuantity=[[selectedProduct valueForKey:@"Quantity"] intValue];
                    int actuallproductquantityselected=[[order valueForKey:@"Quantity"] intValue];
                    
                    if (actuallproductquantityselected>maxProductQuantity) {
                        isOUTofstock=YES;
                        NSString *str=[selectedProduct valueForKey:@"ProductId"];
                        productId_Of_outOfStock = [str intValue];
                        [ProductOutofStockArray addObject:selectedProduct];
                        *stop = YES;
                        isrestrict=YES;
                        // Stop enumerating
                        
                        
                    }
                }
              }];
            }
         ]; */
        DLog(@"ProductOutofStockArray====%@",ProductOutofStockArray);
        if (isOUTofstock==NO) {
            if ([payTypeString isEqualToString:kStringPayAtStore]) {
                [self callApiAddorderWithAuthArray:orderArray withPaymentType:kStringPayAtStore];
            }
            else{
            [self saveCreditCardInfo];
            [self createTransaction];
            }
        }
        else{
            if (!([ProductOutofStockArray count]==0)) {
                
                if ([ProductOutofStockArray count]==1) {
                    [self showAlertViewWithTitle:@"Products is out of stock." message:nil];

                }
               else if ([ProductOutofStockArray count]==[orderArray count]) {
                    [self showAlertViewWithTitle:@"All products are out of stock." message:nil];
                    
                }
                else{
                    UIAlertView *outofstock_alert=  [[UIAlertView alloc] initWithTitle:@"Some Products are out of stock" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [outofstock_alert setTag:outofstock_alert_tag];
                    [outofstock_alert show];
                }

            }
            [self GoToShoppingCartWithUpdatedQuantity:response];
           
        }
        
        
        
   }
     
                                     failure:^(APIResponseStatusCode statusCode, NSString *errorMessage)
    {
          [self hideSpinner];
        [UIView showMessageWithTitle:kError message:@"Server not responding.\n Please try again later." showInterval:1.5 viewController:self];
    }];
    
    
}

-(void)callApiGetAuthorizeCredentialsEncrypt   {
    DLog(@"orderArray---%@",orderArray);

    [orderArray enumerateObjectsUsingBlock:^(id order, NSUInteger idx, BOOL *stop){
        [dict_productQuantityToPurchaseWithID setObject:[order valueForKey:@"Quantity"] forKey:[order valueForKey:@"ProductId"]];
        DLog(@"dict_productQuantityToPurchaseWithID-%@",dict_productQuantityToPurchaseWithID);
        
    }];
    DLog(@"dict_productQuantityToPurchaseWithID---%@",dict_productQuantityToPurchaseWithID);
    
    
    NSMutableDictionary* paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [paramDict setObject:[[dict_productQuantityToPurchaseWithID allKeys] componentsJoinedByString:@","] forKey: kParamProductIds];
    
    [paramDict setObject:[[dict_productQuantityToPurchaseWithID allValues] componentsJoinedByString:@","] forKey: kParamProductQuantities];
    
    [paramDict setObject:[self GetUUID] forKey: kParamUniqueId];
    DLog(@"paramDict--%@",paramDict);
    [self showSpinner1]; // To show spinner
    [[ModelManager modelManager] GetAuthorizeCredentialsEncrypt:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
       // NSLog(@"response-----%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            {
                
                
                BOOL paymentfound=[[[response objectAtIndex:0]objectForKey:@"IsEnablePaymentGateWay"] boolValue];
                
                //Added flag for enable payment
               
                BOOL payatStorefound=[[[response objectAtIndex:0]objectForKey:@"Payatstore"] boolValue];
                
                
                
                ////////code for decryption starts here/////////////////////////////
                NSData * encryptedData_AppLoginId = [NSData dataWithBase64EncodedString:[[response objectAtIndex:0]objectForKey:@"AppLoginId"]];
                NSData * decryptedData_AppLoginId  = [[StringEncryption alloc] decrypt: encryptedData_AppLoginId key:@"742369ea43f8f33dd60469b2fd32bfd" iv:@"MC1Y4BOgyvji_Q3L"];
                NSString * decryptedString_AppLoginId = [[NSString alloc] initWithData:decryptedData_AppLoginId encoding:NSUTF8StringEncoding];
                //   [Helpers showAlertWithMessage:decryptedString_AppLoginId withTitle:nil];
                
                
                NSData * encryptedData_AppTransactionKey = [NSData dataWithBase64EncodedString:[[response objectAtIndex:0]objectForKey:@"AppTransactionKey"]];
                NSData * decryptedData_AppTransactionKey = [[StringEncryption alloc] decrypt: encryptedData_AppTransactionKey key:@"742369ea43f8f33dd60469b2fd32bfd" iv:@"MC1Y4BOgyvji_Q3L"];
                
                
                NSString * decryptedString_AppTransactionKey = [[NSString alloc] initWithData:decryptedData_AppTransactionKey encoding:NSUTF8StringEncoding];
                
                
                NSLog(@"decryptedString_AppTransactionKey: %@", decryptedString_AppTransactionKey); //print the decrypted text
                
                
                
                
                
                [[NSUserDefaults standardUserDefaults]setObject:decryptedString_AppTransactionKey forKey:@"AppTransactionKey"];
                [[NSUserDefaults standardUserDefaults]setObject:decryptedString_AppLoginId forKey:@"AppLoginId"];
                
                [[NSUserDefaults standardUserDefaults]setObject:[[response objectAtIndex:0]objectForKey:@"IsLive"] forKey:@"IsLive"];
                
                [[NSUserDefaults standardUserDefaults]setBool:paymentfound forKey:@"IsEnablePaymentGateWay"];
                
                [[NSUserDefaults standardUserDefaults]setBool:payatStorefound forKey:@"Payatstore"];
                
                
                
                DLog(@"AppTransactionKey-------%@\nAppLoginId-------%@\nIsLive-------%@\nIsEnablePaymentGateWay-------%@\n,Payatstore-------%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"AppTransactionKey"],[[NSUserDefaults standardUserDefaults]objectForKey:@"AppLoginId"],[[NSUserDefaults standardUserDefaults]objectForKey:@"IsLive"],[[NSUserDefaults standardUserDefaults]objectForKey:@"IsEnablePaymentGateWay"],[[NSUserDefaults standardUserDefaults]objectForKey:@"Payatstore"]);
                
                
                if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"AppTransactionKey"] length]) {
                    
                   
                    [self callApiGetRequestProductWiseQuantity];
                    
//                                    [self saveCreditCardInfo];
//                                    [self createTransaction];
                }
                else{
                    [self showAlertViewWithTitle:nil message:PAYMENTNOTSUCCESSFUL];
                    
                }
            }
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}

-(void)callApiAddressValidator  {
    // getEventsWithEventsList
    [self showSpinner1]; // To show spinner
    
    if(!AdressValidatorDict)    {
        AdressValidatorDict = [NSMutableDictionary dictionary];
    }
    else    {
        [AdressValidatorDict removeAllObjects];
    }
    User *userObj=kGetUserModel;
   
    [AdressValidatorDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [AdressValidatorDict setObject:userObj.userId forKey: kParamUserId];
    [AdressValidatorDict setObject:firstNameTxtfd.text forKey: kParamFirstName];
    [AdressValidatorDict setObject:lastNameTxtfd.text forKey: kParamLastName];
    [AdressValidatorDict setObject:addressTxtFd.text forKey: kParamAddress];
    [AdressValidatorDict setObject:apartmentTxtFd.text forKey: kParamApartment];

    [AdressValidatorDict setObject:cityTxtFd.text forKey: kParamCity];
    [AdressValidatorDict setObject:stateTxtFd.text forKey: kParamState];
    [AdressValidatorDict setObject:kEmptyString forKey: kParamCountryId];
    [AdressValidatorDict setObject:zipTxtFd.text forKey: kParamZip];
    [AdressValidatorDict setObject:[self getOriginalPhonestringFromFormated:phoneNumberTxtfd.text] forKey: kParamPhone];
    
    [[ModelManager modelManager] GetAddressValidator:AdressValidatorDict successStatus:^(APIResponseStatusCode statusCode, id response) {
      //  NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            DLog(@"response----%@ ",response);
            AdressValidatorString=[response valueForKey:@"Status"];
            if ([OrderTypeString isEqualToString:kStringDeliver]) {
                if ([AdressValidatorString intValue]==1) {
                    [self callApiGetAuthorizeCredentialsEncrypt];
                }
                else{
                    [self showAlertViewWithTitle:nil message:[response valueForKey:@"Message"]];
                }
            }
            else{
                [self callApiGetAuthorizeCredentialsEncrypt];
            }
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [UIView showMessageWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage] showInterval:5.0  viewController:self];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}
-(void) callApivalidateAddressAsUserEnterAddress    {
    [self showSpinner1]; // To show spinner
    
    if(!AdressValidatorDict)    {
        AdressValidatorDict = [NSMutableDictionary dictionary];
    }
    else    {
        [AdressValidatorDict removeAllObjects];
    }
    User *userObj=kGetUserModel;
    [AdressValidatorDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [AdressValidatorDict setObject:userObj.userId forKey: kParamUserId];
    [AdressValidatorDict setObject:firstNameTxtfd.text forKey: kParamFirstName];
    [AdressValidatorDict setObject:lastNameTxtfd.text forKey: kParamLastName];
    [AdressValidatorDict setObject:addressTxtFd.text forKey: kParamAddress];
    [AdressValidatorDict setObject:apartmentTxtFd.text forKey: kParamApartment];
    [AdressValidatorDict setObject:cityTxtFd.text forKey: kParamCity];
    [AdressValidatorDict setObject:stateTxtFd.text forKey: kParamState];
    [AdressValidatorDict setObject:kEmptyString forKey: kParamCountryId];
    [AdressValidatorDict setObject:zipTxtFd.text forKey: kParamZip];
    [AdressValidatorDict setObject:phoneNumberTxtfd.text forKey: kParamPhone];
    
    [[ModelManager modelManager] GetAddressValidator:AdressValidatorDict successStatus:^(APIResponseStatusCode statusCode, id response) {
        [self hideSpinner];
        AdressValidatorString=[response valueForKey:@"Status"];
        if ([AdressValidatorString intValue]==1) {
            //[self showAlertViewWithTitle:@"We can deliver to this address." message:[response valueForKey:@"Message"]];
              [self showAlertViewWithTitle:nil message:@"We can deliver to this address."];
        }
        else{
            [self showAlertViewWithTitle:nil message:[response valueForKey:@"Message"]];
        }
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [UIView showMessageWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage] showInterval:5.0  viewController:self];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}

-(void) callApivalidateCouponCode    {
    [self showSpinner1]; // To show spinner
    
    if(!CouponCodeValidatorDict)    {
        CouponCodeValidatorDict = [NSMutableDictionary dictionary];
    }
    else    {
        [CouponCodeValidatorDict removeAllObjects];
    }
    User *userObj=kGetUserModel;
    [CouponCodeValidatorDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [CouponCodeValidatorDict setObject:userObj.userId forKey: kParamUserId];
    [CouponCodeValidatorDict setObject:[NSNumber numberWithDouble:itemCost] forKey: kParamOrderValue];
    if(couponCodeTxtFd.text!=nil){
    [CouponCodeValidatorDict setObject:couponCodeTxtFd.text forKey: kParamCouponCode];
    }
      [[ModelManager modelManager] GetCouponCode:CouponCodeValidatorDict successStatus:^(APIResponseStatusCode statusCode, id response) {
        [self hideSpinner];
        CouponCodeValidatorString=[response valueForKey:@"Result"];
          orderTotalArray=response;
          NSLog(@"Order Total Array = %@",orderTotalArray);
        if ([CouponCodeValidatorString isEqualToString:@"Success"]) {
            [self showAlertViewWithTitle:@"CouponCode Successfully Applied" message:[response valueForKey:@"ErrorMessage"]];
            [orderSummaryItems insertObject:@"Discount" atIndex:1];
            [orderSummaryValues insertObject:[response valueForKey:@"CouponFlatDiscount"] atIndex:1];
            couponDiscount =  [[response valueForKey:@"CouponFlatDiscount"] floatValue];
            isCouponApplied=YES;
            [self.defaultTableView reloadData];
            
        }
        else{
            [self showAlertViewWithTitle:nil message:[response valueForKey:@"ErrorMessage"]];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:orderTotalArray];
            [dict mutableCopy];
            [dict setObject:@"0.00" forKey:@"CouponFlatDiscount"];
            orderTotalArray = dict;
            NSLog(@"OrderTotalArray  = %@",orderTotalArray);
          }
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [UIView showMessageWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage] showInterval:5.0  viewController:self];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}

-(void)callApiAddorderWithAuthArray:(NSArray*)AuthArray withPaymentType:(NSString*)paymentType  {
    
    __weak User *userObj=kGetUserModel;
    __weak UserProfile *userprofileObj=kgetUserProfileModel;
    /***   Dynamic date time values        ****/
    NSString *isSaveString;
    DLog(@"IsProfileUpdated=%@ isSaveString=%@",userprofileObj.IsProfileUpdated,isSaveString);
    if ([userprofileObj.IsProfileUpdated isEqualToString:@"1"]) {
        isSaveString=@"0";
    }
    else{
        isSaveString=@"1";
    
    }
    NSString *dateStr = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                       dateStyle:NSDateFormatterShortStyle
                                                       timeStyle:NSDateFormatterFullStyle];    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter1 dateFromString:dateStr];
   // NSLog(@"date : %@",dateStr);
    
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:date];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"dd-MMM-yyyy hh:mm"];
    [dateFormatters setDateStyle:NSDateFormatterShortStyle];
    [dateFormatters setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatters setDoesRelativeDateFormatting:YES];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy hh:mm:ss a"];
   
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *currentTime = [dateFormatter stringFromDate:[NSDate date]];
    DLog(@"currentGMTOffset---%@",[NSString stringWithFormat:@"%ld",(long)currentGMTOffset]);
    DLog(@"currentTimeZone----%@",[NSString stringWithFormat:@"%@",currentTimeZone]);
    DLog(@"currentTimeZone----%@",[NSString stringWithFormat:@"%@",[currentTimeZone abbreviationForDate:[NSDate date]]]);
    DLog(@"currentTime--------%@",[NSString stringWithFormat:@"%@",currentTime]);

    /*****************************************************************/
    /* Kindly add a new tag "IsProfileAddress" in "AddOrder" api, which will be helpful in identifying between profile address or new address at server side.
    "IsProfileAddress" =1(if profile address)
    "IsProfileAddress" =0(if new address instead of profile address)*/
    //isProfileAddressString is used
    /*****************************************************************/
    
    NSString *isProfileAddressString = @"1";
    
    __block CustomerAddressType *deliveryAddressDetails = [CustomerAddressType customerAddressType];
    if(AdressValidatorDict && [AdressValidatorDict objectForKey: @"Address"] && [AdressValidatorDict objectForKey: @"Zip"]) {
        deliveryAddressDetails.firstName         = [AdressValidatorDict objectForKey:@"FirstName"];
        deliveryAddressDetails.lastName          = [AdressValidatorDict objectForKey:@"LastName"];
        deliveryAddressDetails.phoneNumber       = [AdressValidatorDict objectForKey:@"Phone"];
        deliveryAddressDetails.address           = [AdressValidatorDict objectForKey:@"Address"];
        deliveryAddressDetails.address2          = [AdressValidatorDict objectForKey:@"Address2"];
        deliveryAddressDetails.state             = [AdressValidatorDict objectForKey:@"State"];
        deliveryAddressDetails.city              = [AdressValidatorDict objectForKey:@"City"];
        deliveryAddressDetails.country           = [AdressValidatorDict objectForKey:@"CountryId"];
        deliveryAddressDetails.zip               = [AdressValidatorDict objectForKey:@"Zip"];
        
        isProfileAddressString = @"0";
    }
    else    {
        deliveryAddressDetails.firstName         = userprofileObj.FirstName;
        deliveryAddressDetails.lastName          = userprofileObj.LastName;
        deliveryAddressDetails.phoneNumber       = userprofileObj.ContactNo;
        deliveryAddressDetails.address           = userprofileObj.Address;
        deliveryAddressDetails.address2           = userprofileObj.Address2;
        deliveryAddressDetails.state             = userprofileObj.State;
        deliveryAddressDetails.city              = userprofileObj.City;
        deliveryAddressDetails.country           = userprofileObj.CountryName;
        deliveryAddressDetails.zip               = userprofileObj.ZipCode;
        
        isProfileAddressString = @"1";
    }
    
    DLog(@"currentGMTOffset---%@",[NSString stringWithFormat:@"%ld",(long)currentGMTOffset]);
    DLog(@"currentTimeZone----%@",[NSString stringWithFormat:@"%@",currentTimeZone]);
    DLog(@"currentTimeZone----%@",[NSString stringWithFormat:@"%@",[currentTimeZone abbreviationForDate:[NSDate date]]]);
    DLog(@"currentTime--------%@",[NSString stringWithFormat:@"%@",currentTime]);
    DLog(@"deliveryTimeFromString=%@ deliveryTimeToString=%@ deliveryDateString=%@",deliveryTimeFromString,deliveryTimeToString,deliveryDateString);
    NSDictionary * dict = [NSDictionary new];
    if ([OrderTypeString isEqualToString:kStringPickUp]) {
        deliveryDateString=kEmptyString;
        deliveryTimeFromString=kEmptyString;
        deliveryTimeFromString=kEmptyString;
    }

    if ([paymentType isEqualToString:kStringPayAtStore]) {
        
        
        
        DLog(@"deliveryDateString=%@ DeliveryTimeFrom=%@ DeliveryTimeFrom=%@",deliveryDateString,deliveryTimeFromString,deliveryTimeFromString);
       if (![CouponCodeValidatorString isEqualToString:@"Success"] && couponCode==NO){
        
        dict = @{ @"AuthenticationId"               : userObj.userId,
                  @"IsProfileAddress"               : isProfileAddressString,
                  @"DeviceTime"                     : [NSString stringWithFormat:@"%@",currentTime],
                  @"DeviceTimeZoneAbbreviation"     : [NSString stringWithFormat:@"%@",[currentTimeZone abbreviationForDate:[NSDate date]]],
                  @"DeviceTimeZoneName"             : [NSString stringWithFormat:@"%@",currentTimeZone],
                  @"DeviceTimeZoneOffset"           : [NSString stringWithFormat:@"%ld",(long)currentGMTOffset],
                  @"IsSuccessOrder"                 : @"1",
                  @"PaymentDetails" : @[ @{ @"Address"                  : @"",
                                            @"Address2"                  : @"",
                                            @"CardExpiry"               :@"",
                                            @"CardMemberName"           : @"",
                                            @"CavvResultCode"           : kEmptyString,
                                            @"City"                     : @"",
                                            @"CountryId"                : @"",
                                            @"FirstName"                : @"",
                                            @"LastName"                 : @"",
                                            @"Phone"                    : @"",
                                            @"State"                    : @"",
                                            @"Transaction"              : @"",
                                            @"Zip"                      : @"",
                                            @"accountNumber"            : kEmptyString,
                                            @"accounttype"              : kEmptyString,
                                            @"authcode"                 : kEmptyString,
                                            @"avsResultCode"            : kEmptyString,
                                            @"code"                     : @"",
                                            @"cvvResultCode"            : @"",
                                            @"description"              : @"",
                                            @"message"                  : @"[I00001]",
                                            @"referenceTranscationID"   : kEmptyString,
                                            @"responsecode"             : @"APPROVED_1",
                                            @"testrequest"              : @"",
                                            @"transHash"                : kEmptyString,
                                             @"IsSave"                  : isSaveString,
                                            @"transactionID"            : kEmptyString
                                            } ],
                  @"StoreId"            : kSTOREIDbottlecappsString,
                  @"DeliveryCharges"    : @"0.00",
                  @"TotalTax"           : [orderSummaryValues objectAtIndex:1],
                  @"UniqueId"           : [self GetUUID],
                  @"UserRemarks"        : self.specialInstruction.text,
                  @"items"              : orderArray,
                  @"DeliveryDate"       : deliveryDateString,
                  @"DeliveryTimeFrom"   : deliveryTimeFromString,
                  @"DeliveryTimeTo"     : deliveryTimeToString,
                


                  };
        
       }
        else if ([CouponCodeValidatorString isEqualToString:@"Success"] && couponCode==NO){
            dict = @{ @"AuthenticationId"               : userObj.userId,
                      @"IsProfileAddress"               : isProfileAddressString,
                      @"DeviceTime"                     : [NSString stringWithFormat:@"%@",currentTime],
                      @"DeviceTimeZoneAbbreviation"     : [NSString stringWithFormat:@"%@",[currentTimeZone abbreviationForDate:[NSDate date]]],
                      @"DeviceTimeZoneName"             : [NSString stringWithFormat:@"%@",currentTimeZone],
                      @"DeviceTimeZoneOffset"           : [NSString stringWithFormat:@"%ld",(long)currentGMTOffset],
                      @"IsSuccessOrder"                 : @"1",
                      @"PaymentDetails" : @[ @{ @"Address"                  : @"",
                                                @"Address2"                  : @"",
                                                @"CardExpiry"               : @"",
                                                @"CardMemberName"           : @"",
                                                @"CavvResultCode"           : kEmptyString,
                                                @"City"                     : @"",
                                                @"CountryId"                : @"",
                                                @"FirstName"                : @"",
                                                @"LastName"                 : @"",
                                                @"Phone"                    : @"",
                                                @"State"                    : @"",
                                                @"Transaction"              : @"",
                                                @"Zip"                      : @"",
                                                @"accountNumber"            : kEmptyString,
                                                @"accounttype"              : kEmptyString,
                                                @"authcode"                 : kEmptyString,
                                                @"avsResultCode"            : kEmptyString,
                                                @"code"                     : @"",
                                                @"cvvResultCode"            : @"",
                                                @"description"              : @"",
                                                @"message"                  : @"[I00001]",
                                                @"referenceTranscationID"   : kEmptyString,
                                                @"responsecode"             : @"APPROVED_1",
                                                @"testrequest"              : @"",
                                                @"transHash"                : kEmptyString,
                                                @"IsSave"                  : isSaveString,
                                                @"transactionID"            : kEmptyString
                                                } ],
                      @"StoreId"            : kSTOREIDbottlecappsString,
                      @"DeliveryCharges"    : @"0.00",
                      @"TotalTax"           : [orderSummaryValues objectAtIndex:1],
                      @"UniqueId"           : [self GetUUID],
                      @"UserRemarks"        : self.specialInstruction.text,
                      @"items"              : orderArray,
                      @"DeliveryDate"       : deliveryDateString,
                      @"DeliveryTimeFrom"   : deliveryTimeFromString,
                      @"DeliveryTimeTo"     : deliveryTimeToString,
                      
                      
                      
                      };
        
        }
        else{
            if(couponCodeTxtFd.text!=nil){
            dict = @{ @"AuthenticationId"               : userObj.userId,
                      @"IsProfileAddress"               : isProfileAddressString,
                      @"DeviceTime"                     : [NSString stringWithFormat:@"%@",currentTime],
                      @"DeviceTimeZoneAbbreviation"     : [NSString stringWithFormat:@"%@",[currentTimeZone abbreviationForDate:[NSDate date]]],
                      @"DeviceTimeZoneName"             : [NSString stringWithFormat:@"%@",currentTimeZone],
                      @"DeviceTimeZoneOffset"           : [NSString stringWithFormat:@"%ld",(long)currentGMTOffset],
                      @"IsSuccessOrder"                 : @"1",
                      @"PaymentDetails" : @[ @{ @"Address"                  : @"",
                                                @"Address2"                  : @"",
                                                @"CardExpiry"               : @"",
                                                @"CardMemberName"           : @"",
                                                @"CavvResultCode"           : kEmptyString,
                                                @"City"                     : @"",
                                                @"CountryId"                : @"",
                                                @"FirstName"                : @"",
                                                @"LastName"                 : @"",
                                                @"Phone"                    : @"",
                                                @"State"                    : @"",
                                                @"Transaction"              : @"",
                                                @"Zip"                      : @"",
                                                @"accountNumber"            : kEmptyString,
                                                @"accounttype"              : kEmptyString,
                                                @"authcode"                 : kEmptyString,
                                                @"avsResultCode"            : kEmptyString,
                                                @"code"                     : @"",
                                                @"cvvResultCode"            : @"",
                                                @"description"              : @"",
                                                @"message"                  : @"[I00001]",
                                                @"referenceTranscationID"   : kEmptyString,
                                                @"responsecode"             : @"APPROVED_1",
                                                @"testrequest"              : @"",
                                                @"transHash"                : kEmptyString,
                                                @"IsSave"                  : isSaveString,
                                                @"transactionID"            : kEmptyString
                                                } ],
                      @"StoreId"            : kSTOREIDbottlecappsString,
                      @"DeliveryCharges"    : @"0.00",
                      @"TotalTax"           : [orderSummaryValues objectAtIndex:2],
                      @"UniqueId"           : [self GetUUID],
                      @"UserRemarks"        : self.specialInstruction.text,
                      @"items"              : orderArray,
                      @"CouponId"           : [orderTotalArray valueForKey:@"CouponId"],
                      @"CouponCode"         : [orderTotalArray valueForKey:@"CouponCode"],
                      @"CouponDiscountAmount"     :[orderTotalArray valueForKey:@"CouponFlatDiscount"],
                      @"DeliveryDate"       : deliveryDateString,
                      @"DeliveryTimeFrom"   : deliveryTimeFromString,
                      @"DeliveryTimeTo"     : deliveryTimeToString,
                      
                      
                      
                      };
            

        }
            else{
                 [self showAlertViewWithTitle:@"Message" message:@"Please select Coupon code"];
            }
        
        }
        
        /*************************orderSummaryValues values**********************************
         36.49,  // total item cost
         2.92,   // tax
         39.41   // order total
         *************************orderSummaryValues values**********************************/
    }
    else if( payTypeString ==kStringPayNow && ([OrderTypeString isEqualToString: kStringPickUp]))   {
        DLog(@"deliveryDateString=%@ DeliveryTimeFrom=%@ DeliveryTimeFrom=%@",deliveryDateString,deliveryTimeFromString,deliveryTimeFromString);
       if (![CouponCodeValidatorString isEqualToString:@"Success"] && couponCode ==NO){
       dict = @{ @"AuthenticationId"               : userObj.userId,
                  @"IsProfileAddress"               : isProfileAddressString,
                  @"DeviceTime"                     : [NSString stringWithFormat:@"%@",currentTime],
                  @"DeviceTimeZoneAbbreviation"     : [NSString stringWithFormat:@"%@",[currentTimeZone abbreviationForDate:[NSDate date]]],
                  @"DeviceTimeZoneName"             : [NSString stringWithFormat:@"%@",currentTimeZone],
                  @"DeviceTimeZoneOffset"           : [NSString stringWithFormat:@"%ld",(long)currentGMTOffset],
                  @"IsSuccessOrder"                 : @"1",
                  @"PaymentDetails" : @[ @{ @"Address"                  : @"",
                                            @"Address2"                  : @"",
                                            @"CardExpiry"               : monthTextfd.text,
                                            @"CardMemberName"           : @"",
                                            @"CavvResultCode"           : transResponse.cvvResultCode,
                                            @"City"                     : @"",
                                            @"CountryId"                : @"",
                                            @"FirstName"                : @"",
                                            @"LastName"                 : @"",
                                            @"Phone"                    : @"",
                                            @"State"                    : @"",
                                            @"Transaction"              : @"",
                                            @"Zip"                      : @"",
                                            @"accountNumber"            : transResponse.accountNumber,
                                            @"accounttype"              : transResponse.accountType,
                                            @"authcode"                 : transResponse.authCode,
                                            @"avsResultCode"            : transResponse.avsResultCode,
                                            @"code"                     : @"",
                                            @"cvvResultCode"            : @"",
                                            @"description"              : @"",
                                            @"message"                  : @"[I00001]",
                                            @"referenceTranscationID"   : transResponse.refTransID,
                                             @"IsSave"             : isSaveString,
                                            @"responsecode"             : @"APPROVED_1",
                                            @"testrequest"              : @"",
                                            @"transHash"                : transResponse.transHash,
                                            @"transactionID"            : transResponse.transId
                                            }],
                  @"StoreId"            : kSTOREIDbottlecappsString,
                  @"DeliveryCharges"    : @"0.00",
                  @"TotalTax"           : [orderSummaryValues objectAtIndex:1],
                  @"UniqueId"           : [self GetUUID],
                  @"UserRemarks"        : self.specialInstruction.text,
                  @"items"              : orderArray,
                  @"DeliveryDate"       : deliveryDateString,
                  @"DeliveryTimeFrom"   : deliveryTimeFromString,
                  @"DeliveryTimeTo"     : deliveryTimeToString


                  };
        }
        
       else if ([CouponCodeValidatorString isEqualToString:@"Success"] && couponCode ==YES){
           dict = @{ @"AuthenticationId"               : userObj.userId,
                     @"IsProfileAddress"               : isProfileAddressString,
                     @"DeviceTime"                     : [NSString stringWithFormat:@"%@",currentTime],
                     @"DeviceTimeZoneAbbreviation"     : [NSString stringWithFormat:@"%@",[currentTimeZone abbreviationForDate:[NSDate date]]],
                     @"DeviceTimeZoneName"             : [NSString stringWithFormat:@"%@",currentTimeZone],
                     @"DeviceTimeZoneOffset"           : [NSString stringWithFormat:@"%ld",(long)currentGMTOffset],
                     @"IsSuccessOrder"                 : @"1",
                     @"PaymentDetails" : @[ @{ @"Address"                  : @"",
                                               @"Address2"                  : @"",
                                               @"CardExpiry"               : monthTextfd.text,
                                               @"CardMemberName"           : @"",
                                               @"CavvResultCode"           : transResponse.cvvResultCode,
                                               @"City"                     : @"",
                                               @"CountryId"                : @"",
                                               @"FirstName"                : @"",
                                               @"LastName"                 : @"",
                                               @"Phone"                    : @"",
                                               @"State"                    : @"",
                                               @"Transaction"              : @"",
                                               @"Zip"                      : @"",
                                               @"accountNumber"            : transResponse.accountNumber,
                                               @"accounttype"              : transResponse.accountType,
                                               @"authcode"                 : transResponse.authCode,
                                               @"avsResultCode"            : transResponse.avsResultCode,
                                               @"code"                     : @"",
                                               @"cvvResultCode"            : @"",
                                               @"description"              : @"",
                                               @"message"                  : @"[I00001]",
                                               @"referenceTranscationID"   : transResponse.refTransID,
                                               @"IsSave"             : isSaveString,
                                               @"responsecode"             : @"APPROVED_1",
                                               @"testrequest"              : @"",
                                               @"transHash"                : transResponse.transHash,
                                               @"transactionID"            : transResponse.transId
                                               }],
                     @"StoreId"            : kSTOREIDbottlecappsString,
                     @"DeliveryCharges"    : @"0.00",
                     @"CouponId"           : [orderTotalArray valueForKey:@"CouponId"],
                     @"CouponCode"         : [orderTotalArray valueForKey:@"CouponCode"],
                     @"CouponDiscountAmount"     :[orderTotalArray valueForKey:@"CouponFlatDiscount"],
                     @"TotalTax"           : [orderSummaryValues objectAtIndex:1],
                     @"UniqueId"           : [self GetUUID],
                     @"UserRemarks"        : self.specialInstruction.text,
                     @"items"              : orderArray,
                     @"DeliveryDate"       : deliveryDateString,
                     @"DeliveryTimeFrom"   : deliveryTimeFromString,
                     @"DeliveryTimeTo"     : deliveryTimeToString
                     
                     
                     };

           
       }
        else
        {
            dict = @{ @"AuthenticationId"               : userObj.userId,
                      @"IsProfileAddress"               : isProfileAddressString,
                      @"DeviceTime"                     : [NSString stringWithFormat:@"%@",currentTime],
                      @"DeviceTimeZoneAbbreviation"     : [NSString stringWithFormat:@"%@",[currentTimeZone abbreviationForDate:[NSDate date]]],
                      @"DeviceTimeZoneName"             : [NSString stringWithFormat:@"%@",currentTimeZone],
                      @"DeviceTimeZoneOffset"           : [NSString stringWithFormat:@"%ld",(long)currentGMTOffset],
                      @"IsSuccessOrder"                 : @"1",
                      @"PaymentDetails" : @[ @{ @"Address"                  : @"",
                                                @"Address2"                  : @"",
                                                @"CardExpiry"               : monthTextfd.text,
                                                @"CardMemberName"           : @"",
                                                @"CavvResultCode"           : transResponse.cvvResultCode,
                                                @"City"                     : @"",
                                                @"CountryId"                : @"",
                                                @"FirstName"                : @"",
                                                @"LastName"                 : @"",
                                                @"Phone"                    : @"",
                                                @"State"                    : @"",
                                                @"Transaction"              : @"",
                                                @"Zip"                      : @"",
                                                @"accountNumber"            : transResponse.accountNumber,
                                                @"accounttype"              : transResponse.accountType,
                                                @"authcode"                 : transResponse.authCode,
                                                @"avsResultCode"            : transResponse.avsResultCode,
                                                @"code"                     : @"",
                                                @"cvvResultCode"            : @"",
                                                @"description"              : @"",
                                                @"message"                  : @"[I00001]",
                                                @"referenceTranscationID"   : transResponse.refTransID,
                                                @"IsSave"             : isSaveString,
                                                @"responsecode"             : @"APPROVED_1",
                                                @"testrequest"              : @"",
                                                @"transHash"                : transResponse.transHash,
                                                @"transactionID"            : transResponse.transId
                                                }],
                      @"StoreId"            : kSTOREIDbottlecappsString,
                      @"DeliveryCharges"    : @"0.00",
                      @"CouponId"           : [orderTotalArray valueForKey:@"CouponId"],
                      @"CouponCode"         : [orderTotalArray valueForKey:@"CouponCode"],
                      @"CouponDiscountAmount"     :[orderTotalArray valueForKey:@"CouponFlatDiscount"],
                      @"TotalTax"           : [orderSummaryValues objectAtIndex:1],
                      @"UniqueId"           : [self GetUUID],
                      @"UserRemarks"        : self.specialInstruction.text,
                      @"items"              : orderArray,
                      @"DeliveryDate"       : deliveryDateString,
                      @"DeliveryTimeFrom"   : deliveryTimeFromString,
                      @"DeliveryTimeTo"     : deliveryTimeToString
                      
                      
                      };

        }
    }
    else    {
        
         if (![CouponCodeValidatorString isEqualToString:@"Success"]&& couponCode==NO){
        dict = @{ @"AuthenticationId"               : userObj.userId,
                  @"IsProfileAddress"               : isProfileAddressString,
                  @"DeviceTime"                     : [NSString stringWithFormat:@"%@",currentTime],
                  @"DeviceTimeZoneAbbreviation"     : [NSString stringWithFormat:@"%@",[currentTimeZone abbreviationForDate:[NSDate date]]],
                  @"DeviceTimeZoneName"             : [NSString stringWithFormat:@"%@",currentTimeZone],
                  @"DeviceTimeZoneOffset"           : [NSString stringWithFormat:@"%ld",(long)currentGMTOffset],
                  @"IsSuccessOrder"                 : @"1",
                  
                  @"PaymentDetails" : @[ @{ @"Address"                  : deliveryAddressDetails.address,
                                            @"CardExpiry"               : monthTextfd.text,
                                            @"CardMemberName"           : @"",
                                            @"CavvResultCode"           : transResponse.cvvResultCode,
                                            @"City"                     : deliveryAddressDetails.city,
                                            @"CountryId"                : deliveryAddressDetails.country,
                                            @"FirstName"                : deliveryAddressDetails.firstName,
                                            @"LastName"                 : deliveryAddressDetails.lastName,
                                            @"Phone"                    : deliveryAddressDetails.phoneNumber,
                                            @"Address2"                 : deliveryAddressDetails.address2,
                                            @"State"                    : deliveryAddressDetails.state,
                                            @"Transaction"              : @"",
                                            @"Zip"                      : deliveryAddressDetails.zip,
                                            @"accountNumber"            : transResponse.accountNumber,
                                            @"accounttype"              : transResponse.accountType,
                                            @"authcode"                 : transResponse.authCode,
                                            @"avsResultCode"            : transResponse.avsResultCode,
                                            @"code"                     : @"",
                                            @"cvvResultCode"            : @"",
                                            @"description"              : @"",
                                            @"message"                  : @"[I00001]",
                                            @"referenceTranscationID"   : transResponse.refTransID,
                                             @"IsSave"                  : isSaveString,
                                            @"responsecode"             : @"APPROVED_1",
                                            @"testrequest"              : @"",
                                            @"transHash"                : transResponse.transHash,
                                            @"transactionID"            : transResponse.transId
                                            }],
                  @"StoreId"            : kSTOREIDbottlecappsString,
                  @"DeliveryCharges"    : [orderSummaryValues objectAtIndex:1],
                  @"TotalTax"           : [orderSummaryValues objectAtIndex:3],
                  @"UniqueId"           : [self GetUUID],
                  @"UserRemarks"        : self.specialInstruction.text,
                  @"items"              : orderArray,
                  @"DeliveryDate"       : deliveryDateString,
                  @"DeliveryTimeFrom"   : deliveryTimeFromString,
                  @"DeliveryTimeTo"     : deliveryTimeToString
                  

                  };
        }
        
         else if ([CouponCodeValidatorString isEqualToString:@"Success"]&& couponCode==NO){
             dict = @{ @"AuthenticationId"               : userObj.userId,
                       @"IsProfileAddress"               : isProfileAddressString,
                       @"DeviceTime"                     : [NSString stringWithFormat:@"%@",currentTime],
                       @"DeviceTimeZoneAbbreviation"     : [NSString stringWithFormat:@"%@",[currentTimeZone abbreviationForDate:[NSDate date]]],
                       @"DeviceTimeZoneName"             : [NSString stringWithFormat:@"%@",currentTimeZone],
                       @"DeviceTimeZoneOffset"           : [NSString stringWithFormat:@"%ld",(long)currentGMTOffset],
                       @"IsSuccessOrder"                 : @"1",
                       
                       @"PaymentDetails" : @[ @{ @"Address"                  : deliveryAddressDetails.address,
                                                 @"CardExpiry"               : monthTextfd.text,
                                                 @"CardMemberName"           : @"",
                                                 @"CavvResultCode"           : transResponse.cvvResultCode,
                                                 @"City"                     : deliveryAddressDetails.city,
                                                 @"CountryId"                : deliveryAddressDetails.country,
                                                 @"FirstName"                : deliveryAddressDetails.firstName,
                                                 @"LastName"                 : deliveryAddressDetails.lastName,
                                                 @"Phone"                    : deliveryAddressDetails.phoneNumber,
                                                 @"Address2"                 : deliveryAddressDetails.address2,
                                                 @"State"                    : deliveryAddressDetails.state,
                                                 @"Transaction"              : @"",
                                                 @"Zip"                      : deliveryAddressDetails.zip,
                                                 @"accountNumber"            : transResponse.accountNumber,
                                                 @"accounttype"              : transResponse.accountType,
                                                 @"authcode"                 : transResponse.authCode,
                                                 @"avsResultCode"            : transResponse.avsResultCode,
                                                 @"code"                     : @"",
                                                 @"cvvResultCode"            : @"",
                                                 @"description"              : @"",
                                                 @"message"                  : @"[I00001]",
                                                 @"referenceTranscationID"   : transResponse.refTransID,
                                                 @"IsSave"                  : isSaveString,
                                                 @"responsecode"             : @"APPROVED_1",
                                                 @"testrequest"              : @"",
                                                 @"transHash"                : transResponse.transHash,
                                                 @"transactionID"            : transResponse.transId
                                                 }],
                       @"StoreId"            : kSTOREIDbottlecappsString,
                       @"DeliveryCharges"    : [orderSummaryValues objectAtIndex:1],
                       @"TotalTax"           : [orderSummaryValues objectAtIndex:3],
                       @"UniqueId"           : [self GetUUID],
                       @"UserRemarks"        : self.specialInstruction.text,
                       @"items"              : orderArray,
                       @"DeliveryDate"       : deliveryDateString,
                       @"DeliveryTimeFrom"   : deliveryTimeFromString,
                       @"DeliveryTimeTo"     : deliveryTimeToString
                       };
         }
        else{
            dict = @{ @"AuthenticationId"               : userObj.userId,
                      @"IsProfileAddress"               : isProfileAddressString,
                      @"DeviceTime"                     : [NSString stringWithFormat:@"%@",currentTime],
                      @"DeviceTimeZoneAbbreviation"     : [NSString stringWithFormat:@"%@",[currentTimeZone abbreviationForDate:[NSDate date]]],
                      @"DeviceTimeZoneName"             : [NSString stringWithFormat:@"%@",currentTimeZone],
                      @"DeviceTimeZoneOffset"           : [NSString stringWithFormat:@"%ld",(long)currentGMTOffset],
                      @"IsSuccessOrder"                 : @"1",
                      
                      @"PaymentDetails" : @[ @{ @"Address"                  : deliveryAddressDetails.address,
                                                @"CardExpiry"               : monthTextfd.text,
                                                @"CardMemberName"           : @"",
                                                @"CavvResultCode"           : transResponse.cvvResultCode,
                                                @"City"                     : deliveryAddressDetails.city,
                                                @"CountryId"                : deliveryAddressDetails.country,
                                                @"FirstName"                : deliveryAddressDetails.firstName,
                                                @"LastName"                 : deliveryAddressDetails.lastName,
                                                @"Phone"                    : deliveryAddressDetails.phoneNumber,
                                                @"Address2"                 : deliveryAddressDetails.address2,
                                                @"State"                    : deliveryAddressDetails.state,
                                                @"Transaction"              : @"",
                                                @"Zip"                      : deliveryAddressDetails.zip,
                                                @"accountNumber"            : transResponse.accountNumber,
                                                @"accounttype"              : transResponse.accountType,
                                                @"authcode"                 : transResponse.authCode,
                                                @"avsResultCode"            : transResponse.avsResultCode,
                                                @"code"                     : @"",
                                                @"cvvResultCode"            : @"",
                                                @"description"              : @"",
                                                @"message"                  : @"[I00001]",
                                                @"referenceTranscationID"   : transResponse.refTransID,
                                                @"IsSave"                  : isSaveString,
                                                @"responsecode"             : @"APPROVED_1",
                                                @"testrequest"              : @"",
                                                @"transHash"                : transResponse.transHash,
                                                @"transactionID"            : transResponse.transId
                                                }],
                      @"StoreId"            : kSTOREIDbottlecappsString,
                      @"DeliveryCharges"    : [orderSummaryValues objectAtIndex:2],
                      @"TotalTax"           : [orderSummaryValues objectAtIndex:4],
                      @"CouponId"           : [orderTotalArray valueForKey:@"CouponId"],
                      @"CouponCode"         : [orderTotalArray valueForKey:@"CouponCode"],
                      @"CouponDiscountAmount"     :[orderTotalArray valueForKey:@"CouponFlatDiscount"],
                      @"UniqueId"           : [self GetUUID],
                      @"UserRemarks"        : self.specialInstruction.text,
                      @"items"              : orderArray,
                      @"DeliveryDate"       : deliveryDateString,
                      @"DeliveryTimeFrom"   : deliveryTimeFromString,
                      @"DeliveryTimeTo"     : deliveryTimeToString
                      
                      
                      };

        }
        /*************************orderSummaryValues values**********************************
            22.49,  // total item cost
            10.00,  // delivery charges
            32.49,  // total before tax
            1.80,   // tax
            34.29   // order total
        *************************orderSummaryValues values**********************************/
    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                        
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //NSLog(@"Json string = %@",jsonString);
    [self showSpinner1]; // To show spinner
    [[ModelManager modelManager] addOrderWithJsonstring:jsonString  successStatus:^(APIResponseStatusCode statusCode, id response) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
           // NSLog(@"%@", response);
            [self showAlertViewWithTitle:nil message:kAlertPlaceOrderSucess];
            //getAddorderrespnseDict
            OrderConfirmationViewController *vc = nil;
            vc= [self viewControllerWithIdentifier:kOrderConfirmationViewControllerStoryboardId];
            vc.getAddorderrespnseDict=response;
            vc.getCartArray=getCartArray;
            vc.getConfirmationNo=transResponse.transId;
            vc.deliveryAddressDetails = deliveryAddressDetails;
            vc.OrderTypeString = OrderTypeString;
            vc.payTypeString = payTypeString;
            for (Product *product in getCartArray) {
                NSArray *temparray=[NSArray arrayWithObjects:product, nil];
                [[ShoppingCart sharedInstace] removeProductFromMyShoppingCart:temparray];
            }
            
            [self.navigationController pushViewController:vc animated:YES];
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", kInternalInconsistencyErrorMessage]];
       // NSLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        
    }];
}

- (void) validateCreditCardValue {
    NSString *ccNum = self.creditCardBuf;
    // Use the Authorize.Net SDK to validate credit card number
    if (![CreditCardType isValidCreditCardNumber:ccNum]) {
        cardnumberTextfd.textColor = [UIColor redColor];
    } else {
        cardnumberTextfd.textColor = [UIColor colorWithRed:FLOAT_COLOR_VALUE(98) green:FLOAT_COLOR_VALUE(169) blue:FLOAT_COLOR_VALUE(40) alpha:1];
    }
}

-(BOOL) validateAllCardDetails  {
    NSString *ccNum = self.creditCardBuf;
    if([CreditCardType isValidCreditCardNumber:ccNum])
        return YES;
    
    return NO;
}

-(void) shouldEnablePayNowBtn   {
    if(payNowButton)    {
        if([self validateAllCardDetails])   {
            [self payNowButtonEnabled:YES];
            [self payAtStoreButtonEnabled: NO];
        }
        else    {
            [self payNowButtonEnabled:NO];
            [self payAtStoreButtonEnabled:YES];
        }
    }
}

-(void)payNowButtonEnabled:(BOOL) isEnabled {
    if(isEnabled)   {
        payNowButton.alpha = 1.0;
        payNowButton.userInteractionEnabled = YES;
    }
    else    {
        payNowButton.alpha = 0.45;
        payNowButton.userInteractionEnabled = NO;
    }
}

-(void)payAtStoreButtonEnabled:(BOOL) isEnabled {
    if(isEnabled)   {
        payAtStoreButton.alpha = 1.0;
        
        payAtStoreButton.userInteractionEnabled = YES;
    }
    else    {
        payAtStoreButton.alpha = 0.45;
        payAtStoreButton.userInteractionEnabled = NO;

    }
}

- (void)initializeViews
{
    cardnumberTextfd.inputView = _keypad;
    monthTextfd.inputView = _keypad;
    cvvTextfd.inputView = _keypad;
    
    NSString *buf = [cardnumberTextfd.text stringByReplacingOccurrencesOfString:kSpace withString:@""];
    if(buf==nil)
        buf=@"";
    self.creditCardBuf = [NSString stringWithString:buf];
    
    buf = [monthTextfd.text stringByReplacingOccurrencesOfString:kSlash withString:@""];
    if(buf==nil)
        buf=@"";
    self.expirationBuf = [NSString stringWithString:buf];
    
    //TODO:  REMOVE AFTER TESTING
    [self validateCreditCardValue];
}

-(void)initializeAllArrayAndDict{
    //if ([CouponCodeValidatorString isEqualToString:@"Success"]) {
    //self. orderArray =[NSMutableArray new];
    dict_productQuantityToPurchaseWithID=[NSMutableDictionary new];
    ProductOutofStockArray=[NSMutableArray new];
    orderSummaryItems=[[NSMutableArray alloc] init];
    orderSummaryValues=[[NSMutableArray alloc] init];
    enterNewAddress=NO;
    couponCode=NO;
    AdressValidatorString=kEmptyString;
    self. cellItemsArray=[[NSMutableArray alloc] initWithObjects:@"Order Summary:",@"Enter a Delivery Adress:",@"Special Instructions: Payment Info:",nil];
    if (![OrderTypeString isEqualToString:kStringPickUp]) {
        orderSummaryItems=[NSMutableArray arrayWithObjects:@"Items Cost:",@"Delivery Charge:",@"Total Before Tax:",@"Tax:",@"Order Total:",nil];
        
    }
    else{
        orderSummaryItems=[NSMutableArray arrayWithObjects:@"Items Cost:",@"Tax:",@"Order Total:",nil];
        
    }
    specialInstructionString=kEmptyString;
    
}
#pragma mark -
#pragma mark DecimalKeypadViewDelegate
- (void)keypad:(DecimalKeypadView *)keypad keyPressed:(NSString *)string {
    
    if ([string isEqualToString:@"⌫"]) {
        string = @"";
    }
    
    if ([string isEqualToString:@"Cancel Transaction"]) {
        return;
    }
}
-(void)CreateUI{
    [self AddDefaultTableView];
    [self.defaultTableView setDelegate:self];
    [self.defaultTableView setDataSource:self];
    [self.defaultTableView reloadData];
}

/**This api will call in VIewdidload to get the IsEnablePaymentGateWay and Payatstore bool values to enable or disable the paynow and payatstore button*/
-(void)getEnableDisableButtonFromServer{
    NSMutableDictionary* paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [paramDict setObject:[[dict_productQuantityToPurchaseWithID allKeys] componentsJoinedByString:@","] forKey: kParamProductIds];
    
    [paramDict setObject:[[dict_productQuantityToPurchaseWithID allValues] componentsJoinedByString:@","] forKey: kParamProductQuantities];
    
    [paramDict setObject:[self GetUUID] forKey: kParamUniqueId];
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] GetAuthorizeCredentialsEncrypt:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
        // Delay execution of my block for 10 seconds.
      //  NSLog(@"Response  = %@",response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            [self payNowButtonEnabled:NO];
            [self payAtStoreButtonEnabled:NO];
            
            if(statusCode == Success)   {
                enablePayment = [[[response objectAtIndex:0]objectForKey:@"IsEnablePaymentGateWay"] boolValue];
                payStore = [[[response objectAtIndex:0]objectForKey:@"Payatstore"] boolValue];
               //  NSLog(@"Bool value: %d",enablePayment);
                [self CreateUI];
                if(payNowButton && [[[response objectAtIndex:0]objectForKey:@"IsEnablePaymentGateWay"] boolValue])  {
                    [self payNowButtonShouldEnable:YES];
                }
                else if(payNowButton)   {
                    [self payNowButtonShouldEnable:NO];
                }
                
                if(payAtStoreButton && [[[response objectAtIndex:0]objectForKey:@"Payatstore"] boolValue])
                    [self payAtStoreButtonEnabled:YES];
            }
            else    {
                [self showAlertViewWithTitle:kError message:kInternalInconsistencyErrorMessage];
                if(payNowButton)    {
                    [self payNowButtonShouldEnable:NO];
                }
                
                if(payAtStoreButton)
                    payAtStoreButton.enabled = NO;
            }
            
            if(payNowButton)    {
                if([self validateAllCardDetails])   {
                    [self payNowButtonEnabled:YES];
                }
                else    {
                    [self payNowButtonEnabled:NO];
                }
            }
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        if(payNowButton)    {
            [self payNowButtonShouldEnable:NO];
        }
        
        if(payAtStoreButton)
            [self payAtStoreButtonEnabled:NO];
    }];
}

-(void) payNowButtonShouldEnable:(BOOL) shouldEnable    {
    [self payNowButtonEnabled: shouldEnable];
    if(cardnumberTextfd)    {
        cardnumberTextfd.userInteractionEnabled = shouldEnable;
    }
    if(monthTextfd) {
        monthTextfd.userInteractionEnabled = shouldEnable;
    }
    if (cvvTextfd) {
        cvvTextfd.userInteractionEnabled = shouldEnable;
    }
    if(zipTextField)    {
        zipTextField.userInteractionEnabled = shouldEnable;
    }
}
-(void)CheckIfCouponCodeAvailable{
//    UserProfile *userProfileObj=kgetUserProfileModel;
//    if ([userProfileObj.Address isEqualToString:kEmptyString]||userProfileObj.Address ==nil) {
//        enterNewAddress=YES;
//    }
//    else{
//        enterNewAddress=NO;
//    }

}
-(void)CheckIfUserProfileISAvailable{
    UserProfile *userProfileObj=kgetUserProfileModel;
    if ([userProfileObj.Address isEqualToString:kEmptyString]||userProfileObj.Address ==nil) {
        enterNewAddress=YES;
    }
    else{
        enterNewAddress=NO;
    }

}
- (void)viewDidLoad {
    [super viewDidLoad];
    location = [[CLLocationManager alloc] init];
    location.delegate = self;
    [location startUpdatingLocation];
    stoploop=NO;
    isrestrict=NO;
    isCouponApplied=NO;
    applyButton.enabled=YES;
    applyButton.alpha=1;
    cardDeatailArray =[NSMutableArray arrayWithObjects:kEmptyString,kEmptyString,kEmptyString,kEmptyString, nil];
    deliveryDateAndTimeArray =[NSMutableArray arrayWithObjects:[self getCurrentDate],@"From",@"To", nil];
    deliveryTimeFromString=kEmptyString;
    deliveryTimeToString=kEmptyString;
    DLog(@"OrderTypeString--%@",OrderTypeString);
    orderTotalArray = [[NSMutableDictionary alloc]init];
   
        [self initializeViews];
        [self initializeAllArrayAndDict];
        [self GetProfileView];
    
       // [self getEnableDisableButtonFromServer];
    
        [CardIOUtilities preload];
        [self getEnableDisableButtonFromServer];
       // [self performSelector:@selector(getEnableDisableButtonFromServer) onThread:[NSThread mainThread] withObject:nil waitUntilDone:YES];
        // [self performSelector:@selector(CreateUI) onThread:[NSThread mainThread] withObject:nil waitUntilDone:YES];
    
        [self CheckIfUserProfileISAvailable];
   
    DLog(@"payTypeString==%@",payTypeString);
    DLog(@"OrderTypeString==%@",OrderTypeString);
    
    AppDelegate *appdelegateObj=(AppDelegate*)[[UIApplication sharedApplication] delegate];
    currTimStore = [NSString stringWithFormat:@"%@",appdelegateObj.currentTimeStore];
   // currTimStore =  @"5/05/2016 4:04:31 AM";
    //NSLog(@"current Time Store = %@",currTimStore);
    NSArray *dateAndTimeSepartorArray = [currTimStore componentsSeparatedByString:@" "];
  //  NSLog(@"Date and Time Array = %@",dateAndTimeSepartorArray);
    deliveryDateString = [dateAndTimeSepartorArray objectAtIndex:0];
    deliveryDateString = [deliveryDateString stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    timeMode = [dateAndTimeSepartorArray objectAtIndex:2];
    NSLog(@"time mode = %@",timeMode);
    
    if ([OrderTypeString isEqualToString:kStringDeliver]) {
        __weak ClosestStoreModel *     lClosestStoreModelObj = kGetClosestStoreModel;
        
        
        /*if open close time is not present we call this api*/
        if (lClosestStoreModelObj.GetStoredetails==nil) {
            [self lcallApiStoreDetails];
        }
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MM-dd-yyyy"];
        for (StoreOpenCloseTime * storeOpenTime in lClosestStoreModelObj.GetStoreOpenclose) {
            if ([storeOpenTime.DayID isEqualToString:[self GetWeekdayNameFromGivenDate:[NSDate date]]]) {
                DLog(@"StoreOpenTime=%@",storeOpenTime.StoreOpenTime);
                DLog(@"StoreCloseTime=%@",storeOpenTime.StoreCloseTime);
                fromTimeString = [self formatServerDateAndTime:currTimStore];
                [self getPickerArrayForStoreOpenTime:storeOpenTime.StoreOpenTime andStoreCloseTime:storeOpenTime.StoreCloseTime];
            }
            
        }
    }
    
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    if(locations.count>0){
        loc = [locations lastObject];
        NSLog(@"lat%f - lon%f", loc.coordinate.latitude, loc.coordinate.longitude);
    NSString *address = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true",loc.coordinate.latitude,loc.coordinate.longitude];
  //  NSString *address = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true",32.89473,-96.93959];
    [self getLocationFromAddressString:address];
    }
    else{
        [self showAlertViewWithTitle:@"Message" message:@"We are not able to find your current location right now,please try again!"];
    }
   
 }

-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr {
    double latitude = 0, longitude = 0;
   NSString *req = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true",loc.coordinate.latitude,loc.coordinate.longitude];
  // NSString *req = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true",32.89473,-96.93959];

    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    NSData* data = [result dataUsingEncoding:NSUTF8StringEncoding];
    NSError *e = nil;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
    
    if (!jsonArray)
    {
        NSLog(@"Error parsing JSON: %@", e);
    }
    else {
        NSArray *resultFromJson=[jsonArray valueForKey:@"results"];
        orderAddress=[[resultFromJson objectAtIndex:0]objectForKey:@"formatted_address"];
        orderAddress = [orderAddress stringByReplacingOccurrencesOfString:@", USA"
                                             withString:@""];
        
        NSArray *subStrings = [orderAddress componentsSeparatedByString:@","]; //or rather @" - "
        addresstotal = [NSString stringWithFormat:@"%@", [subStrings objectAtIndex:0] ];
        
//        if(subStrings.count>3){
//        if([subStrings objectAtIndex:3]!=nil || ![[subStrings objectAtIndex:3]isEqualToString:@""] ){
//
//        addresstotal = [NSString stringWithFormat:@"%@", [subStrings objectAtIndex:0] ];
//        }
//        }
//        else{
//             addresstotal = [NSString stringWithFormat:@"%@", [subStrings objectAtIndex:0]];
//        }
        
 //       addresstotal3 = [subStrings objectAtIndex:1];
        
            NSArray *innerArray=[[resultFromJson valueForKey:@"address_components"] objectAtIndex:0];
        for (NSDictionary *dict in innerArray){
            
            if([[[dict valueForKey:@"types"] objectAtIndex:0]isEqualToString:@"country"])
            {
                ordercountry=[dict valueForKey:@"long_name"];
                
            }
            if([[[dict valueForKey:@"types"] objectAtIndex:0]isEqualToString:@"postal_code"])
            {
                orderzipcode=[dict valueForKey:@"long_name"];
                
            }
            if([[[dict valueForKey:@"types"] objectAtIndex:0]isEqualToString:@"locality"])
            {
                ordercity=[dict valueForKey:@"long_name"];
                
                
            }
            if([[[dict valueForKey:@"types"] objectAtIndex:0]isEqualToString:@"administrative_area_level_1"])
            {
                orderstate=[dict valueForKey:@"long_name"];
                
                
            }
            
        }
        
        
        
    }
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude = longitude;
    
    NSLog(@"View Controller get Location Logitute : %f",center.latitude);
    NSLog(@"View Controller get Location Latitute : %f",center.longitude);
    [location stopUpdatingLocation];
    return center;
    
}

//This calculate delivery time by adding 2 hrs from the selected time
-(NSString*)addTimeIntervalInGivenTime:(NSString*)givenTime withtimeinterval:(int)hoursToAdd{
    NSString *datestring=[NSString stringWithFormat:@"%@ %@",deliveryDateString,givenTime];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:deliveryDateTimeFormat];
    
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:deliveryDateTimeFormat];
    
    NSDate *newdateAndTime = [dateFormat dateFromString:datestring];
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:deliveryTimeFormat];

  //  NSString *theDate = [dateFormater stringFromDate:now];
    NSString *theTime = [dateFormater stringFromDate:newdateAndTime];
   
    DLog(@"theTime===%@",theTime);
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setHour:hoursToAdd];
    NSDate *newDate= [calendar dateByAddingComponents:components toDate:newdateAndTime options:0];
    NSString *newTimeString;

    NSDateFormatter *dateFormater2 = [[NSDateFormatter alloc] init];
    [dateFormater2 setDateFormat:deliveryTimeFormat];
    if ([theTime isEqualToString:@"11:00 PM"]) {
       stoploop=YES;

    }
    else{
        stoploop=NO;
       // NSLog(@"newTimeString: %@",newTimeString);
    }
    newTimeString = [dateFormater2 stringFromDate:newDate];

    return newTimeString;

}
-(void) viewWillAppear:(BOOL)animated   {
    UIView * cartBtn = [self.navigationItem.rightBarButtonItem customView];
    cartBtn.userInteractionEnabled = NO;
    [self shouldEnablePayNowBtn]; // To disable Pay now button
}

-(void) viewDidAppear:(BOOL)animated    {
  //  [self callApiGetRequestProductWiseQuantity];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-TABLEVIEWDELEGATES
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
     if ([OrderTypeString isEqualToString:kStringPickUp]) {
         if(enablePayment == NO)
             return 4;
         else
             return 5;
     }
     else{
      return 6;
     }
}
-(void)addDeliveryDateAndTimeToCell:(UITableViewCell*)lcell{
   // deliveryDateString = [self getCurrentDate];
    
    
    
    
   //  start date next when saturday sunday formet
    
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"EEEE"];
//        NSDate * day = [dateFormatter dateFromString:deliveryDateString];
//        NSString*dayName=[dateFormatter stringFromDate:day];
//    
//        if([dayName isEqualToString:@"Sunday"] || [dayName isEqualToString:@"Saturday"]){
//            NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
//            [offsetComponents setDay:3];
//            NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//            NSDate *nextDate = [gregorian dateByAddingComponents:offsetComponents toDate:day options:0];
//            NSString *refinedate=[dateFormatter stringFromDate:nextDate];
//            deliveryDateString=refinedate;
//            NSLog(@"%@",nextDate);
//        }
//    
    
    if(pickerArray.count>0){
    [self addTimeIntervalInGivenTime:[pickerArray objectAtIndex:0] withtimeinterval:2 ];
    }
    [DeliveryDateButton.titleLabel setFont:[UIFont fontWithName:kFontSemiBold size:kFontRegularSize]];
    [DeliveryDateButton setTitleColor:[UIColor colorWithRed:0.4824 green:0.4824 blue:0.4941 alpha:1.0] forState:UIControlStateNormal];
    [[DeliveryDateButton layer] setBorderWidth:0.5f];
    [DeliveryDateButton setTitle:deliveryDateString forState:UIControlStateNormal];
    [[DeliveryDateButton layer] setBorderColor:[self getColorForRedText].CGColor];
    [DeliveryDateButton addTarget:self action:@selector(DeliveryDateButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [lcell addSubview:DeliveryDateButton];
    
    
    UILabel *DeliveryTimeLabel=[self createLblWithRect:CGRectMake(kLeftPadding,DeliveryDateButton.frame.origin.y+DeliveryDateButton.frame.size.height+kPaddingSmall, SCREEN_WIDTH-(kLeftPadding*2), kLabelHeight)
                                                  font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize + 1]
                                                  text:@"Delivery Time:" textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
    [lcell addSubview:DeliveryTimeLabel];
    DeliveryTimeFromButton=[self createButtonWithFrame:CGRectMake(kLeftPadding, DeliveryTimeLabel.frame.origin.y+DeliveryTimeLabel.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_small, kTEXTfieldHeightSmall) withTitle:@"From"];
    [DeliveryTimeFromButton.titleLabel setFont:[UIFont fontWithName:kFontSemiBold size:kFontRegularSize]];
    if (![deliveryTimeFromString isEqualToString:kEmptyString]) {
        [DeliveryTimeFromButton setTitle:deliveryTimeFromString forState:UIControlStateNormal];

    }
    
    [DeliveryTimeFromButton setTitleColor:[UIColor colorWithRed:0.4824 green:0.4824 blue:0.4941 alpha:1.0] forState:UIControlStateNormal];
    [[DeliveryTimeFromButton layer] setBorderWidth:0.5f];
    [[DeliveryTimeFromButton layer] setBorderColor:[self getColorForRedText].CGColor];
    [DeliveryTimeFromButton addTarget:self action:@selector(DeliveryTimeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [lcell addSubview:DeliveryTimeFromButton];
    
    DeliveryTimeToButton=[self createButtonWithFrame:CGRectMake(DeliveryTimeFromButton.frame.size.width+DeliveryTimeFromButton.frame.origin.x+kPaddingSmall,DeliveryTimeLabel.frame.size.height+DeliveryTimeLabel.frame.origin.y+kPaddingSmall, kGreyBorderTextfieldWidth_small, kTEXTfieldHeightSmall) withTitle:@"To"];
    [DeliveryTimeToButton.titleLabel setFont:[UIFont fontWithName:kFontSemiBold size:kFontRegularSize]];
    if (![deliveryTimeToString isEqualToString:kEmptyString]) {
        [DeliveryTimeToButton setTitle:deliveryTimeToString forState:UIControlStateNormal];

    }

    [DeliveryTimeToButton setTitleColor:[UIColor colorWithRed:0.4824 green:0.4824 blue:0.4941 alpha:1.0] forState:UIControlStateNormal];
    [[DeliveryTimeToButton layer] setBorderWidth:0.5f];
    [[DeliveryTimeToButton layer] setBorderColor:[self getColorForRedText].CGColor];
    [lcell addSubview:DeliveryTimeToButton];
    
}
-(void)addPaymentInfoTocell:(UITableViewCell*)lcell{
    if(enablePayment == NO)
    {
        payAtStoreButton =[self AddCustomButtonWithRedBorder:CGRectMake(kLeftPadding,100, kGreyBorderTextfieldWidth_Large, kButtonHeight) withTitle:kButtonTittlePAYATSTORE];
        [payAtStoreButton addTarget:self action:@selector(payAtStoreButton:) forControlEvents:UIControlEventTouchUpInside];
        payAtStoreButton .enabled=YES;
        [lcell addSubview:payAtStoreButton];
    }
    else
    {
    monthTextfd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, cardnumberTextfd.frame.origin.y+ cardnumberTextfd.frame.size.height+kPaddingSmall,kTEXTfieldWidth/3 + 3, kTEXTfieldHeightSmall) text:kEmptyString withTag:kTextfieldTagGreyBorder withPlaceholder:@"MM/YY"];
    [monthTextfd setKeyboardType:UIKeyboardTypeNumberPad];
    [monthTextfd setValue:[self getColorForBlackText] forKeyPath:@"_placeholderLabel.textColor"];
    monthTextfd.textAlignment = NSTextAlignmentLeft;
    [monthTextfd setDelegate:self];
    monthTextfd.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize];
    
    [lcell addSubview:monthTextfd];
    
    cvvTextfd=[self createDefaultTxtfieldWithRect:
               CGRectMake(kPaddingSmall+monthTextfd.frame.origin.x+ monthTextfd.frame.size.width + 10,
                          cardnumberTextfd.frame.origin.y+ cardnumberTextfd.frame.size.height+kPaddingSmall,
                          kTEXTfieldWidth/3 - 9, kTEXTfieldHeightSmall)
                                             text:kEmptyString withTag:kTextfieldTagGreyBorder withPlaceholder:@"CVV"];
    cvvTextfd.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize];
    [cvvTextfd setKeyboardType:UIKeyboardTypeNumberPad];
    [cvvTextfd setValue:[self getColorForBlackText] forKeyPath:@"_placeholderLabel.textColor"];
    cvvTextfd.textAlignment = NSTextAlignmentLeft;
    [cvvTextfd setDelegate:self];
    [lcell addSubview:cvvTextfd];
    
    zipTextField=[self createDefaultTxtfieldWithRect:
                  CGRectMake(kPaddingSmall+cvvTextfd.frame.origin.x+ cvvTextfd.frame.size.width + 10,
                             cardnumberTextfd.frame.origin.y+ cardnumberTextfd.frame.size.height+kPaddingSmall,kGreyBorderTextfieldWidth_Large-((kTEXTfieldWidth/3)+(kTEXTfieldWidth/3)+(kPaddingSmall*2) + 13), kTEXTfieldHeightSmall)
                                                text:kEmptyString withTag:kTextfieldTagGreyBorder withPlaceholder:@"Zip code"];
    zipTextField.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize];
    
    [zipTextField setKeyboardType:UIKeyboardTypeNumberPad];
    [zipTextField setValue:[self getColorForBlackText] forKeyPath:@"_placeholderLabel.textColor"];
    zipTextField.textAlignment = NSTextAlignmentCenter;
    
    [zipTextField setDelegate:self];
    [lcell addSubview:zipTextField];
    
    if ([cardDeatailArray count]) {
        cardnumberTextfd.text=[cardDeatailArray objectAtIndex:0];
        monthTextfd.text=[cardDeatailArray objectAtIndex:1];
        cvvTextfd.text=[cardDeatailArray objectAtIndex:2];
        zipTextField.text=[cardDeatailArray objectAtIndex:3];
    }
        /************  Add privacy Button *********************************/
        
        
        
        /* couponCodeButton=[self createButtonWithFrame:CGRectMake(kLeftPadding,(titleLabel.frame.origin.y+titleLabel.frame.size.height/2)+kTopPadding - 20, SCREEN_WIDTH-(kRightPadding*2), 20) withTitle:kLabelCouponCodeInfo];
        NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
        couponCodeButton.titleLabel.attributedText=[[NSAttributedString alloc] initWithString:kLabelCouponCodeInfo
                                                                                   attributes:underlineAttribute];
        [couponCodeButton setTitleColor:[self getColorForRedText]
                               forState:UIControlStateNormal];
        [couponCodeButton addTarget:self action:@selector(couponCodeButton:) forControlEvents:UIControlEventTouchUpInside];
        couponCodeButton.tag=12;
        couponCodeButton.titleLabel.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize + 2];
        [couponCodeButton setBackgroundColor:[UIColor clearColor]];
        couponCodeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft; */
        
        CGFloat topPadding;
   
        if([privacyPolicyText length]==0)
        {
            topPadding = kTopPadding;
        }
        else
        {
          topPadding = kTopPadding+20.0;
         NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName: [self getColorForRedText],
                                  NSFontAttributeName: [UIFont fontWithName:kFontRegular size:kFontRegularSize + 2],
                                  NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)
                                  };
        NSString * text = kMessagePrivacypolicyText;
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:text
                                               attributes:attribs];
        
        UIButton *privacyButton = [self createButtonWithFrame:CGRectMake(kLeftPadding-5.0,zipTextField.frame.origin.y+ zipTextField.frame.size.height+kTopPadding-20.0, kGreyBorderTextfieldWidth_Large, kTEXTfieldHeight) withTitle:kEmptyString];
        [privacyButton.titleLabel setFont:[UIFont fontWithName:kFontRegular size:kFontRegularSize]];
        privacyButton.titleLabel.numberOfLines=2;
        privacyButton.backgroundColor=[UIColor clearColor];
        [privacyButton setAttributedTitle:attributedText forState:UIControlStateNormal];
        privacyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        privacyButton.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
        [privacyButton addTarget:self action:@selector(openPrivacyPolicy) forControlEvents:UIControlEventTouchUpInside];
         [lcell addSubview:privacyButton];
        }
    
    if ([OrderTypeString isEqualToString:kStringPickUp]) {
        payNowButton =[self AddCustomButtonWithRedBorder:CGRectMake(kLeftPadding, zipTextField.frame.origin.y+ zipTextField.frame.size.height+topPadding,kGreyBorderTextfieldWidth_Large, kButtonHeight) withTitle:kButtonTittlePAYNOW];
        payNowButton.enabled = YES;
        
        [payNowButton addTarget:self action:@selector(payNowButton:) forControlEvents:UIControlEventTouchUpInside];
        payNowButton.tag=0;
        if (![payNowButton isEnabled]) {
            [payNowButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [[payNowButton layer] setBorderColor:[UIColor lightGrayColor].CGColor];
        }
        else {
            [payNowButton setTitleColor:[self  getColorForRedText] forState:UIControlStateNormal];
            [[payNowButton layer] setBorderColor:[self  getColorForRedText].CGColor];
        }
        [lcell addSubview:payNowButton];
        NSLog(@"payStore = %d",(int)payStore);
        if(payStore)
        { payAtStoreButton =[self AddCustomButtonWithRedBorder:CGRectMake(kLeftPadding, payNowButton.frame.origin.y+ payNowButton.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_Large, kButtonHeight) withTitle:kButtonTittlePAYATSTORE];
            [payAtStoreButton addTarget:self action:@selector(payAtStoreButton:) forControlEvents:UIControlEventTouchUpInside];
            payAtStoreButton .enabled=YES;
            [lcell addSubview:payAtStoreButton];
        }
        else
        {}
    
    }
    else{
        payNowButton =[self AddCustomButtonWithRedBorder:CGRectMake(kLeftPadding, zipTextField.frame.origin.y+ zipTextField.frame.size.height+topPadding, kGreyBorderTextfieldWidth_Large, kButtonHeight) withTitle:kButtonTittlePAYNOW];
        [payNowButton addTarget:self action:@selector(payNowButton:) forControlEvents:UIControlEventTouchUpInside];
        payNowButton .enabled=YES;
        [lcell addSubview:payNowButton];
    }
    [self shouldEnablePayNowBtn];
    
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    couponCodeTxtFd.text=textfieldData;
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    DLog(@"cardDeatailArray====%@",cardDeatailArray);
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
   // cell = nil;
    //     UIImageView *backgroundImageView=[self createImageViewWithRect:CGRectMake(0, 0, SCREEN_WIDTH, cell.frame.size.width) image:[UIImage imageNamed:kImageButtonStrip]];
    
   // if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
        [self getSeperatorVForTableCell:cell];
    
        UILabel *titleLabel=[self createLblWithRect:CGRectMake(kLeftPadding,kPaddingSmall, SCREEN_WIDTH-(kLeftPadding*2), kLabelHeight)
                                               font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize + 1]
                                               text:kEmptyString textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
        [cell addSubview:titleLabel];
        
        switch (indexPath.row) {
            case 0:{
                titleLabel.text=kLabelOrderSummary;
              //  NSLog(@"orderSummaryItems = %@",orderSummaryItems);
                for (int i=0; i<[orderSummaryItems count]; i++) {
                    UILabel *rightLabel=[self createLblWithRect:CGRectMake(kLeftPadding, titleLabel.frame.origin.y+ titleLabel.frame.size.height+(i*kLabelHeight)+kPaddingSmall - 5,kLabelWidthOrderSummary, kLabelHeight)
                                                           font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize] text:[orderSummaryItems objectAtIndex:i] textAlignment:NSTextAlignmentLeft textColor:[UIColor colorWithRed:0.4824 green:0.4824 blue:0.4941 alpha:1.0]];
                    [rightLabel setBackgroundColor:[UIColor clearColor]];
                    [cell addSubview:rightLabel];
                    itemCost = 0.0;
                    double orderTotal=0.0;
                    double tax = 0.0;
                    discount = 0.0;
                    DLog(@"getCartArray---%@",getCartArray);
                    
                    if ([OrderTypeString isEqualToString:kStringPickUp]) {
                        if (![CouponCodeValidatorString isEqualToString:@"Success"]&& couponCode==NO){
                        for (Product *product in getCartArray) {
                            if ([product.productSalesPrice floatValue]>0.0) {
                                itemCost=[product.productSalesPrice floatValue]*product.orderQuantity + itemCost;
                                orderTotal=([product.productSalesPrice floatValue]+[product.productSalesPrice floatValue]*[product.ProductTax floatValue])*product.orderQuantity +orderTotal;
                                tax=[product.productSalesPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity + tax;
                                
                            }
                            else {
                                itemCost=[product.productPrice floatValue]*product.orderQuantity + itemCost;
                                orderTotal=([product.productPrice floatValue]+[product.productPrice floatValue]*[product.ProductTax floatValue])*product.orderQuantity +orderTotal;
                                tax=[product.productPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity + tax;
                                
                            }
                        }
                            itemsCost_oc = [NSString stringWithFormat:@"%.2f",itemCost];
                            tax_oc = [NSString stringWithFormat:@"%.2f",tax];
                            orderTotal_oc = [NSString stringWithFormat:@"%.2f",orderTotal];
                        orderSummaryValues=[NSMutableArray arrayWithObjects:[NSString stringWithFormat:@"%.2f",itemCost],[NSString stringWithFormat:@"%.2f",tax],[NSString stringWithFormat:@"%.2f", orderTotal], nil];
                    
                        
                        }
                        else if ([CouponCodeValidatorString isEqualToString:@"Success"]&& couponCode==NO){
                            
                            for (Product *product in getCartArray) {
                                if ([product.productSalesPrice floatValue]>0.0) {
                                    itemCost=[product.productSalesPrice floatValue]*product.orderQuantity + itemCost;
                                    orderTotal=([product.productSalesPrice floatValue]+[product.productSalesPrice floatValue]*[product.ProductTax floatValue])*product.orderQuantity +orderTotal;
                                    tax=[product.productSalesPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity + tax;
                                    
                                }
                                else {
                                    itemCost=[product.productPrice floatValue]*product.orderQuantity + itemCost;
                                    orderTotal=([product.productPrice floatValue]+[product.productPrice floatValue]*[product.ProductTax floatValue])*product.orderQuantity +orderTotal;
                                    tax=[product.productPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity + tax;
                                    
                                }
                            }
                            
                            //Added tax after discount
                            float priceAfterDiscount = itemCost-couponDiscount;
                            float taxAfterDiscount = ((tax/itemCost)*priceAfterDiscount);
                            itemsCost_oc = [NSString stringWithFormat:@"%.2f",itemCost];
                            tax_oc = [NSString stringWithFormat:@"%.2f",taxAfterDiscount];
                            orderTotal_oc = [NSString stringWithFormat:@"%.2f",orderTotal];
                            orderSummaryValues=[NSMutableArray arrayWithObjects:[NSString stringWithFormat:@"%.2f",itemCost],[NSString stringWithFormat:@"%.2f",taxAfterDiscount],[NSString stringWithFormat:@"%.2f", orderTotal], nil];
                           
                        }
                        
                        else {
                            for (Product *product in getCartArray) {
                                if ([product.productSalesPrice floatValue]>0.0) {
                                  
                                        discount=[[orderTotalArray valueForKey:@"CouponFlatDiscount"] floatValue];
                                    
                                    discount=[[orderTotalArray valueForKey:@"CouponFlatDiscount"] floatValue];
                                    discount=[[NSString stringWithFormat:@"%.02f", discount] floatValue];
                                    
                                    itemCost=[product.productSalesPrice floatValue]*product.orderQuantity + itemCost;
                                    orderTotal=([product.productSalesPrice floatValue]+[product.productSalesPrice floatValue]*[product.ProductTax floatValue])*product.orderQuantity +orderTotal;
                                    tax=[product.productSalesPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity + tax;
                                    
                                }
                                else {
                                   
                                    discount=[[orderTotalArray valueForKey:@"CouponFlatDiscount"] floatValue];
                                    discount=[[NSString stringWithFormat:@"%.02f", discount] floatValue];
                                    
                                    itemCost=[product.productPrice floatValue]*product.orderQuantity + itemCost;
                                    orderTotal=([product.productPrice floatValue]+[product.productPrice floatValue]*[product.ProductTax floatValue])*product.orderQuantity +orderTotal;
                                    tax=[product.productPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity + tax;
                                    
                                }
                            }
                            //Added tax after discount
                            float priceAfterDiscount = itemCost-couponDiscount;
                            float taxAfterDiscount = ((tax/itemCost)*priceAfterDiscount);
                            
                            orderTotal = orderTotal-tax-discount+taxAfterDiscount;
                            
                            itemsCost_oc = [NSString stringWithFormat:@"%.2f",itemCost];
                            discount_oc = [NSString stringWithFormat:@"%.2f",discount];
                            tax_oc = [NSString stringWithFormat:@"%.2f",taxAfterDiscount];
                            orderTotal_oc = [NSString stringWithFormat:@"%.2f",orderTotal];
                            
                            orderSummaryValues=[NSMutableArray arrayWithObjects:[NSString stringWithFormat:@"%.2f",itemCost],[NSString stringWithFormat:@"%.2f",discount],[NSString stringWithFormat:@"%.2f",taxAfterDiscount],[NSString stringWithFormat:@"%.2f", orderTotal], nil];
                            if([orderSummaryItems count]==3)
                                [orderSummaryValues removeObjectAtIndex:1];
                            else
                            {}

                        }
                        DLog(@"orderSummaryValues=========%@",orderSummaryValues);
                        UILabel *leftLabel=[self createLblWithRect:CGRectMake((rightLabel.frame.origin.x)+(rightLabel.frame.size.width), titleLabel.frame.origin.y+ titleLabel.frame.size.height+(i*kLabelHeight)+kPaddingSmall - 5,kLabelWidthOrderSummary, kLabelHeight) font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 2]
                                                              text:[self addDollarSignToString:[orderSummaryValues objectAtIndex:i]]
                                                     textAlignment:NSTextAlignmentRight
                                                         textColor:[UIColor colorWithRed:0.5843 green:0.5843 blue:0.5843 alpha:1.0]];
                        [leftLabel setBackgroundColor:[UIColor clearColor]];
                        [cell addSubview:leftLabel];
                        
                    
                    
                    }
                    else if([OrderTypeString isEqualToString:kStringDeliver]){
                        
                        
                         if (![CouponCodeValidatorString isEqualToString:@"Success"] && couponCode==NO){
                             
                        __weak AppDelegate *appdelegateObj=(AppDelegate*)[[UIApplication sharedApplication] delegate];
                           //  NSLog(@"%@",getCartArray);
                        for (Product *product in getCartArray) {
                            if ([product.productSalesPrice floatValue]>0.0) {
                                itemCost=[product.productSalesPrice floatValue]*product.orderQuantity + itemCost;
                                orderTotal=([product.productSalesPrice floatValue]+[product.productSalesPrice floatValue]*[product.ProductTax floatValue])*product.orderQuantity +orderTotal;
                                tax=[product.productSalesPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity + tax;
                            }
                            else {
                                itemCost=[product.productPrice floatValue]*product.orderQuantity + itemCost;
                                orderTotal=([product.productPrice floatValue]+[product.productPrice floatValue]*[product.ProductTax floatValue])*product.orderQuantity +orderTotal;
                                tax=[product.productPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity + tax;
                            }
//                            orderSummaryValues=[[NSMutableArray alloc] initWithObjects:[NSString stringWithFormat:@"%.2f",itemCost],[NSString stringWithFormat:@"%.2f",tax],[NSString stringWithFormat:@"%.2f", orderTotal],[NSString stringWithFormat:@"%.2f", discount], nil];
                        }
                        
                             /**********************************************************************************/
                        /**Delivery charges only applicable if total item cost is less then delivery limit*/
                        double deliveryCharges  = 0.00;
                         
                       if(itemCost < [appdelegateObj.DeliveryLimit_Cart floatValue]||[appdelegateObj.DeliveryLimit_Cart length]==0)
                            deliveryCharges = [appdelegateObj.DeliveryCharges_Cart floatValue];
                            
                        float totalWithoutTax = itemCost + deliveryCharges-discount;
                        orderTotal = orderTotal + deliveryCharges;
                        
                        orderTotal = (((int)([[NSString stringWithFormat:@"%.2f", orderTotal] floatValue] * 100))/ 100.00)-discount ;
                       // tax = (((int)([[NSString stringWithFormat:@"%.2f", tax] floatValue] * 100))/ 100.00) ;
                             tax = [[NSString stringWithFormat:@"%.2f", tax] floatValue];
                        
                             //Added delivery tax rate
                             float deliveryTaxRatePerDollar = [appdelegateObj.deliveryTaxRate floatValue];
                             float totalDeliveryTaxRate = deliveryTaxRatePerDollar*deliveryCharges;
                             
                             tax = tax+totalDeliveryTaxRate;
                             //Added delivery tax rate to total order
                             orderTotal = orderTotal+totalDeliveryTaxRate;
                             
                             itemsCost_oc = [NSString stringWithFormat:@"%.2f",itemCost];
                             deliveryCharge_oc = [NSString stringWithFormat:@"%.2f",deliveryCharges];
                             totalBeforeTax_oc = [NSString stringWithFormat:@"%.2f",totalWithoutTax];
                             tax_oc = [NSString stringWithFormat:@"%.2f",tax];
                             orderTotal_oc = [NSString stringWithFormat:@"%.2f",orderTotal];
                             
                             orderSummaryValues=[[NSMutableArray alloc] initWithObjects:[NSString stringWithFormat:@"%.2f", itemCost],
                                                 //[NSString stringWithFormat:@"%2f", discount],
                                                 [NSString stringWithFormat:@"%.2f", deliveryCharges],
                                                 [NSString stringWithFormat:@"%.2f", totalWithoutTax],
                                                 [NSString stringWithFormat:@"%.2f", tax],
                                                 [NSString stringWithFormat:@"%.2f", orderTotal], nil];
                        /**********************************************************************************/
                             
                    
                         
                         }
                         else  if ([CouponCodeValidatorString isEqualToString:@"Success"] && couponCode==NO){
                             __weak AppDelegate *appdelegateObj=(AppDelegate*)[[UIApplication sharedApplication] delegate];
                             for (Product *product in getCartArray) {
                                 if ([product.productSalesPrice floatValue]>0.0) {
                                     itemCost=[product.productSalesPrice floatValue]*product.orderQuantity + itemCost;
                                     orderTotal=([product.productSalesPrice floatValue]+[product.productSalesPrice floatValue]*[product.ProductTax floatValue])*product.orderQuantity +orderTotal;
                                     tax=[product.productSalesPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity + tax;
                                 }
                                 else {
                                     itemCost=[product.productPrice floatValue]*product.orderQuantity + itemCost;
                                     orderTotal=([product.productPrice floatValue]+[product.productPrice floatValue]*[product.ProductTax floatValue])*product.orderQuantity +orderTotal;
                                     tax=[product.productPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity + tax;
                                 }
                                 //                            orderSummaryValues=[[NSMutableArray alloc] initWithObjects:[NSString stringWithFormat:@"%.2f",itemCost],[NSString stringWithFormat:@"%.2f",tax],[NSString stringWithFormat:@"%.2f", orderTotal],[NSString stringWithFormat:@"%.2f", discount], nil];
                             }
                             
                             /**********************************************************************************/
                             /**Delivery charges only applicable if total item cost is less then delivery limit*/
                             double deliveryCharges  = 0.00;
                             
                             if(itemCost < [appdelegateObj.DeliveryLimit_Cart floatValue]||[appdelegateObj.DeliveryLimit_Cart length]==0)
                                 deliveryCharges = [appdelegateObj.DeliveryCharges_Cart floatValue];
                             
                             float totalWithoutTax = itemCost + deliveryCharges-discount;
                             orderTotal = orderTotal + deliveryCharges;
                             
                             orderTotal = (((int)([[NSString stringWithFormat:@"%.2f", orderTotal] floatValue] * 100))/ 100.00)-discount ;
                            // tax = (((int)([[NSString stringWithFormat:@"%.2f", tax] floatValue] * 100))/ 100.00) ;
                              tax = [[NSString stringWithFormat:@"%.2f", tax] floatValue];
                             
                             //Added delivery tax rate
                             float deliveryTaxRatePerDollar = [appdelegateObj.deliveryTaxRate floatValue];
                             float totalDeliveryTaxRate = deliveryTaxRatePerDollar*deliveryCharges;
                             
                             tax = tax+totalDeliveryTaxRate;
                             //Added delivery tax rate to total order
                             orderTotal = orderTotal+totalDeliveryTaxRate;
                             
                             itemsCost_oc = [NSString stringWithFormat:@"%.2f",itemCost];
                             deliveryCharge_oc = [NSString stringWithFormat:@"%.2f",deliveryCharges];
                             totalBeforeTax_oc = [NSString stringWithFormat:@"%.2f",totalWithoutTax];
                             tax_oc = [NSString stringWithFormat:@"%.2f",tax];
                             orderTotal_oc = [NSString stringWithFormat:@"%.2f",orderTotal];
                             
                             orderSummaryValues=[[NSMutableArray alloc] initWithObjects:[NSString stringWithFormat:@"%.2f", itemCost],
                                                 //[NSString stringWithFormat:@"%2f", discount],
                                                 [NSString stringWithFormat:@"%.2f", deliveryCharges],
                                                 [NSString stringWithFormat:@"%.2f", totalWithoutTax],
                                                 [NSString stringWithFormat:@"%.2f", tax],
                                                 [NSString stringWithFormat:@"%.2f", orderTotal], nil];
                             /**********************************************************************************/
                             
                           //  NSLog(@"OrderSummaryValues1111 = %@",orderSummaryValues);
                         
                         }
                         else{
                             __weak AppDelegate *appdelegateObj=(AppDelegate*)[[UIApplication sharedApplication] delegate];
                             for (Product *product in getCartArray) {
                                 if ([product.productSalesPrice floatValue]>0.0) {
                                   
                                     discount=[[orderTotalArray valueForKey:@"CouponFlatDiscount"] floatValue];
                                    discount=[[NSString stringWithFormat:@"%.02f", discount] floatValue];
                                    
                                     itemCost=[product.productSalesPrice floatValue]*product.orderQuantity + itemCost;
                                     orderTotal=([product.productSalesPrice floatValue]+[product.productSalesPrice floatValue]*[product.ProductTax floatValue])*product.orderQuantity +orderTotal;
                                     tax=[product.productSalesPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity + tax;
                                 }
                                 else {
                                    
                                     discount=[[orderTotalArray valueForKey:@"CouponFlatDiscount"] floatValue];
                                     
                                     discount=[[orderTotalArray valueForKey:@"CouponFlatDiscount"] floatValue];
                                     discount=[[NSString stringWithFormat:@"%.02f", discount] floatValue];
                                     
                                     itemCost=[product.productPrice floatValue]*product.orderQuantity + itemCost;
                                     orderTotal=([product.productPrice floatValue]+[product.productPrice floatValue]*[product.ProductTax floatValue])*product.orderQuantity +orderTotal;
                                     tax=[product.productPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity + tax;
                                 }
                             
                             
                             }
                             
                             /**********************************************************************************/
                             /**Delivery charges only applicable if total item cost is less then delivery limit*/
                             double deliveryCharges  = 0.00;
                             
                             if(itemCost < [appdelegateObj.DeliveryLimit_Cart floatValue]||[appdelegateObj.DeliveryLimit_Cart length]==0)
                                 deliveryCharges = [appdelegateObj.DeliveryCharges_Cart floatValue];
                             
                             float totalWithoutTax = itemCost + deliveryCharges-discount;
                             orderTotal = orderTotal + deliveryCharges;
                             
                              float deliveryTaxRatePerDollar = [appdelegateObj.deliveryTaxRate floatValue];
                             float totalDeliveryTaxRate = deliveryTaxRatePerDollar *deliveryCharges;
                             
                             float discountedItem = itemCost-discount;
                             float taxAfterDiscount = ((tax/itemCost)*discountedItem);
                             
                            
                             
                           /*  orderTotal = (((int)([[NSString stringWithFormat:@"%.2f", orderTotal] floatValue] * 100))/ 100.00)-discount ;
                             tax = (((int)([[NSString stringWithFormat:@"%.2f", tax] floatValue] * 100))/ 100.00) ;
                             //Added delivery tax rate
                             float deliveryTaxRatePerDollar = [appdelegateObj.deliveryTaxRate floatValue];
                             float totalDeliveryTaxRate = deliveryTaxRatePerDollar*deliveryCharges;
                             //Added taxAfterDiscount
                             float discountedItem = itemCost-discount;
                             float taxAfterDiscount = ((tax/itemCost)*discountedItem);
                             // NSLog(@"Tax After Discount = %f",taxAfterDiscount); */
                             
                             
                             tax = taxAfterDiscount+ totalDeliveryTaxRate;
                              orderTotal = itemCost+deliveryCharges+tax-discount;
                            // NSLog(@"1111TotalDeliveryTaxRate = %f\n discountedItemCost = %f\n TaxAfterDiscount = %f\n tax = %f \n order total = %f",totalDeliveryTaxRate,discountedItem,taxAfterDiscount,tax,orderTotal);
                             // tax = tax+totalDeliveryTaxRate;
                             //Added delivery tax rate to total order
                            // orderTotal = orderTotal+totalDeliveryTaxRate+tax;
                             
                             itemsCost_oc = [NSString stringWithFormat:@"%.2f",itemCost];
                             discount_oc = [NSString stringWithFormat:@"%.2f",discount];
                             deliveryCharge_oc = [NSString stringWithFormat:@"%.2f",deliveryCharges];
                             totalBeforeTax_oc = [NSString stringWithFormat:@"%.2f",totalWithoutTax];
                             tax_oc = [NSString stringWithFormat:@"%.2f",tax];
                             orderTotal_oc = [NSString stringWithFormat:@"%.2f",orderTotal];
                             
                                orderSummaryValues=[[NSMutableArray alloc] initWithObjects:[NSString stringWithFormat:@"%.2f", itemCost],
                                                 [NSString stringWithFormat:@"%.2f", discount],
                                                 [NSString stringWithFormat:@"%.2f", deliveryCharges],
                                                 [NSString stringWithFormat:@"%.2f", totalWithoutTax],
                                                 [NSString stringWithFormat:@"%.2f", tax],
                                                 [NSString stringWithFormat:@"%.2f", orderTotal], nil];

                           
                             /**********************************************************************************/
                             if([orderSummaryItems count]==5)
                                 [orderSummaryValues removeObjectAtIndex:1];
                             else
                             {}
                         }
                      
                    DLog(@"orderSummaryValues=========%@",orderSummaryValues);
                    UILabel *leftLabel=[self createLblWithRect:CGRectMake((rightLabel.frame.origin.x)+(rightLabel.frame.size.width), titleLabel.frame.origin.y+ titleLabel.frame.size.height+(i*kLabelHeight)+kPaddingSmall - 5,kLabelWidthOrderSummary, kLabelHeight) font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 2]
                                                          text:[self addDollarSignToString:[orderSummaryValues objectAtIndex:i]]
                                                 textAlignment:NSTextAlignmentRight
                                                     textColor:[UIColor colorWithRed:0.5843 green:0.5843 blue:0.5843 alpha:1.0]];
                    [leftLabel setBackgroundColor:[UIColor clearColor]];
                    [cell addSubview:leftLabel];
                    }
                }
                
                break;
            }
            case 1:{
                
                     if (enterNewAddress && [OrderTypeString isEqualToString:kStringDeliver]) {
                    titleLabel.text=kLabelEnterADeliveryAddress;
                    UIButton *removeButton=[self createButtonWithFrame:CGRectMake(SCREEN_WIDTH-(kLeftPadding ), kPaddingSmall, kLeftPadding, kButtonHeight) withTitle:nil];
                    [removeButton setImage:[UIImage imageNamed:kImageCrossButton] forState:UIControlStateNormal];
                    [removeButton setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
                    [removeButton addTarget:self action:@selector(removeButton:) forControlEvents:UIControlEventTouchUpInside];
                    [cell addSubview:removeButton];
                         

                         UIImage *addAPhotoButtonImage=[UIImage imageNamed:@"out-of-stock"];
                       UIButton * findMe=[self createButtonWithFrame:CGRectMake(kLeftPadding, titleLabel.frame.origin.y+ titleLabel.frame.size.height+kPaddingSmall, 60, 20) withTitle:@"Find Me"];
                         findMe.imageView.image=addAPhotoButtonImage;
                         [findMe addTarget:self action:@selector(findMeButton:) forControlEvents:UIControlEventTouchUpInside];
                         // self.findMe.layer.cornerRadius = (addAPhotoButtonImage.size.height)/2.0f;
                         
                         findMe.clipsToBounds = YES;
                         [findMe setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                         [findMe  setBackgroundColor:[UIColor whiteColor]];
                         findMe.titleLabel.font=[UIFont fontWithName:kFontBold size:10];
                         // self.addAPhotoButton.titleLabel.numberOfLines=2;
                        findMe.layer.borderColor=[UIColor redColor].CGColor;
                        findMe.layer.borderWidth=0.5f;
                        findMe.tag=kButtonTagSignUpAddAphotoButton+40;
                         [findMe setEnabled:YES];
                         [cell addSubview:findMe];
                         
                    UserProfile *userProfileObj=kgetUserProfileModel;
                                        firstNameTxtfd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, findMe.frame.origin.y+ titleLabel.frame.size.height+kPaddingSmall,kGreyBorderTextfieldWidth_small, kTEXTfieldHeightSmall) text:userProfileObj.FirstName withTag:kTextfieldTagGreyBorder withPlaceholder:@"First Name"];
                    [firstNameTxtfd setValue:[UIColor colorWithRed:0.4549 green:0.4549 blue:0.4667 alpha:1.0]
                                  forKeyPath:@"_placeholderLabel.textColor"];
                    [firstNameTxtfd setDelegate:self];
                    [cell addSubview:firstNameTxtfd];
                    lastNameTxtfd=[self createDefaultTxtfieldWithRect:CGRectMake(kPaddingSmall+firstNameTxtfd.frame.origin.x+ firstNameTxtfd.frame.size.width, findMe.frame.origin.y+ titleLabel.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_small, kTEXTfieldHeightSmall) text:userProfileObj.LastName withTag:kTextfieldTagGreyBorder withPlaceholder:@"Last Name"];
                    [lastNameTxtfd setValue:[UIColor colorWithRed:0.4549 green:0.4549 blue:0.4667 alpha:1.0]
                                 forKeyPath:@"_placeholderLabel.textColor"];
                    [lastNameTxtfd setDelegate:self];
                    [cell addSubview:lastNameTxtfd];
                         
                    addressTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, firstNameTxtfd.frame.origin.y+ firstNameTxtfd.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_Large, kTEXTfieldHeightSmall) text:kEmptyString withTag:kTextfieldTagGreyBorder withPlaceholder:@"Address"];
                    [addressTxtFd setValue:[UIColor colorWithRed:0.4549 green:0.4549 blue:0.4667 alpha:1.0]
                                forKeyPath:@"_placeholderLabel.textColor"];
                    [addressTxtFd setDelegate:self];
                   
                    [cell addSubview:addressTxtFd];
                         
                        apartmentTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, addressTxtFd.frame.origin.y+ firstNameTxtfd.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_Large, kTEXTfieldHeightSmall) text:kEmptyString withTag:kTextfieldTagGreyBorder withPlaceholder:@"Apartment/Suite"];
                         [apartmentTxtFd setValue:[UIColor colorWithRed:0.4549 green:0.4549 blue:0.4667 alpha:1.0]
                                     forKeyPath:@"_placeholderLabel.textColor"];
                         [apartmentTxtFd setDelegate:self];
                         [cell addSubview:apartmentTxtFd];

                    cityTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding,apartmentTxtFd.frame.origin.y+ addressTxtFd.frame.size.height+kPaddingSmall,kGreyBorderTextfieldWidth_small, kTEXTfieldHeightSmall) text:kEmptyString withTag:kTextfieldTagGreyBorder withPlaceholder:@"City"];
                    [cityTxtFd setValue:[UIColor colorWithRed:0.4549 green:0.4549 blue:0.4667 alpha:1.0]
                             forKeyPath:@"_placeholderLabel.textColor"];
                    [cityTxtFd setDelegate:self];
                    [cell addSubview:cityTxtFd];
                    stateTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kPaddingSmall+cityTxtFd.frame.origin.x+ cityTxtFd.frame.size.width, apartmentTxtFd.frame.origin.y+ addressTxtFd.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_small, kTEXTfieldHeightSmall) text:kEmptyString withTag:kTextfieldTagGreyBorder withPlaceholder:@"State"];
                    [stateTxtFd setValue:[UIColor colorWithRed:0.4549 green:0.4549 blue:0.4667 alpha:1.0]
                              forKeyPath:@"_placeholderLabel.textColor"];
                    [stateTxtFd setDelegate:self];
                    [cell addSubview:stateTxtFd];
                    zipTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, stateTxtFd.frame.origin.y+ stateTxtFd.frame.size.height+kPaddingSmall,kGreyBorderTextfieldWidth_small, kTEXTfieldHeightSmall) text:kEmptyString withTag:kTextfieldTagGreyBorder withPlaceholder:@"Zip Code"];
                    [zipTxtFd setValue:[UIColor colorWithRed:0.4549 green:0.4549 blue:0.4667 alpha:1.0]
                            forKeyPath:@"_placeholderLabel.textColor"];
                    zipTxtFd.keyboardType = UIKeyboardTypeNumberPad;
                    [zipTxtFd setDelegate:self];
                         [cell addSubview:zipTxtFd];
                         
                    
                   phoneNumberTxtfd=[self createDefaultTxtfieldWithRect:CGRectMake(kPaddingSmall+zipTxtFd.frame.origin.x+ zipTxtFd.frame.size.width, stateTxtFd.frame.origin.y+ stateTxtFd.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_small, kTEXTfieldHeightSmall) text:[self getformattedPhoneNumberstringFrom:[userProfileObj ContactNo]] withTag:kTextfieldTagGreyBorder withPlaceholder:@"Phone"];
                    [phoneNumberTxtfd setValue:[UIColor colorWithRed:0.4549 green:0.4549 blue:0.4667 alpha:1.0]
                                    forKeyPath:@"_placeholderLabel.textColor"];
                         userProfileObj.ContactNo=[self getformattedPhoneNumberstringFrom:[userProfileObj ContactNo]];
                    phoneNumberTxtfd.keyboardType = UIKeyboardTypeNumberPad;
                    [phoneNumberTxtfd setDelegate:self];
                    [cell addSubview:phoneNumberTxtfd];
                    UIButton *validateAddress = [self AddCustomButtonWithRedBorder:
                                                 CGRectMake(kLeftPadding, phoneNumberTxtfd.frame.origin.y+ phoneNumberTxtfd.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_Large, kButtonHeight)
                                                                         withTitle:kButtonTitleAddress];
                    [validateAddress addTarget:self action:@selector(validateAddressBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
                    validateAddress.enabled=YES;
                    [cell addSubview:validateAddress];
                    [firstNameTxtfd setTag:(1000 + indexPath.row)];
                    [lastNameTxtfd setTag:(2000 + indexPath.row)];
                    [addressTxtFd setTag:(3000 + indexPath.row)];
                    [apartmentTxtFd setTag:(8000 + indexPath.row)];
                    [cityTxtFd setTag:(4000 + indexPath.row)];
                    [stateTxtFd setTag:(5000 + indexPath.row)];
                    [zipTxtFd setTag:(6000 + indexPath.row)];
                    [phoneNumberTxtfd setTag:(7000 + indexPath.row)];
                    
                    if (AdressValidatorDict!=nil) {
                        [firstNameTxtfd setText:[AdressValidatorDict valueForKey:kParamFirstName]];
                        [lastNameTxtfd setText:[AdressValidatorDict valueForKey:kParamLastName]];
                        [addressTxtFd setText: [AdressValidatorDict valueForKey:kParamAddress]];
                        [apartmentTxtFd setText:[AdressValidatorDict valueForKey:kParamApartment]];
                        [cityTxtFd setText:[AdressValidatorDict valueForKey:kParamCity]];
                        [stateTxtFd setText:[AdressValidatorDict valueForKey:kParamState]];
                        [zipTxtFd setText:[AdressValidatorDict valueForKey:kParamZip]];
                        [phoneNumberTxtfd setText:[AdressValidatorDict valueForKey:kParamPhone]];
                    }
                    else{
                        User *userObj=kGetUserModel;
                        AdressValidatorDict = [NSMutableDictionary dictionary];
                        [AdressValidatorDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
                        [AdressValidatorDict setObject:userObj.userId forKey: kParamUserId];
                        [AdressValidatorDict setObject:firstNameTxtfd.text forKey: kParamFirstName];
                        [AdressValidatorDict setObject:lastNameTxtfd.text forKey: kParamLastName];
                        [AdressValidatorDict setObject:addressTxtFd.text forKey: kParamAddress];
                        [AdressValidatorDict setObject:apartmentTxtFd.text forKey: kParamApartment];
                        [AdressValidatorDict setObject:cityTxtFd.text forKey: kParamCity];
                        [AdressValidatorDict setObject:stateTxtFd.text forKey: kParamState];
                        [AdressValidatorDict setObject:kEmptyString forKey: kParamCountryId];
                        [AdressValidatorDict setObject:zipTxtFd.text forKey: kParamZip];
                        [AdressValidatorDict setObject:phoneNumberTxtfd.text forKey: kParamPhone];
                    }
                }
                else{
              
                    self.streetLabel=[self createLblWithRect:CGRectMake(kLeftPadding, titleLabel.frame.origin.y+ titleLabel.frame.size.height, SCREEN_WIDTH-(kLeftPadding*2), kLabelHeight) font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 2] text:kEmptyString textAlignment:NSTextAlignmentLeft
                                                   textColor:[UIColor colorWithRed:0.5098 green:0.5098 blue:0.5176 alpha:1.0]];
                    [cell addSubview:self.streetLabel];
                    
                    self.StateLabel=[self createLblWithRect:CGRectMake(kLeftPadding, (self.streetLabel.frame.origin.y+self.streetLabel.frame.size.height), SCREEN_WIDTH-(kLeftPadding*2), kLabelHeight) font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 2] text:kEmptyString textAlignment:NSTextAlignmentLeft
                                                  textColor:[UIColor colorWithRed:0.5098 green:0.5098 blue:0.5176 alpha:1.0]];
                    [cell addSubview:self.StateLabel];
                    
                    
                    if ( [OrderTypeString isEqualToString:kStringPickUp]) {
                        titleLabel.text=kLabelPickUpAddress;
                        __weak ClosestStoreModel* lClosestStoreModelObj;
                        __weak StoreDetails *storeDetails;
                      lClosestStoreModelObj = kGetClosestStoreModel;
                        storeDetails = kGetStoreDetailsModel;

                        if (storeDetails!=nil) {
                            //self.apartmentLabel.text=  storeDetails.StoreUserAddress1;
                            if(storeDetails.StoreUserApartment!=nil){
                            self.streetLabel.text=[NSString stringWithFormat:@"%@,%@",storeDetails.StoreUserAddress1,storeDetails.StoreUserApartment];
                                self.streetLabel.numberOfLines=0;
                                numberofLine = [self lineCountForLabel:self.streetLabel];
                                NSAttributedString *aString = [[NSAttributedString alloc] initWithString:self.streetLabel.text];
                                UILabel *calculationView = [[UILabel alloc] init];
                                [calculationView setAttributedText:aString];
                                textRect2 = [self sizeCountForLabel:self.streetLabel];
                                labelHeight = textRect2.size.height;
                                self.streetLabel.frame=CGRectMake(kLeftPadding, titleLabel.frame.origin.y+ titleLabel.frame.size.height, SCREEN_WIDTH-(kLeftPadding*2), labelHeight);
                                
                               self.StateLabel.frame=CGRectMake(kLeftPadding,(self.streetLabel.frame.origin.y+self.streetLabel.frame.size.height), SCREEN_WIDTH-(kLeftPadding*2), kLabelHeight);
                            }
                            else{
                                NSString *text= @"";
                                  self.streetLabel.text=[NSString stringWithFormat:@"%@ %@",storeDetails.StoreUserAddress1,text];
                                self.streetLabel.numberOfLines=0;
                                numberofLine = [self lineCountForLabel:self.streetLabel];
                                NSAttributedString *aString = [[NSAttributedString alloc] initWithString:self.streetLabel.text];
                                UILabel *calculationView = [[UILabel alloc] init];
                                [calculationView setAttributedText:aString];
                                textRect2 = [self sizeCountForLabel:self.streetLabel];
                                labelHeight = textRect2.size.height;
                                self.streetLabel.frame=CGRectMake(kLeftPadding, titleLabel.frame.origin.y+ titleLabel.frame.size.height, SCREEN_WIDTH-(kLeftPadding*2), labelHeight);
                                 self.StateLabel.frame=CGRectMake(kLeftPadding,(self.streetLabel.frame.origin.y+self.streetLabel.frame.size.height), SCREEN_WIDTH-(kLeftPadding*2), kLabelHeight);
                            }
                          
                            
                            self.StateLabel.text=[NSString stringWithFormat:@"%@, %@ %@",storeDetails.StoreUserCity,storeDetails.StoreStateName,storeDetails.StoreUserZipCode];
                            self.apartmentLabel.text=storeDetails.StoreUserApartment;
                        }
                        else{
                            [self callApiStoreDetails];
                            //self.apartmentLabel.text=  storeDetails.StoreUserApartment;
                           // self.streetLabel.text=  storeDetails.StoreUserAddress1;
                             self.streetLabel.text=[NSString stringWithFormat:@"%@,%@",storeDetails.StoreUserAddress1,storeDetails.StoreUserApartment];
                            self.StateLabel.text=[NSString stringWithFormat:@"%@, %@ %@",storeDetails.StoreUserCity,storeDetails.StoreStateName,storeDetails.StoreUserZipCode];
                            self.streetLabel.numberOfLines=0;
                            numberofLine = [self lineCountForLabel:self.streetLabel];
                            NSAttributedString *aString = [[NSAttributedString alloc] initWithString:self.streetLabel.text];
                            UILabel *calculationView = [[UILabel alloc] init];
                            [calculationView setAttributedText:aString];
                            textRect2 = [self sizeCountForLabel:self.streetLabel];
                            labelHeight = textRect2.size.height;
                            self.streetLabel.frame=CGRectMake(kLeftPadding, titleLabel.frame.origin.y+ titleLabel.frame.size.height, SCREEN_WIDTH-(kLeftPadding*2), labelHeight);
                             self.StateLabel.frame=CGRectMake(kLeftPadding,(self.streetLabel.frame.origin.y+self.streetLabel.frame.size.height), SCREEN_WIDTH-(kLeftPadding*2), kLabelHeight);
                            
                        }
                        
                        UIImage *LocationImage=[UIImage imageNamed:kImage_locationicon];
                        UIButton *getLocationButton=[self createButtonWithFrame:CGRectMake(SCREEN_WIDTH-LocationImage.size.width-kRightPadding + 10, self.streetLabel.frame.origin.y+ self.streetLabel.frame.size.height - 20, LocationImage.size.width, LocationImage.size.height) withTitle:kEmptyString];
                        getLocationButton.tag=2222;
                        [getLocationButton addTarget:self action:@selector(getLocationButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                        [getLocationButton setImage:LocationImage forState:UIControlStateNormal];
                        [cell addSubview:getLocationButton];
                    }
                    else{
                        titleLabel.text=kLabelDeliveryAddress;
                        __weak UserProfile *userProfileObj = kgetUserProfileModel;
                        
                        if (![[userProfileObj Address]isEqualToString:kEmptyString]) {
                            if (![[userProfileObj Address2]isEqualToString:kEmptyString]){
                            NSString *address=[NSString stringWithFormat:@"%@, %@ ",[userProfileObj Address],[userProfileObj Address2]];
                            self.streetLabel.text=[NSString stringWithFormat:@"%@",address];
                                self.streetLabel.numberOfLines=0;
                                numberofLine = [self lineCountForLabel:self.streetLabel];
                                NSAttributedString *aString = [[NSAttributedString alloc] initWithString:self.streetLabel.text];
                                UILabel *calculationView = [[UILabel alloc] init];
                                [calculationView setAttributedText:aString];
                                textRect2 = [self sizeCountForLabel:self.streetLabel];
                                labelHeight = textRect2.size.height;
                                self.streetLabel.frame=CGRectMake(kLeftPadding, titleLabel.frame.origin.y+ titleLabel.frame.size.height, SCREEN_WIDTH-(kLeftPadding*2), labelHeight);
                                 self.StateLabel.frame=CGRectMake(kLeftPadding,(self.streetLabel.frame.origin.y+self.streetLabel.frame.size.height), SCREEN_WIDTH-(kLeftPadding*2), kLabelHeight);
                             }
                             else{
                                 
                                 NSString *address=[NSString stringWithFormat:@"%@  %@ ",[userProfileObj Address],@""];
                                 self.streetLabel.text=[NSString stringWithFormat:@"%@",address];
                                 self.streetLabel.numberOfLines=0;
                                 numberofLine = [self lineCountForLabel:self.streetLabel];
                                 NSAttributedString *aString = [[NSAttributedString alloc] initWithString:self.streetLabel.text];
                                 UILabel *calculationView = [[UILabel alloc] init];
                                 [calculationView setAttributedText:aString];
                                 textRect2 = [self sizeCountForLabel:self.streetLabel];
                                 labelHeight = textRect2.size.height;
                                 self.streetLabel.frame=CGRectMake(kLeftPadding, titleLabel.frame.origin.y+ titleLabel.frame.size.height, SCREEN_WIDTH-(kLeftPadding*2), labelHeight);
                          
                                self.StateLabel.frame=CGRectMake(kLeftPadding,(self.streetLabel.frame.origin.y+self.streetLabel.frame.size.height), SCREEN_WIDTH-(kLeftPadding*2), kLabelHeight);
                                // NSLog(@"%f",self.StateLabel.frame.origin.y);
                             }

                        }
                        else{
                            self.streetLabel.text=kEmptyAddress;
                        }
                        if (![[userProfileObj City]isEqualToString:kEmptyString]) {
                              self.StateLabel.frame=CGRectMake(kLeftPadding,(self.streetLabel.frame.origin.y+self.streetLabel.frame.size.height), SCREEN_WIDTH-(kLeftPadding*2), kLabelHeight);
                            self.StateLabel.text=[NSString stringWithFormat:@"%@, %@ %@",[userProfileObj City],[userProfileObj State],[userProfileObj ZipCode]];
                        }
                        else{
                            self.StateLabel.text=kEmptyString;
                        }
                        UIButton *AddnewAddressButton;
                        AddnewAddressButton=[self createButtonWithFrame:CGRectMake(kLeftPadding,(self.StateLabel.frame.origin.y+self.StateLabel.frame.size.height)+(kPaddingSmall), SCREEN_WIDTH-(kRightPadding*2), 20) withTitle:@"Add a new address"];
                        NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
                        AddnewAddressButton.titleLabel.attributedText=[[NSAttributedString alloc] initWithString:@"Add a new address"
                                                                                                      attributes:underlineAttribute];
                        [AddnewAddressButton setTitleColor:[UIColor colorWithRed:0.5098 green:0.5098 blue:0.5176 alpha:1.0]
                                                  forState:UIControlStateNormal];
                        [AddnewAddressButton addTarget:self action:@selector(AddnewAddressButton:) forControlEvents:UIControlEventTouchUpInside];
                         AddnewAddressButton.titleLabel.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize + 2];
                        [AddnewAddressButton setBackgroundColor:[UIColor clearColor]];
                        AddnewAddressButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                        [cell addSubview:AddnewAddressButton];

                    }
                             }
            }
                break;
                
                           case 2:{
                               if ([OrderTypeString isEqualToString:kStringPickUp]) {
                                   titleLabel.text=kLabelSpecialInstructions;
                                   _specialInstruction=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, titleLabel.frame.origin.y+titleLabel.frame.size.height+kPaddingSmall, SCREEN_WIDTH-(kLeftPadding*2), kTEXTfieldHeight) text:specialInstructionString withTag:kTextfieldTagGreyBorder withPlaceholder:@"Special Instruction"];
                                   //[_specialInstruction setBorderStyle:UITextBorderStyleRoundedRect];
                                   [_specialInstruction setDelegate:self];
                                   [cell addSubview:_specialInstruction];

                               }
                               else{
                titleLabel.text=kLabelDeliveryDate;
                DeliveryDateButton=[self createButtonWithFrame:CGRectMake(kLeftPadding,titleLabel.frame.size.height+titleLabel.frame.origin.y+kPaddingSmall, SCREEN_WIDTH-(kLeftPadding*2), kTEXTfieldHeightSmall) withTitle:[deliveryDateAndTimeArray objectAtIndex:0]];
                                   
                [self addDeliveryDateAndTimeToCell:cell];

                               }
                break;
            }

            case 3:{
                if ([OrderTypeString isEqualToString:kStringPickUp]) {
                    {
                        if(couponCode==NO){
                            couponCodeButton=[self createButtonWithFrame:CGRectMake(kLeftPadding,(titleLabel.frame.origin.y+titleLabel.frame.size.height/2)+kTopPadding - 20, SCREEN_WIDTH-(kRightPadding*2), 20) withTitle:kLabelCouponCodeInfo];
                            NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
                            couponCodeButton.titleLabel.attributedText=[[NSAttributedString alloc] initWithString:kLabelCouponCodeInfo
                                                                                                       attributes:underlineAttribute];
                            [couponCodeButton setTitleColor:[self getColorForRedText]
                                                   forState:UIControlStateNormal];
                            [couponCodeButton addTarget:self action:@selector(couponCodeButton:) forControlEvents:UIControlEventTouchUpInside];
                            couponCodeButton.tag=12;
                            couponCodeButton.titleLabel.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize + 2];
                            [couponCodeButton setBackgroundColor:[UIColor clearColor]];
                            couponCodeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                            
                            [cell addSubview:couponCodeButton];
                            if(enablePayment == NO)
                            {
                                [self addPaymentInfoTocell:cell];
                            }
                            else
                            {//Nothing to do
                            }
                        }
                        else if(couponCode==YES){
                            [couponCodeButton addTarget:self action:@selector(removeCouponCodeButton:) forControlEvents:UIControlEventTouchUpInside];
                            UIButton *removeButton=[self createButtonWithFrame:CGRectMake(SCREEN_WIDTH-(kLeftPadding ), kPaddingSmall, kLeftPadding-10, kButtonHeight-10) withTitle:nil];
                            [removeButton setImage:[UIImage imageNamed:kImageCrossButton] forState:UIControlStateNormal];
                            [removeButton setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
                            [removeButton addTarget:self action:@selector(removeCouponCodeButton:) forControlEvents:UIControlEventTouchUpInside];
                            //removeButton.tag=9;
                            [cell addSubview:removeButton];
                            
                            NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
                            couponCodeButton.titleLabel.attributedText=[[NSAttributedString alloc] initWithString:kLabelCouponCodeInfo1
                                                                                                       attributes:underlineAttribute];
                            [couponCodeButton setTitle:kLabelCouponCodeInfo1 forState:UIControlStateNormal];
                            
                            couponCodeTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, couponCodeButton.frame.origin.y+ couponCodeButton.frame.size.height+kPaddingSmall,kGreyBorderTextfieldWidth_Large-40, kTEXTfieldHeightSmall) text:nil withTag:kTextfieldTagGreyBorder withPlaceholder:@"Enter coupon code"];
                            couponCodeTxtFd.autocapitalizationType= UITextAutocapitalizationTypeNone;
                            [couponCodeTxtFd setValue:[UIColor colorWithRed:0.4549 green:0.4549 blue:0.4667 alpha:1.0]
                                           forKeyPath:@"_placeholderLabel.textColor"];
                            [couponCodeTxtFd setDelegate:self];
                            [cell addSubview:couponCodeTxtFd];
                             couponCodeTxtFd.autocapitalizationType= UITextAutocapitalizationTypeNone;
                            UIImage *authimage=[UIImage imageNamed:kImageCouponCodeApply];
                          applyButton = [self AddCustomButtonWithRedBorder:
                                                            CGRectMake(kLeftPadding+couponCodeTxtFd.frame.size.width+8,couponCodeButton.frame.origin.y+ couponCodeButton.frame.size.height+kPaddingSmall, 58, 24)
                                                                                    withTitle:kButtonTitleCouponCode];
                            [applyButton setImage:authimage forState:UIControlStateNormal];
                            applyButton.tag=8;
                            if(!isCouponApplied){
                                applyButton.alpha=1.0;
                            [applyButton addTarget:self action:@selector(validateCouponCodeBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
                            }
                            else{
                                applyButton.alpha=0.45;
                               // [self showAlertViewWithTitle:@"message" message:[orderTotalArray valueForKey:@"ErrorMessage"]];
                            }
                            applyButton.enabled=YES;
                            [cell addSubview:applyButton];
                            [couponCodeTxtFd setTag:(5000 + indexPath.row)];
                            [cell addSubview:couponCodeButton];
                            if(enablePayment == NO)
                            {
                                [self addPaymentInfoTocell:cell];
                            }
                            else
                            {
                                //Nothing to do
                            }
                        }
                        
                    }
                    

                }
                else{
                    titleLabel.text=kLabelSpecialInstructions;
                    _specialInstruction=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, titleLabel.frame.origin.y+titleLabel.frame.size.height+kPaddingSmall, SCREEN_WIDTH-(kLeftPadding*2), kTEXTfieldHeight) text:specialInstructionString withTag:kTextfieldTagGreyBorder withPlaceholder:@"Special Instruction"];
                    //[_specialInstruction setBorderStyle:UITextBorderStyleRoundedRect];
                    [_specialInstruction setDelegate:self];
                    [cell addSubview:_specialInstruction];

                }
                
                break;
            }
                case 4:{
                    if ([OrderTypeString isEqualToString:kStringDeliver]) {
                       // titleLabel.text=kLabelPaymentInfo;
                        if(couponCode==NO){
                        couponCodeButton=[self createButtonWithFrame:CGRectMake(kLeftPadding,(titleLabel.frame.origin.y+titleLabel.frame.size.height/2)+kTopPadding - 20, SCREEN_WIDTH-(kRightPadding*2), 20) withTitle:kLabelCouponCodeInfo];
                        NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
                        couponCodeButton.titleLabel.attributedText=[[NSAttributedString alloc] initWithString:kLabelCouponCodeInfo
                                                                                                      attributes:underlineAttribute];
                        [couponCodeButton setTitleColor:[self getColorForRedText]
                                                  forState:UIControlStateNormal];
                        [couponCodeButton addTarget:self action:@selector(couponCodeButton:) forControlEvents:UIControlEventTouchUpInside];
                            couponCodeButton.tag=12;
                             couponCodeTxtFd.autocapitalizationType= UITextAutocapitalizationTypeNone;
                        couponCodeButton.titleLabel.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize + 2];
                        [couponCodeButton setBackgroundColor:[UIColor clearColor]];
                        couponCodeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
                         [cell addSubview:couponCodeButton];
                    }
                    else if(couponCode==YES){
                        [couponCodeButton addTarget:self action:@selector(removeCouponCodeButton:) forControlEvents:UIControlEventTouchUpInside];
                         UIButton *removeButton=[self createButtonWithFrame:CGRectMake(SCREEN_WIDTH-(kLeftPadding ), kPaddingSmall, kLeftPadding-10, kButtonHeight-10) withTitle:nil];
                        [removeButton setImage:[UIImage imageNamed:kImageCrossButton] forState:UIControlStateNormal];
                        [removeButton setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
                        [removeButton addTarget:self action:@selector(removeCouponCodeButton:) forControlEvents:UIControlEventTouchUpInside];
                        //removeButton.tag=9;
                        [cell addSubview:removeButton];

                        NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
                        couponCodeButton.titleLabel.attributedText=[[NSAttributedString alloc] initWithString:kLabelCouponCodeInfo1
                                                                                                   attributes:underlineAttribute];
                         [couponCodeButton setTitle:kLabelCouponCodeInfo1 forState:UIControlStateNormal];

                        couponCodeTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, couponCodeButton.frame.origin.y+ couponCodeButton.frame.size.height+kPaddingSmall,kGreyBorderTextfieldWidth_Large-40, kTEXTfieldHeightSmall) text:nil withTag:kTextfieldTagGreyBorder withPlaceholder:@"Enter coupon code"];
                         couponCodeTxtFd.autocapitalizationType= UITextAutocapitalizationTypeNone;
                        [couponCodeTxtFd setValue:[UIColor colorWithRed:0.4549 green:0.4549 blue:0.4667 alpha:1.0]
                                      forKeyPath:@"_placeholderLabel.textColor"];
                        [couponCodeTxtFd setDelegate:self];
                        [cell addSubview:couponCodeTxtFd];
                        UIImage *authimage=[UIImage imageNamed:kImageCouponCodeApply];
                       applyButton = [self AddCustomButtonWithRedBorder:
                                                     CGRectMake(kLeftPadding+couponCodeTxtFd.frame.size.width+8,couponCodeButton.frame.origin.y+ couponCodeButton.frame.size.height+kPaddingSmall, 58, 24)
                                                                             withTitle:kButtonTitleCouponCode];
                        [applyButton setImage:authimage forState:UIControlStateNormal];
                        applyButton.tag=8;
                        
                        if(!isCouponApplied){
                            applyButton.alpha=1.0;
                            [applyButton addTarget:self action:@selector(validateCouponCodeBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
                        }
                        else{
                            applyButton.alpha=0.45;
                            //[self showAlertViewWithTitle:@"message" message:[orderTotalArray valueForKey:@"ErrorMessage"]];
                        }
                        
                       // applyButton.enabled=YES;
                        [cell addSubview:applyButton];
                        [couponCodeTxtFd setTag:(5000 + indexPath.row)];
                          [cell addSubview:couponCodeButton];
                    }
                    
                    }
                    else{
                        if(enablePayment == NO)
                        { titleLabel.text=@"";}
                        else
                        {
                            titleLabel.text = kLabelPaymentInfo;
                        UIImage *authimage=[UIImage imageNamed:kImageAuthorize];
                        
                        UIButton *authriseNetButton=[self createButtonWithFrame:CGRectMake((SCREEN_WIDTH-(authimage.size.width/2))/2, (titleLabel.frame.origin.y+titleLabel.frame.size.height/2)+kTopPadding - 3, authimage.size.width/2, authimage.size.height/2) withTitle:kEmptyString];
                        [authriseNetButton setImage:authimage forState:UIControlStateNormal];
                        [cell addSubview:authriseNetButton];
                        cardnumberTextfd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, authriseNetButton.frame.origin.y+ authriseNetButton.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_Large, kTEXTfieldHeight) text:kEmptyString withTag:kTextfieldTagGreyBorder withPlaceholder:kPlaceHolderEnterCard];
                        DLog(@"cardDeatailArray=======%@",cardDeatailArray);
                        cardnumberTextfd.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize+ 1];
                        [cardnumberTextfd setValue:[self getColorForBlackText] forKeyPath:@"_placeholderLabel.textColor"];
                        [cardnumberTextfd setKeyboardType:UIKeyboardTypeNumberPad];
                        [cell addSubview:cardnumberTextfd];
                        UIImage *cameraImge=kimageCamera;
                        
                        cameraButton=[self createButtonWithFrame:CGRectMake(cardnumberTextfd.frame.origin.x+cardnumberTextfd.frame.size.width-(kTEXTfieldHeight), authriseNetButton.frame.origin.y+ authriseNetButton.frame.size.height+kPaddingSmall, kTEXTfieldHeight, kTEXTfieldHeight) withTitle:kEmptyString];
                        [cameraButton addTarget:self action:@selector(cameraButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                        [cameraButton setImage:cameraImge forState:UIControlStateNormal];
                        [cell addSubview:cameraButton];
                        }
                        [self addPaymentInfoTocell:cell];

                    }
                }
                break;
            case 5:{
                if ([OrderTypeString isEqualToString:kStringDeliver]) {
                     titleLabel.text=kLabelPaymentInfo;
                    UIImage *authimage=[UIImage imageNamed:kImageAuthorize];
                    
                    UIButton *authriseNetButton=[self createButtonWithFrame:CGRectMake((SCREEN_WIDTH-(authimage.size.width/2))/2, (titleLabel.frame.origin.y+titleLabel.frame.size.height/2)+kTopPadding - 3, authimage.size.width/2, authimage.size.height/2) withTitle:kEmptyString];
                    [authriseNetButton setImage:authimage forState:UIControlStateNormal];
                    [cell addSubview:authriseNetButton];
                    cardnumberTextfd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, authriseNetButton.frame.origin.y+ authriseNetButton.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_Large, kTEXTfieldHeight) text:kEmptyString withTag:kTextfieldTagGreyBorder withPlaceholder:kPlaceHolderEnterCard];
                    DLog(@"cardDeatailArray=======%@",cardDeatailArray);
                    cardnumberTextfd.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize+ 1];
                    [cardnumberTextfd setValue:[self getColorForBlackText] forKeyPath:@"_placeholderLabel.textColor"];
                    [cardnumberTextfd setKeyboardType:UIKeyboardTypeNumberPad];
                    [cell addSubview:cardnumberTextfd];
                    UIImage *cameraImge=kimageCamera;
                    
                    cameraButton=[self createButtonWithFrame:CGRectMake(cardnumberTextfd.frame.origin.x+cardnumberTextfd.frame.size.width-(kTEXTfieldHeight), authriseNetButton.frame.origin.y+ authriseNetButton.frame.size.height+kPaddingSmall, kTEXTfieldHeight, kTEXTfieldHeight) withTitle:kEmptyString];
                    [cameraButton addTarget:self action:@selector(cameraButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                    [cameraButton setImage:cameraImge forState:UIControlStateNormal];
                    [cell addSubview:cameraButton];
                    [self addPaymentInfoTocell:cell];

                
                }
                
                
                break;
            }
            default:
                break;
        }
   // }
    return cell;
}
-(void)payAtNow:(id)sender
{
    NSLog(@"Please work");
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    /* if (indexPath.row==0) {
        if ([OrderTypeString isEqualToString:kStringPickUp]) {
            if(couponCode!=YES){
            return  kRowHeight_orderconfirmation;
            }
            else{
                return  kRowHeight_orderconfirmation+20;

            }
        }
        else{
            if(couponCode!=YES){
            return  kRowHeight_orderconfirmation+(kLabelHeight*2);
            }
            else{
                return  kRowHeight_orderconfirmation+(kLabelHeight*2)+20;
            }
        }
    }
    if ((indexPath.row==1)&& enterNewAddress==YES && [OrderTypeString isEqualToString:kStringDeliver]) {
   
    return kRowHeight_orderconfirmation + 50 + 120;
    
    }
    else  if ((indexPath.row==1)&& enterNewAddress==NO && [OrderTypeString isEqualToString:kStringDeliver]){
        if(numberofLine>1){
            return kRowHeight_orderconfirmation +labelHeight+5 ;
        }
        else{
             return kRowHeight_orderconfirmation+5;
        }
       }
    
    if ([OrderTypeString isEqualToString:kStringPickUp]) {
           if (indexPath.row==2) {
            return kRowHeight_orderconfirmationSmall;
        }
        else if (indexPath.row==3){
            if (couponCode==YES) {
            return kRowHeight_orderconfirmation;
            }
            else{
                return 70;
            }
           
        }
        
        else if (indexPath.row==4){
            if(enablePayment == NO)
                return kRowHeight_orderconfirmation;
            else
            return  kRowHeight_orderconfirmationLarge;
        }
        else    {
            return kRowHeight_orderconfirmation;
        }
        
    
    }
    else{
        if (indexPath.row==2) {
            return krowHeightDeliveryDateAndTime;
        }
        
        if (indexPath.row==3) {
            return kRowHeight_orderconfirmationSmall;
        }
        if (indexPath.row==4){
            if (couponCode==YES) {
            return kRowHeight_orderconfirmation;
            }
            else{
                return 70;
            }
            
        }
        if (indexPath.row==5) {
            return kRowHeight_orderconfirmationLarge;
        }
        else    {
           return kRowHeight_orderconfirmation+20;
        }
    
    } */
    
    if ([OrderTypeString isEqualToString:kStringPickUp]) {
          if (indexPath.row==0) {
              if(couponCode!=YES){
                  return  kRowHeight_orderconfirmation;
              }
              else{
                  return  kRowHeight_orderconfirmation+20;
            }
          }
        if (indexPath.row==2) {
            return kRowHeight_orderconfirmationSmall;
        }
        else if(indexPath.row==3){
            if(couponCode==YES) {
                return kRowHeight_orderconfirmation;
            }
            else{
                if(enablePayment == NO)
                  return 120;
                else
                  return 70;
            }
        }
        else if (indexPath.row==4){
            if(enablePayment == NO)
                return kRowHeight_orderconfirmation;
            else
                return  kRowHeight_orderconfirmationLarge;
        }
        else    {
            return kRowHeight_orderconfirmation;
        }
    }
    else if([OrderTypeString isEqualToString:kStringDeliver])
    {
        if(indexPath.row == 0)
        {
            if(couponCode!=YES){
                return  kRowHeight_orderconfirmation+(kLabelHeight*2);
            }
            else{
                return  kRowHeight_orderconfirmation+(kLabelHeight*2)+20;
            }
        }
      if (indexPath.row==1)
      {
          if(enterNewAddress==YES)
                return kRowHeight_orderconfirmation + 50 + 120;
          else if(enterNewAddress == NO)
          {
              if(numberofLine>1){
                  return kRowHeight_orderconfirmation +labelHeight+5 ;
              }
              else{
                  return kRowHeight_orderconfirmation+5;
              }
          }
      }
     if (indexPath.row==2) {
     return krowHeightDeliveryDateAndTime;
     }
     
     if (indexPath.row==3) {
     return kRowHeight_orderconfirmationSmall;
     }
     if (indexPath.row==4){
     if (couponCode==YES) {
     return kRowHeight_orderconfirmation;
     }
     else{
     return 70;
     }
     
     }
     if (indexPath.row==5) {
     return kRowHeight_orderconfirmationLarge-60;
     }
     else    {
     return kRowHeight_orderconfirmation;
     }
    }
    else
        return 0;
}

-(BOOL)isClosedStore:(NSDate*)date {
     dd=date;
     isclosed=NO;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-mm-dd hh:mm:ss zzz"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    /* NSString *stringFromDate = [dateFormatter stringFromDate:date];
      NSDate *date1 = [dateFormatter dateFromString:stringFromDate];
      NSCalendar *calendar = [NSCalendar currentCalendar];
      NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date1];
      NSInteger hour = [components hour];
      NSInteger minute = [components minute]; */
    
    
    __weak ClosestStoreModel *     lClosestStoreModelObj = kGetClosestStoreModel;
    for (StoreOpenCloseTime * storeOpenTime in lClosestStoreModelObj.GetStoreOpenclose) {
        DLog(@"%@-%@",[storeOpenTime.DayID stringByReplacingOccurrencesOfString:@"." withString:@""],[self GetWeekdayNameFromGivenDate:date])
        if ([[storeOpenTime.DayID stringByReplacingOccurrencesOfString:@"." withString:@""] isEqualToString:[self GetWeekdayNameFromGivenDate:date]]) {
            
            DLog(@"StoreOpenTime=%@ StoreCloseTime=%@",storeOpenTime.StoreOpenTime,storeOpenTime.StoreCloseTime)
                    if ([storeOpenTime.StoreOpenTime isEqualToString:storeOpenTime.StoreCloseTime]||[storeOpenTime.StoreOpenTime isEqualToString:kEmptyString]||[storeOpenTime.StoreCloseTime isEqualToString:kEmptyString])
                    {
                        
                        isclosed=YES;
                         break;
                
            }
            
//                    else if(hour>4 && minute >45){
//                        isclosed=YES;
//                    }
            
        }
        
      
    
    }
    return isclosed;

}
-(void)resetDeliveryTime{
    
    [deliveryDateAndTimeArray replaceObjectAtIndex:1 withObject:@"From"];
    [deliveryDateAndTimeArray replaceObjectAtIndex:2 withObject:@"To"];
    [DeliveryTimeFromButton setTitle:[deliveryDateAndTimeArray objectAtIndex:1] forState:UIControlStateNormal];
    [DeliveryTimeToButton setTitle:[deliveryDateAndTimeArray objectAtIndex:2] forState:UIControlStateNormal];
}

-(BOOL)isStoreTimeSurpassed:(NSMutableArray*)pickerArray{
    BOOL isStoreTimeSurpassed;
    
//    if (<#condition#>) {
//        <#statements#>
//    }
    
    return isStoreTimeSurpassed;


}

#pragma mark-BUTTON ACTION
-(void)closeClicked
{
    [transparentlayer removeFromSuperview];
    [alertBodyView removeFromSuperview];
}
-(void)openPrivacyPolicy
{
    NSLog(@"popupview");
    
    /********** Creating pop up when click on privacy Button********************/
    
    //Create transparent layer for popup background
    transparentlayer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    transparentlayer.backgroundColor=[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:140.0/255.0 alpha:0.7];
    [self.view addSubview:transparentlayer];
    
    //Creating custom alertview
    alertBodyView  = [[UIView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH/8)-20.0, (SCREEN_HEIGHT/6)-10.0,SCREEN_WIDTH-40.0,SCREEN_HEIGHT-120.0)];  //241 27 46
    alertBodyView.layer.zPosition = 10;
    alertBodyView.layer.borderWidth =1.0f;
    alertBodyView.layer.borderColor = [UIColor colorWithRed:241.0/255/.0 green:27.0/255.0 blue:46.0/255.0 alpha:1.0].CGColor;
    alertBodyView.layer.cornerRadius = 10;
    alertBodyView.backgroundColor = [UIColor whiteColor];
    [transparentlayer addSubview:alertBodyView];
    
    // Add Header view,
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0,1,alertBodyView.frame.size.width,40)];
    headerView.backgroundColor=[UIColor colorWithRed:241.0/255/.0 green:27.0/255.0 blue:46.0/255.0 alpha:1.0];
    //Code for top left radius and top right radius
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:headerView.bounds byRoundingCorners:( UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    headerView.layer.mask = maskLayer;
    //End
    [alertBodyView addSubview:headerView];
    
    //Add title Label on Header view
    UILabel *headerLabel = [self createLblWithRect:CGRectMake(0, 0, headerView.frame.size.width, headerView.frame.size.height) font:[self addFontBold] text:@"Privacy Policy" textAlignment:NSTextAlignmentCenter textColor:[UIColor whiteColor]];
    [headerView addSubview:headerLabel];
    
    //Add close button Header View
    UIImage *closeImage = [UIImage imageNamed:@"close-icon"];
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    closeButton.frame = CGRectMake(headerView.frame.size.width-45.0, headerView.frame.size.height-37.0, closeImage.size.width, closeImage.size.height);
    [closeButton setTitle:kEmptyString forState:UIControlStateNormal];
    closeButton.backgroundColor=[UIColor clearColor];
    [closeButton addTarget:self action:@selector(closeClicked) forControlEvents:UIControlEventTouchUpInside];
    [closeButton setBackgroundImage:closeImage forState:UIControlStateNormal];
    [headerView addSubview:closeButton];
    
    UIWebView *webView = [[UIWebView alloc]initWithFrame:CGRectMake(3,40,alertBodyView.frame.size.width-5.0,alertBodyView.frame.size.height-60.0)];
    webView.backgroundColor=[UIColor clearColor];
    webView.layer.cornerRadius = 10;
    //NSString *privacyHTMLString = privacyPolicyText;
    [webView loadHTMLString:privacyPolicyText baseURL:nil];
    [alertBodyView addSubview:webView];
    
}
-(NSDate*)getCurrentTime{
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = deliveryTimeFormat;
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSLog(@"The Current Time is %@",[dateFormatter stringFromDate:now]);
    return now;

}
-(NSString *)formatServerDateAndTime:(NSString *)dateWithTime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
    NSDate *stringDate = [dateFormatter dateFromString:dateWithTime];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:stringDate];
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    
    NSString *localFromString = [NSString stringWithFormat:@"%ld:%ld %@",hour,minute,timeMode];
    return localFromString;
}
-(void)DeliveryTimeButtonClicked:(UIButton*)button{
    
    NSString *storeCloseTimeString;
   
   
    //NSLog(@"fromTimeString = %@",fromTimeString);
    //NSLog(@"CurrTimeStore = %@",currTimStore);
   
    fromTimeString = [self formatServerDateAndTime:currTimStore];
    
     __weak ClosestStoreModel *     lClosestStoreModelObj = kGetClosestStoreModel;
    for (StoreOpenCloseTime * storeOpenTime in lClosestStoreModelObj.GetStoreOpenclose) {
        if ([storeOpenTime.DayID isEqualToString:[self GetWeekdayNameFromGivenDate:self.datePicker.date]]) {
            storeCloseTimeString = storeOpenTime.StoreCloseTime;
        }
    }

    
    [self.dropDownView removeFromSuperview];
    if ([self isClosedStore:self.datePicker.date]==YES) {
        if ([self GetWeekdayNameFromGivenDate:self.datePicker.date]==nil) {
            [self showAlertViewWithTitle:[NSString stringWithFormat:@"%@ on %@",kAlertStoreISClosed,[self GetWeekdayNameFromGivenDate:[NSDate date]]] message:nil];
        }
        
        
        else{
            [self showAlertViewWithTitle:[NSString stringWithFormat:@"%@ on %@",kAlertStoreISClosed,[self GetWeekdayNameFromGivenDate:self.datePicker.date]] message:nil];
            
            
        }
        [self resetDeliveryTime];


    }
   /*  else if([deliveryDateString isEqualToString:[self getCurrentDate]])
    {
        
        NSDate *storeCloseTime = [self getDateFromString:storeCloseTimeString];
        NSDate *currSerTime = [self getDateFromString:fromTimeString];
        NSTimeInterval interval = [currSerTime timeIntervalSinceDate: storeCloseTime];//[date1 timeIntervalSince1970] - [date2 timeIntervalSince1970];
        if(interval < 0)
        {
            NSDate *nxtDate = [self getNextDayDate:[self getDateFromDateString:deliveryDateString]];
            [DeliveryDateButton setTitle:[NSString stringWithFormat:@"%@",[self getstringFromDate:nxtDate]] forState:UIControlStateNormal];
        }
        else
        {
             [self  addDefaultPicker];
        }
    
    } */
    // else if (hour >= 17 && [deliveryDateString isEqualToString:stringDate]) {
       // [self showAlertViewWithTitle:@"Store closed!" message:nil];
        
  //  }
    
    else{
    
       [self  addDefaultPicker];
   
    }
        
  //  }
}
-(void)DeliveryDateButtonClicked:(UIButton*)button{
    
    [self.dropDownView removeFromSuperview];
    [self adddefaultDatePicker];
}

-(void)UnSaveDeliveryAddress{
    if (AdressValidatorDict !=nil) {
        [AdressValidatorDict removeAllObjects];
        
    }
}

-(void)payNowButton:(UIButton*)sender{
    if ([OrderTypeString isEqualToString:kStringDeliver]) {
        if (![deliveryDateString length]) {
            [self showAlertViewWithTitle:@"Please select Delivery Date." message:nil];
            return;
            
        }
        if (![deliveryTimeFromString length]){
            [self showAlertViewWithTitle:@"Please select Delivery Time." message:nil];
            return;
        }
        if (![deliveryTimeToString length]){
            [self showAlertViewWithTitle:@"Please select Delivery Time." message:nil];
            return;
            
        }
       
    }
  
   
    if (![CouponCodeValidatorString isEqualToString:@"Success"] && couponCode==YES){
        [self showAlertViewWithTitle:@"Please click on apply button to avail discount." message:nil];
        return;
        
    }
           if (![self.creditCardBuf length]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:NSLocalizedString(@"A card number is required to continue.", @"")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                              otherButtonTitles:nil];
        [alert setTag:kCardNumberErrorAlert];
        [alert show];
        
        return;
    }
    
    if(!scanFound){
        if (![CreditCardType isValidCreditCardNumber:self.creditCardBuf]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:NSLocalizedString(@"Please enter a valid card number.", @"")
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                  otherButtonTitles:nil];
            [alert setTag:kCardNumberErrorAlert];
            [alert show];
            
            return;
        }
    }
    
    
    if (![monthTextfd.text length]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:NSLocalizedString(@"An expiration date is required to continue.", @"")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                              otherButtonTitles:nil];
        [alert setTag:kCardExpirationErrorAlert];
        [alert show];
        
        return;
    }
    if(!scanFound)
    {
        if ([self.expirationBuf length] != EXPIRATION_DATE_LENGTH) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:NSLocalizedString(@"Please enter a valid expiration date.", @"")
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                  otherButtonTitles:nil];
            [alert setTag:kCardExpirationErrorAlert];
            [alert show];
            
            return;
        }
    }
    else {
        // Validate
        NSArray *components = [[monthTextfd text] componentsSeparatedByString:@"/"];
        NSString *month = [components objectAtIndex:0];
        NSString *year = [components objectAtIndex:1];
        
        // Check to see if month is correct
        if ([month intValue] == 0 || [month intValue] > 12) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:NSLocalizedString(@"Please enter a valid expiration date.", @"")
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                  otherButtonTitles:nil];
            [alert setTag:kCardExpirationErrorAlert];
            [alert show];
            
            return;
        }
        
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        NSDate *currentDate = [NSDate date];
        
        // Convert date object to desired output format
        [dateFormat setDateFormat:@"M/yyyy"];
        NSString *currentDateString = [dateFormat stringFromDate:currentDate];
        components = [currentDateString componentsSeparatedByString:@"/"];
        NSString *currentMonth = [components objectAtIndex:0];
        NSString *currentYear = [[components objectAtIndex:1] substringFromIndex:2];
        
        
        
        // Check if we are correct
        if ([year intValue] == [currentYear intValue]) {
            if ([month intValue] < [currentMonth intValue]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                message:NSLocalizedString(@"Please enter a valid expiration date.", @"")
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                      otherButtonTitles:nil];
                [alert setTag:kCardExpirationErrorAlert];
                [alert show];
                
                return;
            }
        } else if ([year intValue] < [currentYear intValue]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:NSLocalizedString(@"Please enter a valid expiration date.", @"")
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                  otherButtonTitles:nil];
            [alert setTag:kCardExpirationErrorAlert];
            [alert show];
            
            return;
        }
    }
    if([cvvTextfd.text length] > kCVV2Length || [cvvTextfd.text length]<3 )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:NSLocalizedString(@"Please enter a valid Cvv number.", @"")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                              otherButtonTitles:nil];
        [alert setTag:kCvvErrorAlert];
        [alert show];
        return;
    }
    if([zipTextField.text length] != kZipLength)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:NSLocalizedString(@"Please enter a valid Zip code.", @"")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                              otherButtonTitles:nil];
        [alert setTag:kZipAlert];
        [alert show];
        return;
    }
    payTypeString=kStringPayNow;
    
    if(addressTxtFd != nil && (!(addressTxtFd.text == kEmptyString && cityTxtFd.text == kEmptyString && stateTxtFd.text == kEmptyString
                                 && zipTxtFd.text == kEmptyString && firstNameTxtfd.text == kEmptyString && lastNameTxtfd.text == kEmptyString
                                 && phoneNumberTxtfd.text == kEmptyString)))  {
        if ([firstNameTxtfd.text length]==0) {
            [self showAlertViewWithTitle:nil message:kAlertEnterfirstName];
            return;
        }
        else if ([lastNameTxtfd.text length]==0){
            [self showAlertViewWithTitle:nil message:kAlertEnterLastName];
            return;
        }
        else if ([addressTxtFd.text length]==0){
            [self showAlertViewWithTitle:nil message:kAlertEnterAddress];
            return;
        }
//        else if ([apartmentTxtFd.text length]==0){
//            [self showAlertViewWithTitle:nil message:kAlertEnterApartment];
//            return;
//        }
        else if ([cityTxtFd.text length]==0){
            [self showAlertViewWithTitle:nil message:kAlertEnterCity];
            return;
        }
        else if ([stateTxtFd.text length]==0){
            [self showAlertViewWithTitle:nil message:kAlertEnterState];
            return;
        }
        else if ([zipTxtFd.text length]==0){
            [self showAlertViewWithTitle:nil message:kAlertEnterZipCode];
            return;
        }
        else if ([phoneNumberTxtfd.text length]==0){
            [self showAlertViewWithTitle:nil message:kAlertEnterPhone];
            return;
        }
        else    {
            [self callApiAddressValidator];
        }
    }
    else    {
        if([OrderTypeString isEqualToString:kStringPickUp]) {
            [self callApiGetAuthorizeCredentialsEncrypt];
        }
        else    {
            AdressValidatorDict = nil;
            __weak UserProfile *userProfileObj = kgetUserProfileModel;
            __weak User *userObj = kGetUserModel;
            if ((![[userProfileObj Address] isEqualToString:kEmptyString]) && (![[userProfileObj City]isEqualToString:kEmptyString])) {
                
                NSMutableDictionary *userAddressDict = [NSMutableDictionary dictionary];
                [userAddressDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
                [userAddressDict setObject:userObj.userId forKey: kParamUserId];
                [userAddressDict setObject:userProfileObj.FirstName forKey: kParamFirstName];
                [userAddressDict setObject:userProfileObj.LastName forKey: kParamLastName];
                [userAddressDict setObject:userProfileObj.Address forKey: kParamAddress];
                 [userAddressDict setObject:userProfileObj.Address2 forKey: kParamApartment];
                [userAddressDict setObject:userProfileObj.City forKey: kParamCity];
                [userAddressDict setObject:userProfileObj.State forKey: kParamState];
                [userAddressDict setObject:kEmptyString forKey: kParamCountryId];
                [userAddressDict setObject:userProfileObj.ZipCode forKey: kParamZip];
                [userAddressDict setObject:userProfileObj.ContactNo forKey: kParamPhone];
                
                [self showSpinner1];
                [[ModelManager modelManager] GetAddressValidator:userAddressDict successStatus:^(APIResponseStatusCode statusCode, id response) {
                    [self hideSpinner];
                    AdressValidatorString=[response valueForKey:@"Status"];
                    if ([AdressValidatorString intValue]==1) {
                        [self callApiGetAuthorizeCredentialsEncrypt];
                    }
                    else{
                        [self showAlertViewWithTitle:nil message:[response valueForKey:@"Message"]];
                    }
                } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
                    [self hideSpinner];
                    [UIView showMessageWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage] showInterval:5.0  viewController:self];
                    DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
                }];
            }
            else    {
                [self showAlertViewWithTitle:nil message: @"Please enter delivery address."];
            }
        }
    }
}
-(void)validateCouponCodeBtnPressed:(UIButton *) sender    {
    if(couponCodeTxtFd.text != kEmptyString){
        if (!([couponCodeTxtFd.text length]==0)) {
         textfieldData =couponCodeTxtFd.text;
            [self callApivalidateCouponCode];
       
            return;
            
        }
    }
}
-(void)validateAddressBtnPressed:(UIButton *) sender    {
    if(addressTxtFd != nil && (!(addressTxtFd.text == kEmptyString && cityTxtFd.text == kEmptyString && stateTxtFd.text == kEmptyString
                                 && zipTxtFd.text == kEmptyString && firstNameTxtfd.text == kEmptyString && lastNameTxtfd.text == kEmptyString
                                 && phoneNumberTxtfd.text == kEmptyString)))  {
        if ([firstNameTxtfd.text length]==0) {
            [self showAlertViewWithTitle:nil message:kAlertEnterfirstName];
            return;
        }
        else if ([lastNameTxtfd.text length]==0){
            [self showAlertViewWithTitle:nil message:kAlertEnterLastName];
            return;
        }
        else if ([addressTxtFd.text length]==0){
            [self showAlertViewWithTitle:nil message:kAlertEnterAddress];
            return;
        }
//        else if ([apartmentTxtFd.text length]==0){
//            [self showAlertViewWithTitle:nil message:kAlertEnterApartment];
//            return;
//        }
        else if ([cityTxtFd.text length]==0){
            [self showAlertViewWithTitle:nil message:kAlertEnterCity];
            return;
        }
        else if ([stateTxtFd.text length]==0){
            [self showAlertViewWithTitle:nil message:kAlertEnterState];
            return;
        }
        else if ([zipTxtFd.text length]==0){
            [self showAlertViewWithTitle:nil message:kAlertEnterZipCode];
            return;
        }
        else if ([zipTxtFd.text length]<5){
            [self showAlertViewWithTitle:nil message:kAlertValidZipCode];
            return;
        }
        else if ([zipTxtFd.text length]>5){
            [self showAlertViewWithTitle:nil message:kAlertValidZipCode];
            return;
        }

        else if ([phoneNumberTxtfd.text length]==0){
            [self showAlertViewWithTitle:nil message:kAlertEnterPhone];
            return;
        }
        else if ([phoneNumberTxtfd.text length]<10){
            [self showAlertViewWithTitle:nil message:kAlertValidPhoneNumber];
            return;
        }
        else    {
            [self callApivalidateAddressAsUserEnterAddress];
        }
    }
}

- (void) saveCreditCardInfo {
    AuthNet *an ;
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"IsLive"] intValue]==0) {
        an =   [AuthNet initWithAPILoginID:[[NSUserDefaults standardUserDefaults]objectForKey:@"AppLoginId"] andTransactionKey:[[NSUserDefaults standardUserDefaults]objectForKey:@"AppTransactionKey"] forEnvironment:ENV_TEST];
        
    }
    else{
        an =  [AuthNet initWithAPILoginID:[[NSUserDefaults standardUserDefaults]objectForKey:@"AppLoginId"] andTransactionKey:[[NSUserDefaults standardUserDefaults]objectForKey:@"AppTransactionKey"] forEnvironment:ENV_LIVE];
        
    }
    
    
    [an setDelegate:self];
    
    CreditCardType *c = [CreditCardType creditCardType];
    
    
    if(scanFound)
    {
        
        c.cardNumber = scanCerditCard;
    }
    else
    {
        c.cardNumber = self.creditCardBuf;
    }
    
    // c.cardNumber = self.creditCardBuf;
    c.expirationDate = [monthTextfd.text stringByReplacingOccurrencesOfString:kSlash withString:@""];
    if ([cvvTextfd.text length]) {
        c.cardCode = [NSString stringWithString:cvvTextfd.text];
    }
    expirationDateString=c.expirationDate;
    
    
    CustomerAddressType *b=[CustomerAddressType customerAddressType];
    NSMutableDictionary *userDict=[[NSUserDefaults standardUserDefaults] objectForKey:@"UserData"];
   // __weak User *userObj = kGetUserModel;
    __weak UserProfile *userprofileObj = kgetUserProfileModel;
    

        __weak ClosestStoreModel* lClosestStoreModelObj;
        __weak StoreDetails *storeDetails;
        lClosestStoreModelObj = kGetClosestStoreModel;
        
        if (lClosestStoreModelObj==nil) {
            __weak User *userObj = [[ModelManager modelManager] getuserModel];
            NSMutableDictionary* StoreDetailDict = [[ NSMutableDictionary alloc] init];
            [StoreDetailDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
            [StoreDetailDict setObject:userObj.userId forKey: kParamUserId];
            
            [self showSpinner]; // To show spinner
            [[ModelManager modelManager] GetStoreDetailWithParam:StoreDetailDict  successStatus:^(BOOL status) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [self hideSpinner];
                    __weak ClosestStoreModel* lClosestStoreModelObj;
                    __weak StoreDetails *storeDetails;
                    lClosestStoreModelObj = kGetClosestStoreModel;
                    storeDetails = kGetStoreDetailsModel;
                    b.firstName         = userprofileObj.FirstName;
                    b.lastName          = userprofileObj.LastName;
                    b.phoneNumber       = userprofileObj.ContactNo;
                    b.address           = storeDetails.StoreUserAddress1;
                  //  b.address2          = storeDetails.StoreUserApartment;
                    b.city              = storeDetails.StoreUserCity;
                    b.state             = storeDetails.StoreStateName;
                    b.country           = storeDetails.StoreUserAddress1;
                    b.zip               = zipTextField.text;
                });
            } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
                [self hideSpinner];
                [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
                DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
            }];
            


        }
        else
        {
            storeDetails = kGetStoreDetailsModel;
            b.firstName         = userprofileObj.FirstName;
            b.lastName          = userprofileObj.LastName;
            b.phoneNumber       = userprofileObj.ContactNo;
            b.address           = storeDetails.StoreUserAddress1;
            b.address2          = storeDetails.StoreUserApartment;
            b.city              = storeDetails.StoreUserCity;
            b.state             = storeDetails.StoreStateName;
            b.country           = storeDetails.StoreUserAddress1;
            b.zip               = zipTextField.text;
    
    }


    CustomerDataType *data=[CustomerDataType customerDataType];
    data.email=[userDict objectForKey:@"email"];
    
    
    PaymentType *paymentType = [PaymentType paymentType];
    paymentType.creditCard = c;
    
    ExtendedAmountType *extendedAmountTypeTax = [ExtendedAmountType extendedAmountType];
    NSArray *taxArr=[totaltax componentsSeparatedByString:@"$"];
    extendedAmountTypeTax.amount = [taxArr objectAtIndex:1] ;
    extendedAmountTypeTax.name = @"Tax";
    
    ExtendedAmountType *extendedAmountTypeShipping = [ExtendedAmountType extendedAmountType];
    extendedAmountTypeShipping.amount = @"0";
    extendedAmountTypeShipping.name = @"Shipping";
    
    TransactionRequestType *requestType = [TransactionRequestType transactionRequest];
    LineItemType *lineItem;
    NSMutableArray *lineArr=[[NSMutableArray alloc]init];;
    switch (oType) {
        case kMyorder:
        { LineItemType *lineItem;
            float total=0.0;
            for (int i=0; i<orderArray.count; i++) {
                
                Order *order=[orderArray objectAtIndex:i];
                lineItem = [LineItemType lineItem];
                NSString *product_Name=order.productName;
                if(product_Name.length>31)
                {
                    product_Name=[product_Name substringToIndex:30];
                    lineItem.itemName = product_Name;
                }
                else
                {
                    lineItem.itemName = order.productName;
                }
                
                lineItem.itemDescription = order.productName;
                lineItem.itemQuantity = order.productQnty;
                lineItem.itemPrice = order.productPrice;
                lineItem.itemID = order.productID;
                
                total=total+[order.productTotalCost floatValue];
                TotalAmount=[NSString stringWithFormat:@"%.2f",total];
                if(order.productTax.length >0)
                    lineItem.itemTaxable=YES;
                else
                {
                    lineItem.itemTaxable=NO;
                }
                [lineArr addObject:lineItem];
            }
            
        }
            break;
        case kPlaceorder:
        {
            for (int i=0; i<orderArray.count; i++) {
                
                
                lineItem = [LineItemType lineItem];
                
                NSString *product_Name=[[orderArray objectAtIndex:i ] objectForKey:@"ProductName"];
                if(product_Name.length>31)
                {
                    product_Name=[product_Name substringToIndex:30];
                    lineItem.itemName = product_Name;
                }
                else
                {
                    lineItem.itemName = [[orderArray objectAtIndex:i ] objectForKey:@"ProductName"];
                }
                // lineItem.itemName = [[orderArray objectAtIndex:i ] objectForKey:@"ProductName"];
                lineItem.itemDescription = [[orderArray objectAtIndex:i ] objectForKey:@"ProductName"];
                lineItem.itemQuantity = [[orderArray objectAtIndex:i ] objectForKey:@"Quantity"];
                lineItem.itemPrice = [[orderArray objectAtIndex:i ] objectForKey:@"Product_Price"];
                lineItem.itemID = [[orderArray objectAtIndex:i ] objectForKey:@"ProductId"];
                if([[[orderArray objectAtIndex:i] objectForKey:@"ProductTax"] length]>0)
                    lineItem.itemTaxable=YES;
                else
                {
                    lineItem.itemTaxable=NO;
                }
                [lineArr addObject:lineItem];
                
            }
        }
            
            break;
            
        default:
            break;
    }
    
    //  requestType.lineItems = lineArr;
    requestType.amount = TotalAmount;
    requestType.payment = paymentType;
    requestType.tax = extendedAmountTypeTax;
    requestType.shipping = extendedAmountTypeShipping;
    requestType.customer=data;
    requestType.billTo=b;
    [self showSpinner1];
    
    
    CreateTransactionRequest *request = [CreateTransactionRequest createTransactionRequest];
    request.transactionRequest = requestType;
    request.transactionType = AUTH_ONLY;

    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    request.anetApiRequest.merchantAuthentication.mobileDeviceId = [[oNSUUID UUIDString]stringByReplacingOccurrencesOfString:@"-" withString:@"_"];
    request.anetApiRequest.merchantAuthentication.sessionToken = _sessionToken;
    
    [an purchaseWithRequest:request];
    
    //}
    
    
    
}
- (void)clearInputFields
{
    cvvTextfd.text = @"";
    self.zipTextField.text = @"";
    cardnumberTextfd.text = @"";
    monthTextfd.text = @"";
    
    // Clear the buffers too
    self.creditCardBuf = @"";
    self.expirationBuf = @"";
    
    [cardnumberTextfd resignFirstResponder];
    [monthTextfd resignFirstResponder];
    [cvvTextfd resignFirstResponder];
    [self.zipTextField resignFirstResponder];
}
- (NSString*)expirationDateWithoutSeparator
{
    return [monthTextfd.text stringByReplacingOccurrencesOfString:kSlash withString:@""];
}
- (void) formatValue:(UITextField *)textField {
    NSMutableString *value = [NSMutableString string];
    
    if (textField == cardnumberTextfd ) {
        NSInteger length = [self.creditCardBuf length];
        for (int i = 0; i < length; i++) {
            // Reveal only the last character.
            if (length <= kCreditCardObscureLength) {
                
                //if (i == (length - 1)) {
                    [value appendString:[self.creditCardBuf substringWithRange:NSMakeRange(i,1)]];
                //} else {
                   // [value appendString:@"●"];
                //}
            }
            // Reveal the last 4 characters
            else {
                //if (i < kCreditCardObscureLength) {
                   // [value appendString:@"●"];
               // } else {
                    [value appendString:[self.creditCardBuf substringWithRange:NSMakeRange(i,1)]];
                //}
            }
            //After 4 characters add a space
            if ((i +1) % 4 == 0 &&
                ([value length] < kCreditCardLengthPlusSpaces)) {
                [value appendString:kSpace];
            }
        }
        textField.text = value;
    }
    if (textField == monthTextfd) {
        for (int i = 0; i < [self.expirationBuf length]; i++) {
            [value appendString:[self.expirationBuf substringWithRange:NSMakeRange(i,1)]];
            
            // After two characters append a slash.
            if ((i + 1) % 2 == 0 &&
                ([value length] < kExpirationLengthPlusSlash)) {
                [value appendString:kSlash];
            }
        }
        textField.text = value;
    }
}

-(void)AddnewAddressButton:(id)sender{
  enterNewAddress =YES;
    
    [self.defaultTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}
-(void)couponCodeButton:(UIButton *)sender{
    if (sender.tag==12){
    couponCode=YES;
        if ([OrderTypeString isEqualToString:kStringDeliver]) {
    [self.defaultTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:4 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
           }
           else{
               [self.defaultTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
           }
    }
}
-(void)removeCouponCodeButton:(id)sender{
   // if(!isCouponApplied){
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                    message:@"Do you really want to cancel this coupon?"
                                                   delegate:self
                                          cancelButtonTitle:@"Yes"
                                          otherButtonTitles:@"No",nil];
    alert.tag=100;
    [alert show];
   // }
   
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag==100) {
        if (buttonIndex == 0) {
            if(couponCode==YES){
        if ([CouponCodeValidatorString isEqualToString:@"Success"]){
            
            [orderSummaryItems removeObjectAtIndex:1];
            [orderSummaryValues removeObjectAtIndex:1];
            textfieldData=nil;
            
            CouponCodeValidatorString=@"failure";
        }
             
            }
            isCouponApplied=NO;
            couponCode=NO;
              if ([OrderTypeString isEqualToString:kStringDeliver]) {
             [self.defaultTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:4 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                   [self.defaultTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                couponCodeTxtFd=nil;
        }
              else{
                  [self.defaultTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                   [self.defaultTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                  couponCodeTxtFd=nil;

              }
        }
        
    }
    if (alertView.tag==102) {
        if (buttonIndex == 0) {
            // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            if([CLLocationManager locationServicesEnabled]){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your Location"
                                                                message:orderAddress
                                                               delegate:self
                                                      cancelButtonTitle:@"Do you want to keep this address as your profile address?"
                                                      otherButtonTitles:@"Yes",@"No",nil];
                alert.tag=103;
                [alert show];
                
                
            }
            else{
                [self hideSpinner];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                message:@"Location services is disable"
                                                               delegate:self
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
                alert.tag=104;
                [alert show];
                
            }
            
        }
    }
    else if (alertView.tag==103) {
        [self hideSpinner];
        if (buttonIndex == 1) {
            cityTxtFd.text=ordercity;
            stateTxtFd.text=orderstate;
            zipTxtFd.text=orderzipcode;
            addressTxtFd.text=addresstotal;
//            if(addresstotal!=nil){
//            apartmentTxtFd.text=addresstotal;
//            }
//            else{
//                 apartmentTxtFd.text=@"";
//            }
            //[location stopUpdatingLocation];
        }
        
    }
}


-(void)findMeButton:(id)sender{
    
        [self showSpinner];
     [location startUpdatingLocation];
        if([CLLocationManager locationServicesEnabled])
        {
            // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            if([CLLocationManager locationServicesEnabled]){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your Location"
                                                                message:[NSString stringWithFormat:@"%@ \n\n %@",orderAddress,@"Do you want to keep this address as your profile address?"]
                                                               delegate:self
                                                      cancelButtonTitle:@"No"
                                                      otherButtonTitles:@"Yes",nil];
                alert.tag=103;
                [alert show];
                
                
                
            }
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SETTINGS"
                                                            message:@"Enable Location Provider!Go to settings menu?"
                                                           delegate:self
                                                  cancelButtonTitle:@"Settings"
                                                  otherButtonTitles:@"Cancel",nil];
            alert.tag=102;
            [alert show];
            
            [location startUpdatingLocation];
            
        }
        
    }
    

-(void)removeButton:(id)sender{
    enterNewAddress =NO;
    
    // [self.defaultTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
    [self.defaultTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    addressTxtFd        = nil;
    cityTxtFd           = nil;
    stateTxtFd          = nil;
    zipTxtFd            = nil;
    firstNameTxtfd      = nil;
    lastNameTxtfd       = nil;
    phoneNumberTxtfd    = nil;
    phoneNumberTxtfd    = nil;
    apartmentTxtFd      =nil;
    
}



-(void)payAtStoreButton:(UIButton*)sender{
    //[self GotoOrderconfirmation];
    
    if(isrestrict==NO){
    payTypeString=kStringPayAtStore;
        if (![CouponCodeValidatorString isEqualToString:@"Success"] && couponCode==YES){
            [self showAlertViewWithTitle:@"Please click on apply button to avail discount." message:nil];
            return;
            
        }
        

    [self callApiGetRequestProductWiseQuantity];
        
    }
    //[self callApiAddorderWithAuthArray:orderArray withPaymentType:kStringPayAtStore];
}

#pragma-MARK BUTTON ACTION
-(void)getLocationButtonClicked:(UIButton*)sender{
    if(sender.tag==2222){
    __weak ClosestStoreModel* lClosestStoreModelObj = kGetClosestStoreModel;
    __weak StoreDetails *storeDetails = kGetStoreDetailsModel;
    [self ShowLocationWithLat:storeDetails.StoreLatitude longitude:storeDetails.StoreLongitude];
}
}
// became first responder
#pragma mark-
- (void)operationDidFailWithError:(NSError *)error {
    DLog(@"error %@",error);
    [self hideSpinner];
}


///
- (void)operationDidFailWithResponse:(NSHTTPURLResponse *)response {
    DLog(@"response %@",response);
    [self hideSpinner];
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark-FetchUserData Delegate
- (void)GetProfileView{
    __weak UserProfile *userProfileObj=kgetUserProfileModel;
    NSMutableDictionary *userDict=[[NSMutableDictionary alloc]init];
    [userDict setObject:userProfileObj.FirstName forKey:@"firstname"];
    [userDict setObject:kEmptyString forKey:@"lastname"];
    [userDict setObject:userProfileObj.State forKey:@"state"];
    [userDict setObject:userProfileObj.ZipCode forKey:@"zip"];
    [userDict setObject:userProfileObj.ContactNo forKey:@"contactno"];
    [userDict setObject:userProfileObj.EmailID forKey:@"email"];
    [userDict setObject:userProfileObj.Address forKey:@"address"];
    [userDict setObject:userProfileObj.CountryName forKey:@"country"];
    [userDict setObject:userProfileObj.Address2 forKey:@"address2"];
    [[NSUserDefaults standardUserDefaults]setObject:userDict forKey:@"UserData"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

#pragma mark -FetchState Delegate Call back

- (void) createTransaction {
    
    AuthNet *an ;
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"IsLive"] intValue]==0) {
        an =   [AuthNet initWithAPILoginID:[[NSUserDefaults standardUserDefaults]objectForKey:@"AppLoginId"] andTransactionKey:[[NSUserDefaults standardUserDefaults]objectForKey:@"AppTransactionKey"] forEnvironment:ENV_TEST];
        
    }
    else{
        an =  [AuthNet initWithAPILoginID:[[NSUserDefaults standardUserDefaults]objectForKey:@"AppLoginId"] andTransactionKey:[[NSUserDefaults standardUserDefaults]objectForKey:@"AppTransactionKey"] forEnvironment:ENV_LIVE];
        
    }
    
    [an setDelegate:self];
    
    CreditCardType *creditCardType = [CreditCardType creditCardType];
    if(scanFound)
    {
        
        
        
        creditCardType.cardNumber = scanCerditCard;
    }
    else
    {
        //creditCardTextField.text=@"5123456789012346";
        //cardnumberString=[CreditCardType cardNumber];
        // DLog(@"cardnumberString--%@",cardnumberString);
        creditCardType.cardNumber = self.creditCardBuf;
    }
    
    creditCardType.cardCode = cvvTextfd.text;
    creditCardType.expirationDate = monthTextfd.text;
    
    PaymentType *paymentType = [PaymentType paymentType];
    paymentType.creditCard = creditCardType;
    
    
    ExtendedAmountType *extendedAmountTypeTax = [ExtendedAmountType extendedAmountType];
    NSArray *taxArr=[totaltax componentsSeparatedByString:@"$"];
    extendedAmountTypeTax.amount = [taxArr objectAtIndex:1] ;
    extendedAmountTypeTax.name = @"Tax";
    
    ExtendedAmountType *extendedAmountTypeShipping = [ExtendedAmountType extendedAmountType];
    extendedAmountTypeShipping.amount = @"0";
    extendedAmountTypeShipping.name = @"Shipping";
    CustomerAddressType *b=[CustomerAddressType customerAddressType];

    NSMutableDictionary *userDict=[[NSUserDefaults standardUserDefaults] objectForKey:@"UserData"];
       __weak ClosestStoreModel* lClosestStoreModelObj;
    __weak StoreDetails *storeDetails;
    lClosestStoreModelObj = kGetClosestStoreModel;
    ///////shipping address/////////////////////////////////////////////////////////
    __weak User *userObj = kGetUserModel;
    __weak UserProfile *userprofileObj = kgetUserProfileModel;
    

    NameAndAddressType *s=[NameAndAddressType nameAndAddressType];

    if ([OrderTypeString isEqualToString:kStringPickUp]) {
        s.firstName         = userprofileObj.FirstName;
        s.lastName          = userprofileObj.LastName;
        s.address           = storeDetails.StoreUserAddress1;
        s.address2           = storeDetails.StoreUserApartment;
        s.city              = storeDetails.StoreUserCity;
        s.state             = storeDetails.StoreStateName;
        s.country           = storeDetails.StoreUserAddress1;
        s.zip               = zipTextField.text;
    }
    else{
        if(AdressValidatorDict && [AdressValidatorDict objectForKey: @"Address"] && [AdressValidatorDict objectForKey: @"Zip"]) {
            
            s.firstName         = [AdressValidatorDict objectForKey:@"FirstName"];
            s.lastName          = [AdressValidatorDict objectForKey:@"LastName"];
            s.address           = [AdressValidatorDict objectForKey:@"Address"];
            s.address2          = [AdressValidatorDict objectForKey:@"Address2"];
            s.state             = [AdressValidatorDict objectForKey:@"State"];
            s.city              = [AdressValidatorDict objectForKey:@"City"];
            s.country           = [AdressValidatorDict objectForKey:@"CountryId"];
            s.zip               = [AdressValidatorDict objectForKey:@"Zip"];
        }
        else    {
            s.firstName         = userprofileObj.FirstName;
            s.lastName          = userprofileObj.LastName;
            s.address           = [userDict objectForKey:@"address"];
            s.address2         = [userDict objectForKey:@"address2"];
            s.state             = [userDict objectForKey:@"state"];
            s.country           = [userDict objectForKey:@"country"];
            s.zip               = [userDict objectForKey:@"zip"];
        }
        
        
    }
    

    if (lClosestStoreModelObj==nil) {
       
        NSMutableDictionary* StoreDetailDict = [[ NSMutableDictionary alloc] init];
        [StoreDetailDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
        [StoreDetailDict setObject:userObj.userId forKey: kParamUserId];
        
        [self showSpinner]; // To show spinner
        
        
        [[ModelManager modelManager] GetStoreDetailWithParam:StoreDetailDict  successStatus:^(BOOL status) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self hideSpinner];
                __weak ClosestStoreModel* lClosestStoreModelObj;
                __weak StoreDetails *storeDetails;
                lClosestStoreModelObj = kGetClosestStoreModel;
                storeDetails = kGetStoreDetailsModel;
                b.firstName         = userprofileObj.FirstName;
                b.lastName          = userprofileObj.LastName;
                b.phoneNumber       = userprofileObj.ContactNo;
                b.address           = storeDetails.StoreUserAddress1;
                b.address2           = storeDetails.StoreUserApartment;
                b.city              = storeDetails.StoreUserCity;
                b.state             = storeDetails.StoreStateName;
                b.country           = storeDetails.StoreUserAddress1;
                b.zip               = zipTextField.text;
            });
        } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
            [self hideSpinner];
            [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
            DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        }];
        
        
        
    }
    else
    {
        storeDetails = kGetStoreDetailsModel;
        b.firstName         = userprofileObj.FirstName;
        b.lastName          = userprofileObj.LastName;
        b.phoneNumber       = userprofileObj.ContactNo;
        b.address           = storeDetails.StoreUserAddress1;
        b.address2          = storeDetails.StoreUserApartment;

        b.city              = storeDetails.StoreUserCity;
        b.state             = storeDetails.StoreStateName;
        b.country           = storeDetails.StoreUserAddress1;
        b.zip               = zipTextField.text;
        
    }
    
    
    CustomerDataType *data=[CustomerDataType customerDataType];
    data.email=[userDict objectForKey:@"email"];
    
    TransactionRequestType *requestType = [TransactionRequestType transactionRequest];
    NSMutableArray   *lineArr=[[NSMutableArray alloc]init];;
            LineItemType *lineItem;
    
       DLog(@"orderTotal=====%f",orderTotal);
              for (Product *product in getCartArray) {
                
                lineItem = [LineItemType lineItem];
                NSString *product_Name=product.productName;
                if(product_Name.length>31)
                {
                    product_Name=[product_Name substringToIndex:30];
                    lineItem.itemName = product_Name;
                }
                else
                {
                    lineItem.itemName = product.productName;
                }
                
                
                lineItem.itemDescription = product.productDesc;
                lineItem.itemQuantity = [NSString stringWithFormat:@"%d",product.orderQuantity];
                lineItem.itemPrice =  product.productPrice;
                lineItem.itemID =  product.productId;
                if( product.ProductTax>0)
                    lineItem.itemTaxable=YES;
                else
                {
                    lineItem.itemTaxable=NO;
                }
                lineArr= [NSMutableArray arrayWithObjects:lineItem, nil];
              
                
            }
    
    NSLog(@"Order total = %@",orderTotal_oc);
    requestType.amount = orderTotal_oc;
    requestType.payment = paymentType;
    requestType.tax = extendedAmountTypeTax;
    requestType.shipping = extendedAmountTypeShipping;
    requestType.customer=data;
    requestType.billTo=b;
    
    CreateTransactionRequest *request = [CreateTransactionRequest createTransactionRequest];
    request.transactionRequest = requestType;
    request.transactionType = AUTH_ONLY;
    request.anetApiRequest.merchantAuthentication.name = [[NSUserDefaults standardUserDefaults]objectForKey:@"AppLoginId"];
    
    request.anetApiRequest.merchantAuthentication.transactionKey = [[NSUserDefaults standardUserDefaults]objectForKey:@"AppTransactionKey"];
    an.duplicateWindowValue = 28800;//8 hours
    //[an authorizeWithRequest:request];
    [an purchaseWithRequest:request];
    
    

}
- (void) requestFailed:(CreateTransactionResponse *)response { // Handle a failed request // getting callback to this method }
    DLog(@"response.errorType--%u",response.errorType);
    transResponse = response.transactionResponse;
    
    [self hideSpinner];
    if ([transResponse.errors count]) {
        NSString *tempstr=[[transResponse.errors objectAtIndex:0]valueForKey:@"errorText"];
        
        [self showAlertViewWithTitle:kAlertTransactionFailed message:tempstr];
    }
    
}

#pragma mark-textfield delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    

    return YES;

}
- (BOOL) isMaxLength:(UITextField *)textField {
    
    if (textField == cardnumberTextfd && [textField.text length] >= kCreditCardLengthPlusSpaces) {
        return YES;
    }
    else if (textField == monthTextfd && [textField.text length] >= kExpirationLengthPlusSlash) {
        return YES;
    }
    else if (textField == cvvTextfd && [textField.text length] >= kCVV2Length) {
        return YES;
    }
    else if (textField == zipTextField && [textField.text length] >= kZipLength) {
        return YES;
    }
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField==cardnumberTextfd||textField==monthTextfd||textField==cvvTextfd) {
        payNowButton.userInteractionEnabled = YES;
    }
    
    if([textField isEqual:_specialInstruction] && [textField.text isEqualToString:kEmptyInstructions]) {
        textField.text = kEmptyString;
    }
    
    
}
- (BOOL)textFieldShouldClear:(UITextField *)textField {
    if (textField == cardnumberTextfd) {
        self.creditCardBuf = [NSString string];
    }
    
    if (textField == monthTextfd) {
        self.expirationBuf = [NSString string];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    //restrict entry of empty space in beginning
    if (range.location==0 && [string isEqualToString: @" "] ) {
        return NO;
    }

    if (textField == cardnumberTextfd) {
        scanFound = NO;
        if ([string length] > 0) { //NOT A BACK SPACE Add it
            if ([self isMaxLength:textField])
                return NO;
            
            self.creditCardBuf  = [NSString stringWithFormat:@"%@%@", self.creditCardBuf, string];
        } else {
            
            //Back Space do manual backspace
            if ([self.creditCardBuf length] > 1) {
                self.creditCardBuf = [self.creditCardBuf substringWithRange:NSMakeRange(0, [self.creditCardBuf length] - 1)];
            } else {
                self.creditCardBuf = @"";
            }
        }
        [self formatValue:textField];
        [self validateCreditCardValue];
        return NO;
    }
    if (textField==firstNameTxtfd||textField==lastNameTxtfd) {
        NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:ALPHA];
        if (([string rangeOfCharacterFromSet:set].location == NSNotFound) && (![string isEqualToString:kEmptyString])) {
            return NO;
        }
        else if([string isEqualToString:kEmptyString])
            return YES;
    }
    if (textField==phoneNumberTxtfd) {

        int length = (int)[self getLength:textField.text];
        //NSLog(@"Length  =  %d ",length);
        
        if(length == 10)
        {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 3)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"(%@) ",num];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
        }
        else if(length == 6)
        {
            NSString *num = [self formatNumber:textField.text];
            //NSLog(@"%@",[num  substringToIndex:3]);
            //NSLog(@"%@",[num substringFromIndex:3]);
            textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
        }
        return YES;
    }
    else if (textField == monthTextfd) {
        if ([string length] > 0) { //NOT A BACK SPACE Add it
            
            if ([self isMaxLength:textField])
                return NO;
            
            NSMutableString *finalMonthValue = [NSMutableString string];
            
            if([textField.text length] == 0)    {
                if([string integerValue] == 0 || [string integerValue] == 1)  {
                    [finalMonthValue appendString:string];
                }
                else if ([string integerValue] >= 2 && [string integerValue] <= 9)  {
                    [finalMonthValue appendFormat:@"0%@", string];
                }
                else    {
                    [finalMonthValue appendFormat:@""];
                }
            }
            else if([textField.text length] == 1)   {
                if([textField.text integerValue] == 0)  {
                    if([string integerValue] >= 1 && [string integerValue] <= 9)    {
                        [finalMonthValue appendFormat:@"%@", string];
                    }
                    else    {
                        [finalMonthValue appendFormat:@""];
                    }
                }
                else if ([textField.text integerValue] == 1)    {
                    if([string integerValue] >= 0 && [string integerValue] <= 2)    {
                        [finalMonthValue appendFormat:@"%@", string];
                    }
                    else    {
                        [finalMonthValue appendFormat:@"01"];
                        self.expirationBuf = @"";
                    }
                }
                else    {
                    [finalMonthValue appendFormat:@""];
                }
            }
            else    {
                [finalMonthValue appendString:string];
            }
            self.expirationBuf  = [NSString stringWithFormat:@"%@%@", self.expirationBuf, finalMonthValue];
        } else {
            //Back Space do manual backspace
            if ([self.expirationBuf length] > 1) {
                self.expirationBuf = [self.expirationBuf substringWithRange:NSMakeRange(0, [self.expirationBuf length] - 1)];
                [self formatValue:textField];
            } else {
                self.expirationBuf = @"";
            }
        }
        
        [self formatValue:textField];
        return NO;
    }
    else if (textField == cvvTextfd) {
        if ([string length] > 0) {
            
            if ([self isMaxLength:textField])
                return NO;
            
            cvvTextfd.text = [NSString stringWithFormat:@"%@%@", cvvTextfd.text, string];
        }else {
            
            //Back Space do manual backspace
            if ([cvvTextfd.text length] > 1) {
                cvvTextfd.text = [cvvTextfd.text substringWithRange:NSMakeRange(0, [cvvTextfd.text length] - 1)];
            } else {
                cvvTextfd.text = @"";
            }
        }
        return NO;
    }
    else if (textField == zipTextField) {
        if ([string length] > 0) {
            
            if ([self isMaxLength:textField])
                return NO;
            
            self.zipTextField.text = [NSString stringWithFormat:@"%@%@", self.zipTextField.text, string];
        }else {
            
            //Back Space do manual backspace
            if ([self.zipTextField.text length] > 1) {
                self.zipTextField.text = [self.zipTextField.text substringWithRange:NSMakeRange(0, [self.zipTextField.text length] - 1)];
            } else {
                self.zipTextField.text = @"";
            }
        }
        return NO;
        
    }
    else if(zipTxtFd == textField)  {
        
        NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:NUMERIC];
        if (([string rangeOfCharacterFromSet:set].location == NSNotFound) && (![string isEqualToString:kEmptyString])) {
            return NO;
        }
        else if([string isEqualToString:kEmptyString])
            return YES;
        else    {
            if ([string length] > 0) {
                if (textField == zipTxtFd && [textField.text length] >= kZipLength)
                    return NO;
                
                zipTxtFd.text = [NSString stringWithFormat:@"%@%@", zipTxtFd.text, string];
            }else {
                //23Back Space do manual backspace
                if ([zipTxtFd.text length] > 1) {
                    zipTxtFd.text = [zipTxtFd.text substringWithRange:NSMakeRange(0, [zipTxtFd.text length] - 1)];
                } else {
                    zipTxtFd.text = @"";
                }
            }
            return NO;
        }
    }
    else if (apartmentTxtFd==textField){
        if(range.length + range.location > textField.text.length)
        {
             //[self showAlertViewWithTitle:@"Alert" message:@"Please enter 15 character address!"];
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
       
        return newLength <= 15;
    }
    else if (addressTxtFd==textField){
        if(range.length + range.location > textField.text.length)
        {
            //[self showAlertViewWithTitle:@"Alert" message:@"Please enter 100 character address!"];
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        return newLength <= 100;
    }

    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField  {
    [self shouldEnablePayNowBtn];
    
    if([textField isEqual:_specialInstruction] && [textField.text isEqualToString:kEmptyString]) {
        textField.text = kEmptyInstructions;
    }
    specialInstructionString=_specialInstruction.text;
    User *userObj=kGetUserModel;
    if (couponCode && [OrderTypeString isEqualToString:kStringDeliver]) {
        
    }
    
    if (enterNewAddress && [OrderTypeString isEqualToString:kStringDeliver]) {
    /*this to done so as to save all the filled detail in NSdictionary ,so when ever user click done item does not get vanished*/
    AdressValidatorDict=nil;
    AdressValidatorDict = [NSMutableDictionary dictionary];
    [AdressValidatorDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
        
    [AdressValidatorDict setObject:userObj.userId forKey: kParamUserId];
    [AdressValidatorDict setObject:firstNameTxtfd.text forKey: kParamFirstName];
   // [lastNameTxtfd becomeFirstResponder];
    [AdressValidatorDict setObject:lastNameTxtfd.text forKey: kParamLastName];
     //   [addressTxtFd becomeFirstResponder];
    [AdressValidatorDict setObject:addressTxtFd.text forKey: kParamAddress];
     //   [apartmentTxtFd becomeFirstResponder];
    [AdressValidatorDict setObject:apartmentTxtFd.text forKey: kParamApartment];
     //   [cityTxtFd becomeFirstResponder];
    [AdressValidatorDict setObject:cityTxtFd.text forKey: kParamCity];
      //  [stateTxtFd becomeFirstResponder];
    [AdressValidatorDict setObject:stateTxtFd.text forKey: kParamState];
      //  [zipTextField becomeFirstResponder];
    [AdressValidatorDict setObject:kEmptyString forKey: kParamCountryId];
    [AdressValidatorDict setObject:zipTxtFd.text forKey: kParamZip];
      //  [phoneNumberTxtfd becomeFirstResponder];
    [AdressValidatorDict setObject:phoneNumberTxtfd.text forKey: kParamPhone];
        
    }
    if (cardnumberTextfd!=nil&&monthTextfd!=nil&&cvvTextfd!=nil&&zipTextField!=nil) {
        [cardDeatailArray replaceObjectAtIndex:0 withObject:cardnumberTextfd.text];
        [cardDeatailArray replaceObjectAtIndex:1 withObject:monthTextfd.text];
        [cardDeatailArray replaceObjectAtIndex:2 withObject:cvvTextfd.text];
        [cardDeatailArray replaceObjectAtIndex:3 withObject:zipTextField.text];
    }
   

}
- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        NSLog(@"%@", mobileNumber);
        
    }
    
    return mobileNumber;
}

- (int)getLength:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
}
#pragma mark-
-(void)ScrollTableViewDown{
[self.defaultTableView setContentOffset:CGPointZero animated:YES];
}
-(void)ScrollTableViewUP{
    [self.defaultTableView setContentOffset:CGPointMake(0, SCREEN_HEIGHT/2) animated:YES];
}
-(void)DoneButtonDatePicker:(UIButton*)button{
   // [self ScrollTableViewDown];
    [self.dropDownView removeFromSuperview];

}

-(void)DoneButtonPicker:(UIButton*)button{
    //[self ScrollTableViewDown];
    [self.dropDownView removeFromSuperview];
}

-(NSString*)getCurrentDate{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM-dd-yyyy"];
    
    
    NSString*currentDate=[NSString stringWithFormat:@"%@",[df stringFromDate:[NSDate date]]];
    NSDateFormatter *format= [[NSDateFormatter alloc] init];
    [format setDateFormat:@"EEEE"];
    NSString *day=[NSString stringWithFormat:@"%@",[format stringFromDate:[NSDate date]]];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:[NSDate date]];
    NSInteger hour = [components hour];
    if([currentDate isEqualToString:[df stringFromDate:[NSDate date]]] && hour>=17){
        if([day isEqualToString:@"Friday"])
        {
            NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
                    [offsetComponents setDay:3];
            NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDate *nextDate = [gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0];
            currentDate=[NSString stringWithFormat:@"%@",[df stringFromDate:nextDate]];

        }
        
    }
       return currentDate;
}

-(NSMutableArray*)getTwoHoursLessInPickerArrayWithPickerArray:(NSMutableArray*)lpickerArray{
    NSMutableArray *newPickerArray=[NSMutableArray new];
    newPickerArray =lpickerArray;
    [newPickerArray removeLastObject];
    [newPickerArray removeLastObject];
    if ([newPickerArray count]==0) {
        [self showAlertViewWithTitle:kAlertStoreISClosed message:nil];
        return newPickerArray;
    }
    else{
    return newPickerArray;
    }
    

}
/*this is the dynamic array that contain periodic time interval of 1 hrs  between the store open time and close time */
-(BOOL)compareCurrentDateWith:(NSString*)closeTime andOpenTime:(NSString*)openTime{
    BOOL isclose;
    DLog(@"openTime=%@ closeTime=%@",openTime,closeTime);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = deliveryTimeFormat;
    NSDate *date = [dateFormatter dateFromString:openTime];
    dateFormatter.dateFormat = @"HH:mm";
    NSString *openTimeString = [dateFormatter stringFromDate:date];
    NSArray *temparr=[openTimeString componentsSeparatedByString:@":"];
    
    NSDateFormatter *closeTimeDF = [[NSDateFormatter alloc] init];
    closeTimeDF.dateFormat = deliveryTimeFormat;
    NSDate *date2 = [closeTimeDF dateFromString:closeTime];
    
    closeTimeDF.dateFormat = @"HH:mm";
    NSString *closetimeString = [closeTimeDF stringFromDate:date2];
    NSArray *temparr2=[closetimeString componentsSeparatedByString:@":"];
    
    
    NSDateComponents *openingTime = [[NSDateComponents alloc] init];
    openingTime.hour = [[temparr objectAtIndex:0] intValue];
    openingTime.minute =[[temparr objectAtIndex:1] intValue] ;
    
    NSDateComponents *closingTime = [[NSDateComponents alloc] init];
    closingTime.hour = [[temparr2 objectAtIndex:0] intValue];
    closingTime.minute = [[temparr2 objectAtIndex:1] intValue];
    
    NSDate *now = [NSDate date];
    
    NSDateComponents *currentTime = [[NSCalendar currentCalendar] components:NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond
                                                                    fromDate:now];
    
    NSMutableArray *times = [@[openingTime, closingTime, currentTime] mutableCopy];
    [times sortUsingComparator:^NSComparisonResult(NSDateComponents *t1, NSDateComponents *t2) {
        if (t1.hour > t2.hour) {
            return NSOrderedDescending;
        }
        
        if (t1.hour < t2.hour) {
            return NSOrderedAscending;
        }
        // hour is the same
        if (t1.minute > t2.minute) {
            return NSOrderedDescending;
        }
        
        if (t1.minute < t2.minute) {
            return NSOrderedAscending;
        }
        // hour and minute are the same
        if (t1.second > t2.second) {
            return NSOrderedDescending;
        }
        
        if (t1.second < t2.second) {
            return NSOrderedAscending;
        }
        return NSOrderedSame;
        
    }];
    
    if ([times indexOfObject:currentTime] == 1) {
        NSLog(@"We are Open!");
        
        isclose=NO;
    } else {
        NSLog(@"Sorry, we are closed!");
        isclose=YES;
    }
   
    return isclose;
    
}


-(NSString*)getstringFromDate:(NSDate*)ldate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = deliveryTimeFormat;
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    return [formatter stringFromDate:ldate];
    
}
-(NSDate*)getDateFromString:(NSString*)dateString{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = deliveryTimeFormat;
   // [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    return [formatter dateFromString:dateString];
}
-(NSDate*)getDateFromDateString:(NSString*)dateString{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = deliveryDateFormat;
   // [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    return [formatter dateFromString:dateString];
}
- (NSDate*) roundOffTime:(NSDate*)inDate{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components: NSCalendarUnitEra|NSCalendarUnitYear| NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour fromDate: inDate];
    [comps setMinute:0]; //NSDateComponents handles rolling over between days, months, years, etc
  //  NSDate *newdate=[calendar dateFromComponents:comps];
    //DLog(@"%@",[self getstringFromDate:newdate]);
    return [calendar dateFromComponents:comps];
}


-(NSMutableArray*)getPickerArrayForStoreOpenTime:(NSString*)storeOpenTime andStoreCloseTime:(NSString*)storeCloseTime{
    
   
    NSDate *storeOpenTimedate = [self getDateFromString:storeOpenTime];

    NSDate *storeCloseTimedate = [self getDateFromString:storeCloseTime];
    NSDate *serverCurrentTime = [self getDateFromString:fromTimeString];
    
    NSComparisonResult result;
    NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
    dateFormatter3.dateFormat = deliveryTimeFormat;
   // [dateFormatter3 setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter3 setDateFormat:@"hh:mm a"];
    NSString *currentSTime = [dateFormatter3 stringFromDate:serverCurrentTime];
    NSLog(@"The getCurrentTime Time is %@",[dateFormatter3 stringFromDate:serverCurrentTime]);
    NSLog(@"The storeCloseTimedate Time is %@",[dateFormatter3 stringFromDate:storeCloseTimedate]);
    NSLog(@"The storeOpenTimedate Time is %@",[dateFormatter3 stringFromDate:storeOpenTimedate]);
    
   

    
    NSLog(@"del String = %@ ..%@",deliveryDateString,currentSTime);
    
    
  deliveryDateString = [self addZeroBeforeMonth:[self getDateFromDateString:deliveryDateString]];
    
    NSLog(@"Current date = %@",[self getCurrentDate]);
    NSLog(@"Delivery date string = %@",deliveryDateString);
    if ([deliveryDateString isEqualToString:[self getCurrentDate]]) {
      
        NSTimeInterval interval = [serverCurrentTime timeIntervalSinceDate: storeOpenTimedate];//[date1 timeIntervalSince1970] - [date2 timeIntervalSince1970];
        if(interval < 0)
        {
            storeOpenTime = storeOpenTime;
            oneHourExtraServerTime = [self getDateFromString:storeOpenTime];
        }
        else
        {
          currentSTime = [NSString stringWithFormat:@"%@",[self addTimeIntervalInGivenTime:currentSTime withtimeinterval:1 ]];
            NSLog(@"CurrentStime = %@",currentSTime);
            oneHourExtraServerTime = [self getDateFromString:currentSTime];
            storeOpenTime = currentSTime;
        }
        
                if ([self compareCurrentDateWith:storeCloseTime andOpenTime:storeOpenTime]==NO) {
                    
                    
                            //storeCloseTimedate is later than getCurrentTime"
                           storeOpenTimedate=[self roundOffTime:oneHourExtraServerTime];
                    
                    
                           //storeOpenTimedate=[self getCurrentTime];
                           storeOpenTime=[self getstringFromDate:storeOpenTimedate];
                           


            }
//        else if ([dateFormatter3 stringFromDate:[self getCurrentTime]] > [dateFormatter3 stringFromDate:storeCloseTimedate])
//        {
//                    [self showAlertViewWithTitle:[NSString stringWithFormat:@"%@ on %@",kAlertStoreISClosed,[self GetWeekdayNameFromGivenDate:self.datePicker.date]] message:nil];
//            isclosed=YES;
//    }
            else{
                result=NSOrderedDescending;
                storeOpenTimedate=[self roundOffTime:oneHourExtraServerTime];
                
                //storeOpenTimedate=[self getCurrentTime];
                storeOpenTime=[self getstringFromDate:storeOpenTimedate];
            }
        


    }
    else
    {
        oneHourExtraServerTime = [self getDateFromString:storeOpenTime];
    }
  //else{
    storeOpenTimedate = [self getDateFromString:storeOpenTime];
    
    storeCloseTimedate = [self getDateFromString:storeCloseTime];
    
         result = [storeOpenTimedate compare:storeCloseTimedate];
        pickerArray = [[NSMutableArray alloc] init];
    
        [pickerArray addObject:storeOpenTime];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
       // [df setTimeZone:[NSTimeZone systemTimeZone]];
        [df setDateFormat:deliveryTimeFormat];
        if(result == NSOrderedDescending)
        {
            NSLog(@"date1 is later than date2");
        }
        else if(result == NSOrderedAscending)
        {
            NSLog(@"date2 is later than date1");
        }
        else
        {
            NSLog(@"date1 is equal to date2");
        }
        
        
        while (result == NSOrderedAscending) {
            
            [pickerArray addObject:[self addTimeIntervalInGivenTime:storeOpenTime withtimeinterval:1]];
            //[pickerArray removeObjectAtIndex:[pickerArray count]-1];
            storeOpenTime=[self addTimeIntervalInGivenTime:storeOpenTime withtimeinterval:1];
            if (stoploop==YES) {
                break;
            }
            else{
                
                storeOpenTimedate = [self getDateFromString:storeOpenTime];
                
                storeCloseTimedate = [self getDateFromString:storeCloseTime];
                result = [storeOpenTimedate compare:storeCloseTimedate];
            }
            
            
        }
        DLog(@"pickerArray===%@",pickerArray);
        pickerArray=[self getTwoHoursLessInPickerArrayWithPickerArray:pickerArray];
//  }
    
       return pickerArray;

}
-(void)changeDateInLabel:(id)sender withTextfield:(UITextField*)textfield
{
    textfield=[UITextField new];
    textfield=self.dateOfBirthTextfield;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSLog(@"date is %@", self.datePicker.date);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en"];
    
        [df setDateFormat:@"MM-dd-yyyy"];
        
   
    
    /* sender.text = [NSString stringWithFormat:@"%@",
     [df stringFromDate:self.datePicker.date]];
     */
    
   // NSDate *date = self.datePicker.date;
    
    if ([self isClosedStore:self.datePicker.date]==YES) {
        [self showAlertViewWithTitle:[NSString stringWithFormat:@"%@ on %@",kAlertStoreISClosed,[self GetWeekdayNameFromGivenDate:self.datePicker.date]] message:nil];
        self.datePicker.date=[self getNextDayDate:self.datePicker.date];
       
    }
   
    deliveryDateString =  [NSString stringWithFormat:@"%@",
                                [df stringFromDate:self.datePicker.date]];
    [DeliveryDateButton setTitle:deliveryDateString forState:UIControlStateNormal];
    if ([self.datePicker.date isEqual:[NSNull null]]) {
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"SelectedDate"];
    }
    else{
        [[NSUserDefaults standardUserDefaults]setObject:self.datePicker.date forKey:@"SelectedDate"];
    }
    NSDate*selectedDate;
    selectedDate = self.datePicker.date;
    [deliveryDateAndTimeArray replaceObjectAtIndex:0 withObject:deliveryDateString];
    __weak ClosestStoreModel *     lClosestStoreModelObj = kGetClosestStoreModel;
    for (StoreOpenCloseTime * storeOpenTime in lClosestStoreModelObj.GetStoreOpenclose) {
        if ([storeOpenTime.DayID isEqualToString:[self GetWeekdayNameFromGivenDate:self.datePicker.date]]) {
            NSLog(@"StoreOpenTime=%@",storeOpenTime.StoreOpenTime);
            NSLog(@"StoreCloseTime=%@",storeOpenTime.StoreCloseTime);
            [self getPickerArrayForStoreOpenTime:storeOpenTime.StoreOpenTime andStoreCloseTime:storeOpenTime.StoreCloseTime];
        }
    }
}

-(void)addDefaultPicker{
    
        __weak ClosestStoreModel *     lClosestStoreModelObj = kGetClosestStoreModel;
    for (StoreOpenCloseTime * storeOpenTime in lClosestStoreModelObj.GetStoreOpenclose) {
        DLog(@"%@-%@",[storeOpenTime.DayID stringByReplacingOccurrencesOfString:@"." withString:@""],[self GetWeekdayNameFromGivenDate:self.datePicker.date]);
        NSLog(@"%@ -%@",[NSDate date],self.datePicker.date);
        if ([self.datePicker.date compare:[NSDate date]]) {
            if ([[storeOpenTime.DayID stringByReplacingOccurrencesOfString:@"." withString:@""] isEqualToString:[self GetWeekdayNameFromGivenDate:self.datePicker.date]])
            {
                DLog(@"StoreOpenTime=%@",storeOpenTime.StoreOpenTime);
                DLog(@"StoreCloseTime=%@",storeOpenTime.StoreCloseTime);
                
                
                [self getPickerArrayForStoreOpenTime:storeOpenTime.StoreOpenTime andStoreCloseTime:storeOpenTime.StoreCloseTime];
            }
        }
        else{
        if ([[storeOpenTime.DayID stringByReplacingOccurrencesOfString:@"." withString:@""] isEqualToString:[self GetWeekdayNameFromGivenDate:self.datePicker.date]]) {
            DLog(@"StoreOpenTime=%@",storeOpenTime.StoreOpenTime);
            DLog(@"StoreCloseTime=%@",storeOpenTime.StoreCloseTime);
            [self getPickerArrayForStoreOpenTime:storeOpenTime.StoreOpenTime andStoreCloseTime:storeOpenTime.StoreCloseTime];
        }
        }
        
    }
    DLog(@"pickerArray==%@",pickerArray);
    if ([pickerArray count]) {
        [self ScrollTableViewUP];
        self.dropDownView = [[UIView alloc]initWithFrame:CGRectMake(0,SCREEN_HEIGHT/2 , SCREEN_WIDTH, SCREEN_HEIGHT/2)];
        self.dropDownView.backgroundColor = [UIColor whiteColor];
        if([self.dropDownView isDescendantOfView:[self view]])
        {
            [self.dropDownView removeFromSuperview];
        }
        [self.view addSubview:self.dropDownView];
        UIPickerView *pickerV = [[UIPickerView alloc] initWithFrame:CGRectMake(0,kDoneButtonHeight-2, SCREEN_WIDTH, (SCREEN_HEIGHT/2)-kDoneButtonHeight)];
        [pickerV setDelegate:self];
        [pickerV setDataSource:self];
        pickerV.backgroundColor = [UIColor clearColor];

        deliveryTimeFromString = [pickerArray objectAtIndex:0];
        [DeliveryTimeFromButton setTitle:deliveryTimeFromString forState:UIControlStateNormal];
        deliveryTimeToString   = [NSString stringWithFormat:@"%@",[self addTimeIntervalInGivenTime:[pickerArray objectAtIndex:0] withtimeinterval:2 ]];
        [DeliveryTimeToButton setTitle:deliveryTimeToString forState:UIControlStateNormal];
        [self.dropDownView addSubview:pickerV];
        UISegmentedControl *DoneButtonDatePicker = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
        DoneButtonDatePicker.momentary = YES;
        DoneButtonDatePicker.frame = CGRectMake(SCREEN_WIDTH-kDoneButtonWidth-5,2,kDoneButtonWidth, kDoneButtonHeight);
        DoneButtonDatePicker.tintColor = [UIColor blackColor];
        [DoneButtonDatePicker addTarget:self action:@selector(DoneButtonPicker:) forControlEvents:UIControlEventValueChanged];
        [self.dropDownView addSubview:DoneButtonDatePicker];
        
        

    }
  }

-(NSString *)addZeroBeforeMonth:(NSDate *)givenDate
{
    NSString *preMonth;
    NSString *preDay;
    NSString *returnString;
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *todayComponents = [gregorian components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:givenDate];
    NSInteger theDay = [todayComponents day];
    NSInteger theMonth = [todayComponents month];
    NSInteger theYear = [todayComponents year];
   if(theMonth <= 9)
   {
       if(theDay <= 9)
       {
       preMonth = [NSString stringWithFormat:@"0%ld",(long)theMonth];
       preDay = [NSString stringWithFormat:@"0%ld",(long)theDay];
       returnString = [NSString stringWithFormat:@"%@-%@-%ld",preMonth,preDay,theYear];
       }
       else
       {
           preMonth = [NSString stringWithFormat:@"0%ld",(long)theMonth];
           returnString = [NSString stringWithFormat:@"%@-%ld-%ld",preMonth,theDay,theYear];
       }
   }
    else
    {
        if(theDay <= 9)
        {
            preDay = [NSString stringWithFormat:@"0%ld",(long)theDay];
            returnString = [NSString stringWithFormat:@"%@-%@-%ld",preMonth,preDay,theYear];
        }
        else
        returnString = [NSString stringWithFormat:@"%ld-%ld-%ld",theMonth,theDay,theYear];
    }
     return returnString;
}
-(NSDate*)getNextDayDate:(NSDate*)currentDate{
    // start by retrieving day, weekday, month and year components for yourDate
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *todayComponents = [gregorian components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:currentDate];
    NSInteger theDay = [todayComponents day];
    NSInteger theMonth = [todayComponents month];
    NSInteger theYear = [todayComponents year];
    
    // now build a NSDate object for yourDate using these components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:theDay];
    [components setMonth:theMonth];
    [components setYear:theYear];
    NSDate *thisDate = [gregorian dateFromComponents:components];
    
    // now build a NSDate object for the next day
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    
    [offsetComponents setDay:1];
    
    NSDate *nextDate = [gregorian dateByAddingComponents:offsetComponents toDate:thisDate options:0];
   /* start date next when saturday sunday formet */
//    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"EEEE"];
//    NSString * dayName = [dateFormatter stringFromDate:nextDate];
//    
//    if([dayName isEqualToString:@"Sunday"] || [dayName isEqualToString:@"Saturday"]){
//        NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
//        [offsetComponents setDay:3];
//        
//        NSDate *nextDate = [gregorian dateByAddingComponents:offsetComponents toDate:thisDate options:0];
//        NSLog(@"%@",nextDate);
//    }
    return nextDate;
}

-(void)adddefaultDatePicker{
    [self ScrollTableViewUP];
    self.dropDownView = [[UIView alloc]initWithFrame:CGRectMake(0,SCREEN_HEIGHT/2 , SCREEN_WIDTH, SCREEN_HEIGHT/2)];
    self.dropDownView.backgroundColor = [UIColor whiteColor];
    
    if([self.dropDownView isDescendantOfView:[self view]])
    {
        [self.dropDownView removeFromSuperview];
    }
    [self.view addSubview:self.dropDownView];
    
    self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,kDoneButtonHeight-2, SCREEN_WIDTH, (SCREEN_HEIGHT/2)-kDoneButtonHeight)];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
   
    if ([self isClosedStore:[NSDate date]]==NO) {
    self.datePicker.minimumDate = [NSDate date];
        
        
        // Convert string to date object
//        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//        [dateFormat setDateFormat:@"dd-MM-yyyy"];
//        NSDate *selecteddate = [dateFormat dateFromString:deliveryDateString];
//        self.datePicker.minimumDate=selecteddate;
    }
    else{
        self.datePicker.minimumDate = [self getNextDayDate:[NSDate date]];
    }
    
    /*********this id done so that User should be able to place a order for more than 1 month from the current date******/
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setMonth:1];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    [self.datePicker setMaximumDate:maxDate];

    
    self.datePicker.backgroundColor = [UIColor whiteColor];

    deliveryDateString = [self getCurrentDate];
    [DeliveryDateButton setTitle:deliveryDateString forState:UIControlStateNormal];
    [self.datePicker addTarget:self action:@selector(changeDateInLabel:withTextfield:) forControlEvents:UIControlEventValueChanged];
    [self.dropDownView addSubview:self.datePicker];
       UISegmentedControl *DoneButtonDatePicker = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    DoneButtonDatePicker.momentary = YES;
    DoneButtonDatePicker.frame = CGRectMake(SCREEN_WIDTH-kDoneButtonWidth-5,2,kDoneButtonWidth, kDoneButtonHeight);
    DoneButtonDatePicker.tintColor = [UIColor blackColor];
    [DoneButtonDatePicker addTarget:self action:@selector(DoneButtonDatePicker:) forControlEvents:UIControlEventValueChanged];
    [self.dropDownView addSubview:DoneButtonDatePicker];
    
    
}


#pragma mark- CAMERA ACTION
//DeliveryTimeButton
-(void)cameraButtonAction:(id)sender{
    scanFound=YES;
    @try {
        CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
        scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        scanViewController.hideCardIOLogo=YES;
        scanViewController.maskManualEntryDigits=YES;
        scanViewController.collectExpiry=YES;
        scanViewController.collectCVV=YES;
        scanViewController.collectPostalCode=YES;
        [self presentViewController:scanViewController animated:YES completion:nil];
    }
    @catch (NSException *exception) {
        DLog(@"exception----%@",exception.userInfo);
    }
    @finally {
        
    }
}

#pragma mark - CardIOPaymentViewControllerDelegate
- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    NSLog(@"Scan succeeded with info: %@", info);
    // Do whatever needs to be done to deliver the purchased items.
   
    NSLog(@"Received card info. Number: %@, expiry: %02lu/%lu, cvv: %@.", info.redactedCardNumber, (unsigned long)info.expiryMonth, (unsigned long)info.expiryYear, info.cvv);
   

    NSString *postalRegex = @"^[0-9]*$";
    NSPredicate *postalTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",postalRegex];
    if ([postalTest evaluateWithObject:info.postalCode] && [info.postalCode length]==5) {
        scanCerditCard=info.cardNumber;
        NSString *str=info.cardNumber;
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    NSNumber *aNsNumber= [formatter numberFromString: str];
    [formatter setUsesGroupingSeparator:YES];
    [formatter setGroupingSize:4];
    [formatter setGroupingSeparator:@"\u00a0"];
    NSString *string = [formatter stringFromNumber:aNsNumber];
    cardnumberTextfd.text=string;
    NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    self.creditCardBuf  = [[string componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""];;
    [self validateCreditCardValue];
    
    [self shouldEnablePayNowBtn]; // To disable Pay now button
    
    cardnumberTextfd.textColor = [UIColor colorWithRed:FLOAT_COLOR_VALUE(98) green:FLOAT_COLOR_VALUE(169) blue:FLOAT_COLOR_VALUE(40) alpha:1];
    
    NSString *yearString = [NSString stringWithFormat:@"%lu",(unsigned long)info.expiryYear];
    if([yearString length]==2)
    {}
    else
    {
        yearString = [yearString substringFromIndex:2];
    }
    monthTextfd.text=[NSString stringWithFormat:@"%02lu/%@",(unsigned long)info.expiryMonth, yearString];
    cvvTextfd.text=info.cvv;
    zipTextField.text=info.postalCode;
    [self formatValue:cardnumberTextfd];
    [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self showAlertViewWithTitle:kNumbersOnly message:@"Postal code must be numbers only and should be five digits"];
    }
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    scanFound=NO;
    [self shouldEnablePayNowBtn]; // To disable Pay now button
    NSLog(@"User cancelled scan");
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark-Place Order on server

-(void)placeOrder{
    DLog(@"");
    AuthArr=[[NSMutableArray alloc]init];
    NSMutableDictionary *authDict=[[NSMutableDictionary alloc]init];
    DLog(@"transResponse---------%@",transResponse.responseCode);
    if (!kLiveApp) {
        
        [authDict setObject:expirationDateString forKey:@"CardExpiry"];
        [authDict setObject:@"" forKey:@"CardMemberName"];
        
        
        switch ([transResponse.responseCode intValue]) {
            case 1:
                [authDict setObject:[NSString stringWithFormat:@"APPROVED_%@",transResponse.responseCode] forKey:@"responsecode"];
                break;
            case 2:
                [authDict setObject:[NSString stringWithFormat:@"DECLINED_%@",transResponse.responseCode] forKey:@"responsecode"];
                break;
            case 3:
                [authDict setObject:[NSString stringWithFormat:@"ERROR_%@",transResponse.responseCode] forKey:@"responsecode"];
                break;
            case 4:
                [authDict setObject:[NSString stringWithFormat:@"FRAUD_REVIEW_%@",transResponse.responseCode] forKey:@"responsecode"];
                
                break;
                
            default:
                break;
        }
        
    }
    else{
        [authDict setObject:transResponse.responseCode forKey:@"responsecode"];
        
    }
    [authDict setObject:@"Success" forKey:@"Transaction"];
    [authDict setObject:transResponse.authCode forKey:@"authcode"];
    [authDict setObject:transResponse.avsResultCode forKey:@"avsResultCode"];
    [authDict setObject:transResponse.cvvResultCode forKey:@"cvvResultCode"];
    [authDict setObject:transResponse.cavvResultCode forKey:@"CavvResultCode"];
    [authDict setObject:transResponse.transId forKey:@"transactionID"];
    [authDict setObject:transResponse.refTransID forKey:@"referenceTranscationID"];
    [authDict setObject:transResponse.transHash forKey:@"transHash"];
    [authDict setObject:transResponse.testRequest forKey:@"testrequest"];
    [authDict setObject:transResponse.accountNumber forKey:@"accountNumber"];
    [authDict setObject:transResponse.accountType forKey:@"accounttype"];
    [authDict setObject:messageStr forKey:@"message"];
    [authDict setObject:@"1" forKey:@"code"];
    [authDict setObject:messageStr forKey:@"description"];
    
    [AuthArr addObject:authDict];
    NSMutableArray *productArr=[[NSMutableArray alloc]init];
    for (int i=0; i<orderArray.count; i++) {
        NSMutableDictionary *orderDict=[[NSMutableDictionary alloc]init];
        [orderDict setValue:[[orderArray objectAtIndex:i]objectForKey:@"ProductId"] forKey:@"ProductId"];
        [orderDict setValue:[[orderArray objectAtIndex:i]objectForKey:@"Quantity"] forKey:@"Quantity"];
        [orderDict setValue:[[orderArray objectAtIndex:i]objectForKey:@"IsOnSaleItems"] forKey:@"IsOnSaleItems"];
        [productArr addObject:orderDict];
    }
    if([productArr count]>0)    {
      //[self callApiGetRequestProductWiseQuantity];
        [self callApiAddorderWithAuthArray:AuthArr withPaymentType:kStringPayNow];
    }
}


#pragma mark-Payment Delegate
- (void) paymentSucceeded:(CreateTransactionResponse *) response {
    DLog(@"");
    [self hideSpinner];
    // Handle payment success
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"text"];
    scanFound=NO;
    NSLog(@"Payment Success **********************");
    
    NSString *title = @"Successful Transaction";
    NSString *alertMsg = nil;
    UIAlertView *PaumentSuccess = nil;
    
    transResponse = response.transactionResponse;
    TransactionIDString=transResponse.transId;
    DLog(@"TransactionIDString--------%@",TransactionIDString);
    alertMsg = [response responseReasonText];
   // NSLog(@"%@",response.responseReasonText);
    messageStr=response.responseReasonText;
    
    if ([transResponse.responseCode isEqualToString:@"4"])
    {
        PaumentSuccess = [[UIAlertView alloc] initWithTitle:title message:alertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"LOGOUT",nil];
    }
    else
    {
        PaumentSuccess = [[UIAlertView alloc] initWithTitle:@"Successful Transaction" message:PAYMENT_SUCCESSFUL delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
    }
    [self placeOrder];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    
    NSLog(@"%@",[pickerArray objectAtIndex:row]);
    deliveryTimeFromString = [NSString stringWithFormat:@"%@",[pickerArray objectAtIndex:row]];
        [deliveryDateAndTimeArray replaceObjectAtIndex:1 withObject:deliveryTimeFromString];
    [deliveryDateAndTimeArray replaceObjectAtIndex:1 withObject:deliveryTimeToString];
    [DeliveryTimeFromButton setTitle:deliveryTimeFromString forState:UIControlStateNormal];
    deliveryTimeToString   = [NSString stringWithFormat:@"%@",[self addTimeIntervalInGivenTime:[pickerArray objectAtIndex:row] withtimeinterval:2 ]];
    [DeliveryTimeToButton setTitle:deliveryTimeToString forState:UIControlStateNormal];
}
// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [pickerArray count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [pickerArray objectAtIndex: row];
    
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = SCREEN_WIDTH;
    
    return sectionWidth;
}
#pragma dynamic line method
- (CGRect)sizeCountForLabel:(UILabel *)label {
    
    CGSize constrain = CGSizeMake(label.bounds.size.width, FLT_MAX);
    // CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constrain lineBreakMode:NSLineBreakByWordWrapping];
   /* CGSize size = [label.text sizeWithFont:label.font
                         constrainedToSize:constrain  // - 40 For cell padding
                             lineBreakMode:NSLineBreakByWordWrapping]; */
    
    
    CGRect labelRect =  [label.text boundingRectWithSize:constrain
                            options:NSStringDrawingUsesLineFragmentOrigin
                         attributes:@{NSFontAttributeName:label.font}
                            context:nil];
    CGSize size = labelRect.size;
    
    return CGRectMake(0, 0, size.width, size.height);
    
}

- (int)lineCountForLabel:(UILabel *)label {
    
    CGSize constrain = CGSizeMake(label.bounds.size.width, FLT_MAX);
    // CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constrain lineBreakMode:NSLineBreakByWordWrapping];
  /*  CGSize size = [label.text sizeWithFont:label.font
                         constrainedToSize:constrain  // - 40 For cell padding
                             lineBreakMode:NSLineBreakByWordWrapping]; */
    
    
    CGRect labelRect =  [label.text boundingRectWithSize:constrain
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:label.font}
                                                 context:nil];
    CGSize size = labelRect.size;
    return ceil(size.height / label.font.lineHeight);
}

@end
