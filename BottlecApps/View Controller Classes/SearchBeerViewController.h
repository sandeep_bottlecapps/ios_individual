//
//  SearchBeerViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 8/25/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "MTBBarcodeScanner.h"
#import "AppDelegate.h"
@interface SearchBeerViewController : BaseViewController{
    
    NSMutableArray *getOfflineFavoriteArray;
   NSMutableArray *productBrowseArray;
    NSMutableArray *globalFavoriteOfflineArray;
   // NSMutableArray *cartBrowseArray;
    
}

@property (nonatomic,retain)NSMutableArray *searchBeerArray;
@property (nonatomic,retain)  NSString *searchItemString;
@property (strong, nonatomic)  UIImageView *SearchStripImageView;
@property (strong, nonatomic)  UIView *ContainerView;
@property (strong, nonatomic)  UITextField *SearchTextField;
//@property (strong, nonatomic)  UIButton    *FilterButton;
@property (strong, nonatomic)  UIButton    *GoButton;
@property (strong, nonatomic)  UITableView         *expandableTableView;

@property(nonatomic,retain)     NSMutableArray  *typeArray;
@property(nonatomic,retain)     NSMutableArray  *subTypeArray;
@property(nonatomic,retain)     NSMutableArray  *sizeArray;
@property(nonatomic,retain)     NSMutableArray  *rangeArray;
@property(nonatomic,retain)     NSString        *PickerState;
@property(nonatomic,retain)     NSMutableArray  *MainArray;
@property(nonatomic,retain)     NSString        *CategoryVal;
@property(nonatomic,retain)     NSMutableArray  *subTypeIDArray;
@property(nonatomic,retain)     NSString        *searchType;
@property (retain, nonatomic)   UIView *transparentView;
@property (retain, nonatomic)   UIView *containerView;

@property (nonatomic, retain)   NSString *searchProductMinPrice;
@property (nonatomic, retain)   NSString *searchProductMaxPrice;
@property (nonatomic, retain)   NSString *searchProductName;

//MTBBarcodeScanner    *scanner
//- (IBAction)GoButton:(id)sender;

- (IBAction)FilterButtonClicked:(id)sender;
@end
