//
//  ShoppingCartViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 8/11/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ShoppingCartViewController.h"
#import "SearchResultViewController.h"
#import "OrderDetailViewController.h"
#import "AsyncImageView.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "LoginViewController.h"

#define UPDATED_LABEL_TAG2 56
@interface ShoppingCartViewController (){
    
    UILabel                 *productQuantityLabel;
    Product                 *productObj;
    NSMutableArray          *ItemsArray;
    NSMutableArray          *orderDetailsArray;
    UILabel                 *lProductQuantyLabel;
    NSString                *deliveryStatusString;
    NSArray                 *ProductIDAndStatusArray;
    UILabel                 *NorecordFoundLabel;
}


//-(void)removeFromShoppingCart;
@end

@implementation ShoppingCartViewController
@synthesize updatedQuantityDict;
-(void)removeNorecordfoundLabel{
    for (UIView *subView in self.defaultTableView.subviews)
    {
        
        if (subView.tag==kTagNorecordFoundLabel)
        {
            
            [subView removeFromSuperview];
        }
        
        
    }
    
    
}
-(void)addNorecordFoundLAbelOnTableView:(UITableView*)tableView{
    NorecordFoundLabel = [[UILabel alloc] init];
    [NorecordFoundLabel setBackgroundColor:[UIColor clearColor]];
    [NorecordFoundLabel setTextColor:[self getColorForBlackText]];
    [NorecordFoundLabel setText:@"No item in shopping cart!"];
    [NorecordFoundLabel sizeToFit];
    NorecordFoundLabel.frame = CGRectMake((tableView.bounds.size.width - NorecordFoundLabel.bounds.size.width) / 2.0f,
                                          50,
                                          NorecordFoundLabel.bounds.size.width,
                                          NorecordFoundLabel.bounds.size.height);
    [tableView insertSubview:NorecordFoundLabel atIndex:0];
}

-(void)getProductQuantityArray{
    
    for (Product *product in ItemsArray) {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        [dict setObject:product.TEMPproductQuanity forKey:@"MaxQuantity"];
        [dict setObject:[NSString stringWithFormat:@"%d",product.orderQuantity] forKey:@"QuantityAddedToCart"];
        
        [arrayForQuantity addObject:dict];
    }
}

-(void)UpdateOrderSummary   {
    float itemCost = 0.00;
    float totalTax=0.00;
    for (Product *product in ItemsArray) {
        if ([product.productSalesPrice intValue]>0.0) {
            itemCost=([product.productSalesPrice floatValue]*product.orderQuantity)+itemCost;
            totalTax=([product.productSalesPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity)+totalTax;
        }
        else {
            itemCost=([product.productPrice floatValue]*product.orderQuantity)+itemCost;
            totalTax=([product.productPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity)+totalTax;
        }
    }
    self.itemCostLabel.text=[self addDollarSignToString:[NSString stringWithFormat:@"%.2f",itemCost]];
    self.taxLabel.text=[self addDollarSignToString:[NSString stringWithFormat:@"%.2f",totalTax]];
    self.orderTotalLabel.text=[self addDollarSignToString:[NSString stringWithFormat:@"%.2f",(itemCost+totalTax)]];
}

-(void)addOrderSummaryView{
    float itemCost = 0.00;
    float totalTax=0.00;
    DLog(@"ItemsArray--%@",ItemsArray);
    for (Product *product in ItemsArray) {
      //  product.orderQuantity=1;
        
        if ([product.productSalesPrice intValue]>0.0) {
            itemCost=([product.productSalesPrice floatValue]*product.orderQuantity)+itemCost;
            totalTax=([product.productSalesPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity)+totalTax;
        }
        else {
            itemCost=([product.productPrice floatValue]*product.orderQuantity)+itemCost;
            totalTax=([product.productPrice floatValue]*[product.ProductTax floatValue]*product.orderQuantity)+totalTax;
        }
    }
    
    UIView *orderSummaryView=[self createContentViewWithFrame:CGRectMake(0, self.defaultTableView.frame.origin.y+ self.defaultTableView.frame.size.height, SCREEN_WIDTH, (SCREEN_HEIGHT/2)-kNavigationheight)];
    orderSummaryView.backgroundColor=[UIColor colorWithRed:240/255.0f green:240/255.0f blue:240/255.0f alpha:1];
    //orderSummaryView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:orderSummaryView];
    //To Put the shadow
    UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.3)];
    [shadowView setBackgroundColor:[UIColor colorWithRed:240/255.0f green:240/255.0f blue:240/255.0f alpha:1]];
    [shadowView.layer setShadowColor:[UIColor blackColor].CGColor];
    [shadowView.layer setShadowOpacity:0.8];
    [shadowView.layer setShadowRadius:0.7];
    [shadowView.layer setShadowOffset:CGSizeMake(2.0, 0.3)];
    [orderSummaryView addSubview:shadowView];
    
    self.ordersummaryLabel=[self createLblWithRect:CGRectMake(kLeftPadding, kPaddingSmall+15.0, SCREEN_WIDTH-(kLeftPadding*2), kLabelHeight) font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize + 2] text:kLabelOrderSummary textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
    [orderSummaryView addSubview:self.ordersummaryLabel];
    
    UILabel *itemCostTitle=[self createLblWithRect:CGRectMake(kLeftPadding, self.ordersummaryLabel.frame.origin.y+ self.ordersummaryLabel.frame.size.height, kLabelWidth, kLabelHeight) font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize] text:kLabelItemsCost textAlignment:NSTextAlignmentLeft textColor:[UIColor colorWithRed:0.4824 green:0.4824 blue:0.4941 alpha:1.0]];
    [orderSummaryView addSubview:itemCostTitle];
    self.itemCostLabel=[self createLblWithRect:CGRectMake(kLeftPadding+(itemCostTitle.frame.origin.x+itemCostTitle.frame.size.width) - 40, self.ordersummaryLabel.frame.origin.y+ self.ordersummaryLabel.frame.size.height,  kLabelWidth, kLabelHeight) font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 2] text:[self addDollarSignToString:[NSString stringWithFormat:@"%.2f",itemCost]] textAlignment:NSTextAlignmentRight textColor: [UIColor colorWithRed:0.6392 green:0.6392 blue:0.6431 alpha:1.0]];
    [orderSummaryView addSubview:self.itemCostLabel];
    
    UILabel *taxTitle=[self createLblWithRect:CGRectMake(kLeftPadding, self.itemCostLabel.frame.origin.y+ self.itemCostLabel.frame.size.height,  kLabelWidth, kLabelHeight) font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize] text:kLabelTax textAlignment:NSTextAlignmentLeft textColor:[UIColor colorWithRed:0.4824 green:0.4824 blue:0.4941 alpha:1.0]];
    [orderSummaryView addSubview:taxTitle];
    self.taxLabel=[self createLblWithRect:CGRectMake(kLeftPadding+(taxTitle.frame.origin.x+taxTitle.frame.size.width - 40), self.itemCostLabel.frame.origin.y+ self.itemCostLabel.frame.size.height,  kLabelWidth, kLabelHeight) font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 2] text:[self addDollarSignToString:[NSString stringWithFormat:@"%.2f",totalTax]] textAlignment:NSTextAlignmentRight textColor:[UIColor colorWithRed:0.6392 green:0.6392 blue:0.6431 alpha:1.0]];
    [orderSummaryView addSubview:self.taxLabel];
    
    UILabel *orderTotalTitle=[self createLblWithRect:CGRectMake(kLeftPadding, self.taxLabel.frame.origin.y+ self.taxLabel.frame.size.height, kLabelWidth, kLabelHeight) font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize] text:kLabelOrderTotal textAlignment:NSTextAlignmentLeft textColor:[UIColor colorWithRed:0.4824 green:0.4824 blue:0.4941 alpha:1.0]];
    [orderSummaryView addSubview:orderTotalTitle];
    self.orderTotalLabel=[self createLblWithRect:CGRectMake(kLeftPadding+(orderTotalTitle.frame.origin.x+orderTotalTitle.frame.size.width- 40), self.taxLabel.frame.origin.y+ self.taxLabel.frame.size.height,  kLabelWidth, kLabelHeight) font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 2] text:[self addDollarSignToString:[NSString stringWithFormat:@"%.2f",(totalTax+itemCost)]] textAlignment:NSTextAlignmentRight textColor:[UIColor colorWithRed:0.6392 green:0.6392 blue:0.6431 alpha:1.0]];
    [orderSummaryView addSubview:self.orderTotalLabel];
    
    self.continueShoppingButton=[self createButtonWithFrame:CGRectMake(kLeftPadding, self.orderTotalLabel.frame.origin.y+ self.orderTotalLabel.frame.size.height+kPaddingSmall,  SCREEN_WIDTH-(kLeftPadding*2), kButtonHeight2) withTitle:kButtonTittleCONTINUESHOPPING];
    [_continueShoppingButton setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
    [_continueShoppingButton.titleLabel setFont:[UIFont fontWithName:kFontBold size:kFontBoldSize + 2]];
    [[_continueShoppingButton layer] setBorderWidth:1.0f];
    [[_continueShoppingButton layer] setBorderColor:[self getColorForRedText].CGColor];
    [self.continueShoppingButton addTarget:self action:@selector(continueShoppingButton:) forControlEvents:UIControlEventTouchUpInside];
    [orderSummaryView addSubview:_continueShoppingButton];
    __weak AppDelegate *appdelegateObj=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    DLog(@"IsDeliveryEnabled_Cart-%@",appdelegateObj.IsDeliveryEnabled_Cart);
    
    if ([appdelegateObj.IsDeliveryEnabled_Cart intValue]==1) {
        self.DeliverButton=[self createButtonWithFrame:CGRectMake(kLeftPadding, self.continueShoppingButton.frame.origin.y+ self.continueShoppingButton.frame.size.height+kTopPadding_small,((SCREEN_WIDTH-(kLeftPadding*2))/2)-2, kButtonHeight2) withTitle:kButtonTittleDELIVER];
        [_DeliverButton setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
        [_DeliverButton.titleLabel setFont:[UIFont fontWithName:kFontBold size:kFontBoldSize + 2]];
        [[_DeliverButton layer] setBorderWidth:1.0f];
        [[_DeliverButton layer] setBorderColor:[self getColorForRedText].CGColor];
        [self.DeliverButton addTarget:self action:@selector(DeliverButton:) forControlEvents:UIControlEventTouchUpInside];
        [orderSummaryView addSubview:_DeliverButton];
        self.PickUpButton=[self createButtonWithFrame:CGRectMake(((self.DeliverButton.frame.size.width)+(self.DeliverButton.frame.origin.x))+kPaddingSmall, self.continueShoppingButton.frame.origin.y+ self.continueShoppingButton.frame.size.height+kTopPadding_small,((SCREEN_WIDTH-(kLeftPadding*2))/2)-2, kButtonHeight2) withTitle:kButtonTittlePICKUP];
        [_PickUpButton setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
        [_PickUpButton.titleLabel setFont:[UIFont fontWithName:kFontBold size:kFontBoldSize + 2]];
        [[_PickUpButton layer] setBorderWidth:1.0f];
        [[_PickUpButton layer] setBorderColor:[self getColorForRedText].CGColor];
        [self.PickUpButton addTarget:self action:@selector(PickUpButton:) forControlEvents:UIControlEventTouchUpInside];
        [orderSummaryView addSubview:_PickUpButton];
    }
    else{
        self.PickUpButton=[self createButtonWithFrame:CGRectMake(kLeftPadding, self.continueShoppingButton.frame.origin.y+ self.continueShoppingButton.frame.size.height+kTopPadding_small ,  SCREEN_WIDTH-(kLeftPadding*2), kButtonHeight2) withTitle:kButtonTittlePICKUP];
        [_PickUpButton setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
        [_PickUpButton.titleLabel setFont:[UIFont fontWithName:kFontBold size:kFontBoldSize + 3]];
        [[_PickUpButton layer] setBorderWidth:1.0f];
        [[_PickUpButton layer] setBorderColor:[self getColorForRedText].CGColor];
        [self.PickUpButton addTarget:self action:@selector(PickUpButton:) forControlEvents:UIControlEventTouchUpInside];
        [orderSummaryView addSubview:_PickUpButton];
    }
}
-(void)getItemsArray{
    ItemsArray=[[NSMutableArray alloc] initWithArray:[[ShoppingCart sharedInstace] fetchProductFromShoppingCart]] ;
    NSLog(@"Item Array = %@", ItemsArray);
   // NSLog(@"%lu", (unsigned long)ItemsArray.count); 
  }

-(void)initializeArrayAndDict{
    arrayForQuantity        =[NSMutableArray new];
    deliveryStatusString    =kEmptyString;
    updatedQuantityDict     =[NSMutableDictionary new];
    
}
-(void)callApiGetAPPSettings{
    NSMutableDictionary* paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
 //   NSLog(@"Param Dict = %@",paramDict);
    
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] GetAPPSettings:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
        NSLog(@"response---%@", response);
        [self hideSpinner];
        DLog(@"response----%@ ",response);
        __weak AppDelegate *appdelegateObj=appdelegateSharedInstance;
        [appdelegateObj setAppSettingWithDict:response];
        
        [self CreateUI];
        
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [UIView showMessageWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage] showInterval:5.0  viewController:self];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        
    }];
}
-(void)CreateUI{
    [self getItemsArray];
    [self AddDefaultTableView];
    [self.defaultTableView setDelegate:self];
    [self.defaultTableView setDataSource:self];
    [self addOrderSummaryView];
    [self getProductQuantityArray];
}

-(void)upDateSearchResultWith:(NSArray*)productoutOfstockArr
{
    DLog(@"");
    searchResult=[[NSMutableArray alloc] initWithArray:[[ShoppingCart sharedInstace] fetchProductFromShoppingCart]] ;
    DLog(@"searchResult----%@",searchResult);
    
    NSLog(@"productoutOfstockArr---%@  searchResult---%@",productoutOfstockArr,searchResult);
    
    [productoutOfstockArr enumerateObjectsUsingBlock:^(id outOfStockProduct, NSUInteger idx, BOOL *stop) {
        int productiDOfoutofStockProd = [[outOfStockProduct objectForKey:@"ProductId"] intValue];
        
        [searchResult enumerateObjectsUsingBlock:^(Product* shopingCartProduct, NSUInteger idx, BOOL *stop) {
            int shopingCartProductId = [[shopingCartProduct productId] intValue];
            if(productiDOfoutofStockProd == shopingCartProductId)   {
                int productQuantityAddedByUser = [shopingCartProduct.TEMPproductQuanity intValue];
                int maxProductAvailable = 0;
                maxProductAvailable=[[outOfStockProduct valueForKey:@"Quantity"] intValue];
                if (maxProductAvailable == 0) {
                    NSArray *tempArray=[NSArray arrayWithObject:shopingCartProduct];
                    [[ShoppingCart sharedInstace] removeProductFromMyShoppingCart:tempArray];
                }
                else if(productQuantityAddedByUser > maxProductAvailable)
                {
                    DLog(@"comes to else part");
                    
                    //productQuantityAddedByUser = maxProductAvailable;
                    [updatedQuantityDict setObject:[NSString stringWithFormat:@"%d",maxProductAvailable] forKey:shopingCartProduct.productId];
                    DLog(@"updatedQuantityDict--%@",updatedQuantityDict);
                    
                }
            }
            
        }];
    }];
    searchResult=[[NSMutableArray alloc] initWithArray:[[ShoppingCart sharedInstace] fetchProductFromShoppingCart]] ;
    ItemsArray=searchResult;
    if([ItemsArray count]==0)
    {
        [self addNorecordFoundLAbelOnTableView:self.defaultTableView];
       
    }
    else{
        [self removeNorecordfoundLabel];
      
    }
    [self.defaultTableView reloadData];
    [self UpdateOrderSummary];
    
}
-(void)GetOUtOfStockProductList:(NSNotification*)notification{

    
    NSDictionary *respDictionary = [[notification userInfo] objectForKey:@"responseArray"];
    ProductIDAndStatusArray = [respDictionary objectForKey:@"QtyList"];
   //  ProductIDAndStatusArray = [[notification userInfo] objectForKey:@"responseArray"];
      DLog(@"ProductIDAndStatusArray============%@",ProductIDAndStatusArray)
    [self upDateSearchResultWith:ProductIDAndStatusArray];


}
-(void)viewWillAppear:(BOOL)animated{
    
    UIView * cartBtn = [self.navigationItem.rightBarButtonItem customView];
    cartBtn.userInteractionEnabled = NO;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(GetOUtOfStockProductList:)
                                                 name:@"CheckForOutOfStock" object:nil];
    if([pickUpClicked intValue]==1 || [deliverClicked intValue]==1)
    {
        //[self AddNavigationControllerWithTitleAndKartButton];
        favoritesAlreadyAdded = @"1";
        [self CallApiAddFavoriteWithProductID:combinedString];
        self.navigationController.navigationBarHidden = NO;
    }
    
    
    
   /* if([kartAddedAndLoggedIn intValue]==1)
    {
        kartAddedAndLoggedIn = 0;
    }
    else
    {
    if([pickUpClicked intValue]==1)
      [self PickUpButton:0];
     if([deliverClicked intValue]==1)
       [self DeliverButton:0];
    } */
//    else if([deliverClicked intValue]==1)
//        [self DeliverButton:nil];
//    else
//    {
    

   // }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializeArrayAndDict];
    [self callApiGetAPPSettings];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton*)button {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ItemsArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kSectionHeight;
}

/*- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 static NSString *simpleTableIdentifier = @"ShoppingCartTableViewCell";
 
 ShoppingCartTableViewCell *cell = (ShoppingCartTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
 UIImageView *backgroundImageView=[self createImageViewWithRect:CGRectMake(0, 0, SCREEN_WIDTH, cell.frame.size.width) image:[UIImage imageNamed:kImageButtonStrip]];
 
 if (cell == nil)
 {
 NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShoppingCartTableViewCell" owner:self options:nil];
 cell = [nib objectAtIndex:0];
 [cell.contentView addSubview:[self getSeperatorV]];
 
 }
 cell.ProductNameLabel.textColor=[self getColorForRedText];
 cell.ProductNameLabel.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize];
 
 
 cell.productPriceLabel.textColor=[self getColorForBlackText];
 cell.productPriceLabel.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize];
 
 cell.ProductQuantityLabel.textColor=[self getColorForBlackText];
 cell.ProductQuantityLabel.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize];
 
 cell.ProductConcentrationLabel.textColor=[self getColorForBlackText];
 cell.ProductConcentrationLabel.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize];
 
 return cell;
 }*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
   
    productObj = [ItemsArray objectAtIndex:indexPath.row];
   
    AsyncImageView *ProductImageView=[self createAsynImageViewWithRect:CGRectMake(3, (kSectionHeight-kProductImageHeight)/2 -1, kProductImageWidth - 6, kProductImageHeight + 1) imageUrl:[NSURL URLWithString:productObj.productImageURL] isThumnail:YES];
    ProductImageView.backgroundColor = [UIColor whiteColor];
    [ProductImageView setContentMode:UIViewContentModeScaleAspectFit];
    ProductImageView.layer.masksToBounds = YES;
    
    UILabel*productNameLabel=[self createLblWithRect:CGRectMake((ProductImageView.frame.size.width)+(ProductImageView.frame.origin.x)+8,3, kLabelWidth*2, kLabelHeight*2) font:[self addFontSemiBold] text:productObj.productName textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
    productNameLabel.numberOfLines=2;
    
    UILabel *updatedLabel=[self createLblWithRect:CGRectMake((ProductImageView.frame.size.width)+(ProductImageView.frame.origin.x)+8, kSectionHeight-20, (ProductImageView.frame.size.width), 20) font:[UIFont fontWithName:kFontBold size:10] text:@"Updated" textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
    [updatedLabel setBackgroundColor:[UIColor clearColor]];
    [updatedLabel setTag:UPDATED_LABEL_TAG2];
    updatedLabel.hidden = YES;
    UILabel*productSizeLabel=[self createLblWithRect:CGRectMake((ProductImageView.frame.size.width)+(ProductImageView.frame.origin.x)+8,(productNameLabel.frame.origin.y)+(productNameLabel.frame.size.height), kLabelWidthSmall + 15, kLabelHeight) font:[UIFont fontWithName:kFontRegular size:kFontRegularSize] text:productObj.productSize textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
    productSizeLabel.adjustsFontSizeToFitWidth=YES;
    productSizeLabel.minimumScaleFactor = 0.8;
    
    UILabel*productPriceLabel=[self createLblWithRect:CGRectMake((productSizeLabel.frame.size.width)+(productSizeLabel.frame.origin.x), (productNameLabel.frame.size.height)+(productNameLabel.frame.origin.y), kLabelWidthSmall, kLabelHeight) font:[UIFont fontWithName:kFontRegular size:kFontRegularSize] text:[self addDollarSignToString:productObj.productPrice] textAlignment:NSTextAlignmentCenter textColor:[self getColorForBlackText]];
    
    if(productObj.productSalesPrice && (![productObj.productSalesPrice isEqualToString:kEmptyString] && [productObj.productSalesPrice floatValue] > 0.00))
        productPriceLabel.text = [self addDollarSignToString:productObj.productSalesPrice];
    
    UIActionButton *deleteButton=[self createActionButtonWithFrame:CGRectMake(SCREEN_WIDTH - 35, 0 ,35, 35)
                                                         withTitle:kEmptyString];
    [deleteButton setImage:[UIImage imageNamed:kImageCrossButtonSmall] forState:UIControlStateNormal];
    [deleteButton setBackgroundColor:[UIColor clearColor]];
    deleteButton.rowNumber = (int)indexPath.row;
    [deleteButton addTarget:self action:@selector(deleteButton:) forControlEvents:UIControlEventTouchUpInside];
    /*************************************************************************************************************************/
    UIView *QuantityView=[self createContentViewWithFrame:CGRectMake((productPriceLabel.frame.size.width)+(productPriceLabel.frame.origin.x), (productNameLabel.frame.size.height)+(productNameLabel.frame.origin.y), 120, kLabelHeight)];
    QuantityView.tag=kTagproductQuantityView;
    [QuantityView setBackgroundColor:[UIColor clearColor]];
    
    UIButton *decQuantityButton=[self createButtonWithFrame:CGRectMake(0,0, (QuantityView.frame.size.width)/3, QuantityView.frame.size.height) withTitle:@"-"];
    decQuantityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [decQuantityButton addTarget:self action:@selector(decQuantityButton:) forControlEvents:UIControlEventTouchUpInside];
    decQuantityButton.tag=indexPath.row;
    [decQuantityButton setTitleColor:[self getColorForBlackText] forState:UIControlStateNormal];
    [decQuantityButton setBackgroundColor:[UIColor clearColor]];
    DLog(@"updatedQuantityDict=======%@",updatedQuantityDict);
    
    if (updatedQuantityDict) {
      // updatedLabel = (UILabel *)[cell viewWithTag:UPDATED_LABEL_TAG2];

        int i=0;
        for (NSString *productidstr in [updatedQuantityDict allKeys]) {
            if ([productidstr intValue]==[[productObj productId]intValue ]) {
                updatedLabel.hidden = NO;
                productObj.orderQuantity = [[[updatedQuantityDict allValues] objectAtIndex:i] intValue];
                
            }
            else {
                updatedLabel.hidden = YES;
            }
            i++;
        }
    }
      productQuantityLabel=[self createLblWithRect:CGRectMake(decQuantityButton.frame.origin.x+decQuantityButton.frame.size.width, 0, QuantityView.frame.size.width/3, QuantityView.frame.size.height) font:[UIFont fontWithName:kFontRegular size:kFontRegularSize] text:[NSString stringWithFormat:@"%d",productObj.orderQuantity] textAlignment:NSTextAlignmentCenter textColor:[self getColorForBlackText]];
    productQuantityLabel.adjustsFontSizeToFitWidth=YES;
    productQuantityLabel.minimumScaleFactor = 0.8;
    productQuantityLabel.tag=kTagproductQuantityLabel;
    
    UIButton *incQuantityButton=[self createButtonWithFrame:CGRectMake(productQuantityLabel.frame.origin.x+productQuantityLabel.frame.size.width,0, QuantityView.frame.size.width/3, QuantityView.frame.size.height) withTitle:@"+"];
    incQuantityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    incQuantityButton.tag=indexPath.row;
    [incQuantityButton addTarget:self action:@selector(incQuantityButton:) forControlEvents:UIControlEventTouchUpInside];
    [incQuantityButton setTitleColor:[self getColorForBlackText] forState:UIControlStateNormal];
    [incQuantityButton setBackgroundColor:[UIColor clearColor]];
    /*************************************************************************************************************************/
    
    if (cell == nil)    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:kDefaultTableViewCell];
    }
    
    [cell addSubview:ProductImageView];
    
    [cell addSubview:productNameLabel];
    [cell addSubview:updatedLabel];
    [cell addSubview:productSizeLabel];
    
    [cell addSubview:productPriceLabel];
    [cell addSubview:deleteButton];
    
    [QuantityView addSubview:productQuantityLabel];
    
    [QuantityView addSubview:incQuantityButton];
    [QuantityView addSubview:decQuantityButton];
    
    [cell addSubview:QuantityView];
    UIImageView *separatorIView = [[UIImageView alloc] initWithFrame:CGRectMake(0, kSectionHeight-2, SCREEN_WIDTH, 0.5)];
    [separatorIView setBackgroundColor:[self getColorForLightGreyColor]];
    [separatorIView.layer setShadowColor:[UIColor blackColor].CGColor];
    [separatorIView.layer setShadowOpacity:0.4];
    [separatorIView.layer setShadowRadius:0.9];
    [separatorIView.layer setShadowOffset:CGSizeMake(2.0, 0.3)];
    [cell addSubview:separatorIView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    updatedLabel = (UILabel *)[cell viewWithTag:UPDATED_LABEL_TAG2];

    return cell;
}


//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"didSelectRowAtIndexPath");
//    /*UIAlertView *messageAlert = [[UIAlertView alloc]
//     initWithTitle:@"Row Selected" message:@"You've selected a row" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];*/
//      // Checked the selected row
////    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
////    cell.accessoryType = UITableViewCellAccessoryNone;
////    cell.selectionStyle=UITableViewCellSelectionStyleNone;
////    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//}

//- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"willSelectRowAtIndexPath");
//    if (indexPath.row == 0) {
//        return nil;
//    }
//
//    return indexPath;
//}
//
#pragma-mark-BUTTONACTION

-(void)deleteButton:(UIActionButton*)button{
    NSArray *temparray=[NSArray arrayWithObjects:[ItemsArray objectAtIndex:button.rowNumber], nil];
    [[ShoppingCart sharedInstace] removeProductFromMyShoppingCart:temparray];
    [self getItemsArray];
    [self.defaultTableView reloadData];
    [self UpdateOrderSummary];
    
    // Cart has zero element, we need to back to search screen.
    if(ItemsArray.count == 0)   {
        [self popViewControllerWithDelay:0.25 complition:nil];
    }
}

-(void)incQuantityButton:(UIButton*)button{
    UIView *view = button.superview; //Cell contentView
    UITableViewCell *cell = (UITableViewCell *)view.superview;
    UIView *tempV= (UIView*)[cell viewWithTag:kTagproductQuantityView ];
    DLog(@"subviews----%@",   [cell subviews]);
    for (UIView *i in [tempV subviews]){
        if([i isKindOfClass:[UILabel class]]){
            lProductQuantyLabel = (UILabel *)i;
            if(lProductQuantyLabel.tag == kTagproductQuantityLabel){
                if(!([[[arrayForQuantity objectAtIndex:button.tag] valueForKey:@"MaxQuantity"] intValue] <= ([[lProductQuantyLabel text] intValue])))   {
                    [lProductQuantyLabel setText:[NSString stringWithFormat:@"%d",[[lProductQuantyLabel text] intValue]+1]];
                    NSDictionary*dict=[[NSDictionary alloc] initWithObjectsAndKeys:[[arrayForQuantity objectAtIndex:button.tag] valueForKey:@"MaxQuantity"],@"MaxQuantity",[lProductQuantyLabel text],@"QuantityAddedToCart", nil];
                    [arrayForQuantity replaceObjectAtIndex:button.tag withObject:dict];
                    __weak Product * product = [ItemsArray objectAtIndex:button.tag];
                    product.orderQuantity = [[lProductQuantyLabel text] intValue];
                    product = nil;
                    [self UpdateOrderSummary];
                    break;
                }
                else {
                
                    [self showAlertViewWithTitle:@"Quantity" message:[NSString stringWithFormat:@"Only %d products in stock!",[[[arrayForQuantity objectAtIndex:button.tag] valueForKey:@"MaxQuantity"] intValue]]];
                
                }
            }
        }
    }
}

-(void)decQuantityButton:(UIButton*)button{
    UIView *view = button.superview; //Cell contentView
    UITableViewCell *cell = (UITableViewCell *)view.superview;
    UIView *tempV= (UIView*)[cell viewWithTag:kTagproductQuantityView ];
    DLog(@"subviews----%@",   [cell subviews]);
    for (UIView *i in [tempV subviews]){
        if([i isKindOfClass:[UILabel class]]){
            lProductQuantyLabel = (UILabel *)i;
            if(lProductQuantyLabel.tag == kTagproductQuantityLabel){
                if(!([[lProductQuantyLabel text] intValue] <= 1))  { //To check for minimum value
                    [lProductQuantyLabel setText:[NSString stringWithFormat:@"%d",[[lProductQuantyLabel text] intValue]-1]];
                    NSDictionary*dict=[[NSDictionary alloc] initWithObjectsAndKeys:[[arrayForQuantity objectAtIndex:button.tag] valueForKey:@"MaxQuantity"],@"MaxQuantity",[lProductQuantyLabel text],@"QuantityAddedToCart", nil];
                    [arrayForQuantity replaceObjectAtIndex:button.tag withObject:dict];
                    __weak Product * product = [ItemsArray objectAtIndex:button.tag];
                    product.orderQuantity = [[lProductQuantyLabel text] intValue];
                    product = nil;
                    [self UpdateOrderSummary];
                    break;
                }
            }
        }
    }
}
-(void)continueShoppingButton:(UIButton*)button{
    SearchResultViewController *vc=[self viewControllerWithIdentifier:kSearchResultViewControllerStoryboardId];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}
-(void)DeliverButton:(UIButton*)button{
    /* if([notloggedIn intValue]==1)
    {
        deliverClicked = @"1";
        LoginViewController *vc = [self viewControllerWithIdentifier:kLoginViewControllerStoryboardId];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {

    } */
    if([notloggedIn intValue]==1)
    {
        deliverClicked = @"1";
        [self showConfirmViewWithTitle:@"Confirm" message:@"In order to proceed further please Sign in or Sign Up for the app." otherButtonTitle:@"Sign In/ Sign Up"];
       // LoginViewController *vc = [self viewControllerWithIdentifier:kLoginViewControllerStoryboardId];
       // [self.navigationController pushViewController:vc animated:YES];
    }
    else if([deliverClicked intValue]==1 && [notloggedIn intValue]==0)
    {
         deliverClicked = @"0";
        [self showSpinner1];
        /* dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            [self CallApiMyaccount];
            [self pushToOrderViewControllerThroughDeliver];
            // Background code that doesn't need to update the UI here
         }); */
        
        dispatch_queue_t serialQueue = dispatch_queue_create("com.cta.serialqueue", DISPATCH_QUEUE_SERIAL);
        dispatch_async(serialQueue, ^{
            [self CallApiMyaccount];
        });
        
        dispatch_async(serialQueue, ^{
            [self pushToOrderViewControllerThroughDeliver];
        });
        dispatch_async(serialQueue, ^{
           [self hideSpinner];
        }); 
    }
    else
    {
      [self pushToOrderViewControllerThroughDeliver];
    }
}

-(void)PickUpButton:(UIButton*)button{
 
    if([notloggedIn intValue]==1)
    {
         pickUpClicked = @"1";
        [self showConfirmViewWithTitle:@"Confirm" message:@"In order to proceed further please Sign in or Sign Up for the app." otherButtonTitle:@"Sign In/ Sign Up"];
    }
    else if([pickUpClicked intValue]==1 && [notloggedIn intValue]==0)
    {
        deliverClicked = @"0";
        [self showSpinner1];
        /* dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
         
         [self CallApiMyaccount];
         [self pushToOrderViewControllerThroughDeliver];
         // Background code that doesn't need to update the UI here
         }); */
        
        dispatch_queue_t serialQueue = dispatch_queue_create("com.cta.serialqueue", DISPATCH_QUEUE_SERIAL);
        dispatch_async(serialQueue, ^{
            [self CallApiMyaccount];
        });
        
        dispatch_async(serialQueue, ^{
            [self pushToOrderViewControllerThroughDeliver];
        });
        dispatch_async(serialQueue, ^{
            [self hideSpinner];
        });

    }
    else
    {
        [self pushToOrderViewControllerThroughPickUp];
    }
  
}
-(void)pushToOrderViewControllerThroughPickUp
{
    OrderDetailViewController *vc=[self viewControllerWithIdentifier:kOrderDetailViewControllerStoryboardId];
    orderDetailsArray =[NSMutableArray new];
    int i=0;
    for (Product *product in ItemsArray) {
        NSDictionary *dict=[[NSDictionary alloc] initWithObjectsAndKeys:product.productId,@"ProductId",[[arrayForQuantity objectAtIndex:i] valueForKey:@"QuantityAddedToCart"],@"Quantity",product.productStatus,@"IsOnSaleItems", nil];
        [orderDetailsArray addObject:dict];
        i++;
    }
  //  NSLog(@"orderDetailsArray-- %@",orderDetailsArray);
    vc.OrderTypeString=kStringPickUp;
    vc.getCartArray=ItemsArray;
    vc.orderArray=orderDetailsArray;
    [self.navigationController pushViewController:vc animated:YES];

}
-(void)pushToOrderViewControllerThroughDeliver
{
    OrderDetailViewController *vc=[self viewControllerWithIdentifier:kOrderDetailViewControllerStoryboardId];
    orderDetailsArray =[NSMutableArray new];
    int i=0;
    for (Product *product in ItemsArray) {
      //  NSDictionary *dict=[[NSDictionary alloc] initWithObjectsAndKeys:product.productId,@"ProductId",[NSNumber numberWithInt:product.orderQuantity],@"Quantity",product.productStatus,@"IsOnSaleItems", nil];
       NSDictionary *dict=[[NSDictionary alloc] initWithObjectsAndKeys:product.productId,@"ProductId",[[arrayForQuantity objectAtIndex:i] valueForKey:@"QuantityAddedToCart"],@"Quantity",product.productStatus,@"IsOnSaleItems", nil];
        [orderDetailsArray addObject:dict];
        i++;
    }
    // NSLog(@"orderDetailsArray-- %@",orderDetailsArray);
    vc.getCartArray=ItemsArray;
    vc.OrderTypeString=kStringDeliver;
    vc.orderArray=orderDetailsArray;
    [self.navigationController pushViewController:vc animated:YES];
}
                                               


@end