//
//  MyOrdersViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 10/7/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "MyOrdersViewController.h"
#define COMMENT_LABEL_WIDTH 230
#define COMMENT_LABEL_MIN_HEIGHT 65
#define COMMENT_LABEL_PADDING 10
#define krowheightProductlist 450
#import "OrderDetailsTableViewCell.h"
#import "ViewMyOrderModel.h"
#import "ViewMyOrderProductListModel.h"
#import "OrderDetails.h"
#import "UIActionButton.h"
#import "Constants.h"

@interface MyOrdersViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableIndexSet    *expandedSections;
    NSMutableDictionary  *orderDetailDict;
    NSArray              *orderDetailSItemArray;
    UIView               *sectionView;
    BOOL                  isClosed;
    UIImage              *plusImage;
    UIImage              *crossImage;
    UIView               *NewOrderView;
    BOOL                 isOpen;
    int                  pagenumber;
    int                  totalCount;
    NSArray              *allitems;
    NSString             *deliveryCharges;
    NSString             *CouponDiscountAmount;

    NSString             *discount;
   
    NSMutableArray *orderStore ;
}

@property (nonatomic, strong) NSMutableArray* orderDataSource;
@end

@implementation MyOrdersViewController
@synthesize searchProductMinPrice,searchProductMaxPrice,searchProductName,searchType;
@synthesize orderDataSource;

-(void)addNewOrderView{
    CGRect ContentainerViewFrame ;
    ContentainerViewFrame.origin.x=0;
    ContentainerViewFrame.origin.y=SCREEN_HEIGHT-kBottomViewHeight-kCustomButtonContainerHeight;
    ContentainerViewFrame.size.width=SCREEN_WIDTH;
    ContentainerViewFrame.size.height=kCustomButtonContainerHeight;
    NewOrderView=[self createContentViewWithFrame:ContentainerViewFrame];
    _NewOrderButton=[self createButtonWithFrame:CGRectMake((SCREEN_WIDTH-kcustomButtonwidth)/2,((NewOrderView.frame.size.height)-kButtonHeight)/2,kcustomButtonwidth,kButtonHeight) withTitle:kEmptyString];
    [_NewOrderButton setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
    [_NewOrderButton setTitle:kButtonTittleNEWORDER forState:UIControlStateNormal];
    [_NewOrderButton.titleLabel setFont:[UIFont fontWithName:kFontBold size:kFontBoldSize+4]];
    [_NewOrderButton addTarget:self  action:@selector(NewOrderButton:) forControlEvents:UIControlEventTouchUpInside];
    [[_NewOrderButton layer] setBorderWidth:1.0f];
    [[_NewOrderButton layer] setBorderColor:[self getColorForRedText].CGColor];
    [NewOrderView setBackgroundColor:[UIColor whiteColor]];
    
    [NewOrderView addSubview:_NewOrderButton];
    [NewOrderView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:NewOrderView ];
}

-(void)CreateUI{
    [self AddDefaultTableView];
    [self addNewOrderView];
}

-(void)UpdateUI{
    arrayForBool=[[NSMutableArray alloc]init];
    for (int i=0; i<[self.orderDataSource count]; i++) {
        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    }
    [self.defaultTableView setDelegate:self];
    [self.defaultTableView setDataSource:self];
    [self.defaultTableView reloadData];
}

-(void)CallApiViewMyOrderWithPage:(int)page{
    __weak User *userObj=[[ModelManager modelManager] getuserModel];
    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
    [paramDict setValue:[NSString stringWithFormat:@"%d",page] forKey:kParamPageNumber];
    [paramDict setValue:@"1000" forKey:kParamPageSize];
    [paramDict setValue:kSTOREIDbottlecappsString forKey:kParamStoreId];
    [paramDict setValue:userObj.userId forKey:kParamUserId];
    [self showSpinner];
    [[ModelManager modelManager] ViewMyOrderWithParam:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
       NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            //getDetailArray=response;
            totalCount=[[response valueForKey:@"totalNoOfRecords"] intValue];
            deliveryCharges=[NSString stringWithFormat:@"%@",[response valueForKey:@"DeliveryCharges"]];
            
            
            [self hideSpinner];
            if (totalCount!=0) {
                /**********************New code*****************************/
                if(!self.orderDataSource)   {
                    self.orderDataSource = [NSMutableArray array];
                }
                
               orderStore = [response valueForKey:@"productCount"];
                NSString *storeAddress = [NSString stringWithFormat:@"%@\n%@, %@ %@",[response valueForKey:@"StoreAddress"],
                                          [response valueForKey:@"StoreCity"],
                                          [response valueForKey:@"StoreStateName"],
                                          [response valueForKey:@"StoreZip"]];
                
                for (NSDictionary *order in orderStore) {
                    OrderDetails *orderDetails = [[OrderDetails alloc] initWithOrderDetails:order storeAddress:storeAddress];
                    [self.orderDataSource addObject:orderDetails];
                   // NSLog(@"%@",[order objectForKey:@"CouponDiscountAmount"]);
                    

                }
                /**********************New code*****************************/
                [self UpdateUI];
            }
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}

-(void)initScrollViewContent {
    self.CustomScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT/2);
    self.CustomScrollView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Home_Img2.jpg"]];
    [self.CustomScrollView setScrollEnabled:NO];
    
}
-(void)initializeArrayAndDict{
    orderDetailDict=[NSMutableDictionary new];
    orderDetailSItemArray=@[@"Order Details:",@"Order Summary:",@"DeliveryAddress:",@"Payment Details:"];
    allitems =[NSArray new];
    pagenumber=1;
    totalCount=0;
}
- (void)viewDidLoad {
      [super viewDidLoad];
      [self indirectViewDidLoad];
  }

-(void)indirectViewDidLoad
{
    self.orderDataSource = [NSMutableArray array];
    DLog(@"");
    isClosed=YES;
    isOpen=NO;
    [self initializeArrayAndDict];
    
    [self addScrollView];
    [self initScrollViewContent];
    [self CreateUI];
    [self CallApiViewMyOrderWithPage:pagenumber];
}
-(void) dealloc {
    [self.orderDataSource removeAllObjects];
    self.orderDataSource = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//This just a convenience function to get the height of the label based on the comment text
#pragma mark - UITableView Delegate & Datasrouce -
#pragma mark - UITableView Delegate & Datasrouce -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.orderDataSource.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section    {
    if ([[arrayForBool objectAtIndex:section] boolValue]==1) {
        return 1;
    }
    else
        return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
        __weak OrderDetails *orderDetails = [self.orderDataSource objectAtIndex:indexPath.section];
        float rowHeight;
        if([orderDetails.isCancelable isEqualToString:@"1"])
         rowHeight = 480; // standard height of summary base view if status is in pending for cancel order
        else
            rowHeight = 435; // standard height of summary base view
        if(!orderDetails.isOrderPickUp) {
            if([orderDetails.isCancelable isEqualToString:@"1"])
              rowHeight += 50;
            else
                rowHeight += 40;
        }
        rowHeight += (orderDetails.orderItemStore.count * 20);   //standard height of label
        orderDetails = nil;
        return rowHeight;
    }
    return 0;
}

/* - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *lindexPath = [NSIndexPath indexPathForRow:0 inSection:indexPath.section];
    [self openOrderDetailsWithIndexPath: lindexPath];

    
//   // Close the section, once the data is selected
//    [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:NO]];
//    
//    [self.defaultTableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
//} */

#pragma mark - Creating View for TableView Section
#pragma mark - Table header gesture tapped

-(void)CreateUIForOrderDetail{
    [NewOrderView removeFromSuperview];
    CGRect rect=self.defaultTableView.frame;
    [self setNavBarTitle:kNavigationTitleORDERDETAILS controller:self italic:NO];
    rect.origin.y =kNavigationheight;
    rect.size.height =SCREEN_HEIGHT-kNavigationheight-kBottomViewHeight;
    self.defaultTableView.frame=rect;
    
    
}
-(void)CreateUIForOrderList{
    [self addNewOrderView];
    CGRect rect=self.defaultTableView.frame;
    [self setNavBarTitle:kNavigationTitleVIEWORDERS controller:self italic:NO];
    rect.origin.y =(self.CustomScrollView.frame.origin.y+self.CustomScrollView.frame.size.height);
    rect.size.height =SCREEN_HEIGHT-(self.CustomScrollView.frame.origin.y+self.CustomScrollView.frame.size.height)-kBottomViewHeight;
    self.defaultTableView.frame=rect;
}

-(void)openCloseButtonClicked:(UIButton*)sender{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    [self openOrderDetailsWithIndexPath: indexPath];
}

-(void) openOrderDetailsWithIndexPath:(NSIndexPath *) indexPath {
    if ([self.navigationController.title isEqualToString:kNavigationTitleORDERDETAILS]) {
        [self CreateUIForOrderList];
    }
    else{
        [self CreateUIForOrderDetail];
    }

    if (indexPath.row == 0) {
        isClosed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
        
       // NSLog(@"Order Source = %@",self.orderDataSource);
        for (int i=0; i<[self.orderDataSource count]; i++) {
            if (indexPath.section==i) {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!isClosed]];
            }
            else    {
                if([[arrayForBool objectAtIndex:i] boolValue])  {
                    [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
                    [indexSet addIndex:i];
                }
            }
        }
        [indexSet addIndex:indexPath.section];
        [self.defaultTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
    }

}

-(NSArray *) convertDateFormat:(NSString *) orignalDate    {
    
    NSMutableArray* dateandTimeArray = [NSMutableArray arrayWithArray: [orignalDate componentsSeparatedByString: @" "]];
    NSString *finalTimeString = [dateandTimeArray objectAtIndex:1];
    finalTimeString = [finalTimeString substringWithRange:NSMakeRange(0, 4)];
    [dateandTimeArray replaceObjectAtIndex:1 withObject:finalTimeString];
    return dateandTimeArray;
}
-(NSString*)ChangeTimeZoneTodeviceLocalTimeZone:(NSString*)dateString{
    //11/23/2015 12:22:33
    // create dateFormatter with UTC time format
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSDate *date = [dateFormatter dateFromString:dateString]; // create date from string
    
    // change to a readable time format and change to local time zone
    [dateFormatter setDateFormat:@"MM.dd.yyyy | hh:mm a"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *localFormatDate = [dateFormatter stringFromDate:date];
    
    return localFormatDate;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section==[self.orderDataSource count]) {
        sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,kSectionHeight)];
        UILabel * lbl=[self createLblWithRect:CGRectMake(0, 0, SCREEN_WIDTH, kSectionHeight) font:[self addFontRegular] text:kTapToloadMore textAlignment:NSTextAlignmentCenter textColor:[self getColorForRedText]];
        [sectionView addSubview:lbl];
        return  sectionView;
    }
    else{
        __weak OrderDetails *orderDetails = [self.orderDataSource objectAtIndex:section];
        
        sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,kSectionHeight)];
        sectionView.tag=section;
        UIImageView *BgImageV=[self createImageViewWithRect:CGRectMake(0, 0, SCREEN_WIDTH,kSectionHeight) image:[UIImage imageNamed:kImageButtonStrip]];
        
        [sectionView addSubview:BgImageV];
        self.ProductNameLabel=[self createLblWithRect:CGRectMake(kPaddingSmall*10 - 15,
                                                                 kPaddingSmall*4,
                                                                 kcellLabelwidth,
                                                                 kcellLabelheight) font:[self addFontSemiBold] text:orderDetails.orderNumber textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
        [sectionView addSubview:self.ProductNameLabel];
        self.ProductStatusLabel=[self createLblWithRect:CGRectMake(self.ProductNameLabel.frame.origin.x+self.ProductNameLabel.frame.size.width,
                                                                   kPaddingSmall*4,
                                                                   kcellLabelwidth,kcellLabelheight) font:[self addFontRegular]
                                                   text:orderDetails.orderStatus textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
        
        [sectionView addSubview:self.ProductStatusLabel];
        
        
        self.ProductPriceLabel=[self createLblWithRect:
                                CGRectMake(self.ProductStatusLabel.frame.origin.x+self.ProductStatusLabel.frame.size.width,
                                                                  kPaddingSmall*4,
                                                                  kcellLabelwidth,kcellLabelheight) font:[self addFontRegular]
                                                  text:[NSString stringWithFormat:@"$ %@",orderDetails.orderTotal]
                                         textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
        [self.ProductPriceLabel setBackgroundColor:[UIColor clearColor]];
        [sectionView addSubview:self.ProductPriceLabel];

        self.DateAndTimeLabel=[self createLblWithRect:CGRectMake(kPaddingSmall*10 - 15,
                                                                 self.ProductNameLabel.frame.origin.y+self.ProductNameLabel.frame.size.height+kPaddingSmall,
                                                                 SCREEN_WIDTH,kcellLabelheight) font:[UIFont fontWithName:kFontRegular size:kFontRegularSize]
                                                 text:[self ChangeTimeZoneTodeviceLocalTimeZone: orderDetails.orderDateTime]
                                        textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
        [sectionView addSubview:self.DateAndTimeLabel];
        plusImage=[UIImage imageNamed:kImageDownArrow];
        crossImage=[UIImage imageNamed:kImageUpIcon];
        UIImageView *openCloseImageV;
        DLog(@"%@",[arrayForBool objectAtIndex:section]);
        if ([[arrayForBool objectAtIndex:section] intValue]==0) {
            openCloseImageV=[self createImageViewWithRect:CGRectMake(SCREEN_WIDTH-(plusImage.size.width+kRightPadding),
                                                                     (kSectionHeight-plusImage.size.width)/2,
                                                                     plusImage.size.width,
                                                                     plusImage.size.height) image:plusImage];
        }
        else{
            openCloseImageV=[self createImageViewWithRect:CGRectMake(SCREEN_WIDTH-(crossImage.size.width+kRightPadding),
                                                                     (kSectionHeight-crossImage.size.width)/2,
                                                                     crossImage.size.width,
                                                                     crossImage.size.height) image:crossImage];
            
        }
        [openCloseImageV setTag:999];
        [sectionView addSubview:openCloseImageV];
        UIActionButton *openCloseButton=[self createActionButtonWithFrame:CGRectMake(0, 0,sectionView.frame.size.width, sectionView.frame.size.height) withTitle:nil];
        [openCloseButton addTarget:self action:@selector(openCloseButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [openCloseButton setTag:section];
        [openCloseButton setBackgroundColor:[UIColor clearColor]];
        [sectionView addSubview:openCloseButton];
        
        return  sectionView;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    if (section==[self.orderDataSource count]) {
        return kButtonHeight;
    }
    else
        return kSectionHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    static NSString *CellIdentifier = @"OrderDetailsTableViewCell";
    OrderDetailsTableViewCell *cell = (OrderDetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[OrderDetailsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (self.orderDataSource && [self.orderDataSource objectAtIndex:indexPath.row])
        [cell setOrderDetailsWithOrderDetails:[self.orderDataSource objectAtIndex:indexPath.section]
                        navigationBtnSelector:@selector(getLocationButtonClicked:) cancelButtonSelector:@selector(confirmCancelOrder:) target:self];
    
    return cell;
}

#pragma MARK-IBACTIONMETHODS
-(void)NewOrderButton:(id)sender{
    [self performSegueWithIdentifier:kGoToSearchResultViewControllerSegueId sender:nil];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
}

#pragma-MARK BUTTON ACTION
-(void)getLocationButtonClicked:(UIButton*)sender{
  
    __weak ClosestStoreModel* lClosestStoreModelObj = kGetClosestStoreModel;
    __weak StoreDetails *storeDetails = kGetStoreDetailsModel;
    [self ShowLocationWithLat:storeDetails.StoreLatitude longitude:storeDetails.StoreLongitude];
}

-(void)confirmCancelOrder:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Confirm" message:@"Are you sure to cancel this order"
                                                  delegate:self cancelButtonTitle:@"Don't Cancel" otherButtonTitles:@"Cancel", nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
    NSLog(@"Button Index = %ld",(long)buttonIndex);
    [self cancelBookedOrder];
    }

}
     
-(void)cancelBookedOrder
{
    
        NSArray *cancelParamArray = [cancelCombinedString componentsSeparatedByString:@"##"];
         NSMutableDictionary* paramDict = [NSMutableDictionary dictionary];
         [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
         [paramDict setObject:[cancelParamArray objectAtIndex:0] forKey:kOrderId];
         [paramDict setObject:[cancelParamArray objectAtIndex:1] forKey:kCurrentStatusId];
            NSLog(@"Param Dict = %@",paramDict);
         
         [self showSpinner1]; // To show spinner
         [[ModelManager modelManager] cancelBookedOrders:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
              NSLog(@"response---%@", response);
             [self hideSpinner];
             NSString *successMessage = [response objectForKey:@"IsSuccess"];
             if([successMessage intValue]==1)
             {
                 [self showAlertViewWithTitle:@"Success" message:[response objectForKey:@"Message"]];
             }
             else
             {
                 [self showAlertViewWithTitle:@"Failure" message:[response objectForKey:@"Message"]];
             }
             [self CallApiViewMyOrderWithPage:pagenumber];
             [self.orderDataSource removeAllObjects];
             [self.defaultTableView reloadData];
             DLog(@"response----%@ ",response);
             
         } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
             [self hideSpinner];
             [UIView showMessageWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage] showInterval:5.0  viewController:self];
             DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
             
         }];
     }

@end
