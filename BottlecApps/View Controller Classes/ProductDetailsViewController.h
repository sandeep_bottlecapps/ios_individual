//
//  ProductDetailsViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 9/3/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface ProductDetailsViewController : BaseViewController
@property (nonatomic,retain)NSString *productIDSelected;
@property (nonatomic,retain)NSDictionary *getproduct;
@property (nonatomic,retain)NSString *isfav;
@property (nonatomic,retain)NSArray *getProductarray;

@end
