//
//  ForgotPasswordViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 8/24/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ForgotPasswordViewController : BaseViewController
@property (retain, nonatomic)  UIButton                   *SubmitButton;
@property (retain, nonatomic)  UITextField                *UserEmailTextField;


@end
