//
//  NotificationHistoryViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 9/16/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface NotificationHistoryViewController : BaseViewController
@property (nonatomic,retain)NSArray *getNotificationArray;
@end
