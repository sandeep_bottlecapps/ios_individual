//
//  EditAccountViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 8/18/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "BaseViewController.h"
#import <CoreLocation/CoreLocation.h>
@interface EditAccountViewController  :BaseViewController<UITextFieldDelegate,DobEditedDelegate,CLLocationManagerDelegate>
{
 CLLocationManager *location;
 CLLocation *loc;
 CLGeocoder *geocoder;
 CLPlacemark *placemark;
}

@property (retain, nonatomic)  UIImageView *CoverPhotoImageView;
@property (retain, nonatomic)  UILabel *UserNameLabel;
@property (retain, nonatomic)  UILabel *DateOfBirthLabel;
@property (retain, nonatomic)  UILabel *TitleLabel;

@property (retain, nonatomic)  UILabel *CardNumberLabel;
@property (retain, nonatomic)  UILabel *addressLabel;
@property (retain, nonatomic)  UILabel *contactLabel;
@property (retain, nonatomic)  UIButton     *SaveButton;
@property (retain, nonatomic)  UIButton     *EditPhotoButton;
@property (retain, nonatomic)  UIButton *findMe;

//@property (retain, nonatomic)  UIView                     *dropDownView;
//@property (nonatomic,retain)   UIDatePicker               *datePicker;
//
//

@end
