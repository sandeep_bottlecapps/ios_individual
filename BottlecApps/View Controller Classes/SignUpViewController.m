//
//  SignUpViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 8/12/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "SignUpViewController.h"
#import "BarcodeScannerViewController.h"
#import "BaseViewController.h"
#import "Constants.h"
@interface SignUpViewController ()
{
    BOOL check;
}
@end

@implementation SignUpViewController
@synthesize barcodeTypeString;
@synthesize selectedDOBString;
-(void)AddTermsAndconditionWithIAgreeButton{
    
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName: [self getColorForRedText],
                              NSFontAttributeName: [UIFont fontWithName:kFontRegular size:kFontRegularSize],
                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)
                              };
    NSString * text = kMessageIagree2;
    NSMutableAttributedString *attributedText =
    [[NSMutableAttributedString alloc] initWithString:text
                                           attributes:attribs];
    // Red text attributes
    NSRange redTextRange = [text rangeOfString:kMessagetermsAndConditions];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                    NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)
                                    }
                            range:redTextRange];
    // Gray text attributes
    NSRange grayTextRange = [text rangeOfString:kMessageIAgreeTo2];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
  
    if(kStoreBottleCapps == 11)
        [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}
                                range:grayTextRange];
    else
     [attributedText setAttributes:@{NSForegroundColorAttributeName:[self getColorForBlackText]}
                            range:grayTextRange];
//    NSRange grayTextRange1 = [text rangeOfString:kMessageAndCertify];
//    [attributedText setAttributes:@{NSForegroundColorAttributeName:[self getColorForBlackText]}
//                            range:grayTextRange1];
    self.TermsAndConditionButton=[self createButtonWithFrame:CGRectMake(0,self.signUpButton.frame.origin.y+self.signUpButton.frame.size.height+kPaddingSmall, SCREEN_WIDTH, kTEXTfieldHeight) withTitle:kEmptyString];
    [self.TermsAndConditionButton.titleLabel setFont:[UIFont fontWithName:kFontRegular size:kFontRegularSize]];
    self.TermsAndConditionButton .titleLabel.numberOfLines=2;
    _TermsAndConditionButton.backgroundColor=[UIColor clearColor];
    [self.TermsAndConditionButton setAttributedTitle:attributedText forState:UIControlStateNormal];
    _TermsAndConditionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    _TermsAndConditionButton.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
    [_TermsAndConditionButton addTarget:self action:@selector(TermsAndConditionButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.CustomScrollView addSubview:self.TermsAndConditionButton];
    
  /*  UIButton *termsButton=[self createButtonWithFrame:CGRectMake(0,self.signUpButton.frame.origin.y+self.signUpButton.frame.size.height+kPaddingSmall, SCREEN_WIDTH, kTEXTfieldHeight) withTitle:nil];
    termsButton.backgroundColor=[UIColor clearColor];
    [termsButton addTarget:self action:@selector(TermsAndConditionButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.CustomScrollView addSubview:termsButton]; */
    
    
}
-(void)addAppLogoImageInCenterOFLoginScreen{

    /***********create image view with bottlecapps logo*******************/
    UIImage *logoimage=[UIImage imageNamed:kImageLogo];
    if(kStoreImage == 1)
        self.LogoImageView= [self createImageViewWithRect:CGRectMake((SCREEN_WIDTH-logoimage.size.width)/2, kTopPadding2, logoimage.size.width, logoimage.size.height)  image:logoimage];
    else
        self.LogoImageView= [self createImageViewWithRect:CGRectMake((SCREEN_WIDTH-logoimage.size.width)/2-26, kTopPadding2, logoimage.size.width+50.0, logoimage.size.height+10.0)  image:logoimage];
    [self.CustomScrollView addSubview:self.LogoImageView];
    /***********create image view with bottlecapps logo*******************/
}
-(void)createStep1ForSignUpPage{
      /***********create text field with lastname *******************/
    [self addAppLogoImageInCenterOFLoginScreen];
    UILabel *almostDoneLabel=[self createLblWithRect:CGRectMake(0, (self.LogoImageView.frame.origin.y+self.LogoImageView.frame.size.height), SCREEN_WIDTH, kLabelHeight) font:[self addFontBold] text:@"Almost done!" textAlignment:NSTextAlignmentCenter textColor:[self getColorForRedText]];
    [self.CustomScrollView addSubview:almostDoneLabel];
    
    
    /***********create text field with email *******************/
    self.emailTextfield= [self createTxtfieldWithRect:CGRectMake(kRightPadding,(almostDoneLabel.frame.origin.y+almostDoneLabel.frame.size.height)+(kPaddingSmall),SCREEN_WIDTH-(kRightPadding*2)
                                                                 ,kTEXTfieldHeight) text:@"" withLeftImage:[UIImage imageNamed:kImage_emailicon] withPlaceholder:kTextfieldPlaceholderEmail withTag:kTextfieldTagSignupEmail];
    self.emailTextfield.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextfield.autocapitalizationType = UITextAutocapitalizationTypeNone;

    [self.emailTextfield setDelegate:self];
    [self.emailTextfield setFont:[UIFont fontWithName:kFontRegular size:kFontRegularSize]];
    [self.emailTextfield setKeyboardType:UIKeyboardTypeEmailAddress];
    [self.CustomScrollView addSubview:self.emailTextfield];
    /***********create text field with email *******************/
    
    /***********create text field with password *******************/
    self.passwordTextfield= [self createTxtfieldWithRect:CGRectMake(kRightPadding,(self.emailTextfield.frame.origin.y+self.emailTextfield.frame.size.height)+(kPaddingSmall),SCREEN_WIDTH-(kRightPadding*2)
                                                                    ,kTEXTfieldHeight) text:@"" withLeftImage:[UIImage imageNamed:kImagePasswordIcon] withPlaceholder:kTextfieldPlaceholderPasswordWithStar withTag:kTextfieldTagSignupPassword];
    [self.passwordTextfield setDelegate:self];
    [self.passwordTextfield setFont:[UIFont fontWithName:kFontRegular size:kFontRegularSize]];
    [self.passwordTextfield setSecureTextEntry:YES];
    [self.CustomScrollView addSubview:self.passwordTextfield];
    /***********create text field with password *******************/
    
    /**************adding button for next ********************/
    self.signUpButton = [self createButtonWithFrame:CGRectMake(((SCREEN_WIDTH-kcustomButtonwidth)/2),(self.passwordTextfield.frame.origin.y+self.passwordTextfield.frame.size.height)+(kTopPadding),kcustomButtonwidth
                                                               ,kButtonHeight) withTitle:kButtonTitleSignUp];
    [self.signUpButton setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
    [self.signUpButton.titleLabel setFont:[UIFont fontWithName:kFontBold size:kFontBoldSize]];
    [[self.signUpButton layer] setBorderWidth:1.0f];
    [[self.signUpButton layer] setBorderColor:[self getColorForRedText].CGColor];
    [self.signUpButton addTarget:self action:@selector(signUpButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.CustomScrollView addSubview:self.signUpButton];
    /**************adding button for next ********************/
     [self AddTermsAndconditionWithIAgreeButton];
    
}


-(void)initScrollViewContent {

        [self.CustomScrollView setBackgroundColor:[UIColor clearColor]];
    /************************set content size to scroll view***************/
    self.CustomScrollView.contentSize = CGSizeMake(kCustomScrollViewWidth,kCustomScrollViewHeight);
    /************************set content size to scroll view***************/
    [self.CustomScrollView setScrollEnabled:YES];
    [self createStep1ForSignUpPage];

   // [self createStep2ForSignUpPage];
    
   }



-(void)CallApiVerifyEmailId{
    NSMutableDictionary* paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:self.emailTextfield.text forKey: kParamEmailId];
    [paramDict setObject:@"1" forKey: kParamPageNumber];
    [paramDict setObject:@"10" forKey: kParamPageSize];
    // getEventsWithEventsList
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager]VerifyEmailIdWithParam:paramDict successStatus:^(BOOL status) {
        [self hideSpinner];
        if (status==YES) {
            [self CallApiSignUp];
        }
        else{
            [self showAlertViewWithTitle:nil message:kAlertVerifyEmail];
        }
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:nil message:errorMessage];
    }];
    
}

-(void)CallApiSignUp{
    [self.view endEditing:YES];
    NSMutableDictionary *SignupDict=[[NSMutableDictionary alloc] init];
    if([self formValidate]){
        //selectedDOBString
        [SignupDict setObject:[self.emailTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]forKey: kSignupParamEmailId];
        [SignupDict setObject:[self.passwordTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]forKey: kSignupParamPassword];
     
        if (selectedDOBString==nil) {
            NSString *stringToFormat = selectedDOBString;
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MM-dd-yyyy"];
            NSDate *date = [dateFormatter dateFromString:stringToFormat];
            [dateFormatter setDateFormat:@"MM-dd-yyyy"];
            [dateFormatter stringFromDate:date];
            DLog(@"%@",[dateFormatter stringFromDate:date]);
            [SignupDict setObject:[dateFormatter stringFromDate:date] forKey: kSignupParamdateofBirth];
            
        }
        else{
            [SignupDict setObject:[selectedDOBString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey: kSignupParamdateofBirth];
        }
        

        DLog(@"image--%@",self.userProfileImage);
        [SignupDict setObject:@"" forKey: kSignupParamGender];
        [SignupDict setObject:@"" forKey: kSignupParamAddress];
        [SignupDict setObject:@"" forKey: kSignupParamApartment];
        [SignupDict setObject:@"" forKey: kSignupParamZipCode];
        [SignupDict setObject:@"" forKey: kSignupParamCountryId];
        [SignupDict setObject:@"" forKey: kSignupParamStateName];
        [SignupDict setObject:@"" forKey: kSignupParamCity];
        [SignupDict setObject:kSTOREIDbottlecappsString forKey: kSignupParamstoreid];
        if (TARGET_IPHONE_SIMULATOR){
            [SignupDict setObject:kDEVICETOKENString forKey: kUserDeviceId];
            
        }
        else{
            [SignupDict setObject:[[ModelManager modelManager] getCurrentDeviceToken] forKey: kSignupParamDeviceId];
            
        }
        
        [SignupDict setObject:@"" forKey: kSignupParamRegionId];
        [SignupDict setObject:@"I" forKey: kSignupParamDeviceType];
        DLog(@"SignupDict---%@",SignupDict);
        [self showSpinner]; // To show spinner
        [[ModelManager modelManager] SignupWithUserCredentials:SignupDict  successStatus:^(BOOL status) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.8 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self hideSpinner];
                [self CallApiGetSelectedProductCategory];
                 [[ModelManager modelManager] archiveIsUserAlreadyLoggedInVariable:YES];
                
                [[ShoppingCart sharedInstace] loadShopingCartfromArchiver];
                if([pickUpClicked intValue]==1||[deliverClicked intValue]==1)
                {
                     signupBack = @"1";
                    [self performSegueWithIdentifier:kGoToShoppingCartViewControllerSegueId sender:nil];
                }
                else
                    [self performSegueWithIdentifier:kGoToHomeViewControllerSegueId sender:nil];
                
                
                
                
            });
        } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
            [self hideSpinner];
            [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
            
            
        }];
        
        
        
    }


}

#pragma mark-viewController Life cycle

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if ([self.barcodeTypeString isEqualToString:kBarcodeTypeSignUp]) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receivebarcodeNotification:)
                                                     name:@"barcodeNotification"
                                                   object:nil];
        
    }

    NSURL* ImageURL = [NSURL URLWithString:self. updatedImageUrl];
    NSData *imageData = [NSData dataWithContentsOfURL:ImageURL];
    if ( self.updatedImageUrl) {
        [self.addAPhotoButton setTitle:kEmptyString forState:UIControlStateNormal];
        [self.addAPhotoButtonStep2 setTitle:kEmptyString forState:UIControlStateNormal];
        [self.addAPhotoButton setBackgroundColor:[UIColor clearColor]];
        [self.addAPhotoButtonStep2 setBackgroundColor:[UIColor clearColor]];

        [self.addAPhotoButton setImage: [self centerCropImage:[UIImage imageWithData:imageData]] forState:UIControlStateNormal];
        [self.addAPhotoButtonStep2 setImage: [self centerCropImage:[UIImage imageWithData:imageData]] forState:UIControlStateNormal];
        
    }
    else{
        [self.addAPhotoButton setTitle:kButtonTitleAddAPhoto forState:UIControlStateNormal];
        [self.addAPhotoButtonStep2 setBackgroundColor:[UIColor clearColor]];
        [self.addAPhotoButtonStep2 setImage: [UIImage imageNamed:kImage_PlaceholderProfile] forState:UIControlStateNormal];
    }
    DLog(@"barcodeID%@",[[NSUserDefaults standardUserDefaults]valueForKey:kBarcodeID]);
    if ([[NSUserDefaults standardUserDefaults]valueForKey:kBarcodeID]) {
        self.loyaltyCardTextfield.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:kBarcodeID]];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:kBarcodeID];
    }
}
-(void)receivebarcodeNotification:(NSNotification*)notification{
    
    [self GoBack];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    DLog(@"selectedDOBString=========%@",selectedDOBString);
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = NO;
    /************** set up navigation controller  ******************/
    [self.refreshButton addTarget:self action:@selector(refreshButton:)
                 forControlEvents:UIControlEventTouchUpInside];
    /************** set up navigation controller with image******************/
     [self addScrollView];
    [self initScrollViewContent];
    self.navigationItem.hidesBackButton = YES;
 }
- (void) UploadimageNotification:(NSNotification *) notification{
  NSURL* ImageURL = [NSURL URLWithString:self. updatedImageUrl];
    NSData *imageData = [NSData dataWithContentsOfURL:ImageURL];
    DLog(@"UIImage%@",[UIImage imageWithData:imageData]);
       if ( self.updatedImageUrl) {
        [self.addAPhotoButton setTitle:kEmptyString forState:UIControlStateNormal];
        [self.addAPhotoButtonStep2 setTitle:kEmptyString forState:UIControlStateNormal];
        [self.addAPhotoButton setBackgroundColor:[UIColor clearColor]];
        [self.addAPhotoButtonStep2 setBackgroundColor:[UIColor clearColor]];
        
           [self.addAPhotoButton setImage: [UIImage imageWithData:imageData] forState:UIControlStateNormal];
        [self.addAPhotoButtonStep2 setImage: [UIImage imageWithData:imageData] forState:UIControlStateNormal];
        
    }
    else{
        [self.addAPhotoButton setTitle:kButtonTitleAddAPhoto forState:UIControlStateNormal];
        [self.addAPhotoButtonStep2 setBackgroundColor:[UIColor clearColor]];
        [self.addAPhotoButtonStep2 setImage: [UIImage imageNamed:kImage_PlaceholderProfile] forState:UIControlStateNormal];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(UploadimageNotification:)
                                                 name:@"Uploadimage"
                                               object:nil];
    [SlideNavigationController sharedInstance].enableSwipeGesture = NO;
}

-(void)viewDidDisappear:(BOOL)animated  {
    if (termsAndConditionsChecked == YES) {
        [SlideNavigationController sharedInstance].enableSwipeGesture = NO;
        termsAndConditionsChecked = NO;
    }
    else
        [SlideNavigationController sharedInstance].enableSwipeGesture = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(BOOL)formValidate
{
    check = FALSE;
   NSString *errMsg = @"Error";
//    if ([self.firstNameTextfield.text length]==0)
//    {
//        check = TRUE;
//        errMsg = kAlertEnterfirstName;
//    }
//    else if([self.lastNameTextfield.text length]==0)
//    {
//        check = TRUE;
//        errMsg = kAlertEnterLastName;
//    }
     if([self.emailTextfield.text length]==0)
    {
        check = TRUE;
        errMsg = kAlertEnterEmail;
    }
     else if(![self validateEmail:self.emailTextfield.text])
     {
         check = TRUE;
         errMsg = kAlertValidEmail;
     }
    
    else if([self.passwordTextfield.text length]==0)
    {
        check = TRUE;
        errMsg = kAlertPassword;
    }
    else if([self.passwordTextfield.text length]<4)
    {
        check = TRUE;
        errMsg = kAlertMinPassword;
    }
    else if([selectedDOBString length]==0)
    {
        check = TRUE;
        errMsg = kAlertEnterDOB;
    }
//    else if([self.phoneNumberTextfield.text length]==0)
//    {
//        check = TRUE;
//        errMsg = kAlertEnterContactNO;
//    }
//    else if([[self getOriginalPhonestringFromFormated:self.phoneNumberTextfield.text] length]<10)
//    {
//        check = TRUE;
//        errMsg = kAlertValidPhoneNumber;
//    }
//    else if([[self getOriginalPhonestringFromFormated:self.phoneNumberTextfield.text] length]>10)
//    {
//        check = TRUE;
//        errMsg = kAlertValidPhoneNumber;
//    }
//    else if(!self.iAgreeButton.selected)
//    {
//        check = TRUE;
//        errMsg = kAlertTermsAndCondition;
//    }
        if (check)
    {
        [self showAlertViewWithTitle:nil message:errMsg];
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

#pragma mark-BUTTON ACTIONS
-(void)btnDOB:(UIButton*)sender{
    if(sender.tag == kTagDatePickerHidden)    {
        sender.tag = kTagDatePickerShow;
        
        [self.firstNameTextfield resignFirstResponder];
        [self.lastNameTextfield resignFirstResponder];
        [_emailTextfield resignFirstResponder];
        [_passwordTextfield resignFirstResponder];
        [self.phoneNumberTextfield resignFirstResponder];
        [self.loyaltyCardTextfield resignFirstResponder];
        
        [[self view]endEditing:YES];
        [self addDatePickerToView];
    }
    else if(sender.tag == kTagDatePickerShow)   {
        sender.tag = kTagDatePickerHidden;
        [self hideDatePickerview];
    }
}

-(void)NextButton:(id)sender{
    check = FALSE;
    NSString *errMsg = @"Error";
    if ([self.firstNameTextfield.text length]==0){
        check = TRUE;
        errMsg = kAlertEnterfirstName;
    }
    else if([self.lastNameTextfield.text length]==0){
        check = TRUE;
        errMsg = kAlertEnterLastName;
    }
    else if([self.emailTextfield.text length]==0){
        check = TRUE;
        errMsg = kAlertEnterEmail;
    }
    else if(![self validateEmail:self.emailTextfield.text]){
        check = TRUE;
        errMsg = kAlertValidEmail;
    }
    else if([self.passwordTextfield.text length]==0){
        check = TRUE;
        errMsg = kAlertPassword;
    }
    else if([self.passwordTextfield.text length]<4){
        check = TRUE;
        errMsg = kAlertMinPassword;
    }

    //kAlertPassword
    if (check){
        [self showAlertViewWithTitle:nil message:errMsg];
    }
    else{
        [self.CustomScrollView setScrollEnabled:YES];
        self.CustomScrollView.contentSize = CGSizeMake(kCustomScrollViewWidth, kCustomScrollViewHeight);
        self.CustomScrollView.contentOffset = CGPointMake(kCustomScrollViewWidth, 0);
    }
}


-(void)refreshButton:(UIButton*)button{
    self.firstNameTextfield.text=kEmptyString;
    self.lastNameTextfield.text=kEmptyString;
    
    _emailTextfield.text=kEmptyString;
    
    _passwordTextfield.text=kEmptyString;
    
    selectedDOBString=kEmptyString;
    
    self.loyaltyCardTextfield.text=kEmptyString;
    
    self.phoneNumberTextfield.text=kEmptyString;
    
    
}

//-(void)changeDateInLabel:(id)sender withTextfield:(UITextField*)textfield
//{
//    textfield=[UITextField new];
//    textfield=self.dateOfBirthTextfield;
//    NSDateFormatter *df = [[NSDateFormatter alloc] init];
//    NSLog(@"date is %@", self.datePicker.date);
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en"];
//    
//    [df setDateFormat:@"dd-MMM-yyyy"];
//    /* sender.text = [NSString stringWithFormat:@"%@",
//     [df stringFromDate:self.datePicker.date]];
//     */
//    self. DateOFBirthString =  [NSString stringWithFormat:@"%@",
//                                [df stringFromDate:self.datePicker.date]];
//    textfield.text=self. DateOFBirthString;
//    
//    if ([self.datePicker.date isEqual:[NSNull null]]) {
//        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"SelectedDate"];
//    }
//    else{
//        [[NSUserDefaults standardUserDefaults]setObject:self.datePicker.date forKey:@"SelectedDate"];
//    }
//    NSDate*selectedDate;
//    selectedDate = self.datePicker.date;
//    
//}

-(void)cameraButtonAction:(id)sender{
    [self addBarCodeScannerWithType:kBarcodeTypeSignUp];
}
- (void)TermsAndConditionButton:(UIButton*)sender {
    termsAndConditionsChecked = YES;
    [self performSegueWithIdentifier:kGoToTermsAndConditionViewControllerSegueId sender:nil];
}

- (void)IAgreeButton:(UIButton*)sender {
    if ([sender isSelected]) {
        self.iAgreeImageView.image=[UIImage imageNamed:kImageUnselectedBox ];
        [sender setSelected:NO];
    }
    else{
        self.iAgreeImageView.image=[UIImage imageNamed:kImageSelectedBox ];
        [sender setSelected:YES];
        
    }
}

-(void)signUpButton:(UIButton*)sender{
    //[self CallApiVerifyEmailId];
    [self CallApiSignUp];
   }

#pragma mark-textfield delegate method

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
   
    if([self.dropDownView isDescendantOfView:[self view]])
    {
        [self.dropDownView setHidden:YES];
    }

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    DLog(@"");
//    if ([segue.identifier isEqualToString:kGoToBarCodeScannerViewControllerSegueId]) {
//        //[segue destinationViewController]
//       // self.barcodeTypeString=kBarcodeTypeSignUp;
//        BarcodeScannerViewController *vc=[self viewControllerWithIdentifier:kBarcodeScannerViewControllerStoryboardId];
//        vc.barcodeTypeString=kBarcodeTypeSignUp;
//        
//        
//    }
    
}
#pragma mark-scrollview delegate methods
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (sender.contentOffset.y != 0) {
        CGPoint offset = sender.contentOffset;
        offset.y = 0;
        sender.contentOffset = offset;
    }
}

@end
