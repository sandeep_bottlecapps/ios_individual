//
//  LoyaltyCardViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 8/11/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "BaseViewController.h"

@interface LoyaltyCardViewController  :BaseViewController<UITextFieldDelegate,UIGestureRecognizerDelegate>
@property(nonatomic,retain)UILabel                *currentFSPointLabel;
@property(nonatomic,retain)UILabel                *currentFSCreditLabel;
@property(nonatomic,retain)UILabel                *pointsTillNextCreditLabel;
@property(nonatomic,retain)AsyncImageView            *barcodeScannerImageView;


//

@end
