//
//  NotificationHistoryViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 9/16/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "NotificationHistoryViewController.h"
#define krowHeightForNotification ((rect.size.height)+(kPaddingSmall*2)+(kLabelHeight*2))
@interface NotificationHistoryViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableDictionary *apiResponseDictionary;
    NSMutableArray      *  getNotificationArray;
    CGSize             labelHeight;
    int                 totalCount;
    int                 pagenumber;
    UILabel             *NorecordFoundLabel;
    NSInteger           totalNumberOfRecordFound;
}
@end

@implementation NotificationHistoryViewController
@synthesize  getNotificationArray;

-(void)CreateUI{
        if ([getNotificationArray count]==0) {
        [self addNorecordFoundLAbelOnTableView:self.defaultTableView];
    }
    else{
        [NorecordFoundLabel removeFromSuperview];
    }
    [self.defaultTableView reloadData];
}

-(void)addNorecordFoundLAbelOnTableView:(UITableView*)tableView{
    NorecordFoundLabel = [[UILabel alloc] init];
    [NorecordFoundLabel setBackgroundColor:[UIColor clearColor]];
    [NorecordFoundLabel setTextColor:[self getColorForBlackText]];
    [NorecordFoundLabel setText:kAlertNoRecordFound];
    [NorecordFoundLabel sizeToFit];
    NorecordFoundLabel.frame = CGRectMake((tableView.bounds.size.width - NorecordFoundLabel.bounds.size.width) / 2.0f,
                                          50,
                                          NorecordFoundLabel.bounds.size.width,
                                          NorecordFoundLabel.bounds.size.height);
    [tableView insertSubview:NorecordFoundLabel atIndex:0];
}

-(void)callApiUserNotificationHistory:(NSString*)page{
   User *userObj=kGetUserModel;
    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
    [paramDict setObject:userObj.userId forKey: kParamUserId];
    [paramDict setObject:page forKey: kParamPageNumber];
    [paramDict setObject:kPageSize forKey: kParamPageSize];
    
    [self showSpinner];
    [[ModelManager modelManager]GetUserNotificationHistory:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            @try {
                [self hideSpinner];
                [getNotificationArray addObjectsFromArray:[response valueForKey:@"list"]]  ;
                DLog(@"getNotificationArray--%@ \n count====%d",getNotificationArray,[getNotificationArray count]);

                totalNumberOfRecordFound    =[[response valueForKey:kapiTotalNoOfRecords] intValue];
                [self CreateUI];
            }
            @catch (NSException *exception) {
                [self showAlertViewWithTitle:kError message:kInternalInconsistencyErrorMessage];
            }
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}

-(void)viewWillAppear:(BOOL)animated    {
    getNotificationArray = [NSMutableArray array];
    pagenumber = 1;
    totalNumberOfRecordFound = 0;
    [self callApiUserNotificationHistory:@"1"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self AddDefaultTableView];
    [self.defaultTableView setDelegate:self];
    [self.defaultTableView setDataSource:self];
    CGRect ContentainerViewFrame;
    ContentainerViewFrame.origin.x=0;
    ContentainerViewFrame.origin.y=SCREEN_HEIGHT-kBottomViewHeight-kCustomButtonContainerHeight-kPaddingSmall;
    ContentainerViewFrame.size.width=SCREEN_WIDTH;
    ContentainerViewFrame.size.height=kCustomButtonContainerHeight;
    UIButton *manageNotificationButton;
    manageNotificationButton=  [self AddCustomButtonInsideContentViewWithFrame:ContentainerViewFrame];
    [manageNotificationButton setTitle:kButtonTitleMANAGENOTIFICATIONS forState:UIControlStateNormal];
    [manageNotificationButton addTarget:self action:@selector(manageNotificationButton:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-BUTTON ACTION
//FavButton
//FavButton
-(void)manageNotificationButton:(UIButton*)button{
    
    [self performSegueWithIdentifier:kGoToNotificationsViewControllerSegueId sender:nil];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton*)button {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of ections.
    return 1;
}
-(CGSize)heigtForCellwithString:(NSString *)stringValue withFont:(UIFont*)font{
    CGSize constraint = CGSizeMake(300,9999); // Replace 300 with your label width
    NSDictionary *attributes = @{NSFontAttributeName: font};
    CGRect rect = [stringValue boundingRectWithSize:constraint
                                            options:         (NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                         attributes:attributes
                                            context:nil];
    return rect.size;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==[getNotificationArray count]) {
        return kButtonHeight;
        
    }
    else{
        CGRect rect=[self setDynamicViewsFrameFor:[[  getNotificationArray objectAtIndex:indexPath.row] valueForKey:@"Notification"]];

        return krowHeightForNotification;

    
    
    }
    //    labelHeight = [self heigtForCellwithString:[[  getNotificationArray objectAtIndex:indexPath.row] valueForKey:@"Notification"]    withFont:[self addFontRegular]];
    //    return labelHeight.height;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([getNotificationArray count]<totalNumberOfRecordFound && [getNotificationArray count]!=0) {
        return [getNotificationArray count]+1;
    }
    
    else{
        return [getNotificationArray count];
    }

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    //static NSString *postCellId = @"postCell";
    static NSString *moreCellId = @"moreCell";
    NSUInteger row = [indexPath row];
    NSUInteger count = [getNotificationArray count];
    UITableViewCell *cell =nil;
    if (row == count && count != 0) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:moreCellId];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:moreCellId];
            cell.textLabel.text = kTapToloadMore;
            cell.textLabel.textColor = [self getColorForRedText];
            cell.textLabel.font = [self addFontRegular];
            
        }
        return cell;
    }
    
    else{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    
    /*************************************************Add Cart and Like*****************************************************/
    //    CGSize textSize = [[[  getNotificationArray objectAtIndex:indexPath.row] valueForKey:@"Notification"] sizeWithFont:[self addFontRegular] constrainedToSize:CGSizeMake(240, 20000) lineBreakMode: NSLineBreakByWordWrapping]; //Assuming your width is 240
    //    float heightToAdd = MIN(textSize.width, 60.0f);
    //    DLog(@"heightToAdd==%f",heightToAdd);
    
    NSString *string1 = [[  getNotificationArray objectAtIndex:indexPath.row] valueForKey:@"Notification"];
    CGSize constrainedSize = CGSizeMake(SCREEN_WIDTH - 10,9999);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:@"OpenSans" size:10.0f], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:string1 attributes:attributesDictionary];
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];

    CGRect rect=[self setDynamicViewsFrameFor:[[  getNotificationArray objectAtIndex:indexPath.row] valueForKey:@"Notification"]];
    
    UILabel *notificationLabel=[self createLblWithRect:CGRectMake(kPaddingSmall, kPaddingSmall, SCREEN_WIDTH - kPaddingSmall*2, requiredHeight.size.height + 30) font:[self addFontRegular] text:[[  getNotificationArray objectAtIndex:indexPath.row] valueForKey:@"Notification"] textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
    notificationLabel.numberOfLines=0;
    [notificationLabel setBackgroundColor:[UIColor clearColor]];
    
    UILabel *DateLabel=[self createLblWithRect:CGRectMake(kPaddingSmall, (notificationLabel.frame.size.height+notificationLabel.frame.origin.y - 5), SCREEN_WIDTH-(kPaddingSmall*2), kLabelHeight - 3) font:[self addFontRegular] text:[[  getNotificationArray objectAtIndex:indexPath.row] valueForKey:@"Date"] textAlignment:NSTextAlignmentRight textColor:[self getColorForBlackText]];
    DateLabel.backgroundColor = [UIColor clearColor];
    
    if (cell == nil)    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:kDefaultTableViewCell];
        cell .selectionStyle=UITableViewCellSelectionStyleNone ;
        
        [cell addSubview:notificationLabel];
        [cell addSubview:DateLabel];
        
        UIImageView  *separatorEnd = [[UIImageView alloc] initWithFrame:CGRectMake(0, krowHeightForNotification-1, SCREEN_WIDTH, 1)];
        [separatorEnd setBackgroundColor:[self getColorForLightGreyColor]];
        [cell.contentView addSubview:separatorEnd];
    }
    return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSUInteger row = [indexPath row];
    NSUInteger count = [getNotificationArray count];
    if (row == count) {
        pagenumber++;
        if (count<totalNumberOfRecordFound) {
            [self callApiUserNotificationHistory:[NSString stringWithFormat:@"%d",pagenumber]];
        }
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end