//
//  BarcodeScannerViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 9/15/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "BarcodeScannerViewController.h"

@interface BarcodeScannerViewController ()

@end

@implementation BarcodeScannerViewController
@synthesize barcodeTypeString;
-(void)receivebarcodeNotification:(NSNotification*)notification{
    
       [self GoBack];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if ([self.barcodeTypeString isEqualToString:kBarcodeTypeSignUp]) {

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receivebarcodeNotification:)
                                                     name:@"barcodeNotification"
                                                   object:nil];

    }
    }

- (void)viewDidLoad {
    [super viewDidLoad];
    DLog(@"%@",self.barcodeTypeString);
    // Do any additional setup after loading the view.
    [self addBarCodeScannerWithType:barcodeTypeString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
