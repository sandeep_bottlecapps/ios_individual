//
//  OrderConfirmationViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 8/27/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CustomerAddressType.h"

@interface OrderConfirmationViewController :BaseViewController


@property (nonatomic,retain)UILabel *nameLabel;
@property(nonatomic,retain)NSMutableArray *cellItemsArray;
@property (nonatomic,retain)UILabel *productNameLabel;
@property (nonatomic,retain)UILabel *productQuantityLabel;
@property (nonatomic,retain)UILabel *productPriceLabel;
@property (nonatomic,retain)UILabel *productNoteLabel;
@property (nonatomic,retain)UILabel *ItemCostLabel;
@property (nonatomic,retain)UILabel *TaxLabel;
@property (nonatomic,retain)UILabel *OrderTotalLabel;
@property (nonatomic,retain)UILabel *StreetAddressLabel;
@property (nonatomic,retain)UILabel *CityLabel;
@property (nonatomic,retain)UILabel *statusNameLabel;
@property (nonatomic,retain)UILabel *confirmationNumberLabel;
@property (nonatomic,retain)UIButton *returnToHomeButton;

@property(nonatomic,strong)NSString *totalAmount;
@property(nonatomic,strong)NSString *OrderNo;
@property(nonatomic,strong)NSString *OrderDate;
@property(nonatomic,strong)NSString *TrasactionId;
@property(nonatomic,strong)NSString *senderName;
@property(nonatomic,strong)NSMutableArray *getCartArray;
@property(nonatomic,strong)NSString *getConfirmationNo;

@property(nonatomic,strong)NSDictionary *getAddorderrespnseDict;
@property (nonatomic,retain)NSString       *OrderTypeString;
@property (nonatomic,retain)NSString           *payTypeString;
@property (nonatomic,retain)NSString       *paymentTypeString;
@property (nonatomic,strong) CustomerAddressType *deliveryAddressDetails;
@property (nonatomic,strong) NameAndAddressType *uName;
//getCartArray

@end
