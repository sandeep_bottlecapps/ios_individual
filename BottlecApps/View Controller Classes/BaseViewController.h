//
//  BaseViewController.h
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/7/14.
//  Copyright (c) 2014 Kuldeep Tyagi. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <Foundation/Foundation.h>
#import "SlideNavigationController.h"
#import "UIView+AlertCompatibility.h"
#import "RTSpinKitView.h"
#import "ModelManager.h"
#import "CompletionBlocks.h"
#import "PageControl.h"
#import "ConstantsAndMacros.h"
#import "AsyncImageView.h"
#import "EDStarRating.h"
#import "MTBBarcodeScanner.h"
#import "ShoppingCart.h"
#import "category.h"
#import "SubCategory.h"
#import "AddOptionEvent.h"
#import "UIActionButton.h"
#import "SAMTextView.h"

#pragma mark-Login credentials
#define EVENTSELECTED                 @"0"
#define keventImageWidthLarge 200
#define keventImageHeightLarge 200


#define keventImageWidthThumnail 100
#define keventImageHeightThumnail 100

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH >= 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define HALF_SCREEN_HEIGHT (SCREEN_HEIGHT/2)
#define HALF_SCREEN_WIDTH (SCREEN_WIDTH/2)

/////////////////////////////////////////////////////////////////////This is the check for Bottlecapps and Argonaut App ////////////////////////////////////////////////////////////////////////////////////////////////
/*
 
 Make note for storeid in staging and live url
 staging url
 For argonaut app - 10001
 For bottolcapps app- 10066
 
 live url
 For argonaut app - 10001
 For bottolcapps app - 10002
*/

/*
 Apps and their strings
 Argonaut ------- 0
 Bottlecapps------1
 Renaissance------2
 Philippe Liquor--3
 Specs------------4
 
 Marlboro---------5
 Acton -----------6
 Loading Dock Liquors ---- 7
 Atlantic Liquors ------- 8
 
 Bottlecapps Lite ---- 9
 Pleasure Bay -------- 10
 The wild duck -------- 11
 */

#define kLiveApp 0//check if it is live server or staging server 0 for stagging 1 for live
#define kStoreBottleCapps 1 //check if it is Argonaut or bottlecapps  0 for argonaut 1 for bottlecapps


#if kStoreBottleCapps == 1
#define kSTOREIDbottlecappsString  kLiveApp ?@"10002": @"10066"
#define kHeaderTitle @"BOTTLECAPPS"
#define kImageHeaderLogo @""
#define kImageLogo @"Logo"
#define kTopPadding2 (IS_IPHONE_4_OR_LESS?10:IS_IPHONE_6?30:25)
#define kLoyaltyCardPlaceHolder 0
#define KproductNameYAxis 3
#define kLoyaltyCardTitle @"BOTTLECAPPS"
#define kGaps 10
#define kStoreImage 1


#endif

/////////////////////////////////////////////////////////////////////This is the check for Bottlecapps and Argonaut App ////////////////////////////////////////////////////////////////////////////////////////////////

#pragma mark- login credential code



static NSString * const     kloginuser                  =     @"";
static NSString * const     kloginpassword              =     @"";
#pragma mark-STOREID


//static NSString *  const kSTOREIDbottlecappsString      =@"10066";
static NSString *  const kDEVICETOKENString             =@"00000000-54b3-e7c7-0000-000046bffd97"; // It must not be changed due to dependancy on server. Please ask to server guy if you want to change it.
static NSString *  const kDeviceType                    =@"I";
//static NSString *  const ktestUserid                  =@"72712";
//static NSString *  const ktestUserid2                 =@"256";
//static NSString *  const ktestUserid3                 =@"252";
//static NSString *  const ktestUserid4                 =@"72895";



//@"72895"
//@"00000000-54b3-e7c7-0000-000046bffd97"

//#52565a
#pragma mark-HEXCODE STRING
static NSString *  const kgrayHASHCODEstring        =@"#52565a";
static NSString *  const kredHASHCODEstring         =@"#eb1c2e";
static NSString *  const kyellowHASHCODEstring      =@"#ffba01";

//#ffba01
#pragma mark-NAVIGATION-TITLE
static NSString *  const kNavigationTitleSignUp                 =@"SIGN UP";
static NSString *  const kNavigationTitleFINDAPRODUCT           =@"FIND A PRODUCT";
static NSString *  const kNavigationTitlePROFILE                =@"PROFILE";
static NSString *  const kNavigationTitleEDITPROFILE            =@"EDIT PROFILE";
static NSString *  const kNavigationTitleEVENTS                 =@"EVENTS";

static NSString *  const kNavigationTitleEVENTDETAILS           =@"EVENT DETAILS";
static NSString *  const kNavigationTitleCART                   =@"CART";
static NSString *  const kNavigationTitleORDERCONFIRMATION      =@"ORDER CONFIRMATION";
static NSString *  const kNavigationTitleCONTACTUS              =@"CONTACT US";
static NSString *  const kNavigationTitleDEALS                  =@"DEALS";
static NSString *  const kNavigationTitleORDERDETAILS           =@"ORDER DETAILS";
static NSString *  const kNavigationTitleLOYALTYCARD            =@"LOYALTY CARD";
static NSString *  const kNavigationTitleMANAGENOTIFICATIONS    =@"MANAGE NOTIFICATIONS";
static NSString *  const kNavigationTitleNOTIFICATIONS          =@"NOTIFICATIONS";

static NSString *  const kNavigationTitleVIEWORDERS             =@"VIEW ORDERS";
static NSString *  const kNavigationTitlePRODUCTDETAILS         =@"PRODUCT DETAILS";
static NSString *  const kNavigationTitleFINDAPRODUCTLiquor     =@"FIND LIQUOR";
static NSString *  const kNavigationTitleFINDAPRODUCTWine       =@"FIND WINE";
static NSString *  const kNavigationTitleFINDAPRODUCTBeer       =@"FIND BEER";

static NSString *  const kNavigationTitleTERMSANDCONDITIONS     =@"TERMS AND CONDITIONS";
static NSString *  const kNavigationTitleREVIEWS                =@"REVIEWS";
static NSString *  const kNavigationTitleBARCODESCANNER         =@"BARCODE SCANNER";
static NSString *  const kNavigationTitleFAVORITES              =@"FAVORITES";

#pragma mark-SIGNUP PARAM
static NSString *  const kSignupParamEmailId            =@"EmailId";
static NSString *  const kSignupParamPassword           =@"Password";
static NSString *  const kSignupParamFirstName          =@"FirstName";
static NSString *  const kSignupParamLastName           =@"LastName";
static NSString *  const kSignupParamContactNumber      =@"ContactNumber";
static NSString *  const kSignupParamdateofBirth        =@"dateofBirth";
static NSString *  const kSignupParamGender             =@"Gender";
static NSString *  const kSignupParamAddress            =@"Address";
static NSString *  const kSignupParamApartment          =@"Address2";
static NSString *  const kSignupParamZipCode            =@"ZipCode";
static NSString *  const kSignupParamCountryId          =@"CountryId";
static NSString *  const kSignupParamStateName          =@"StateName";
static NSString *  const kSignupParamCity               =@"City";
static NSString *  const kSignupParamUserLoyalityCardNo =@"UserLoyalityCardNo";
static NSString *  const kSignupParamUserImage          =@"UserImage";
static NSString *  const kSignupParamstoreid            =@"storeid";
static NSString *  const kSignupParamstoreid2            =@"StoreId";

static NSString *  const kSignupParamDeviceId           =@"DeviceId";
static NSString *  const kSignupParamRegionId           =@"RegionId";
static NSString *  const kSignupParamDeviceType         =@"DeviceType";
static NSString *  const kEditProfile_IsProfileUpdated   =@"IsProfileUpdated";


#define kTagDatePickerShow                              1112
#define kTagDatePickerHidden                            1113

//kBottomPAdding
#pragma mark- TextfieldTag
static int  const kTextfieldTagGreyBorder               =99;
static int  const kTextfieldTagWhiteBorder              =999;
static int  const kTextfieldTagLightGreyBorder          =1000;


static int  const kTextfieldTagSigninUserEmail        =1001;
static int  const kTextfieldTagSigninPassword         =1002;
static int  const kTextfieldTagSignupFirstName        =1003;
static int  const kTextfieldTagSignupLastName         =1004;
static int  const kTextfieldTagSignupEmail            =1005;
static int  const kTextfieldTagSignupPassword         =1006;
static int  const kTextfieldTagSignupDOB              =1007;
static int  const kTextfieldTagSignupPhone            =1008;
static int  const kTextfieldTagSignupLoyaltyCard      =1009;
static int  const kTextfieldTagEditProfileStreetName  =1010;
static int  const kTextfieldTagEditProfileStateName   =1011;
static int  const kTextfieldTagEditProfilePhone       =1012;
static int  const kTextfieldTagEditProfileEmail       =1013;
static int  const kTextfieldTagReview                 =1014;
static int  const kTextfieldTagFirstName              =1015;
static int  const kTextfieldTagLastName               =1016;
static int  const kTextfieldTagAddress                =1017;
static int  const kTextfieldTagCity                   =1018;
static int  const kTextfieldTagState                  =1019;
static int  const kTextfieldTagZip                    =1020;
static int  const kTextfieldTagPhone                  =1021;
static int  const kTextfieldTagCardNo                 =1023;
static int  const kTextfieldTadMonth                  =1024;
static int  const kTextfieldTagCvv                    =1025;
static int  const kTextfieldTagZipCode                =1026;
//productQuantityLabel
static int  const kTagLikeButton                      =3001;

static int  const kTagNorecordFoundLabel              =2010;
static int  const kTagproductQuantityLabel            =2000;
static int  const kTagproductQuantityView             =2001;
static int  const kTagCartAndLikeView                 =2002;
static int  const kTagsearchtextfield                 =2003;

static int  const kTagProductImageV                   =2004;
static int  const kTagProductName                     =2005;
static int  const kTagProductSize                     =2006;
static int  const kTagProductPrice                    =2007;
static int  const kTagProductDetails                  =2123;
static int  const kTagCartButton                      =2008;
static int  const kTagFavButton                       =8009;


static int  const kTagReviewTitle                     =2008;
static int  const kTagReviewDateLabel                 =2009;
static int  const kTagReviewDescTextView              =2010;
static int  const kTagReviewStarRating                =2011;
static int  const kTagProductimgSize                  =2012;

//static int  const kTagReviewEditButton                  =2011;
//[[self ProperValue:[[ getReviewArray objectAtIndex:indexPath.row] valueForKey:@"ReviewRating"]] intValue]
//CartAndLikeView
static int  const kTextViewTagTastingNotes     =4015;

#pragma mark-placeholdes

static NSString*  const kTextfieldPlaceHolder      =  @"Review Title";

//@"Review"
#pragma mark- Buttn Tag
static int  const kButtonTagSignUpAddAphotoButton       =2001;


#pragma mark- validation Title and messages
static NSString * const validationAlertTitle        =  @"Validation Alert";
static NSString * const kLogoutSuccessTitle         =  @"Logout Success!";
static NSString * const kSignInSuccessTitle         =  @"Sign In Success!";
static NSString * const kSuccessTitle               =  @"Success!";
static NSString * const kError                      =  @"Error!";
static NSString * const kWarning                    =  @"Warning!";
static NSString * const kRequestFailTitle           =  @"Request Fail!";

static NSString * const kUnknownErrorMessage        =  @"Some unknown error occurred!\nPlease try again...";
static NSString * const kNumbersOnly        =  @"Numbers only";

//badge
static NSString * const kImageDownloadFailMessage   =  @"Can not download image...";
//@"EVENTS & PROMOTIONS teaser copy about an upcoming event here"

#pragma mark- FONT NAME Constents
static NSString * const kFontRegular                =   @"OpenSans";
static NSString * const kFontSemiBold               =   @"OpenSans-Semibold";
static NSString * const kFontSemiBoldItalic         =   @"OpenSans-SemiBoldIt";
static NSString * const kFontBold                   =   @"OpenSans-Bold";
static NSString * const kFontBoldExtra              =   @"OpenSans-Extrabold";
//[UIFont fontWithName:@"ProximaNova-Regular" size:20];
#pragma mark-tempimagesNames


//#pragma mark-imagesNames

#pragma mark-imagesNames
//up_arrow
static NSString * const kImageDownArrow                             =   @"down_arrow";
static NSString * const kImageDefaultUpcomingImage                  =   @"DefaultUpcomingImage";

//kImageDefaultUpcomingImage
static NSString * const kImageUpArrow                               =   @"up_arrow";

static NSString * const kImageHeartIconSelected                     =   @"heart_icon_selected";
static NSString * const kImageSaveAddress                           =   @"save_address";
static NSString * const kImageCrossSaveAddress                      =   @"cross_save_address";

static NSString * const kImageIcon                                  =   @"Icon.png";
static NSString * const kImagebadge                                 =   @"badge";

static NSString * const kImageBackground                            =   @"Background";
//static NSString * const kImageBelowHeaderWhiteStrip                 =   @"BelowHeaderWhiteStrip";
static NSString * const kImageButtonStrip                           =   @"ButtonStrip";
static NSString * const kImageCardNumberStrip                       =   @"CardNumberStrip";
static NSString * const kImageDealsIcon                             =   @"DealsIcon";
static NSString * const kImageEditButton                            =   @"EditButton";
static NSString * const kImagefooter                                =   @"footer";
static NSString * const kImageLoginFooter                           =   @"LoginFooter";
static NSString * const kImageForgotYourPasswordText                =   @"ForgotYourPasswordText";
static NSString * const kImageHeader                                =   @"Header";



//static NSString * const kImageHeaderLogo                            =   @"HeaderLogo";
static NSString * const kImageKartIcon                              =   @"KartIcon";
//static NSString * const kImageLogo                                  =   @"Logo";
static NSString * const kImageLogoutButton                          =   @"LogoutButton";
static NSString * const kImageMarqueImageBox                        =   @"MarqueImageBox";
static NSString * const kImageMarqueImageBoxShadow                  =   @"MarqueImageBoxShadow";
static NSString * const kImageMenuBase                              =   @"MenuBase";
static NSString * const kImageMenuIcon                              =   @"MenuIcon";



static NSString * const kImageMenuLogo                            =   @"MenuLogo";
static NSString * const kImageMenuLogoForSlider                     =   @"MenuLogoForSlider";
static NSString * const kImageNotificationsIcon                     =   @"NotificationsIcon";
static NSString * const kImageOrderIcon                             =   @"OrderIcon";
static NSString * const kImagePasswordIcon                          =   @"PasswordIcon";
static NSString * const kImageProfileIcon                           =   @"ProfileIcon";
static NSString * const kImageProfileImageBackground                =   @"ProfileImageBackground";
static NSString * const kImageProfileImageCircle                    =   @"ProfileImageCircle";
static NSString * const kImageSearchIcon                            =   @"SearchIcon";
static NSString * const kImageSelectedBox                           =   @"SelectedBox";
static NSString * const kImageSelectedMarquePoint                   =   @"SelectedMarquePoint";
static NSString * const kImageSignInButton                          =   @"SignInButton";
static NSString * const kImageTermsAndConditionText                 =   @"TermsAndConditionText";
static NSString * const kImageTextbox                               =   @"Textbox";
static NSString * const kImageUnselectedBox                         =   @"UnselectedBox";
static NSString * const kImageUnselectedMarquePoint                 =   @"UnselectedMarquePoint";
static NSString * const kImageUsernameIcon                          =   @"UsernameIcon";
static NSString * const kImageVerticalShadow                        =   @"VerticalShadow";
static NSString * const kImageViewMoreButton                        =   @"ViewMoreButton";
//////imagenot available
static NSString * const kImageDateofBirthIcon                       =   @"DoteofBirthIcon";
static NSString * const kImagePhonenumberIcon                       =   @"PhonenumberIcon";
static NSString * const kImageLoyaltyCardIcon                       =   @"LoyaltyCardIcon";

//monika 16 Nov

static NSString * const kImageReviewBig                             =   @"review_big";
static NSString * const kImageReviewSmall                           =   @"review_small";

static NSString * const kImageaddmoreitembutton                     =@"add-more-item-button";
static NSString * const kImagecarticongrey=@"cart-icon-grey";
static NSString * const kImagecarticonediticon=@"cart-iconedit-icon";
static NSString * const kImagegreystar=@"grey-star";
static NSString * const kImagewriteareviewbutton                =@"write-a-review-button";
static NSString * const kImagecallusbutton                      =@"call-us-button";
static NSString * const kImageEmailUsbutton                     =@"Email-Us-button";
static NSString * const kImageneworderbutton                    =@"new-order-button";
static NSString * const kImageplusicon=@"plus-icon";
static NSString * const kImagedescriptiontextbox=@"description-textbox";
static NSString * const kImageredstar=@"red-star";
static NSString * const kImageredstrip=@"red-strip";
static NSString * const kImagereviewtitletextbox=@"review-title-textbox";
static NSString * const kImagesubmitbutton                =@"submit-button";
static NSString * const kImagewhitestar=@"white-star";
///image provided on  26 aug
static NSString * const kImageAddtocartButton=@"add-to-cart-button";

static NSString * const kImageBeerSelected=@"beer-selected";

static NSString * const kImageBeerUnselected=@"beer-unselected";

static NSString * const kImageBigTextbox=@"big-textbox";

static NSString * const kImageCartIconfadeout=@"cart-icon-fade-out";

static NSString * const kImageCartIconGrey=@"cart-icon-grey";

static NSString * const kImageCartIconwhite=@"cart-icon-white";

static NSString * const kImageContinueshoppingButton=@"continue-shopping-button";

static NSString * const kImageCrossbox=@"cross-box";
static NSString * const kImageCrossForLocation=@"close.png";

static NSString * const kImageCrossButton=@"cross-button";
static NSString * const kImageCrossButtonSmall=@"cross-button-small";
//close.png
static NSString * const kImageCrossIcon=@"cross-icon";

static NSString * const kImageDeliver=@"deliver";

static NSString * const kImageEditIcon=@"edit-icon";

static NSString * const kImageEventsSelected=@"events-selected";

static NSString * const kImageEventsUnselected=@"events-unselected";

static NSString * const kImageFilterButton=@"filter-button";

static NSString * const kImageGoButton=@"go-button";
static NSString * const kImageAuthorize=@"authorize_logo.png";
static NSString * const kImageCouponCodeApply=@"Apply-Button-MDPI.png";
//
static NSString * const kImageHeartIconselected=@"heart-icon-selected";

static NSString * const kImageHeartIconUnselected=@"heart-icon-unselected";

static NSString * const kImageLiquorSelected=@"liquor-selected";

static NSString * const kImageLiquorUnselected=@"liquor-unselected";

static NSString * const kImageLoyaltycardImage=@"loyalty-card-image";

static NSString * const kImageMinusIcon=@"minus-icon";

static NSString * const kImageNeworderButton=@"new-order-button";

static NSString * const kImagePayatstoreButton=@"pay-at-store-button";

static NSString * const kImagePaynowButton=@"pay-now-button";

static NSString * const kImagePickupButton=@"pick-up-button";

static NSString * const kImageRepeatorderButton=@"repeat-order-button";

static NSString * const kImageReturntohomeButton=@"return-to-home-button";

static NSString * const kImageReviewsButton=@"reviews-button";
static NSString * const kImageReviewsPlainButton=@"reviews-button-plain";

static NSString * const kImageSaveButton=@"save-button";

static NSString * const kImageSearchbox=@"search-box";

static NSString * const kImageShadowverticaldivider=@"shadow-vertical-divider";

static NSString * const kImageSkipButton=@"skip-button";

static NSString * const kImageSmallTextbox=@"small-textbox";

static NSString * const kImageStrip=@"strip";

static NSString * const kImageWineSelected=@"wine-selected";

static NSString * const kImageWineUnselected=@"wine-unselected";

static NSString * const kImageWriteareviewButton=@"write-a-review-button";

static NSString * const kImageHeartIconfadeout=@"heart-icon-fade-out";
static NSString * const kImageHeartIconGrey=@"heart-icon-grey";
static NSString * const kImageHeartIconwhite=@"heart-icon-white";
static NSString * const kImageCouponCode=@"out-of-stock";
static NSString * const kImagePlusIcon=@"plus-icon";

static NSString *const kImageUpIcon = @"up-arrow";

//kImageNewOrderButton

//signupscreen

//////////////added on 18 aug/////////
static NSString * const kImageDropdown                              =@"dropdown.png";

static NSString * const kImage_PlaceholderProfile                   =@"profileIconPlaceHolder";

static NSString * const kImage_addaphotobutton                      =@"addaphotobutton";

static NSString * const kImage_BarcodeScanner                       =@"BarcodeScanner";

static NSString * const kImage_beer                                 =@"beer";

static NSString * const kImage_bigtextstrip                         =@"bigtextstrip";

static NSString * const kImage_bottombackbutton                     =@"bottombackbutton";

static NSString * const kImage_Bottomlogo                           =@"Bottomlogo";

static NSString * const kImage_cameraicon                           =@"cameraicon";

static NSString * const kImage_dobicon                              =@"dobicon";

static NSString * const kImage_editwihtetextbox                     =@"editwihtetextbox";

static NSString * const kImage_emailicon                            =@"emailicon";

static NSString * const kImage_liquor                               =@"liquor";

static NSString * const kImage_locationicon                         =@"locationicon";

static NSString * const kImage_loyaltycardicon                      =@"loyaltycardicon";

static NSString * const kImage_nextbutton                           =@"nextbutton";

static NSString * const kImage_notificationsbatch                   =@"notificationsbatch";

static NSString * const kImage_phoneicon                            =@"phoneicon";

static NSString * const kImage_refreshicon                          =@"refreshicon";

static NSString * const kImage_savebutton                           =@"savebutton";

static NSString * const kImage_signupbutton                         =@"signupbutton";

static NSString * const kImage_textbox                              =@"textbox";

static NSString * const kImage_wine                                 =@"wine";


/*
 
 */
#pragma mark-BUTTON
static NSString * const kButtonTitlePayNow                      =   @"PAY NOW";

static NSString * const kButtonTitleRememberMe                      =   @"Remember me";
static NSString * const kButtonForgotYouPsssword                    =   @"Forgot your password?";

static NSString * const kButtonTitleNext                            =   @"NEXT";
static NSString * const kButtonTitleSignUp                          =  @"SIGN UP";
static NSString * const kButtonTitleSignIn                          =  @"SIGN IN";

static NSString * const kButtonTitleBEER                            =  @"BEER";
static NSString * const kButtonTitleWINE                            =  @"WINE";
static NSString * const kButtonTitleLIQUOR                          =  @"LIQUOR";
static NSString * const kButtonTitleEVENTS                          =  @"EVENTS";

static NSString * const kButtonTitleAddAPhoto                       =   @"Add a Photo";
static NSString * const kButtonTitleUploadedPhoto                   =   @"uploaded photo is here";
static NSString * const kButtonLogoutTittle                         =   @"Log out";
static NSString * const kButtonLogoutTittleINCAPS                   =   @"LOG OUT";
static NSString * const kButtonSIGNINTitleINCAPS                    =   @"SIGN IN";
static NSString * const kButtonTittleNEWORDER                       =   @"NEW ORDER";
static NSString * const kButtonTittleEDIT                           =   @"EDIT";
static NSString * const kButtonTittleADDTOCART                           =   @"ADD TO CART";
static NSString * const kButtonTittleREVIEW                         =   @"REVIEWS";
static NSString * const kButtonTittleWRITEAREVIEW                   =   @"WRITE A REVIEW";
static NSString * const kButtonTitleSUBMIT                          =   @"SUBMIT";
static NSString * const kButtonTitleADDMOREITEMS                    =   @"ADD MORE ITEMS";
static NSString * const kButtonTitleMANAGENOTIFICATIONS             =   @"MANAGE NOTIFICATIONS";
static NSString * const kButtonTitleCURRENTLOCATION                       =   @"CURRENT LOCATION";

//ReviewButton
static NSString * const kButtonTittleEMAILUS                        =   @"EMAIL US";
static NSString * const kButtonTittleCALLUS                         =   @"CALL US";
static NSString * const kButtonTittleREPEATORDER                    =   @"REPEAT ORDER";
static NSString * const kButtonTittleSKIP                           =   @"SKIP";
static NSString * const kButtonTittleSAVE                           =   @"SAVE";

static NSString * const kButtonTittleCONTINUESHOPPING               =   @"CONTINUE SHOPPING";
static NSString * const kButtonTittleDELIVER                        =   @"DELIVER";
static NSString * const kButtonTittlePICKUP                         =   @"PICK UP";
static NSString * const kButtonTitleCouponCode                      =   @"APPLY";
static NSString * const kButtonTitleAddress                         =   @"CHECK DELIVERY";
static NSString * const kButtonTittleDONE                           =   @"DONE";
static NSString * const kButtonTittlePAYNOW                         =   @"PAY NOW";
static NSString * const kButtonTittlePAYATSTORE                     =   @"PAY AT STORE";

static NSString * const kButtonTittleSUBMIT                         =   @"SUBMIT";


//static NSString * const kButtonForgotPasswordTittle                 =   @"Forgot your password ?";
static NSString * const kButtonSignUpWithExclamation                =   @"Sign Up!";
static NSString * const kButtonEDIT                                 =   @"EDIT";
static NSString * const kButtonSAVE                                 =   @"SAVE";
static NSString * const kButtonTitleClose                          =  @"CLOSE";
static NSString * const kButtonTitleFilter                          =  @"SEARCH";
static NSString * const kButtonTapToDetail                          =  @"tap for details";

static NSString *const kButtonTitleCancel                           = @"Cancel";
static NSString *const kButtonTitleAccept                           = @"Accept";

//@"tap to detail"
//@"EDIT"
#pragma mark-textfield
static NSString * const kTextfieldPlaceholderusername               =   @"Username";
static NSString * const kTextfieldPlaceholderpassword               =   @"Password";
static NSString * const kTextfieldPlaceholderFirstName              =   @"First name*";
static NSString * const kTextfieldPlaceholderLastName               =   @"Last name*";
static NSString * const kTextfieldPlaceholderEmail                  =   @"Email*";
static NSString * const kTextfieldPlaceholderemail                  =   @"Email";

static NSString * const kTextfieldPlaceholderEmailForgotpassword    =   @"Email";

static NSString * const kTextfieldPlaceholderPasswordWithStar       =   @"Password*";
static NSString * const kTextfieldPlaceholderDOB                    =   @"Date of Birth*";
static NSString * const kTextfieldPlaceholderPhoneNumber            =   @"Phone number*";
static NSString * const kTextfieldPlaceholderLoyaltyCard            =   @"Loyalty card";
//REMEMBERME
#pragma MARK-USERDEFAULTTEXT
static NSString * const kUserdefaultRememberme                      =   @"REMEMBERME";
//PROFILE
#pragma mark-PLaceholder
static NSString * const kPlaceHolderSearch                        =   @"Search";
static NSString * const kPlaceHolderEnterCard                     =   @"Enter card no. or use camera";
static NSString * const kPlaceHolderLoyaltyCard                   =   @"Loyalty Card";
//monika 16 Nov
static NSString * const kPlaceHolderReviewDescription             =   @"Review Description";
static NSString * const kPlaceHolderReviewTitle                   =   @"Review Title";


//@"Enter card no or use camera"
#pragma mark-label text
static NSString * const klabelProductDetails                        =   @"Product Details:";

static NSString * const kLabelNowSignUpAccount                      =   @"Sign up!";

static NSString * const kLabelProfile                               =   @"PROFILE";
static NSString * const kLabelPoweredBy                             =   @"Powered by";
static NSString * const kLabelItemsCost                             =   @"Items Cost:";
static NSString * const kLabelTax                                   =   @"Tax:";
static NSString * const kLabelOrderTotal                            =   @"Order Total:";
static NSString * const kLabelOrderSummary                          =   @"Order Summary:";
static NSString * const kLabelOrderDetails                          =   @"Order Details:";
static NSString * const kLabelPickUpAddress                         =   @"Pick Up Address:";

static NSString * const kLabelDeliveryAddress                       =   @"Delivery Address:";
static NSString * const kLabelEnterADeliveryAddress                 =   @"Enter A Delivery Address:";
static NSString * const kLabelPaymentDetails                        =   @"Payment Details:";
static NSString * const kLabelCouponCodeInfo1        =   @"-Coupon/Promotion Code:";
static NSString * const kLabelCouponCodeInfo        =   @"+Coupon/Promotion Code:";
static NSString * const kLabelPaymentInfo           =   @"Payment Info:";
static NSString * const kLabelSpecialInstructions   =   @"Special Instructions:";
static NSString * const kLabelDeliveryDate          =   @"Delivery Date:";

static NSString * const kLabelStreetAddress         =   @"Street Address";
static NSString * const kLabelStoreHours            =   @"Store Hours";
static NSString * const kLabelAddress               =   @"Address:";
static NSString * const kLabelContact               =   @"Contact:";
static NSString * const kLabelNotificationDesc      =   @"Manage your notification preferences by clicking on each of the icons below.";
static NSString * const kLabelClosed                =   @"Closed";
static NSString * const kLabelStatus                =   @"Status:";
static NSString * const kLabelConfirmationNo        =   @"Confirmation Number:";
static NSString * const kLabelNote                  =   @"Note:";
static NSString * const kLabelPaid                  =   @"Paid";
static NSString * const kLabelPending               =   @"Pending";
static NSString * const kLabelUnpaid                =   @"Unpaid";
static NSString * const kLabelProductName           =   @"Product Name";
static NSString * const kLabelQuantity              =   @"Quantity";
static NSString * const kLabelPrice                 =   @"Price";
static NSString * const kStringPayNow               =   @"PayNow";
static NSString * const kStringPayAtStore           =   @"PayAtStore";
static NSString * const kStringDeliver              =   @"Deliver";
static NSString * const kStringPickUp               =   @"PickUp";
static NSString * const kStringSelectAll            =   @"Select All";




//@"Product Details:"
//menu-logo//menu-logoForSlider @"PickUp"//below-header-white-strip @@"Powered by"
//@"No Product Available..."
static NSString * const kNoEvents                      =   @"No Events Available...";

static NSString * const kNoProduct                      =   @"No Product Available...";
static NSString * const kNoDeals                        =   @"No Deals Available...";
static NSString * const kNoFavorites                    =   @"No Favorites Available...";
static NSString * const kNoNotification                 =   @"No Notifications Available...";
static NSString * const kNoReview                       =   @"No Review Available...";
static NSString * const kTapToloadMore                  =   @"Tap here to load more items...";

//@"Tap here to load more items..."
#pragma mark-textview title
static NSString * const kMessagetermsAndConditions              =   @"terms & conditions";
static NSString * const kMessageIAgreeTo                        =   @"By Signing In, you agree to ";
static NSString * const kMessageIAgreeTo2                        =   @"By Signing up, you agree to the ";

static NSString * const kMessageAndCertify                      =   @" ";
static NSString * const kMessageGetDirectionFrom                      =   @"Get Direction From";

//@" and certify that I am over 21 years of age"
static NSString * const kMessageEventsAndPromotionTextViewTittle =   @"EVENTS & PROMOTIONS teaser copy about an upcoming event here";
static NSString * const kMessageIagree                           =   @"By Signing In, you agree to terms and conditions";
static NSString * const kMessageIagree2                           =   @"By Signing up, you agree to the terms and conditions";

static NSString * const kMessageNotificationScreen               =   @"Manage your notification preferences by clicking on each of the icons below";

static NSString * const kMessageForgotPasswordSuccess            =   @"Password reset email has been sent";
static NSString * const kMessageProductQuantityCheck1            =    @"Added quantity cannot be more than actual product quantity";
static NSString * const kMessageProductQuantityCheck2            =    @"Please select valid product quantity";

//Privacy Text
static NSString * const kMessagePrivacypolicyText                =   @"Privacy, Security & Refund policy";

#pragma mark-alertmessage title

static NSString * const kAlertMessageProfileEdited          =   @"Profile edited ";
static NSString * const kAlertMessageReviewSubmit           =   @"Review is submitted ";
static NSString * const kAlertValidEmail                    =   @"Enter a valid Email Id!";

static NSString * const kAlertForgotPasswordSucess          =   @"Password reset email has been sent";
static NSString * const kAlertEditUserProfileSucess         =   @"Profile has been updated ";
static NSString * const kAlertEditUserProfileFailed         =   @"Profile updation failed";
static NSString * const kAlertNoOrderAvailable              =   @"No order is available! ";
static NSString * const kAlertNoRecordFound                 =   @"No record found! ";

static NSString * const kAlertShoppingCart                  =   @"Product added to Shopping Cart ";
static NSString * const kAlertTransactionFailed            =   @"Transaction Failed";
static NSString * const kAlertTryAgain                     =   @"Please try again";
static NSString * const kAlertStoreISClosed                =   @"Store is closed";
static NSString * const kAlertStoreISClosedForToday        =   @"Store is closed for today";

//@"Transaction Unsuccessful"
static NSString * const kAlertLogot                         =   @"Please press logout button ! ";
static NSString * const kAlertPlaceOrderSucess              =   @"Your order has been placed ! ";
static NSString * const kAlertnotificationSend              =   @"Notification send ! ";

static NSString * const kAlertVerifyEmail                   =   @"Email id already exist";
static NSString * const kAlertRemoveFavorite                =   @"Removed from Favorites";
static NSString * const kAlertNotRemoveFavorite             =   @"Product not removed from Favorites";
static NSString * const kAlertNotification                  =   @"Your request has been saved";
static NSString * const kAlertNotificationHistory           =   @"No notification available!";
static NSString * const kAlertReview                        =   @"No Review available!";
static NSString * const kAlertReviewBeFirst                 =   @"Be the first one to review this product!";
static NSString * const kAlertReviewTitleRequired           =   @"Review Title is required.";
static NSString * const kAlertReviewDescriptionRequired     =   @"Review Description is required.";

static NSString * const kAlertSearchError                   =   @"No Result Found!";
static NSString * const kAlertLoyaltyCardError              =   @"No Loyalty Card Found!";
static NSString * const kAlertValidPhoneNumber              =   @"Phone number must be 10 digit.";
static NSString * const kAlertEnterApartment                =   @"Apartment is required.";

static NSString * const kAlertEnterUsername                 =   @"User Name is required.";
static NSString * const kAlertEnterEmail                    =   @"Email required!";
static NSString * const kAlertEnterDOB                      =   @"Date of Birth is required.";
static NSString * const kAlertEnterContactNO                =   @"Phone number is required.";
static NSString * const kAlertTermsAndCondition             =   @"You must agree to the terms & conditions before login.";
static NSString * const kAlertMinPassword                   =   @"Password must be of minimum 4 characters.";
static NSString * const kAlertPassword                      =   @"Password is required.";
static NSString * const kAlertEnterfirstName                =   @"First Name is required.";
static NSString * const kAlertEnterLastName                 =   @"Last Name is required.";
static NSString * const kAlertEnterAddress                  =   @"Address is required.";

static NSString * const kAlertEnterCity                     =   @"City is required.";
static NSString * const kAlertEnterState                    =   @"State is required.";
static NSString * const kAlertEnterZipCode                  =   @"Zip Code is required.";
static NSString * const kAlertValidZipCode                  =   @"Zip Code must be five digits.";
static NSString * const kAlertEnterPhone                     =   @"Phone number is required.";
static NSString * const kAlertInvalidAddress                 =   @"We do not deliver at this address!";
static NSString * const kAlertInvalidDOB                     =   @"You must be 21 year old to use our application!";

//[response valueForKey:@"Message"]

static NSString * const kBarcodeTypeSignUp              =   @"SignUp";
static NSString * const kBarcodeID                      =   @"BarcodeID";
//
//@"Date of Birth is required."
//@"I agree to the terms and conditions and certify that I am over 21 years of age"
//@"Forgot your password ?"



//@"Contact number is required."
#pragma mark- validation Title and messages

#pragma mark- validation Constents
//ProductDetailsViewControllerStoryBoardID
#pragma mark- View controller Storyboard Identifiers
static NSString * const kStoryboardIdentifier                                 =    @"Main";
static NSString * const kProductDetailsViewControllerStoryBoardID             =    @"ProductDetailsViewControllerStoryBoardID";
static NSString * const kMyOrdersViewControllerStoryBoardID                   =    @"MyOrdersViewControllerStoryBoardID";

static NSString * const kHomeViewControllerStoryboardId                       =    @"HomeViewControllerStoryboardId";
static NSString * const kUserProfileViewControllerStoryboardId                =    @"UserProfileViewControllerStoryboardId";
static NSString * const kContactUsViewControllerStoryboardId                  =    @"ContactUsViewControllerStoryboardId";
static NSString * const kDealsProfileViewControllerStoryboardId               =    @"DealsViewControllerStoryboardId";
static NSString * const kNotificationViewControllerStoryboardId               =    @"NotificationsViewControllerStoryboardId";
static NSString * const kLoyaltyCardViewControllerStoryboardId                =    @"LoyaltyCardViewControllerStoryboardId";
static NSString * const kFavoritesViewControllerStoryboardId                  =    @"FavoritesViewControllerStoryboardId";
static NSString * const kShoppingCartViewControllerStoryboardId               =    @"ShoppingCartViewControllerStoryboardId";
static NSString * const kTermsAndConditionViewControllerStoryboardId          =    @"TermsAndConditionViewControllerStoryboardId";
static NSString * const kSignUpViewControllerStoryboardId                     =    @"SignUpViewControllerStoryboardId";
static NSString * const kEventsDetailViewControllerStoryboardId               =    @"EventsDetailViewControllerStoryboardId";
static NSString * const kLoginViewControllerStoryboardId                      =    @"LoginController";
static NSString * const kEditAccountViewControllerStoryboardId                =    @"EditAccountViewControllerStoryboardId";
static NSString * const kSearchBeerViewControllerStoryboardId                 =    @"SearchBeerViewControllerStoryboardId";

static NSString * const kSearchResultViewControllerStoryboardId               =    @"SearchResultViewControllerStoryboardId";
static NSString * const kOrderDetailViewControllerStoryboardId                =    @"OrderDetailViewControllerStoryboardId";
static NSString * const kOrderConfirmationViewControllerStoryboardId          =    @"OrderConfirmationViewControllerStoryboardId";
static NSString * const kReviewDetailsViewControllerStoryboardId              =    @"ReviewDetailsViewControllerStoryboardId";
static NSString * const kBarcodeScannerViewControllerStoryboardId             =    @"BarcodeScannerViewControllerStoryboardId";

static NSString * const kReviewViewControllerStoryboardId                     =    @"ReviewViewControllerStoryboardId";
static NSString * const kFavoriteBrowseControllerStoryboardId                 =    @"FavoriteBrowseStoryBoard";

//ReviewViewController

#pragma mark- View controller Segue Identifiers
static NSString * const kGoToHomeViewControllerSegueId                       =    @"GoToHomeViewControllerSegueId";
static NSString * const kGoToLoginViewControllerSegueId                      =    @"kGoToLoginViewController";
static NSString * const kGoToSignUpViewControllerSegueId                     =    @"GoToSignUpViewControllerSegueId";
static NSString * const kGoToTermsAndConditionViewControllerSegueId          =    @"GoToTermsAndConditionViewControllerSegueId";
static NSString * const kGoToForgotPasswordViewControllerSegueId             =    @"GoToForgotPasswordViewControllerSegueId";
static NSString * const kGoToUserProfileViewControllerSegueId                =    @"GoToUserProfileViewControllerSegueId";
static NSString * const kGoToLoyaltyCardViewControllerSegueId                =    @"GoToLoyaltyCardViewControllerSegueId";
static NSString * const kGoToFavoritesViewControllerSegueId                  =    @"GoToFavoritesViewControllerSegueId";
static NSString * const kGoToContactUsViewControllerSegueId                  =    @"GoToContactUsViewControllerSegueId";
static NSString * const kGoToDealsViewControllerSegueId                      =    @"GoToDealsViewControllerSegueId";
static NSString * const kGoToNotificationsViewControllerSegueId              =    @"GoToNotificationsViewControllerSegueId";
static NSString * const kGoToNotificationsHistoryViewControllerSegueId       =    @"GoToNotificationsHistoryViewControllerSegueId";

static NSString * const kGoToSearchResultViewControllerSegueId               =    @"GoToSearchResultViewControllerSegueId";
static NSString * const kGoToSearchWineViewControllerSegueId                 =    @"GoToSearchWineViewControllerSegueId";
static NSString * const kGoToSearchLiquorViewControllerSegueId               =    @"GoToSearchLiquorViewControllerSegueId";
static NSString * const kGoToSearchBeerViewControllerSegueId                 =    @"GoToSearchBeerViewControllerSegueId";

static NSString * const kGoToEditAccountViewControllerSegueId                =    @"GoToEditAccountViewControllerSegueId";
static NSString * const kGoToEventsDetailViewControllerSegueId               =    @"GoToEventsDetailViewControllerSegueId";
static NSString * const kGoToShoppingCartViewControllerSegueId               =    @"GoToShoppingCartViewControllerSegueId";
static NSString * const kGoToMyOrderViewControllerSegueId                    =    @"GoToMyOrderViewControllerSegueId";

static NSString * const kGoToOrderConfirmationViewControllerSegueId          =    @"GoToOrderConfirmationViewControllerSegueId";
static NSString * const kGoToOrderDetailViewControllerSegueId                =    @"GoToOrderDetailViewControllerSegueId";
static NSString * const kGoToProductDetailsViewControllerSegueId             =    @"GoToProductDetailsViewControllerSegueId";
static NSString * const kGoToReviewViewControllerSegueId                     =    @"GoToReviewViewControllerSegueId";
static NSString * const kGoToReviewDetailsViewControllerSegueId             =    @"GoToReviewDetailsViewControllerSegueId";
static NSString * const kGoToEventsListViewControllerSegueId                =    @"GoToEventsListViewControllerSegueId";
static NSString * const kGoToBarCodeScannerViewControllerSegueId            =    @"GoToBarCodeScannerViewControllerSegueId";
static NSString * const kGotoFavoriteBrowseControllerSegueId                =    @"kGotoFavoriteBrowseController";



//GoToBarCodeScannerViewControllerSegueId

#pragma mark- View controller StoryboardId
static NSString * const kRecommendedBESDetailsStoryboardId                   =   @"RecommendedBESDetails";

#pragma mark- Date Format constants
static NSString * const kClientDateFormat                                    = @"MMM dd, yyyy";
static NSString * const kServerDateFormat                                    = @"yyyy-MM-dd HH:mm:ss";
static NSString * const kServerDateFormat2                                   = @"MMM dd, yyyy";
static NSString * const kTimeFormat                                          = @"hh:mm:ss a";

//[dfTime setDateFormat:@"hh:mm:ss a"];

#pragma mark- Tableview cell
static NSString * const  kDefaultTableViewCell                               = @"defaultLoadingCell";
static int const         kLoadingCellTag                                     = 1273;
#pragma mark- tablview cell activity indicator tag value
static int const         kCellActivityIndicatorTag                           = 3000;

/**********adding delegate ****/
@protocol DobEditedDelegate <NSObject>
@required
- (void) GetDobEditedMethod:(NSString*)selectedDob ;

@end

/**********adding delegate ****/

@interface BaseViewController : UIViewController <UIViewControllerTransitioningDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,EDStarRatingProtocol>

@property (nonatomic, assign) id<DobEditedDelegate> delegate;

@property (retain, nonatomic)  UITextField                *firstNameTextfield;
@property (retain, nonatomic)  UITextField                *lastNameTextfield;
@property(nonatomic,retain)UITextField                      *phoneNumberTextfield;


@property (nonatomic, retain) NSString *updatedImageUrl;
@property(nonatomic,retain)   UIView                        *customView;
@property(nonatomic,retain)   UILabel                       *placeholderLabel;
@property(nonatomic,retain)   UIImageView                   *placeholderImageView;
@property(nonatomic,retain)   UIScrollView                  *scrollView;
@property (nonatomic, retain) UIImage                       *userProfileImage;
@property (nonatomic, retain) NSString                      *DateOFBirthString;
@property (nonatomic, retain) UIButton                      *refreshButton;
@property (nonatomic, retain) UIButton                      *bottomBack;
@property (retain, nonatomic) UIImageView                   *LogoImageView;
@property (retain, nonatomic) UIScrollView                  *CustomScrollView;
@property (retain, nonatomic) UITableView                   *defaultTableView;
@property (retain, nonatomic) UIView                   *CustomButtonContainerView;
@property (retain, nonatomic)  EDStarRating *starRating;
@property (retain, nonatomic)  NSString *starRatingstring;
@property (retain, nonatomic)  UIView *RatingView;
@property (retain, nonatomic)  UILabel *ValuesLabel;
@property (retain, nonatomic)  UIButton                   *addAPhotoButton;
@property (retain, nonatomic)  UITextField                *dateOfBirthTextfield;
@property (retain, nonatomic)  UIView                     *dropDownView;
@property (nonatomic,retain)   UIDatePicker               *datePicker;
@property (retain, nonatomic)  UITextField                *loyaltyCardTextfield;
@property (retain, nonatomic)  NSArray             *FavouriteListArray;
@property (nonatomic, strong) NSMutableArray      *arrayForBoolProductStatus;

//    NSMutableArray      *FavouriteListArray;


//@property (retain, nonatomic) NSMutableArray *productTypeArray;
//@property (strong, nonatomic) NSMutableArray *eventTypeArray;
//@property (strong, nonatomic) NSMutableArray *postEventArray,*postItemArray;

@property(nonatomic,retain)MTBBarcodeScanner    *scanner;
@property(nonatomic,retain)UIView    *scannerView;
@property(nonatomic,retain)UILabel    *apartmentLabel;
@property(nonatomic,retain)UILabel    *streetLabel;
@property(nonatomic,retain)UILabel    *StateLabel;
@property(nonatomic,retain)NSString *barcodeID;
//@property(nonatomic,retain)NSString *dateString;

@property(nonatomic,retain)UITextField *CardNumberTextFd;
//phoneNumberTextfield
//NSString *barcodeID;
// UIView *containerView
-(NSString*)getDOBStringFromDate:(NSDate*)date;
//-(void)CallApiGetreviewwithProductId:(NSString*)getProductID;
-(void) showSpinner;
-(void) showSpinner1;
-(void) showSpinnerToSuperView:(UIView*) superView;
-(void) hideSpinner;
-(void)GoBack;
-(void)addDatePickerToSignUpScreen;
-(UIColor*)getColorForLightestGrey;
// dismiss and pop view controllers
-(void) popViewControllerWithDelay:(double)delay complition:(completion)complition;
-(void) dismissViewControllerWithDelay:(double)delay complition:(completion)complition;
-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar;
//Instantiate and return View Controller in storyboard
-(id) viewControllerWithIdentifier:(NSString*)identifier;

- (NSString*) convertDateToClientrePresentFormat: (NSString*)date; //convert date format to Client date formats
//headercolors
- (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message;
-(void)showConfirmViewWithTitle:(NSString *)title message:(NSString *)message otherButtonTitle:(NSString *)otherButtonTitle;
#pragma mark-create labelimageview view webviewtextfields etc
-(UIBarButtonItem*) createBackButtonWithSelector:(SEL)selectorName;
-(void)customizeTheNavigationbar:(UIViewController*)controller;
-(void)setNavBarTitle:(NSString*)title controller:(UIViewController*)controller italic:(BOOL) isItalic;
- (UIImage *)crop:(UIImage *)image convertToSize:(CGRect) rect;
-(UIImage*)imageNamed:(NSString*) imageName;
-(void) setGradient:(UIView*)view colors:(NSArray*)colors locations:(NSArray*)locations;
-(UIView*)createContentViewWithFrame:(CGRect) frame ;
-(UIImageView*) createImageViewWithRect:(CGRect)rect image:(UIImage*) image ;
-(UITextField*) createTxtfieldWithRect:(CGRect)rect text:(NSString*) text withLeftImage:(UIImage*)leftImageName  withPlaceholder:(NSString*)placeholder withTag:(int)textfieldTag ;
-(UITextField*)createDefaultTxtfieldWithRect:(CGRect)rect text:(NSString*) text withTag:(int)textfieldTag withPlaceholder:(NSString*)placeholder;
-(UIScrollView*)createScrollViewWithFrame:(CGRect) frame   ;
-(UIButton*)createSelectedUnselectedButtonViewWithFrame:(CGRect) frame withTitle:(NSString*)Title  ;
-(UIButton*)createButtonWithFrame:(CGRect) frame withTitle:(NSString*)Title  ;
-(UIActionButton*)createActionButtonWithFrame:(CGRect)frame withTitle:(NSString *)Title;
-(UITextView*) createTxtViewWithRect:(CGRect)rect text:(NSAttributedString*) text withTextColor:(UIColor*)textcolor withfont:(UIFont*)font;
-(SAMTextView*) createCustomTxtViewWithRect:(CGRect)rect text:(NSString*) text withTextColor:(UIColor*)textcolor withfont:(UIFont*)font;
-(UITextView*) createDefaultTxtViewWithRect:(CGRect)rect text:(NSString*) text withTextColor:(UIColor*)textcolor withfont:(UIFont*)font;
-(UILabel*) createLblWithRect:(CGRect) rect font:(UIFont*) font text:(NSString*)text textAlignment:(NSTextAlignment) textAlignment textColor:(UIColor*) textColor;

-(PageControl*)createCustompagecontrol :(int)NumberOfpages currentPage:(int)currentpage withFrame:(CGRect)frame;
-(UIWebView*)createAWebView:(CGRect)frame withURL:(NSString*)urlString;
-(void)AddNavigationControllerWithTitleAndKartButton;
-(void)AddDefaultTableView;
-(UIButton*)AddCustomButtonInsideContentViewWithFrame:(CGRect)frame;
-(UIColor *) colorFromHexString:(NSString *)hexString;
#pragma mark-getcolorcodes
//getcolorcodes
-(void)addBarCodeScannerWithType:(NSString*)barcodeTypeString;
-(UIColor*)getColorForOrderDetailCell;
-(UIColor*)getColorForBlackText;
-(UIColor*)getColorForCardNo;
-(UIColor*)getColorForRedText;
-(UIColor*)getColorForYellowText;
-(UIColor*)getColorForLightGreyColor;
-(void)addImagePickerOnClickOfButton:(UIButton*)button;
-(void)addScrollView;
//-(void)addAppLogoImageInCenterOFLoginScreen;
//-(void)addTrantParentview;
-(UIButton*)AddCustomButtonWithRedBorder:(CGRect)buttonFrame withTitle:(NSString*)title;
#pragma mark-ADDfont
-(UIFont*)addFontRegular;
-(UIFont*)addFontBold;
-(UIFont*)customeFontNeha;
-(UIFont*)addFontSemiBold;
-(NSString*) ProperValue:(NSString*)value;
-(BOOL) validateEmail: (NSString *) email;
//-(UITableViewCell*)createCustomCellForTableView:(UITableView*)tableView ;
-(AsyncImageView*) createAsynImageViewWithRect:(CGRect)rect imageUrl:(NSURL*) url isThumnail:(BOOL)isthumnail;
-(NSString *)dateStringFromString:(NSString *)sourceString sourceFormat:(NSString *)sourceFormat destinationFormat:(NSString *)destinationFormat;
-(void)addGreyBorderToCartAndLikeVike :(UIView*)view;
//-(void)addCartAndLikeWithImageV:(UIView*)bgV WithRightButton:(UIButton*)rightButton withLeftButton:(UIButton*)leftButton andSeperatorV:(UIView*)seperatorV;

-(void)CallApiAddFavoriteWithProductID:(NSString*)productId;
-(void)CallApiRemoveFavoriteWithProductID:(NSString*)productId;
-(void)callApiGetLoyalityCardImage;
-(void)callApiStoreDetails;
-(void)CallApiMyaccount;
- (NSMutableArray *)createMutableArray:(NSArray *)array;
- (void) clickImage;
- (NSString *)GetUUID;
//-(void)configureTableCell:(UITableViewCell*)tCell AtIndexPath:(NSIndexPath*)indexPath;
-(void)CreateAddAphotoButton;
//- (void) UploadimageNotification:(NSNotification *) notification;
-(NSDate*)SetDefaultDateForDatePicker;
-(void)addDatePickerToView;
-(void) hideDatePickerview;
-(void)AddBottomView;
- (void) getuserImage;
-(BOOL)isNull:(id)object;
-(void)CallApiGetSelectedProductCategory;
-(void)CallapiInsertNotificationSubscription:(NSMutableArray *)eventArr storeID:(NSString *)store_ID itemsArray:(NSMutableArray *)itemArr userID:(NSString *)user_ID isEventSelected:(NSString *)event_Selected;
- (UIImage *)centerCropImage:(UIImage *)image;
//- (void)getCurrentLocation:(id)sender;
-(void)ShowLocationWithLat:(NSString*)latStr longitude:(NSString *)longStr;
-(NSString*)GetTimeInAmPmFormat:(NSString*)timeString;
-(NSString*)addDollarSignToString:(NSString*)givenString;
-(CGRect)setDynamicViewsFrameFor:(NSString*)stringOfDynamicSize;
-(void)getSeperatorVForTableCell:(UITableViewCell*)cell;
-(NSString*)getformattedPhoneNumberstringFrom:(NSString*)phoneString;
-(NSString*)getOriginalPhonestringFromFormated:(NSString*)mobileNumber;
-(NSAttributedString*)addDollarSignTocutOverString:(NSString*)givenString;
-(void)addNorecordFoundLAbelOnTableView:(UITableView*)tableView;
-(float) floatWithTwoDecimalPoints:(float) myFloat;
@end