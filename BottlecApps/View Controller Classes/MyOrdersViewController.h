//
//  MyOrdersViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 10/7/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "OrderDetailsTableViewCell.h"
@interface MyOrdersViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>{
    //This array will store our coments
    //This is the index of the cell which will be expanded
    NSInteger selectedIndex;
    NSMutableArray  *arrayForBool;
}
@property (nonatomic,retain)    UIButton * NewOrderButton;
@property (strong, nonatomic)   UILabel *ProductNameLabel;
@property (strong, nonatomic)   UILabel *ProductStatusLabel;
@property (strong, nonatomic)   UILabel *ProductPriceLabel;
@property (strong, nonatomic)   UIButton *addOrDeleteButton;
@property (strong, nonatomic)   UILabel *DateAndTimeLabel;
@property (nonatomic, retain)   NSString *searchProductMinPrice;
@property (nonatomic, retain)   NSString *searchProductMaxPrice;
@property (nonatomic, retain)   NSString *searchProductName;
@property (nonatomic, retain)   NSString *searchType;

-(void)indirectViewDidLoad;

@end
