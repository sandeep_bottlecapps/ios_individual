//
//  EventsDetailViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 8/12/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "EventsDetailViewController.h"
#import "AsyncImageView.h"
#import "EventTableViewCell.h"
#define keventImageWidth 320
#define keventImageHeight 200
@interface EventsDetailViewController (){
    NSMutableArray *itemArray;
    NSMutableArray *EventsDetailItemsArray;
    NSMutableDictionary *apiResponseDictionary;
    NSArray *TitleArray;
    UIView*  _transparentView;
    CGRect txtRect;
    NSMutableArray * cellArray;
    long descriptionlength;
}
@end
UILabel *detailLbl;
@implementation EventsDetailViewController
@synthesize getEventID;
@synthesize eventsDict;
- (CGRect)sizeCountForLabel:(UILabel *)label {
    
    CGSize constrain = CGSizeMake(label.bounds.size.width, FLT_MAX);
    // CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constrain lineBreakMode:NSLineBreakByWordWrapping];
  /*  CGSize size = [label.text sizeWithFont:label.font
                         constrainedToSize:constrain  // - 40 For cell padding
                             lineBreakMode:NSLineBreakByWordWrapping]; */
    
    CGRect labelRect =  [label.text boundingRectWithSize:constrain
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:label.font}
                                                 context:nil];
    CGSize size = labelRect.size;

    
    return CGRectMake(0, 0, size.width, size.height);
    
}

-(void)UpdateUI{
    NSURL *imageurl = [NSURL URLWithString:[eventsDict valueForKey:@"EventLargeImage"]];
    
    EventsDetailItemsArray =[NSMutableArray arrayWithObjects:[apiResponseDictionary valueForKey:@"EventCountDetail"], nil];
    AsyncImageView  * EventsImageView =[self createAsynImageViewWithRect:CGRectMake(0,0
                                                                                    ,SCREEN_WIDTH, keventImageHeight + 50) imageUrl:imageurl isThumnail:NO]
    ;
    [self.CustomScrollView addSubview:EventsImageView];
    
   /* UILabel *eventtitleLabel=[self createLblWithRect:CGRectMake(0,(keventImageHeight + 50) - kLabelHeight*2-15,SCREEN_WIDTH, kLabelHeight*2) font:[UIFont fontWithName:kFontSemiBold size:13] text:[NSString stringWithFormat:@" %@ ",[[itemArray objectAtIndex:0] valueForKey:@"EventName"]] textAlignment:NSTextAlignmentCenter textColor:[UIColor whiteColor]];
    eventtitleLabel.backgroundColor=[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:140.0/255.0 alpha:1.0];
   // [eventtitleLabel sizeToFit];
    [EventsImageView addSubview:eventtitleLabel]; */
    
    [self AddDefaultTableView];
    [self.defaultTableView setDelegate:self];
    [self.defaultTableView setDataSource:self];
    self.defaultTableView.frame = CGRectMake(0, self.CustomScrollView.frame.size.height+self.CustomScrollView.frame.origin.y
                                             , SCREEN_WIDTH, SCREEN_HEIGHT-((kBottomViewHeight)+kNavigationheight+kTopPadding_small + self.CustomScrollView.frame.size.height));
    // self.defaultTableView.alwaysBounceVertical = YES;
    /**********************************Setup tableView****************************************/
}

-(void)CallApiGetEventDetails{
    NSMutableDictionary* paramDict = [NSMutableDictionary dictionary];
  //  User *userobj=kGetUserModel;
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [paramDict setObject:@"1" forKey: kParamPageNumber];
    [paramDict setObject:@"10" forKey: kParamPageSize];
    if ([eventsDict valueForKey:@"EventId"]) {
        [paramDict setObject:[eventsDict valueForKey:@"EventId"] forKey: kParamEventId];
    }
    else{
        [paramDict setObject:@"21" forKey: kParamEventId];
    }
    //[paramDict setObject:userobj.userId forKey: kParamUserId];
    
    // getEventsWithEventsList
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] GetEventDetailsDetailsWithParam:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
       // NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            apiResponseDictionary=[[NSMutableDictionary alloc] initWithDictionary:response];;
            itemArray=[apiResponseDictionary valueForKey:@"EventCountDetail"];
            DLog(@"response%@ apiResponseDictionary--%@ itemArray--%@",response,apiResponseDictionary,[[itemArray objectAtIndex:0] valueForKey:@"EventImage"]);
            
            [self UpdateUI];
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        
    }];
    
}

-(void)initializeAllArrayAndDict    {
    itemArray =[NSMutableArray new];
}

#pragma mark-viewcontrollerlifecycle.
-(void)viewDidLoad{
    
    cellArray = [NSMutableArray arrayWithCapacity:4];
    [super viewDidLoad];
    TitleArray= @[@"When",@"Where",@"Description",@"Price"];
    [self initializeAllArrayAndDict];
    [self CallApiGetEventDetails];
    [self addScrollView];
    
    self.CustomScrollView.frame = CGRectMake(0,kNavigationheight+kTopPadding_small
                                             , SCREEN_WIDTH, ((SCREEN_HEIGHT/3)+100)-kNavigationheight-kTopPadding_small);
    [self.CustomScrollView setDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getLocationButtonClicked:(UIButton*)sender{
    [self ShowLocationWithLat:[[itemArray objectAtIndex:0] valueForKey:@"StoreLatitude"] longitude:[[itemArray objectAtIndex:0] valueForKey:@"StoreLongitude"]];
}

#pragma mark - UITableView Delegate & Datasrouce -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section    {
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    if(indexPath.row == 0)  {
        return kRowHeight_custom-kPaddingSmall + 20;
    }
    else if (indexPath.row==1) {
        return kRowHeight_custom-kPaddingSmall +kLabelHeight;
    }
    else if (indexPath.row==2){
       NSStringDrawingContext *ctx = [NSStringDrawingContext new];
        NSAttributedString *aString = [[NSAttributedString alloc] initWithString: [[itemArray objectAtIndex:0] valueForKey:@"EventDescription"]];
//        NSAttributedString *aString = [[NSAttributedString alloc] initWithString: @"ul 4, 2015 - Temporary exceptions can be configured via your app's Info.plist ... And then a Boolean within that Dictionary named  set to YES. .... App Transport Security has blocked a cleartext HTTP (http://) resource load since it is ... Raise exception on App Transport Security?"];

        
        UITextView *calculationView = [[UITextView alloc] init];
        [calculationView setAttributedText:aString];
        
        txtRect = [calculationView.text boundingRectWithSize:self.view.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:calculationView.font} context:ctx];
        NSLog(@"%f",txtRect.size.height);
        if(txtRect.size.height<=30){
            descriptionlength = txtRect.size.height+30,0;
            return txtRect.size.height+30.0;
        }
        else{
             descriptionlength = txtRect.size.height+60.0;
          return txtRect.size.height+60.0;
        }
        
        /* NSString *description = [[itemArray objectAtIndex:0] valueForKey:@"EventDescription"];
         descriptionlength = [description length];
        NSLog(@"Description length = %ld",descriptionlength);
        if([description length]<=50)
            return 50;
        else
            return descriptionlength/2.0+40.0; */
       
          }
    else if (indexPath.row==3)   {
        return kRowHeight_custom-kPaddingSmall;
    }
    else
        return  kRowHeight_custom-kPaddingSmall;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    UIView *separatorIView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0.5)];
    [separatorIView setBackgroundColor:[UIColor lightGrayColor]];

//    if(cellArray && (cellArray.count > indexPath.row) && [cellArray objectAtIndex:indexPath.row])
//        return [cellArray objectAtIndex:indexPath.row];
 //   else    {
   
    tableView.scrollEnabled=YES;
    tableView.allowsSelection = NO;
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier: kDefaultTableViewCell];
        if (cell == nil)    {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                          reuseIdentifier:kDefaultTableViewCell];
        // [cell addSubview:separatorIView];
        }
    tableView.bounces = YES;
    cell.layoutMargins = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins =NO ;

        NSArray *arrSubView = cell.subviews;
        for(UIView *subView in arrSubView){
            //if(subView.tag == 1 || subView.tag == 2 || subView.tag == 3){
                [subView removeFromSuperview];
          //  }
        }
        
    
        
        UILabel *TitleLabel=[self createLblWithRect:CGRectMake(15, kPaddingSmall, kLabelWidthForCell, kLabelHeight)
                                               font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize - 1] text:[TitleArray objectAtIndex:indexPath.row] textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
        
       
        switch (indexPath.row) {
            case 0:{
               
                UILabel *detailLbl=[self createLblWithRect:CGRectMake(15,TitleLabel.frame.origin.y+TitleLabel.frame.size.height - 3, kLabelWidthForCell, kLabelHeight) font:[self addFontRegular] text:[NSString stringWithFormat:@"%@ - %@",[self convertDateToClientrePresentFormat:[[itemArray objectAtIndex:0] valueForKey:@"EventStartDt"]],[self convertDateToClientrePresentFormat:[[itemArray objectAtIndex:0] valueForKey:@"EventEndDt"]]] textAlignment:NSTextAlignmentLeft textColor:[UIColor colorWithRed:0.4569 green:0.4569 blue:0.4608 alpha:1.0]];
                
                NSString *starttime=[self ProperValue:[[itemArray objectAtIndex:0] valueForKey:@"EventStartTime"]];
                NSString *endtime=[self ProperValue:[[itemArray objectAtIndex:0] valueForKey:@"EventEndTime"]];
                
                if (![starttime isEqualToString:kEmptyString]) {
                    UILabel * timeLabel = [self createLblWithRect:CGRectMake(15,detailLbl.frame.origin.y+detailLbl.frame.size.height - 3, kLabelWidthForCell, kLabelHeight) font:[self addFontRegular] text:[NSString stringWithFormat:@"%@ - %@",[self convertDateToClientrePresentFormat:[[itemArray objectAtIndex:0] valueForKey:@"EventStartTime"]],[self convertDateToClientrePresentFormat:[[itemArray objectAtIndex:0] valueForKey:@"EventEndTime"]]] textAlignment:NSTextAlignmentLeft textColor:[UIColor colorWithRed:0.4569 green:0.4569 blue:0.4608 alpha:1.0]];
                     timeLabel.text=[NSString stringWithFormat:@"%@ - %@",[self GetTimeInAmPmFormat:starttime],[self GetTimeInAmPmFormat:endtime]];
                    [cell addSubview:timeLabel];
                }
                               [cell addSubview:TitleLabel];
                [cell addSubview:detailLbl];
                TitleLabel.tag=1;
                detailLbl.tag=2;
            }
                break;
            case 1:{
                UILabel *detailLbl2;
               
                if ([[[itemArray objectAtIndex:0] valueForKey:@"UserAddress2"]isEqualToString:kEmptyString]) {

               detailLbl=[self createLblWithRect:CGRectMake(15,TitleLabel.frame.origin.y+TitleLabel.frame.size.height - 3,
                                                             kLabelWidthForCell - 32, kLabelHeight) font:[self addFontRegular] text:[[itemArray objectAtIndex:0] valueForKey:@"UserAddress1"] textAlignment:NSTextAlignmentLeft
                                        textColor:[UIColor colorWithRed:0.4569 green:0.4569 blue:0.4608 alpha:1.0]];
                }
                else{
                    detailLbl=[self createLblWithRect:CGRectMake(15,TitleLabel.frame.origin.y+TitleLabel.frame.size.height - 3,
                                                                 kLabelWidthForCell - 32, kLabelHeight) font:[self addFontRegular] text:[NSString stringWithFormat:@"%@,%@",[[itemArray objectAtIndex:0] valueForKey:@"UserAddress1"] ,[[itemArray objectAtIndex:0] valueForKey:@"UserAddress2"]] textAlignment:NSTextAlignmentLeft
                                            textColor:[UIColor colorWithRed:0.4569 green:0.4569 blue:0.4608 alpha:1.0]];
                }
               
                    detailLbl2=[self createLblWithRect:CGRectMake(15,detailLbl.frame.origin.y+detailLbl.frame.size.height - 3, kLabelWidthForCell- 32, kLabelHeight) font:[self addFontRegular] text:[NSString stringWithFormat:@"%@, %@ %@",[[itemArray objectAtIndex:0] valueForKey:@"UserCity"],[[itemArray objectAtIndex:0] valueForKey:@"StateName"],[[itemArray objectAtIndex:0] valueForKey:@"UserZipCode"]] textAlignment:NSTextAlignmentLeft
                                             textColor:[UIColor colorWithRed:0.4569 green:0.4569 blue:0.4608 alpha:1.0]];
//                }
//                else{
//                   detailLbl2=[self createLblWithRect:CGRectMake(15,detailLbl.frame.origin.y+detailLbl.frame.size.height - 3, kLabelWidthForCell- 32, kLabelHeight) font:[self addFontRegular] text:[NSString stringWithFormat:@"%@, %@ %@",[[itemArray objectAtIndex:0] valueForKey:@"UserCity"],[[itemArray objectAtIndex:0] valueForKey:@"StateName"],[[itemArray objectAtIndex:0] valueForKey:@"UserZipCode"]] textAlignment:NSTextAlignmentLeft textColor:[UIColor colorWithRed:0.4569 green:0.4569 blue:0.4608 alpha:1.0]];
//                }
                UIImage *getLocationImage=[UIImage imageNamed:kImage_locationicon];
                UIButton *getLocationButton=[self createButtonWithFrame:CGRectMake(SCREEN_WIDTH-(getLocationImage.size.width)- 2,TitleLabel.frame.origin.y+TitleLabel.frame.size.height+kPaddingSmall - 5, getLocationImage.size.width, getLocationImage.size.height) withTitle:nil];
                [getLocationButton setImage:getLocationImage forState:UIControlStateNormal];
                [getLocationButton addTarget:self action:@selector(getLocationButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                TitleLabel.tag=1;
                detailLbl.tag=2;
                getLocationButton.tag=3;
                
            
                
                [cell addSubview:TitleLabel];
                [cell addSubview:detailLbl];
                [cell addSubview:detailLbl2];
                [cell addSubview:getLocationButton];
                CGPoint buttonPosition = [TitleLabel convertPoint:CGPointZero toView:tableView];
                NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
                NSLog(@"Indexpath = %@",indexPath);

            }
                break;
            case 2:{
               
                NSString * text = [[itemArray objectAtIndex:0] valueForKey:@"EventDescription"];
             
                UITextView * textview = [[UITextView alloc]initWithFrame: CGRectMake(11, TitleLabel.frame.origin.y+TitleLabel.frame.size.height-5, kLabelWidthForCell, descriptionlength)];
                
                textview.backgroundColor = [UIColor clearColor];
                textview.font = [self addFontRegular];
                textview.textAlignment = NSTextAlignmentLeft;
                textview.textColor =[UIColor colorWithRed:0.4569 green:0.4569 blue:0.4608 alpha:1.0];
                textview.editable = NO;
                textview.dataDetectorTypes = UIDataDetectorTypeAll;
                textview.textAlignment = NSTextAlignmentJustified;
                textview.textContainer.lineBreakMode = NSLineBreakByCharWrapping;
                [textview  setTextContainerInset:UIEdgeInsetsMake(3, 0, 0, 0)];
                textview.scrollEnabled=NO;
               
                if(text && (![text isEqualToString:kEmptyString]))  {
                    textview.text = text;
                    
//                    textview.text = @"ul 4, 2015 - Temporary exceptions can be configured via your app's Info.plist ... And then a Boolean within that Dictionary named  set to YES. .... App Transport Security has blocked a cleartext HTTP (http://) resource load since it is ... Raise exception on App Transport Security?";
                }
                else    {
                    textview.text = kEmptytextfield;
                }
                
                [cell addSubview:TitleLabel];
                [cell addSubview:textview];
                TitleLabel.tag=2;
                textview.tag=1;
                
            }
                break;
            case 3:{
               
                NSString *price = [[itemArray objectAtIndex:0] valueForKey:@"EventPrice"];
                if((!price) || (price && [price isEqualToString:kEmptytextfield]) || (price && [price floatValue] <= 0.00))  {
                    price = @"Free";
                }
                else    {
                    price =  [self addDollarSignToString:price];
                }
                
                UILabel *detailLbl=[self createLblWithRect:CGRectMake(15,TitleLabel.frame.origin.y+TitleLabel.frame.size.height-3, kLabelWidthForCell, kLabelHeight) font:[self addFontRegular] text:price textAlignment:NSTextAlignmentLeft
                                        textColor:[UIColor colorWithRed:0.4569 green:0.4569 blue:0.4608 alpha:1.0]];
                detailLbl.tag=1;
                TitleLabel.tag=3;
                [cell addSubview:TitleLabel];
                [cell addSubview:detailLbl];
                
            }
                
                break;
            default:
                break;
        }
        
       // cell.selectionStyle=UITableViewCellSelectionStyleNone ;
        
        [cellArray insertObject:cell atIndex:indexPath.row];
        [cell addSubview:separatorIView];
        [cell bringSubviewToFront:separatorIView];
        return cell;
   // }
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    [self performSelector:@selector(performRelodingAfterAutoScroll) withObject:nil afterDelay:0.0];
}


-(void)cancelButtonClicked:(UIButton*)sender{
    [_transparentView removeFromSuperview ];
    
}
- (void)performRelodingAfterAutoScroll {
    
   }
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //    switch (indexPath.row)  {
    //        case 0:
    //            break;
    //
    //        case 1:
    //            [self performSegueWithIdentifier:kGoToDealsViewControllerSegueId sender:nil];
    //            break;
    //
    //        case 2:
    //            [self performSegueWithIdentifier:kGoToSearchResultViewControllerSegueId sender:nil];
    //            break;
    //
    //        case 3:
    //            [self performSegueWithIdentifier:kGoToNotificationsViewControllerSegueId sender:nil];
    //
    //            return;
    //            break;
    //    }
    //    
    //[self.navigationController pushViewController:vc animated:YES];
}

@end
