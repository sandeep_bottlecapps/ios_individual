//
//  FavoriteBrowseViewController.h
//  BOTTLECAPPS
//
//  Created by Techmatic Systems BCapps on 27/05/16.
//  Copyright © 2016 Monika Kumari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "AppDelegate.h"
@interface FavoriteBrowseViewController :BaseViewController
{
    AppDelegate *appDelegate;
    NSMutableArray *browseCartArray;
}

@end
