//
//  ContactUsViewController.h
//  BottlecApps
//
//  Created by Kuldeep Tyagi on 8/4/15.
//  Copyright (c) 2015 Kuldeep Tyagi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ClosestStoreModel.h"
#import <MessageUI/MessageUI.h>

@interface ContactUsViewController  :BaseViewController<UITextFieldDelegate,MFMailComposeViewControllerDelegate,UIActionSheetDelegate,UIScrollViewDelegate>{
    UIImage *storeImage;
    NSURL *storeImageURL;
    UIView*  _transparentView;
}
@property (nonatomic,retain) UIButton                   *EmailUsButton;
@property (nonatomic,retain) UIButton                   *CallUsButton;
@property (nonatomic,retain) NSMutableArray             *WeekDaysArray;
@property (nonatomic,retain) NSMutableArray             *WeekOpenTimeArray;
@property (nonatomic,retain) NSMutableArray             *WeekCloseTimeArray;
@property (nonatomic,retain) UIButton                   *getLocationButton;
@property (nonatomic,retain) NSMutableArray             *StoreDetailArray;
@property (nonatomic,retain) NSMutableArray             *StoreTimeArray;
@property (nonatomic,retain) NSMutableArray             *StoreAddressArray;

@end
