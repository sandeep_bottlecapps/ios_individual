//
//  TermsAndConditionViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 8/12/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "TermsAndConditionViewController.h"

@interface TermsAndConditionViewController ()

@end

@implementation TermsAndConditionViewController
-(void)viewWillAppear:(BOOL)animated{
     self.navigationController.view.userInteractionEnabled = YES;
     self.navigationItem.hidesBackButton=YES;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden =NO;

   
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] GetTermsAndConditionWithsuccessStatus:nil  successStatus:^(APIResponseStatusCode statusCode, id response) {
       // NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            [self serverOperationdidFinishWithAuthenticationKey:response];

        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [UIView showMessageWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage] showInterval:5.0  viewController:self];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-operationqueue
-(void)serverOperationdidFinishWithAuthenticationKey:(NSDictionary *)key
{
    /***************add a webview ************/
    webView=[self createAWebView:CGRectMake(kPaddingSmall, kNavigationheight, SCREEN_WIDTH-(kPaddingSmall*2), SCREEN_HEIGHT-kNavigationheight-kBottomViewHeight) withURL:nil];
    /***************add a webview ************/
    webView.backgroundColor=[UIColor clearColor];
    [self .view addSubview:webView];

    NSString *termsString = [key valueForKey:@"GetTermsResult"];
  //  NSLog(@"string is %@", termsString);
    [webView loadHTMLString:termsString baseURL:nil];
    [self hideSpinner];
}
-(void)serverOperationdidFailWithError:(NSError *)error
{
    [self hideSpinner];
}

-(void)serverOperationdidFailWithResponse:(NSHTTPURLResponse *)response
{
    [self hideSpinner];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
