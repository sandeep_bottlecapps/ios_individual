//
//  SideMenuViewController.h
//  BottlecApps
//
//  Created by Kuldeep Tyagi on 8/4/15.
//  Copyright (c) 2015 Kuldeep Tyagi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "SlideNavigationController.h"

@interface SideMenuViewController  :BaseViewController<UITextFieldDelegate>
{
    NSArray *sidemenuArray;
    NSArray *viewControllersArray;

}
@property (nonatomic,retain)UIButton * LogoutButton;
@end
