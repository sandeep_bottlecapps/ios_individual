//
//  OrderConfirmationViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 8/27/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "OrderConfirmationViewController.h"
#import "HomeViewController.h"
#import "AppDelegate.h"
#import "Constants.h"
#define kRowHeight_orderconfirmation                                150

@interface OrderConfirmationViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *orderSummaryItems,*orderSummaryValues,*getDetailArray;
    UITableViewCell *cell;

}
@end

@implementation OrderConfirmationViewController
@synthesize totalAmount,OrderNo,OrderDate,TrasactionId,senderName;
@synthesize getAddorderrespnseDict,getConfirmationNo,getCartArray;
@synthesize OrderTypeString,paymentTypeString;
@synthesize payTypeString;
@synthesize deliveryAddressDetails;
@synthesize uName;
- (void)viewDidLoad {
    [super viewDidLoad];
    getDetailArray=[[NSMutableArray alloc] init];
    getDetailArray=[getAddorderrespnseDict valueForKey:@"GetOrderStatus"] ;
    if([[[getDetailArray objectAtIndex:0]objectForKey:@"CouponCode"] isEqualToString:kEmptyString]){
//    if(![[getDetailArray valueForKey:@"CouponCode"] isEqualToString:kEmptyString]){
    self.cellItemsArray=[NSMutableArray arrayWithObjects:@"Order Details:",@"Order Summary:",@"Pick Up Address:",@"Payment Details:",nil];
    if([OrderTypeString isEqualToString:kStringDeliver])    {
        self.cellItemsArray=[NSMutableArray arrayWithObjects:@"Order Details:",@"Order Summary:",@"Delivery Address:",@"Payment Details:",nil];
    }
    else    {
        self.cellItemsArray=[NSMutableArray arrayWithObjects:@"Order Details:",@"Order Summary:",@"Pick Up Address:",@"Payment Details:",nil];
    }
    
    if (![OrderTypeString isEqualToString:kStringPickUp]) {
        orderSummaryItems=[NSMutableArray arrayWithObjects:@"Items Cost:",@"Delivery Charge:",@"Total Before Tax:",@"Tax:",@"Order Total:",nil];
    }
    else    {
        orderSummaryItems=[NSMutableArray arrayWithObjects:@"Items Cost:",@"Tax:",@"Order Total:",nil];
    }
    }
    else{
        self.cellItemsArray=[NSMutableArray arrayWithObjects:@"Order Details:",@"Order Summary:",@"Pick Up Address:",@"Payment Details:",nil];
        if([OrderTypeString isEqualToString:kStringDeliver])    {
            self.cellItemsArray=[NSMutableArray arrayWithObjects:@"Order Details:",@"Order Summary:",@"Delivery Address:",@"Payment Details:",nil];
        }
        else    {
            self.cellItemsArray=[NSMutableArray arrayWithObjects:@"Order Details:",@"Order Summary:",@"Pick Up Address:",@"Payment Details:",nil];
        }
        
        if (![OrderTypeString isEqualToString:kStringPickUp]) {
            orderSummaryItems=[NSMutableArray arrayWithObjects:@"Items Cost:",@"Discount",@"Delivery Charge:",@"Total Before Tax:",@"Tax:",@"Order Total:",nil];
        }
        else    {
            orderSummaryItems=[NSMutableArray arrayWithObjects:@"Items Cost:",@"Discount:",@"Tax:",@"Order Total:",nil];
        }

    }
    DLog(@"OrderTypeString---%@",OrderTypeString);
    
    [self AddDefaultTableView];
    [self.defaultTableView setDelegate:self ];
    [self.defaultTableView setDataSource:self];
    CGRect ContentainerViewFrame ;
    ContentainerViewFrame.origin.x=0;
    ContentainerViewFrame.origin.y=SCREEN_HEIGHT-kBottomViewHeight-kCustomButtonContainerHeight;
    ContentainerViewFrame.size.width=SCREEN_WIDTH;
    ContentainerViewFrame.size.height=kCustomButtonContainerHeight;
    self.returnToHomeButton=  [self AddCustomButtonInsideContentViewWithFrame:ContentainerViewFrame];
    [self.returnToHomeButton setTitle:kButtonTittleDONE forState:UIControlStateNormal];
    [self.returnToHomeButton addTarget:self action:@selector(returnToHomeButton:) forControlEvents:UIControlEventTouchUpInside];
    
    self.bottomBack.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark-TABLEVIEWDELEGATES
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView; {
    return 4;

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section    {
    if (section==0) {
        return [getCartArray count] + 2;
    }
    else    {
        return 2;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    if (indexPath.row==0) {
        return (kLabelHeight + 13);
    }
    else    {
        if (indexPath.section == 0) {
            
            return (kLabelHeight - 2);
        }
        else if((indexPath.section == 1) && [[[getDetailArray objectAtIndex:0]objectForKey:@"CouponCode"] isEqualToString:kEmptyString]) {
            if([OrderTypeString isEqualToString:kStringPickUp])
            {
                
               return (krowHeight - 27);
            }
            else
            {
                return (krowHeight - 2 + 20);
            }
            
        }
        else if((indexPath.section == 1) && (![[[getDetailArray objectAtIndex:0]objectForKey:@"CouponCode"] isEqualToString:kEmptyString])){
            if([OrderTypeString isEqualToString:kStringPickUp])
            {
                return (krowHeight -7);
            }
            else
            {
                return (krowHeight - 2 + 40);
            }
            
        }

        else if(indexPath.section == 3)   {
            if([getAddorderrespnseDict valueForKey:@"OrderNote"] &&
               (![[getAddorderrespnseDict valueForKey:@"OrderNote"] isEqualToString:kEmptyString])) {
                return (krowHeight - 28) + 65;
            }
            else    {
                return (krowHeight - 28);
            }
        }
        else    {
            return (krowHeight - 28 +10);
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell = nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if(indexPath.row == 0)  {
        if(indexPath.section != 0) {
            UIImageView *separator = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 1.7)];
            [separator setBackgroundColor:[self getColorForLightGreyColor]];
            [cell.contentView addSubview:separator];
        }
        
        UILabel *titleLabel=[self createLblWithRect:CGRectMake(15,kPaddingSmall + 10, SCREEN_WIDTH-(kLeftPadding*2), kLabelHeight) font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize] text:[_cellItemsArray objectAtIndex:indexPath.section] textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
        titleLabel.text=[_cellItemsArray objectAtIndex:indexPath.section];
        [cell addSubview:titleLabel];
        return cell;
    }
    
    if (indexPath.section == 0) {
        if (indexPath.row==1)   {
           UILabel*productNameLabel=[self createLblWithRect:
                                     CGRectMake(15, 0, kLabelWidth, kLabelHeight) font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize- 1.3] text:kLabelProductName textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
            
            UILabel*productSizeLabel=[self createLblWithRect:
                               CGRectMake(SCREEN_WIDTH - 15 - 50, 0, 50, kLabelHeight)
                                                 font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize- 1.3] text:kLabelPrice textAlignment:NSTextAlignmentRight textColor:[self getColorForBlackText]];
            
           UILabel*productPriceLabel=[self createLblWithRect:
                             CGRectMake(SCREEN_WIDTH - (15 + 50)*2, 0, 60, kLabelHeight)
                                               font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize- 1.3] text:kLabelQuantity textAlignment:NSTextAlignmentRight textColor:[self getColorForBlackText]];
            [cell addSubview:productNameLabel];
            [cell addSubview:productSizeLabel];
            [cell addSubview:productPriceLabel];
            return cell;
        }
        else    {
            Product *productObj=[getCartArray objectAtIndex:(indexPath.row - 2)];
            
            NSString *productPriceStr = [self addDollarSignToString:[NSString stringWithFormat:@"%.2f", [productObj.productPrice floatValue] * productObj.orderQuantity]];
            if([productObj.productSalesPrice floatValue] > 0)
                productPriceStr = [self addDollarSignToString:[NSString stringWithFormat:@"%.2f", [productObj.productSalesPrice floatValue] * productObj.orderQuantity]];

            UILabel*productPrice=[self createLblWithRect:
                                  CGRectMake(SCREEN_WIDTH - 15 - 52, 0, 52, kLabelHeight -2)
                                                    font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 1]
                                                    text:productPriceStr
                                           textAlignment:NSTextAlignmentRight
                                               textColor:[UIColor colorWithRed:0.4392 green:0.4392 blue:0.4431 alpha:1.0]];
    
            UILabel*productSize=[self createLblWithRect:
                                  CGRectMake(SCREEN_WIDTH - 15 - 52 - 40 - 10, 0, 40, kLabelHeight - 2)
                                                   font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 1.3]
                                                   text:[NSString stringWithFormat:@"%d", productObj.orderQuantity]
                                          textAlignment:NSTextAlignmentCenter
                                              textColor:[UIColor colorWithRed:0.4392 green:0.4392 blue:0.4431 alpha:1.0]];
    
            UILabel*productName=[self createLblWithRect:
                                    CGRectMake(15, 0, SCREEN_WIDTH - (SCREEN_WIDTH - (15 + 52)*2 - 40 - 15) - 5, kLabelHeight - 2)
                                                   font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 1.3]
                                                   text:productObj.productName
                                          textAlignment:NSTextAlignmentLeft
                                              textColor:[UIColor colorWithRed:0.4392 green:0.4392 blue:0.4431 alpha:1.0]];
            
            [cell addSubview:productName];
            [cell addSubview:productSize];
            [cell addSubview:productPrice];
            return cell;
        }
    }
    else    {
        switch (indexPath.section) {
            case 1:  {
                 if([[[getDetailArray objectAtIndex:0]objectForKey:@"CouponCode"] isEqualToString:kEmptyString]){
                     
                     if ([OrderTypeString isEqualToString:kStringPickUp])
                         orderSummaryValues = [[NSMutableArray alloc]initWithObjects:itemsCost_oc,tax_oc,orderTotal_oc,nil];
                     
                     else
                         orderSummaryValues = [[NSMutableArray alloc]initWithObjects:itemsCost_oc,deliveryCharge_oc,totalBeforeTax_oc,tax_oc,orderTotal_oc,nil];
                
                 for (int i=0; i<[orderSummaryItems count]; i++) {
                    UILabel *rightLabel=[self createLblWithRect:
                                         CGRectMake(15, (i*kLabelHeight)+kPaddingSmall, kLabelWidthOrderSummary, kLabelHeight - 3)
                                                           font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize- 1.3]
                                                           text:[orderSummaryItems objectAtIndex:i]
                                                  textAlignment:NSTextAlignmentLeft
                                                      textColor:[self getColorForBlackText]];
                    [cell addSubview:rightLabel];
                    UILabel *leftLabel=[self createLblWithRect:
                                        CGRectMake((rightLabel.frame.origin.x)+(rightLabel.frame.size.width) + 30,
                                                   (i*kLabelHeight)+kPaddingSmall, kLabelWidthOrderSummary, kLabelHeight - 3)
                                                          font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 1.3]
                                                          text: [self addDollarSignToString:[orderSummaryValues objectAtIndex:i]]
                                                 textAlignment:NSTextAlignmentRight
                                                     textColor:[self getColorForBlackText]];
                    [cell addSubview:leftLabel];

                }
                }
                 else{
                     if ([OrderTypeString isEqualToString:kStringPickUp])
                        
                         orderSummaryValues = [[NSMutableArray alloc]initWithObjects:itemsCost_oc,discount_oc,tax_oc,orderTotal_oc,nil];
                     else
                         orderSummaryValues=[[NSMutableArray alloc] initWithObjects:itemsCost_oc,discount_oc,deliveryCharge_oc,totalBeforeTax_oc,tax_oc,orderTotal_oc, nil];
                     
                     for (int i=0; i<[orderSummaryItems count]; i++) {
                         UILabel *rightLabel=[self createLblWithRect:
                                              CGRectMake(15, (i*kLabelHeight)+kPaddingSmall, kLabelWidthOrderSummary, kLabelHeight - 3)
                                                                font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize- 1.3]
                                                                text:[orderSummaryItems objectAtIndex:i]
                                                       textAlignment:NSTextAlignmentLeft
                                                           textColor:[self getColorForBlackText]];
                         [cell addSubview:rightLabel];
                         
                         
                         UILabel *leftLabel=[self createLblWithRect:
                                             CGRectMake((rightLabel.frame.origin.x)+(rightLabel.frame.size.width) + 30,
                                                        (i*kLabelHeight)+kPaddingSmall, kLabelWidthOrderSummary, kLabelHeight - 3)
                                                               font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 1.3]
                                                               text: [self addDollarSignToString:[orderSummaryValues objectAtIndex:i]]
                                                      textAlignment:NSTextAlignmentRight
                                                          textColor:[self getColorForBlackText]];
                         [cell addSubview:leftLabel];
                         
                     }

                 }
                return cell;
            }
            case 2:{
               if ( [OrderTypeString isEqualToString:kStringPickUp]) {
                               self.streetLabel=[self createLblWithRect:
                                  CGRectMake(15, kPaddingSmall, SCREEN_WIDTH-(15*2), kLabelHeight - 3)
                                                    font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 1.3]
                                                    text:kEmptyString
                                           textAlignment:NSTextAlignmentLeft
                                               textColor:[self getColorForBlackText]];
                [cell addSubview:self.streetLabel];
                
                
                self.StateLabel=[self createLblWithRect:
                                 CGRectMake(15, (self.streetLabel.frame.origin.y+self.streetLabel.frame.size.height), (SCREEN_WIDTH- 15)/2, kLabelHeight - 1.3)
                                                   font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 1.3]
                                                   text:kEmptyString
                                          textAlignment:NSTextAlignmentLeft
                                              textColor:[self getColorForBlackText]];
                [cell addSubview:self.StateLabel];
                
                UILabel * zipLbl=[self createLblWithRect:
                                  CGRectMake(15, (self.StateLabel.frame.origin.y+self.StateLabel.frame.size.height), (SCREEN_WIDTH- 15)/2, kLabelHeight - 1.3)
                                                    font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 1.3]
                                                    text:kEmptyString
                                           textAlignment:NSTextAlignmentLeft
                                               textColor:[self getColorForBlackText]];
                [cell addSubview:zipLbl];
                
               
                    __weak ClosestStoreModel* lClosestStoreModelObj;
                    __weak StoreDetails *storeDetails;
                    lClosestStoreModelObj = kGetClosestStoreModel;
                    
                    if (lClosestStoreModelObj!=nil) {
                        storeDetails = kGetStoreDetailsModel;
                        self.streetLabel.text=  storeDetails.StoreUserAddress1;
                        self.StateLabel.text=[NSString stringWithFormat:@"%@, %@ %@",storeDetails.StoreUserCity,storeDetails.StoreStateName,storeDetails.StoreUserZipCode];
                         storeDetails.StoreUserContact=[self getformattedPhoneNumberstringFrom:storeDetails.StoreUserContact];
                        zipLbl.text = [NSString stringWithFormat:@"%@", storeDetails.StoreUserContact];
                    }
                    else    {
                        [self callApiStoreDetails];
                    }
                }
                else    {
                    self.nameLabel=[self createLblWithRect:
                                    CGRectMake(15, kPaddingSmall, SCREEN_WIDTH-(15*2), kLabelHeight - 3)
                                                      font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 1.3]
                                                      text:kEmptyString
                                             textAlignment:NSTextAlignmentLeft
                                                 textColor:[self getColorForBlackText]];
                    [cell addSubview:self.nameLabel];
                    
                    
                    self.streetLabel=[self createLblWithRect:
                                      CGRectMake(15, (self.nameLabel.frame.origin.y+self.nameLabel.frame.size.height), SCREEN_WIDTH-(15*2), kLabelHeight - 3)
                                                        font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 1.3]
                                                        text:kEmptyString
                                               textAlignment:NSTextAlignmentLeft
                                                   textColor:[self getColorForBlackText]];
                    [cell addSubview:self.streetLabel];
                    
                    
                    self.StateLabel=[self createLblWithRect:
                                     CGRectMake(15, (self.streetLabel.frame.origin.y+self.streetLabel.frame.size.height), (SCREEN_WIDTH- 15)/2, kLabelHeight - 1.3)
                                                       font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 1.3]
                                                       text:kEmptyString
                                              textAlignment:NSTextAlignmentLeft
                                                  textColor:[self getColorForBlackText]];
                    [cell addSubview:self.StateLabel];
                    
                    UILabel * zipLbl=[self createLblWithRect:
                                      CGRectMake(15, (self.StateLabel.frame.origin.y+self.StateLabel.frame.size.height), (SCREEN_WIDTH- 15)/2, kLabelHeight - 1.3)
                                                        font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 1.3]
                                                        text:kEmptyString
                                               textAlignment:NSTextAlignmentLeft
                                                   textColor:[self getColorForBlackText]];
                    [cell addSubview:zipLbl];

                    //added delivery address
                    if(self.deliveryAddressDetails && self.deliveryAddressDetails.address && self.deliveryAddressDetails.address2 &&(![self.deliveryAddressDetails.address isEqualToString:kEmptyString])) {
                       
                        if(![self.deliveryAddressDetails.address2 isEqualToString:kEmptyString]){
                        NSString*address=[NSString stringWithFormat:@"%@, %@",deliveryAddressDetails.address,deliveryAddressDetails.address2];
                        self.streetLabel.text = address;
                        }
                        else{
                            NSString*address=[NSString stringWithFormat:@"%@",deliveryAddressDetails.address];
                            self.streetLabel.text = address;
                        }
                        self.nameLabel.text=[NSString stringWithFormat:@"%@ %@", deliveryAddressDetails.firstName, deliveryAddressDetails.lastName];
                        self.StateLabel.text=[NSString stringWithFormat:@"%@, %@ %@", deliveryAddressDetails.city, deliveryAddressDetails.state,deliveryAddressDetails.zip];
                       deliveryAddressDetails.phoneNumber=[self getformattedPhoneNumberstringFrom:deliveryAddressDetails.phoneNumber];
                       zipLbl.text = [NSString stringWithFormat:@"%@", deliveryAddressDetails.phoneNumber];
                    }
                    else    {
                        __weak UserProfile *userProfileObj = kgetUserProfileModel;
                        if (![[userProfileObj Address]isEqualToString:kEmptyString]) {
                             if(![[userProfileObj Address2]isEqualToString:kEmptyString]){
                                 
                             NSString*address=[NSString stringWithFormat:@"%@, %@",[userProfileObj Address],[userProfileObj Address2]];
                            self.streetLabel.text=address;
                             }
                             else{
                                 NSString*address=[NSString stringWithFormat:@"%@  %@",[userProfileObj Address],@""];
                                 self.streetLabel.text=address;
                             }
                        }
                        else    {
                            self.streetLabel.text=kEmptyAddress;
                        }
//                        if (![[userProfileObj Address2]isEqualToString:kEmptyApartment]) {
//                            self.apartmentLabel.text=[userProfileObj Address2];
//                        }
//                        else    {
//                            self.streetLabel.text=kEmptyAddress;
//                        }
                        if (![[userProfileObj City]isEqualToString:kEmptyString]) {
                            self.StateLabel.text=[NSString stringWithFormat:@"%@, %@ %@",[userProfileObj City],[userProfileObj State],[userProfileObj ZipCode]];
                            zipLbl.text = [NSString stringWithFormat:@"%@", [userProfileObj ContactNo]];
                           
                        }
                        else    {
                            self.StateLabel.text=kEmptyString;
                            zipLbl.text = kEmptyString;
                        }
                         if (![[userProfileObj City]isEqualToString:kEmptyString]) {
                              self.nameLabel.text=[NSString stringWithFormat:@"%@ %@", [userProfileObj FirstName], [userProfileObj LastName]];
                         }
                         else    {
                             self.nameLabel.text=kEmptyString;
                             
                         }
                    }
                }
                return cell;
            }
            case 3: {
                UILabel *statusLbl=[self createLblWithRect:
                                    CGRectMake(15, kPaddingSmall, kLabelWidthOrderSummary, kLabelHeight - 3)
                                                      font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize- 1.3]
                                                      text:kLabelStatus
                                             textAlignment:NSTextAlignmentLeft
                                                 textColor:[self getColorForBlackText]];
                
                UILabel *status=[self createLblWithRect:
                                 CGRectMake((statusLbl.frame.origin.x)+(statusLbl.frame.size.width) + 60,
                                            kPaddingSmall, kLabelWidthOrderSummary, kLabelHeight - 3)
                                                   font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 1.3]
                                                   text:kEmptytextfield
                                          textAlignment:NSTextAlignmentLeft
                                              textColor:[self getColorForBlackText]];
                
                if ([payTypeString isEqualToString:kStringPayAtStore]) {
                    [status setText: kLabelUnpaid];
                }
                else {
                    [status setText: kLabelPaid];
                }
                [cell addSubview:statusLbl];
                [cell addSubview:status];
                
                int y_Position = kPaddingSmall * 2 +  kLabelHeight - 3;
                
                if (([OrderTypeString isEqualToString:kStringPickUp] && [payTypeString isEqualToString:kStringPayNow])
                    || [OrderTypeString isEqualToString:kStringDeliver]) {
                    UILabel *confirmationLbl=[self createLblWithRect:
                                              CGRectMake(15, statusLbl.frame.size.height+statusLbl.frame.origin.y, kLabelWidth+50, kLabelHeight)
                                                                font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize- 1.3]
                                                                text:kLabelConfirmationNo
                                                       textAlignment:NSTextAlignmentLeft
                                                           textColor:[self getColorForBlackText]];
                    
                    UILabel *confirmation=[self createLblWithRect:
                                           CGRectMake((statusLbl.frame.origin.x)+(statusLbl.frame.size.width) + 60 , statusLbl.frame.size.height+statusLbl.frame.origin.y, kLabelWidthOrderSummary, kLabelHeight - 3)
                                                             font:[UIFont fontWithName:kFontRegular size:kFontRegularSize + 1.3]
                                                             text:getConfirmationNo
                                                    textAlignment:NSTextAlignmentLeft
                                                        textColor:[self getColorForBlackText]];
                    [cell addSubview:confirmationLbl];
                    [cell addSubview:confirmation];
                    
                    y_Position = statusLbl.frame.size.height+statusLbl.frame.origin.y + kLabelHeight;
                }
                
                UILabel *noteLbl=[self createLblWithRect:
                                          CGRectMake(15, y_Position - 3, 50, kLabelHeight)
                                                            font:[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize- 1.3]
                                                            text:kLabelNote
                                                   textAlignment:NSTextAlignmentLeft
                                                       textColor:[self getColorForBlackText]];
                
                UITextView * noteTxtView = [[UITextView alloc] initWithFrame:CGRectMake(15 + 32, y_Position + 2, SCREEN_WIDTH - 50 - 12, 70)];
                [noteTxtView setTextColor:[self getColorForBlackText]];
                [noteTxtView setFont:[UIFont fontWithName:kFontRegular size:kFontRegularSize]];
                [noteTxtView setTextAlignment:NSTextAlignmentLeft];
                [noteTxtView setEditable:NO];
                [noteTxtView setScrollEnabled:YES];
                [noteTxtView setUserInteractionEnabled:YES];
                [noteTxtView setDataDetectorTypes:UIDataDetectorTypeAll];
                noteTxtView.textAlignment = NSTextAlignmentJustified;
                noteTxtView.textContainer.lineBreakMode = NSLineBreakByCharWrapping;
                noteTxtView.textContainerInset = UIEdgeInsetsMake(-3, 0, 0, 0);
                
                if([getAddorderrespnseDict valueForKey:@"OrderNote"] &&
                   (![[getAddorderrespnseDict valueForKey:@"OrderNote"] isEqualToString:kEmptyString])) {
                    [noteTxtView setText:[getAddorderrespnseDict valueForKey:@"OrderNote"]];
                }
                else    {
                    [noteTxtView setText:kEmptytextfield];
                }
                
                [cell addSubview:noteTxtView];
                [cell addSubview:noteLbl];
                return cell;
            }
        }
    }
    return 0;
}

#pragma mark-BUTTON ACTION
-(void)returnToHomeButton:(id)sender{
    HomeViewController *vc=[self viewControllerWithIdentifier:kHomeViewControllerStoryboardId];
    [self.navigationController pushViewController:vc animated:YES];

}
@end