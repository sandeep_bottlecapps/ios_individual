//
//  LoyaltyCardViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 8/11/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "LoyaltyCardViewController.h"
#define  kLoyaltyCardWidth 200
#define  kLoyalCardHeight  70
#define  kStoreIconWidthHeight  (storeiconImg.size.height)
#define kFontTitle [UIFont fontWithName:kFontRegular size:kFontRegularSize-1]
#define kLabelTitleWidth                          (IS_IPHONE_4_OR_LESS? 120:IS_IPHONE_6?120:120)

@interface LoyaltyCardViewController (){

    UILabel *LoyalityCardNoLabel;
    UILabel *currentFSPointLabelTitle;
    UILabel *currentFSCreditLabelTitle;
    UILabel *pointsTillNextCreditLabelTitle;
    UIImageView *storeiconImageV;

}
@end

@implementation LoyaltyCardViewController
- (void)barcodeButtonClicked:(UIButton *)sender
{
   // [self performSegueWithIdentifier:kGoToBarCodeScannerViewControllerSegueId sender:nil];
}

-(void)upDateUI{
    __weak LoyaltyCard *LoyaltyCardObj = [[ModelManager modelManager] getLoyaltyCardModel];
    DLog(@"CurrentFSPoints-%@",LoyaltyCardObj.CurrentFSPoints);
    
        NSURL *loyaltycardImageUrl=[NSURL URLWithString:LoyaltyCardObj.UserLoyalityCardImage];
        // Update the UI
    _barcodeScannerImageView.tag=2003;

    if ([LoyaltyCardObj.UserLoyalityCardImage isEqualToString:kEmptyString ]) {
        [_barcodeScannerImageView setHidden:YES];
        storeiconImageV.imageURL=[NSURL URLWithString:LoyaltyCardObj.StoreImage];
    }
    else{
        [_barcodeScannerImageView setHidden:NO];

        _barcodeScannerImageView.imageURL=loyaltycardImageUrl;
        
        LoyalityCardNoLabel.text=[NSString stringWithFormat:@"%@",LoyaltyCardObj.UserLoyalityCardNo];
        
        self.currentFSPointLabel.text=[LoyaltyCardObj CurrentFSPoints];
      //  [self.currentFSPointLabel sizeToFit];
       // [self.currentFSPointLabel setTextAlignment:NSTextAlignmentRight];

        self.currentFSCreditLabel.text=[LoyaltyCardObj CurrentCreditDollars];
       // [self.currentFSCreditLabel sizeToFit];
       // [self.currentFSCreditLabel setTextAlignment:NSTextAlignmentRight];

        DLog(@"StoreImage--%@",LoyaltyCardObj.StoreImage);
        
        storeiconImageV.imageURL=[NSURL URLWithString:LoyaltyCardObj.StoreImage];
        self.pointsTillNextCreditLabel.text=[LoyaltyCardObj AmountCredit];
        //[self.pointsTillNextCreditLabel sizeToFit];
       // [self.pointsTillNextCreditLabel setTextAlignment:NSTextAlignmentRight];
    }
}

-(void)createUI{
                           // Update the UI
    UIImageView *bgImageView=[self createImageViewWithRect:CGRectMake(0, kNavigationheight+kPaddingSmall, SCREEN_WIDTH, SCREEN_HEIGHT/2) image:[UIImage imageNamed:kEmptyString]];
    [self.view addSubview:bgImageView];
    
    UIImageView *separatorIView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, ( bgImageView.frame.size.height)-2, SCREEN_WIDTH, 2)];
    [separatorIView2 setBackgroundColor:[self getColorForLightGreyColor]];
    [bgImageView addSubview:separatorIView2];
    
    UIImage *plaholderImage=[UIImage imageNamed:kImageLoyaltycardImage];
    UIImageView *placeholderImgV=[self createImageViewWithRect:CGRectMake(kRightPadding, kNavigationheight+kTopPadding+10, SCREEN_WIDTH-(kRightPadding*2), plaholderImage.size.height) image:plaholderImage];
    [placeholderImgV setBackgroundColor:[UIColor clearColor]];
    [placeholderImgV setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:placeholderImgV];
    
    /*  UIImageView  *headerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0,10,placeholderImgV.frame.size.width,30)];
    [headerImageView setImage:[UIImage imageNamed:@"Loyalty-header.png"]];
    [placeholderImgV addSubview:headerImageView];
    
    UIImage *storeiconImg=[UIImage imageNamed:kImageIcon];
    storeiconImageV=[self createAsynImageViewWithRect:CGRectMake((placeholderImgV.frame.size.width-kStoreIconWidthHeight)/2, kPaddingSmall*3, kStoreIconWidthHeight,kStoreIconWidthHeight) imageUrl:nil isThumnail:YES];
    [placeholderImgV addSubview:storeiconImageV]; */
    
    UILabel *storeNameLabel = [self createLblWithRect:CGRectMake(0, placeholderImgV.frame.origin.y+13.0, SCREEN_WIDTH, kLabelHeight) font:[self addFontSemiBold] text:kLoyaltyCardTitle textAlignment:NSTextAlignmentCenter textColor:[UIColor whiteColor]];
    [self.view addSubview:storeNameLabel];
    
    _barcodeScannerImageView =[self createAsynImageViewWithRect:CGRectMake(kPaddingSmall, storeiconImageV.frame.size.height+storeiconImageV.frame.origin.y+kPaddingSmall*7,placeholderImgV.frame.size.width-(kPaddingSmall*2),(placeholderImgV.frame.size.height)-(storeiconImageV.frame.size.height+kLabelHeight+50)) imageUrl:nil isThumnail:YES];
    [_barcodeScannerImageView setBackgroundColor:[UIColor clearColor]];
        [_barcodeScannerImageView setContentMode:UIViewContentModeScaleAspectFit];
    [placeholderImgV addSubview:_barcodeScannerImageView];
    
    
    UILabel *placeholderLabel=[self createLblWithRect:CGRectMake(kPaddingSmall,(_barcodeScannerImageView.frame.size.height+_barcodeScannerImageView.frame.origin.y)+kLoyaltyCardPlaceHolder, placeholderImgV.frame.size.width-(kPaddingSmall*2), kLabelHeight) font:[self addFontSemiBold] text:kNavigationTitleLOYALTYCARD textAlignment:NSTextAlignmentCenter textColor:[UIColor whiteColor]];
    [placeholderLabel setBackgroundColor:[UIColor clearColor]];
    [placeholderImgV addSubview:placeholderLabel];
    
    LoyalityCardNoLabel=[self createLblWithRect:CGRectMake(0, placeholderImgV.frame.origin.y+placeholderImgV.frame.size.height+kPaddingSmall, SCREEN_WIDTH, kLabelHeight) font:kFontTitle text:kEmptytextfield textAlignment:NSTextAlignmentCenter textColor:[self getColorForRedText]];
    [self.view addSubview:LoyalityCardNoLabel];
    

    currentFSPointLabelTitle=[self createLblWithRect:CGRectMake(kLeftPadding, bgImageView.frame.origin.y+bgImageView.frame.size.height, kLabelTitleWidth, kTEXTfieldHeight) font:kFontTitle text:@"Current FS Points:" textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
     [currentFSPointLabelTitle setBackgroundColor:[UIColor clearColor]];

    [self.view addSubview:currentFSPointLabelTitle];

    self.currentFSPointLabel=[self createLblWithRect:CGRectMake(((currentFSPointLabelTitle.frame.size.width)+(currentFSPointLabelTitle.frame.origin.x)), bgImageView.frame.origin.y+bgImageView.frame.size.height, kLabelWidth, kTEXTfieldHeight) font:[UIFont fontWithName:kFontRegular size:kFontRegularSize] text:kEmptytextfield textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
    [_currentFSPointLabel setBackgroundColor:[UIColor clearColor]];

    [self.view addSubview:self.currentFSPointLabel];

    currentFSCreditLabelTitle=[self createLblWithRect:CGRectMake(kLeftPadding, _currentFSPointLabel.frame.origin.y+_currentFSPointLabel.frame.size.height, kLabelTitleWidth, kTEXTfieldHeight) font:kFontTitle text:@"Current FS Credits:" textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
    [self.view addSubview:currentFSCreditLabelTitle];

    self.currentFSCreditLabel=[self createLblWithRect:CGRectMake(((currentFSCreditLabelTitle.frame.size.width)+(currentFSCreditLabelTitle.frame.origin.x)), _currentFSPointLabel.frame.origin.y+_currentFSPointLabel.frame.size.height , kLabelWidth, kTEXTfieldHeight) font:[UIFont fontWithName:kFontRegular size:kFontRegularSize] text:kEmptytextfield textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
    [self.view addSubview:self.currentFSCreditLabel];

    pointsTillNextCreditLabelTitle=[self createLblWithRect:CGRectMake(kLeftPadding, _currentFSCreditLabel.frame.origin.y+_currentFSCreditLabel.frame.size.height, kLabelTitleWidth+20, kTEXTfieldHeight) font:kFontTitle text:@"Points till next credit:" textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
    [self.view addSubview:pointsTillNextCreditLabelTitle];

    self.pointsTillNextCreditLabel=[self createLblWithRect:CGRectMake(((pointsTillNextCreditLabelTitle.frame.size.width)+(pointsTillNextCreditLabelTitle.frame.origin.x)-10.0), _currentFSCreditLabel.frame.origin.y+_currentFSCreditLabel.frame.size.height, kLabelWidth, kTEXTfieldHeight) font:[UIFont fontWithName:kFontRegular size:kFontRegularSize] text:kEmptytextfield textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
    [self.view addSubview:self.pointsTillNextCreditLabel];
}

-(void)callApiGetLoyalityCardImage{
    __weak User *userObj = kGetUserModel;
    
    NSMutableDictionary* paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [paramDict setObject:userObj.userId forKey: kParamUserId];
    //
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] GetLoyalityCardImageWithParam:paramDict successStatus:^(BOOL status) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            [self upDateUI];

            
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    [self refreshPage:nil];
}

-(void)refreshPage:(id)sender{
    [self callApiGetLoyalityCardImage];
  }
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshPage:) name:kLoyaltyCardViewControllerStoryboardId object:nil];

    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
