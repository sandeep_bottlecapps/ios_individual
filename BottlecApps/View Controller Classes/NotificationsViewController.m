//
//  NotificationsViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 8/11/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "NotificationsViewController.h"
#define  kNotificatinButtonWidth  68
#define  kNotificatinButtonHeight 68
#import "HomeViewController.h"

#define kBeerButtonWidthHeight              (IS_IPHONE_4_OR_LESS? 100:IS_IPHONE_6?100:100)
#define knotificationPadding               (SCREEN_WIDTH-200)/2

@interface NotificationsViewController (){
    NSMutableArray *productCategoryArray;
    NSMutableArray *productSubCategoryArray;
    NSMutableArray  *subCatSelectionArray;
    NSInteger       subCatagoryIndex;
    int       selectedEventIndex;
    NSMutableArray  *dropDownArray;
    BOOL         isEvents;
    NSMutableArray  *productTypeArray;
    NSMutableArray  *postEventArray, *postItemArray,*eventTypeArray;
}
@property (strong, nonatomic) IBOutlet UIView *MainContainer;

@end

@implementation NotificationsViewController
//@synthesize eventTypeArray;

-(void)initializeAllArrayAndDict{
    
    dropDownArray = [[NSMutableArray alloc]init];
    productCategoryArray = [[NSMutableArray alloc]init];
    productSubCategoryArray = [[NSMutableArray alloc]init];
    productTypeArray = [[NSMutableArray alloc]init];
    subCatSelectionArray =  [[NSMutableArray alloc]init];
    postEventArray = [[NSMutableArray alloc]init];
    postItemArray = [[NSMutableArray alloc]init];
   // NSLog(@"post array is :%@",postEventArray);
   // NSLog(@"postItemArray :%@",postItemArray);
    eventTypeArray=[NSMutableArray new];
    
}

-(void)createUI{
    [self addScrollView];
    [self.CustomScrollView setDelegate:self];
    
    [self.CustomScrollView setContentSize:CGSizeMake(self.CustomScrollView.frame.size.width, self.CustomScrollView.frame.size.height)];
    _DescriptionLabel=[self createLblWithRect:CGRectMake(kPaddingSmall,kTopPadding, SCREEN_WIDTH-(kPaddingSmall*2), kLabelHeight*2) font:[self addFontRegular] text:kLabelNotificationDesc textAlignment:NSTextAlignmentCenter textColor:[self getColorForBlackText]];
    _DescriptionLabel .numberOfLines=2;
    [self.CustomScrollView addSubview:_DescriptionLabel];
    [_DescriptionLabel setBackgroundColor:[UIColor clearColor]];
    [self.CustomScrollView setBackgroundColor:[UIColor clearColor]];
    
    _BeerButton =[self createButtonWithFrame:CGRectMake(knotificationPadding, _DescriptionLabel.frame.size.height+_DescriptionLabel.frame.origin.y+kPaddingSmall
                                                        , kBeerButtonWidthHeight, kBeerButtonWidthHeight) withTitle:nil];
    [_BeerButton setImage:[UIImage imageNamed:kImageBeerSelected] forState:UIControlStateSelected];
    [_BeerButton setImage:[UIImage imageNamed:kImageBeerUnselected] forState:UIControlStateNormal];
    [self.CustomScrollView addSubview:_BeerButton];
    [_BeerButton setTag:1];
    [_BeerButton addTarget:self action:@selector(BeerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    _BeerLabel=[self createLblWithRect:CGRectMake(knotificationPadding, _BeerButton.frame.size.height+_BeerButton.frame.origin.y, kBeerButtonWidthHeight, kLabelHeightSmall) font:[self addFontSemiBold] text:@"BEER" textAlignment:NSTextAlignmentCenter textColor:[self getColorForBlackText]];
    [self.CustomScrollView addSubview:_BeerLabel];
    
    _LiquorButton=[self createButtonWithFrame:CGRectMake(knotificationPadding, _BeerLabel.frame.size.height+_BeerLabel.frame.origin.y
                                                         , kBeerButtonWidthHeight, kBeerButtonWidthHeight) withTitle:nil];
    [_LiquorButton setTag:3];
    [_LiquorButton addTarget:self action:@selector(liquorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [_LiquorButton setImage:[UIImage imageNamed:kImageLiquorSelected] forState:UIControlStateSelected];
    [_LiquorButton setImage:[UIImage imageNamed:kImageLiquorUnselected] forState:UIControlStateNormal];
    [self.CustomScrollView addSubview:_LiquorButton];
    _LiquorLabel=[self createLblWithRect:CGRectMake(knotificationPadding, _LiquorButton.frame.size.height+_LiquorButton.frame.origin.y, kBeerButtonWidthHeight, kLabelHeightSmall) font:[self addFontSemiBold] text:@"LIQUOR" textAlignment:NSTextAlignmentCenter textColor:[self getColorForBlackText]];
    [self.CustomScrollView addSubview:_LiquorLabel];
    
    
    _WineButton =[self createButtonWithFrame:CGRectMake(_BeerButton.frame.size.width+_BeerButton.frame.origin.x, _DescriptionLabel.frame.size.height+_DescriptionLabel.frame.origin.y+kPaddingSmall
                                                        , kBeerButtonWidthHeight, kBeerButtonWidthHeight) withTitle:nil];
    [_WineButton setTag:2];
    [_WineButton addTarget:self action:@selector(WineButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [_WineButton setImage:[UIImage imageNamed:kImageWineSelected] forState:UIControlStateSelected];
    [_WineButton setImage:[UIImage imageNamed:kImageWineUnselected] forState:UIControlStateNormal];
    [self.CustomScrollView addSubview:_WineButton];
    _WineLabel=[self createLblWithRect:CGRectMake(_BeerButton.frame.size.width+_BeerButton.frame.origin.x, _BeerButton.frame.size.height+_BeerButton.frame.origin.y, kBeerButtonWidthHeight, kLabelHeightSmall) font:[self addFontSemiBold] text:@"WINE" textAlignment:NSTextAlignmentCenter textColor:[self getColorForBlackText]];
    [self.CustomScrollView addSubview:_WineLabel];
    
    
    
    _EventsButton =[self createButtonWithFrame:CGRectMake(_BeerButton.frame.size.width+_BeerButton.frame.origin.x, _WineLabel.frame.size.height+_WineLabel.frame.origin.y
                                                          , kBeerButtonWidthHeight, kBeerButtonWidthHeight) withTitle:nil];
    [_EventsButton setTag:4];
    [_EventsButton addTarget:self action:@selector(eventsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [_EventsButton setImage:[UIImage imageNamed:kImageEventsSelected] forState:UIControlStateSelected];
    [_EventsButton setImage:[UIImage imageNamed:kImageEventsUnselected] forState:UIControlStateNormal];
    [self.CustomScrollView addSubview:_EventsButton];
    _EventsLabel=[self createLblWithRect:CGRectMake(_BeerButton.frame.size.width+_BeerButton.frame.origin.x, _LiquorButton.frame.size.height+_LiquorButton.frame.origin.y, kBeerButtonWidthHeight, kLabelHeightSmall) font:[self addFontSemiBold] text:@"EVENTS" textAlignment:NSTextAlignmentCenter textColor:[self getColorForBlackText]];
    [self.CustomScrollView addSubview:_EventsLabel];
    
    UIImage *saveImage=[UIImage imageNamed:kImageSaveButton];
    
    _saveButton =(IS_IPHONE_4_OR_LESS? [self createButtonWithFrame:CGRectMake((SCREEN_WIDTH-(saveImage.size.width))/2, _EventsLabel.frame.size.height+_EventsLabel.frame.origin.y+(40), saveImage.size.width, saveImage.size.height) withTitle:nil]:IS_IPHONE_6?[self createButtonWithFrame:CGRectMake((SCREEN_WIDTH-(saveImage.size.width))/2, _EventsLabel.frame.size.height+_EventsLabel.frame.origin.y+(100), saveImage.size.width, saveImage.size.height) withTitle:nil]:[self createButtonWithFrame:CGRectMake((SCREEN_WIDTH-(saveImage.size.width))/2, _EventsLabel.frame.size.height+_EventsLabel.frame.origin.y+(100), saveImage.size.width, saveImage.size.height) withTitle:nil]);
    
    
    
    
    //    [self createButtonWithFrame:CGRectMake((SCREEN_WIDTH-(saveImage.size.width))/2, _EventsLabel.frame.size.height+_EventsLabel.frame.origin.y+(100), saveImage.size.width, saveImage.size.height) withTitle:nil];
    [_saveButton setImage:saveImage forState:UIControlStateNormal];
    [_saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.CustomScrollView addSubview:_saveButton];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    isEvents=NO;
    [self initializeAllArrayAndDict];
    for (int i = 1; i < 4; i++)
    {
        NSMutableDictionary *dict1 = [[NSMutableDictionary alloc]init];
        [dict1 setValue:[NSString stringWithFormat:@"%d",i] forKey:@"EventId"];
        [postEventArray addObject:dict1];
    }
    
    [self CallApiGetSelectedProductCategory];
}
-(void)CallApiGetSelectedProductCategory{
    User *userObj=kGetUserModel;
    NSMutableDictionary *paramDict=[[NSMutableDictionary alloc] init];
    [paramDict setValue:kSTOREIDbottlecappsString forKey:kParamStoreId];
    [paramDict setValue:userObj.userId forKey:kParamUserId];
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] GetSelectedProductCategoryWithParam:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
       // NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"key");
            [self hideSpinner];
            NSDictionary *dictLocal = [[NSDictionary alloc]initWithDictionary:response];
            NSLog(@"key is %@", dictLocal);
            NSString *eventSelectionValue = [dictLocal valueForKey:@"IsEventSelected"];
           // NSLog(@"event selection value is %@", eventSelectionValue);
            
            NSMutableArray *categoryArr = [[NSMutableArray alloc] init];
            NSMutableArray *eventsAndPromotionArr = [[NSMutableArray alloc] initWithArray:[dictLocal objectForKey:@"ListEventPromotion"] copyItems:YES];
            
            [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%@",eventSelectionValue] forKey:EVENTSELECTED];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            for (int k = 0;k< [[dictLocal valueForKey:@"ProductCategories"] count]; k++)
            {
                [categoryArr addObject:[[dictLocal objectForKey:@"ProductCategories"] objectAtIndex:k]];
            }
            
            NSLog(@"event and promption array is %@", eventsAndPromotionArr);
            
            
          //  NSMutableArray *arr = [[NSMutableArray alloc]initWithArray:categoryArr];
            //    [categoryArr setValue:[key valueForKey:@"ProductCategories"] forKey:@"cat"];
            //  categoryDict = [key valueForKey:@"ProductCategories"];
          //  NSLog(@"category dict is %@", arr);
          //  NSLog(@"count-----%lu",(unsigned long)[categoryArr count]);
            
            for (int i = 0; i < [categoryArr count]; i++)
            {
                category *catObj = [[category alloc]init];
                catObj.categoryId =[categoryArr[i] valueForKey:@"CatId"];
                catObj.categoryName =[categoryArr[i] valueForKey:@"CatName"];
                catObj.subCategoryArray = [[NSMutableArray alloc]init];
                NSMutableArray *arrayLocal = [[NSMutableArray alloc]initWithArray:[categoryArr[i] valueForKey:@"SubCategory"]];
                for (int j=0; j<[arrayLocal count]; j++)
                {
                    NSMutableDictionary *localDict = [[NSMutableDictionary alloc]init];
                    [localDict setValue:[[arrayLocal objectAtIndex:j] valueForKey:@"SubCatId"] forKey:@"CatId"];
                    [localDict setValue:[[arrayLocal objectAtIndex:j] valueForKey:@"SubCatName"] forKey:@"CatName"];
                    [localDict setValue:[[arrayLocal objectAtIndex:j] valueForKey:@"IsSelected"] forKey:@"catSelection"];
                    [catObj.subCategoryArray addObject:localDict];
                }
                NSLog(@"category obj array is %@", catObj.subCategoryArray);
                
                [productTypeArray addObject:catObj];
                
            }
            for (int l = 0; l< [eventsAndPromotionArr count]; l++)
            {
                AddOptionEvent *eventObj = [[AddOptionEvent alloc]init];
                NSLog(@"value is %@",[eventsAndPromotionArr[l] valueForKey:@"IsSelected"]);
                eventObj.eventName = [eventsAndPromotionArr[l] valueForKey:@"EventName"];
                eventObj.eventId = [eventsAndPromotionArr[l] valueForKey:@"EventId"];
                eventObj.isSelected = [eventsAndPromotionArr[l] valueForKey:@"IsSelected"];
                [eventTypeArray addObject:eventObj];
            }
            NSLog(@"product category array count is %lu",(unsigned long)[productTypeArray count]);
            DLog(@"productTypeArray0=========%@",[[productTypeArray objectAtIndex:0] subCategoryArray]);
            DLog(@"productTypeArray1=========%@",[[productTypeArray objectAtIndex:1] subCategoryArray]);
            DLog(@"productTypeArray2=========%@",[[productTypeArray objectAtIndex:2] subCategoryArray]);
            
            
            NSLog(@"event array is %@", eventTypeArray);
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton*)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)addSubcategogyToBeer{
    [subCatSelectionArray removeAllObjects];
    NSLog(@"%lu",(unsigned long)[[[productTypeArray objectAtIndex:2] subCategoryArray] count]);
    NSMutableArray *arr = [[NSMutableArray alloc]initWithArray:[[productTypeArray objectAtIndex:2] subCategoryArray]];
    for (int i = 0; i<[[[productTypeArray objectAtIndex:2] subCategoryArray] count]; i++)
    {
        [dropDownArray addObject:[arr objectAtIndex:i]];
        [subCatSelectionArray addObject:[[arr objectAtIndex:i]valueForKey:@"catSelection"]];
    }
}
-(void)addSubcategogyToLiquor{
    [subCatSelectionArray removeAllObjects];
    NSMutableArray *arr = [[NSMutableArray alloc]initWithArray:[[productTypeArray objectAtIndex:1] subCategoryArray]];
    for (int i = 0; i<[[[productTypeArray objectAtIndex:1] subCategoryArray] count]; i++)
    {
        [dropDownArray addObject:[arr objectAtIndex:i]];
        [subCatSelectionArray addObject:[[arr objectAtIndex:i]valueForKey:@"catSelection"]];
        NSLog(@"%@",[dropDownArray[0] valueForKey:@"SubCatName"]);
    }
}
-(void)addSubcategogyToWine{
    [subCatSelectionArray removeAllObjects];
    NSLog(@"%lu",(unsigned long)[[[productTypeArray objectAtIndex:0] subCategoryArray] count]);
    NSMutableArray *arr = [[NSMutableArray alloc]initWithArray:[[productTypeArray objectAtIndex:0] subCategoryArray]];
    for (int i = 0; i<[[[productTypeArray objectAtIndex:0] subCategoryArray] count]; i++)
    {
        [dropDownArray addObject:[arr objectAtIndex:i]];
        [subCatSelectionArray addObject:[[arr objectAtIndex:i]valueForKey:@"catSelection"]];
    }
}


#pragma mark-BUTTONACTION
- (void)BeerButtonClicked:(UIButton*)sender {
    isEvents=NO;
    selectedEventIndex = (int)[sender tag];
    [sender setSelected:NO];
    if (![sender isSelected]) {
        [sender setSelected:YES];
        [sender setImage:[UIImage imageNamed:kImageBeerSelected] forState:UIControlStateNormal];
        [_BeerLabel setTextColor:[self getColorForRedText]];
        [dropDownArray removeAllObjects];
        [self addSubcategogyToBeer];
        [self addTrantParentviewWithArray:dropDownArray ];
    }
    else{
        [sender setSelected:NO];
        [sender setImage:[UIImage imageNamed:kImageBeerUnselected] forState:UIControlStateNormal];
        [_BeerLabel setTextColor:[self getColorForBlackText]];
        
        
    }
}

- (void)WineButtonClicked:(UIButton*)sender {
    isEvents=NO;
    selectedEventIndex = (int)[sender tag];
    [sender setSelected:NO];
    if (![sender isSelected]) {
        [sender setSelected:YES];
        [sender setImage:[UIImage imageNamed:kImageWineSelected] forState:UIControlStateNormal];
        [_WineLabel setTextColor:[self getColorForRedText]];
        [dropDownArray removeAllObjects];
        [self addSubcategogyToWine];
        [self addTrantParentviewWithArray:dropDownArray];
        
        
    }
    else{
        [sender setSelected:NO];
        [sender setImage:[UIImage imageNamed:kImageWineUnselected] forState:UIControlStateNormal];
        [_WineLabel setTextColor:[self getColorForBlackText]];
        
    }
    
    
}

- (void)liquorButtonClicked:(UIButton*)sender {
    isEvents=NO;
    selectedEventIndex = (int)[sender tag];
    [sender setSelected:NO];
    if (![sender isSelected]) {
        [sender setSelected:YES];
        [sender setImage:[UIImage imageNamed:kImageLiquorSelected] forState:UIControlStateNormal];
        [_LiquorLabel setTextColor:[self getColorForRedText]];
        [dropDownArray removeAllObjects];
        [self addSubcategogyToLiquor];
        [self addTrantParentviewWithArray:dropDownArray];
        
        
    }
    else{
        [sender setSelected:NO];
        [sender setImage:[UIImage imageNamed:kImageLiquorUnselected] forState:UIControlStateNormal];
        [_LiquorLabel setTextColor:[self getColorForBlackText]];
        
        
    }
    
}

- (void)eventsButtonClicked:(UIButton*)sender {
    selectedEventIndex = (int)[sender tag];
    [sender setSelected:NO];
    if (![sender isSelected]) {
        [sender setSelected:YES];
        [sender setImage:[UIImage imageNamed:kImageEventsSelected] forState:UIControlStateNormal];
        [_EventsLabel setTextColor:[self getColorForRedText]];
        [dropDownArray removeAllObjects];
        [dropDownArray addObject:@"Notify Events"];
        isEvents=YES;
        [self addTrantParentviewWithArray:dropDownArray];
        
        
    }
    else{
        isEvents=NO;
        [sender setSelected:NO];
        [sender setImage:[UIImage imageNamed:kImageEventsUnselected] forState:UIControlStateNormal];
        [_EventsLabel setTextColor:[self getColorForBlackText]];
        
        
    }
    
    
}

-(void)CallapiInsertNotificationSubscription:(NSMutableArray *)eventArr storeID:(NSString *)store_ID itemsArray:(NSMutableArray *)itemArr userID:(NSString *)user_ID isEventSelected:(NSString *)event_Selected{
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    [postDict setValue:event_Selected forKey:@"IsEventSelected"];
    [postDict setValue:kSTOREIDbottlecappsString forKey:@"StoreId"];
    [postDict setValue:itemArr forKey:@"items"];
    [postDict setValue:user_ID forKey:@"UserId"];
    NSLog(@"post dict %@",[postDict valueForKey:@"Events"]);
    NSArray *array=[NSArray arrayWithObject:postDict];
    DLog(@"array--%@",array);
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        DLog(@"jsonString--%@",jsonString);
        [self showSpinner]; // To show spinner
        [[ModelManager modelManager] InsertNotificationSubscriptionJsonstring:jsonString  successStatus:^(APIResponseStatusCode statusCode, id response) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self hideSpinner];
                NSLog(@"%@", response);
                [self GoBack];
                
            });
        } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
            [self hideSpinner];
            [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
            
            
        }];
        
        
    }
    
    
    
    
}

-(void)saveButtonClicked:(UIButton*)sender{
    User *userObj=kGetUserModel;
    NSString *userID=userObj.userId;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *storeId = [prefs stringForKey:@"StoreID"];
    NSString *eventSelectionString = [[NSUserDefaults standardUserDefaults]valueForKey:EVENTSELECTED];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    for (int k = 0; k<[[[productTypeArray objectAtIndex:0] subCategoryArray] count]; k++)
    {
        if ([[[[[productTypeArray objectAtIndex:0] subCategoryArray] objectAtIndex:k] valueForKey:@"catSelection"] isEqualToString:@"True"])
        {//wine=3
            NSMutableDictionary *dict2 = [[NSMutableDictionary alloc]init];
            [dict2 setValue:[[[[productTypeArray objectAtIndex:0] subCategoryArray] objectAtIndex:k] valueForKey:@"CatId"] forKey:@"SubCatId"];
            [dict2 setValue:[NSString stringWithFormat:@"%d",3] forKey:@"CatId"];
            [postItemArray addObject:dict2];
        }
    }
    for (int k = 0; k<[[[productTypeArray objectAtIndex:1] subCategoryArray] count]; k++)
    {//liquor=2
        if ([[[[[productTypeArray objectAtIndex:1] subCategoryArray] objectAtIndex:k] valueForKey:@"catSelection"] isEqualToString:@"True"])
        {
            NSMutableDictionary *dict2 = [[NSMutableDictionary alloc]init];
            [dict2 setValue:[[[[productTypeArray objectAtIndex:1] subCategoryArray] objectAtIndex:k] valueForKey:@"CatId"] forKey:@"SubCatId"];
            [dict2 setValue:[NSString stringWithFormat:@"%d",2] forKey:@"CatId"];
            [postItemArray addObject:dict2];
        }
    }
    for (int k = 0; k<[[[productTypeArray objectAtIndex:2] subCategoryArray] count]; k++)
    {//beer=1
        if ([[[[[productTypeArray objectAtIndex:2] subCategoryArray] objectAtIndex:k] valueForKey:@"catSelection"] isEqualToString:@"True"])
        {
            NSMutableDictionary *dict2 = [[NSMutableDictionary alloc]init];
            [dict2 setValue:[[[[productTypeArray objectAtIndex:2] subCategoryArray] objectAtIndex:k] valueForKey:@"CatId"] forKey:@"SubCatId"];
            [dict2 setValue:[NSString stringWithFormat:@"%d",1] forKey:@"CatId"];
            [postItemArray addObject:dict2];
        }
    }
    
    [self CallapiInsertNotificationSubscription:postEventArray storeID:storeId itemsArray:postItemArray userID:userID isEventSelected:eventSelectionString];
    
    
    
    
}

#pragma mark-self.transparentView
-(void)SubCatagoryButtonClicked:(UIActionButton*)sender{
    NSLog(@"cell tag is %ld", (long)[sender tag]);
    //UITableViewCell *cell = (UITableViewCell *)sender;
    
    switch (selectedEventIndex)
    {
        case 1://beer
            if ([sender isSelected])
            {
                [sender setSelected:NO];
                [sender setBackgroundColor:[UIColor clearColor]];
                [sender setTitleColor:[self getColorForBlackText] forState:UIControlStateNormal];
                
                [[[[productTypeArray objectAtIndex:2] subCategoryArray] objectAtIndex:[sender tag]-200] setValue:@"False" forKey:@"catSelection"];
                DLog(@"====%@",[[[productTypeArray objectAtIndex:2] subCategoryArray] objectAtIndex:[sender tag]-200]);
                
                
            }
            else
            {
                [sender setSelected:YES];
                [sender setBackgroundColor:[self getColorForRedText]];
                [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                
                [[[[productTypeArray objectAtIndex:2] subCategoryArray] objectAtIndex:[sender tag]-200] setValue:@"True" forKey:@"catSelection"];
                DLog(@"====%@",[[[productTypeArray objectAtIndex:2] subCategoryArray] objectAtIndex:[sender tag]-200]);
                
                
            }
            
            break;
        case 2://wine
            if ([sender isSelected])
            {
                [sender setSelected:NO];
                [sender setBackgroundColor:[UIColor clearColor]];
                [sender setTitleColor:[self getColorForBlackText] forState:UIControlStateNormal];
                
                
                [[[[productTypeArray objectAtIndex:0] subCategoryArray] objectAtIndex:[sender tag]-200] setValue:@"False" forKey:@"catSelection"];
                DLog(@"====%@",[[[productTypeArray objectAtIndex:0] subCategoryArray] objectAtIndex:[sender tag]-200]);
                
            }
            else
            {
                [sender setSelected:YES];
                [sender setBackgroundColor:[self getColorForRedText]];
                [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                [[[[productTypeArray objectAtIndex:0] subCategoryArray] objectAtIndex:[sender tag]-200] setValue:@"True" forKey:@"catSelection"];
                DLog(@"====%@",[[[productTypeArray objectAtIndex:0] subCategoryArray] objectAtIndex:[sender tag]-200]);
                
                
            }
            break;
        case 3://liquor
            if ([sender isSelected])
            {
                [sender setSelected:NO];
                [sender setBackgroundColor:[UIColor clearColor]];
                [sender setTitleColor:[self getColorForBlackText] forState:UIControlStateNormal];
                [[[[productTypeArray objectAtIndex:1] subCategoryArray] objectAtIndex:[sender tag]-200] setValue:@"False" forKey:@"catSelection"];
                DLog(@"====%@",[[[productTypeArray objectAtIndex:1] subCategoryArray] objectAtIndex:[sender tag]-200]);
                
            }
            else
            {
                [sender setSelected:YES];
                [sender setBackgroundColor:[self getColorForRedText]];
                [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                [[[[productTypeArray objectAtIndex:1] subCategoryArray] objectAtIndex:[sender tag]-200] setValue:@"True" forKey:@"catSelection"];
                DLog(@"====%@",[[[productTypeArray objectAtIndex:1] subCategoryArray] objectAtIndex:[sender tag]-200]);
                
            }
            break;
        case 4://events
            if ([sender isSelected])
            {
                [sender setSelected:NO];
                [sender setBackgroundColor:[UIColor clearColor]];
                [sender setTitleColor:[self getColorForBlackText] forState:UIControlStateNormal];
                [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:EVENTSELECTED];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
            }
            else
            {
                [sender setSelected:YES];
                [sender setBackgroundColor:[self getColorForRedText]];
                [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                
                
                [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:EVENTSELECTED];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            break;
            
        default:
            break;
    }
}
-(void)closeClicked:(UIActionButton*)sender{
    [self.transparentView removeFromSuperview];
}

- (void)closeFilterTapRecognizer:(UITapGestureRecognizer*)sender {
    UIView *view = sender.view;
    NSLog(@"%ld", (long)view.tag);//By tag, you can find out where you had tapped.
    [self.transparentView removeFromSuperview];
}

-(void)addTrantParentviewWithArray:(NSArray*)array{
    DLog(@"array---%@",array);
    NSLog(@"drop down array count is %lu", (unsigned long)[dropDownArray count]);
    
    self.transparentView=[self createContentViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self.transparentView setBackgroundColor:[UIColor clearColor]];
    UITapGestureRecognizer *closeFilterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeFilterTapRecognizer:)];
    [self.view addSubview:self.transparentView];
    [_transparentView addGestureRecognizer:closeFilterTapRecognizer];
    
    UIImageView *transparentImageView=[self createImageViewWithRect:CGRectMake(0, 0, SCREEN_WIDTH
                                                                               , SCREEN_HEIGHT) image:nil];
    [self.transparentView addSubview:transparentImageView];
    [transparentImageView setBackgroundColor:[UIColor lightGrayColor]];
    [transparentImageView setAlpha:.75];
    
    if ([array count]<6) {
        self.containerView=[self createContentViewWithFrame:CGRectMake(kPaddingSmall, kNavigationheight+kBottomViewHeight+kTopPadding,SCREEN_WIDTH-(kPaddingSmall*2),(kButtonHeight*([array count]+1))+(([array count]+1)*5))];
        self.CustomScrollView=[self createScrollViewWithFrame:CGRectMake(0,0,self.containerView.frame.size.width,self.containerView.frame.size.height-kButtonHeight)];
        [self.CustomScrollView setContentSize:CGSizeMake(kcustomButtonwidth, kCustomScrollViewHeight)];
        
    }
    else{
        self.containerView=[self createContentViewWithFrame:CGRectMake(kPaddingSmall, kNavigationheight+kBottomViewHeight+kTopPadding,SCREEN_WIDTH-(kPaddingSmall*2),(kButtonHeight*8))];
        self.CustomScrollView=[self createScrollViewWithFrame:CGRectMake(0,0,self.containerView.frame.size.width,self.containerView.frame.size.height-kButtonHeight)];
        [self.CustomScrollView setContentSize:CGSizeMake(kcustomButtonwidth, kCustomScrollViewHeight+250)];
        
        
    }
    [self.containerView setBackgroundColor:[UIColor whiteColor]];
    [self.transparentView addSubview: self.containerView];
    [self.CustomScrollView setBackgroundColor:[UIColor clearColor]];
    [self.containerView addSubview:self.CustomScrollView];
    UIButton *closeButton=[self createButtonWithFrame:CGRectMake(0,  self.CustomScrollView.frame.size.height+ self.CustomScrollView.frame.origin.y, self.containerView.frame.size.width, kButtonHeight) withTitle:kButtonTitleClose];
    
    
    [closeButton setBackgroundColor:[UIColor lightGrayColor]];
    [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:closeButton];
    CGFloat marginX = 0;
    CGFloat marginY = kPaddingSmall+kButtonHeight;
    CGRect buttonFrame = CGRectMake(0, kPaddingSmall, self.containerView.frame.size.width,kButtonHeight);
    
    for(int row = 0; row < [array count]; row++) {
        UIActionButton *SubCatagoryButton = [self createActionButtonWithFrame:CGRectMake(0,  self.CustomScrollView.frame.size.height+ self.CustomScrollView.frame.origin.y, self.containerView.frame.size.width, kButtonHeight) withTitle:kButtonTitleClose];
        SubCatagoryButton.frame = buttonFrame;
        //... more button settings
        [self.view addSubview:SubCatagoryButton];
        buttonFrame.origin.x += marginX;
        [SubCatagoryButton setTag:200+row];
        if (selectedEventIndex == 4)
        {
            [SubCatagoryButton setTitle:array[0]forState:UIControlStateNormal];
            if ([[[NSUserDefaults standardUserDefaults]valueForKey:EVENTSELECTED] isEqualToString:@"0"])
            {
                [SubCatagoryButton setBackgroundColor:[UIColor clearColor] ];
                [SubCatagoryButton setTitleColor:[self getColorForBlackText] forState:UIControlStateNormal];
                SubCatagoryButton.selected = NO;
                
            }
            if ([[[NSUserDefaults standardUserDefaults]valueForKey:EVENTSELECTED] isEqualToString:@"1"])
            {
                [SubCatagoryButton setBackgroundColor:[self    getColorForRedText] ];
                [SubCatagoryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                SubCatagoryButton.selected = YES;
            }
        }
        else
        {
            [SubCatagoryButton setTitle:[array[row] valueForKey:@"CatName"] forState:UIControlStateNormal];
            if ([[array[row]valueForKey:@"catSelection"]isEqualToString:@"False"])
            {
                
                [SubCatagoryButton setBackgroundColor:[UIColor clearColor] ];
                [SubCatagoryButton setTitleColor:[self getColorForBlackText] forState:UIControlStateNormal];
                SubCatagoryButton.selected = NO;
            }
            if ([[array[row]valueForKey:@"catSelection"] isEqualToString:@"True"])
            {
                [SubCatagoryButton setBackgroundColor:[self    getColorForRedText] ];
                [SubCatagoryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                SubCatagoryButton.selected = YES;
            }
        }
        
        [SubCatagoryButton setSectionNumber:row];
        [SubCatagoryButton setRowNumber:selectedEventIndex];
        [SubCatagoryButton addTarget:self action:@selector(SubCatagoryButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        //        if (isEvents) {
        //            [button setTitle:[array objectAtIndex:row] forState:UIControlStateNormal];
        //
        //        }
        //        else{
        //            [button setTitle:[[array objectAtIndex:row] valueForKey:@"CatName"] forState:UIControlStateNormal];
        //
        //        }
        //        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //        [button setBackgroundColor:[self getColorForRedText]];
        [self.CustomScrollView addSubview:SubCatagoryButton];
        
        
        
        buttonFrame.origin.x = 0.;
        buttonFrame.origin.y += marginY;
    }
    
}
@end