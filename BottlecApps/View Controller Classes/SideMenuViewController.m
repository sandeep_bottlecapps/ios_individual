//
//  SideMenuViewController.m
//  BottlecApps
//
//  Created by Kuldeep Tyagi on 8/4/15.
//  Copyright (c) 2015 Kuldeep Tyagi. All rights reserved.
//

#import "SideMenuViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "HomeViewController.h"
#import "ModelManager.h"
#import "Constants.h"

#define kRowHeight_custom (IS_IPHONE_4_OR_LESS? 50:IS_IPHONE_6?60:50)
#define klogoutgap  (IS_IPHONE_4_OR_LESS? 58:IS_IPHONE_6?20:58)

@interface SideMenuViewController ()  <UITableViewDelegate, UITableViewDataSource>  {
    
}

@property (nonatomic, strong) UITableView   *_sideMenuTableView;
@property (nonatomic, assign) BOOL          _slideOutAnimationEnabled;
@end

@implementation SideMenuViewController
@synthesize _sideMenuTableView;
@synthesize _slideOutAnimationEnabled;

#pragma mark - UIViewController Methods -
- (id)initWithCoder:(NSCoder *)aDecoder {
    self._slideOutAnimationEnabled = YES;
    return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad {
    [super viewDidLoad];DLog(@"");
    /**********************************Setup tableView****************************************/
    self._sideMenuTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,
                                                                            CGRectGetWidth(self.view.frame),
                                                                            CGRectGetHeight(self.view.frame))
                                                           style:UITableViewStylePlain];
    [self._sideMenuTableView setSeparatorColor: [UIColor lightGrayColor]];
    [self._sideMenuTableView setDelegate:self];
    [self._sideMenuTableView setDataSource:self];
    [self.view addSubview:self._sideMenuTableView];
    /**********************************Setup tableView****************************************/
    
    _sideMenuTableView.backgroundColor=[UIColor whiteColor];
    _sideMenuTableView .separatorStyle=UITableViewCellSeparatorStyleNone;
    
    
    /*********************************logout at bottam ************************************/
    
    UIImage *logoutImage=[UIImage imageNamed:kImageLogoutButton];
    if (IS_IPHONE_6_PLUS) {
        self.LogoutButton = [self createButtonWithFrame:CGRectMake(0, self._sideMenuTableView.bounds.size.height-logoutImage.size.height, (logoutImage.size.width)+5, logoutImage.size.height) withTitle:kEmptyString];
    }
    else{
        self.LogoutButton = [self createButtonWithFrame:CGRectMake(0, self._sideMenuTableView.bounds.size.height-logoutImage.size.height, (logoutImage.size.width)-klogoutgap, logoutImage.size.height) withTitle:kEmptyString];
        
    }
    [self.LogoutButton setTitle:kButtonLogoutTittleINCAPS forState:UIControlStateNormal];
    [self.LogoutButton setBackgroundColor:[self getColorForRedText]];
    [self.LogoutButton.titleLabel setFont:[UIFont fontWithName:kFontBold size:kFontBoldSize+4]];
    self.LogoutButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.LogoutButton addTarget:self action:@selector(LogoutButton:) forControlEvents:UIControlEventTouchUpInside];
    self.LogoutButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    self._sideMenuTableView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
    [_sideMenuTableView addSubview:self.LogoutButton];
    
    //[self.LogoutButton setImage:logoutImage forState:UIControlStateNormal];
       /*********************************logout at bottam ************************************/
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    /**********************************adding items to side menu*****************************/
    
     deliverClicked = @"0"; pickUpClicked = @"0";
    if([loyaltyEnable intValue]== 1)
    {
        sidemenuArray=@[@"HOME",@"PROFILE",@"LOYALTY CARD",@"FAVORITES",@"CONTACT US"];
        if([notloggedIn intValue]==1)
            viewControllersArray = @[kHomeViewControllerStoryboardId,kLoginViewControllerStoryboardId,kLoginViewControllerStoryboardId,kFavoriteBrowseControllerStoryboardId,kContactUsViewControllerStoryboardId];
        else
            viewControllersArray = @[kHomeViewControllerStoryboardId,kUserProfileViewControllerStoryboardId,kLoyaltyCardViewControllerStoryboardId,kFavoritesViewControllerStoryboardId,kContactUsViewControllerStoryboardId];
    }
    
    else
    {
        sidemenuArray=@[@"HOME",@"PROFILE",@"FAVORITES",@"CONTACT US"];
        if([notloggedIn intValue]==1)
            viewControllersArray = @[kHomeViewControllerStoryboardId,kLoginViewControllerStoryboardId,kFavoriteBrowseControllerStoryboardId,kContactUsViewControllerStoryboardId];
        else
            viewControllersArray = @[kHomeViewControllerStoryboardId,kUserProfileViewControllerStoryboardId,kFavoritesViewControllerStoryboardId,kContactUsViewControllerStoryboardId];
    }
    
    
    //Logout Button
    if([notloggedIn intValue]==1)
        [self.LogoutButton setTitle:kButtonSIGNINTitleINCAPS forState:UIControlStateNormal];
    else
        [self.LogoutButton setTitle:kButtonLogoutTittleINCAPS forState:UIControlStateNormal];
    
    /**********************************adding items to side menu*****************************/
    

}

-(void)dealloc  {
    self._sideMenuTableView.dataSource  = nil;
    self._sideMenuTableView.delegate    = nil;
    self._sideMenuTableView             = nil;
}

#pragma mark - UITableView Delegate & Datasrouce -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section    {
    return [sidemenuArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
      return kRowHeight_custom;

}

/*- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    UIView *headerView = [[UIView alloc] init];
    
    headerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:kImageButtonStrip]];
       UIButton *HomeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [HomeButton addTarget:self
               action:@selector(HomeButton:)
     forControlEvents:UIControlEventTouchUpInside];
    [HomeButton setTitle:@"HOME" forState:UIControlStateNormal];
    [HomeButton setTitleColor:[self getColorForBlackText] forState:UIControlStateNormal];
    HomeButton.titleLabel.font=[UIFont fontWithName:kFontBoldCondensed size:kFontSemiBoldSize];
    HomeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

    HomeButton.backgroundColor=[UIColor clearColor];
    HomeButton.frame = CGRectMake(14, 20,185, 46);
    [headerView addSubview:HomeButton];
    //[headerView addSubview:homeButton];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    return 77;
    
}*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier: kDefaultTableViewCell];
    if (cell == nil)    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:kDefaultTableViewCell];
           }
    
    [self getSeperatorVForTableCell:cell];
    UILabel *itemLabel=[self createLblWithRect:CGRectMake(kPaddingSmall*4, kPaddingSmall, kLabelWidth*2, kRowHeight_custom) font:[self addFontSemiBold] text:[sidemenuArray objectAtIndex:indexPath.row] textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
    [itemLabel setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell addSubview:itemLabel];
   // cell.textLabel.text=[sidemenuArray objectAtIndex:indexPath.row];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIViewController *vc = nil;
    NSString *viewControllerIdentifier = [viewControllersArray objectAtIndex:indexPath.row];
   // NSLog(@"ViewController = %@",viewControllerIdentifier);
    switch (indexPath.row)  {
        case 0:
              vc= [self viewControllerWithIdentifier:viewControllerIdentifier];
              [[NSNotificationCenter defaultCenter] postNotificationName:viewControllerIdentifier object:nil];
            [self.navigationController pushViewController:vc animated:YES];
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                                     withSlideOutAnimation:self._slideOutAnimationEnabled
                                                                      andCompletion:nil];
             break;
        case 1:
            if([notloggedIn intValue]==1)
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Confirm" message:@"In order to proceed further please Sign in or Sign Up for the app."
                                                                 delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Sign In/ Sign Up", nil];
                [alert show];
            }
            else
            {
               vc= [self viewControllerWithIdentifier:viewControllerIdentifier];
              [[NSNotificationCenter defaultCenter] postNotificationName:viewControllerIdentifier object:nil];
               [self.navigationController pushViewController:vc animated:YES];
                [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                                         withSlideOutAnimation:self._slideOutAnimationEnabled
                                                                                 andCompletion:nil];
            }

            break;
        case 2:
            if([notloggedIn intValue]==1 && [loyaltyEnable intValue]==1)
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Confirm" message:@"In order to proceed further please Sign in or Sign Up for the app."
                                                              delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Sign In/ Sign Up", nil];
                [alert show];
            }
            else
            {
              vc= [self viewControllerWithIdentifier:viewControllerIdentifier];
              [[NSNotificationCenter defaultCenter] postNotificationName:viewControllerIdentifier object:nil];
               [self.navigationController pushViewController:vc animated:YES];
                [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                                         withSlideOutAnimation:self._slideOutAnimationEnabled
                                                                                 andCompletion:nil];
            }
            break;
        case 3:
            vc= [self viewControllerWithIdentifier:viewControllerIdentifier];
            [[NSNotificationCenter defaultCenter] postNotificationName:viewControllerIdentifier object:nil];
             [self.navigationController pushViewController:vc animated:YES];
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                                     withSlideOutAnimation:self._slideOutAnimationEnabled
                                                                             andCompletion:nil];

            break;
        case 4:
            vc= [self viewControllerWithIdentifier:viewControllerIdentifier];
            [[NSNotificationCenter defaultCenter] postNotificationName:viewControllerIdentifier object:nil];
             [self.navigationController pushViewController:vc animated:YES];
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                                     withSlideOutAnimation:self._slideOutAnimationEnabled
                                                                             andCompletion:nil];;

            break;
    }
   
   /* */

}
#pragma mark --  AlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([notloggedIn intValue]==1)
    {
        if(buttonIndex == 1)
        {
            UIViewController *vc= [self viewControllerWithIdentifier:kLoginViewControllerStoryboardId];
            [[NSNotificationCenter defaultCenter] postNotificationName:kLoginViewControllerStoryboardId object:nil];
            [self.navigationController pushViewController:vc animated:YES];
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                                     withSlideOutAnimation:self._slideOutAnimationEnabled
                                                                             andCompletion:nil];
        }
        else {}
    }
}

#pragma MARK-IBACTIONMETHODS

-(void)LogoutButton:(id)sender
{
    if([self.LogoutButton.titleLabel.text isEqualToString:kButtonLogoutTittleINCAPS])
    {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"badgecount"];
    [UserDefaults removeObjectForKey:@"SelectedDate"];
    [self._sideMenuTableView deselectRowAtIndexPath:[self._sideMenuTableView indexPathForSelectedRow] animated:YES];
    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
    [[ModelManager modelManager] archiveIsUserAlreadyLoggedInVariable:NO];
    }
    else if([self.LogoutButton.titleLabel.text isEqualToString:kButtonSIGNINTitleINCAPS])
    {
        [self._sideMenuTableView deselectRowAtIndexPath:[self._sideMenuTableView indexPathForSelectedRow] animated:YES];
        [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
    }
    else{}
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    self.LogoutButton.transform = CGAffineTransformMakeTranslation(0, scrollView.contentOffset.y);
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // this is needed to prevent cells from being displayed above our static view
    [self._sideMenuTableView bringSubviewToFront:self.LogoutButton];
}

@end
