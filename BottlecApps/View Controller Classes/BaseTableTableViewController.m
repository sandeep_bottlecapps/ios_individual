//
//  BaseTableTableViewController.m
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/27/14.
//  Copyright (c) 2014 classic. All rights reserved.
//

#import "BaseTableTableViewController.h"

@interface BaseTableTableViewController ()

@property (nonatomic, retain) BaseViewController* baseController;

@end

@implementation BaseTableTableViewController
@synthesize baseController;
- (void)viewDidLoad {
    [super viewDidLoad];DLog(@"");
       baseController = [[BaseViewController alloc] init];
    [baseController customizeTheNavigationbar:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Custom methods from BaseviewController
-(void)setNavBarTitle:(NSString*)title italic:(BOOL) isItalic   {
    if(baseController)  {
        [baseController setNavBarTitle:title controller:self italic:isItalic];
    }
}

- (UIImage *)crop:(UIImage *)image convertToSize:(CGRect) rect  {
    if(baseController)  {
        return [baseController crop:image convertToSize:rect];
    }
    return nil;
}

#pragma spinner mathods
// to show spinner
-(void) showSpinner {
    if(baseController)  {
        [baseController showSpinnerToSuperView:self.view];
    }
}

// to hide spinner
-(void) hideSpinner {
    if(baseController)  {
        [baseController hideSpinner];
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Potentially incomplete method implementation.
    // Return the number of ections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}

#pragma mark - To show refresh control
-(void)createRefreshControlWithSelector:(SEL)selectorName   {
    //to add the UIRefreshControl to UIView
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Please Wait..."]; //to give the attributedTitle
    [refreshControl addTarget:self action:selectorName forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}

@end
