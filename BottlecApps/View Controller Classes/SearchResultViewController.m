//
//  SearchResultViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 8/25/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "SearchResultViewController.h"
#import "SearchBeerViewController.h"
#import "MTBBarcodeScanner.h"
@interface SearchResultViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation SearchResultViewController
-(void)initScrollViewContent  {
    [self.CustomScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, self.CustomScrollView.frame.size.height )];
    [self.CustomScrollView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Home_Img5.jpg"]]];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if(kStoreBottleCapps == 2||kStoreBottleCapps == 3)
    {
        self.searchItemsArray = @[@"BARCODE SCANNER",@"LIQUOR",@"WINE"];
        self. search_icons_Array=@[kImage_BarcodeScanner,kImage_liquor,kImage_wine];
    }
    else
    {
        self.searchItemsArray=@[@"BARCODE SCANNER",@"BEER",@"LIQUOR",@"WINE"];
        self. search_icons_Array=@[kImage_BarcodeScanner,kImage_beer,kImage_liquor,kImage_wine];
    }

       
   

    [self addScrollView];
    [self initScrollViewContent];
    [self AddDefaultTableView];
    [self.defaultTableView setDelegate:self];
    [self.defaultTableView setDataSource:self];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - UITableView Delegate & Datasrouce -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section    {
    return [self.searchItemsArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
 if(kStoreBottleCapps == 2||kStoreBottleCapps == 3)
 {
    return kRowHeight_custom_without_Beer;
 }
  else
    return kRowHeight_custom;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier: kDefaultTableViewCell];
    UIImage *IconImage = [UIImage imageNamed:[self.search_icons_Array objectAtIndex:indexPath.row]];
    CGRect IconImageFrame ;
    IconImageFrame.origin.x       =   kCellPadding;
    IconImageFrame.size.height    =   kIconImageHightAndWidth;
    IconImageFrame.size.width     =   kIconImageHightAndWidth;
    if(kStoreBottleCapps == 2 || kStoreBottleCapps == 3)
    {
        IconImageFrame.origin.y = (kRowHeight_custom-kIconImageHightAndWidth)/2+10.0;
    }
    else
      IconImageFrame.origin.y       =   (kRowHeight_custom-kIconImageHightAndWidth)/2;
    CGRect searchItemsLabelFrame ;
    searchItemsLabelFrame.origin.y       =   IconImageFrame.origin.y;
    searchItemsLabelFrame.origin.x       =   kIconImageHightAndWidth+IconImageFrame.origin.x+(kPaddingSmall*6);
    searchItemsLabelFrame.size.height    =   kIconImageHightAndWidth;
    searchItemsLabelFrame.size.width     =   kLabelWidth*2;
    UILabel *searchItemsLabel=[self createLblWithRect:searchItemsLabelFrame font:[self addFontSemiBold] text:[self.searchItemsArray objectAtIndex:indexPath.row] textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
    searchItemsLabel.backgroundColor=[UIColor clearColor];
    UIImageView *eventsIconImageView=[self createImageViewWithRect:IconImageFrame image:IconImage];
    [eventsIconImageView setContentMode:UIViewContentModeScaleAspectFit];

    if (cell == nil)    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:kDefaultTableViewCell];

            }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell addSubview:searchItemsLabel];
    [cell addSubview:eventsIconImageView];
    [self getSeperatorVForTableCell:cell];

    UIView *containerView=[self createContentViewWithFrame:CGRectMake(80, 15, cell.frame.size.width-100, cell.frame.size.height)];
    [containerView setBackgroundColor:[UIColor clearColor]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SearchBeerViewController *vc=nil;
    vc=[self viewControllerWithIdentifier:kSearchBeerViewControllerStoryboardId];
    switch (indexPath.row)  {
        case 0:{
            [self addBarCodeScannerWithType:nil];
        }
            
            break;
            
        case 1:
            if(kStoreBottleCapps == 2 || kStoreBottleCapps == 3)
            {
               vc.searchItemString=kNavigationTitleFINDAPRODUCTLiquor;
            }
            else
                vc.searchItemString=kNavigationTitleFINDAPRODUCTBeer;

            DLog(@"%@", vc.searchItemString);
            [self.navigationController pushViewController:vc animated:YES];
            break;
            
        case 2:
            if(kStoreBottleCapps == 2 || kStoreBottleCapps == 3)
            {
               vc.searchItemString=kNavigationTitleFINDAPRODUCTWine;
            }
            else
               vc.searchItemString=kNavigationTitleFINDAPRODUCTLiquor;
            [self.navigationController pushViewController:vc animated:YES];


            break;
        case 3:
            vc.searchItemString=kNavigationTitleFINDAPRODUCTWine;
            [self.navigationController pushViewController:vc animated:YES];

            
            break;

      
            
            return;
            break;
    }
    
}


@end
