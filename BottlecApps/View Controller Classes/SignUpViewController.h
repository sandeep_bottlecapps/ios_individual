//
//  SignUpViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 8/12/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "BaseViewController.h"
#import "User.h"

@interface SignUpViewController  :BaseViewController<UITextFieldDelegate,DobEditedDelegate>
{
    BOOL termsAndConditionsChecked;
}
//textfields
@property (retain, nonatomic)  UITextField                *emailTextfield;
@property (retain, nonatomic)  UITextField                *passwordTextfield;
//button
@property (retain, nonatomic)  UIButton                   *NextButton;
@property (retain, nonatomic)  UIButton                   *signUpButton;
@property (retain, nonatomic)  UIButton                   *addAPhotoButtonStep2;
@property (retain, nonatomic)  UIButton                   *iAgreeButton;
@property (retain, nonatomic)  UIButton                   *TermsAndConditionButton;
@property (retain, nonatomic)  User                       *user;
@property (retain, nonatomic)  UIImageView                   *iAgreeImageView;
@property(nonatomic,retain)NSString *barcodeTypeString;
@property(nonatomic,retain)NSString *selectedDOBString;


@end
