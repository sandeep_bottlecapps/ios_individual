//
//  LoginViewController.m
//  BottlecApps
//
//  Created by Kuldeep Tyagi on 8/4/15.
//  Copyright (c) 2015 Kuldeep Tyagi. All rights reserved.
//

#import "LoginViewController.h"
#import "BaseViewController.h"
#import "AsyncImageView.h"
#import "RCSwitchOnOff.h"
#import "Constants.h"


// #define kGaps              kStoreBottleCapps?10:(IS_IPHONE_4_OR_LESS? 25:IS_IPHONE_6?35:15)

#define topPadding (IS_IPHONE_4_OR_LESS? 0:IS_IPHONE_6?3:0)
#define sidePadding (IS_IPHONE_4_OR_LESS?10:IS_IPHONE_6?3:10)
#define Padding (IS_IPHONE_4_OR_LESS?4:IS_IPHONE_6?0:0)
#define kLogoImageFrame        (IS_IPHONE_4_OR_LESS? CGRectMake((SCREEN_WIDTH-189)/2, kTopPadding_logo, 189, 45):IS_IPHONE_6?CGRectMake((SCREEN_WIDTH-189)/2, kTopPadding_logo, 189, 45):CGRectMake((SCREEN_WIDTH-189)/2, kTopPadding_logo, 189, 45))

@implementation LoginViewController
-(void)CallApiLogin{
    NSMutableDictionary* userInfo = [NSMutableDictionary dictionary];
    if([self formValidate]){
        [userInfo setObject:self.UserEmailTextField.text forKey: kUserEmailAddress];
        [userInfo setObject:self.PasswordTextField.text forKey: kUserPassword];
        [userInfo setObject:kSTOREIDbottlecappsString forKey: kUserStoreId];
        if (TARGET_IPHONE_SIMULATOR) {
            [userInfo setObject:kDEVICETOKENString forKey: kUserDeviceId];
        }
        else{
            [userInfo setObject:[[ModelManager modelManager] getCurrentDeviceToken] forKey: kUserDeviceId];
        }
        [userInfo setObject:@"I" forKey: kUserDeviceType];
      //  NSLog(@"user info = %@",userInfo);
        [self showSpinner]; // To show spinner
        [[ModelManager modelManager] loginWithUserCredentials:userInfo  successStatus:^(BOOL status) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self hideSpinner];
                if ([self.Remember_meButton isSelected]) {
                    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"RememberMe"];
                }
                else{
                    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"RememberMe"];
                }
                if([notloggedIn intValue]== 1)
                {
                   notloggedIn = @"0";
                     //All browse favorites added to that particular userid when user logs in
                    returnProductIDArray = [[NSMutableArray alloc]init];
                   NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                  [self getProductIdsFromLocalStorage:[defaults objectForKey:@"offlineFavoriteArray"]];
                  if([returnProductIDArray count]==0){}
                  else
                   {
                   combinedString = [returnProductIDArray componentsJoinedByString:@","];
                    //[self CallApiAddFavoriteWithProductID:combinedString];
                
                   }
                }
                else
                {
                    favoritesAlreadyAdded = @"1";
                }
                if([pickUpClicked intValue]==1||[deliverClicked intValue]==1)
                    [self popViewControllerWithDelay:0.0 complition:nil];
                else
                [self performSegueWithIdentifier:kGoToHomeViewControllerSegueId sender:nil];
            });
        } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
            [self hideSpinner];
            [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
            DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        }];
    }
}

-(void)getProductIdsFromLocalStorage:(NSArray *)localStorageArray
{
    for(int i=0;i<[localStorageArray count];i++)
    {
        NSArray *individualArray = [localStorageArray objectAtIndex:i];
        [returnProductIDArray addObject:[individualArray objectAtIndex:4]];
    }
}

-(void)createFooterforView{
    /*******************create footer with sign in and sign up button***************/
    /***************add footer image on view**********************************************/
    UIImageView *footerImageView=[self createImageViewWithRect:CGRectMake(0, SCREEN_HEIGHT-kLoginfooterHeight, SCREEN_WIDTH, kLoginfooterHeight) image:[UIImage imageNamed:kImageLoginFooter]];
    [self.view addSubview:footerImageView];
    
    /***************add signin button**********************************************/
    UIImage *signinbuttonImage=[UIImage imageNamed:kImageSignInButton];
    
    
    self.SigninButton=[self createButtonWithFrame:CGRectMake((SCREEN_WIDTH-kSigninButtonWidth)/2, SCREEN_HEIGHT-kSigninButtonHeight-(kBottomPadding*2), kSigninButtonWidth, kSigninButtonHeight) withTitle:kEmptyString];
    [self.SigninButton addTarget:self action:@selector(SigninButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.SigninButton setBackgroundImage:[UIImage imageNamed:kImageSignInButton] forState:UIControlStateNormal];
    [self.view addSubview:self.SigninButton];
    /***************add terms and condition button**********************************************/
      NSDictionary *attribs = @{
                              NSForegroundColorAttributeName: [UIColor whiteColor],
                              NSFontAttributeName: [UIFont fontWithName:kFontRegular size:kFontRegularSize],
                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)
                              };
    NSString * text = kMessageIagree;
    NSMutableAttributedString *attributedText =
    [[NSMutableAttributedString alloc] initWithString:text
                                           attributes:attribs];
    // Red text attributes
    NSRange redTextRange = [text rangeOfString:kMessagetermsAndConditions];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                    NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)
                                    }
                            range:redTextRange];
    // Gray text attributes
    NSRange grayTextRange = [text rangeOfString:kMessageIAgreeTo];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}
                            range:grayTextRange];
    NSRange grayTextRange1 = [text rangeOfString:kMessageAndCertify];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}
                            range:grayTextRange1];
    self.TermsAndConditionButton=[self createButtonWithFrame:CGRectMake(0,SCREEN_HEIGHT-kTEXTfieldHeight+Padding, SCREEN_WIDTH, kTEXTfieldHeight) withTitle:kEmptyString];
    [self.TermsAndConditionButton.titleLabel setFont:[UIFont fontWithName:kFontRegular size:kFontRegularSize]];
    self.TermsAndConditionButton .titleLabel.numberOfLines=2;
    _TermsAndConditionButton.backgroundColor=[UIColor clearColor];
    [self.TermsAndConditionButton setAttributedTitle:attributedText forState:UIControlStateNormal];
    _TermsAndConditionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    _TermsAndConditionButton.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
    [self.view addSubview:self.TermsAndConditionButton];
    
    UIButton *termsButton=[self createButtonWithFrame:CGRectMake(0, SCREEN_HEIGHT-kTEXTfieldHeight+Padding, SCREEN_WIDTH, kTEXTfieldHeight) withTitle:nil];
    termsButton.backgroundColor=[UIColor clearColor];
    [termsButton addTarget:self action:@selector(TermsAndConditionButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:termsButton];
    /***************add terms and condition button**********************************************/

    
}


-(void)AddTermsAndconditionWithIAgreeButton{
    
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName: [self getColorForRedText],
                              NSFontAttributeName: [UIFont fontWithName:kFontRegular size:kFontRegularSize],
                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)
                              };
    NSString * text = kMessageIagree;
    NSMutableAttributedString *attributedText =
    [[NSMutableAttributedString alloc] initWithString:text
                                           attributes:attribs];
    // Red text attributes
    NSRange redTextRange = [text rangeOfString:kMessagetermsAndConditions];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                    NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)
                                    }
                            range:redTextRange];
    // Gray text attributes
    NSRange grayTextRange = [text rangeOfString:kMessageIAgreeTo];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}
                            range:grayTextRange];
    NSRange grayTextRange1 = [text rangeOfString:kMessageAndCertify];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}
                            range:grayTextRange1];
    self.TermsAndConditionButton=[self createButtonWithFrame:CGRectMake(0,450, SCREEN_WIDTH, kTEXTfieldHeight) withTitle:kEmptyString];
    [self.TermsAndConditionButton.titleLabel setFont:[UIFont fontWithName:kFontRegular size:kFontRegularSize]];
    self.TermsAndConditionButton .titleLabel.numberOfLines=2;
    _TermsAndConditionButton.backgroundColor=[UIColor greenColor];
    [self.TermsAndConditionButton setAttributedTitle:attributedText forState:UIControlStateNormal];
    _TermsAndConditionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    _TermsAndConditionButton.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
    [self.CustomScrollView addSubview:self.TermsAndConditionButton];
    UIButton *termsButton=[self createButtonWithFrame:CGRectMake(0, SCREEN_HEIGHT-kTEXTfieldHeight+Padding, SCREEN_WIDTH, kTEXTfieldHeight) withTitle:nil];
    termsButton.backgroundColor=[UIColor clearColor];
    [termsButton addTarget:self action:@selector(TermsAndConditionButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.CustomScrollView addSubview:termsButton];
    
    
}
-(void)addAppLogoImageInCenterOFLoginScreen{
   // NSLog(@"kTop padding = %d",kTopPadding2);
    /***********create image view with bottlecapps logo*******************/
    UIImage *logoimage=[UIImage imageNamed:kImageLogo];
    // self.LogoImageView= [self createImageViewWithRect:CGRectMake((SCREEN_WIDTH-logoimage.size.width)/2, kTopPadding2, logoimage.size.width, logoimage.size.height)  image:logoimage];
      if(kStoreImage == 1)
        self.LogoImageView= [self createImageViewWithRect:CGRectMake((SCREEN_WIDTH-logoimage.size.width)/2, kTopPadding2, logoimage.size.width, logoimage.size.height)  image:logoimage];
    else
        self.LogoImageView= [self createImageViewWithRect:CGRectMake((SCREEN_WIDTH-logoimage.size.width)/2-26, kTopPadding2, logoimage.size.width+50.0, logoimage.size.height+10.0)  image:logoimage];
    [self.CustomScrollView addSubview:self.LogoImageView];
    /***********create image view with bottlecapps logo*******************/
}

-(void)initScrollViewContent{
    /*****************set content size to scrollview******************/
    [self createUI];
}

#pragma mark-viewcontroller life cycle
-(void)addCustomSwitch{
    float cYAxis;
    if(kStoreImage == 1)
        cYAxis = kGaps-4;
    else
        cYAxis = kGaps-34;
    [[UISwitch appearance] setOnTintColor:[UIColor clearColor]];
    [[UISwitch appearance] setTintColor:[self getColorForBlackText]];
    UISwitch *aSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(108, (self.PasswordTextField.frame.origin.y+self.PasswordTextField.frame.size.height)+cYAxis, 10, 10)];
    aSwitch.transform = CGAffineTransformMakeScale(0.55, 0.55);
    aSwitch.onImage = [UIImage imageNamed:@"toggle_off"];
    aSwitch.offImage = [UIImage imageNamed:@"toggle_on"];
    [aSwitch setOnTintColor:[self getColorForRedText]];
    [self.view addSubview:aSwitch];

    [aSwitch addTarget:self action:@selector(RememberMeButton:) forControlEvents:UIControlEventValueChanged];
   }
-(void)createUI{
    
    //Maipulate KGaps as per kstringBottlecapps
    float uyAxis,pyAxis,ryAxis,fpyAxis;
    if(kStoreImage == 1)
    {
        uyAxis = kGaps;
        pyAxis = kGaps;
        ryAxis = kGaps;
        fpyAxis = kGaps;
    }
    else
    {
        uyAxis = kGaps+10;
        pyAxis = kGaps-30;
        ryAxis = kGaps-30;
        fpyAxis = kGaps-20;
    }
    
    self.CustomScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, self.CustomScrollView.frame.size.height);
    /*****************set content size to scrollview******************/
    [self addAppLogoImageInCenterOFLoginScreen];
    /***********create text field with username *******************/
   // NSLog(@"Rect = %f...%d...%d",(self.LogoImageView.frame.origin.y+self.LogoImageView.frame.size.height)+uyAxis,kTopPadding2,kGaps);
    //NSLog(@"Kright padding  = %d",kRightPadding);
    self.UserEmailTextField= [self createTxtfieldWithRect:CGRectMake(kRightPadding,(self.LogoImageView.frame.origin.y+self.LogoImageView.frame.size.height)+uyAxis,SCREEN_WIDTH-(kRightPadding*2)
                                                                     ,kTEXTfieldHeight) text:kloginuser withLeftImage:[UIImage imageNamed:kImageUsernameIcon] withPlaceholder:kTextfieldPlaceholderemail withTag:kTextfieldTagSigninUserEmail ] ;
    self.UserEmailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.UserEmailTextField setDelegate:self];
    [self.UserEmailTextField setTextColor:[self getColorForRedText]];
    [self.UserEmailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    self.UserEmailTextField.font= [UIFont fontWithName:kFontRegular size:kFontRegularSize];
    [self.CustomScrollView addSubview:self.UserEmailTextField];
    /***********create text field with username *******************/
    /***********create text field with password *******************/
    self.PasswordTextField= [self createTxtfieldWithRect:CGRectMake(kRightPadding,(self.UserEmailTextField.frame.origin.y+self.UserEmailTextField.frame.size.height)+pyAxis, SCREEN_WIDTH-(kRightPadding*2),kTEXTfieldHeight) text:kloginpassword withLeftImage:[UIImage imageNamed:kImagePasswordIcon] withPlaceholder:kTextfieldPlaceholderpassword withTag:kTextfieldTagSigninPassword ];
    [self.PasswordTextField setSecureTextEntry:YES];
    [self.PasswordTextField setTextColor:[self getColorForRedText]];
    [self.PasswordTextField setDelegate:self];
    self.PasswordTextField.font= [UIFont fontWithName:kFontRegular size:kFontRegularSize];
    [self.CustomScrollView addSubview:self.PasswordTextField];
    /***********create text field with password *******************/
    /***********create button to remember username  *******************/
    self.Remember_meButton=[self createSelectedUnselectedButtonViewWithFrame:CGRectMake(10,(self.PasswordTextField.frame.origin.y+self.PasswordTextField.frame.size.height)+ryAxis,SCREEN_WIDTH-(kRightPadding*2), 20) withTitle:kButtonTitleRememberMe];
    self.Remember_meButton.titleLabel.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize];
    // [self.Remember_meButton addTarget:self action:@selector(RememberMeButton:) forControlEvents:UIControlEventTouchUpInside];
    _Remember_meButton.backgroundColor=[UIColor clearColor];
    [self.CustomScrollView addSubview:self.Remember_meButton];
    [self addCustomSwitch];
    /***********create button to remember username  *******************/
    /***************add signup button**********************************************/
    self.SignUpButton=[self createButtonWithFrame:CGRectMake(kRightPadding-5.0,(self.Remember_meButton.frame.origin.y+self.Remember_meButton .frame.size.height)-topPadding, SCREEN_WIDTH-(kRightPadding*10), kButtonHeight) withTitle:kEmptyString];
    [self.SignUpButton addTarget:self action:@selector(SignupButton:) forControlEvents:UIControlEventTouchUpInside];
    self.SignUpButton.titleLabel.textColor=[self getColorForBlackText];
    self.SignUpButton.titleLabel.font = [self addFontRegular];
    [self.SignUpButton setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.SignUpButton];
    NSMutableAttributedString *attributedString=[[NSMutableAttributedString alloc]init];
    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:kLabelNowSignUpAccount
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),NSForegroundColorAttributeName: [self getColorForRedText]}]];
    if(kStoreBottleCapps == 11)
     [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@" now or just"
                                                                              attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone),NSForegroundColorAttributeName:[UIColor whiteColor]}]];
    else
        [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@" now or just"
                                                                                 attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)}]];
    
    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@""]];
    [self.SignUpButton setAttributedTitle:attributedString forState:UIControlStateNormal];
  

    /******************************** End *********************************************/
    /***************add Browse button**********************************************/
    
    self.browseButton=[self createButtonWithFrame:CGRectMake(kRightPadding+(SCREEN_WIDTH-(kRightPadding*12.3)),(self.Remember_meButton.frame.origin.y+self.Remember_meButton .frame.size.height)-topPadding, SCREEN_WIDTH-(kRightPadding*10), kButtonHeight) withTitle:kEmptyString];
    [self.browseButton setBackgroundColor:[UIColor clearColor]];
    [self.browseButton addTarget:self action:@selector(browseClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.browseButton.titleLabel.textColor=[self getColorForBlackText];
    self.browseButton.titleLabel.font = [self addFontRegular];
    NSMutableAttributedString *browseAttributedtext=[[NSMutableAttributedString alloc]init];
    [browseAttributedtext appendAttributedString:[[NSAttributedString alloc] initWithString:@"Browse"
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),NSForegroundColorAttributeName: [self getColorForRedText]}]];
    [self.view addSubview:self.browseButton];
    [self.browseButton setAttributedTitle:browseAttributedtext forState:UIControlStateNormal];
    
    /******************************** End *********************************************/
    
    
    /***********create button forgot password  *******************/
    self.ForgotYourPasswordButton=[self createButtonWithFrame:CGRectMake(kRightPadding+70,(self.PasswordTextField.frame.origin.y+self.PasswordTextField.frame.size.height)+pyAxis, SCREEN_WIDTH-(kRightPadding*2), 20) withTitle:kButtonForgotYouPsssword];
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    self.ForgotYourPasswordButton.titleLabel.attributedText=[[NSAttributedString alloc] initWithString:kButtonForgotYouPsssword
                                                                                            attributes:underlineAttribute];
    if(kStoreBottleCapps == 11)
       [self.ForgotYourPasswordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    else
         [self.ForgotYourPasswordButton setTitleColor:[self getColorForBlackText] forState:UIControlStateNormal];
    [self.ForgotYourPasswordButton addTarget:self action:@selector(ForgotYourPassword:) forControlEvents:UIControlEventTouchUpInside];
    self.ForgotYourPasswordButton.titleLabel.font = [UIFont fontWithName:kFontRegular size:kFontRegularSize];
    [self.CustomScrollView addSubview:self.ForgotYourPasswordButton];
    /***********create button forgot password  *******************/
}

-(void)viewDidLoad{
    //loyaltyEnable = @"1";
    
    [super viewDidLoad];DLog(@"");
    [self addScrollView];
    [self initScrollViewContent];
    [self createFooterforView];
    //[self browseClicked:0];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    //If User is is already logged in then we need to show homepage
    if([[ModelManager modelManager] isUserAlreadyLoggedIn])  {
        [self performSegueWithIdentifier:kGoToHomeViewControllerSegueId sender:nil];
        [[ShoppingCart sharedInstace] loadShopingCartfromArchiver];
    }
    else    {
        [SlideNavigationController sharedInstance].enableSwipeGesture = NO;
        if ([[NSUserDefaults standardUserDefaults]boolForKey:kUserdefaultRememberme]==YES) {
            [self.Remember_meButton setSelected:YES];
            self.UserEmailTextField.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"username"];
            self.PasswordTextField.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"password"];
        }
        else{
            [self.Remember_meButton setSelected:NO];
            self.UserEmailTextField.text=kEmptyString;
            self.PasswordTextField.text=kEmptyString;
        }
        
    }
}

-(void)viewDidDisappear:(BOOL)animated  {
    if (termsAndConditionsChecked == YES) {
        [SlideNavigationController sharedInstance].enableSwipeGesture = NO;
        termsAndConditionsChecked = NO;
    }
    else
      [SlideNavigationController sharedInstance].enableSwipeGesture = YES;
}
-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [self.CustomScrollView setContentOffset:CGPointZero];
    self.UserEmailTextField.text=kloginuser;
    self.PasswordTextField.text=kloginpassword;
    [self.Remember_meButton setSelected:NO];
}
-(void)dealloc  {
    self.CustomScrollView .delegate           = nil;
}


- (void)SignupButton:(UIButton*)sender {
    notloggedIn = @"0";
    [self addDatePickerToSignUpScreen];
   // [self performSegueWithIdentifier:kGoToSignUpViewControllerSegueId sender:nil];
    
}
- (void)browseClicked:(id)sender
{
    
    /********** Creating pop up when click on browse Button********************/
    
    //Create transparent layer for popup background
     transparentlayer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    transparentlayer.backgroundColor=[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:140.0/255.0 alpha:0.7];
    [self.view addSubview:transparentlayer];
  
    //Creating custom alertview
    alertBodyView  = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/8, (SCREEN_HEIGHT/3)+20.0, 250.0, 130.0)];  //241 27 46
    alertBodyView.layer.zPosition = 10;
    alertBodyView.layer.borderWidth =1.0f;
    alertBodyView.layer.borderColor = [UIColor colorWithRed:241.0/255/.0 green:27.0/255.0 blue:46.0/255.0 alpha:1.0].CGColor;
    alertBodyView.layer.cornerRadius = 10;
    alertBodyView.backgroundColor = [UIColor whiteColor];
    [transparentlayer addSubview:alertBodyView];
    
    //Create Alert Titile
    UILabel *agreementLabel = [self createLblWithRect:CGRectMake(0.0,10.0,alertBodyView.frame.size.width,30.0) font:[UIFont fontWithName:kFontBold size:14] text:@"Agree" textAlignment:NSTextAlignmentCenter textColor:[self getColorForBlackText]];
    [alertBodyView addSubview:agreementLabel];
    
    UILabel *termsAndConditionsLabel = [self createLblWithRect:CGRectMake(10.0,30.0,alertBodyView.frame.size.width,60.0) font:[UIFont fontWithName:kFontRegular size:12] text:@"I represent that I am at least 21 years old and agree to the" textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
    termsAndConditionsLabel.numberOfLines = 2;
    [alertBodyView addSubview:termsAndConditionsLabel];
    
    
    UIButton *termsAndConditonsButton = [self createButtonWithFrame:CGRectMake(alertBodyView.frame.size.width/2.0-22.0,53.0,130.0,30.0) withTitle:@"terms and conditions"];
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    termsAndConditonsButton.titleLabel.attributedText=[[NSAttributedString alloc] initWithString:@"terms and conditions"
                                                                                            attributes:underlineAttribute];
    [termsAndConditonsButton setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
    [termsAndConditonsButton addTarget:self action:@selector(TermsAndConditionButton:) forControlEvents:UIControlEventTouchUpInside];
    termsAndConditonsButton.titleLabel.font = [UIFont fontWithName:kFontRegular size:12];
    [alertBodyView addSubview:termsAndConditonsButton];

    //Create Cancel and Accept buttons
    
    UIButton *cancelButton = [self createButtonWithFrame:CGRectMake(10.0,termsAndConditionsLabel.frame.size.height+ 25.0,alertBodyView.frame.size.width/2,40.0) withTitle:kButtonTitleCancel];
    [cancelButton setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
    [cancelButton.titleLabel setFont:[UIFont fontWithName:kFontRegular size:12]];

    [cancelButton addTarget:self action:@selector(cancelClicked:) forControlEvents:UIControlEventTouchUpInside];
    [alertBodyView addSubview:cancelButton];
    
     UIButton *acceptButton = [self createButtonWithFrame:CGRectMake(alertBodyView.frame.size.width/2.0,termsAndConditionsLabel.frame.size.height+ 25.0,alertBodyView.frame.size.width/2,40.0) withTitle:kButtonTitleAccept];
    [acceptButton setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
    [acceptButton.titleLabel setFont:[UIFont fontWithName:kFontRegular size:12]];
    // [[cancelButton layer] setBorderWidth:1.0f];
    // [[cancelButton layer] setBorderColor:[self getColorForRedText].CGColor];
    [acceptButton addTarget:self action:@selector(acceptClicked:) forControlEvents:UIControlEventTouchUpInside];
    [alertBodyView addSubview:acceptButton];
  /*********************** End of Custom Alert popup ******************************************************************************/
}
-(void)cancelClicked:(id)sender
{
    [transparentlayer removeFromSuperview];
    [alertBodyView removeFromSuperview];
}
-(void)acceptClicked:(id)sender
{
    notloggedIn = @"1";
    pickUpClicked = @"0";
    [transparentlayer removeFromSuperview];
    [alertBodyView removeFromSuperview];
    [self performSegueWithIdentifier:kGoToHomeViewControllerSegueId sender:nil];
    [[ShoppingCart sharedInstace] loadShopingCartfromArchiver];
}
// validate login form

-(BOOL)formValidate{
    check = FALSE;
    NSString *errMsg = @"Error";
    if ([self.UserEmailTextField.text length]==0)
    {
        check = TRUE;
        errMsg = @"Email ID is required.";
    }
    else if([self.PasswordTextField.text length]==0)
    {
        check = TRUE;
        errMsg = @"Password is required.";
    }
      if (check)
    {
        [self showAlertViewWithTitle:nil message:errMsg];
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}
#pragma mark-button click methods
- (void)SigninButton:(UIButton*)sender {
    [[NSUserDefaults standardUserDefaults]setValue:self.UserEmailTextField.text forKey:@"username"];
    [[NSUserDefaults standardUserDefaults]setValue:self.PasswordTextField.text forKey:@"password"];
    
    [self CallApiLogin];
    
    
}
- (void)ForgotYourPassword:(UIButton*)sender {
    [self performSegueWithIdentifier:kGoToForgotPasswordViewControllerSegueId sender:nil];
    
}

- (void)RememberMeButton:(UISwitch*)sender {
    
    if([sender isOn]){
       // NSLog(@"Switch is ON");
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kUserdefaultRememberme];

    } else{
       // NSLog(@"Switch is OFF");
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kUserdefaultRememberme];

    }

   }
- (void)TermsAndConditionButton:(UIButton*)sender {
    termsAndConditionsChecked = YES; //Side Menu navigation worked in TermsAndConditions Screen. To avoid this,need to check this condition.
    [alertBodyView removeFromSuperview];
    [transparentlayer removeFromSuperview];
    [self performSegueWithIdentifier:kGoToTermsAndConditionViewControllerSegueId sender:nil];
    
}

- (void)IAgreeButton:(UIButton*)sender {
    if ([sender isSelected]) {
        self.iAgreeImageView.image=[UIImage imageNamed:kImageUnselectedBox ];
        [sender setSelected:NO];
    }
    else{
        self.iAgreeImageView.image=[UIImage imageNamed:kImageSelectedBox ];
        [sender setSelected:YES];
        
    }
}

@end