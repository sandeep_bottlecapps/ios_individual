//
//  LoginViewController.h
//  BottlecApps
//
//  Created by Kuldeep Tyagi on 8/4/15.
//  Copyright (c) 2015 Kuldeep Tyagi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "iCarousel.h"

@interface LoginViewController  :BaseViewController<UITextFieldDelegate>
{
    BOOL check;
    BOOL termsAndConditionsChecked;
    UIButton *iAgreeButton;
    UIView*transparentlayer;
    UIView *alertBodyView;
    NSMutableArray *returnProductIDArray;
}
@property (retain, nonatomic)  UIButton                   *ForgotYourPasswordButton;
@property (retain, nonatomic)  UITextView                 *TermsAndConditionTextView;
@property (retain, nonatomic)  UILabel                    *RemembermeLabel;
@property (retain, nonatomic)  UITextField                *PasswordTextField;
@property (retain, nonatomic)  UITextField                *UserEmailTextField;
@property (retain, nonatomic)  UIButton                   *SignUpButton;
@property (retain, nonatomic)  UIButton                   *browseButton;
@property (retain, nonatomic)  UIButton                   *SigninButton;
@property (retain, nonatomic)  UIButton                   *TermsAndConditionButton;
@property (retain, nonatomic)  UILabel                    *TermsAndConditionLabel;
@property (retain, nonatomic)  UIButton                   *Remember_meButton;
@property (retain, nonatomic)  UIImageView                *iAgreeImageView;
@property (retain, nonatomic)  UIView                     *ContentView;


- (void)SignupButton:(id)sender;
- (void)SigninButton:(id)sender;
- (void)ForgotYourPassword:(id)sender;
- (void)RememberMeButton:(id)sender;
- (void)IAgreeButton:(id)sender;
- (void)browseClicked:(id)sender;

@end
