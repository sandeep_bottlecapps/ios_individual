//
//  BaseTableTableViewController.h
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/27/14.
//  Copyright (c) 2014 classic. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseTableTableViewController : UITableViewController

-(void) showSpinner;
-(void) hideSpinner;
-(void)setNavBarTitle:(NSString*)title italic:(BOOL) isItalic;
- (UIImage *)crop:(UIImage *)image convertToSize:(CGRect) rect;

-(void)createRefreshControlWithSelector:(SEL)selectorName; // to add refresh control
@end
