//
//  SearchResultViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 8/25/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface SearchResultViewController : BaseViewController
@property (nonatomic,retain)   NSArray                    *searchItemsArray;
@property (retain, nonatomic)  NSArray                    *search_icons_Array;
//- (void) clickImage;
@end
