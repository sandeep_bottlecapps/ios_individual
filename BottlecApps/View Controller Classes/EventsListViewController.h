//
//  EventsListViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 9/7/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface EventsListViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,retain)NSArray *eventsArray;

@end
