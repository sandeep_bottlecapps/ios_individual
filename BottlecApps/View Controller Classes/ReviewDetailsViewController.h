//
//  ReviewDetailsViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 9/3/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface ReviewDetailsViewController : BaseViewController
@property (nonatomic,retain)Product *getProductObj;
@property (nonatomic,retain)NSDictionary *getReviewDict;
@property (nonatomic,retain)NSString *getProductID;

@end
