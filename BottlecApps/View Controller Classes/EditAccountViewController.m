    //
//  EditAccountViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 8/18/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "EditAccountViewController.h"
#import "CJSONDeserializer.h"
#import "Constants.h"

#define API_KEY @"AuthenticationId"
#define ERROR_MSG @"Message"
#define AUTH @"Authentication"
#define dobPadding                                       (IS_IPHONE_4_OR_LESS? 16:IS_IPHONE_6?13:21)
#define kLabelHeighteditprofile                          (IS_IPHONE_4_OR_LESS? 20:IS_IPHONE_6?30:25)
#define kLabelWidtheditprofile                          ((SCREEN_WIDTH-(15*2))/2)-5

@interface EditAccountViewController ()<UITextFieldDelegate,UIScrollViewDelegate>{
    UITextField *addressTxtFd;
    UITextField *stateTxtFd;
    UITextField *zipTxtFd;
    UITextField *userphoneTxtFd;
    UITextField *userApartment;
    UITextField *emailTxtFd;
    NSMutableDictionary *apiResponseDictionary;
    UITextField *cityTxtFd;
    BOOL check;
    BOOL dateofbirthisEdited;
    NSMutableDictionary *AdressValidatorDict;

}
@end
NSString *getAddress;
NSString *state;
NSString *city;
NSString *country;
NSString *zipcode;
NSString* addresstotal1;
NSString* addresstotal2;
@implementation EditAccountViewController
-(BOOL)formValidate{
    check = FALSE;
    
    NSString *errMsg = @"Error";
    if ([self.firstNameTextfield.text length]==0)
    {
        check = TRUE;
        errMsg = kAlertEnterUsername;
    }
//    else if([userApartment.text length]==0)
//    {
//        check = TRUE;
//        errMsg = kAlertEnterApartment;
//    }
  
       else if([emailTxtFd.text length]==0)
    {
        check = TRUE;
        errMsg = kAlertEnterEmail;
    }
    
    
    else if([self.dateOfBirthTextfield.text length]==0){
        check = TRUE;
        errMsg = kAlertEnterDOB;
    }
    
      else if([self.phoneNumberTextfield.text length]==0)
    {
        check = TRUE;
        errMsg = kAlertEnterContactNO;
    }
    else if([[self getOriginalPhonestringFromFormated:self.phoneNumberTextfield.text] length] !=10)
    {
        check = TRUE;
        errMsg = kAlertValidPhoneNumber;
    }

    else if ([emailTxtFd.text length]!=0)
    {
        if (![self validateEmail:emailTxtFd.text])
        {
            check = TRUE;
            errMsg = kAlertValidEmail;
        }
    }
    
    
    else  if(!([self.dateOfBirthTextfield.text length]==0))
    {
        NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
        [inputFormatter setDateFormat:@"MM-dd-yyyy"];
        NSString *date=self.dateOfBirthTextfield.text;
        date=[date stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSDate *dateA = [inputFormatter dateFromString:date];
        NSDate* dateB = [NSDate date];
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        
        [formatter setDateFormat:@"dd-MM-yyyy"];
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                                   fromDate:dateA
                                                     toDate:dateB
                                                    options:0];
        
        NSUInteger diffYear=components.year;
        if (diffYear<21)
        {
            check = TRUE;
            errMsg = @"You must have 21 years of age.";
        }
    }
    
    
    
    if (check)
    {
        [self showAlertViewWithTitle:nil message:errMsg];
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}
#pragma mark-BUTTON CLICKED
-(void)btnDOB:(UIButton*)sender {
    if(sender.tag == kTagDatePickerHidden)    {
        sender.tag = kTagDatePickerShow;
        
        [self.lastNameTextfield resignFirstResponder];
        
        [self.firstNameTextfield resignFirstResponder];
        [self.CardNumberTextFd resignFirstResponder];
        [addressTxtFd resignFirstResponder];
        [userApartment resignFirstResponder];
        [cityTxtFd resignFirstResponder];
        [stateTxtFd resignFirstResponder];
        [zipTxtFd resignFirstResponder];
        [self.phoneNumberTextfield resignFirstResponder];
        [emailTxtFd resignFirstResponder];
        
        [self addDatePickerToView];
    }
    else if(sender.tag == kTagDatePickerShow)   {
        sender.tag = kTagDatePickerHidden;
        [self hideDatePickerview];
    }
}
-(void)CreateFindMeButton{
    UIImage *addAPhotoButtonImage=[UIImage imageNamed:@"out-of-stock"];
    self.findMe=[self createButtonWithFrame:CGRectMake(self.addressLabel.frame.size.width+199,SCREEN_HEIGHT/2 -30, 70, 20) withTitle:@"Find Me"];
    _findMe.imageView.image=addAPhotoButtonImage;
    [self.findMe addTarget:self action:@selector(FindMe:) forControlEvents:UIControlEventTouchUpInside];
   // self.findMe.layer.cornerRadius = (addAPhotoButtonImage.size.height)/2.0f;
    
    self.findMe.clipsToBounds = YES;
    [self.findMe setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
   [self.findMe  setBackgroundColor:[UIColor whiteColor]];
    self.findMe.titleLabel.font=[UIFont fontWithName:kFontBold size:10];
   // self.addAPhotoButton.titleLabel.numberOfLines=2;
    self.findMe.layer.borderColor=[UIColor redColor].CGColor;
    self.findMe.layer.borderWidth=0.5f;
    self.findMe .tag=kButtonTagSignUpAddAphotoButton+40;
    [self.findMe setEnabled:YES];
    [self.CustomScrollView addSubview:_findMe];
   
}

-(void)CreateUI{
    [self addScrollView];
    [self.CustomScrollView setDelegate:self];
    [self.CustomScrollView setContentSize:CGSizeMake(kCustomScrollViewWidth, kCustomScrollViewHeight+100)];
      __weak UserProfile *userProfileObj = kgetUserProfileModel;
    /**************adding imageview for coverphoto********************/
    self.CoverPhotoImageView=[self createImageViewWithRect:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT/2 - 35) image:[UIImage imageNamed:kImageProfileImageBackground]];
    [self.CustomScrollView addSubview:self.CoverPhotoImageView];
    /************************add button for profile imageview***************/

    //EditPhotoButton
    /************************add button for profile imageview***************/
    [self CreateAddAphotoButton];
   [self CreateFindMeButton];
    if (![userProfileObj.UserImage isEqualToString:kEmptyString]) {
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:userProfileObj.UserImage]];
        [self.addAPhotoButton setTitle:kEmptyString forState:UIControlStateNormal];
        [self.addAPhotoButton setBackgroundColor:[UIColor clearColor]];
        [self.addAPhotoButton setImage: [UIImage imageWithData:imageData] forState:UIControlStateNormal];
    }
    else{
        [self.addAPhotoButton setTitle:kButtonTitleAddAPhoto forState:UIControlStateNormal];
    }
    [self.CustomScrollView addSubview:self.addAPhotoButton];
        /************************add button for profile imageview***************/
    /**************adding label for user name********************/
    self.firstNameTextfield=[self createDefaultTxtfieldWithRect:CGRectMake(15,self.addAPhotoButton.frame.size.height+self.addAPhotoButton.frame.origin.y+kTopPadding, kLabelWidtheditprofile, kLabelHeighteditprofile) text:kEmptytextfield withTag:kTextfieldTagWhiteBorder withPlaceholder:@"First Name*"];
    self.firstNameTextfield.textColor = [self getColorForRedText];

    self.firstNameTextfield.font = [UIFont fontWithName:kFontSemiBold size:kFontUserName];

    [self.firstNameTextfield setTextAlignment:NSTextAlignmentCenter];
    self.firstNameTextfield.delegate=self;
    [self.CustomScrollView addSubview:self.firstNameTextfield];
    self.lastNameTextfield=[self createDefaultTxtfieldWithRect:CGRectMake(self.firstNameTextfield.frame.size.width+self.firstNameTextfield.frame.origin.x+10,self.addAPhotoButton.frame.size.height+self.addAPhotoButton.frame.origin.y+kTopPadding, kLabelWidtheditprofile, kLabelHeighteditprofile) text:[NSString stringWithFormat:kEmptytextfield ] withTag:kTextfieldTagWhiteBorder withPlaceholder:@"Last Name*"];
    self.lastNameTextfield.textColor = [self getColorForRedText];
    self.lastNameTextfield.font = [UIFont fontWithName:kFontSemiBold size:kFontUserName];
    [self.lastNameTextfield setTextAlignment:NSTextAlignmentCenter];
    self.lastNameTextfield.delegate=self;
    [self.CustomScrollView addSubview:self.lastNameTextfield];
    self.firstNameTextfield .text=userProfileObj.FirstName;
    self.lastNameTextfield.text=userProfileObj.LastName;
    /**************adding label for user name********************/



    /**************adding label for user name********************/
    
    /**************adding label for user DOB********************/
    NSString *stringToFormat = [userProfileObj DateOfBirth];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    NSDate *date = [dateFormatter dateFromString:stringToFormat];
    
    [dateFormatter setDateFormat:@"MMM dd,yyyy"];
    
    [dateFormatter stringFromDate:date];

    self.dateOfBirthTextfield=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding,self.firstNameTextfield.frame.size.height+self.firstNameTextfield.frame.origin.y+kPaddingSmall*2, kGreyBorderTextfieldWidth_Large, kLabelHeighteditprofile) text:[dateFormatter stringFromDate:date] withTag:kTextfieldTagWhiteBorder withPlaceholder:@"Date of Birth"];
    [self.dateOfBirthTextfield setFont:[UIFont fontWithName:kFontSemiBold size:kFontDOB]];
    self.dateOfBirthTextfield.delegate=self;
    [self.dateOfBirthTextfield setTextAlignment:NSTextAlignmentCenter];

    [self.CustomScrollView addSubview:self.dateOfBirthTextfield];
    [self.dateOfBirthTextfield setUserInteractionEnabled:NO];
    UIButton *btnDOB=[self createButtonWithFrame:CGRectMake(kLeftPadding,self.firstNameTextfield.frame.size.height+self.firstNameTextfield.frame.origin.y+kPaddingSmall*2, kGreyBorderTextfieldWidth_Large, kLabelHeight*2) withTitle:kEmptyString];
    btnDOB.titleLabel.font = [UIFont fontWithName:kFontBold size:kFontBoldSize+4];
    [btnDOB setBackgroundColor:[UIColor clearColor]];
    [btnDOB addTarget:self action:@selector(btnDOB:) forControlEvents:UIControlEventTouchUpInside];
    btnDOB.tag = kTagDatePickerHidden;
    [self.CustomScrollView  addSubview:btnDOB];

    /**************adding label for user DOB********************/
    
    /**************adding label for card number********************/
    UIView *loyaltyCardNumView;

    loyaltyCardNumView=[self createContentViewWithFrame:CGRectMake(0, (SCREEN_HEIGHT/2)-kTEXTfieldHeight - 35, SCREEN_WIDTH, kTEXTfieldHeight)];
    [loyaltyCardNumView setBackgroundColor:[self getColorForBlackText]];
    [self.CustomScrollView addSubview:loyaltyCardNumView];
    self.CardNumberTextFd=[self createDefaultTxtfieldWithRect:
                             CGRectMake(kPaddingSmall + 65, kPaddingSmall,SCREEN_WIDTH-((kPaddingSmall + 65) *2), kTEXTfieldHeight-(kPaddingSmall*2)) text:userProfileObj.UserLoyalityCardNo withTag:kTextfieldTagWhiteBorder withPlaceholder:kPlaceHolderLoyaltyCard];
    self.CardNumberTextFd.delegate=self;
    self.CardNumberTextFd.keyboardType = UIKeyboardTypeNumberPad;
    self.CardNumberTextFd.font = [self addFontSemiBold];
    [self.CardNumberTextFd setTextAlignment:NSTextAlignmentCenter];
     self.CardNumberTextFd.textColor=[UIColor whiteColor];;

    [loyaltyCardNumView addSubview:self.CardNumberTextFd];
    if([loyaltyEnable intValue] == 0)
    {
        loyaltyCardNumView.hidden = YES;
    }
    /**************adding label for card number********************/
    
//    userApartment=[self createDefaultTxtfieldWithRect:CGRectMake(self.firstNameTextfield.frame.size.width+self.firstNameTextfield.frame.origin.x+30,self.addAPhotoButton.frame.size.height+self.addAPhotoButton.frame.origin.y+kTopPadding, kLabelWidtheditprofile, kLabelHeighteditprofile) text:[NSString stringWithFormat:kEmptytextfield ] withTag:kTextfieldTagWhiteBorder withPlaceholder:@"Apartment/Suite*"];
//    userApartment.textColor = [self getColorForRedText];
//    userApartment.font = [UIFont fontWithName:kFontSemiBold size:kFontUserName];
//    [userApartment setTextAlignment:NSTextAlignmentCenter];
//    userApartment.delegate=self;
//    [self.CustomScrollView addSubview:userApartment];
    
    userApartment.text=userProfileObj.Address2;
      /**************adding label for address********************/
    self.addressLabel=[self createLblWithRect:CGRectMake(kLeftPadding, (loyaltyCardNumView.frame.origin.y+loyaltyCardNumView.frame.size.height)+kPaddingSmall, SCREEN_WIDTH-100, kLabelHeight) font:[self addFontSemiBold] text:kLabelAddress textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
    [self.CustomScrollView addSubview:self.addressLabel];
    
//    self.findMe=[self createButtonWithFrame:CGRectMake(self.addressLabel.frame.size.width+150,(loyaltyCardNumView.frame.origin.y+loyaltyCardNumView.frame.size.height), 40, 30) withTitle:@"Find Me"];
//    _findMe = [UIButton buttonWithType:UIButtonTypeCustom];
//    _findMe.backgroundColor=[UIColor clearColor];
//    _findMe.imageView.image=[UIImage imageNamed:@"out-of-stock"];
//    [_findMe addTarget:self action:@selector(FindMe:) forControlEvents:UIControlEventTouchUpInside];
//    [self.CustomScrollView addSubview:_findMe];

    
//        _findMe=[[UIButton alloc] initWithFrame:CGRectMake(self.addressLabel.frame.size.width+150, (loyaltyCardNumView.frame.origin.y+loyaltyCardNumView.frame.size.height)+220, 40, 30)];
//        [_findMe setTitle:@"Find Me" forState:UIControlStateNormal];
//        _findMe.imageView.image=[UIImage imageNamed:@"out-of-stock"];
//        [_findMe addTarget:self action:@selector(FindMe:) forControlEvents:UIControlEventTouchUpInside];
//      [self.CustomScrollView addSubview:_findMe];
//    

    /**************adding label for address********************/
    
    
    if (![userProfileObj.Address isEqualToString:kEmptyString]) {
        addressTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, _addressLabel.frame.origin.y+ _addressLabel.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_Large, kTEXTfieldHeightSmall) text:userProfileObj.Address withTag:kTextfieldTagGreyBorder withPlaceholder:@"Address"];
        
        userApartment=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, _addressLabel.frame.origin.y+ addressTxtFd.frame.size.height+kPaddingSmall+_addressLabel.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_Large, kTEXTfieldHeightSmall) text:userProfileObj.Address2 withTag:kTextfieldTagGreyBorder withPlaceholder:@"Apartment/Suite"];
    }
    
    else{
    addressTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, _addressLabel.frame.origin.y+ _addressLabel.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_Large, kTEXTfieldHeightSmall) text:kEmptyString withTag:kTextfieldTagGreyBorder withPlaceholder:@"Address"];
        
    userApartment=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, _addressLabel.frame.origin.y+ addressTxtFd.frame.size.height+kPaddingSmall+_addressLabel.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_Large, kTEXTfieldHeightSmall) text:kEmptyString withTag:kTextfieldTagGreyBorder withPlaceholder:@"Apartment/Suite"];
    }
    
    userApartment.delegate=self;
    [self.CustomScrollView addSubview:addressTxtFd];
    [self.CustomScrollView addSubview:userApartment];
    
    if (![userProfileObj.City isEqualToString:kEmptyString]) {
        cityTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, userApartment.frame.origin.y+ userApartment.frame.size.height+kPaddingSmall,kGreyBorderTextfieldWidth_small, kTEXTfieldHeightSmall) text:userProfileObj.City withTag:kTextfieldTagGreyBorder withPlaceholder:@"City"];

    }
    else{
        cityTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, userApartment.frame.origin.y+ userApartment.frame.size.height+kPaddingSmall,kGreyBorderTextfieldWidth_small, kTEXTfieldHeightSmall) text:kEmptyString withTag:kTextfieldTagGreyBorder withPlaceholder:@"City"];

    
    }
       cityTxtFd.delegate=self;
    [self.CustomScrollView addSubview:cityTxtFd];
    
    if (![userProfileObj.State isEqualToString:kEmptyString]) {
        stateTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kPaddingSmall+cityTxtFd.frame.origin.x+ cityTxtFd.frame.size.width, userApartment.frame.origin.y+ userApartment.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_small, kTEXTfieldHeightSmall) text:userProfileObj.State withTag:kTextfieldTagGreyBorder withPlaceholder:@"State"];
    }
    else{
    stateTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kPaddingSmall+cityTxtFd.frame.origin.x+ cityTxtFd.frame.size.width, userApartment.frame.origin.y+ userApartment.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_small, kTEXTfieldHeightSmall) text:kEmptyString withTag:kTextfieldTagGreyBorder withPlaceholder:@"State"];
    
    }
    stateTxtFd.delegate=self;
    [self.CustomScrollView addSubview:stateTxtFd];
    if (![userProfileObj.ZipCode isEqualToString:kEmptyString]) {
         zipTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, stateTxtFd.frame.origin.y+ stateTxtFd.frame.size.height+kPaddingSmall,kGreyBorderTextfieldWidth_Large, kTEXTfieldHeightSmall) text:userProfileObj.ZipCode withTag:kTextfieldTagGreyBorder withPlaceholder:@"Zip Code"];
    }
    else{
     zipTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, stateTxtFd.frame.origin.y+ stateTxtFd.frame.size.height+kPaddingSmall,kGreyBorderTextfieldWidth_Large, kTEXTfieldHeightSmall) text:kEmptyString withTag:kTextfieldTagGreyBorder withPlaceholder:@"Zip Code"];
    
    }
    zipTxtFd.keyboardType=UIKeyboardTypeNumberPad;
    zipTxtFd.delegate=self;
    [self.CustomScrollView addSubview:zipTxtFd];
    
         /**************adding label contact address title********************/
    self.contactLabel=[self createLblWithRect:CGRectMake(kLeftPadding, (zipTxtFd.frame.origin.y+zipTxtFd.frame.size.height)+kTopPadding - 7, SCREEN_WIDTH-100, kLabelHeight) font:[self addFontSemiBold] text:kLabelContact textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
    [self.CustomScrollView addSubview:self.contactLabel];
  
    self.phoneNumberTextfield=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, _contactLabel.frame.origin.y+ _contactLabel.frame.size.height+kPaddingSmall, kGreyBorderTextfieldWidth_Large, kTEXTfieldHeightSmall) text:[self getformattedPhoneNumberstringFrom:userProfileObj.ContactNo] withTag:kTextfieldTagGreyBorder withPlaceholder:@"Phone number"];
   self. phoneNumberTextfield.keyboardAppearance=UIKeyboardTypeNumbersAndPunctuation;
    self.phoneNumberTextfield.keyboardType = UIKeyboardTypeNumberPad;
    [self.CustomScrollView addSubview:self.phoneNumberTextfield];
    
    
    /*****Add email Textfield*******/
    emailTxtFd=[self createDefaultTxtfieldWithRect:CGRectMake(kLeftPadding, (self.phoneNumberTextfield.frame.origin.y+self.phoneNumberTextfield.frame.size.height)+kPaddingSmall, kGreyBorderTextfieldWidth_Large, kTEXTfieldHeightSmall) text:userProfileObj.EmailID withTag:kTextfieldTagLightGreyBorder withPlaceholder:@"Emai ID"];
    emailTxtFd.userInteractionEnabled=NO;
    [emailTxtFd setTextColor:[UIColor lightGrayColor]];
    emailTxtFd.keyboardAppearance=UIKeyboardTypeEmailAddress;
    emailTxtFd.delegate=self;
    [self.CustomScrollView addSubview:emailTxtFd];
    
       /**************adding button for  edit********************/
    
    CGRect ContentainerViewFrame ;
    ContentainerViewFrame.origin.x=0;
    ContentainerViewFrame.origin.y=SCREEN_HEIGHT-kBottomViewHeight-kCustomButtonContainerHeight;
    ContentainerViewFrame.size.width=SCREEN_WIDTH;
    ContentainerViewFrame.size.height=kCustomButtonContainerHeight;
    _SaveButton=  [self AddCustomButtonInsideContentViewWithFrame:ContentainerViewFrame];
    [self.SaveButton setTitle:kButtonTittleSAVE forState:UIControlStateNormal];
    [self.SaveButton addTarget:self action:@selector(SaveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)FindMe:(UIButton*)button
{
   [self showSpinner];
     [location startUpdatingLocation];
      if([CLLocationManager locationServicesEnabled])
      {
         // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
          if([CLLocationManager locationServicesEnabled]){
              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your Location"
                                                              message:[NSString stringWithFormat:@"%@ \n\n %@",getAddress,@"Do you want to keep this address as your profile address?"]
                                                             delegate:self
                                                    cancelButtonTitle:@"No"
                                                    otherButtonTitles:@"Yes",nil];
              alert.tag=103;
              [alert show];

          
          
      }
      }
      else{
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SETTINGS"
                                                          message:@"Enable Location Provider!Go to settings menu?"
                                                         delegate:self
                                                cancelButtonTitle:@"Settings"
                                                otherButtonTitles:@"Cancel",nil];
          alert.tag=102;
          [alert show];

            [location startUpdatingLocation];
          
      }
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag==102) {
        if (buttonIndex == 0) {
           // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
             if([CLLocationManager locationServicesEnabled]){
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your Location"
                                                                 message:getAddress
                                                                delegate:self
                                                       cancelButtonTitle:@"Do you want to keep this address as your profile address?"
                                                       otherButtonTitles:@"Yes",@"No",nil];
                 alert.tag=103;
                 [alert show];

                 
             }
             else{
                 [self hideSpinner];
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                 message:@"Location services is disable"
                                                                delegate:self
                                                       cancelButtonTitle:@"Ok"
                                                       otherButtonTitles:nil];
                 alert.tag=104;
                 [alert show];

             }
            
        }
        }
    else if (alertView.tag==103) {
         [self hideSpinner];
        if (buttonIndex == 1) {
            cityTxtFd.text=city;
            stateTxtFd.text=state;
            zipTxtFd.text=zipcode;
            
            addressTxtFd.text=addresstotal1;
//            if(addresstotal2 !=nil){
//            userApartment.text=addresstotal1;
//            }
//            else{
//                 userApartment.text=@"";
//            }
            
          //  [location stopUpdatingLocation];
        }
        
    }
   }

-(void)callApiAddressValidator:(NSDictionary*)dict{
    // getEventsWithEventsList
    [self showSpinner]; // To show spinner
    
    
    [[ModelManager modelManager] GetAddressValidator:AdressValidatorDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
      //  NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            if ([[response valueForKey:@"Status"] intValue]==0) {
                [self showAlertViewWithTitle:nil message:kAlertInvalidAddress];
            }
            else{
                if ([self formValidate]) {
                    [self CallApiEdit];
                    
                }

            }
          
                   });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [UIView showMessageWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage] showInterval:5.0  viewController:self];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        
    }];
    
    
    
}
-(void)getAddressValidatorDict{
    AdressValidatorDict = [NSMutableDictionary dictionary];
    __weak User *userObj=kGetUserModel;
    [AdressValidatorDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [AdressValidatorDict setObject:userObj.userId forKey: kParamUserId];
    [AdressValidatorDict setObject:self.firstNameTextfield.text forKey: kParamFirstName];
    [AdressValidatorDict setObject:self.lastNameTextfield.text forKey: kParamLastName];
    [AdressValidatorDict setObject:addressTxtFd.text forKey: kParamAddress];
     [AdressValidatorDict setObject:userApartment.text forKey: kParamApartment];
    [AdressValidatorDict setObject:cityTxtFd.text forKey: kParamCity];
    [AdressValidatorDict setObject:stateTxtFd.text forKey: kParamState];
    [AdressValidatorDict setObject:kEmptyString forKey: kParamCountryId];
    [AdressValidatorDict setObject:zipTxtFd.text forKey: kParamZip];
    [AdressValidatorDict setObject:[self getOriginalPhonestringFromFormated:self.phoneNumberTextfield.text] forKey: kParamPhone];
      [self callApiAddressValidator:AdressValidatorDict];
    

}
-(void)CallApiEdit{
    NSMutableDictionary *editProfileDict=[[NSMutableDictionary alloc] init];
    
    if([self formValidate])
    {
           //self.dateOfBirthTextfield.text
        User *userobj=kGetUserModel;
        [editProfileDict setObject:userobj.userId forKey: kParamAuthenticationID];

        [editProfileDict setObject:[emailTxtFd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]forKey: kSignupParamEmailId];
        [editProfileDict setObject:[self.firstNameTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]forKey: kSignupParamFirstName];
        [editProfileDict setObject:[self.lastNameTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]forKey: kSignupParamLastName];
        [editProfileDict setObject:[[self getOriginalPhonestringFromFormated:self.phoneNumberTextfield.text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]forKey: kSignupParamContactNumber];
        NSString*datestring=  [self dateStringFromString:self.dateOfBirthTextfield.text sourceFormat:kClientDateFormat destinationFormat:@"dd-MM-yyyy"];
        if (datestring==nil) {
            
            NSString *stringToFormat = self.dateOfBirthTextfield.text;
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            
            [dateFormatter setDateFormat:@"MM-dd-yyyy"];
            
            NSDate *date = [dateFormatter dateFromString:stringToFormat];
            
            [dateFormatter setDateFormat:@"MM-dd-yyyy"];
            
            [dateFormatter stringFromDate:date];
            DLog(@"%@",[dateFormatter stringFromDate:date]);
            [editProfileDict setObject:[dateFormatter stringFromDate:date] forKey: kSignupParamdateofBirth];

        }
        else{
            [editProfileDict setObject:[self.dateOfBirthTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey: kSignupParamdateofBirth];

        }
        
        DLog(@"datestring---%@",datestring);
        
               DLog(@"image--%@",self.userProfileImage);
        [editProfileDict setObject:@"" forKey: kSignupParamGender];
        [editProfileDict setObject:@"" forKey: kSignupParamCountryId];
        [editProfileDict setObject:[userApartment.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey: kSignupParamApartment];
        [editProfileDict setObject:[addressTxtFd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey: kSignupParamAddress];
        [editProfileDict setObject:[stateTxtFd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey: kSignupParamStateName];
        [editProfileDict setObject:[cityTxtFd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey: kSignupParamCity];
        
        [editProfileDict setObject:[zipTxtFd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey: kSignupParamZipCode];
        NSString *cardnumber;
//        if ([self.CardNumberTextFd.text isEqualToString:kEmptyCardNumber]) {
//           cardnumber=kEmptyString;
//        }
//        else{
//           
//        }
        cardnumber=self.CardNumberTextFd.text;
        DLog(@"cardnumber-%@",cardnumber);
        [editProfileDict setObject:[cardnumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey: kSignupParamUserLoyalityCardNo];
        __weak UserProfile *userProfileObj = [[ModelManager modelManager] getUserProfileModel];
        DLog(@"updatedImageUrl-%@",self. updatedImageUrl);
        
        if (self. updatedImageUrl) {
            [editProfileDict setObject:self. updatedImageUrl forKey: kSignupParamUserImage];
        }
        else{
            [editProfileDict setObject:userProfileObj.UserImage forKey: kSignupParamUserImage];
        }
        [editProfileDict setObject:kSTOREIDbottlecappsString forKey: kSignupParamstoreid2];
        if (TARGET_IPHONE_SIMULATOR){
            [editProfileDict setObject:kDEVICETOKENString forKey: kUserDeviceId];
            
        }
        else{
            [editProfileDict setObject:[[ModelManager modelManager] getCurrentDeviceToken] forKey: kSignupParamDeviceId];
            
        }
        NSString *isProfileUpdatedString;
        isProfileUpdatedString=@"1";
//        if ([addressTxtFd.text length]&&[cityTxtFd.text length ]&&[stateTxtFd.text length]&&[zipTxtFd.text length]) {
//            isProfileUpdatedString=@"1";
//        }
//        else{
//        isProfileUpdatedString=@"0";
//        }
        [editProfileDict setObject:@"6" forKey: kSignupParamRegionId];
        [editProfileDict setObject:@"I" forKey: kSignupParamDeviceType];
        [editProfileDict setObject:isProfileUpdatedString forKey: kEditProfile_IsProfileUpdated];

        DLog(@"editProfileDict---%@",editProfileDict);
        
        [self showSpinner]; // To show spinner
        [[ModelManager modelManager] EditUserAccount:editProfileDict  successStatus:^(BOOL status,NSString*Authentication) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.8 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self hideSpinner];

                if ([Authentication isEqualToString:@"true"]) {
                    [self showAlertViewWithTitle:nil message:kAlertEditUserProfileSucess];
                    [self GoBack];

                }
                else{
                    [self showAlertViewWithTitle:nil message:kAlertEditUserProfileFailed];

                }
                
            });
        } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
            [self hideSpinner];
            [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
            DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
            
            
        }];
        
        
        
    }
    
    
}


#pragma mark-viewlifeCycle
- (void) UploadimageNotification:(NSNotification *) notification{
    NSURL* ImageURL = [NSURL URLWithString:self. updatedImageUrl];
    NSData *imageData = [NSData dataWithContentsOfURL:ImageURL];
    DLog(@"UIImage%@",[UIImage imageWithData:imageData]);
    if ( self.updatedImageUrl) {
        [self.addAPhotoButton setTitle:kEmptyString forState:UIControlStateNormal];
        [self.addAPhotoButton setBackgroundColor:[UIColor clearColor]];
        [self.addAPhotoButton setImage: [UIImage imageWithData:imageData] forState:UIControlStateNormal];
    }
    else{
        [self.addAPhotoButton setTitle:kButtonTitleAddAPhoto forState:UIControlStateNormal];
    }
    //
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(UploadimageNotification:)
                                                 name:@"Uploadimage"
                                               object:nil];
}
-(void)viewDidLoad{
    [super viewDidLoad];
    [self CreateUI];
    location = [[CLLocationManager alloc] init];
    location.delegate = self;
    [location startUpdatingLocation];
    userphoneTxtFd.delegate=self;
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    if(locations.count>0){
   loc = [locations lastObject];
    NSLog(@"lat%f - lon%f", loc.coordinate.latitude, loc.coordinate.longitude);
   //NSString *address = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true",32.89473,-96.93959];
    NSString *address = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true",loc.coordinate.latitude,loc.coordinate.longitude];

    [self getLocationFromAddressString:address];
    }
    else{
        [self showAlertViewWithTitle:@"Message" message:@"We are not able to find your current location right now,please try again!"];
    }
}

-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr {
    double latitude = 0, longitude = 0;
  // NSString *req = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true",32.89473,-96.93959];
    
     NSString *req = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true",loc.coordinate.latitude,loc.coordinate.longitude];
     NSError *e1 = nil;
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:&e1];
    NSData* data = [result dataUsingEncoding:NSUTF8StringEncoding];
    NSError *e = nil;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
    
    if (!jsonArray)
    {
        NSLog(@"Error parsing JSON: %@", e);
    }
    else {
        NSArray *resultFromJson=[jsonArray valueForKey:@"results"];
        getAddress=[[resultFromJson objectAtIndex:0]objectForKey:@"formatted_address"];
        NSArray *subStrings = [getAddress componentsSeparatedByString:@","]; //or rather @" - "
        getAddress = [getAddress stringByReplacingOccurrencesOfString:@", USA"
                                                               withString:@""];
        
        addresstotal1 = [NSString stringWithFormat:@"%@", [subStrings objectAtIndex:0]];

        NSArray *innerArray=[[resultFromJson valueForKey:@"address_components"] objectAtIndex:0];
        for (NSDictionary *dict in innerArray){
         
            if([[[dict valueForKey:@"types"] objectAtIndex:0]isEqualToString:@"country"])
            {
               country=[dict valueForKey:@"long_name"];
                
            }
            if([[[dict valueForKey:@"types"] objectAtIndex:0]isEqualToString:@"postal_code"])
            {
                zipcode=[dict valueForKey:@"long_name"];
                
            }
            if([[[dict valueForKey:@"types"] objectAtIndex:0]isEqualToString:@"locality"])
            {
                city=[dict valueForKey:@"long_name"];
                
                
            }
            if([[[dict valueForKey:@"types"] objectAtIndex:0]isEqualToString:@"administrative_area_level_1"])
            {
               state=[dict valueForKey:@"long_name"];
             
                
            }
          
        }
        
        

    }
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude = longitude;
    
   // NSLog(@"View Controller get Location Logitute : %f",center.latitude);
   // NSLog(@"View Controller get Location Latitute : %f",center.longitude);
    [location stopUpdatingLocation];
    return center;
    
}

#pragma mark - SlideNavigationController Methods -
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu  {
    return YES;
}
#pragma mark-BUTTON ACTION

-(void)SaveButtonClicked:(UIButton*)button {
    if([[self.firstNameTextfield.text
         stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )  {
         [self showAlertViewWithTitle:nil message:kAlertEnterfirstName];
        return;
    }
    else if([[self.lastNameTextfield.text
              stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )  {
        [self showAlertViewWithTitle:nil message:kAlertEnterLastName];
        return;
    }
  
        else if([[self.phoneNumberTextfield.text
              stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 )  {
        [self showAlertViewWithTitle:nil message:kAlertEnterPhone];
        return;
    }
    else if ([addressTxtFd.text length]||[stateTxtFd.text length]||[cityTxtFd.text length]||[zipTxtFd.text length]){
        if (![addressTxtFd.text length]) {
            [self showAlertViewWithTitle:nil message:kAlertEnterAddress];
        }
        else if (![cityTxtFd.text length]){
            [self showAlertViewWithTitle:nil message:kAlertEnterCity];
        }
//        else if (![userApartment.text length]){
//            [self showAlertViewWithTitle:nil message:kAlertEnterApartment];
//        }
        else if (![stateTxtFd.text length]){
            [self showAlertViewWithTitle:nil message:kAlertEnterState];
        }
        else if ([zipTxtFd.text length]==0){
            [self showAlertViewWithTitle:nil message:kAlertEnterZipCode];
            return;
        }
        else if ([zipTxtFd.text length]<5){
            [self showAlertViewWithTitle:nil message:kAlertValidZipCode];
            return;
        }
        else if ([zipTxtFd.text length]>5){
            [self showAlertViewWithTitle:nil message:kAlertValidZipCode];
            return;
        }
        else    {
            if ([self formValidate]) {
                [self CallApiEdit];
            }
        }
        /***************************Removed by Chethana*****************************/
        //else    {
        //    [self getAddressValidatorDict];
        //}
        /***************************Removed by Chethana*****************************/
    }
    else {
        if ([self formValidate]) {
            [self CallApiEdit];
        }
    }
}


#pragma mark-textfield delegate method

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if ([textField.text isEqualToString: kEmptytextfield]) {
        textField.text=kEmptyString;
    }
    if([self.dropDownView isDescendantOfView:[self view]]){
        [self.dropDownView setHidden:YES];
    }
        if (textField==self.dateOfBirthTextfield) {
        [textField resignFirstResponder];
        [self addDatePickerToView ];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (  range.location==0 && [string isEqualToString: @" "] ) {
        return NO;
    }

    if (textField == zipTxtFd) {
        if ([string length] > 0) {
            if ([textField.text length] == 5)
                return NO;
            
            zipTxtFd.text = [NSString stringWithFormat:@"%@%@", zipTxtFd.text, string];
        }else {
            //Back Space do manual backspace
            if ([zipTxtFd.text length] > 1) {
                zipTxtFd.text = [zipTxtFd.text substringWithRange:NSMakeRange(0, [zipTxtFd.text length] - 1)];
            } else {
                zipTxtFd.text = @"";
            }
        }
        return NO;
    }
    if(textField==self.phoneNumberTextfield){
        int length = (int)[self getLength:textField.text];
        //NSLog(@"Length  =  %d ",length);
        
        if(length == 10)
        {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 3)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"(%@) ",num];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
        }
        else if(length == 6)
        {
            NSString *num = [self formatNumber:textField.text];
            //NSLog(@"%@",[num  substringToIndex:3]);
            //NSLog(@"%@",[num substringFromIndex:3]);
            textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
        }
         return YES;
    }
    return YES;
}
- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        NSLog(@"%@", mobileNumber);
        
    }
    
    return mobileNumber;
}

- (int)getLength:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
}
- (void)operationDidFinishWitUrl:(NSString *)url{
    [self hideSpinner];

   }


- (void)operationDidFailWithResponse:(NSHTTPURLResponse *)response {
    [self hideSpinner];
    
}


- (void)operationDidFailWithError:(NSError *)error {
    [self hideSpinner];

    
}

#pragma mark-CallDOBdelegate Method
-(void)GetDobEditedMethod:(NSString*)selectedDob{
    self.dateOfBirthTextfield.text=selectedDob;

}
@end
