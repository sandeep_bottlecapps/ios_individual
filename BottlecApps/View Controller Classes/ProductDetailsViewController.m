//
//  ProductDetailsViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 9/3/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ProductDetailsViewController.h"
#import "ReviewViewController.h"
#import "Product.h"
#import "QALabel.h"

#define kproductImgeHeightWidth (kLabelHeight*3)
#define krowHeightProdDetail (kLabelHeight*4)
#define kpaddingForProductname  (ImageBGv.frame.size.width)+(ImageBGv.frame.origin.x)+15
#define kFontTitle [UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize-2]
@interface ProductDetailsViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSURL               *productImageURL;
    UIImage             *productImage;
    NSMutableArray  *arrayForBoolFav;
    NSMutableArray  *arrayForBoolCart;
    NSMutableArray *reviewArray;
    NSMutableArray      *cartArray;
    AsyncImageView *ProductImageView;
    QALabel *productNameLabel;
    UILabel *PriceLabel;
    UILabel *ValuesLabel;
    UITextView *tastingTextView;
    UILabel *reviewCountLabel;

    UIView *ImageBGv;
    UIImageView *separatorIView;
    UILabel*productSalePriceLabel;
    UILabel *producDetailLabel;
    
    UIButton*  reviewButton;

    Product *productObj;
}

@property (nonatomic)  NSInteger getReviewsCount;

//-(NSString*)getReviewCount;
//-(NSArray*)getReviewArray;

@end

@implementation ProductDetailsViewController
@synthesize productIDSelected,getproduct,isfav,getProductarray;
-(void)getProductImage{
    
    productImageURL = [NSURL URLWithString:productObj.productImageURL];
    NSData *imageData = [NSData dataWithContentsOfURL:productImageURL];
    productImage = [UIImage imageWithData:imageData];
}

-(void)UpdateUI{
      NSURL *producturl= [NSURL URLWithString:productObj.productImageURL];
    [ProductImageView setImageURL:producturl];
    [ProductImageView setContentMode:UIViewContentModeScaleAspectFit];
    ProductImageView.layer.masksToBounds = YES;
    
    [productNameLabel setText:[self ProperValue:productObj.productName]];
    productNameLabel.verticalAlignment = UIControlContentVerticalAlignmentBottom;
    
    self.starRating.rating= [productObj.productRating floatValue];
    
    if ([productObj.productSalesPrice isEqualToString:kEmptyString]||[productObj.productSalesPrice isEqualToString:kEmptySalePrice]) {
        [PriceLabel setText:kEmptytextfield];
    }
    else{
        [PriceLabel setText:[self addDollarSignToString:productObj.productPrice]];
    }
    if ([productObj.productSalesPrice isEqualToString:kEmptyString]||[productObj.productSalesPrice isEqualToString:kEmptySalePrice]) {
        [PriceLabel setText:[self addDollarSignToString:productObj.productPrice]];
    }
    else{
        NSString *tempstr=[self addDollarSignToString:productObj.productPrice];
        [PriceLabel setAttributedText:[self addDollarSignTocutOverString:tempstr]];
        [productSalePriceLabel setText:[self addDollarSignToString:productObj.productSalesPrice]];
    }
    
    [separatorIView setBackgroundColor:[self getColorForLightGreyColor]];
   
    for (int i=0; i<4; i++) {
        
             switch (i) {
            case 0:
                if (![productObj.productSize isEqualToString:kEmptyString]) {
                    [ValuesLabel setText:productObj.productSize];
                    
                }
                
                break;
            case 1:
                if (![productObj.productSize isEqualToString:kEmptyString]) {
                    [ValuesLabel setText:productObj.productSubCatg];
                    
                }
                break;
            case 2:
                if (![productObj.ProductSku isEqualToString:kEmptyString]) {
                    [ValuesLabel setText:productObj.ProductSku];
                    
                }
                break;
            case 3:{
                if (![productObj.productUpc isEqualToString:kEmptyString]) {
                    [ValuesLabel setText:productObj.productUpc];
                    
                }
            }
                break;
            default:
                break;
        }
        
        
    }
    
    if (![productObj.productDesc isEqualToString:kEmptyString]) {
        tastingTextView.text=productObj.productDesc;
    }
    [separatorIView setBackgroundColor:[self getColorForLightGreyColor]];
    
    [self setReviewCountLabelWithValue:self.getReviewsCount];
    
    for (int i=0; i<4; i++) {
        UILabel *Titlelabel=[self createLblWithRect:CGRectMake(30, producDetailLabel.frame.origin.y+producDetailLabel.frame.size.height+(i*kLabelHeight), kLabelWidth, kLabelHeight) font:kFontTitle text:kEmptyString textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
        switch (i) {
            case 0:
                [Titlelabel setText:@"Size:"];
                break;
            case 1:
                [Titlelabel setText:@"Type:"];
                break;
            case 2:
                [Titlelabel setText:@"SKU:"];
                break;
            case 3:{
                [Titlelabel setText:@"UPC:"];
            }
                break;
            default:
                break;
        }
        Titlelabel.tag=i;
        [self.CustomScrollView addSubview:Titlelabel];
        
        
        ValuesLabel=[self createLblWithRect:CGRectMake((Titlelabel.frame.origin.x)+(Titlelabel.frame.size.width), producDetailLabel.frame.origin.y+ producDetailLabel.frame.size.height+(i*kLabelHeight),kLabelWidth*2, kLabelHeight) font:[self addFontRegular] text:kEmptytextfield textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
        [ValuesLabel setTag:i];
        for (int i=0; i<4; i++) {
            
            switch (ValuesLabel.tag) {
                case 0:
                    if ([productObj.productSize length]) {
                        [ValuesLabel setText:productObj.productSize];
                    }
                    break;
                case 1:
                    if ([productObj.productCatName length]) {
                        [ValuesLabel setText:productObj.productCatName];
                    }
                    break;
                case 2:
                    if ([productObj.ProductSku length]) {
                        [ValuesLabel setText:productObj.ProductSku];
                    }
                    break;
                case 3:{
                    if ([productObj.productUpc length]) {
                        [ValuesLabel setText:productObj.productUpc];
                    }
                }
                    break;
                default:
                    break;
            }
            
        }
        [ValuesLabel setBackgroundColor:[UIColor clearColor]];
        [self.CustomScrollView addSubview:ValuesLabel];
    }
}

-(void) setReviewCountLabelWithValue:(NSInteger) reviewCount  {
    if(reviewCount <= 0)   {
        [reviewCountLabel setText: kEmptyString];
        reviewCountLabel.hidden = YES;
        [reviewButton setImage:[UIImage imageNamed:kImageReviewsPlainButton] forState:UIControlStateNormal];
    }
    else    {
        reviewCountLabel.hidden = NO;
        [reviewButton setImage:[UIImage imageNamed:kImageReviewsButton] forState:UIControlStateNormal];
        [reviewCountLabel setText:[NSString stringWithFormat:@"%ld",(long)reviewCount]];
    }
}

-(void)createUI{
    ProductImageView=[self createAsynImageViewWithRect:CGRectMake(3, 5 , 76, 77) imageUrl:nil isThumnail:YES];
    
    ImageBGv=[self createContentViewWithFrame:CGRectMake(0, kPaddingcartAndFav, krowHeightProdDetail, krowHeightProdDetail-(kPaddingcartAndFav*2))];
    [ImageBGv setBackgroundColor:[UIColor whiteColor]];
    [self.CustomScrollView addSubview:ImageBGv];

    [self.CustomScrollView addSubview:ProductImageView];
    
    productNameLabel= [[QALabel alloc] initWithFrame:CGRectMake((ImageBGv.frame.size.width)+(ImageBGv.frame.origin.x)+15, 3, SCREEN_WIDTH-(kpaddingForProductname*2 - 10), kLabelHeight*2)];
    productNameLabel.font = kFontTitle;
    productNameLabel.textAlignment = NSTextAlignmentLeft;
    productNameLabel.textColor = [self getColorForRedText];
    productNameLabel.tag=kTagProductName;
    productNameLabel.numberOfLines=3;
    productNameLabel.minimumScaleFactor = 0.8;
    productNameLabel.adjustsFontSizeToFitWidth = YES;
    [self.CustomScrollView addSubview:productNameLabel];
    
    UIImage *redStarImage=[UIImage imageNamed:kImageredstar];
    UIImage *grayStarImage=[UIImage imageNamed:kImagegreystar];

    self.starRating=[[EDStarRating alloc ]initWithFrame:CGRectMake((ImageBGv.frame.size.width)+(ImageBGv.frame.origin.x)-kPaddingSmall,(productNameLabel.frame.origin.y)+(productNameLabel.frame.size.height), redStarImage.size.width*6, redStarImage.size.height) ];
    [self.CustomScrollView addSubview:self.starRating];
    [self.starRating setBackgroundColor:[UIColor clearColor]];
    [self.CustomScrollView setBackgroundColor:[UIColor clearColor]];
    self.starRating.starImage =grayStarImage ;
    self.starRating.starHighlightedImage = redStarImage;
    self.starRating.maxRating = 5.0;
    self.starRating.delegate = self;
    self.starRating.horizontalMargin = 17;
    self.starRating.editable=NO;
    self.starRating.tintColor = [self getColorForRedText];
    self.starRating.displayMode=EDStarRatingDisplayAccurate;
    self.starRating.rating= 0.0;
    //
    PriceLabel=[self createLblWithRect:CGRectMake((ImageBGv.frame.size.width)+(ImageBGv.frame.origin.x)+15, (self.starRating.frame.size.height)+(self.starRating.frame.origin.y),  kLabelWidth/2, kLabelHeight) font:[self addFontRegular] text:kEmptytextfield textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
    [self.CustomScrollView addSubview:PriceLabel];
    
    
   productSalePriceLabel=[self createLblWithRect:CGRectMake((PriceLabel.frame.size.width)+(PriceLabel.frame.origin.x), (self.starRating.frame.size.height)+(self.starRating.frame.origin.y), kLabelWidth/2, kLabelHeight) font:[UIFont fontWithName:kFontBold size:kFontBoldSize-2] text:kEmptyString textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
    [productSalePriceLabel setBackgroundColor:[UIColor clearColor]];
    [self.CustomScrollView addSubview:productSalePriceLabel];

    
    
    UIButton *FavButton=[self createButtonWithFrame:CGRectMake(SCREEN_WIDTH-kheartIconWidth-(kPaddingSmall*2), (productNameLabel.frame.origin.y)+(productNameLabel.frame.size.height)+2, kheartIconWidth, kHeartIconHeight) withTitle:kEmptyString];
    [FavButton addTarget:self action:@selector(favButton:) forControlEvents:UIControlEventTouchUpInside];
    if ([isfav intValue]==0) {
        [FavButton setImage:[UIImage imageNamed:kImageHeartIconUnselected] forState:UIControlStateNormal];
        [FavButton setBackgroundColor:[UIColor clearColor]];
        [FavButton setSelected:NO];
    }
    else{
        [FavButton setImage:[UIImage imageNamed:kImageHeartIconSelected] forState:UIControlStateNormal];
        [FavButton setBackgroundColor:[UIColor clearColor]];
        [FavButton setSelected:YES];
    }

    [self.CustomScrollView addSubview:FavButton];
    
    UIView *seperatorV=[self createContentViewWithFrame:CGRectMake(0, ProductImageView.frame.size.height+ProductImageView.frame.origin.y+5, SCREEN_WIDTH, 0.3)];
    [seperatorV setBackgroundColor:[UIColor colorWithRed:0.9255 green:0.9255 blue:0.9255 alpha:1.0]];
    [self.CustomScrollView addSubview:seperatorV];
    [seperatorV.layer setShadowColor:[UIColor blackColor].CGColor];
    [seperatorV.layer setShadowOpacity:0.8];
    [seperatorV.layer setShadowRadius:0.6];
    [seperatorV.layer setShadowOffset:CGSizeMake(0.5, 0.5)];
    
    producDetailLabel=[self createLblWithRect:CGRectMake(30, (ImageBGv.frame.size.height)+(ImageBGv.frame.origin.y)+20, kcustomButtonwidth-(kPaddingSmall*2), kLabelHeight) font:kFontTitle text:klabelProductDetails textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
    [self.CustomScrollView addSubview:producDetailLabel];
   
    UILabel *tastingLabel=[self createLblWithRect:CGRectMake(30, producDetailLabel.frame.origin.y+producDetailLabel.frame.size.height+(5*kLabelHeight), kCustomScrollViewWidth-(2*kLeftPadding),kLabelHeight) font:kFontTitle text:@"Tasting Notes:" textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
    [self.CustomScrollView addSubview:tastingLabel];
    //monika 16 Nov
    tastingTextView=[self createDefaultTxtViewWithRect:CGRectMake(30 - 5, tastingLabel.frame.size.height+tastingLabel.frame.origin.y+kPaddingSmall, kCustomScrollViewWidth - 50,100) text:kEmptytextfield withTextColor:[self getColorForBlackText] withfont:[self addFontRegular]];
    tastingTextView.editable = NO;
    tastingTextView.dataDetectorTypes = UIDataDetectorTypeAll;
    tastingTextView.textAlignment = NSTextAlignmentJustified;
    tastingTextView.textContainer.lineBreakMode = NSLineBreakByCharWrapping;

    [tastingTextView setBackgroundColor:[UIColor clearColor]];

    [self.CustomScrollView addSubview:tastingTextView];
    UIImage  *addtocartImage=[UIImage imageNamed:kImageAddtocartButton];
    UIButton *  addToCartButton=  [self createButtonWithFrame:CGRectMake((SCREEN_WIDTH-(addtocartImage.size.width))/2, tastingTextView.frame.origin.y+tastingTextView.frame.size.height+20, (addtocartImage.size.width),(addtocartImage.size.height)) withTitle:kEmptyString];
    [addToCartButton setImage:addtocartImage forState:UIControlStateNormal];
    [addToCartButton setBackgroundColor:[UIColor clearColor]];
    [addToCartButton addTarget:self action:@selector(addToCartButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.CustomScrollView addSubview:addToCartButton];
    
    UIImage *reviewbuttonImage=[UIImage imageNamed:kImageReviewsPlainButton];
    
    reviewButton =  [self createButtonWithFrame:CGRectMake((SCREEN_WIDTH-reviewbuttonImage.size.width)/2, addToCartButton.frame.origin.y+addToCartButton.frame.size.height+10, reviewbuttonImage.size.width,reviewbuttonImage.size.height) withTitle:kEmptyString];
    [reviewButton setImage:reviewbuttonImage forState:UIControlStateNormal];
    [reviewButton setBackgroundColor:[UIColor clearColor]];

    [reviewButton addTarget:self action:@selector(reviewButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.CustomScrollView addSubview:reviewButton];
    
   reviewCountLabel=[self createLblWithRect:CGRectMake((reviewButton.frame.size.width)-15, 0, 15, (reviewbuttonImage.size.height)/2) font:[self addFontBold] text:@"" textAlignment:NSTextAlignmentCenter textColor:[UIColor whiteColor]];
    [reviewCountLabel setBackgroundColor:[UIColor clearColor]];
    [reviewButton addSubview:reviewCountLabel];
}

-(void)CallApiGetreviewwithProductId:(NSString*)getProductID{
    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [paramDict setObject:@"1" forKey: kParamPageNumber];
    [paramDict setObject:kPageSize forKey: kParamPageSize];
    [paramDict setObject:getProductID forKey: kParamProductID];
    [self showSpinner];
    [[ModelManager modelManager] GetReviewWithParam:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
        NSLog(@"Getreview--%@", response);
            [self hideSpinner];
            @try {
                reviewArray=[response valueForKey:@"Getreview"];
                self.getReviewsCount=[reviewArray count];
                [self setReviewCountLabelWithValue: self.getReviewsCount];
            }
            @catch (NSException *exception) {
                self.getReviewsCount=0;
                [self setReviewCountLabelWithValue: 0];
            }
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        self. getReviewsCount=0;
        [self setReviewCountLabelWithValue: 0];
    }];
}

-(void)callApiProductDetail{
    NSMutableDictionary* paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    if (productIDSelected) {
        [paramDict setObject:productIDSelected forKey: kParamProductID];

    }
    // getEventsWithEventsList
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] GetProductDetailWithParam:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
        NSLog(@"%@", response);
            [self hideSpinner];
            
            @try {
                  productObj=[[Product alloc] initWithDictionary:[[response valueForKey:@"GetProductDetailResult"] objectAtIndex:0]];

                [self UpdateUI];
                           }
            @catch (NSException *exception) {
                [self showAlertViewWithTitle:kError message:kInternalInconsistencyErrorMessage];

                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
            
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}

-(void)initializeArrayAndDictionary{
    reviewArray=[NSMutableArray new];
    cartArray = [NSMutableArray new];
}
-(void)viewWillAppear:(BOOL)animated{
    DLog(@"productIDSelected----%@",productIDSelected);
    [self initializeArrayAndDictionary];
    [self CallApiGetreviewwithProductId:productIDSelected];

    [self callApiProductDetail];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addScrollView];
    [self.CustomScrollView setBackgroundColor:[self getColorForLightGreyColor]];
    (IS_IPHONE_4_OR_LESS? [self.CustomScrollView setContentSize:CGSizeMake(self.CustomScrollView.frame.size.width, self.CustomScrollView.frame.size.height+100)]:IS_IPHONE_6?[self.CustomScrollView setContentSize:CGSizeMake(self.CustomScrollView.frame.size.width, self.CustomScrollView.frame.size.height)]:[self.CustomScrollView setContentSize:CGSizeMake(self.CustomScrollView.frame.size.width, self.CustomScrollView.frame.size.height)]);
    [self createUI];
    [self.CustomScrollView setContentInset:UIEdgeInsetsMake(7, 0, 0, 0)];
    [self.CustomScrollView setContentOffset:CGPointMake(0, -7)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-BUTTON clicked
-(void)favButton:(UIButton*)button{
    if (![button isSelected]) {
        [button setSelected:YES];
        [button setImage:[UIImage imageNamed:kImageHeartIconSelected] forState:UIControlStateNormal];
        [arrayForBoolFav replaceObjectAtIndex:button.tag withObject:[NSNumber numberWithBool:YES]];
        [self CallApiAddFavoriteWithProductID:productIDSelected];
    }
    else{
        [button setSelected:NO];
        [button setImage:[UIImage imageNamed:kImageHeartIconUnselected] forState:UIControlStateNormal];
        [arrayForBoolFav replaceObjectAtIndex:button.tag withObject:[NSNumber numberWithBool:NO]];
        [self CallApiRemoveFavoriteWithProductID:productIDSelected];
    }
}
-(void)reviewButton:(UIButton*)buton{
    ReviewViewController *vc=[self viewControllerWithIdentifier:kReviewViewControllerStoryboardId];
    vc.getProductID = productIDSelected;
    vc.getProductObj=productObj;
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)addToCartButton:(UIButton*)button{
    DLog(@"getproduct--%@",getProductarray);


       if (![button isSelected]) {
        NSString *msg = [[ShoppingCart sharedInstace] addProductToMyShoppingCart:getProductarray];
        if ([msg isEqualToString:@"Product added to Shopping Cart successfully"]) {
            [button setSelected:YES];
        }
        NSMutableArray   *searchResult=[[NSMutableArray alloc] initWithArray:[[ShoppingCart sharedInstace] fetchProductFromShoppingCart]] ;
        if(searchResult.count>0){
            [self showAlertViewWithTitle:nil message:msg];
        }
        else{
            [self showAlertViewWithTitle:nil message:msg];
        }
    }
    else{
        [button setSelected:NO];
        [[ShoppingCart sharedInstace] removeProductFromMyShoppingCart:getProductarray];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton*)button {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//    
//    if([segue.identifier isEqualToString:kGoToReviewViewControllerSegueId]){
//        
//        ReviewViewController *controller =[segue destinationViewController];
//        controller.getProductID = productIDSelected;
//        controller.getProductObj=productObj;
//        controller.getReviewArray=reviewArray;
//    
//    }
//    
//}


@end
