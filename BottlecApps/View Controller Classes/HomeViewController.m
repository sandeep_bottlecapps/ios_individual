
//
//  HomeViewController.m
//  BottlecApps
//
//  Created by Kuldeep Tyagi on 8/4/15.
//  Copyright (c) 2015 Kuldeep Tyagi. All rights reserved.
//

#import "HomeViewController.h"
#import "PageControl.h"
#import "EventsDetailViewController.h"
#import "AsyncImageView.h"
#import "NotificationHistoryViewController.h"
#import "EventsListViewController.h"
#import "Constants.h"

#define iCraiselHeight (SCREEN_HEIGHT/2)-kNavigationheight-kTopPadding_small + 35
@interface HomeViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableDictionary *apiResponseDictionary;
    NSMutableArray      * notificationArray;
    NSMutableArray      * eventsArray;
    
    NSUInteger                  notificationCount;
    NSInteger selectedIndex;
    UIImageView          *DefaultUpcomingImageV;
    UILabel *EventsTitleLabel;
}
@end
@implementation HomeViewController
-(void)UpdateUI{
    
    /*****************add slider**********/
    if(!iCar)   {
        iCar = [[iCarousel alloc]initWithFrame:CGRectMake(0,kNavigationheight+kTopPadding_small
                                                          , SCREEN_WIDTH, iCraiselHeight)];
        [iCar setDelegate:self];
        [iCar setDataSource:self];
        [iCar setBackgroundColor:[UIColor clearColor]];
        iCar.scrollSpeed = 2.0f;
        iCar.stopAtItemBoundary = YES;
        iCar.scrollToItemBoundary = YES;
        iCar.decelerationRate = 0.0f;
        iCar.bounces = NO;
        iCar.layer.masksToBounds = YES;
        
        [self.view addSubview:iCar];
        
        
        /************** adding view more button**********************************************/
        
        //Adding Tap Here Label
        /* self.tapHereLabel  =[self createLblWithRect:CGRectMake((SCREEN_WIDTH-50)/2,(iCar.frame.size.height+iCar.frame.origin.y)-(55*3), 250, 42) font:[UIFont fontWithName:kFontSemiBold size:14] text:@" Tap Here " textAlignment:NSTextAlignmentCenter textColor: [UIColor whiteColor]];
        self.tapHereLabel.backgroundColor=[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:140.0/255.0 alpha:1.0];
        [self.tapHereLabel setHidden:YES];
        [self.tapHereLabel sizeToFit]; */
        
        
        
    /*    self.viewmoreButton = [self createButtonWithFrame:CGRectMake((SCREEN_WIDTH-255)/2,(iCar.frame.size.height+iCar.frame.origin.y)-(55*3) +30, 250, 42) withTitle:@" Touch Image to learn more "];
        self.viewmoreButton.titleLabel.font=[UIFont fontWithName:kFontSemiBold size:14];
        [self.viewmoreButton setBackgroundColor:[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:140.0/255.0 alpha:1.0]];
        //[self.viewmoreButton sizeToFit];
        //[self.viewmoreButton setBackgroundImage:[UIImage imageNamed:kImageViewMoreButton] forState:UIControlStateNormal];
//        self.viewmoreButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
//        [self.viewmoreButton setBackgroundColor:[UIColor clearColor]];
//        self.viewmoreButton.adjustsImageWhenHighlighted = NO;
        self.viewmoreButton.userInteractionEnabled=NO;
        self.viewmoreButton.center = CGPointMake(iCar.center.x, self.viewmoreButton.center.y);
        [self.viewmoreButton addTarget:self action:@selector(viewmoreButton:) forControlEvents:UIControlEventTouchUpInside];
     
     
     Normal
     // EventsTitleLabel=[self createLblWithRect:CGRectMake(0,(iCar.frame.size.height+iCar.frame.origin.y)-(55*3)-155,SCREEN_WIDTH, 42) font:[UIFont fontWithName:kFontSemiBold size:14] text:[NSString stringWithFormat:@" %@ - Tap Here ",[[_EventsAndPromotionArray objectAtIndex:0] objectForKey:@"EventName"]] textAlignment:NSTextAlignmentLeft textColor: [UIColor whiteColor]];
     
     */
        
        if (!([_EventsAndPromotionArray count]==0)) {
            [iCar addSubview:self.tapHereLabel];
           
             
           EventsTitleLabel=[self createLblWithRect:CGRectMake(0,(iCar.frame.size.height+iCar.frame.origin.y)-(55*3)+66,SCREEN_WIDTH,30) font:[UIFont fontWithName:kFontSemiBold size:12   ] text:[NSString stringWithFormat:@" %@ - Tap Here ",[[_EventsAndPromotionArray objectAtIndex:0] objectForKey:@"EventName"]] textAlignment:NSTextAlignmentCenter textColor: [UIColor whiteColor]];
            EventsTitleLabel.backgroundColor=[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:140.0/255.0 alpha:1.0];
          //  [EventsTitleLabel sizeToFit];
            [iCar addSubview:EventsTitleLabel];
         
        }
        
        /*****************add slider**********/
    }
    else    {
        [iCar reloadData];
    }
    
    if(!self.defaultTableView)  {
        self.defaultTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,iCar.frame.origin.y+iCar.frame.size.height+2,SCREEN_WIDTH, SCREEN_HEIGHT-((iCar.frame.size.height)+(kBottomViewHeight)+kNavigationheight+8))
                                 
                                                             style:UITableViewStylePlain];
        [self.defaultTableView setSeparatorColor: [UIColor lightGrayColor]];
        self.defaultTableView .separatorStyle=UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.defaultTableView];
        self. defaultTableView.scrollEnabled = NO;
        [self.defaultTableView setDelegate:self];
        [self.defaultTableView setDataSource:self];
    }
    else    {
        [self.defaultTableView reloadData];
    }
}
-(void)CallApiEventWithPagenumber:(int)page{
    NSMutableDictionary* paramDict = [NSMutableDictionary dictionary];
    apiResponseDictionary=[NSMutableDictionary dictionary];
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [paramDict setObject:[NSString stringWithFormat:@"%d",page] forKey: kParamPageNumber];
    [paramDict setObject:@"10" forKey: kParamPageSize];
    // getEventsWithEventsList
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] getEventsWithEventsList:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"Response = %@",response);
            [self hideSpinner];
            _EventsAndPromotionArray =[response valueForKey:@"productCount"];
            loyaltyEnable = [response valueForKey:@"IsFrequentShopper"];
            if ([_EventsAndPromotionArray count]==0) {
                [DefaultUpcomingImageV setHidden:NO];
            }
            else{
                [DefaultUpcomingImageV setHidden:YES];
            }
            DLog(@"eventsArray--%@",eventsArray);
            [self UpdateUI];
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        NSLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}

-(void)callApiUserNotificationHistory{
    User *userObj=kGetUserModel;
    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
    [paramDict setObject:userObj.userId forKey: kParamUserId];
    [paramDict setObject:@"1" forKey: kParamPageNumber];
    [paramDict setObject:@"1000" forKey: kParamPageSize];
 //    NSLog(@"%@",paramDict);
    [self showSpinner];
    [[ModelManager modelManager]GetUserNotificationHistory:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
    //    NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            @try {
                [self hideSpinner];
                DLog(@"apiResponseDictionary--%@",apiResponseDictionary);
                notificationArray =[response valueForKey:@"list"];
                notificationCount=[notificationArray count];
               // [self UpdateUI];
            }
            @catch (NSException *exception) {
                [self showAlertViewWithTitle:kError message:kInternalInconsistencyErrorMessage];
            }
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}

-(void)initializeArrayAndDictionary{
    self. EventsArray=@[@"ORDERS",@"DEALS",@"SEARCH",@"NOTIFICATIONS"];
    //self.EventsAndPromotionImageArray=@[@"EventDetails_Img1.jpg",@"EventDetails_Img2.jpg",@"EventDetails_Img3.jpg",@"Home_Img1.jpg",@"Home_Img2.jpg",@"Home_Img3.jpg"];
    _EventsAndPromotionArray=[NSMutableArray new];
    notificationArray=[NSMutableArray new];
    apiResponseDictionary=[NSMutableDictionary new];
    self. Events_icons_Array=@[kImageOrderIcon,kImageDealsIcon,kImageSearchIcon,kImageNotificationsIcon];
    notificationCount=0;
}
#pragma mark-viewcontrollerlifecycle.
-(void)viewWillAppear:(BOOL)animated{
    [self refreshPage:nil];
}
-(void)refreshPage:(id)sender{
    _segueIdentifierArray = [NSMutableArray arrayWithObjects:kGoToMyOrderViewControllerSegueId,kGoToDealsViewControllerSegueId,kGoToSearchResultViewControllerSegueId,kGoToNotificationsHistoryViewControllerSegueId,nil];
    if([notloggedIn isEqualToString:@"1"])
    {
       [self initializeArrayAndDictionary];
       [self CallApiEventWithPagenumber:1];
    }
    else
    {
      [self initializeArrayAndDictionary];
      [self callApiUserNotificationHistory];
      [self CallApiEventWithPagenumber:1];
      [self CallApiMyaccount];
    }
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];

}
-(void)viewDidLoad{
    [super viewDidLoad];DLog(@"");
    _segueIdentifierArray = [[NSMutableArray alloc]init];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshPage:) name:kHomeViewControllerStoryboardId object:nil];
     [self AddDefaultUpcomingImage];
    
    //All browse favorites added to that particular userid when user logs inx
    if([notloggedIn intValue]==0)
    {
         favoritesAlreadyAdded = @"1";
        [self CallApiAddFavoriteWithProductID:combinedString];
    }
    else{}
    
}

#pragma mark - SlideNavigationController Methods -
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu  {
    return YES;
}
#pragma mark - UITableView Delegate & Datasrouce -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section    {
    return [self.EventsArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kRowHeight_custom;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier: kDefaultTableViewCell];
    
    CGRect IconImageFrame ;
    IconImageFrame.origin.x         =   kCellPadding;
    IconImageFrame.size.height      =   kIconImageHightAndWidth;
    IconImageFrame.size.width       =   kIconImageHightAndWidth;
    IconImageFrame.origin.y         =   ((kRowHeight_custom-kIconImageHightAndWidth))/2;
    if(indexPath.row == 0 || indexPath.row == 1)    {
        IconImageFrame.size.height      =   kIconImageHightAndWidth + 10;
        IconImageFrame.size.width       =   kIconImageHightAndWidth + 10;
    }
    
    CGRect EventsLabelFrame ;
    EventsLabelFrame.origin.y       =   0;
    EventsLabelFrame.origin.x       =  IconImageFrame.origin.x+ kIconImageHightAndWidth+(kPaddingSmall*6);
    EventsLabelFrame.size.height    =   kRowHeight_custom;
    EventsLabelFrame.size.width     =   kLabelWidth*2;
    
    if (cell == nil)    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:kDefaultTableViewCell];
        UIImage *IconImage = [UIImage imageNamed:[self.Events_icons_Array objectAtIndex:indexPath.row]];
        
        UILabel *EventsLabel=[self createLblWithRect:EventsLabelFrame font:[self addFontSemiBold] text:[self.EventsArray objectAtIndex:indexPath.row] textAlignment:NSTextAlignmentLeft textColor: [UIColor colorWithRed:0.4118 green:0.4078 blue:0.4314 alpha:1.0]];
        EventsLabel.backgroundColor=[UIColor clearColor];
        [cell addSubview:EventsLabel];
        
        UIImageView *eventsIconImageView=[self createImageViewWithRect:IconImageFrame image:IconImage];
        [eventsIconImageView setContentMode:UIViewContentModeScaleAspectFit];
        [eventsIconImageView setBackgroundColor:[UIColor clearColor]];
        [cell addSubview:eventsIconImageView];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [self getSeperatorVForTableCell:cell];
    }
    
    UIImageView *batchImgeV;
    UILabel *batchLabel;
    if (indexPath.row==3) {
        batchImgeV=[self createImageViewWithRect:CGRectMake(SCREEN_WIDTH-37, 0, 37, 37) image:[UIImage imageNamed:kImage_notificationsbatch ]];
        batchLabel=[self createLblWithRect:CGRectMake(16, 2, 20, 16) font:[self addFontBold] text:[NSString stringWithFormat:@"%lu",(unsigned long)notificationCount] textAlignment:NSTextAlignmentCenter textColor:[UIColor whiteColor]];
        
        batchLabel.minimumScaleFactor=0.4;
        batchLabel.adjustsFontSizeToFitWidth = YES;
        
        if(notificationCount > 999) {
            batchLabel.text = [NSString stringWithFormat:@"999+"];
        }
        
        [batchImgeV addSubview:batchLabel];
        if (notificationCount>0) {
            [cell addSubview:batchImgeV];
            
        }
        batchLabel .backgroundColor=[UIColor clearColor];
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   // UIViewController *vc = nil;
     NSString *segueIdentifier = [_segueIdentifierArray objectAtIndex:indexPath.row];
    switch (indexPath.row)  {
        case 0:
            if([notloggedIn intValue]==1)
                [self showConfirmViewWithTitle:@"Confirm" message:@"In order to proceed further please Sign in or Sign Up for the app." otherButtonTitle:@"Sign In/ Sign Up"];
             else
              [self performSegueWithIdentifier:segueIdentifier sender:nil];
               break;
        case 1:
            [self performSegueWithIdentifier:segueIdentifier sender:nil];
            break; 
            
        case 2:
            [self performSegueWithIdentifier:segueIdentifier sender:nil];
            break;
            
        case 3:
            if([notloggedIn intValue]==1)
                [self showConfirmViewWithTitle:@"Confirm" message:@"In order to proceed further please Sign in or Sign Up for the app." otherButtonTitle:@"Sign In/ Sign Up"];
            else
                [self performSegueWithIdentifier:segueIdentifier sender:nil];
            break;
        
        default:
            NSLog(@"Hello");
            break;
    }

    //[self.navigationController pushViewController:vc animated:YES];
}

//viewmoreButton
#pragma mark-scroll view delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender{
    CGFloat pageWidth = 320;
    int page = floor((self.CustomScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.custompagecontrol .currentPage = page;
}
#pragma mark-BUTTON ACTIONS
-(void)GetDetailOfEvent:(NSInteger)index{
    EventsDetailViewController *vc=[self viewControllerWithIdentifier:kEventsDetailViewControllerStoryboardId];
    if(_EventsAndPromotionArray.count!=0){
    vc.eventsDict=[_EventsAndPromotionArray objectAtIndex:index];
    [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)changePage{
    CGRect frame;
    frame.origin.x = self.CustomScrollView.frame.size.width * self.custompagecontrol.currentPage;
    frame.origin.y = 0;
    frame.size = self.custompagecontrol.frame.size;
    [self.CustomScrollView scrollRectToVisible:frame animated:YES];
}

-(void)kartButton:(UIButton*)sender{
    __weak NSArray *shoppingbagarray=[[ShoppingCart sharedInstace] fetchProductFromShoppingCart];
    if (shoppingbagarray.count == 0) {
        [UIView showMessageWithTitle:nil message:@"No Item in Shopping cart!" showInterval:1.5 viewController:self];
    }else   {
        // kartAddedAndLoggedIn = @"1";
        [self performSegueWithIdentifier:kGoToShoppingCartViewControllerSegueId sender:nil];
    }
    shoppingbagarray = nil;
}

-(void)teaserCopyAboutButton:(id)sender{
    
}
-(void)viewmoreButton:(id)sender{
    [self showSpinner];
    [self performSegueWithIdentifier:kGoToEventsListViewControllerSegueId sender:nil];
}
/* -(void)getProductIdsFromLocalStorage:(NSArray *)localStorageArray
{
    for(int i=0;i<[localStorageArray count];i++)
    {
        NSArray *individualArray = [localStorageArray objectAtIndex:i];
        [returnProductIDArray addObject:[individualArray objectAtIndex:4]];
    }
} */

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    // this is needed to prevent cells from being displayed above our static view
}

#pragma mark-SEGUE IDENTIOFIER
#pragma mark iCarousel methods
-(void)AddDefaultUpcomingImage{
    DefaultUpcomingImageV=[self createImageViewWithRect:CGRectMake(0,kNavigationheight+kTopPadding_small
                                                                 , SCREEN_WIDTH, iCraiselHeight) image:[UIImage imageNamed:kImageDefaultUpcomingImage]];
    [self.view addSubview:DefaultUpcomingImageV];
    [DefaultUpcomingImageV setHidden:YES];

}
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel  {
   // _EventsAndPromotionArray=nil;

    DLog(@"_EventsAndPromotionArray----%@",_EventsAndPromotionArray);
//    if ([_EventsAndPromotionArray count]==0) {
//        
//        return 0;
//
//    }
    if ([_EventsAndPromotionArray count]==0) {
         return 0;
    }
    else if ([_EventsAndPromotionArray count]<5){
        return [_EventsAndPromotionArray count];
    }
    else{
     return 5;
    }
   
}


- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    [self GetDetailOfEvent:index];
    
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view   {
    selectedIndex=index;
    if(!view)   {
        view = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, iCar.frame.size.height)];
        [view setContentMode:UIViewContentModeScaleToFill];
        view.layer.masksToBounds = YES;
    
    }
    
    if([_EventsAndPromotionArray count]>0){
        NSString *strBusinessImg = [[_EventsAndPromotionArray objectAtIndex:index] valueForKey:@"EventLargeImage"];
        strBusinessImg = [strBusinessImg stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        ((AsyncImageView *)view).showActivityIndicator = YES;
        
        if(![strBusinessImg isEqualToString:@""])   {
            
            ((AsyncImageView *)view).imageURL = [NSURL URLWithString:[[_EventsAndPromotionArray objectAtIndex:index] valueForKey:@"EventLargeImage"]];
        }
        if(!self.custompagecontrol)  {
            CGRect pagecontrolFrame = CGRectMake((SCREEN_WIDTH-255)/2,(iCar.frame.size.height+iCar.frame.origin.y)-(55*3)+30.0, 255 ,55);
            self.custompagecontrol=[self createCustompagecontrol:(int)[_EventsAndPromotionArray count] currentPage:0 withFrame:pagecontrolFrame];
            [iCar addSubview:self.custompagecontrol];
            [iCar bringSubviewToFront:self.custompagecontrol];
        }
        if ([_EventsAndPromotionArray count]<5) {
            self.custompagecontrol.numberOfPages=[_EventsAndPromotionArray count];
        }
        else{
            self.custompagecontrol.numberOfPages=5;
        }
        
        if(self.tapHereLabel)
            [iCar bringSubviewToFront:self.tapHereLabel];
        

    }
    else{
        ((AsyncImageView *)view).image = [UIImage imageNamed:@"Home_Img1.png"];

    
    }
       return view;
}
- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing)
    {
        return value * 1;
    }
    return value;
}
- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel{
    _custompagecontrol.currentPage = carousel.currentItemIndex;
    //_custompagecontrol.numberOfPages
    if(_EventsAndPromotionArray.count!=0){
    EventsTitleLabel.text=[NSString stringWithFormat:@" %@ -  Tap Here ",[[_EventsAndPromotionArray objectAtIndex:carousel.currentItemIndex] objectForKey:@"EventName"]];
   // [EventsTitleLabel sizeToFit];
    }

}

#pragma mark - Page controller method
- (void)pageControlPageDidChange:(PageControl *)pageControl{
    [iCar scrollToItemAtIndex:_custompagecontrol.currentPage animated:YES];
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton*)button {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:kGoToNotificationsHistoryViewControllerSegueId]){
        
        NotificationHistoryViewController *controller =[segue destinationViewController];
        controller.getNotificationArray = notificationArray;
        
    }
    if([segue.identifier isEqualToString:kGoToEventsListViewControllerSegueId]){
        
        EventsListViewController *controller =[segue destinationViewController];
        controller.eventsArray = _EventsAndPromotionArray;
    }
    if([segue.identifier isEqualToString:kGoToEventsDetailViewControllerSegueId]){
        
        EventsListViewController *controller =[segue destinationViewController];
        controller.eventsArray = [_EventsAndPromotionArray objectAtIndex:selectedIndex];
    }
}
-(void)dealloc{
    
    
}
@end