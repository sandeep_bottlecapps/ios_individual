//
//  BarcodeScannerViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 9/15/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface BarcodeScannerViewController : BaseViewController
@property(nonatomic,retain)NSString *barcodeTypeString;

@end
