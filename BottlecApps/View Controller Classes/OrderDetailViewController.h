//
//  OrderDetailViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 8/28/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <PassKit/PKPaymentRequest.h>
#import <PassKit/PKPaymentToken.h>
#import <PassKit/PKPayment.h>
#import <PassKit/PKPaymentAuthorizationViewController.h>
#import "CardIOPaymentViewControllerDelegate.h"
#import "CardIOViewDelegate.h"

#import "AuthNet.h"
#import "DecimalKeypadView.h"
#import <CoreLocation/CoreLocation.h>

typedef enum oDetail
{
    kPlaceorder,
    kMyorder
    
}OrderdetailsType ;

@interface OrderDetailViewController : BaseViewController<AuthNetDelegate,DecimalKeypadViewDelegate,CardIOPaymentViewControllerDelegate,CardIOViewDelegate,CLLocationManagerDelegate>{
    NSString* cardnumberString;
    NSString* TransactionIDString;
    NSString *expirationDateString;
    TransactionResponse *transResponse;
    NSString *messageStr;
    BOOL scanFound;
    NSString *scanCerditCard;
    NSMutableDictionary *dict_productQuantityToPurchaseWithID;
    NSMutableArray *ProductOutofStockArray;
    NSUInteger indexOfInvalidProduct;
    int productId_Of_outOfStock;
    
    CLLocationManager *location;
    CLLocation *loc;
    
    BOOL enablePayment;
    BOOL payStore;
    
    //Added by Sandeep for taxAfterDiscount
    
  //float orderTotalPickUp;
 // float taxPickUp;
  float couponDiscount;
    NSString *fromTimeString;
    NSString *currTimStore;
    NSString *timeMode;
    
    NSDate *oneHourExtraServerTime;
    
    UIView *transparentlayer;
    UIView *alertBodyView;
}
@property (nonatomic,retain) NSMutableArray  *cellItemsArray;
@property (nonatomic,retain)UILabel        *productNameLabel;
@property (nonatomic,retain)UILabel        *productQuantityLabel;
@property (nonatomic,retain)UILabel        *productPriceLabel;
@property (nonatomic,retain)UILabel        *productNoteLabel;
@property (nonatomic,retain)UILabel        *ItemCostLabel;
@property (nonatomic,retain)UILabel        *TaxLabel;
@property (nonatomic,retain)UILabel        *OrderTotalLabel;
@property (nonatomic,retain)UILabel        *StreetAddressLabel;
@property (nonatomic,retain)UILabel        *CityLabel;
@property (nonatomic,retain)UILabel        *statusNameLabel;
@property (nonatomic,retain)UILabel        *confirmationNumberLabel;
//@property (nonatomic,retain)UIButton       *payNowButton;
@property (nonatomic,retain)UIButton       *Apply;

@property (nonatomic,retain)NSString       *OrderTypeString;
@property (nonatomic,retain)NSMutableArray        *getCartArray;
@property (nonatomic,retain)UITextField    *specialInstruction;
@property (nonatomic,retain)NSMutableArray *orderArray;
@property (nonatomic, strong) NSString *creditCardBuf;
@property (nonatomic, strong) NSString *expirationBuf;
@property (nonatomic, strong) NSString *sessionToken;
@property (nonatomic, strong) NSString *discount;
@property (nonatomic, strong) NSString *TotalAmount;
@property (nonatomic, strong) NSString *instructionStr;;
@property (nonatomic, strong) NSString *totaltax;
@property (nonatomic, strong) NSString *orderID;
@property (nonatomic,strong) NSMutableString *onSaleId;
@property (nonatomic)           OrderdetailsType oType;
@property (nonatomic, strong) NSMutableString *productID;
@property (nonatomic, strong) UITextField *currentField;
//@property (nonatomic, assign) int productId_Of_outOfStock;
//@property (nonatomic, assign)  NSMutableArray *ProductOutofStockArray;
@property (nonatomic, strong)  DecimalKeypadView *keypad;
@property (nonatomic, strong)  UITextField *zipTextField;

-(NSString *)addZeroBeforeMonth:(NSDate *)givenDate;
//UITextField *specialInstruction
@end
