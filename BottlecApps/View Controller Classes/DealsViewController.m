//
//  DealsViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 8/11/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "DealsViewController.h"
#import "AsyncImageView.h"
#import "ProductDetailsViewController.h"
#import "QALabel.h"
#import "Constants.h"
#import "AppDelegate.h"

@interface DealsViewController ()<UITableViewDataSource,UITableViewDelegate>{
    
    NSURL               *productImageURL;
    UIImage             *productImage;
    
    
    int pagenumber;
    int                 totalCount;
    NSMutableArray      *ItemArray;
    
    UILabel *NorecordFoundLabel;
    AppDelegate *appDelegate;
    
}
@end

@implementation DealsViewController
-(void)addNorecordFoundLAbelOnTableView:(UITableView*)tableView{
    NorecordFoundLabel = [[UILabel alloc] init];
    [NorecordFoundLabel setBackgroundColor:[UIColor clearColor]];
    [NorecordFoundLabel setTextColor:[self getColorForBlackText]];
    [NorecordFoundLabel setText:kAlertNoRecordFound];
    [NorecordFoundLabel sizeToFit];
    NorecordFoundLabel.frame = CGRectMake((tableView.bounds.size.width - NorecordFoundLabel.bounds.size.width) / 2.0f,
                                          50,
                                          NorecordFoundLabel.bounds.size.width,
                                          NorecordFoundLabel.bounds.size.height);
    [tableView insertSubview:NorecordFoundLabel atIndex:0];
}

-(void)UpdateUI{
    //  [ self createUIForSearchBar ];
    if ([ItemArray count]>0) {
        for (Product *productObj in ItemArray) {
            if ([[productObj productStatus] intValue]==0) {
                [self.arrayForBoolProductStatus addObject:@"0"];
            }
            else{
                [self.arrayForBoolProductStatus addObject:@"1"];
            }
        }
        DLog(@"self.arrayForBoolProductStatus-----------%@",self.arrayForBoolProductStatus);
        // Do any additional setup after loading the view.
        
        self.defaultTableView .delegate=self;
        self.defaultTableView.dataSource=self;
        
        [self.defaultTableView reloadData];
        
        //   [self getCountry];
    }
}

-(void)callApiGetProductDealsWithPageNumber:(int)page{
   // User *userObj=kGetUserModel;
    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
   // [paramDict setObject:userObj.userId forKey: kParamUserId];
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [paramDict setObject:[NSString stringWithFormat:@"%d",page] forKey: kParamPageNumber];
    [paramDict setObject:kPageSize forKey: kParamPageSize];
    
    DLog(@"paramDict--%@",paramDict);
    
    [self showSpinner];
    [[ModelManager modelManager] GetProductDealsWithParam:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
        //NSLog(@"response---%@", response);
        [self hideSpinner];
        @try {
            for (NSDictionary *dict in [response valueForKey:@"productCount"]) {
                Product *productObj=[[Product alloc] initWithDictionary:dict];
                [ItemArray addObject:productObj];
            }
            totalCount=[[response valueForKey:@"totalNoOfRecords"] intValue];
            [self UpdateUI];
        }
        @catch (NSException *exception) {
            [self showAlertViewWithTitle:kError message:kInternalInconsistencyErrorMessage];
        }
        
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        
    }];
}

-(void)initializeAllArrayAndDict{
    pagenumber=1;
    totalCount=0;
    ItemArray=[NSMutableArray new];
}

-(void)viewDidAppear:(BOOL)animated{
    
}

-(void) viewWillAppear:(BOOL)animated   {
    [self initializeAllArrayAndDict];
    pagenumber=1;
    [self callApiGetProductDealsWithPageNumber:pagenumber];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addScrollView];
    [self.CustomScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, self.CustomScrollView.frame.size.height )];
    [self.CustomScrollView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Home_Img5.jpg"]]];
    [self AddDefaultTableView];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of ections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==[ItemArray count]) {
        return kButtonHeight;
        
    }
    else
        return krowHeightFavorite;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];
    NSUInteger count = [ItemArray count];
    
    if (row == count) {
        pagenumber++;
        
        if (count<totalCount) {
            [self callApiGetProductDealsWithPageNumber:pagenumber];
        }
    }
    else{
        
        Product *productObj = [ItemArray objectAtIndex:indexPath.row];
        NSMutableArray *temparray=[NSMutableArray new];
        [temparray addObject:productObj];
        ProductDetailsViewController *vc=[self viewControllerWithIdentifier:kProductDetailsViewControllerStoryBoardID ];
        vc.productIDSelected=productObj.productId;
        vc.isfav=productObj.productFavStatus;
        vc.getProductarray=temparray;
        [self.navigationController pushViewController:vc animated:YES ];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    DLog(@"ItemArray--%@",ItemArray);
    if ([ItemArray count]<totalCount && [ItemArray count]!=0) {
        return [ItemArray count]+1;
    }
    
    else{
        return [ItemArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *postCellId = @"postCell";
    static NSString *moreCellId = @"moreCell";
    NSUInteger row = [indexPath row];
    NSUInteger count = [ItemArray count];
    UITableViewCell *cell =nil;
    
    if (row == count && count != 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:moreCellId];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:moreCellId];
            cell.textLabel.text = kTapToloadMore;
            cell.textLabel.textColor = [self getColorForRedText];
            cell.textLabel.font = [self addFontRegular];
        }
        return cell;
    }
    else    {
        AsyncImageView *ProductImageView;
        QALabel*productNameLabel;
        UILabel*productSizeLabel;
        UILabel*productPriceLabel;
        UIView    *CartAndLikeView;
        UIActionButton  *cartButton;
        UIActionButton  * FavButton;
        UIView *seperatorV;
        UIImageView* imgOutOfStock;
        UIView*  leftSeperatorV;
        UIImageView *separatorEnd;
        
        cell = [tableView dequeueReusableCellWithIdentifier:postCellId];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:postCellId];
            
            ProductImageView=[self createAsynImageViewWithRect:CGRectMake(0, 0, 73, 77) imageUrl:[NSURL URLWithString:kEmptyString] isThumnail:YES];
            ProductImageView.tag = kTagProductImageV;
            ProductImageView.backgroundColor = [UIColor whiteColor];
            
            productNameLabel= [[QALabel alloc] initWithFrame:CGRectMake((ProductImageView.frame.size.width)+(ProductImageView.frame.origin.x)+10,3, kLabelWidth+37, kLabelHeight*2)];
            productNameLabel.font = [UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize-2];
            productNameLabel.textAlignment = NSTextAlignmentLeft;
            productNameLabel.textColor = [self getColorForRedText];
            productNameLabel.tag=kTagProductName;
            productNameLabel.numberOfLines=2;
            
            productSizeLabel=[self createLblWithRect:CGRectMake((ProductImageView.frame.size.width)+(ProductImageView.frame.origin.x)+10,(productNameLabel.frame.origin.y)+(productNameLabel.frame.size.height) - 5, kLabelWidth+37, kLabelHeight) font:[self addFontRegular] text:kEmptyString textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
            productSizeLabel.tag=kTagProductSize;
            
            productPriceLabel=[self createLblWithRect:CGRectMake((ProductImageView.frame.size.width)+(ProductImageView.frame.origin.x)+10, (productSizeLabel.frame.size.height)+(productSizeLabel.frame.origin.y) - 5, kLabelWidth+37, kLabelHeight) font:[UIFont fontWithName:kFontBold size:kFontBoldSize-2] text:kEmptyString textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
            [productPriceLabel setTag:kTagProductPrice];
            
            CartAndLikeView=[self createContentViewWithFrame:CGRectMake(SCREEN_WIDTH-kCartAndLikeViewWidth, 0, kCartAndLikeViewWidth,krowHeightFavorite-kPaddingcartAndFav)];
            [CartAndLikeView setBackgroundColor:[UIColor clearColor]];
            [CartAndLikeView setTag:kTagCartAndLikeView];
            CartAndLikeView.layer.masksToBounds = YES;
            
            cartButton=[self createActionButtonWithFrame:CGRectMake(kCartAndLikeViewWidth/2,0, kCartAndLikeViewWidth/2,kCartLikeViewHeight ) withTitle:kEmptyString];
            cartButton.tag = kTagCartButton;
            
            [cartButton addTarget:self action:@selector(cartButton:) forControlEvents:UIControlEventTouchUpInside];
            leftSeperatorV=[self createContentViewWithFrame:CGRectMake(0,0, 1,kCartLikeViewHeight )];
            [leftSeperatorV setBackgroundColor:[UIColor clearColor]];
            [[leftSeperatorV layer] setCornerRadius:0];
            [[leftSeperatorV layer] setBorderWidth:1.0];
            seperatorV=[self createContentViewWithFrame:CGRectMake(kCartAndLikeViewWidth/2,0, 1,kCartLikeViewHeight )];
            [seperatorV setBackgroundColor:[UIColor clearColor]];
            [[seperatorV layer] setCornerRadius:0];
            [[seperatorV layer] setBorderWidth:1.0];
            
            FavButton=[self createActionButtonWithFrame:CGRectMake(0,0, kCartAndLikeViewWidth/2,kCartLikeViewHeight) withTitle:kEmptyString];
            FavButton.tag = kTagFavButton;
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            [cell addSubview:ProductImageView];
            
            [cell addSubview:productNameLabel];
            [cell addSubview:productSizeLabel];
            [cell addSubview:productPriceLabel];
            [cell addSubview:CartAndLikeView];
            [CartAndLikeView addSubview:leftSeperatorV];
            [CartAndLikeView addSubview:FavButton];
            [CartAndLikeView addSubview:seperatorV];
            [CartAndLikeView addSubview:cartButton];
            
            separatorEnd = [[UIImageView alloc] initWithFrame:CGRectMake(0, krowHeightFavorite-1, SCREEN_WIDTH, 1)];
            [separatorEnd setBackgroundColor:[UIColor colorWithRed:0.8278 green:0.8278 blue:0.8278 alpha:1.0]];
            [cell.contentView addSubview:separatorEnd];
        }
        imgOutOfStock.hidden=YES;
        imgOutOfStock= [[UIImageView alloc] initWithFrame:CGRectMake((imgOutOfStock.frame.size.width)+182, 39, 38,38)];
        [imgOutOfStock setTag:101];
        [cell addSubview:imgOutOfStock];
        imgOutOfStock=(UIImageView *)[cell viewWithTag:101];

        Product *productObj = [ItemArray objectAtIndex:indexPath.row];
        
        productImageURL = [NSURL URLWithString:productObj.productImageURL];
        ProductImageView = (AsyncImageView *)[cell viewWithTag: kTagProductImageV];
        [ProductImageView setImageURL:productImageURL];
        ProductImageView.backgroundColor = [UIColor whiteColor];
        [ProductImageView setContentMode:UIViewContentModeScaleAspectFit];
        ProductImageView.layer.masksToBounds = YES;
        
        productNameLabel = (QALabel *)[cell viewWithTag: kTagProductName];
        [productNameLabel setText: productObj.productName];
        productNameLabel.verticalAlignment = UIControlContentVerticalAlignmentBottom;
        
        productSizeLabel = (UILabel *)[cell viewWithTag: kTagProductSize];
        [productSizeLabel setText:productObj.productSize];
        
        productPriceLabel = (UILabel *)[cell viewWithTag: kTagProductPrice];
        [productPriceLabel setText:[self addDollarSignToString:kEmptytextfield]];
        
        
        NSMutableString *price = [NSMutableString string];
        NSString *salePrice = nil;
        if(productObj.productPrice && [productObj.productPrice floatValue] > 0.00) {
            [price appendString:[self addDollarSignToString:productObj.productPrice]];
        }
        
        if(productObj.productSalesPrice && (![productObj.productSalesPrice isEqualToString:kEmptyString]) &&
           (![productObj.productSalesPrice isEqualToString:kEmptySalePrice]) && [productObj.productSalesPrice floatValue] > 0.00)   {
            salePrice = [self addDollarSignToString:productObj.productSalesPrice];
        }
        
        if(price && ([price isEqualToString:kEmptyString]))    {
            [price setString:@"Free"];
            [productPriceLabel setText: price];
        }
        else    {
            if(salePrice && (![salePrice isEqualToString:kEmptyString]))    {
                // attributed text
                [productPriceLabel setText: kEmptyString];
                
                NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:price attributes:
                                                              @{
                                                                NSForegroundColorAttributeName: [self getColorForBlackText],
                                                                NSFontAttributeName: [self addFontRegular],
                                                                }];
                [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                        value:@2
                                        range:NSMakeRange(0, [attributeString length])];
                
                NSAttributedString *attribSalePriceString = [[NSAttributedString alloc] initWithString:
                                                             [NSString stringWithFormat:@"    %@", salePrice] attributes:
                                                             @{
                                                               NSForegroundColorAttributeName: [self getColorForBlackText],
                                                               NSFontAttributeName: [UIFont fontWithName:kFontBold size:kFontBoldSize-2],
                                                               }];
                
                [attributeString appendAttributedString:attribSalePriceString];
                [productPriceLabel setAttributedText:attributeString];
            }
            else    {
                [productPriceLabel setText: price];
            }
        }
        
        cartButton = (UIActionButton *)[[cell viewWithTag:kTagCartAndLikeView] viewWithTag:kTagCartButton];
        FavButton = (UIActionButton *)[[cell viewWithTag:kTagCartAndLikeView] viewWithTag:kTagFavButton];
        cartButton.rowNumber = (int)indexPath.row;
        FavButton.rowNumber = (int)indexPath.row;
        FavButton.productObj=productObj;
        cartButton.productObj=productObj;
        if ([[ShoppingCart sharedInstace ] shoppingCartContains:productObj]==NO) {
            [cartButton setImage:[UIImage imageNamed:kImagecarticongrey] forState:UIControlStateNormal];
            [cartButton setBackgroundColor:[UIColor clearColor]];
            [cartButton setSelected:NO];
        }
        else{
            [cartButton setImage:[UIImage imageNamed:kImageCartIconwhite] forState:UIControlStateNormal];
            [cartButton setBackgroundColor:[self getColorForYellowText]];
            [cartButton setSelected:YES];
        }
        
        [FavButton addTarget:self action:@selector(FavButton:) forControlEvents:UIControlEventTouchUpInside];\
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *favoriteOfflineArray = [userDefaults objectForKey:@"offlineFavoriteArray"];
       // NSLog(@"Favorite offline Array = %@",favoriteOfflineArray);
        
        if([notloggedIn intValue]==1 && [favoriteOfflineArray count]>0)
        {
            for(int i=0;i<[favoriteOfflineArray count];i++)
            {
                NSArray *individualArray = [favoriteOfflineArray objectAtIndex:i];
               // NSLog(@"Individual Array=%@",individualArray);
                int productID = [[individualArray objectAtIndex:4]intValue];
               // NSLog(@"product id=%d,product from cell=%d",productID,[productObj.productId intValue]);
                if([productObj.productId intValue] == productID)
                {
                    [FavButton setImage:[UIImage imageNamed:kImageHeartIconwhite] forState:UIControlStateNormal];
                    [FavButton setBackgroundColor:[self getColorForRedText]];
                    [FavButton setSelected:YES];
                    break;
                }
                else
                {
                    [FavButton setImage:[UIImage imageNamed:kImageHeartIconGrey] forState:UIControlStateNormal];
                    [FavButton setBackgroundColor:[UIColor clearColor]];
                    [FavButton setSelected:NO];
                    imgOutOfStock.hidden=NO;
                }
                
            }
        }
        else
        {
            
            if([notloggedIn intValue]==1)
            {
                [FavButton setImage:[UIImage imageNamed:kImageHeartIconGrey] forState:UIControlStateNormal];
                [FavButton setBackgroundColor:[UIColor clearColor]];
                [FavButton setSelected:NO];
                imgOutOfStock.hidden=NO;
            }
            else
            {
                if ([productObj.productFavStatus intValue] == 0) {
                    [FavButton setImage:[UIImage imageNamed:kImageHeartIconGrey] forState:UIControlStateNormal];
                    [FavButton setBackgroundColor:[UIColor clearColor]];
                    [FavButton setSelected:NO];
                    imgOutOfStock.hidden=NO;
                }
                else{
                    [FavButton setImage:[UIImage imageNamed:kImageHeartIconwhite] forState:UIControlStateNormal];
                    [FavButton setBackgroundColor:[self getColorForRedText]];
                    [FavButton setSelected:YES];
                }
            }
        }
        /* if ([productObj.productFavStatus intValue]==0) {
            [FavButton setImage:[UIImage imageNamed:kImageHeartIconGrey] forState:UIControlStateNormal];
            [FavButton setBackgroundColor:[UIColor clearColor]];
            [FavButton setSelected:NO];
        }
        else{
            [FavButton setImage:[UIImage imageNamed:kImageHeartIconwhite] forState:UIControlStateNormal];
            [FavButton setBackgroundColor:[self getColorForRedText]];
            [FavButton setSelected:YES];
        } */
        
        if ([productObj.productStatus intValue]==0) {
            [productNameLabel setTextColor:[self getColorForBlackText]];
            [cell setBackgroundColor: [UIColor colorWithRed:0.9373 green:0.9373 blue:0.9373 alpha:1.0]];
            imgOutOfStock.hidden=NO;
            if(imgOutOfStock.tag==101){
                imgOutOfStock.image = [UIImage imageNamed:@"out-of-stock"];
            }

        }
        else    {
             imgOutOfStock.hidden=YES;
            [productNameLabel setTextColor:[self getColorForRedText]];
            [cell setBackgroundColor:[UIColor clearColor]];
        }
        
        [[leftSeperatorV layer] setBorderColor:[[UIColor colorWithRed:0.8278 green:0.8278 blue:0.8278 alpha:1.0] CGColor]];
        [[seperatorV layer] setBorderColor:[[UIColor colorWithRed:0.8278 green:0.8278 blue:0.8278 alpha:1.0] CGColor]];
        
        return cell;
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark-BUTTON ACTION

//FavButton
-(void)FavButton:(UIActionButton*)button{
    if (![button isSelected]) {
        button.productObj.productFavStatus=@"1";
        [button setSelected:YES];
        [button setImage:[UIImage imageNamed:kImageHeartIconwhite] forState:UIControlStateNormal];
        [button setBackgroundColor:[self getColorForRedText]];
        if([notloggedIn intValue]==1)
        {
            
            //If Browse option, get the products from local storage if any and add clicked product to locakl storage.
            NSMutableArray *favoriteOfflineArray = [[NSMutableArray alloc]initWithObjects:button.productObj.productImageURL,button.productObj.productName,button.productObj.productSize,button.productObj.productPrice,button.productObj.productId,button.productObj.productStatus,button.productObj.productSalesPrice,nil];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            NSArray *defaultArray = [userDefaults objectForKey:@"offlineFavoriteArray"];
            if([defaultArray count]==0)
                [appDelegate.globalFavoriteOfflineArray addObject:favoriteOfflineArray];
            else
            {
                appDelegate.globalFavoriteOfflineArray = [[userDefaults objectForKey:@"offlineFavoriteArray"]mutableCopy];
                [appDelegate.globalFavoriteOfflineArray addObject:favoriteOfflineArray];
            }
            
            //Saving Favorites global Array in NSUserDefaults
            [self saveFavoriteofflineArray:appDelegate.globalFavoriteOfflineArray];
        }
        else
             [self CallApiAddFavoriteWithProductID:button.productObj.productId];
            
           /* NSMutableArray *favoriteOfflineArray = [[NSMutableArray alloc]initWithObjects:button.productObj.productImageURL,button.productObj.productName,button.productObj.productSize,button.productObj.productPrice,button.productObj.productId,button.productObj.productStatus,button.productObj.productSalesPrice,nil];
            [appDelegate.globalFavoriteOfflineArray addObject:favoriteOfflineArray];
            
            //Saving Favorites global Array in NSUserDefaults
            [self saveFavoriteofflineArray:appDelegate.globalFavoriteOfflineArray];
        }
        else
            */
    }
    else{
        [button setSelected:NO];
        button.productObj.productFavStatus=@"0";
        [button setImage:[UIImage imageNamed:kImageHeartIconGrey] forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor clearColor]];
        if([notloggedIn intValue]==1)
        {
            [appDelegate.globalFavoriteOfflineArray removeObjectAtIndex:button.rowNumber];
           // NSLog(@"Favorite Array = %@",appDelegate.globalFavoriteOfflineArray);
            [self saveFavoriteofflineArray:appDelegate.globalFavoriteOfflineArray];
        }
        else
            [self CallApiRemoveFavoriteWithProductID:button.productObj.productId];
    }
}
-(void)saveFavoriteofflineArray:(NSArray *)offArray
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:offArray forKey:@"offlineFavoriteArray"];
    [defaults synchronize];
}
-(void)cartButton:(UIActionButton*)button{
    NSArray *temparray=[NSArray arrayWithObjects:button.productObj, nil];
   // NSLog(@"Temp Array = %@",temparray);
   // SaveProductArray
    if (![button isSelected]) {
        NSString *msg = [[ShoppingCart sharedInstace] addProductToMyShoppingCart:temparray];
        if ([msg isEqualToString:kAlertShoppingCart]) {
            [button setSelected:YES];
            [button setImage:[UIImage imageNamed:kImageCartIconwhite] forState:UIControlStateNormal];
            [button setBackgroundColor:[self getColorForYellowText]];
        }
        NSMutableArray   *searchResult=[[NSMutableArray alloc] initWithArray:[[ShoppingCart sharedInstace] fetchProductFromShoppingCart]] ;
        if(searchResult.count>0){
            [self showAlertViewWithTitle:nil message:msg];
        }
        else{
            [self showAlertViewWithTitle:nil message:msg];
        }
    }
    else{
        [button setSelected:NO];
        [button setImage:[UIImage imageNamed:kImagecarticongrey] forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor clearColor]];
        [[ShoppingCart sharedInstace] removeProductFromMyShoppingCart:temparray];
    }
}
@end