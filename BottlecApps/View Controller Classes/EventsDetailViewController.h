//
//  EventsDetailViewController.h
//  BOTTLECAPPS
//
//  Created by imac9 on 8/12/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "BaseViewController.h"

@interface EventsDetailViewController  :BaseViewController  <UITextFieldDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>
@property (retain, nonatomic)  UIButton                   *EventsAndPromotionButton;
@property (retain, nonatomic)  UIImage                    *EventsImage;
@property (retain, nonatomic)  NSString                    *getEventID;
@property (nonatomic,retain)NSMutableDictionary *eventsDict;
@property (weak, nonatomic) IBOutlet UITableView *tblEvent;

//getEventID

@end
