//
//  BaseViewController.m
//  @lacarte
//
//  Created by Kuldeep Tyagi on 11/7/14.
//  Copyright (c) 2014 Kuldeep Tyagi. All rights reserved.
//

#import "BaseViewController.h"
#import "PageControl.h"
#import "LoginViewController.h"
#import "SignUpViewController.h"
#import "ForgotPasswordViewController.h"
#import "SearchResultViewController.h"
#import "SearchBeerViewController.h"
//#import "SearchWineViewController.h"
//#import "SearchLiquorViewController.h"
#import "ShoppingCartViewController.h"
#import "ContactUsViewController.h"
#import "DealsViewController.h"
#import "MyOrdersViewController.h"
#import "OrderConfirmationViewController.h"
#import "LoyaltyCardViewController.h"
#import "NotificationsViewController.h"
#import "UserProfileViewController.h"
#import "HomeViewController.h"
#import "TermsAndConditionViewController.h"
#import "EventsDetailViewController.h"
#import "LoginViewController.h"
#import "FavoriteListViewController.h"
#import "EditAccountViewController.h"
#import "ProductDetailsViewController.h"
#import "ReviewDetailsViewController.h"   
#import "ReviewViewController.h"
#import "OrderDetailViewController.h"
#import "EventsListViewController.h"
#import "LoyaltyCardViewController.h"
#import "FavoriteBrowseViewController.h"
#import "UIActionButton.h"
#import "Constants.h"



#import "AsyncImageView.h"
#import "MTBBarcodeScanner.h"
#import "BarcodeScannerViewController.h"
#import "NotificationsViewController.h"
#import "NotificationHistoryViewController.h"
#import "CJSONDataSerializer.h"
#import "CJSONDeserializer.h"
#import "ZBarSDK.h"
#import <CoreLocation/CoreLocation.h>
//#52565a
#import "iCarousel.h"
#define API_KEY @"Path"
#define ERROR_MSG @"Message"
#define AUTH @"Authentication"
#define kphotopadding              (IS_IPHONE_4_OR_LESS? 20:IS_IPHONE_6?0:0)
#import "TOCropViewController.h"
#import <MapKit/MapKit.h>
#define kTopPaddingProfile                 (IS_IPHONE_4_OR_LESS? 10:IS_IPHONE_6?30:30)
#import "SAMTextView.h"
#import "HomeViewController.h"
// self.CustomScrollView.frame.size.width
//self.CustomScrollView.frame.size.height
#define dropdownViewHeight (HALF_SCREEN_HEIGHT+100)
#define dropdownViewWidth SCREEN_WIDTH-(40)

//

@interface BaseViewController ()  <SlideNavigationControllerDelegate,PageControlDelegate,UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,ZBarReaderDelegate,UITextFieldDelegate,TOCropViewControllerDelegate,CLLocationManagerDelegate>  {
    UIView* spinnerPanel;
    UIActionSheet *actionsheet;
    UIImagePickerController *imgPickerCamera;
    UIImagePickerController *imgPicker;
    NSString *FilePath;
    NSData *ImageData;
    UIImage *img;
    
    BOOL isbarcode;
    NSString *barcodeType;
    CLLocationManager *locationManager;
    CLLocation *location;
    NSString *LattitudeString;
    NSString *longitudeString;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    NSString *currentaddress;
    MKMapView *mapView;
    UILabel *badgeLabel;
    NSInteger  badgecount;
    
    
    NSMutableArray *productCategoryArray;
    NSMutableArray *productSubCategoryArray;
    NSMutableArray  *subCatSelectionArray;
    
    NSMutableArray  *productTypeArray;
    NSMutableArray  *postEventArray, *postItemArray,*eventTypeArray;
    UIView *transparentV;
    
    
}
@property(nonatomic,copy)UIPickerView *pickerView;
@property(nonatomic,copy)UIPickerView *pickerViewState;
@property(nonatomic,copy)NSString *pickerType;
@property (nonatomic, copy) NSData *ImageData;
@property (nonatomic, retain)UIImage *img;
@property (nonatomic, copy) NSString *FilePath;
@property (nonatomic)  NSInteger getReviewsCount;
@property (nonatomic, strong) UIImageView *imageView;

//


@end
NSString *currentlat;
NSString *currentlong;
@implementation BaseViewController
@synthesize starRating,starRatingstring,RatingView,updatedImageUrl,userProfileImage;
@synthesize ImageData,img,FilePath;
//@synthesize productTypeArray,postItemArray;
//@synthesize eventTypeArray;
//@synthesize postEventArray;
@synthesize loyaltyCardTextfield;
@synthesize barcodeID,phoneNumberTextfield;
@synthesize FavouriteListArray;
@synthesize arrayForBoolProductStatus;
@synthesize CardNumberTextFd;
@synthesize firstNameTextfield,lastNameTextfield;
#pragma mark-
- (NSMutableArray *)createMutableArray:(NSArray *)array
{
    return [NSMutableArray arrayWithArray:array];
}
#pragma mark-CallApi
-(BOOL)isNull:(id)object
{
    if (object == nil ||[object isKindOfClass:[NSNull class]]||[object isEqualToString:@"<null>"]||[object length]==0||[object isEqualToString:@"(null)"]) {
        return YES;
    }
    
    return NO;
}

-(void)insertNotificationForAllItem{
    
    User *userObj=kGetUserModel;
    NSString *userID=userObj.userId;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *storeId = [prefs stringForKey:@"StoreID"];
    // NSString *eventSelectionString = [[NSUserDefaults standardUserDefaults]valueForKey:EVENTSELECTED];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    for (int k = 0; k<[[[productTypeArray objectAtIndex:0] subCategoryArray] count]; k++)
    {
        NSMutableDictionary *dict2 = [[NSMutableDictionary alloc]init];
        [dict2 setValue:[[[[productTypeArray objectAtIndex:0] subCategoryArray] objectAtIndex:k] valueForKey:@"CatId"] forKey:@"SubCatId"];
        [dict2 setValue:[NSString stringWithFormat:@"%d",3] forKey:@"CatId"];
        [postItemArray addObject:dict2];    }
    for (int k = 0; k<[[[productTypeArray objectAtIndex:1] subCategoryArray] count]; k++)
    {//liquor=2
        NSMutableDictionary *dict2 = [[NSMutableDictionary alloc]init];
        [dict2 setValue:[[[[productTypeArray objectAtIndex:1] subCategoryArray] objectAtIndex:k] valueForKey:@"CatId"] forKey:@"SubCatId"];
        [dict2 setValue:[NSString stringWithFormat:@"%d",2] forKey:@"CatId"];
        [postItemArray addObject:dict2];    }
    for (int k = 0; k<[[[productTypeArray objectAtIndex:2] subCategoryArray] count]; k++)
    {//beer=1
        NSMutableDictionary *dict2 = [[NSMutableDictionary alloc]init];
        [dict2 setValue:[[[[productTypeArray objectAtIndex:2] subCategoryArray] objectAtIndex:k] valueForKey:@"CatId"] forKey:@"SubCatId"];
        [dict2 setValue:[NSString stringWithFormat:@"%d",1] forKey:@"CatId"];
        [postItemArray addObject:dict2];
    }
    
    [self CallapiInsertNotificationSubscription:postEventArray storeID:storeId itemsArray:postItemArray userID:userID isEventSelected:@"1"];
    
    
    
}
-(void)CallApiGetSelectedProductCategory{
    User *userObj=kGetUserModel;
    NSMutableDictionary *paramDict=[[NSMutableDictionary alloc] init];
    [paramDict setValue:kSTOREIDbottlecappsString forKey:kParamStoreId];
    [paramDict setValue:userObj.userId forKey:kParamUserId];
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] GetSelectedProductCategoryWithParam:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
        //NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
           // NSLog(@"key");
            [self hideSpinner];
            NSDictionary *dictLocal = [[NSDictionary alloc]initWithDictionary:response];
            //NSLog(@"key is %@", dictLocal);
            NSString *eventSelectionValue = [dictLocal valueForKey:@"IsEventSelected"];
           // NSLog(@"event selection value is %@", eventSelectionValue);
            
            NSMutableArray *categoryArr = [[NSMutableArray alloc] init];
            NSMutableArray *eventsAndPromotionArr = [[NSMutableArray alloc] initWithArray:[dictLocal objectForKey:@"ListEventPromotion"] copyItems:YES];
            
            [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%@",eventSelectionValue] forKey:EVENTSELECTED];
            [[NSUserDefaults standardUserDefaults]synchronize];
            NSArray *larr=[dictLocal valueForKey:@"ProductCategories"];
            for (int k = 0;k< [larr  count]; k++)
            {
                [categoryArr addObject:[[dictLocal objectForKey:@"ProductCategories"] objectAtIndex:k]];
            }
            
           // NSLog(@"event and promption array is %@", eventsAndPromotionArr);
            
            
            //NSMutableArray *arr = [[NSMutableArray alloc]initWithArray:categoryArr];
            //    [categoryArr setValue:[key valueForKey:@"ProductCategories"] forKey:@"cat"];
            //  categoryDict = [key valueForKey:@"ProductCategories"];
          //  NSLog(@"category dict is %@", arr);
          //  NSLog(@"count-----%lu",(unsigned long)[categoryArr count]);
            
            for (int i = 0; i < [categoryArr count]; i++)
            {
                category *catObj = [[category alloc]init];
                catObj.categoryId =[categoryArr[i] valueForKey:@"CatId"];
                catObj.categoryName =[categoryArr[i] valueForKey:@"CatName"];
                catObj.subCategoryArray = [[NSMutableArray alloc]init];
                NSMutableArray *arrayLocal = [[NSMutableArray alloc]initWithArray:[categoryArr[i] valueForKey:@"SubCategory"]];
                for (int j=0; j<[arrayLocal count]; j++)
                {
                    NSMutableDictionary *localDict = [[NSMutableDictionary alloc]init];
                    [localDict setValue:[[arrayLocal objectAtIndex:j] valueForKey:@"SubCatId"] forKey:@"CatId"];
                    [localDict setValue:[[arrayLocal objectAtIndex:j] valueForKey:@"SubCatName"] forKey:@"CatName"];
                    [localDict setValue:[[arrayLocal objectAtIndex:j] valueForKey:@"IsSelected"] forKey:@"catSelection"];
                    [catObj.subCategoryArray addObject:localDict];
                }
               // NSLog(@"category obj array is %@", catObj.subCategoryArray);
                
                [productTypeArray addObject:catObj];
                
            }
            for (int l = 0; l< [eventsAndPromotionArr count]; l++)
            {
                AddOptionEvent *eventObj = [[AddOptionEvent alloc]init];
               // NSLog(@"value is %@",[eventsAndPromotionArr[l] valueForKey:@"IsSelected"]);
                eventObj.eventName = [eventsAndPromotionArr[l] valueForKey:@"EventName"];
                eventObj.eventId = [eventsAndPromotionArr[l] valueForKey:@"EventId"];
                eventObj.isSelected = [eventsAndPromotionArr[l] valueForKey:@"IsSelected"];
                [eventTypeArray addObject:eventObj];
            }
          //  NSLog(@"product category array count is %lu",(unsigned long)[productTypeArray count]);
            DLog(@"productTypeArray0=========%@",[[productTypeArray objectAtIndex:0] subCategoryArray]);
            DLog(@"productTypeArray1=========%@",[[productTypeArray objectAtIndex:1] subCategoryArray]);
            DLog(@"productTypeArray2=========%@",[[productTypeArray objectAtIndex:2] subCategoryArray]);
            [self insertNotificationForAllItem];
            
           // NSLog(@"event array is %@", eventTypeArray);
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        
    }];
    
}


-(void)CallapiInsertNotificationSubscription:(NSMutableArray *)eventArr storeID:(NSString *)store_ID itemsArray:(NSMutableArray *)itemArr userID:(NSString *)user_ID isEventSelected:(NSString *)event_Selected{
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    [postDict setValue:event_Selected forKey:@"IsEventSelected"];
    [postDict setValue:kSTOREIDbottlecappsString forKey:@"StoreId"];
    [postDict setValue:itemArr forKey:@"items"];
    [postDict setValue:user_ID forKey:@"UserId"];
   // NSLog(@"post dict %@",[postDict valueForKey:@"Events"]);
    NSArray *array=[NSArray arrayWithObject:postDict];
    DLog(@"array--%@",array);
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (!jsonData) {
       // NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        DLog(@"jsonString--%@",jsonString);
        [self showSpinner]; // To show spinner
        [[ModelManager modelManager] InsertNotificationSubscriptionJsonstring:jsonString  successStatus:^(APIResponseStatusCode statusCode, id response) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self hideSpinner];
              //  NSLog(@"%@", response);
               // [self GoBack];
            });
        } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
            [self hideSpinner];
            [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        }];
    }
}

-(void)CallApiMyaccount{
    __weak User *userObj = [[ModelManager modelManager] getuserModel];
    NSMutableDictionary* paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [paramDict setObject:userObj.userId forKey: kParamUserId];
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] ViewProfileWithParam:paramDict successStatus:^(BOOL status) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}
-(void)callApiGetLoyalityCardImage{
    __weak User *userObj = kGetUserModel;
    
    NSMutableDictionary* paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [paramDict setObject:userObj.userId forKey: kParamUserId];
    //
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] GetLoyalityCardImageWithParam:paramDict successStatus:^(BOOL status) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
    }];
    
    
}


-(void)CallApiUploadimageWithDic :(NSMutableDictionary*)postJsonDict{
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] Uploadimage:postJsonDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
       // NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            if ([[response valueForKey:@"Authentication"]isEqualToString:@"true"]) {
                self. updatedImageUrl = [[NSString alloc] initWithString:[response valueForKey:@"Path"]];
                
                self.userProfileImage = [UIImage imageWithData:ImageData];
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Profile picture has been uploaded." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                [alert show];
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"Uploadimage"
                 object:self];
                
            }
            
            //  apiResponseDictionary=[[NSMutableDictionary alloc] initWithDictionary:response];
            //  DLog(@"apiResponseDictionary----%@ EventsArray---%@",apiResponseDictionary,EventsArray);
            //  EventsArray =[NSMutableArray arrayWithObjects:[apiResponseDictionary valueForKey:@"productCount"], nil];
            // [self UpdateUI];
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        
    }];
    
    
    
}
-(void)CallApiAddFavoriteWithProductID:(NSString*)productId{
    __weak User *userObj=kGetUserModel;
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setValue:kSTOREIDbottlecappsString forKey:kParamStoreId];
    [dict setValue:userObj.userId forKey:kParamAuthenticationID];
    [dict setValue:@"Product" forKey:kParamTabName];
    [dict setValue:productId forKey:kParamProductID];
    
    
    //kParamTabName
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] AddFavoriteWithParam:dict  successStatus:^(APIResponseStatusCode statusCode, id response) {
      //  NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"response-----%@", response);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self hideSpinner];
                @try {
                  //  NSLog(@"%@", response);
                    
                    if([favoritesAlreadyAdded isEqualToString:@"1"])
                    {
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults removeObjectForKey:@"offlineFavoriteArray"];
                        favoritesAlreadyAdded = @"0";
                        combinedString = @"";
                    }
                    else
                    [self showAlertViewWithTitle:nil message:[response valueForKey:@"Message"]];
                    
                    
                }
                @catch (NSException *exception) {
                    [self showAlertViewWithTitle:kError message:kInternalInconsistencyErrorMessage];
                    
                }
            });
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}


-(void)callApiStoreDetails{
   // __weak User *userObj = [[ModelManager modelManager] getuserModel];
    
    NSMutableDictionary* StoreDetailDict = [[ NSMutableDictionary alloc] init];
    [StoreDetailDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
  //  [StoreDetailDict setObject:userObj.userId forKey: kParamUserId];
    
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] GetStoreDetailWithParam:StoreDetailDict  successStatus:^(BOOL status) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            __weak ClosestStoreModel* lClosestStoreModelObj;
            __weak StoreDetails *storeDetails;
            lClosestStoreModelObj = kGetClosestStoreModel;
            storeDetails = kGetStoreDetailsModel;
            _apartmentLabel.text=storeDetails.StoreUserApartment;
            _streetLabel.text=  storeDetails.StoreUserAddress1;
            _StateLabel.text=[NSString stringWithFormat:@"%@,%@,%@",storeDetails.StoreUserCity,storeDetails.StoreStateName,storeDetails.StoreUserZipCode];
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
    
    
    
    
}

-(void)CallApiRemoveFavoriteWithProductID:(NSString*)productId{
    __weak User *userObj=kGetUserModel;
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setValue:kSTOREIDbottlecappsString forKey:kParamStoreId];
    [dict setValue:userObj.userId forKey:kParamAuthenticationID];
    [dict setValue:@"Product" forKey:kParamTabName];
    [dict setValue:productId forKey:kParamProductID];
    //kParamTabName
    [self showSpinner];
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] RemoveFavoriteWithParam:dict  successStatus:^(APIResponseStatusCode statusCode, id response) {
      //  NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            @try {
               // NSLog(@"%@", response);
                if ([[response valueForKey:@"Message"] boolValue]==TRUE) {
                    [self showAlertViewWithTitle:nil message:kAlertRemoveFavorite];
                    
                    
                }
                else {
                    [self showAlertViewWithTitle:nil message:kAlertNotRemoveFavorite];
                    
                }
                
            }
            @catch (NSException *exception) {
                [self showAlertViewWithTitle:kError message:kInternalInconsistencyErrorMessage];
                
            }
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}



#pragma mark-
-(NSString *)dateStringFromString:(NSString *)sourceString sourceFormat:(NSString *)sourceFormat destinationFormat:(NSString *)destinationFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:sourceFormat];
    NSDate *date = [dateFormatter dateFromString:sourceString];
    [dateFormatter setDateFormat:destinationFormat];
    return [dateFormatter stringFromDate:date];
    DLog(@"dateFormatter--%@",[dateFormatter stringFromDate:date]);
}


-(BOOL) validateEmail: (NSString *) email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

-(void)AddBackgroundImage{
    /**************add background image**************/
    
    if(kStoreBottleCapps == 11)
    {
        UIView *backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        backgroundView.backgroundColor = [UIColor colorWithRed:23.0/255.0 green:23.0/255.0 blue:24.0/255.0 alpha:1.0];
        [self.view addSubview:backgroundView];
 
    }
    else
    {
    UIImageView *backgroundimageView=[self createImageViewWithRect:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) image:[UIImage imageNamed:kImageBackground]];
    [self.view addSubview:backgroundimageView];
    }
    
    
//       /**************add background image**************/
    
}
-(UIButton*)AddCustomButtonWithRedBorder:(CGRect)buttonFrame withTitle:(NSString*)title{
    UIButton *ButtonWithRedBorder;
    ButtonWithRedBorder=[self createButtonWithFrame:buttonFrame withTitle:title];
    [ButtonWithRedBorder setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
    [ButtonWithRedBorder.titleLabel setFont:[UIFont fontWithName:kFontBold size:kFontBoldSize]];
    [[ButtonWithRedBorder layer] setBorderWidth:1.0f];
    [[ButtonWithRedBorder layer] setBorderColor:[self getColorForRedText].CGColor];
    [self.view addSubview:ButtonWithRedBorder];
    return ButtonWithRedBorder;
    
}
-(void)AddNavigationControllerWithTitleAndKartButton{
    /************** set up navigation controller with image******************/
    
    DLog(@"class-----%@",[self class])
  /*  UIImage *HeaderLogoImage=[UIImage imageNamed:kImageHeaderLogo];
    UIImageView *logoimageview=[self createImageViewWithRect:CGRectMake(0,0, (HeaderLogoImage.size.width)-5, (HeaderLogoImage.size.height)-5) image:HeaderLogoImage];
    UIView *navigationTitleview=[self createContentViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kNavigationheight)];
    
    UILabel *navigationTitleLabel=[self createLblWithRect:CGRectMake(0, 0, SCREEN_WIDTH, kNavigationheight) font:[self addFontSemiBold] text:kEmptyString textAlignment:NSTextAlignmentCenter textColor:[UIColor whiteColor]];
    [navigationTitleview addSubview:navigationTitleLabel]; */
    
    if ([self isKindOfClass:[HomeViewController class]]) {
            [self setNavBarTitle:kHeaderTitle controller:self italic:NO];
    }

    else if ([self isKindOfClass:[EventsDetailViewController class]]){
        [self setNavBarTitle:kNavigationTitleEVENTDETAILS controller:self italic:NO];
    }
    else if ([self isKindOfClass:[EventsListViewController class]]){
        [self setNavBarTitle:kNavigationTitleEVENTS controller:self italic:NO];
    }
    
    else if ([self isKindOfClass:[SignUpViewController class]]){
        [self setNavBarTitle:kNavigationTitleSignUp controller:self italic:NO];
    }
    
    else if ([self isKindOfClass:[UserProfileViewController class]]){
        [self setNavBarTitle:kNavigationTitlePROFILE controller:self italic:NO];
    }
    else if ([self isKindOfClass:[EditAccountViewController class]]){
        [self setNavBarTitle:kNavigationTitleEDITPROFILE controller:self italic:NO];
    }
    else if ([self isKindOfClass:[ShoppingCartViewController class]]){
        [self setNavBarTitle:kNavigationTitleCART controller:self italic:NO];
    }
    
    else if ([self isKindOfClass:[OrderConfirmationViewController class]]){
        [self setNavBarTitle:kNavigationTitleORDERCONFIRMATION controller:self italic:NO];
    }
    else if ([self isKindOfClass:[OrderDetailViewController class]]){
        [self setNavBarTitle:kNavigationTitleORDERDETAILS controller:self italic:NO];
    }
    
    else if ([self isKindOfClass:[ContactUsViewController class]]){
        [self setNavBarTitle:kNavigationTitleCONTACTUS controller:self italic:NO];
    }
    else if ([self isKindOfClass:[DealsViewController class]]){
        [self setNavBarTitle:kNavigationTitleDEALS controller:self italic:NO];
    }
    else if ([self isKindOfClass:[LoyaltyCardViewController class]]){
        [self setNavBarTitle:kNavigationTitleLOYALTYCARD controller:self italic:NO];
    }
    
    else if ([self isKindOfClass:[NotificationsViewController class]]){
        [self setNavBarTitle:kNavigationTitleMANAGENOTIFICATIONS controller:self italic:NO];
    }
    else if ([self isKindOfClass:[NotificationHistoryViewController class]]){
        [self setNavBarTitle:kNavigationTitleNOTIFICATIONS controller:self italic:NO];
    }
    else if ([self isKindOfClass:[MyOrdersViewController class]]){
        [self setNavBarTitle:kNavigationTitleVIEWORDERS controller:self italic:NO];
    }
    else if ([self isKindOfClass:[SearchResultViewController class]]){
        [self setNavBarTitle:kNavigationTitleFINDAPRODUCT controller:self italic:NO];
    }
    else if ([self isKindOfClass:[FavoriteListViewController class]]){
        [self setNavBarTitle:kNavigationTitleFAVORITES controller:self italic:NO];
    }
    else if ([self isKindOfClass:[FavoriteBrowseViewController class]]){
        [self setNavBarTitle:kNavigationTitleFAVORITES controller:self italic:NO];
    }
    else if ([self isKindOfClass:[TermsAndConditionViewController class]]){
        [self setNavBarTitle:kNavigationTitleTERMSANDCONDITIONS controller:self italic:NO];
    }
    else if ([self isKindOfClass:[ReviewViewController class]]){
        [self setNavBarTitle:kNavigationTitleREVIEWS controller:self italic:NO];
    }
    else if ([self isKindOfClass:[ReviewDetailsViewController class]]){
        [self setNavBarTitle:kNavigationTitleREVIEWS controller:self italic:NO];
    }
    else if ([self isKindOfClass:[ProductDetailsViewController class]]){
        [self setNavBarTitle:kNavigationTitlePRODUCTDETAILS controller:self italic:NO];
    }
    else if ([self isKindOfClass:[BarcodeScannerViewController class]]){
        [self setNavBarTitle:kNavigationTitleBARCODESCANNER controller:self italic:NO];
    }
    
    
    //ProductDetailsViewController
    UIImage *kartImage=[UIImage imageNamed:kImageKartIcon];
    UIButton *kartButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0, (kartImage.size.width)-20,(kartImage.size.height)-20)];
    [kartButton setBackgroundImage:kartImage forState:UIControlStateNormal];
    [kartButton addTarget:self action:@selector(kartButton:)
         forControlEvents:UIControlEventTouchUpInside];
    
    //Commented Refresh Image due to never use in application.
  /*  UIImage *refreshImage=[UIImage imageNamed:kImage_refreshicon];
    
    _refreshButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0, (refreshImage.size.width)-20,(refreshImage.size.height)-20)];
    [_refreshButton setBackgroundImage:refreshImage forState:UIControlStateNormal]; */
    
    
    UIBarButtonItem * right_item ;
    if ([self isKindOfClass:[SignUpViewController class]]) {
        right_item = [[UIBarButtonItem alloc] initWithCustomView:_refreshButton];
        self.navigationItem.leftBarButtonItem = nil;
        
    }
    else if ([self isKindOfClass:[TermsAndConditionViewController class]]){
        right_item = [[UIBarButtonItem alloc] initWithCustomView:nil];
    }
    else{
        UIImage *badgeimg=[UIImage imageNamed:kImagebadge];
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]) {
            NSArray *shoppingbagarray=[[ShoppingCart sharedInstace] fetchProductFromShoppingCart];
            badgecount=[shoppingbagarray count];
            badgeLabel=[self createLblWithRect:CGRectMake((kartButton.frame.size.width/2)+4,0, badgeimg.size.width,badgeimg.size.height) font:[UIFont fontWithName:kFontBold size:10] text:[NSString stringWithFormat:@"%ld",(long)badgecount] textAlignment:NSTextAlignmentCenter textColor:[UIColor whiteColor   ]];
        }
        else{
            badgeLabel=[self createLblWithRect:CGRectMake((kartButton.frame.size.width/2)+4,0, badgeimg.size.width,badgeimg.size.height) font:[UIFont fontWithName:kFontBold size:10] text:[NSString stringWithFormat:@"%d",0] textAlignment:NSTextAlignmentCenter textColor:[UIColor whiteColor   ]];
        }

        [badgeLabel setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:kImagebadge]]];
        [kartButton addSubview:badgeLabel];
        if (badgecount==0) {
            [badgeLabel setHidden:YES];
        }
        else {
            [badgeLabel setHidden:NO];
            
            
        }
        
        right_item = [[UIBarButtonItem alloc] initWithCustomView:kartButton];
    }
    self.navigationItem.rightBarButtonItem = right_item;
    /************** set up navigation controller with image******************/
}
-(void)addNorecordFoundLAbelOnTableView:(UITableView*)tableView{
    UILabel *label = [[UILabel alloc] init];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[self getColorForBlackText]];
    [label setText:kAlertNoRecordFound];
    [label sizeToFit];
    label.frame = CGRectMake((tableView.bounds.size.width - label.bounds.size.width) / 2.0f,
                             50,
                             label.bounds.size.width,
                             label.bounds.size.height);
    [tableView insertSubview:label atIndex:0];
    
}

-(void)AddDefaultTableView{
    /**********************************Setup tableView****************************************/
    CGRect defaultTableViewFrame;
    if ([self isKindOfClass:[SearchBeerViewController class]]) {
        defaultTableViewFrame=CGRectMake(0,kNavigationheight*2
                                         , SCREEN_WIDTH, (SCREEN_HEIGHT)-(kNavigationheight*2)-kTopPadding_small-kBottomViewHeight);
        
    }
    
    else if ([self isKindOfClass:[ShoppingCartViewController class]]){
        defaultTableViewFrame=CGRectMake(0,kNavigationheight
                                         , SCREEN_WIDTH,( (SCREEN_HEIGHT)/2)-kBottomViewHeight);
    }
    else if ([self isKindOfClass:[FavoriteListViewController class]]){
        defaultTableViewFrame=CGRectMake(0,kNavigationheight
                                         , SCREEN_WIDTH, SCREEN_HEIGHT-(kBottomViewHeight)-kNavigationheight-kCustomButtonContainerHeight);
    }
    else if ([self isKindOfClass:[FavoriteBrowseViewController class]]){
        defaultTableViewFrame=CGRectMake(0,kNavigationheight
                                         , SCREEN_WIDTH, SCREEN_HEIGHT-(kBottomViewHeight)-kNavigationheight-kCustomButtonContainerHeight);
    }
    else if ([self isKindOfClass:[FavoriteBrowseViewController class]]){
        defaultTableViewFrame=CGRectMake(0,kNavigationheight
                                         , SCREEN_WIDTH, SCREEN_HEIGHT-(kBottomViewHeight)-kNavigationheight-kCustomButtonContainerHeight);
    }
    else if ([self isKindOfClass:[ProductDetailsViewController class]]){
        defaultTableViewFrame=CGRectMake(0,kNavigationheight
                                         , SCREEN_WIDTH, SCREEN_HEIGHT-(kCustomButtonContainerHeight*2)-kNavigationheight-kBottomViewHeight);
    }
    
    else if ([self isKindOfClass:[DealsViewController class]]){
        defaultTableViewFrame=CGRectMake(0,self.CustomScrollView.frame.size.height+self.CustomScrollView.frame.origin.y
                                         , SCREEN_WIDTH, SCREEN_HEIGHT-((kBottomViewHeight)+kNavigationheight+kTopPadding_small+self.CustomScrollView.frame.size.height));
    }
    else if ([self isKindOfClass:[EventsDetailViewController class]]){
        defaultTableViewFrame=CGRectMake(0,self.CustomScrollView.frame.size.height+self.CustomScrollView.frame.origin.y
                                         , SCREEN_WIDTH, SCREEN_HEIGHT-((kBottomViewHeight)+kNavigationheight+kTopPadding_small+self.CustomScrollView.frame.size.height));
    }
    
    else if ([self isKindOfClass:[EventsListViewController class]]){
        defaultTableViewFrame=CGRectMake(0,self.CustomScrollView.frame.size.height+self.CustomScrollView.frame.origin.y
                                         , SCREEN_WIDTH, SCREEN_HEIGHT-((kBottomViewHeight)+kNavigationheight+kTopPadding_small+self.CustomScrollView.frame.size.height));
    }
    else if ([self isKindOfClass:[OrderDetailViewController class]]){
        defaultTableViewFrame=CGRectMake(0,kNavigationheight
                                         , SCREEN_WIDTH, SCREEN_HEIGHT-kBottomViewHeight-kNavigationheight);
    }
    else if ([self isKindOfClass:[OrderConfirmationViewController class]]){
        defaultTableViewFrame=CGRectMake(0,kNavigationheight
                                         , SCREEN_WIDTH, SCREEN_HEIGHT-kBottomViewHeight-kNavigationheight-kCustomButtonContainerHeight);
    }
    else if ([self isKindOfClass:[MyOrdersViewController class]]){
        defaultTableViewFrame=CGRectMake(0,self.CustomScrollView.frame.size.height+self.CustomScrollView.frame.origin.y
                                         , SCREEN_WIDTH, SCREEN_HEIGHT-(self.CustomScrollView.frame.size.height+kNavigationheight+kCustomButtonContainerHeight+kBottomViewHeight));
    }
    
    else if ([self isKindOfClass:[NotificationHistoryViewController class]]){
        defaultTableViewFrame=CGRectMake(0,kNavigationheight
                                         , SCREEN_WIDTH, SCREEN_HEIGHT-(kBottomViewHeight)-kNavigationheight-kCustomButtonContainerHeight);
    }
    else{
        defaultTableViewFrame=CGRectMake(0,(self.CustomScrollView.frame.origin.y+self.CustomScrollView.frame.size.height) ,
                                         SCREEN_WIDTH,
                                         SCREEN_HEIGHT-(self.CustomScrollView.frame.origin.y+self.CustomScrollView.frame.size.height)-kBottomViewHeight);
    }
    self.defaultTableView = [[UITableView alloc] initWithFrame:defaultTableViewFrame
                             
                                                         style:UITableViewStylePlain];
    self.defaultTableView.pagingEnabled = NO;
    [self.defaultTableView setSeparatorColor: [UIColor lightGrayColor]];
    self.defaultTableView .separatorStyle=UITableViewCellSeparatorStyleNone;
    self.defaultTableView.alwaysBounceVertical = NO;
    
    [self.view addSubview:self.defaultTableView];
    
    
    
    /**********************************Setup tableView****************************************/
    
    
    
}
- (void) getuserImage{
    __weak UserProfile *userProfileObj = [[ModelManager modelManager] getUserProfileModel];
    
    NSURL *imageURL = [NSURL URLWithString:userProfileObj.UserImage];
    
    if ( imageURL) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                UIImage  *userImage = [UIImage imageWithData:imageData];
                [self.addAPhotoButton setTitle:kEmptyString forState:UIControlStateNormal];
                [self.addAPhotoButton setBackgroundColor:[UIColor clearColor]];
                [self.addAPhotoButton setImage: userImage forState:UIControlStateNormal];
                
                
            });
        });
        
    }
    else{
        [self.addAPhotoButton setTitle:kButtonTitleAddAPhoto forState:UIControlStateNormal];
        
    }
    
    
    
}


-(void)addAPhotoButtonClicked:(UIButton*)addAPhotoButton{
    
    [self addImagePickerOnClickOfButton:addAPhotoButton];
}
-(void)CreateAddAphotoButton{
    UIImage *addAPhotoButtonImage=[UIImage imageNamed:kImage_addaphotobutton];
    if ([self isKindOfClass:[SignUpViewController class]]) {
        self.addAPhotoButton=[self createButtonWithFrame:CGRectMake(((SCREEN_WIDTH-(addAPhotoButtonImage.size.width))/2), kTopPadding, addAPhotoButtonImage.size.width, addAPhotoButtonImage.size.height) withTitle:kButtonTitleAddAPhoto];
        [self.addAPhotoButton addTarget:self action:@selector(addAPhotoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        self.addAPhotoButton.layer.cornerRadius = (addAPhotoButtonImage.size.height)/2.0f;
    }
    else if ([self isKindOfClass:[UserProfileViewController class]]){
        self.addAPhotoButton=[self createButtonWithFrame:CGRectMake(((SCREEN_WIDTH-((addAPhotoButtonImage.size.width)-kphotopadding))/2), kTopPaddingProfile, (addAPhotoButtonImage.size.width)-kphotopadding, (addAPhotoButtonImage.size.height)-kphotopadding) withTitle:kEmptyString];
        self.addAPhotoButton.layer.cornerRadius = ((addAPhotoButtonImage.size.height)-kphotopadding)/2.0f;
    }
    else {
        self.addAPhotoButton=[self createButtonWithFrame:CGRectMake(((SCREEN_WIDTH-(addAPhotoButtonImage.size.width))/2), kTopPaddingProfile, addAPhotoButtonImage.size.width, addAPhotoButtonImage.size.height) withTitle:kButtonTitleAddAPhoto];
        [self.addAPhotoButton addTarget:self action:@selector(addAPhotoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        self.addAPhotoButton.layer.cornerRadius = (addAPhotoButtonImage.size.height)/2.0f;
        
    }
    //   [self.addAPhotoButton.imageView setContentMode:UIViewContentModeScaleAspectFill];
    
    self.addAPhotoButton.clipsToBounds = YES;
    [self.addAPhotoButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.addAPhotoButton  setBackgroundColor:[self getColorForRedText]];
    self.addAPhotoButton.titleLabel.font=[UIFont fontWithName:kFontBold size:12];
    self.addAPhotoButton.titleLabel.numberOfLines=2;
    self.addAPhotoButton.layer.borderColor=[UIColor whiteColor].CGColor;
    self.addAPhotoButton.layer.borderWidth=2.0f;
    self.addAPhotoButton .tag=kButtonTagSignUpAddAphotoButton;
    [self.addAPhotoButton setEnabled:YES];
    
    
}

-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar{
    if(simpleNumber.length==0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    // check if the number is to long
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"($1) $2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"$1$2$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    return simpleNumber;
}

-(void)addScrollView  {
    // create a scrollview for image sliding
    CGRect ScrollFrame;
    if ([self isKindOfClass:[LoginViewController class]]) {
        ScrollFrame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kLoginfooterHeight);
    }
    else if ([self isKindOfClass:[ReviewDetailsViewController class]]) {
        ScrollFrame = CGRectMake(0, kNavigationheight, SCREEN_WIDTH, SCREEN_HEIGHT - kLoginfooterHeight-kNavigationheight);
    }
    else if ([self isKindOfClass:[MyOrdersViewController class]]){
        ScrollFrame = CGRectMake(0,kNavigationheight+kTopPadding_small
                                 , SCREEN_WIDTH, ((SCREEN_HEIGHT/3)+50)-kNavigationheight-kTopPadding_small);
    }
    else if ([self isKindOfClass:[ForgotPasswordViewController class]]){
        ScrollFrame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kLoginfooterHeight);
        
    }
    else if ([self isKindOfClass:[UserProfileViewController class]]){
        ScrollFrame = CGRectMake(0,kNavigationheight+kTopPadding_small
                                 , SCREEN_WIDTH, SCREEN_HEIGHT-kNavigationheight-kTopPadding_small-kBottomViewHeight);
    }
    else if ([self isKindOfClass:[ProductDetailsViewController class]]){
        ScrollFrame = CGRectMake(0,kNavigationheight+kTopPadding_small
                                 , SCREEN_WIDTH, SCREEN_HEIGHT-kNavigationheight-kTopPadding_small-kBottomViewHeight);
    }
    else if ([self isKindOfClass:[EditAccountViewController class]]){
        ScrollFrame = CGRectMake(0,kNavigationheight+kTopPadding_small
                                 , SCREEN_WIDTH, SCREEN_HEIGHT-kNavigationheight-kTopPadding_small-kBottomViewHeight);
    }
    else if ([self isKindOfClass:[SignUpViewController class]]||[self isKindOfClass:[ReviewDetailsViewController class]]||[self isKindOfClass:[EditAccountViewController class]]){
        ScrollFrame = CGRectMake(0,kNavigationheight
                                 , SCREEN_WIDTH, SCREEN_HEIGHT-kNavigationheight-kBottomViewHeight);
    }
    else if ([self isKindOfClass:[DealsViewController class]]||[self isKindOfClass:[EventsListViewController class]]){
        ScrollFrame = CGRectMake(0,kNavigationheight+kTopPadding_small
                                 , SCREEN_WIDTH, ((SCREEN_HEIGHT/3)+50)-kNavigationheight-kTopPadding_small);
    }
    else if ([self isKindOfClass:[EventsDetailViewController class]]){
        ScrollFrame = CGRectMake(0,kNavigationheight+kTopPadding_small
                                 , SCREEN_WIDTH, ((SCREEN_HEIGHT/3)+50)-kNavigationheight-kTopPadding_small);
    }
    
    else if ([self isKindOfClass:[ContactUsViewController class]]){
        ScrollFrame = CGRectMake(0,kNavigationheight
                                 , SCREEN_WIDTH, SCREEN_HEIGHT-kNavigationheight-kBottomViewHeight-(kCustomButtonContainerHeight*2));
    }
    else if ([self isKindOfClass:[NotificationsViewController class]]){
        ScrollFrame = CGRectMake(0,kNavigationheight
                                 , SCREEN_WIDTH, SCREEN_HEIGHT-kNavigationheight-kBottomViewHeight);
    }
    else {
        ScrollFrame = CGRectMake(0,kNavigationheight+kTopPadding_small
                                 , SCREEN_WIDTH, (SCREEN_HEIGHT/2)-kNavigationheight-kTopPadding_small);
    }
    self.CustomScrollView =[self createScrollViewWithFrame:ScrollFrame];
    [self.view addSubview:self.CustomScrollView];
    [self.CustomScrollView setBackgroundColor:[UIColor clearColor]];
    [self.CustomScrollView setNeedsDisplay];
    self.CustomScrollView.contentOffset = CGPointZero;
}

-(void) receiveBadgeCountNotification:(NSNotification*)notification
{
    if ([notification.name isEqualToString:@"getBadgeCountNotification"])
    {
        NSDictionary* userInfo = notification.userInfo;
        NSNumber* total = [userInfo valueForKey:@"badgecount"];
        //NSLog (@"Successfully received test notification! %i", total.intValue);
        badgecount=total.intValue;
        [[NSUserDefaults standardUserDefaults]setObject:[userInfo valueForKey:@"badgecount"] forKey:@"badgecount"];
        [badgeLabel setText:[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"]];
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"badgecount"] intValue]>0) {
            [badgeLabel setHidden:NO];
        }
        else{
            [badgeLabel setHidden:YES];
            
        }
    }
}

-(void)initializeAllArrayAndDict{
    
    productCategoryArray = [[NSMutableArray alloc]init];
    productSubCategoryArray = [[NSMutableArray alloc]init];
    productTypeArray = [[NSMutableArray alloc]init];
    subCatSelectionArray =  [[NSMutableArray alloc]init];
    postEventArray = [[NSMutableArray alloc]init];
    postItemArray = [[NSMutableArray alloc]init];
  //  NSLog(@"post array is :%@",postEventArray);
  //  NSLog(@"postItemArray :%@",postItemArray);
    eventTypeArray=[NSMutableArray new];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];DLog(@"");
    badgecount=0;
    locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    isbarcode=NO;
    imgPickerCamera = [[UIImagePickerController alloc] init];
    imgPicker = [[UIImagePickerController alloc] init];
    self.navigationController .navigationBarHidden =NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    // Do any additional setup after loading the view.
    spinnerPanel = nil;
    if ([self isKindOfClass:[LoginViewController class]]) {
        self.navigationController.navigationBarHidden = YES;
        [self addScrollView];
    }
    if ([self isKindOfClass:[LoginViewController class]]||[self isKindOfClass:[SignUpViewController class]]||[self isKindOfClass:[ForgotPasswordViewController class]]) {
        [self AddBackgroundImage];
    }
    [self AddNavigationControllerWithTitleAndKartButton];
    if (![self isKindOfClass:[LoginViewController class]]) {
        [self AddBottomView];
    }
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(receiveBadgeCountNotification:) name:@"getBadgeCountNotification" object:nil];
    [self initializeAllArrayAndDict];
    for (int i = 1; i < 4; i++)
    {
        NSMutableDictionary *dict1 = [[NSMutableDictionary alloc]init];
        [dict1 setValue:[NSString stringWithFormat:@"%d",i] forKey:@"EventId"];
        [postEventArray addObject:dict1];
    }
    
}

-(void)viewDidAppear:(BOOL)animated {
    
}

-(void)dealloc  {
    spinnerPanel  = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(float) floatWithTwoDecimalPoints:(float) myFloat  {
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setPositiveFormat:@"0.##"];
    return [[fmt stringFromNumber:[NSNumber numberWithFloat:myFloat]] doubleValue];
}

#pragma mark- customize the navigation bar
-(void)customizeTheNavigationbar:(UIViewController*)controller    {
    // navigation header image
    if ([controller.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:kImageHeaderLogo];
        [controller.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }
    //set navigation back button
    [controller.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    controller.navigationController.navigationBar.topItem.backBarButtonItem = [[UIBarButtonItem alloc]
                                                                               initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

-(void)setNavBarTitle:(NSString*)title controller:(UIViewController*)controller italic:(BOOL) isItalic  {
    controller.navigationItem.title = title;
    NSDictionary *attributes;
    if ([self isKindOfClass:[HomeViewController class]]) {
        if(kStoreBottleCapps == 7)
            attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize], NSFontAttributeName, nil];
        else
            attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:kFontBoldExtra size:kFontBoldSizeExtraSize], NSFontAttributeName, nil];
    }
    else
    {
        //  NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:(isItalic?kFontBoldItalic:kFontRegular) size:19], NSFontAttributeName, nil];
        attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize], NSFontAttributeName, nil];
        
    }
    controller.navigationController.navigationBar.titleTextAttributes = attributes;
}
-(UIBarButtonItem*) createBackButtonWithSelector:(SEL)selectorName  {
    UIButton *childBackBtn = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 12, 20)];
    [childBackBtn setBackgroundImage:[UIImage imageNamed:@"back-icon.png"] forState:UIControlStateNormal];
    [childBackBtn addTarget:self action:selectorName forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:childBackBtn];
}

#pragma mark- Methods for Spinner controls
-(void)createLoadingViewToSuperView:(UIView*) superView   {
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    RTSpinKitView *spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleArc color:[UIColor whiteColor]];
    spinner.center = CGPointMake(CGRectGetMidX(screenBounds), CGRectGetMidY(screenBounds));
    spinnerPanel = [[UIView alloc] initWithFrame:CGRectOffset(screenBounds, 0.0, 0.0)];
    spinnerPanel.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.3];
    [spinnerPanel addSubview:spinner];
    [spinner startAnimating];
    spinnerPanel.alpha = 0.0;
    [superView addSubview: spinnerPanel];
    [superView bringSubviewToFront:spinnerPanel];
}

// Mathod to show the spinner
-(void) showSpinner    {
    if(!spinnerPanel)    {
        [self createLoadingViewToSuperView:self.view];
    }
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations: ^{
        spinnerPanel.alpha = 1.0;
    }completion:nil];
}

// Mathod to show the spinner
-(void) showSpinnerToSuperView:(UIView*) superView    {
    if(!spinnerPanel)    {
        [self createLoadingViewToSuperView:superView];
    }
    [superView bringSubviewToFront:spinnerPanel];
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations: ^{
        spinnerPanel.alpha = 1.0;
    }completion:nil];
}
-(void)showSpinner1
{
    //if(!spinnerPanel)    {
        [self createLoadingViewToSuperView:self.view];
    //}
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations: ^{
        spinnerPanel.alpha = 1.0;
    }completion:nil];
}

//Mathod to hide the spinner
-(void) hideSpinner   {
    if(spinnerPanel)    {
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations: ^{
            spinnerPanel.alpha = 0.0;
        }completion:nil];
        [spinnerPanel removeFromSuperview];
    }
}

#pragma mark- pop and dismiss the view controller
-(void) popViewControllerWithDelay:(double)delay complition:(completion)complition  {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
        if(complition)
            complition(YES);
    });
}

-(void) dismissViewControllerWithDelay:(double)delay complition:(completion)complition  {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:^{
            if(complition)
                complition(YES);
        }];
    });
}

#pragma mark - Instantiate and return View Controller in storyboard
-(id) viewControllerWithIdentifier:(NSString*)identifier  {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardIdentifier bundle:nil];
    id viewController = [storyboard instantiateViewControllerWithIdentifier:identifier];
    return viewController;
}
-(void)addGreyBorderToCartAndLikeVike :(UIView*)view{
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [[UIColor grayColor] CGColor];
    upperBorder.frame = CGRectMake(0, 0,1.0f, CGRectGetWidth(view.frame));
    [view.layer addSublayer:upperBorder];
    
    
}
#pragma mark- crop the image
- (UIImage *)crop:(UIImage *)image convertToSize:(CGRect) rect {
    CGImageRef subImage = CGImageCreateWithImageInRect(image.CGImage, rect);
    UIImage *finalImage = [UIImage imageWithCGImage:subImage];
    CGImageRelease(subImage);
    return finalImage;
}

// temp code need to be completed
-(UIImage*)imageNamed:(NSString*) imageName  {
    NSString* string = [[NSBundle mainBundle] pathForResource:imageName ofType:@"png"];
    return [UIImage imageWithContentsOfFile:string];
}

#pragma mark- convert Date Format
//NSString* date = [self convertDateFormat:@"2014-05-27 19:55:00" currentFormat:@"yyyy-MM-dd HH:mm:ss" newFormat:@"dd, MMM YYYY"];
- (NSString*) convertDateFormat: (NSString*)date currentFormat:(NSString*) currentFormat newFormat : (NSString*) newFormat    {
    NSDateFormatter * dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:currentFormat];
    NSDate *dateObject = [dateFormat dateFromString:date];
    [dateFormat setDateFormat:newFormat]; // set new date format
    return [dateFormat stringFromDate:dateObject];
}

- (NSString*) convertDateToClientrePresentFormat: (NSString*)date   {
    if ([self isKindOfClass:[EventsDetailViewController class]]) {
        return [self convertDateFormat: date currentFormat:@"MMM-dd-yyyy" newFormat:kClientDateFormat];
        
    }
    else{
        return [self convertDateFormat: date currentFormat:kServerDateFormat newFormat:kClientDateFormat];
    }
}
#pragma mark-ADD FONT
-(UIFont*)addFontRegular{
    UIFont *font=[UIFont fontWithName:kFontRegular size:kFontRegularSize];
    return font;
    
}
-(UIFont*)addFontBold{
    UIFont *font=[UIFont fontWithName:kFontBold size:kFontBoldSize];
    return font;
    
}
-(UIFont*)customeFontNeha{
    UIFont *font=[UIFont fontWithName:kFontSemiBold size:10];
    return font;
    
}
-(UIFont*)addFontSemiBold{
    UIFont *font=[UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize];
    return font;
    
}

#pragma mark- set gradient inside the view
// set gradient inside the view
-(void) setGradient:(UIView*)view colors:(NSArray*)colors locations:(NSArray*)locations {
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = view.layer.bounds;
    gradientLayer.colors = colors;
    gradientLayer.locations = locations;
    gradientLayer.cornerRadius = view.layer.cornerRadius;
    [view.layer addSublayer:gradientLayer];
    gradientLayer = nil;
}
- (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message {
    UIAlertView *alertView = alertView = [[UIAlertView alloc] initWithTitle:title
                                                                    message:message
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
    
    [alertView show];
}
-(void)showConfirmViewWithTitle:(NSString *)title message:(NSString *)message otherButtonTitle:(NSString *)otherButtonTitle
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                                    message:message
                                                                   delegate:self
                                                          cancelButtonTitle:@"Cancel"
                                                          otherButtonTitles:otherButtonTitle,nil];
    
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
     if([notloggedIn intValue]==1)
     {
         if(buttonIndex == 1)
         {
            [self performSegueWithIdentifier:kGoToLoginViewControllerSegueId sender:nil];
         }
         else {
             if([deliverClicked intValue] == 1)
                 deliverClicked = 0;
             else if([pickUpClicked intValue]==1)
                 pickUpClicked = 0;
             else{}
             
         }
     }
}

#pragma mark-

/**************************** create contentview **************************/

-(UIView*)createContentViewWithFrame:(CGRect) frame   {
    UIView* ContentView = [[UIView alloc] initWithFrame:frame];
    [ContentView setBackgroundColor:[UIColor clearColor]]; // temp code
    return ContentView;
}

/**************************** create textview **************************/

/**************************** create scrollview **************************/

-(UIScrollView*)createScrollViewWithFrame:(CGRect) frame   {
    self. scrollView = [[UIScrollView alloc] initWithFrame:frame];
    [self. scrollView setScrollEnabled:YES];
    [self. scrollView setPagingEnabled:NO];
    [self. scrollView setBounces:YES];
    [self. scrollView setBouncesZoom:NO];
    [self. scrollView setDirectionalLockEnabled:YES];
    [self. scrollView setShowsVerticalScrollIndicator:YES];
    [self. scrollView setShowsHorizontalScrollIndicator:NO];
    [self. scrollView.layer setMasksToBounds:YES];
    [self. scrollView setBackgroundColor:[UIColor clearColor]]; // temp code
    return self. scrollView;
}

/**************************** create textview **************************/

-(UITextView*) createTxtViewWithRect:(CGRect)rect text:(NSMutableAttributedString*) text withTextColor:(UIColor*)textcolor withfont:(UIFont*)font  {
    UITextView* txtView = [[UITextView alloc] initWithFrame: rect];
    
    [txtView setBackgroundColor:[UIColor clearColor]];
    txtView.attributedText = text;
    txtView.font = font;
    txtView.textContainerInset = UIEdgeInsetsMake(5, 20, 5, 0);
    return txtView;
}
-(UITextView*) createDefaultTxtViewWithRect:(CGRect)rect text:(NSString*) text withTextColor:(UIColor*)textcolor withfont:(UIFont*)font  {
    UITextView* txtView = [[UITextView alloc] initWithFrame: rect];
    txtView.backgroundColor = [UIColor clearColor];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:txtView.frame] ;
    [self.view addSubview:imageView];
    [self.view bringSubviewToFront:txtView];
    txtView.text = text;
    txtView.font = font;
    [txtView setTextColor:textcolor];
    txtView.textContainerInset = UIEdgeInsetsMake(0, 0, 15, 20);
    return txtView;
}
-(SAMTextView*) createCustomTxtViewWithRect:(CGRect)rect text:(NSString*) text withTextColor:(UIColor*)textcolor withfont:(UIFont*)font  {
    SAMTextView* txtView = [[SAMTextView alloc] initWithFrame: rect];
    txtView.backgroundColor = [UIColor clearColor];
    txtView.text = text;
    txtView.font = font;
    txtView.textColor=textcolor;
    txtView.textContainerInset = UIEdgeInsetsMake(0, 0, 15, 20);
    return txtView;
}
//monika 16Nov
-(UITextField*)createDefaultTxtfieldWithRect:(CGRect)rect text:(NSString*) text withTag:(int)textfieldTag withPlaceholder:(NSString*)placeholder{
    UITextField  *TextField = [[UITextField alloc] initWithFrame: rect];
    TextField.textColor = [self getColorForBlackText];
    TextField.font = [self addFontRegular];
    TextField.tag=textfieldTag;
    TextField.text=text;
    TextField.delegate=self;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    // TextField.leftView = paddingView;
    TextField.leftViewMode = UITextFieldViewModeAlways;
    TextField.backgroundColor=[UIColor clearColor];
    if (TextField.tag==kTextfieldTagGreyBorder) {
        TextField.placeholder=placeholder;
        
        TextField.leftView = paddingView;
        TextField.layer.cornerRadius=0.0f;
        TextField.layer.masksToBounds=YES;
        TextField.layer.borderColor=[[UIColor colorWithRed:0.7137 green:0.7137 blue:0.7176 alpha:1.0]CGColor];
        TextField.layer.borderWidth= 0.7f;
        
    }
    else if (TextField.tag==kTextfieldTagLightGreyBorder){
        TextField.placeholder=placeholder;
        
        TextField.leftView = paddingView;
        TextField.layer.cornerRadius=0.0f;
        TextField.layer.masksToBounds=YES;
        TextField.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        TextField.layer.borderWidth= 1.0f;
        
    }
    else if (TextField.tag==kTextfieldTagWhiteBorder){
        TextField.placeholder=placeholder;
        if ([self isKindOfClass:[SearchBeerViewController class]]) {
            TextField.leftView = paddingView;
        }
        TextField.layer.cornerRadius=0.0f;
        TextField.layer.masksToBounds=YES;
        TextField.layer.borderColor=[[UIColor whiteColor]CGColor];
        TextField.layer.borderWidth= 1.0f;
        if ([TextField respondsToSelector:@selector(setAttributedPlaceholder:)]){
            UIColor *color = [UIColor whiteColor];
            TextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: color}];
        }
        else {
            NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
            // TODO: Add fall-back code to set placeholder color.
        }
    }
    else if (TextField.tag==kTextfieldTagReview){
        UIColor *color = [self getColorForRedText];
        TextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: color}];
        TextField.leftView = paddingView;
        TextField.textColor=[self getColorForRedText];
    }
    return  TextField;
}
/**************************** create textfield **************************/

-(UITextField*) createTxtfieldWithRect:(CGRect)rect text:(NSString*) text withLeftImage:(UIImage*)leftImage  withPlaceholder:(NSString*)placeholder withTag:(int)textfieldTag {
    UITextField  *TextField = [[UITextField alloc] initWithFrame: rect];
    TextField.tag=textfieldTag;
    [TextField setTextColor:[UIColor redColor]];
    if ([self isKindOfClass:[LoginViewController class]]||[self isKindOfClass:[SignUpViewController class]]||[self isKindOfClass:[ForgotPasswordViewController class]]) {
        [TextField setBackground:[UIImage imageNamed:kImageTextbox]];
        
    }
    CGFloat width  = 20;// (leftImage.size.width) ;
    CGFloat height = 20;// (leftImage.size.height);
    [TextField setLeftViewMode:UITextFieldViewModeAlways];
    
    self.placeholderImageView=[[UIImageView alloc] initWithImage:leftImage];
    [self.placeholderImageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.placeholderImageView setBackgroundColor:[UIColor clearColor]];
    self.customView = [[UIView alloc] initWithFrame:CGRectMake(0, (kTextFieldCustomViewHeight-kPaddingSmall)/2,30, kTextFieldCustomViewHeight)];
    self.placeholderImageView.frame=CGRectMake((30-width)/2, (kTextFieldCustomViewHeight-height)/2, width, height);
    if ([TextField respondsToSelector:@selector(setAttributedPlaceholder:)]){
        UIColor *color = [UIColor redColor];
        TextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: color}];
    }
    else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
    [self.customView addSubview:self.placeholderImageView];
    
    //  [self.customView addSubview:paddingV];
    
    self.customView.backgroundColor=[UIColor clearColor];
    [TextField setLeftView:self.customView];
    [TextField setTextAlignment:NSTextAlignmentLeft];
    TextField.text = text;
    TextField.textColor = [self getColorForBlackText];
    
    
    return  TextField;
}
/**************************** create imageview **************************/
-(AsyncImageView*) createAsynImageViewWithRect:(CGRect)rect imageUrl:(NSURL*) url isThumnail:(BOOL)isthumnail{
    AsyncImageView *ImageView = [[AsyncImageView alloc] initWithFrame: rect];
    [ImageView setBackgroundColor:[UIColor clearColor]];
    if (isthumnail) {
        [ImageView setTag:2003];
        [ImageView setContentMode:UIViewContentModeScaleAspectFill];
        
    }
    if ([self isKindOfClass:[LoyaltyCardViewController class]]) {
        [ImageView setTag:2004];
        [ImageView setContentMode:UIViewContentModeScaleAspectFit
         ];
        
    }
    else{
        [ImageView setContentMode:UIViewContentModeScaleAspectFill];
        
    }
    
    [ImageView setImageURL:url];
    
    return  ImageView;
}

-(UIImageView*) createImageViewWithRect:(CGRect)rect image:(UIImage*) image {
    UIImageView *ImageView = [[UIImageView alloc] initWithFrame: rect];
    [ImageView setBackgroundColor:[UIColor clearColor]];
    
    ImageView.image = image;
    return  ImageView;
}
/**************************** create bulletbutton**************************/

-(UIButton*)createSelectedUnselectedButtonViewWithFrame:(CGRect) frame withTitle:(NSString*)Title {
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    CGSize size = [button.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:17.0f]}];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -size.width)];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0,10, 0, button.imageView.image.size.width )];
    [button setTitle:Title forState:UIControlStateNormal];
    if(kStoreBottleCapps == 11)
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    else
      [button setTitleColor:[self getColorForBlackText] forState:UIControlStateNormal];
    
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    return button;
    
}
/**************************** create button**************************/

-(UIButton*)createButtonWithFrame:(CGRect)frame withTitle:(NSString *)Title {
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    [button setTitle:Title forState:UIControlStateNormal];
    button.backgroundColor=[UIColor clearColor];
    return button;
    
}

-(UIActionButton*)createActionButtonWithFrame:(CGRect)frame withTitle:(NSString *)Title {
    UIActionButton *button = [[UIActionButton alloc] initWithFrame:frame type:UIButtonTypeCustom];
    [button setTitle:Title forState:UIControlStateNormal];
    button.backgroundColor=[UIColor clearColor];
    return button;
}

-(UIWebView*)createAWebView:(CGRect)frame withURL:(NSString*)urlString{
    UIWebView *webview=[[UIWebView alloc]initWithFrame:frame];
    NSString *url=urlString;
    NSURL *nsurl=[NSURL URLWithString:url];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [webview loadRequest:nsrequest];
    [self.view addSubview:webview];
    return webview;
    
}
/**************************** create label**************************/
-(UILabel*) createLblWithRect:(CGRect) rect font:(UIFont*) font text:(NSString*)text textAlignment:(NSTextAlignment) textAlignment textColor:(UIColor*) textColor{
    UILabel *defaultLbl = [[UILabel alloc] initWithFrame: rect];
    defaultLbl.text = [NSString stringWithFormat:@"%@",text];
    [defaultLbl setFont:font];
    [defaultLbl setTextAlignment:textAlignment];
    defaultLbl.textColor = textColor;
    defaultLbl.backgroundColor = [UIColor clearColor];
    return defaultLbl;
}
/**************************** create label**************************/
/**************************** create custompagecontrol**************************/
-(PageControl*)createCustompagecontrol :(int)NumberOfpages currentPage:(int)currentpage withFrame:(CGRect)frame
{
    CGRect f = frame;
    PageControl *pageControl = [[PageControl alloc] initWithFrame:f];
    pageControl.numberOfPages = NumberOfpages;
    pageControl.currentPage = currentpage;
    pageControl.delegate = self;
    return pageControl;
}
/**************************** create custompagecontrol**************************/
#pragma mark-getColor
-(UIColor *) colorFromHexString:(NSString *)hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}
-(UIColor*)getColorForLightestGrey{
    UIColor *textColor=[UIColor colorWithRed:217.0/255.0f green:217.0/255.0f blue:217.0/255.0f alpha:1];
    return textColor;
}
-(UIColor*)getColorForYellowText{
    UIColor *textColor=[self colorFromHexString:kyellowHASHCODEstring];
    //[UIColor colorWithRed:119/255.0f green:119/255.0f blue:122/255.0f alpha:1];
    return textColor;
}

-(UIColor*)getColorForLightGreyColor{
    UIColor *textColor=[UIColor colorWithRed:237.0/255.0f green:237.0/255.0f blue:237.0/255.0f alpha:1];
    return textColor;
}

-(UIColor*)getColorForBlackText{
    UIColor *textColor=[self colorFromHexString:kgrayHASHCODEstring];
    //[UIColor colorWithRed:119/255.0f green:119/255.0f blue:122/255.0f alpha:1];
    return textColor;
}
-(UIColor*)getColorForOrderDetailCell{
    UIColor *textColor=[UIColor redColor];
    return textColor;
}
-(UIColor*)getColorForRedText
{
    UIColor *RedTextColor=[self colorFromHexString:kredHASHCODEstring];
    //[UIColor colorWithRed:235/255.0f green:0/255.0f blue:24/255.0f alpha:1];
    return RedTextColor;
}
-(UIColor*)getColorForCardNo
{
    UIColor *cardNoColor=[UIColor colorWithRed:83/255.0f green:83/255.0f blue:87/255.0f alpha:1];
    return cardNoColor;
}
//[UIColor colorWithRed:235/255.0f green:0/255.0f blue:24/255.0f alpha:1]
#pragma mark-BUTTON ACTIONS
-(void)bottomBack:(UIButton*)sender{
    DLog(@"this is called");
    if ([self isKindOfClass:[SignUpViewController class]]) {
        
        if (self.CustomScrollView.contentOffset.x==SCREEN_WIDTH) {
            self.CustomScrollView.contentOffset = CGPointMake(0, 0);
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
     
    }
    else if ( [self isKindOfClass:[UserProfileViewController class]]|| [self isKindOfClass:[LoyaltyCardViewController class]]|| [self isKindOfClass:[FavoriteListViewController class]]|| [self isKindOfClass:[ContactUsViewController class]] || [self isKindOfClass:[FavoriteBrowseViewController class]]) {
        UIViewController *vc = nil;
        vc= [self viewControllerWithIdentifier:kHomeViewControllerStoryboardId];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    else if ( [self isKindOfClass:[EditAccountViewController class]]) {
        __weak User *userObj = [[ModelManager modelManager] getuserModel];
        NSMutableDictionary* paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
        [paramDict setObject:userObj.userId forKey: kParamUserId];
        [self showSpinnerToSuperView:self.view]; // To show spinner
        [[ModelManager modelManager] ViewProfileWithParam:paramDict successStatus:^(BOOL status) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self hideSpinner];
                UserProfile *userProfileObj=kgetUserProfileModel;
                if ([userProfileObj.IsProfileUpdated isEqualToString:@"1"]) {
                    [self.navigationController popViewControllerAnimated:YES];
                    
                }
                else{
                    UIViewController *vc = nil;
                    vc= [self viewControllerWithIdentifier:kHomeViewControllerStoryboardId];
                    [self.navigationController pushViewController:vc animated:YES];
                }
                
            });
        } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
            [self hideSpinner];
            //[self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
            UIViewController *vc = nil;
            vc= [self viewControllerWithIdentifier:kHomeViewControllerStoryboardId];
            [self.navigationController pushViewController:vc animated:YES];
            
            
        }];
    }
    else if([self isKindOfClass:[ShoppingCartViewController class]])
    {
        if([signupBack intValue]==1)
        {
            signupBack = @"0";
            UIViewController *vc = nil;
            vc= [self viewControllerWithIdentifier:kHomeViewControllerStoryboardId];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
            [self.navigationController popViewControllerAnimated:YES];
    }
    else{
          [self.navigationController popViewControllerAnimated:YES];
        
    }
}
-(void)GoBack{
    
    if ([self isKindOfClass:[SignUpViewController class]]) {
        
        if (self.CustomScrollView.contentOffset.x==SCREEN_WIDTH) {
            self.CustomScrollView.contentOffset = CGPointMake(0, 0);
        }
        else{
            NSArray *object=self.navigationController.viewControllers;
            
            [self.navigationController popToViewController:[object objectAtIndex:1] animated:YES];
            
           // [self.navigationController popToRootViewControllerAnimated:YES];
           // [self  performSegueWithIdentifier:@"gotomainView" sender:self];
        }
        
        
    }
    else{
        NSArray *object=self.navigationController.viewControllers;
        
        [self.navigationController popToViewController:[object objectAtIndex:1] animated:YES];

      //  [self  performSegueWithIdentifier:@"gotomainView" sender:self];
    }
    
}

-(void)kartButton:(UIButton*)sender{
    __weak NSArray *shoppingbagarray=[[ShoppingCart sharedInstace] fetchProductFromShoppingCart];
    if (shoppingbagarray.count == 0) {
        [UIView showMessageWithTitle:nil message:@"No Item in Shopping cart!" showInterval:1.5 viewController:self];
    }else   {
        // kartAddedAndLoggedIn = @"1";
        UIViewController *vc=[self viewControllerWithIdentifier:kShoppingCartViewControllerStoryboardId];
        [self.navigationController pushViewController:vc animated:YES];
    }
    shoppingbagarray = nil;
}

-(void)addImagePickerOnClickOfButton:(UIButton*)button{
    CGRect frame1=CGRectMake(0, SCREEN_HEIGHT-180, SCREEN_WIDTH, SCREEN_HEIGHT);
    actionsheet=[[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take a Photo",@"Choose from Library", nil];
    actionsheet.frame = frame1;
    [actionsheet showFromRect:frame1 inView:self.view animated:YES];
}
#pragma mark-open camera and gallary
- (NSString*)base64forData:(NSData*)theData {
    
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

- (void) clickImage
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imgPickerCamera.delegate = self;
        imgPickerCamera.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imgPickerCamera animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Camera Available" message:@"Requires a camera to take pictures" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}
#pragma mark imagePickerController
-(void)postImageDataWithImage:(UIImage*)Image
{
    ImageData = UIImageJPEGRepresentation(Image, 1);
    
    if (ImageData != nil)
    {
        self.FilePath = @"PhotoAlbum";
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    User *userObj=kGetUserModel;
    if (userObj.userId) {
        [dic setObject:userObj.userId forKey:kParamAuthenticationID];
        
    }
    else {
        [dic setObject:@"0" forKey:kParamAuthenticationID];
        
    }
    [dic setObject:[ImageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength] forKey:@"UserImage"];
    
    
    [self CallApiUploadimageWithDic:dic];
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    if (isbarcode) {
        isbarcode=NO;
        // ADD: get the decode results
        id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
        ZBarSymbol *symbol = nil;
        for(symbol in results)
            // EXAMPLE: just grab the first barcode
            
            
            break;
        
        // EXAMPLE: do something useful with the barcode data
        
        self. barcodeID = symbol.data;
        DLog(@" self. barcodeID --%@", self. barcodeID );
        
        // EXAMPLE: do something useful with the barcode image
        //resultImage.image = [info objectForKey: UIImagePickerControllerOriginalImage];
        
        // ADD: dismiss the controller (NB dismiss from the *reader*!)
        
        if (![barcodeType isEqualToString:kBarcodeTypeSignUp]) {
            [self performSelector:@selector(findProductsAction:) withObject:symbol.data afterDelay:0.5];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else{
            [[NSUserDefaults standardUserDefaults]setValue: self.barcodeID  forKey:kBarcodeID];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"barcodeNotification"
             object:self];
            
            
        }
        
        return;
        
    }
    if(picker == imgPicker)
    {
        img = [info valueForKey:UIImagePickerControllerOriginalImage];
        [[imgPicker parentViewController] dismissViewControllerAnimated:YES completion:nil];
        
        TOCropViewController *cropController = [[TOCropViewController alloc] initWithImage:img];
        cropController.delegate = self;
        [self presentViewController:cropController animated:YES completion:nil];
        //.........................................................................................
    }
    
    if(picker == imgPickerCamera)
    {
        img = [info valueForKey:UIImagePickerControllerOriginalImage];
        [[imgPickerCamera parentViewController] dismissViewControllerAnimated:YES completion:nil];
        
       
    }
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithImage:img];
    cropController.delegate = self;
    [self presentViewController:cropController animated:YES completion:nil];
    
    // [self postImageData];
    
    
    
    
    
    
}
-(UIImage*)imageThumbnailCropping:(UIImage *)imageToCrop
{
    
    
    // begin an image context that will essentially "hold" our new image
    UIGraphicsBeginImageContext(CGSizeMake(100.0,100.0));
    // now redraw our image in a smaller rectangle.
    [imageToCrop drawInRect:CGRectMake(0.0, 0.0, 100.0, 100.0)];
    // make a "copy" of the image from the current context
    UIImage *newImage    = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    ImageData = UIImageJPEGRepresentation(newImage, 0.5);
  //  NSData *data = ImageData;
    
    while ([ImageData length]>=50000)
    {
        UIImage *image = [UIImage imageWithData:ImageData];
        ImageData = UIImageJPEGRepresentation(image, 0.5);
    }
    
    return newImage;
    
    
    
}

#pragma mark-actionsheet delegates
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self clickImage];
        return;
    }
    if (buttonIndex == 1)
    {
        //imgPicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        imgPicker.delegate = self;
        imgPicker.allowsEditing = YES;
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imgPicker animated:YES completion:nil];
        
        return;
    }
    
    if (buttonIndex == 2)
    {
        //[[timerToEnableButton userInfo] dismissWithClickedButtonIndex:0 animated:YES];
        return;
    }
}

#pragma mark UIPicker Delegate and Datasource

- (NSInteger)pickerView:(UIPickerView *)pv numberOfRowsInComponent:(NSInteger)component
{
    return 0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (void)pickerView:(UIPickerView *)pv didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
}
- (NSString *)pickerView:(UIPickerView *)pv titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return @"";
}
#pragma mark-CUSTOM UI
-(void)AddBottomView{
    CGRect BottomViewFrame ;
    BottomViewFrame.origin.x=0;
    BottomViewFrame.origin.y=SCREEN_HEIGHT-kBottomViewHeight;
    BottomViewFrame.size.width=SCREEN_WIDTH;
    BottomViewFrame.size.height=kBottomViewHeight;
    UIView *bottomView=[self createContentViewWithFrame:BottomViewFrame];
    bottomView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:kImagefooter]];
    [self.view addSubview:bottomView];
    //addbottom back
    UIImage *bottomBackImage=[UIImage imageNamed:kImage_bottombackbutton];
    
    _bottomBack=[self createButtonWithFrame:CGRectMake(SCREEN_WIDTH-bottomBackImage.size.width,0, bottomBackImage.size.width, kBottomViewHeight) withTitle:kEmptyString];
    
    //    if (![self isKindOfClass:[HomeViewController class]]&&[self isKindOfClass:[UserProfileViewController class]]&&[self isKindOfClass:[LoyaltyCardViewController class]]&&[self isKindOfClass:[ContactUsViewController class]]&&[self isKindOfClass:[FavoriteListViewController class]]) {
    //          }
    [_bottomBack addTarget:self action:@selector(bottomBack:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomBack setImage:[UIImage imageNamed:kImage_bottombackbutton] forState:UIControlStateNormal];
    [_bottomBack setBackgroundColor:[UIColor clearColor]];
    if (![self isKindOfClass:[HomeViewController class]] ) {
        [bottomView addSubview:_bottomBack];
        
    }
    
    //poweredbyWithlogo imageview
    UIImage *bottomlogoImage=[UIImage imageNamed:kImage_Bottomlogo];
    UIImageView *BottomlogoImageView=[self createImageViewWithRect:CGRectMake(_bottomBack.frame.origin.x-bottomlogoImage.size.width-kinBetweenGaps, (kBottomViewHeight-bottomlogoImage.size.height)/2,  bottomlogoImage.size.width, bottomlogoImage.size.height) image:bottomlogoImage];
    [bottomView addSubview:BottomlogoImageView ];
    UILabel *PoweredByLabel=[self createLblWithRect:CGRectMake(kSpaceinBottombackAndLogo, 0, SCREEN_WIDTH-kSpaceinBottombackAndLogo, kBottomViewHeight) font:nil text:kLabelPoweredBy textAlignment:NSTextAlignmentLeft textColor:[UIColor whiteColor]];
    [PoweredByLabel setFont:[UIFont boldSystemFontOfSize:11]];
    [bottomView addSubview:PoweredByLabel];
}
-(UIButton*)AddCustomButtonInsideContentViewWithFrame:(CGRect)frame{
    UIButton *customButton;
    _CustomButtonContainerView=[self createContentViewWithFrame:frame];
    _CustomButtonContainerView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:_CustomButtonContainerView];    //addbottom back
    customButton=[self createButtonWithFrame:CGRectMake(((frame.size.width)-kcustomButtonwidth)/2,((_CustomButtonContainerView.frame.size.height)-kButtonHeight)/2,kcustomButtonwidth,kButtonHeight) withTitle:kEmptyString];
    [customButton setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
    [customButton.titleLabel setFont:[UIFont fontWithName:kFontBold size:kFontBoldSize]];
    [[customButton layer] setBorderWidth:1.0f];
    [[customButton layer] setBorderColor:[self getColorForRedText].CGColor];
    [_CustomButtonContainerView addSubview:customButton];
    return customButton;
    //poweredbyWithlogo imageview
    
}
-(NSString*) ProperValue:(NSString*)value   {
    if(!value || [value isEqual:[NSNull null]] ||
       ([value respondsToSelector:@selector(isEqualToString:)] && [value isEqualToString:kEmptyString]== YES))
        return kEmptyString;
    else
        return value;
}
-(void)starsSelectionChanged:(EDStarRating *)control rating:(float)rating
{
    NSString *ratingString = [NSString stringWithFormat:@"Rating: %.1f", rating];
    if( [control isEqual:starRating] )
        starRatingstring = ratingString;
    else
        starRatingstring= ratingString;
}
#pragma mark-BARCODE SCANNER
-(IBAction)findProductsAction:(id)sender
{
    //   NSString *price = [pricelbl.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *minPrice = @"";
    NSString *maxPrice = @"";
    
    //    if([price isEqualToString:@"All"] || [price isEqualToString:@""])
    //    {
    //        minPrice = @"";
    //        maxPrice = @"";
    //    }
    //    else if([price rangeOfString:@"-"].length > 0 ){
    //        NSArray *priceArr = [price componentsSeparatedByString:@"-"];
    //        minPrice = [[[priceArr objectAtIndex:0] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //        maxPrice = [[[priceArr objectAtIndex:1] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //    }else if([price rangeOfString:@"+"].length > 0){
    //        NSArray *priceArr = [price componentsSeparatedByString:@"+"];
    //        minPrice = [[[priceArr objectAtIndex:0] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //        maxPrice = @"";
    //    }
    
    SearchBeerViewController *controller = [self viewControllerWithIdentifier:kSearchBeerViewControllerStoryboardId];
    controller.searchProductName = barcodeID;
    controller.searchProductMinPrice = minPrice;
    controller.searchProductMaxPrice = maxPrice;
    controller.searchType = @"myproduct";
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}
/**************This is done to add barcode scanner functionality ******************/
-(void)addBarCodeScannerWithType:(NSString*)barcodeTypeString{
    isbarcode=YES;
    barcodeType=barcodeTypeString;
    // ADD: present a barcode reader that scans from the camera feed
    ZBarReaderViewController *reader = [[ZBarReaderViewController alloc] init];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    // present and release the controller
    [reader setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self presentViewController:reader animated:YES completion:nil];
    
}
/**************This is done to add barcode scanner functionality ******************/

- (NSString *)GetUUID{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString *)string;
}

- (void)operationDidFinishWitUrl:(NSString *)url
{
}


- (void)operationDidFailWithResponse:(NSHTTPURLResponse *)response {
    
}


- (void)operationDidFailWithError:(NSError *)error {
    
}
#pragma mark-Datepicker delegates
-(void)changeDateInLabel:(id)sender withTextfield:(UITextField*)textfield
{
    textfield=[UITextField new];
    textfield=self.dateOfBirthTextfield;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
   // NSLog(@"date is %@", self.datePicker.date);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en"];
    
    if ([self isKindOfClass:[SignUpViewController class]]) {
        [df setDateFormat:@"MM-dd-yyyy"];
        
    }
    else {
        
        [df setDateFormat:kClientDateFormat];
        
        
    }
    
    /* sender.text = [NSString stringWithFormat:@"%@",
     [df stringFromDate:self.datePicker.date]];
     */
    self. DateOFBirthString =  [NSString stringWithFormat:@"%@",
                                [df stringFromDate:self.datePicker.date]];
    textfield.text=self. DateOFBirthString;
    
    if ([self.datePicker.date isEqual:[NSNull null]]) {
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"SelectedDate"];
    }
    else{
        [[NSUserDefaults standardUserDefaults]setObject:self.datePicker.date forKey:@"SelectedDate"];
    }
    NSDate*selectedDate;
    selectedDate = self.datePicker.date;
    
}
-(void)adddefaultDatePicker{
    self.dropDownView = [[UIView alloc]initWithFrame:CGRectMake(0,SCREEN_HEIGHT/2 , SCREEN_WIDTH, SCREEN_HEIGHT/2)];
    self.dropDownView.backgroundColor = [UIColor whiteColor];
    
    if([self.dropDownView isDescendantOfView:[self view]])
    {
        [self.dropDownView removeFromSuperview];
    }
    [self.view addSubview:self.dropDownView];
    
    self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,kDoneButtonHeight-2, SCREEN_WIDTH, (SCREEN_HEIGHT/2)-kDoneButtonHeight)];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.maximumDate = [NSDate date];
    self.datePicker.backgroundColor = [UIColor whiteColor];
    [self.datePicker addTarget:self action:@selector(changeDateInLabel:withTextfield:) forControlEvents:UIControlEventValueChanged];
    [self.dropDownView addSubview:self.datePicker];


}
- (void)removeTransparentV:(UITapGestureRecognizer*)sender {
    //UIView *view = sender.view;
    //NSLog(@"%ld", (long)view.tag);//By tag, you can find out where you had tapped.
    [transparentV removeFromSuperview];
}


-(void)addTransparentV{
    transparentV=[self createContentViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    UITapGestureRecognizer *closeFilterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeTransparentV:)];
    [transparentV setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:transparentV];
    [transparentV addGestureRecognizer:closeFilterTapRecognizer];
    UIImageView *transparentImageView=[self createImageViewWithRect:CGRectMake(0, 0, SCREEN_WIDTH
                                                                               , SCREEN_HEIGHT) image:nil];
    [transparentV addSubview:transparentImageView];
    [transparentImageView setBackgroundColor:[UIColor colorWithRed:0.05 green:0.05 blue:0.05 alpha:0.5]];

    }
-(void)addDatePickerToView  {
    self.dropDownView = [[UIView alloc]initWithFrame:CGRectMake(0,SCREEN_HEIGHT/2 , SCREEN_WIDTH, SCREEN_HEIGHT/2)];
    self.dropDownView.backgroundColor = [UIColor whiteColor];
    
    if([self.dropDownView isDescendantOfView:[self view]])
    {
        [self.dropDownView removeFromSuperview];
    }
    [self.view addSubview:self.dropDownView];
    
    self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,kDoneButtonHeight-2, SCREEN_WIDTH, (SCREEN_HEIGHT/2)-kDoneButtonHeight)];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.maximumDate = [NSDate date];
    self.datePicker.backgroundColor = [UIColor whiteColor];
    [self.datePicker addTarget:self action:@selector(changeDateInLabel:withTextfield:) forControlEvents:UIControlEventValueChanged];
    [self.dropDownView addSubview:self.datePicker];
    NSDate *today = [NSDate date]; // get the current date
    NSCalendar* cal = [NSCalendar currentCalendar]; // get current calender
    NSDateComponents* dateOnlyToday = [cal components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:today];
    
    long c = [dateOnlyToday year] - 21;
    long date=[dateOnlyToday day];
    long month=[dateOnlyToday month];
    
    
    NSString *dateStringLocal = [NSString stringWithFormat:@"%ld-%ld-%ld-00-00-00",c,month,date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd-HH-mm-ss"];
    
    
    NSDate *twentyYearOldDate = [dateFormatter dateFromString: dateStringLocal];
    
    [dateFormatter setDateFormat:@"yy/MM/dd"];
    
    NSDate *dt=[ dateFormatter dateFromString: @"15/03/04"];
    NSDate *dt1=[ dateFormatter dateFromString: @"13/12/31"];
    
    NSDateComponents *components;
    NSInteger days;
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay
                                                 fromDate: dt toDate: dt1 options: 0];
    days = [components day];
    
   // NSLog(@"%ld", (long)[components month]);
   // NSLog(@"%ld", (long)[components day]);
   // float mili=days*86400000;
    
  /*  NSTimeInterval distanceBetweenDates = [dt timeIntervalSinceDate:dt1];
    
    float daysleftinmonth = ceil(distanceBetweenDates/(1000 * 60 * 60 * 24));
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour; */
    
    if (![self.dateOfBirthTextfield.text isEqualToString:@""])
    {
        
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"SelectedDate"]isEqual:[NSNull null]]) {
            DLog(@"if");
            self.datePicker.date=twentyYearOldDate;
        }
        else{
            if ([[NSUserDefaults standardUserDefaults]objectForKey:@"SelectedDate"]!=nil) {
                self.datePicker.date = [[NSUserDefaults standardUserDefaults]objectForKey:@"SelectedDate"];
            }
            
        }
        
        
    }
    else
    {
        self.datePicker.date = twentyYearOldDate;
        
    }
    self.datePicker.maximumDate = twentyYearOldDate;
    
    
    
    
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    if ([self isKindOfClass:[SignUpViewController class]]) {
        [df setDateFormat:@"MM-dd-yyyy"];
        
    }
    else {
        
        [df setDateFormat:kClientDateFormat];
        
        
    }
    
    
    //        NSString*datestring=  [self dateStringFromString:self.dateOfBirthTextfield.text sourceFormat:@"MMM dd,yyyy" destinationFormat:@"dd-MM-yyyy"];
    
    
    
    
    
    self.dateOfBirthTextfield.text = [NSString stringWithFormat:@"%@",
                                      [df stringFromDate:self.datePicker.date]];
    
    UISegmentedControl *DoneButtonDatePicker = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    DoneButtonDatePicker.momentary = YES;
    DoneButtonDatePicker.frame = CGRectMake(SCREEN_WIDTH-kDoneButtonWidth-5,2,kDoneButtonWidth, kDoneButtonHeight);
    DoneButtonDatePicker.tintColor = [UIColor blackColor];
    [DoneButtonDatePicker addTarget:self action:@selector(DoneButtonDatePicker:) forControlEvents:UIControlEventValueChanged];
    [self.dropDownView addSubview:DoneButtonDatePicker];
}
-(void)addDatePickerToSignUpScreen  {
    [self addTransparentV];
    
    if (IS_OS_9_OR_Prior) {
        self.dropDownView = [[UIView alloc]initWithFrame:CGRectMake(0,(SCREEN_HEIGHT-dropdownViewHeight)/2 , SCREEN_WIDTH,  (HALF_SCREEN_HEIGHT+50))];

    }
    
    else{
        self.dropDownView = [[UIView alloc]initWithFrame:CGRectMake(20,(SCREEN_HEIGHT-dropdownViewHeight)/2 , dropdownViewWidth,  dropdownViewHeight)];

    }

    //self.dropDownView = [[UIView alloc]initWithFrame:CGRectMake(0,SCREEN_HEIGHT/2 , SCREEN_WIDTH, SCREEN_HEIGHT/2)];
    self.dropDownView.backgroundColor = [UIColor whiteColor];
    
    if([self.dropDownView isDescendantOfView:[self view]])
    {
        [self.dropDownView removeFromSuperview];
    }
    [transparentV addSubview:self.dropDownView];
    UILabel *milkORAlcoholLabel;
    if (IS_OS_9_OR_Prior) {
       milkORAlcoholLabel=[self createLblWithRect:CGRectMake(0,kPaddingSmall, SCREEN_WIDTH,  kLabelHeight) font:[UIFont fontWithName:kFontBold size:kFontBoldSize+5] text:@"Milk or Alcohol?" textAlignment:NSTextAlignmentCenter textColor:[self getColorForRedText]];

    }
    else{
       milkORAlcoholLabel=[self createLblWithRect:CGRectMake(0,kPaddingSmall, dropdownViewWidth,  kLabelHeight) font:[UIFont fontWithName:kFontBold size:kFontBoldSize+5] text:@"Milk or Alcohol?" textAlignment:NSTextAlignmentCenter textColor:[self getColorForRedText]];

    }
    [self.dropDownView addSubview:milkORAlcoholLabel];
    
    if (IS_OS_9_OR_Prior) {
        self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,milkORAlcoholLabel.frame.size.height+milkORAlcoholLabel.frame.origin.y, (self.dropDownView.frame.size.width), dropdownViewHeight-(kDoneButtonHeight+(kLabelHeight*4)))];

    }
    else{
        self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,milkORAlcoholLabel.frame.size.height+milkORAlcoholLabel.frame.origin.y, (self.dropDownView.frame.size.width), dropdownViewHeight-(kDoneButtonHeight+(kLabelHeight*4)))];

    
    }
    
    
    
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    
        self.datePicker.maximumDate = [NSDate date];
    self.datePicker.backgroundColor = [UIColor clearColor];
    [self.datePicker addTarget:self action:@selector(changeDateInLabel:withTextfield:) forControlEvents:UIControlEventValueChanged];
    [self.dropDownView addSubview:self.datePicker];
    NSDate *today = [NSDate date]; // get the current date
    NSCalendar* cal = [NSCalendar currentCalendar]; // get current calender
    NSDateComponents* dateOnlyToday = [cal components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:today];
    
    NSUInteger c = [dateOnlyToday year] - 21;
    NSUInteger date=[dateOnlyToday day];
    NSUInteger month=[dateOnlyToday month];
    
    NSString *ldateString = [NSString stringWithFormat:@"%lu-%lu-%lu-00-00-00",(unsigned long)c,(unsigned long)month,(unsigned long)date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd-HH-mm-ss"];
    NSDate *twentyYearOldDate = [ dateFormatter dateFromString: ldateString];
    [dateFormatter setDateFormat:@"yy/MM/dd"];
    NSDate *dt=[ dateFormatter dateFromString: @"15/03/04"];
    NSDate *dt1=[ dateFormatter dateFromString: @"13/12/31"];
    
    NSDateComponents *components;
    NSInteger days;
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay
                                                 fromDate: dt toDate: dt1 options: 0];
    days = [components day];
    
  //  NSLog(@"%ld", (long)[components month]);
  //  NSLog(@"%ld", (long)[components day]);
    
  //  NSTimeInterval distanceBetweenDates = [dt timeIntervalSinceDate:dt1];
    
  //  double secondsInAnHour = 3600;
    
    
            
        
        if ([self.datePicker.date isEqual:[NSNull null]]) {
            DLog(@"if");
            self.datePicker.date=twentyYearOldDate;
        }
        self.datePicker.date = twentyYearOldDate;
        
 
    self.datePicker.maximumDate = twentyYearOldDate;
    
    
    
    
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    if ([self isKindOfClass:[LoginViewController class]]) {
        [df setDateFormat:@"MM-dd-yyyy"];
        
    }
    else {
        
        [df setDateFormat:kClientDateFormat];
        
    }
    
    self.dateOfBirthTextfield.text = [NSString stringWithFormat:@"%@",
                                      [df stringFromDate:self.datePicker.date]];
    
      UIButton *NextButton=[self createButtonWithFrame:CGRectMake(30,self.datePicker.frame.size.height+self.datePicker.frame.origin.y, (self.dropDownView.frame.size.width)-60, kDoneButtonHeight) withTitle:kEmptyString];
    /**************adding button for next ********************/
    [NextButton setTitle:kButtonTitleNext forState:UIControlStateNormal];
    [NextButton addTarget:self action:@selector(nextButtonOnSignUp:) forControlEvents:UIControlEventTouchUpInside];
    [NextButton setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
    [NextButton.titleLabel setFont:[UIFont fontWithName:kFontBold size:kFontBoldSize]];
    [[NextButton layer] setBorderWidth:1.0f];
    [[NextButton layer] setBorderColor:[self getColorForRedText].CGColor];
    [self.dropDownView addSubview:NextButton];
    //(20,(SCREEN_HEIGHT-HALF_SCREEN_HEIGHT)/2 , SCREEN_WIDTH-(40),  HALF_SCREEN_HEIGHT)
    UILabel *tempLabel;
    if (IS_OS_9_OR_Prior) {
       tempLabel=[self createLblWithRect:CGRectMake(0,NextButton.frame.size.height+NextButton.frame.origin.y+10 , SCREEN_WIDTH,  kLabelHeight*2) font:[UIFont fontWithName:kFontSemiBold size:kFontRegularSize] text:@"By selecting Next you certify that you are above 21 years of age" textAlignment:NSTextAlignmentCenter textColor:[self getColorForRedText]];

    }
    else{
       tempLabel=[self createLblWithRect:CGRectMake(0,NextButton.frame.size.height+NextButton.frame.origin.y+10 , dropdownViewWidth,  kLabelHeight*2) font:[UIFont fontWithName:kFontSemiBold size:kFontRegularSize] text:@"By selecting Next you certify that you are above 21 years of age" textAlignment:NSTextAlignmentCenter textColor:[self getColorForRedText]];

    }
    tempLabel.numberOfLines=2;
    [self.dropDownView addSubview:tempLabel];

    /**************adding button for next ********************/
}
-(NSInteger)calculateAgeFromSelectedDate:(NSDate*)date{
    NSDate* birthday = date;
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:birthday
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];
    return age;
}

-(void)DoneButtonDatePicker:(UIButton*)button   {
    UIButton * dateBtn = (UIButton *) [self.view viewWithTag:kTagDatePickerShow];
    if(dateBtn) {
        dateBtn.tag = kTagDatePickerHidden;
    }
    
    [self hideDatePickerview];
}


-(void)nextButtonOnSignUp:(UIButton*)button   {
    
    [transparentV removeFromSuperview];
    if (self.datePicker.date) {
        NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
        [dateformatter setDateFormat:kClientDateFormat];
        if ([self calculateAgeFromSelectedDate:self.datePicker.date]>=21) {
            if (![self isKindOfClass:[EditAccountViewController class]]) {

                SignUpViewController *signupVC=[self viewControllerWithIdentifier:kSignUpViewControllerStoryboardId];
                signupVC.selectedDOBString=[self getDOBStringFromDate:self.datePicker.date];
                [self.navigationController pushViewController:signupVC animated:YES];

            }
            else{
                [_delegate GetDobEditedMethod:[self getDOBStringFromDate:self.datePicker.date]];
            }
            
                   }
        else{
            [self showAlertViewWithTitle:kAlertInvalidDOB message:nil];
        
        }
       
    }
    
  
}

-(void) hideDatePickerview  {
    // self.dateOfBirthTextfield.text=self.DateOFBirthString;
    if([self.dropDownView isDescendantOfView:[self view]])
    {
        [self.dropDownView setHidden:YES];
    }
}


-(NSDate*)SetDefaultDateForDatePicker{
    NSDate *today = [NSDate date]; // get the current date
    NSCalendar* cal = [NSCalendar currentCalendar]; // get current calender
    NSDateComponents* dateOnlyToday = [cal components:( NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay ) fromDate:today];
    [dateOnlyToday setYear:([dateOnlyToday year] -21)];
    NSDate *DefaultDate = [cal dateFromComponents:dateOnlyToday];
    
    return DefaultDate;
}

#pragma mark-addsepertatorForTableCell
-(void)getSeperatorVForTableCell:(UITableViewCell*)cell{
    UIImageView *separatorIView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 1)];
    [separatorIView setBackgroundColor:[self getColorForLightGreyColor]];
   // [separatorIView setBackgroundColor:[UIColor blackColor]];
    [cell.contentView addSubview:separatorIView];
    
}
#pragma mark-ADD VALIDATIONS phone

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:
(NSString *)string {
    DLog(@"");
    //restrict entry of empty space in beginning
    if (  range.location==0 && [string isEqualToString: @" "] ) {
        return NO;
    }
    
    if(textField.tag == kTextfieldTagSigninPassword || textField.tag == kTextfieldTagSignupPassword)    {
     if([string isEqualToString: @" "])
         return NO;
    }
    
    // allow backspace
    if ([textField.text stringByReplacingCharactersInRange:range withString:string].length < textField.text.length) {
        return YES;
    }
    
    if (textField==self.firstNameTextfield||textField==self.lastNameTextfield) {
        NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:ALPHA];
        
        if ([string rangeOfCharacterFromSet:set].location == NSNotFound) {
            return NO;
        }
        
    }
    if (textField==self.loyaltyCardTextfield) {
        NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:NUMERIC];
        
        if ([string rangeOfCharacterFromSet:set].location == NSNotFound) {
            return NO;
        }
        
    }
    if (textField==self.phoneNumberTextfield) {
        NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:NUMERIC];
        
        if ([string rangeOfCharacterFromSet:set].location == NSNotFound) {
            return NO;
        }
        
        if(!(range.location >= PHONELIMIT))
        {
            // Something entered by user
            if(range.location == 0)
            {
                [self.phoneNumberTextfield setText:@"("];
            }
            else if (range.location==4)
            {
                [self.phoneNumberTextfield setText:[NSString stringWithFormat:@"%@) ",textField.text]];
                
            }
            
            
            else if (range.location==9)
            {
                [self.phoneNumberTextfield setText:[NSString stringWithFormat:@"%@-",textField.text]];
                
                
            }
            else if (range.location==16)
            {
                [self.phoneNumberTextfield setText:[NSString stringWithFormat:@"%@)",textField.text]];
                
            }
            return YES;
            
        }
        else{
            return NO;
        }
    }
    if (textField==self.CardNumberTextFd )
    {
        
        //        if (textField==self.phoneNumberTextfield) {
        //            NSInteger length = [[self getOriginalPhonestringFromFormated:self.324.text] length];
        //            //NSLog(@"Length  =  %d ",length);
        //
        //            if(length >9)
        //            {
        //                return NO;
        //            }
        //
        //
        //        }
        //
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS_number] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    
    
    return YES;
}
- (UIImage *)centerCropImage:(UIImage *)image
{
    // Use smallest side length as crop square length
    CGFloat squareLength = MIN(image.size.width, image.size.height);
    // Center the crop area
    CGRect clippedRect = CGRectMake((image.size.width - squareLength) / 2, (image.size.height - squareLength) / 2, squareLength, squareLength);
    
    // Crop logic
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], clippedRect);
    UIImage * croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return croppedImage;
}
#pragma mark - Cropper Delegate -
- (void)layoutImageView
{
    if (self.imageView.image == nil)
        return;
    
    CGFloat padding = 20.0f;
    
    CGRect viewFrame = self.view.frame;
    viewFrame.size.width -= (padding * 2.0f);
    viewFrame.size.height -= ((padding * 2.0f));
    
    CGRect imageFrame = CGRectZero;
    imageFrame.size = self.imageView.image.size;
    
    CGFloat scale = MIN(viewFrame.size.width / imageFrame.size.width, viewFrame.size.height / imageFrame.size.height);
    imageFrame.size.width *= scale;
    imageFrame.size.height *= scale;
    imageFrame.origin.x = (CGRectGetWidth(self.view.bounds) - imageFrame.size.width) * 0.5f;
    imageFrame.origin.y = (CGRectGetHeight(self.view.bounds) - imageFrame.size.height) * 0.5f;
    self.imageView.frame = imageFrame;
    
}

#pragma mark - Gesture Recognizer -
- (void)didTapImageView
{
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithImage:self.img];
    cropController.delegate = self;
    [self presentViewController:cropController animated:YES completion:nil];
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle{
    self.imageView.image = image;
    self.imageView = [[UIImageView alloc] init];
    self.imageView.userInteractionEnabled = YES;
    [self.view addSubview:self.imageView];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapImageView)];
    [self.imageView addGestureRecognizer:tapRecognizer];
    
    
    [self layoutImageView];
    
    self.navigationItem.rightBarButtonItem.enabled = YES;
    
    CGRect viewFrame = [self.view convertRect:self.imageView.frame toView:self.navigationController.view];
    self.imageView.hidden = YES;
    [cropViewController dismissAnimatedFromParentViewController:self withCroppedImage:image toFrame:viewFrame completion:^{
        self.imageView.hidden = NO;
    }];
    
    [self postImageDataWithImage:[self imageThumbnailCropping:image]];
    
}
//- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
//    
//    if(locations.count>0){
//        location = [locations lastObject];
//      }
//}
-(void)ShowLocationWithLat:(NSString*)latStr longitude:(NSString *)longStr {
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
    LattitudeString=latStr;
    longitudeString=longStr;
    location=locationManager.location;
   
    NSString* versionNum = [[UIDevice currentDevice] systemVersion];
    NSString *nativeMapScheme = @"maps.apple.com";
    if ([versionNum compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending){
        nativeMapScheme = @"maps.google.com";
    }
    NSString* url = [NSString stringWithFormat: @"http://%@/maps?saddr=%f,%f&daddr=%f,%f", nativeMapScheme,location.coordinate.latitude,location.coordinate.longitude,
                     [LattitudeString doubleValue], [longitudeString doubleValue]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];

}

#pragma mark-stringFormatting
-(NSString*)addDollarSignToString:(NSString*)givenString{
    NSString *newString=[NSString stringWithFormat:@"$%@",givenString];
    return newString;
    
}
-(NSAttributedString*)addDollarSignTocutOverString:(NSString*)givenString{
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:givenString];
    [attributeString addAttribute:NSStrikethroughStyleAttributeName
                            value:@2
                            range:NSMakeRange(0, [attributeString length])];
    
    
    return attributeString;
}

#pragma mark-GetDate and Time
-(NSString*)GetTimeInAmPmFormat:(NSString*)timeString{
    
    NSString *dats1 = timeString;
    NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
    [dateFormatter3 setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter3 setDateFormat:@"HH:mm:ss"];
    NSDate *date1 = [dateFormatter3 dateFromString:dats1];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
   // NSLog(@"Current Date: %@", [formatter stringFromDate:date1]);
    
    return [formatter stringFromDate:date1];
    
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
   // NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:{
            [locationManager startUpdatingLocation];
        }
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations    {
     location = [locations lastObject];
    [locationManager stopUpdatingLocation];
    
    NSString* versionNum = [[UIDevice currentDevice] systemVersion];
    NSString *nativeMapScheme = @"maps.apple.com";
    if ([versionNum compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending){
        nativeMapScheme = @"maps.google.com";
    }
    NSString* url = [NSString stringWithFormat: @"http://%@/maps?saddr=%f,%f&daddr=%f,%f", nativeMapScheme,location.coordinate.latitude, location.coordinate.longitude,
                     [LattitudeString doubleValue], [longitudeString doubleValue]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}
-(CGRect)setDynamicViewsFrameFor:(NSString*)stringOfDynamicSize{
    CGSize constrainedSize = CGSizeMake(SCREEN_WIDTH-keditReviewImageHeight ,9999);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:@"OpenSans" size:10.0f], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:stringOfDynamicSize attributes:attributesDictionary];
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    return requiredHeight;
}
-(NSString*)getOriginalPhonestringFromFormated:(NSString*)mobileNumber{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    DLog(@"mobileNumber--%@",mobileNumber);
    return mobileNumber;
}
-(NSString*)getformattedPhoneNumberstringFrom:(NSString*)phoneString{
    //userProfileObj.ContactNo
    NSString* phonenumberstring= [self formatPhoneNumber:phoneString deleteLastChar:NO];
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < [phonenumberstring length]; i++) {
        NSString *ch = [phonenumberstring substringWithRange:NSMakeRange(i, 1)];
        if(i==0){
            [array addObject:@"("];
        }
        else if(i==3){
            [array addObject:@")"];
            [array addObject:@" "];
        }
        
        else if(i==6){
            [array addObject:@"-"];
        }
        
        [array addObject:ch];
        
    }
    phonenumberstring = @"";
    for(NSString *strNumber in array){
        phonenumberstring = [NSString stringWithFormat:@"%@%@",phonenumberstring,strNumber];
    }
    
    
    return phonenumberstring;
    
    
}

-(NSString*)getDOBStringFromDate:(NSDate*)date{
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"MM-dd-yyyy"];
    NSString *ldateString=  [dateformatter stringFromDate:date];
    return ldateString;
}
@end