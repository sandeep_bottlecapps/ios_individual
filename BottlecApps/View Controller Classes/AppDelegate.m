//
//  AppDelegate.m
//  BottlecApps
//
//  Created by imac9 on 8/3/15.
//  Copyright (c) 2015 Kuldeep Tyagi. All rights reserved.
// This is first commit.

#import "AppDelegate.h"
#import "SlideNavigationController.h"
#import "SideMenuViewController.h"
#import "APIManager.h"
#import "Constants.h"
@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize IsDeliveryEnabled_Cart,DeliveryLimit_Cart,DeliveryCharges_Cart,deliveryTaxRate;
@synthesize CurrentLocationData,MyOrderStatus,selectCatVal,chooselatitude,chooselongitude,LastModifedStoreId, uuidString, deviceToken;
@synthesize globalFavoriteOfflineArray;
+ (void)Generalstyle {
    //navigationbar
    UINavigationBar *navigationBar = [UINavigationBar appearance];
        [navigationBar setBackgroundImage:[UIImage imageNamed:kImageHeader] forBarMetrics:UIBarMetricsDefault];
 /*   [[UIApplication sharedApplication] setStatusBarHidden:YES
                                            withAnimation:UIStatusBarAnimationFade];*/
    
}
-(void)setAppSettingWithDict:(NSDictionary*)appSettingDict{
    self.DeliveryCharges_Cart=[appSettingDict valueForKey:kParamDeliverCharge];
    self.IsDeliveryEnabled_Cart=[appSettingDict valueForKey:kParamisDeliveryEnabled];
    self.DeliveryLimit_Cart=[appSettingDict valueForKey:kParamDeliveryLimit];
    
    self.deliveryTaxRate = [appSettingDict valueForKey:kParamDeliveryTaxRate];
    self.currentTimeStore = [appSettingDict valueForKey:kParamStoreCurrentTime];
    privacyPolicyText = [appSettingDict valueForKey:kParamPrivacyPolicyText];
    //self.deliveryTaxRate = @"0.06";
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    /**********************************setup the Navigation controller****************************************/
    [self setUpBaseNavigationController];
    [[self class] Generalstyle];
    /**********************************setup the Navigation controller****************************************/
    // To initialize the Network object mathod
    
    [[APIManager apiManager] initializeNetworkObject];
    [ModelManager initializeModelManager];
    
    [NSThread sleepForTimeInterval:3.0];
    
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    // if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) Commented By Sandeep below code
   /* if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
     {
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    } */

    globalFavoriteOfflineArray = [[NSMutableArray alloc]init];
    return YES;
}
- (void)registerForRemoteNotification

{
    
    UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeBadge | UIUserNotificationTypeAlert;
    
    UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    
   /* if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        
    {
        
        UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeBadge | UIUserNotificationTypeAlert;
        
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
        
    }
    else
        
    {
        
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    } */
    
}
#ifdef __IPHONE_8_0

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
    
}


#endif
- (void)requestAlwaysAuthorization
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied) {
        NSString *title;
        title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
        NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"Settings", nil];
        [alertView show];
    }
    // The user has not enabled any location services. Request background authorization.
    else if (status == kCLAuthorizationStatusNotDetermined) {
        [locationManager requestAlwaysAuthorization];
    }
}
//start code for get use's current location's latitude and longitude....
-(void)locationManager: (CLLocationManager *)manager
   didUpdateToLocation: (CLLocation *)newLocation
          fromLocation:(CLLocation *)oldLocation
{
    manager.delegate=nil;
    [manager stopUpdatingLocation];
    chooselatitude = newLocation.coordinate.latitude;
    chooselongitude = newLocation.coordinate.longitude;
    
    [locationManager stopUpdatingLocation];
    
    MKReverseGeocoder *geoCoder = [[MKReverseGeocoder alloc] initWithCoordinate:newLocation.coordinate];
    geoCoder.delegate = self;
    [geoCoder start];
    //[geoCoder release];
    
}
//End code for get use's current location's latitude and longitude....

//start code for get use's current location....

-(void) reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark
{
    geocoder.delegate=nil;
    [geocoder cancel];
    
    CurrentLocationData=[[NSMutableArray alloc]initWithCapacity:3];
    [CurrentLocationData insertObject:[NSString stringWithFormat: @"%f",chooselatitude] atIndex:0];
    [CurrentLocationData insertObject:[NSString stringWithFormat: @"%f",chooselongitude] atIndex:1];
}


-(void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken1
{
    self.deviceToken = [NSString stringWithFormat:@"%@",deviceToken1];
    self.deviceToken = [self.deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
    self.deviceToken = [self.deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
    self.deviceToken = [self.deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"Device Token is : %@", self.deviceToken);
    uuidString = self.deviceToken;
    
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:self.deviceToken delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
    
    [[NSUserDefaults standardUserDefaults]setValue:uuidString forKey:DEVICEID];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSLog(@"Device Token is : %@", uuidString);
    [[ModelManager modelManager] setCurrentDeviceToken:uuidString];
    
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    NSLog(@"Error in registration. Error: %@",err);
    UIAlertView *tokenAlert = [[UIAlertView alloc] initWithTitle:@"Device Token Registration Fail" message:[err localizedDescription] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
    [tokenAlert show];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
     UIApplicationState state = [application applicationState];
    
//    NSString *alertValue = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:alertValue delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//    [alert show];
    
    if (state == UIApplicationStateActive)
    {
        NSString *alertValue = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:alertValue delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    if (alertView.tag == 100 && buttonIndex == 1)
//    {
//        MenuViewController *menuObj = [[MenuViewController alloc]init];
//        LoginViewController *loginObject = [[LoginViewController alloc]init];
//        
//        for (UIViewController *controller in self.navigationController.viewControllers)
//        {
//            if(!user)
//            {
//                if ([loginObject isKindOfClass:[LoginViewController class]])
//                {
//                    [self.navigationController popToViewController:loginObject animated:YES];
//                    break;
//                }
//            }
//            else
//            {
//                if ([menuObj isKindOfClass:[MenuViewController class]])
//                {
//                    [self.navigationController popToViewController:menuObj animated:YES];
//                    break;
//                }
//            }
//            
//        }
//    }
}

//End code for get use's current location....


- (BOOL)prefersStatusBarHidden {
    return YES;
}
#pragma mark - Local Methods
//setup the Navigation controller and configure the view controllers heirarchy.

-(void)setUpBaseNavigationController    {
    SideMenuViewController *sideMenuViewController = [[SideMenuViewController alloc] init];
    [SlideNavigationController sharedInstance].rightMenu = nil;
    [SlideNavigationController sharedInstance].leftMenu = sideMenuViewController;
    [SlideNavigationController sharedInstance].menuRevealAnimationDuration = 0.25;  // set the time of animation
    [[SlideNavigationController sharedInstance] setPortraitSlideOffset: SCREEN_WIDTH/3];       //setup offset for sliding in potrait mode.
    sideMenuViewController = nil;
}
@end