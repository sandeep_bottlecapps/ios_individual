//
//  EventsListViewController.m
//  BOTTLECAPPS
//krowHeight
//  Created by imac9 on 9/7/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "EventsListViewController.h"
#import "EventsDetailViewController.h"
#import "AsyncImageView.h"

#define kTagProductImageView    5642
#define kTagEventsNameLabel     5643
#define kTagEventsDateLabel     5644
#define kTagEventsTimeLabel     5645
#define kTagEventsPriceLabel    5646

#define krowHeightForEvenList (kLabelHeight*6)+(kPaddingSmall*2)
#define kimageHeightWidth (krowHeightForEvenList)

@interface EventsListViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSURL               *productImageURL;
    NSArray             *neweventsArray;
    NSArray             *allEvents;
    int                 totalCount;
    int                 pagenumber;
}
@end

@implementation EventsListViewController
@synthesize eventsArray;

-(void)CallApiEventWithPagenumber:(int)page{
    NSMutableDictionary* paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [paramDict setObject:[NSString stringWithFormat:@"%d",page] forKey: kParamPageNumber];
    [paramDict setObject:kPageSize forKey: kParamPageSize];
    
    [self showSpinner]; // To show spinner
    [[ModelManager modelManager] getEventsWithEventsList:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
        //NSLog(@"%@", response);
        [self hideSpinner];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            totalCount=[[response valueForKey:@"totalNoOfRecords"] intValue];
            if (pagenumber>1) {
                neweventsArray =[response valueForKey:@"productCount"];
                allEvents=[self.eventsArray arrayByAddingObjectsFromArray:neweventsArray];
                self.eventsArray=allEvents;
                [self.defaultTableView reloadData ];
            }
            else{
                self.eventsArray =[response valueForKey:@"productCount"];
                DLog(@"ItemArray--%@",self.eventsArray);
                [self UpdateUI];
            }
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
    }];
}

-(void)UpdateUI {
    [self addScrollView];
    [self.CustomScrollView setContentSize:CGSizeMake(SCREEN_WIDTH, self.CustomScrollView.frame.size.height )];
    [self.CustomScrollView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Home_Img2.jpg"]]];
    [self AddDefaultTableView];
    [self.defaultTableView setDelegate:self];
    [self.defaultTableView setDataSource:self];
}

-(void)viewWillAppear:(BOOL)animated    {
    pagenumber=1;
    totalCount=0;
    [self CallApiEventWithPagenumber:pagenumber];
}

-(void)initializeArrayAndDict{
    neweventsArray =[NSArray new];
    self.eventsArray =[NSArray new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeArrayAndDict];
}

#pragma mark-TABLE VIEW DELEGATES
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of ections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    if (indexPath.row==[self.eventsArray count]) {
        return kButtonHeight;
    }
    else
        return 85;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([self.eventsArray count]<totalCount) {
        return [self.eventsArray count]+1;
    }
    else if (totalCount==0)
        return 1;
    
    else    {
        return [self.eventsArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    static NSString *postCellId = @"postCell";
    static NSString *moreCellId = @"moreCell";
    NSUInteger row = [indexPath row];
    NSUInteger count = [self.eventsArray count];
    UITableViewCell *cell;
      [self getSeperatorVForTableCell:cell];
    self.defaultTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    if (row == count) {
        cell = [tableView dequeueReusableCellWithIdentifier:moreCellId];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:moreCellId];
        }
        
        cell.textLabel.text = kTapToloadMore;
        cell.textLabel.textColor = [self getColorForRedText];
        cell.textLabel.font = [self addFontRegular];
    }
    else if (count==0) {
        cell = [tableView dequeueReusableCellWithIdentifier:moreCellId];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:moreCellId];
        }
        
        cell.textLabel.text = kNoEvents;
        cell.textLabel.textColor = [self getColorForRedText];
        cell.textLabel.font = [self addFontRegular];
        return cell;
    }
    else {
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:postCellId];
            
            AsyncImageView *ProductImageView =[self createAsynImageViewWithRect:CGRectMake(0, 1, 85, 84) imageUrl:nil isThumnail:YES];
            [ProductImageView setTag:kTagProductImageView];
            [ProductImageView setContentMode: UIViewContentModeScaleToFill];
            
            UILabel *EventsNameLabel = [self createLblWithRect:CGRectMake((ProductImageView.frame.size.width)+(ProductImageView.frame.origin.x)+kPaddingSmall, 7, kLabelWidth*2, 17) font:[UIFont fontWithName:kFontSemiBold size:(kFontSemiBoldSize - 2)] text:kEmptytextfield textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
            
            [EventsNameLabel setTag:kTagEventsNameLabel];
            
            UILabel *EventsDateLabel=[self createLblWithRect:CGRectMake((ProductImageView.frame.size.width)+(ProductImageView.frame.origin.x)+kPaddingSmall,(EventsNameLabel.frame.origin.y)+(EventsNameLabel.frame.size.height)+ 2, kLabelWidthLarge, 15)
                                                        font:[UIFont fontWithName:kFontRegular size:(kFontRegularSize - 1)]
                                                        text:kEmptytextfield textAlignment:NSTextAlignmentLeft
                                                   textColor:[UIColor colorWithRed:0.5451 green:0.5451 blue:0.5529 alpha:1.0]];
            [EventsDateLabel setTag:kTagEventsDateLabel];
            
            UILabel *EventsTimeLabel=[self createLblWithRect:CGRectMake((ProductImageView.frame.size.width)+(ProductImageView.frame.origin.x)+kPaddingSmall,(EventsDateLabel.frame.origin.y)+(EventsDateLabel.frame.size.height)+2, kLabelWidthLarge, 15)
                                                        font:[UIFont fontWithName:kFontRegular size:(kFontRegularSize - 1)]
                                                        text:kEmptytextfield textAlignment:NSTextAlignmentLeft
                                                   textColor:[UIColor colorWithRed:0.5451 green:0.5451 blue:0.5529 alpha:1.0]];
            [EventsTimeLabel setTag:kTagEventsTimeLabel];
            
            UILabel *EventsPriceLabel=[self createLblWithRect:CGRectMake((ProductImageView.frame.size.width)+(ProductImageView.frame.origin.x)+kPaddingSmall, (EventsTimeLabel.frame.size.height)+(EventsTimeLabel.frame.origin.y)+2, kLabelWidthLarge, 15)
                                                         font:[UIFont fontWithName:kFontRegular size:(kFontRegularSize - 1)]
                                                         text:kEmptytextfield textAlignment:NSTextAlignmentLeft
                                                    textColor:[UIColor colorWithRed:0.5451 green:0.5451 blue:0.5529 alpha:1.0]];
            [EventsPriceLabel setTag:kTagEventsPriceLabel];
            
            
            [cell addSubview:ProductImageView];
            [cell addSubview:EventsNameLabel];
            [cell addSubview:EventsDateLabel];
            [cell addSubview:EventsTimeLabel];
            [cell addSubview:EventsPriceLabel];
        }
        
        __weak AsyncImageView *ProductImageView   = (AsyncImageView *) [cell viewWithTag: kTagProductImageView];
        __weak UILabel        *EventsNameLabel    = (UILabel *) [cell viewWithTag: kTagEventsNameLabel];
        __weak UILabel        *EventsDateLabel    = (UILabel *) [cell viewWithTag: kTagEventsDateLabel];
        __weak UILabel        *EventsTimeLabel    = (UILabel *) [cell viewWithTag: kTagEventsTimeLabel];
        __weak UILabel        *EventsPriceLabel   = (UILabel *) [cell viewWithTag: kTagEventsPriceLabel];
        
        productImageURL = [NSURL URLWithString:[self ProperValue:
                                                [[self.eventsArray objectAtIndex:indexPath.row] valueForKey:@"EventImage"]]];
        [ProductImageView setImageURL:productImageURL];
        
        NSString *starttime=[self ProperValue:[[self.eventsArray objectAtIndex:indexPath.row] valueForKey:@"EventStartTime"]];
        NSString *endtime=[self ProperValue:[[self.eventsArray objectAtIndex:indexPath.row] valueForKey:@"EventEndTime"]];
        
        if (![[self ProperValue:[[self.eventsArray objectAtIndex:indexPath.row] valueForKey:@"EventName"]]isEqualToString:kEmptyString]) {
            EventsNameLabel.text=[self ProperValue:[[self.eventsArray objectAtIndex:indexPath.row] valueForKey:@"EventName"]];
            
        }
        if (![[[self.eventsArray objectAtIndex:indexPath.row] valueForKey:@"EventStartDt"]isEqualToString:kEmptyString]) {
            EventsDateLabel.text=[NSString stringWithFormat:@"%@ - %@",[self ProperValue:[self dateStringFromString:[[self.eventsArray objectAtIndex:indexPath.row] valueForKey:@"EventStartDt"] sourceFormat:@"MM/dd/yyyy" destinationFormat:kClientDateFormat]],[self ProperValue:[self dateStringFromString:[[self.eventsArray objectAtIndex:indexPath.row] valueForKey:@"EventEndDt"] sourceFormat:@"MM/dd/yyyy" destinationFormat:kClientDateFormat]]];
        }
        if (![starttime isEqualToString:kEmptyString]) {
            EventsTimeLabel.text=[NSString stringWithFormat:@"%@ - %@",[self GetTimeInAmPmFormat:starttime],[self GetTimeInAmPmFormat:endtime]];
        }
        if (![[self ProperValue:[[self.eventsArray objectAtIndex:indexPath.row] valueForKey:@"EventPrice"]]isEqualToString:kEmptyString]) {
            NSString *price = [[self.eventsArray objectAtIndex:indexPath.row] valueForKey:@"EventPrice"];
            if((!price) || (price && [price isEqualToString:kEmptytextfield]) || (price && [price floatValue] <= 0.00))  {
                price = @"Free";
                EventsPriceLabel.text = price;
            }
            else    {
                EventsPriceLabel.text=[NSString stringWithFormat:@"$%@",[self ProperValue:[[self.eventsArray objectAtIndex:indexPath.row] valueForKey:@"EventPrice"]]];
            }
        }
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];
    NSUInteger count = [self.eventsArray count];
    if (row == count) {
        pagenumber++;
        if (count<totalCount) {
            [self CallApiEventWithPagenumber:pagenumber];
            DLog(@"pagenumber--%d",pagenumber);
        }
    }
    else {
        EventsDetailViewController *vc=[self viewControllerWithIdentifier:kEventsDetailViewControllerStoryboardId];
        vc.eventsDict=[self.eventsArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
@end