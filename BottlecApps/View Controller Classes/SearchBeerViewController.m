//
//  SearchBeerViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 8/25/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "SearchBeerViewController.h"
#import "Search.h"
#import "ProductDetailsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AsyncImageView.h"
#import "MTBBarcodeScanner.h"
#import "Product.h"
#import "UIActionButton.h"
#import "TTTAttributedLabel.h"
#import "QALabel.h"
#import "AppDelegate.h"
#import "Constants.h"

#pragma mark VerticalAlign
@interface UILabel (VerticalAlign)
- (void)alignTop;
- (void)alignBottom;
@end

// -- file: UILabel+VerticalAlign.m


#define k_expandableTableViewTag 500
#define kWidthCellSearchBeer (cell.frame.size.width)
#define kHeightCellSearchBeer (cell.frame.size.height)
#define kcartButtonWidth     100
#define kcartButtonHeight      cell.frame.size.height
#define  krightlabelIndex      ((indexPath.row)*2)
#define kFilterHeight                                (IS_IPHONE_4_OR_LESS? 25:IS_IPHONE_6?30:30)
#define kFilterHeightWine                                (IS_IPHONE_4_OR_LESS? 25:IS_IPHONE_6?30:30)
#define  kLastIndexType                ([typeArray count]/2)
#define  kLastIndexSize                ([sizeArray count]/2)
#define  kLastIndexPrice               ([rangeArray count]/2)
#define  kLastIndexcountryArray             ([countryArray count]/2)
#define  kLastIndexregionNameArray     ([regionNameArray count]/2)
#define  kLastIndexSubTypeArray        ([subTypeArray count]/2)
#define kContainerviewHeight (([FilteriTemsArray count]+30)*kFilterHeight)
#define kContainerviewHeightWine (([FilteriTemsArray count]+8)*kFilterHeightWine)

#define krowHeightSearch (krowHeight-12)

//Declaring Height for specs and phillie Liquor
#if kStoreBottleCapps == 3 || kStoreBottleCapps == 4
 #define kNewRowHeight (IS_IPHONE_4_OR_LESS? krowHeightSearch:IS_IPHONE_6?krowHeightSearch-15:krowHeightSearch)
 #define koutOfstockImageYAndXAxis (IS_IPHONE_4_OR_LESS? 0:IS_IPHONE_6?5:IS_IPHONE_6)
#else
  #define kNewRowHeight krowHeightSearch
  #define koutOfstockImageYAndXAxis 0
#endif
#define kCartLikeViewHeight2                            krowHeightSearch-1.5

@interface SearchBeerViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    NSMutableDictionary *apiResponseDictionary;
    UITableView *tblView;
    NSURL               *productImageURL;
    UIView              *bgSearchbarV;
    NSMutableArray      *arrayForBool;
    NSMutableArray      *FilteriTemsArray;
    NSMutableArray      *typeArray;
    NSMutableArray      *subTypeArray;
    NSMutableArray      *sizeArray;
    NSMutableArray      *rangeArray;
    
    NSMutableArray      *regionNameArray;
    NSMutableArray      *regionIdArray;
    
    NSMutableArray *headerViewStore;
    
    NSString            *selectedSubTypeId;
    //Product *productObj;
    BOOL isTypeSelected;
    BOOL isSizeSelected;
    BOOL isPriceSelected;
    UIView *sectionView;
    UIActionButton *rightButton;
    UIActionButton *leftButton;
    UIActionButton *bottomButton;
    NSMutableArray *selectedProductArray;
    int SelectedSection;
    NSString *PriceRange;
    NSString *SUBTypeVal;
    BOOL isfilterClicked;
    UIButton   *dropDownButton;
    UIButton *FilterButton;
    UIView *PriceRangeView;
    UIImageView *ImgV;
    BOOL isClosed;
    UIImage *plusImage;
    UIImage *crossImage;
    
    int pagenumber;
    int                         totalCount;
    NSMutableArray              *ItemArray;
    
    NSUInteger          selectedIndex;
    NSMutableArray *regionResultArray ;
    NSMutableArray *countryResultArray;
    NSMutableArray *subCategoryResultArray;
    NSMutableArray *varietalArray;
   // NSMutableArray *cartLocalArray;
    
    UILabel *NorecordFoundLabel;
    BOOL isfirstTime;
    AppDelegate *appDelegate;

    
}
@property(nonatomic,retain) NSMutableArray *countryArray;
@property(nonatomic ,retain) NSMutableArray *countryIdArray;

@end

@implementation SearchBeerViewController
@synthesize searchItemString,searchType;
@synthesize countryArray,countryIdArray;
@synthesize typeArray,subTypeIDArray,subTypeArray,sizeArray,rangeArray,PickerState,MainArray,CategoryVal;
@synthesize searchProductMinPrice,searchProductMaxPrice,searchProductName;

#pragma mark-CallApi
/*******************This is called to search item based on barcode id *****************/
-(void)CallApiSearchproductWithBarcodeId:(NSString*)BarcodeId :(int)page{
    __weak User *userObj=kGetUserModel;
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setValue:BarcodeId forKey:kParamProductName];
    [dict setValue:userObj.userId forKey:kParamUserId];
    [dict setValue:kPageSize forKey:kParamPageSize];
    
    [dict setValue:kSTOREIDbottlecappsString forKey:kParamStoreId];
    [dict setValue:kEmptyString forKey:kParamMinValue];
    [dict setValue:kEmptyString forKey:kParamMaxvalue];
    [dict setValue:[NSString stringWithFormat:@"%d",page] forKey:kParamPageNumber];
    
    [self showSpinnerToSuperView:self.view]; // To show spinner
    [[ModelManager modelManager] SearchproductWithParam:dict  successStatus:^(APIResponseStatusCode statusCode, id response) {
       // NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            totalCount=[[response valueForKey:@"totalNoOfRecords"] intValue];
            [self hideSpinner];
            Product *productObj;
            DLog(@"productCount===%@",[response valueForKey:@"productCount"]);
            for (NSDictionary *dict in [response valueForKey:@"productCount"]) {
                productObj=[[Product alloc] initWithDictionary:dict];
                [ItemArray addObject:productObj];
            }
            if([ItemArray count] == 0){
                [self addNorecordFoundLAbelOnTableView:self.defaultTableView];
                self.defaultTableView .delegate=self;
                self.defaultTableView.dataSource=self;
                [self.defaultTableView reloadData];
            }
            
            else{
                [self removeNorecordfoundLabel ];
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kNavigationheight)];
                label.backgroundColor = [UIColor clearColor];
                label.numberOfLines = 2;
                label.font = [self addFontSemiBold];
                label.textAlignment = NSTextAlignmentCenter;
                label.textColor = [UIColor whiteColor];
                DLog(@"%@",self.searchItemString);
                label.text =kNavigationTitleFINDAPRODUCT  ;
                self.navigationItem.titleView = label;
                DLog(@"productStatus====%@",productObj.productStatus);
                for (Product *productObj in ItemArray) {
                    if ([[productObj productStatus] intValue]==0) {
                        [self.arrayForBoolProductStatus addObject:@"0"];
                    }
                    else{
                        [self.arrayForBoolProductStatus addObject:@"1"];
                    }
                }
                DLog(@"self.arrayForBoolProductStatus-----------%@",self.arrayForBoolProductStatus);
                // Do any additional setup after loading the view.
                if (isfirstTime==YES) {
                    [self refreshFilterScreen];
                    isfirstTime=NO;
                }
                self.defaultTableView .delegate=self;
                self.defaultTableView.dataSource=self;
                [self.defaultTableView reloadData];
            }
            
            
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}
/*******************This is called to search item based on barcode id *****************/


/*******************This is called to search wine based on Fiter popup *****************/

-(void)CallApiSearchWineWithSearchItem:(NSString*)searchitem :(NSMutableArray*)FilterItems :(int)page{
    DLog(@"FilterItems--%@",FilterItems);
    __weak User *userObj=kGetUserModel;
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setValue:[NSString stringWithFormat:@"%d",page] forKey:kParamPageNumber];
    [dict setValue:kSTOREIDbottlecappsString forKey:kParamStoreId];
    [dict setValue:userObj.userId forKey:kParamUserId];
    [dict setValue:kPageSize forKey:kParamPageSize];
    [dict setValue:searchitem forKey:kParamSearchText];
    
    NSString *minPrice = kEmptyString;
    NSString *maxPrice = kEmptyString;
    NSString *typeName=kEmptyString;
    NSString *size =kEmptyString;
    NSString *price=kEmptyString;
    NSString *SubCat=kEmptyString;
    NSString *country=kEmptyString;
    NSString *region=kEmptyString;
    NSString *regionID=kEmptyString;
    
    if ([FilterItems count]) {
        typeName = [[FilterItems objectAtIndex:0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        size=      [[FilterItems objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        price = PriceRange;
        country=      [[FilterItems objectAtIndex:2] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        DLog(@"countryResultArray%@",countryResultArray);
        for (NSDictionary *dict in countryResultArray) {
            NSString *Name=[dict valueForKey:@"CountryName"];
            if ([Name isEqualToString:country]) {
                NSString  *Cid=[dict valueForKey:@"CountryID"];
                country  =Cid;
                break;
            }
        }
        
        region=      [[FilterItems objectAtIndex:3] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        DLog(@"regionResultArray%@",regionResultArray);
        for (NSDictionary *dict in regionResultArray) {
            NSString *regionName=[dict valueForKey:@"RegionName"];
            if ([regionName isEqualToString:region]) {
                NSString  *regionid=[dict valueForKey:@"RegionId"];
                regionID  =regionid;
                break;
            }
        }
        regionID=kEmptyString;
        
        DLog(@"varietalArray==%@",varietalArray);
        
        if ([[varietalArray objectAtIndex:0]isKindOfClass:[NSArray class]]) {
            for (NSDictionary *dict in [varietalArray objectAtIndex:0]) {
                NSString *Name=[dict valueForKey:@"SubCatName2"];
                if ([Name isEqualToString:[FilterItems objectAtIndex:4]]) {
                    NSString  *Cid=[dict valueForKey:@"SubCatId2"];
                    SubCat  =Cid;
                    break;
                }
            }
            
        }
        else{
            for (NSDictionary *dict in varietalArray) {
                NSString *Name=[dict valueForKey:@"SubCatName2"];
                if ([Name isEqualToString:[FilterItems objectAtIndex:4]]) {
                    NSString  *Cid=[dict valueForKey:@"SubCatId2"];
                    SubCat  =Cid;
                    break;
                }
            }
        }
        
        if ([typeName isEqualToString:@"Type"]||[typeName isEqualToString:kStringSelectAll])
        {
            typeName = @"";
        }
        if ([size isEqualToString:@"Size"]||[size isEqualToString:kStringSelectAll])
        {
            size = @"";
        }
        if ([price isEqualToString:@"Price Range"])
        {
            price = @"";
        }
        if ([country isEqualToString:@"Country"]||[country isEqualToString:kStringSelectAll])
        {
            country = @"";
        }
        if ([region isEqualToString:@"Region"]||[region isEqualToString:kStringSelectAll])
        {
            region = @"";
        }
        if ([SubCat isEqualToString:@"Varietal"]||[SubCat isEqualToString:kStringSelectAll])
        {
            SubCat = @"";
        }
        
        if([price isEqualToString:kStringSelectAll] || [price isEqualToString:@""]){
            minPrice = @"";
            maxPrice = @"";
        }else if([price rangeOfString:@"-"].length > 0 ){
            NSArray *priceArr = [price componentsSeparatedByString:@"-"];
            minPrice = [[[priceArr objectAtIndex:0] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            maxPrice = [[[priceArr objectAtIndex:1] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        }else if([price rangeOfString:@"+"].length > 0){
            NSArray *priceArr = [price componentsSeparatedByString:@"+"];
            minPrice = [[[priceArr objectAtIndex:0] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            maxPrice = @"";
        }
        
        // if type is all
        
        if([typeName isEqualToString:kStringSelectAll])
        {
            typeName = @"";
        }
        // if size is all
        if([size isEqualToString:kStringSelectAll])
        {
            size = @"";
        }
    }
    
    [dict setValue:typeName forKey:kParamTypeName];
    [dict setValue:size forKey:kParamSize];
    [dict setValue:minPrice forKey:kParamPriceMin];
    [dict setValue:maxPrice forKey:kParamPriceMax];
    [dict setValue:size forKey:kParamSize];
    [dict setValue:SubCat forKey:kParamsubCatId2];
    [dict setValue:country forKey:kParamCountry];
    [dict setValue:region forKey:kParamRegion];
    [dict setValue:regionID forKey:kParamRegionId];
    
   // [self showSpinnerToSuperView:self.view]; // To show spinner
    [self showSpinner1];
    [[ModelManager modelManager] SearchWineWithParam:dict  successStatus:^(APIResponseStatusCode statusCode, id response) {
    //    NSLog(@"%@", response);
        [self hideSpinner];
        @try {
            for (NSDictionary *dict in [response valueForKey:@"productCount"]) {
                Product *productObj=[[Product alloc] initWithDictionary:dict];
                [ItemArray addObject:productObj];
            }
            totalCount=[[response valueForKey:@"totalNoOfRecords"] intValue];
            
            if([ItemArray count] == 0){
                [self addNorecordFoundLAbelOnTableView:self.defaultTableView];
                [self.defaultTableView reloadData];
            }
            
            else{
                [self removeNorecordfoundLabel ];
                [self UpdateUI];
            }
            
        }
        @catch (NSException *exception) {
            [self showAlertViewWithTitle:kError message:kInternalInconsistencyErrorMessage];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
                
            });
        }
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}

/*******************This is called to search wine based on Fiter popup *****************/

-(void)CallApiSearchLiquorWithSearchItem:(NSString*)searchitem :(NSMutableArray*)FilterItems :(int)page{
    __weak User *userObj=kGetUserModel;
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setValue:[NSString stringWithFormat:@"%d",page] forKey:kParamPageNumber];
    [dict setValue:kSTOREIDbottlecappsString forKey:kParamStoreId];
    [dict setValue:userObj.userId forKey:kParamUserId];
    [dict setValue:kPageSize forKey:kParamPageSize];
    [dict setValue:searchitem forKey:kParamSearchText];
    
    NSString *minPrice = @"";
    NSString *maxPrice = @"";
    NSString *typeName=@"";
    NSString *size =@"";
    NSString *price=@"";
    
    if ([FilterItems count]) {
        typeName = [[FilterItems objectAtIndex:0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        size=      [[FilterItems objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        price = PriceRange;
        
        if ([typeName isEqualToString:@"Type"])
        {
            typeName = @"";
        }
        if ([size isEqualToString:@"Size"])
        {
            size = @"";
        }
        if ([price isEqualToString:@"Price Range"])
        {
            price = @"";
        }
        
        if([price isEqualToString:kStringSelectAll] || [price isEqualToString:@""]){
            minPrice = @"";
            maxPrice = @"";
        }else if([price rangeOfString:@"-"].length > 0 ){
            NSArray *priceArr = [price componentsSeparatedByString:@"-"];
            minPrice = [[[priceArr objectAtIndex:0] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            maxPrice = [[[priceArr objectAtIndex:1] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        }else if([price rangeOfString:@"+"].length > 0){
            NSArray *priceArr = [price componentsSeparatedByString:@"+"];
            minPrice = [[[priceArr objectAtIndex:0] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            maxPrice = @"";
        }
        
        // if type is all
        
        if([typeName isEqualToString:kStringSelectAll])
        {
            typeName = @"";
        }
        // if size is all
        if([size isEqualToString:kStringSelectAll])
        {
            size = @"";
        }
    }
    [dict setValue:typeName forKey:kParamTypeName];
    [dict setValue:size forKey:kParamSize];
    [dict setValue:minPrice forKey:kParamPriceMin];
    [dict setValue:maxPrice forKey:kParamPriceMax];
    DLog(@"dict--%@",dict);
   // [self showSpinnerToSuperView:self.view]; // To show spinner
    [self showSpinner1];
    [[ModelManager modelManager] SearchLiquorWithParam:dict  successStatus:^(APIResponseStatusCode statusCode, id response) {
       // NSLog(@"%@", response);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
           // NSLog(@"%@", response);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self hideSpinner];
                @try {
                    for (NSDictionary *dict in [response valueForKey:@"productCount"]) {
                        Product *productObj=[[Product alloc] initWithDictionary:dict];
                        [ItemArray addObject:productObj];
                    }
                    totalCount=[[response valueForKey:@"totalNoOfRecords"] intValue];
                    
                    if([ItemArray count] == 0){
                        [self addNorecordFoundLAbelOnTableView:self.defaultTableView];
                        [self.defaultTableView reloadData];
                    }
                    
                    else{
                        for (UIView *subView in self.defaultTableView.subviews)
                        {
                            
                            if (subView.tag==kTagNorecordFoundLabel)
                            {
                                
                                [subView removeFromSuperview];
                            }
                            
                            
                        }
                        
                        [self UpdateUI];
                    }
                    
                }
                @catch (NSException *exception) {
                    [self showAlertViewWithTitle:kError message:kInternalInconsistencyErrorMessage];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                }
            });
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}
-(void)CallApiSearchBeerWithSearchItem:(NSString*)searchitem :(NSMutableArray*)FilterItems :(int)page{
    DLog(@"FilterItems--%@",FilterItems);
    __weak User *userObj=kGetUserModel;
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setValue:[NSString stringWithFormat:@"%d",page] forKey:kParamPageNumber];
    [dict setValue:kSTOREIDbottlecappsString forKey:kParamStoreId];
    [dict setValue:userObj.userId forKey:kParamUserId];
    [dict setValue:searchitem forKey:kParamSearchText];
    
    NSString *minPrice = @"";
    NSString *maxPrice = @"";
    NSString *typeName=@"";
    NSString *size =@"";
    NSString *price=@"";
    if ([FilterItems count]) {
        typeName = [[FilterItems objectAtIndex:0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        size=      [[FilterItems objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        price = PriceRange;
        
        if ([typeName isEqualToString:@"Type"]||[typeName isEqualToString:kStringSelectAll])
        {
            typeName = @"";
        }
        if ([size isEqualToString:@"Size"]||[size isEqualToString:kStringSelectAll])
        {
            size = @"";
        }
        if ([price isEqualToString:@"Price Range"])
        {
            price = @"";
        }
        
        if([price isEqualToString:kStringSelectAll] || [price isEqualToString:@""]){
            minPrice = @"";
            maxPrice = @"";
        }else if([price rangeOfString:@"-"].length > 0 ){
            NSArray *priceArr = [price componentsSeparatedByString:@"-"];
            minPrice = [[[priceArr objectAtIndex:0] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            maxPrice = [[[priceArr objectAtIndex:1] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        }else if([price rangeOfString:@"+"].length > 0){
            NSArray *priceArr = [price componentsSeparatedByString:@"+"];
            minPrice = [[[priceArr objectAtIndex:0] stringByReplacingOccurrencesOfString:@"$" withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            maxPrice = @"";
        }
        
        // if type is all
        
        if([typeName isEqualToString:kStringSelectAll])
        {
            typeName = @"";
        }
        // if size is all
        if([size isEqualToString:kStringSelectAll])
        {
            size = @"";
        }
    }
    
    DLog(@"size-------------------------------%@",size);
    
    [dict setValue:typeName forKey:kParamTypeName];
    [dict setValue:size forKey:kParamSize];
    [dict setValue:minPrice forKey:kParamPriceMin];
    [dict setValue:maxPrice forKey:kParamPriceMax];
    DLog(@"SearchBeerWithParamDict=========================%@",dict);
    
   // [self showSpinnerToSuperView:self.view]; // To show spinner
    [self showSpinner1];
    [[ModelManager modelManager] SearchBeerWithParam:dict  successStatus:^(APIResponseStatusCode statusCode, id response) {
       // NSLog(@"response--%@", response);
        
        [self hideSpinner];
        [_SearchTextField resignFirstResponder];
        
        @try {
            for (NSDictionary *dict in [response valueForKey:@"productCount"]) {
                Product *productObj=[[Product alloc] initWithDictionary:dict];
                [ItemArray addObject:productObj];
            }
            totalCount=[[response valueForKey:@"totalNoOfRecords"] intValue];
            
            if([ItemArray count] == 0){
                [self addNorecordFoundLAbelOnTableView:self.defaultTableView];
                [self.defaultTableView reloadData];
            }
            
            else{
                [self removeNorecordfoundLabel];
                [self UpdateUI];
            }
            
        }
        @catch (NSException *exception) {
            [self showAlertViewWithTitle:kError message:kInternalInconsistencyErrorMessage];
            
        }
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        
    }];
}

-(void)removeNorecordfoundLabel{
    for (UIView *subView in self.defaultTableView.subviews)
    {
        
        if (subView.tag==kTagNorecordFoundLabel)
        {
            
            [subView removeFromSuperview];
        }
        
        
    }
}
-(void)addNorecordFoundLAbelOnTableView:(UITableView*)tableView{
    NorecordFoundLabel = [[UILabel alloc] init];
    [NorecordFoundLabel setBackgroundColor:[UIColor clearColor]];
    [NorecordFoundLabel setTextColor:[self getColorForBlackText]];
    [NorecordFoundLabel setText:kAlertNoRecordFound];
    NorecordFoundLabel.tag=kTagNorecordFoundLabel;
    [NorecordFoundLabel sizeToFit];
    NorecordFoundLabel.frame = CGRectMake((tableView.bounds.size.width - NorecordFoundLabel.bounds.size.width) / 2.0f,
                                          50,
                                          NorecordFoundLabel.bounds.size.width,
                                          NorecordFoundLabel.bounds.size.height);
    [tableView insertSubview:NorecordFoundLabel atIndex:0];
    
}

-(void)UpdateUI{
    //  [ self createUIForSearchBar ];
    if ([ItemArray count]>0) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kNavigationheight)];
        label.backgroundColor = [UIColor clearColor];
        label.numberOfLines = 2;
        label.font = [self addFontSemiBold];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
        DLog(@"%@",self.searchItemString);
        label.text =self.searchItemString  ;
        self.navigationItem.titleView = label;
        for (Product *productObj in ItemArray) {
            if ([[productObj productStatus] intValue]==0) {
                [self.arrayForBoolProductStatus addObject:@"0"];
            }
            else{
                [self.arrayForBoolProductStatus addObject:@"1"];
            }
        }
        DLog(@"self.arrayForBoolProductStatus-----------%@",self.arrayForBoolProductStatus);
        // Do any additional setup after loading the view.
        if (isfirstTime==YES) {
            [self refreshFilterScreen];
            isfirstTime=NO;
            
        }
        self.defaultTableView .delegate=self;
        self.defaultTableView.dataSource=self;
        
        [self.defaultTableView reloadData];
        
        //   [self getCountry];
    }
    
    
    
}

-(void)addSearchBar{
    UIImage *filterImage=[UIImage imageNamed:kImageFilterButton];
    bgSearchbarV=[self createContentViewWithFrame:CGRectMake(0, kNavigationheight, SCREEN_WIDTH, kNavigationheight)];
    [bgSearchbarV setBackgroundColor:[self getColorForBlackText]];
    [self.view addSubview:bgSearchbarV];
    
    _SearchTextField =[self createDefaultTxtfieldWithRect:CGRectMake(kRightPadding, (kNavigationheight-(filterImage.size.height))/2, kTEXTfieldWidth, filterImage.size.height) text:kEmptyString withTag:kTextfieldTagWhiteBorder withPlaceholder:kPlaceHolderSearch];
    [_SearchTextField setTextColor:[UIColor whiteColor]];
    
    [_SearchTextField setDelegate:self];
    
    [bgSearchbarV addSubview:_SearchTextField];
    FilterButton=[self createButtonWithFrame:CGRectMake(_SearchTextField.frame.size.width+_SearchTextField.frame.origin.x+kPaddingSmall, (kNavigationheight-(filterImage.size.height))/2, filterImage.size.width, filterImage.size.height) withTitle:kEmptyString];
    [FilterButton setImage:filterImage forState:UIControlStateNormal];
    [FilterButton addTarget:self action:@selector(FilterButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bgSearchbarV addSubview:FilterButton];
    
    UIImage *goImage=[UIImage imageNamed:kImageGoButton];
    _GoButton=[self createButtonWithFrame:CGRectMake(FilterButton.frame.size.width+FilterButton.frame.origin.x+kPaddingSmall,(kNavigationheight-(goImage.size.height))/2, goImage.size.width, goImage.size.height) withTitle:kEmptyString];
    [_GoButton setImage:goImage forState:UIControlStateNormal];
    [_GoButton addTarget:self action:@selector(GoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bgSearchbarV addSubview:_GoButton];
}
-(void)initializeArrayAndDictionary{
    // arrayForBoolCart=[NSMutableArray new];
    apiResponseDictionary=[NSMutableDictionary new];
    selectedProductArray =[NSMutableArray new];
   // cartLocalArray = [NSMutableArray array];
    productBrowseArray = [NSMutableArray array];
    //cartBrowseArray = [NSMutableArray array];
    self.arrayForBoolProductStatus=[NSMutableArray new];
    ItemArray = [NSMutableArray new];
    isTypeSelected=NO;
    isSizeSelected=NO;
    isPriceSelected=NO;
    //NSMutableArray
    
    headerViewStore = [NSMutableArray array];
}
-(void)viewWillAppear:(BOOL)animated{
      [super viewWillAppear:animated];
//    NSIndexPath *selectedIndex1 = [self.defaultTableView indexPathForSelectedRow];
//    [self.defaultTableView reloadData];
//    [self.defaultTableView selectRowAtIndexPath:selectedIndex1 animated:NO scrollPosition:UITableViewScrollPositionNone];

    
    /************** This changes the navigation title for beer liquor and wine*******/

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addSearchBar];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    isfilterClicked=NO;
    isfirstTime=YES;
    [self AddDefaultTableView];
    
    [self initializeArrayAndDictionary];
    pagenumber=1;
    totalCount=0;
    /************** This changes the navigation title for beer liquor and wine*******/
    if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTBeer]) {
        [self CallApiSearchBeerWithSearchItem:self.SearchTextField.text :FilteriTemsArray:1];
    }
    else if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine]){
        [self CallApiSearchWineWithSearchItem:self.SearchTextField.text :FilteriTemsArray:1];
    }
    else if ([searchType isEqualToString:@"myproduct"]){
        [self setNavBarTitle:kNavigationTitleFINDAPRODUCT controller:self italic:NO];
        
        [self   CallApiSearchproductWithBarcodeId :self.searchProductName:1];
    }    else{
        [self CallApiSearchLiquorWithSearchItem:self.SearchTextField.text :FilteriTemsArray:1];
    }
    
}

-(void) viewDidDisappear:(BOOL)animated {
    [self.transparentView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==k_expandableTableViewTag) {
        if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine]) {
            return kFilterHeightWine;
            
        }
        else{
            return kFilterHeight;
        }
    }
    else{
        return 0.0;
        
    }
}
-(void)dropDownButtonClicked:(UIButton*)button{
    //[sender setImage:[UIImage imageNamed:kImageCrossButton] forState:UIControlStateNormal];
    
    if (![button isSelected]) {
        [button setSelected:YES];
        // [button setBackgroundColor:[self getColorForRedText]];
    }
    else{
        [button setSelected:NO];
        //  [button setBackgroundColor:[UIColor clearColor]];
    }
    
    SelectedSection= (int) button.tag;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:button.tag];
    
    BOOL isValuesUpdated = NO;
    
    switch (SelectedSection) {
        case 0:
            isValuesUpdated = [self getType];
            break;
            
        case 1:
            isValuesUpdated = [self getSize];
            
            break;
            
        case 2:
            isValuesUpdated = [self getCountry];
            
            break;
            
        case 3:
            isValuesUpdated = [self getRegion];
            
            break;
        case 4:
            isValuesUpdated = [self getSubType];
            
            break;
        default:
            break;
    }
    
    if(isValuesUpdated) {
        if (indexPath.row == 0) {
            isClosed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
            NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
            
            for (int i=0; i<[FilteriTemsArray count]; i++) {
                if (indexPath.section==i) {
                    [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!isClosed]];
                }
                else    {
                    if([[arrayForBool objectAtIndex:i] boolValue])  {
                        [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
                        [indexSet addIndex:i];
                    }
                }
            }
            [indexSet addIndex:button.tag];
            [_expandableTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine]) {
        sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.expandableTableView.frame.size.width,kFilterHeightWine)];
    }
    else{
        sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.expandableTableView.frame.size.width,kFilterHeight)];
    }
    sectionView.tag=kTagCartAndLikeView;
    
    [sectionView setBackgroundColor:[UIColor whiteColor]];
    if (tableView.tag==k_expandableTableViewTag) {
        if (section<[FilteriTemsArray count]) {
            UIImageView *bgImageView=[[UIImageView alloc]initWithFrame:sectionView.frame ];
            [bgImageView setImage:[UIImage imageNamed:kImageButtonStrip]];
            [sectionView addSubview:bgImageView];
            
            UILabel *viewLabel;
            if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine]) {
                viewLabel=[[UILabel alloc]initWithFrame:CGRectMake((SCREEN_WIDTH-kLabelWidthLarge)/2, kPaddingSmall - 3, kLabelWidthLarge,kFilterHeightWine - 5)];
            }
            else{
                viewLabel=[[UILabel alloc]initWithFrame:CGRectMake((SCREEN_WIDTH-kLabelWidthLarge)/2, kPaddingSmall - 3, kLabelWidthLarge,kFilterHeight - 5)];
            }
            viewLabel.backgroundColor=[UIColor clearColor];
            viewLabel.text=[FilteriTemsArray objectAtIndex:section];
            
            //[UIColor colorWithPatternImage:[UIImage imageNamed:kImageButtonStrip]];
            viewLabel.font=[self addFontRegular];
            if ([[FilteriTemsArray objectAtIndex:section]isEqualToString:@"Type"]||[[FilteriTemsArray objectAtIndex:section]isEqualToString:@"Size"]||[[FilteriTemsArray objectAtIndex:section]isEqualToString:@"Country"]||[[FilteriTemsArray objectAtIndex:section]isEqualToString:@"Region"]||[[FilteriTemsArray objectAtIndex:section]isEqualToString:@"Varietal"]) {
                viewLabel.textColor=[self getColorForBlackText];
            }
            else {
                viewLabel.textColor=[self getColorForRedText];
            }
            [sectionView addSubview:viewLabel];
            [sectionView setClipsToBounds:YES];
            
            dropDownButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [dropDownButton setTag:section];
            [dropDownButton addTarget:self
                               action:@selector(dropDownButtonClicked:)
                     forControlEvents:UIControlEventTouchUpInside];
            
            dropDownButton.frame = CGRectMake(0.0, 0.0,sectionView.frame.size.width,sectionView.frame.size.height);
            [sectionView addSubview:dropDownButton];
            
            UIImageView * arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(sectionView.frame.size.width - 30, 8, 15, 15)];
            if([[arrayForBool objectAtIndex:section] boolValue])
                [arrowView setImage:[UIImage imageNamed:kImageUpArrow]];
            else
                [arrowView setImage:[UIImage imageNamed:kImageDownArrow]];
            
            [sectionView addSubview:arrowView];
            
            if(headerViewStore) {
                if(headerViewStore.count > section && [headerViewStore objectAtIndex:section])
                    [headerViewStore replaceObjectAtIndex:section withObject:sectionView];
                else
                    [headerViewStore insertObject:sectionView atIndex:section];
                
            }
            return  sectionView;
        }
        else if (section==[FilteriTemsArray count]){
            if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine]) {
                PriceRangeView=[self createContentViewWithFrame:CGRectMake(0, 0, self.expandableTableView.frame.size.width, kFilterHeightWine)];
            }
            else{
                PriceRangeView=[self createContentViewWithFrame:CGRectMake(0, 0, self.expandableTableView.frame.size.width, kFilterHeight)];
            }
            [PriceRangeView setBackgroundColor:[UIColor clearColor]];
            UIButton *priceRangeButton;
            for (int i=0; i<3; i++) {
                //(((k_containerViewWidth/3)-(kPaddingSmall*3))*i,0,((k_containerViewWidth/3)-kPaddingSmall),kButtonHeight)
                if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine]) {
                    priceRangeButton=[self createButtonWithFrame:CGRectMake((i*(2 + SCREEN_WIDTH/3)),0,(SCREEN_WIDTH/3 - 3),kFilterHeightWine)                                                             withTitle:kEmptyString];
                }
                else {
                    priceRangeButton=[self createButtonWithFrame:CGRectMake(+(i*(2 + SCREEN_WIDTH/3)),0,(SCREEN_WIDTH/3 - 4),kFilterHeight) withTitle:kEmptyString];
                }
                
                priceRangeButton.backgroundColor=[UIColor clearColor];
                [[priceRangeButton layer]setBorderWidth:1 ];
                [[priceRangeButton layer]setBorderColor:(__bridge CGColorRef)([UIColor grayColor]) ];
                [priceRangeButton setTag:i];
                switch (i) {
                    case 0:
                        [priceRangeButton setTitle:@"$" forState:UIControlStateNormal];
                        break;
                    case 1:
                        [priceRangeButton setTitle:@"$$" forState:UIControlStateNormal];
                        break;
                    case 2:
                        [priceRangeButton setTitle:@"$$$" forState:UIControlStateNormal];
                        break;
                    default:
                        break;
                }
                [priceRangeButton addTarget:self action:@selector(priceRangeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                [priceRangeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                
                [PriceRangeView addSubview:priceRangeButton];
                
                UIView *lineView = [[UIView alloc] initWithFrame:
                                    CGRectMake(priceRangeButton.frame.origin.x + priceRangeButton.frame.size.width + 2, 0, 1, kFilterHeightWine)];
                
                lineView.backgroundColor = [UIColor colorWithRed:0.9408 green:0.9408 blue:0.9408 alpha:1.0];
                [PriceRangeView addSubview:lineView];
            }
            [sectionView addSubview:PriceRangeView];
            return  sectionView;
        }
        else
            
        {
            
            if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine]) {
                
                
                FilterButton=[self createButtonWithFrame:CGRectMake(0,0, self.expandableTableView.frame.size.width, kFilterHeightWine) withTitle:kButtonTitleFilter];
            }
            else{
                FilterButton=[self createButtonWithFrame:CGRectMake(0,0, self.expandableTableView.frame.size.width, kFilterHeight) withTitle:kButtonTitleFilter];
                
            }
            [FilterButton setBackgroundColor:[UIColor colorWithRed:0.5843 green:0.5843 blue:0.5843 alpha:1.0]];
            [FilterButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [FilterButton.titleLabel setFont: [UIFont fontWithName:kFontBold size:kFontBoldSize]];
            [FilterButton addTarget:self action:@selector(FilterItems:) forControlEvents:UIControlEventTouchUpInside];
            [sectionView addSubview:FilterButton];
            return  sectionView;
            
            
        }
        /********** Add UITapGestureRecognizer to SectionView   **************/
        
        // [sectionView addGestureRecognizer:headerTapped];
        
        
    }
    else{
        return nil;
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of ections.
    if (tableView.tag==k_expandableTableViewTag) {
        return [FilteriTemsArray count]+2;
    }
    else{
        return 1;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag==k_expandableTableViewTag) {
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
            return kdropdownButtonHeight - 1;
        }
        return 0;
    }
    else    {
        if (indexPath.row==[ItemArray count]) {
            return kButtonHeight;
            
        }
        else
            return kNewRowHeight;
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag==k_expandableTableViewTag) {
        if (section<[FilteriTemsArray count]) {
            if ([[arrayForBool objectAtIndex:section] boolValue]) {
                switch (section) {
                    case 0:{
                        if ([typeArray count]%2==0) {
                            return  [typeArray count]/2;
                        }
                        else{
                            return  ([typeArray count]/2)+1;
                        }
                    }
                        break;
                    case 1:{
                        if ([sizeArray count]%2==0) {
                            return  [sizeArray count]/2;
                        }
                        else{
                            return  ([sizeArray count]/2)+1;
                        }
                    }
                    case 2:{
                        if ([countryArray count]%2==0) {
                            return  [countryArray count]/2;
                        }
                        else{
                            return  ([countryArray count]/2)+1;
                            
                        }
                        
                    }
                    case 3:{
                        DLog(@"%@",regionNameArray);
                        if ([regionNameArray count]%2==0) {
                            return  [regionNameArray count]/2;
                            
                        }
                        else{
                            return  ([regionNameArray count]/2)+1;
                            
                        }
                        
                    }
                    case 4:{
                        DLog(@"%@",subTypeArray);
                        if ([subTypeArray count]%2==0) {
                            return  [subTypeArray count]/2;
                            
                        }
                        else{
                            return  ([subTypeArray count]/2)+1;
                            
                        }
                        
                    }
                        
                    default:
                        break;
                }
                return 0;
            }
            else
                return 0;
        }
        else{
            return 0;
            
        }
    }
    else{
        if ([ItemArray count]<totalCount && [ItemArray count]!=0) {
            return [ItemArray count]+1;
        }
        
        else{
            return [ItemArray count];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //int favoriteButtonTagValue = (int)indexPath.row;
    if (tableView.tag==k_expandableTableViewTag) {
        static NSString *simpleTableIdentifier = @"SimpleTableItem";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
            [cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
        }
        rightButton = [UIActionButton buttonWithType:UIButtonTypeCustom];
        [rightButton addTarget:self
                        action:@selector(rightButton:)
              forControlEvents:UIControlEventTouchUpInside];
        [rightButton setRowNumber:(int)indexPath.row];
        [rightButton setSectionNumber:(int)indexPath.section];
        [rightButton setTitle:kEmptyString forState:UIControlStateNormal];
        rightButton.frame = CGRectMake(0, 1, (SCREEN_WIDTH/2), kdropdownButtonHeight-2);
        [rightButton   setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [rightButton   setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [rightButton setBackgroundColor: [UIColor colorWithRed:0.9294 green:0.9294 blue:0.9294 alpha:1.0]];
        [rightButton.titleLabel setFont:[self addFontRegular]];
        leftButton = [UIActionButton buttonWithType:UIButtonTypeCustom];
        [leftButton addTarget:self
                       action:@selector(leftButton:)
             forControlEvents:UIControlEventTouchUpInside];
        [leftButton setTitle:kEmptyString forState:UIControlStateNormal];
        [leftButton setRowNumber:(int)indexPath.row];
        [leftButton setSectionNumber:(int)indexPath.section];
        leftButton.frame = CGRectMake((rightButton.frame.origin.x)+(rightButton.frame.size.width)+1, 1, (SCREEN_WIDTH/2), kdropdownButtonHeight-2);
        [leftButton   setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [leftButton   setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [leftButton setBackgroundColor:[UIColor colorWithRed:0.9294 green:0.9294 blue:0.9294 alpha:1.0]];
        [leftButton .titleLabel setFont:[self addFontRegular]];
        
        bottomButton = [UIActionButton buttonWithType:UIButtonTypeCustom];
        [bottomButton addTarget:self
                         action:@selector(bottomButton:)
               forControlEvents:UIControlEventTouchUpInside];
        [bottomButton setTitle:kEmptyString forState:UIControlStateNormal];
        [bottomButton setRowNumber:(int)indexPath.row];
        [bottomButton setSectionNumber:(int)indexPath.section];
        bottomButton.frame = CGRectMake(0, 1, SCREEN_WIDTH, kdropdownButtonHeight-2);
        [bottomButton   setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [bottomButton   setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [bottomButton setBackgroundColor:[UIColor colorWithRed:0.9294 green:0.9294 blue:0.9294 alpha:1.0]];
        [bottomButton .titleLabel setFont:[self addFontRegular]];
        
        switch (indexPath.section) {
            case 0:{
                if ([typeArray count]%2==0) {
                    [cell addSubview:rightButton];
                    [cell addSubview:leftButton];
                    [rightButton setTitle:[typeArray objectAtIndex:krightlabelIndex] forState:UIControlStateNormal] ;
                    [leftButton setTitle: [typeArray objectAtIndex:krightlabelIndex+1 ]forState:UIControlStateNormal];
                }
                else {
                    if(typeArray.count<2){
                        [cell addSubview:bottomButton];
                        [bottomButton setTitle:[typeArray lastObject] forState:UIControlStateNormal];
                    }
                    else if (indexPath.row==kLastIndexType) {
                        [cell addSubview:bottomButton];
                        [bottomButton setTitle:[typeArray lastObject] forState:UIControlStateNormal];
                        
                    }
                    else{
                        [cell addSubview:rightButton];
                        [cell addSubview:leftButton];
                        [rightButton setTitle:[typeArray objectAtIndex:krightlabelIndex] forState:UIControlStateNormal] ;
                        [leftButton setTitle: [typeArray objectAtIndex:krightlabelIndex+1 ]forState:UIControlStateNormal];
                    }
                }
                
            }
                break;
            case 1:{
                if ([sizeArray count]%2==0) {
                    [cell addSubview:rightButton];
                    [cell addSubview:leftButton];
                    [rightButton setTitle:[sizeArray objectAtIndex:krightlabelIndex] forState:UIControlStateNormal] ;
                    [leftButton setTitle: [sizeArray objectAtIndex:krightlabelIndex+1 ]forState:UIControlStateNormal];
                }
                else {
                    if(sizeArray.count<2){
                        [cell addSubview:bottomButton];
                        [bottomButton setTitle:[sizeArray lastObject] forState:UIControlStateNormal];
                    }
                    else if (indexPath.row==kLastIndexSize) {
                        [cell addSubview:bottomButton];
                        [bottomButton setTitle:[sizeArray lastObject] forState:UIControlStateNormal];
                    }
                    else{
                        [cell addSubview:rightButton];
                        [cell addSubview:leftButton];
                        [rightButton setTitle:[sizeArray objectAtIndex:krightlabelIndex] forState:UIControlStateNormal] ;
                        [leftButton setTitle: [sizeArray objectAtIndex:krightlabelIndex+1 ]forState:UIControlStateNormal];
                    }
                }
            }
                break;
                
            case 2:{
                if ([countryArray count]%2==0) {
                    [cell addSubview:rightButton];
                    [cell addSubview:leftButton];
                    
                    [rightButton setTitle:[countryArray objectAtIndex:krightlabelIndex] forState:UIControlStateNormal] ;
                    [leftButton setTitle: [countryArray objectAtIndex:krightlabelIndex+1 ]forState:UIControlStateNormal];
                }
                else {
                    if(countryArray.count<2){
                        [cell addSubview:bottomButton];
                        [bottomButton setTitle:[countryArray lastObject] forState:UIControlStateNormal];
                    }
                    if (indexPath.row==kLastIndexcountryArray) {
                        [cell addSubview:bottomButton];
                        [bottomButton setTitle:[countryArray lastObject] forState:UIControlStateNormal];
                    }
                    else{
                        [cell addSubview:rightButton];
                        [cell addSubview:leftButton];
                        [rightButton setTitle:[countryArray objectAtIndex:krightlabelIndex] forState:UIControlStateNormal] ;
                        [leftButton setTitle: [countryArray objectAtIndex:krightlabelIndex+1 ]forState:UIControlStateNormal];
                    }
                }
            }
                break;
                
            case 3:{
                if ([regionNameArray count]%2==0) {
                    [cell addSubview:rightButton];
                    [cell addSubview:leftButton];
                    
                    [rightButton setTitle:[regionNameArray objectAtIndex:krightlabelIndex] forState:UIControlStateNormal] ;
                    [leftButton setTitle: [regionNameArray objectAtIndex:krightlabelIndex+1 ]forState:UIControlStateNormal];
                }
                else {
                    if(regionNameArray.count<2){
                        [cell addSubview:bottomButton];
                        [bottomButton setTitle:[regionNameArray lastObject] forState:UIControlStateNormal];
                    }
                    if (indexPath.row==kLastIndexregionNameArray) {
                        [cell addSubview:bottomButton];
                        [bottomButton setTitle:[regionNameArray lastObject] forState:UIControlStateNormal];
                    }
                    else{
                        [cell addSubview:rightButton];
                        [cell addSubview:leftButton];
                        [rightButton setTitle:[regionNameArray objectAtIndex:krightlabelIndex] forState:UIControlStateNormal] ;
                        [leftButton setTitle: [regionNameArray objectAtIndex:krightlabelIndex+1 ]forState:UIControlStateNormal];
                    }
                }
            }
                break;
            case 4:{
                if ([subTypeArray count]%2==0) {
                    [cell addSubview:rightButton];
                    [cell addSubview:leftButton];
                    
                    [rightButton setTitle:[subTypeArray objectAtIndex:krightlabelIndex] forState:UIControlStateNormal] ;
                    [leftButton setTitle: [subTypeArray objectAtIndex:krightlabelIndex+1 ]forState:UIControlStateNormal];
                }
                else {
                    if(subTypeArray.count<2){
                        [cell addSubview:bottomButton];
                        [bottomButton setTitle:[subTypeArray lastObject] forState:UIControlStateNormal];
                    }
                    if (indexPath.row==kLastIndexSubTypeArray) {
                        [cell addSubview:bottomButton];
                        [bottomButton setTitle:[subTypeArray lastObject] forState:UIControlStateNormal];
                    }
                    else{
                        [cell addSubview:rightButton];
                        [cell addSubview:leftButton];
                        [rightButton setTitle:[subTypeArray objectAtIndex:krightlabelIndex] forState:UIControlStateNormal] ;
                        [leftButton setTitle: [subTypeArray objectAtIndex:krightlabelIndex+1 ]forState:UIControlStateNormal];
                    }
                }
            }
                break;
                
        }
        
        return cell;
    }
    else{
        
        static NSString *postCellId = @"postCell";
        static NSString *moreCellId = @"moreCell";
        NSUInteger row = [indexPath row];
        NSUInteger count = [ItemArray count];
        UITableViewCell *cell =nil;
        
        if (row == count && count != 0) {
            
            cell = [tableView dequeueReusableCellWithIdentifier:moreCellId];
            if (cell == nil) {
                cell = [[UITableViewCell alloc]
                        initWithStyle:UITableViewCellStyleDefault
                        reuseIdentifier:moreCellId];
                cell.textLabel.text = kTapToloadMore;
                cell.textLabel.textColor = [self getColorForRedText];
                cell.textLabel.font = [self addFontRegular];
                
            }
            
            return cell;
            
            
        }
        
        
        
        else    {
            AsyncImageView  *ProductImageView;
            // UITextView      *productDetailsTxtView;
            
            
            QALabel         *productNameLabel;
            UILabel         *productSizeLabel;
            UILabel         *productPriceLabel;
            
            
            UIView          *CartAndLikeView;
            UIActionButton  *cartButton;
            UIActionButton  * FavButton;
            UIView          *seperatorV;
            UIView          *bgv;
            UIView          *leftSeperatorV;
            UIImageView     *separatorEnd;
             UIImageView* imgOutOfStock;
           
            
            cell = [tableView dequeueReusableCellWithIdentifier:postCellId];
                        if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:postCellId];
                bgv=[self createContentViewWithFrame:CGRectMake(0, 0, kNewRowHeight-1, krowHeightSearch-1)];
                [bgv setBackgroundColor:[UIColor whiteColor]];
                 [cell addSubview:imgOutOfStock];
               imgOutOfStock.hidden=YES;
                ProductImageView=[self createAsynImageViewWithRect:CGRectMake(3, 0, 76 ,77) imageUrl:[NSURL URLWithString:kEmptyString] isThumnail:YES];
                ProductImageView.tag = kTagProductImageV;
                
                productNameLabel= [[QALabel alloc] initWithFrame:CGRectMake((ProductImageView.frame.size.width)+(ProductImageView.frame.origin.x)+5, KproductNameYAxis, kLabelWidth+39, kLabelHeight*2)];
                productNameLabel.font = [UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize-2];
                productNameLabel.textAlignment = NSTextAlignmentLeft;
                productNameLabel.textColor = [self getColorForRedText];
                productNameLabel.tag=kTagProductName;
                productNameLabel.numberOfLines=2;
                
                productSizeLabel=[self createLblWithRect:CGRectMake((ProductImageView.frame.size.width)+(ProductImageView.frame.origin.x)+5,(productNameLabel.frame.origin.y)+(productNameLabel.frame.size.height) - 5, kLabelWidth+39, kLabelHeight) font:[self addFontRegular] text:kEmptyString textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
                productSizeLabel.tag=kTagProductSize;
                imgOutOfStock= [[UIImageView alloc] initWithFrame:CGRectMake((imgOutOfStock.frame.size.width)+183+koutOfstockImageYAndXAxis, 43+koutOfstockImageYAndXAxis, 38,38)];
                [imgOutOfStock setTag:101];
                [cell addSubview:imgOutOfStock];
                productPriceLabel=[self createLblWithRect:CGRectMake((ProductImageView.frame.size.width)+(ProductImageView.frame.origin.x)+5, (productSizeLabel.frame.size.height)+(productSizeLabel.frame.origin.y) - 5, kLabelWidth+39, kLabelHeight) font:[UIFont fontWithName:kFontBold size:kFontBoldSize-2] text:kEmptyString textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
                [productPriceLabel setTag:kTagProductPrice];
                
                  
                
                CartAndLikeView=[self createContentViewWithFrame:CGRectMake(SCREEN_WIDTH-kCartAndLikeViewWidth, 0, kCartAndLikeViewWidth,krowHeightSearch-(kPaddingcartAndFav*2))];
                [CartAndLikeView setBackgroundColor:[UIColor clearColor]];
                [CartAndLikeView setTag:kTagCartAndLikeView];
                
                cartButton=[self createActionButtonWithFrame:CGRectMake(kCartAndLikeViewWidth/2,0, kCartAndLikeViewWidth/2,kCartLikeViewHeight2 ) withTitle:kEmptyString];
                cartButton.tag = kTagCartButton;
                
                [cartButton addTarget:self action:@selector(cartButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                leftSeperatorV=[self createContentViewWithFrame:CGRectMake(0,0, 1.25,kCartLikeViewHeight2 )];
                [leftSeperatorV setBackgroundColor:[UIColor clearColor]];
                [[leftSeperatorV layer] setBorderWidth:1.0];
                seperatorV=[self createContentViewWithFrame:CGRectMake(kCartAndLikeViewWidth/2,0, 1.25 ,kCartLikeViewHeight2)];
                [seperatorV setBackgroundColor:[UIColor clearColor]];
                [[seperatorV layer] setBorderWidth:1.0];
                
                FavButton=[self createActionButtonWithFrame:CGRectMake(0,0, kCartAndLikeViewWidth/2,kCartLikeViewHeight2) withTitle:kEmptyString];
                FavButton.tag = kTagFavButton;
                            [FavButton addTarget:self action:@selector(FavButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:bgv];
                
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                
                [cell addSubview:bgv];
                
                [cell addSubview:ProductImageView];
                
                [cell addSubview: productNameLabel];
                [cell addSubview: productSizeLabel];
                [cell addSubview: productPriceLabel];
                
                [cell addSubview:CartAndLikeView];
                [CartAndLikeView addSubview:leftSeperatorV];
                [CartAndLikeView addSubview:FavButton];
                [CartAndLikeView addSubview:cartButton];
                [CartAndLikeView addSubview:seperatorV];
                separatorEnd = [[UIImageView alloc] initWithFrame:CGRectMake(0, krowHeightSearch-1, SCREEN_WIDTH, 1.25)];
                [separatorEnd setBackgroundColor:[UIColor colorWithRed:0.8278 green:0.8278 blue:0.8278 alpha:1.0]];
                [cell.contentView addSubview:separatorEnd];
            }
          //  NSLog(@"Item Array = %@",ItemArray);
            Product *productObj = [ItemArray objectAtIndex:indexPath.row];
            
            productImageURL = [NSURL URLWithString:productObj.productImageURL];
            
            ProductImageView = (AsyncImageView *)[cell viewWithTag: kTagProductImageV];
            [ProductImageView setImageURL:productImageURL];
            [ProductImageView setContentMode:UIViewContentModeScaleAspectFit];
            ProductImageView.layer.masksToBounds = YES;
            
            productNameLabel  = (QALabel *) [cell viewWithTag: kTagProductName];
            productSizeLabel  = (UILabel *) [cell viewWithTag: kTagProductSize];
            productPriceLabel = (UILabel *) [cell viewWithTag: kTagProductPrice];
            
            [productNameLabel setText: productObj.productName];
            productNameLabel.verticalAlignment = UIControlContentVerticalAlignmentBottom;
            [productSizeLabel setText:productObj.productSize];
            
            NSString *productPrice = nil;
            if ([productObj.productSalesPrice isEqualToString:kEmptyString]||[productObj.productSalesPrice isEqualToString:kEmptySalePrice]) {
                productPrice = [self addDollarSignToString:productObj.productPrice];
            }
            else{
                productPrice = [self addDollarSignToString:productObj.productSalesPrice];
            }
            [productPriceLabel setText:productPrice];
            
            cartButton = (UIActionButton *)[[cell viewWithTag:kTagCartAndLikeView] viewWithTag:kTagCartButton];
            FavButton = (UIActionButton *)[[cell viewWithTag:kTagCartAndLikeView] viewWithTag:kTagFavButton];
            cartButton.rowNumber = (int)indexPath.row;
            FavButton.rowNumber = (int)indexPath.row;
            FavButton.productObj=productObj;
            cartButton.productObj=productObj;
            imgOutOfStock=(UIImageView *)[cell viewWithTag:101];

            
            if ([[ShoppingCart sharedInstace ] shoppingCartContains:productObj]==NO) {
                [cartButton setImage:[UIImage imageNamed:kImagecarticongrey] forState:UIControlStateNormal];
                [cartButton setBackgroundColor:[UIColor clearColor]];
                [cartButton setSelected:NO];
           
            }
            else{
                [cartButton setImage:[UIImage imageNamed:kImageCartIconwhite] forState:UIControlStateNormal];
                [cartButton setBackgroundColor:[self getColorForYellowText]];
                [cartButton setSelected:YES];
                
            }
            
            //Product IMage
            //Product Name
            //Product Size
            //product Price
            //Product Fav Status
            //productStatus
            //Product Sale price
            //(kCartAndLikeViewWidth/2,0, kCartAndLikeViewWidth/2,kCartLikeViewHeight2 )
            
           // NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
           // NSMutableArray *defaultArray = [[defaults objectForKey:@"offlineFavoriteArray"]mutableCopy];
            /* if([notloggedIn intValue] == 1 && [defaultArray count]>0)
            {
               
                NSMutableArray *individualArray = [defaultArray objectAtIndex:indexPath.row];
                NSLog(@"Individual Array = %@",individualArray);
                if([[individualArray objectAtIndex:4] intValue]==0)
                {
                    [FavButton setImage:[UIImage imageNamed:kImageHeartIconGrey] forState:UIControlStateNormal];
                    [FavButton setBackgroundColor:[UIColor clearColor]];
                    [FavButton setSelected:NO];
                    imgOutOfStock.hidden=NO;
                }
                else
                {
                    [FavButton setImage:[UIImage imageNamed:kImageHeartIconwhite] forState:UIControlStateNormal];
                    [FavButton setBackgroundColor:[self getColorForRedText]];
                    [FavButton setSelected:YES];
                    
                    if ([[individualArray objectAtIndex:5] intValue]==0)
                    {
                        [cell setBackgroundColor: [UIColor colorWithRed:0.9373 green:0.9373 blue:0.9373 alpha:1.0]];
                        [bgv setBackgroundColor:[UIColor whiteColor]];
                        imgOutOfStock.hidden=NO;
                        if(imgOutOfStock.tag==101){
                            imgOutOfStock.image = [UIImage imageNamed:@"out-of-stock"];
                        }
                    }
                    else if ([[individualArray objectAtIndex:5] intValue]==1){
                        imgOutOfStock.hidden=YES;
                        [bgv setBackgroundColor:[UIColor whiteColor]];
                        [cell setBackgroundColor:[UIColor clearColor]];
                    }

                }
                
                
            }
            else
            { */
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            NSArray *favoriteOfflineArray = [userDefaults objectForKey:@"offlineFavoriteArray"];
           // NSLog(@"Favorite offline Array = %@",favoriteOfflineArray);
            
            if([notloggedIn intValue]==1 && [favoriteOfflineArray count]>0)
            {
                for(int i=0;i<[favoriteOfflineArray count];i++)
                {
                    NSArray *individualArray = [favoriteOfflineArray objectAtIndex:i];
                    //NSLog(@"Individual Array=%@",individualArray);
                    int productID = [[individualArray objectAtIndex:4]intValue];
                   // NSLog(@"product id=%d,product from cell=%d",productID,[productObj.productId intValue]);
                    if([productObj.productId intValue] == productID)
                    {
                        [FavButton setImage:[UIImage imageNamed:kImageHeartIconwhite] forState:UIControlStateNormal];
                        [FavButton setBackgroundColor:[self getColorForRedText]];
                        [FavButton setSelected:YES];
                        break;
                    }
                    else
                    {
                        [FavButton setImage:[UIImage imageNamed:kImageHeartIconGrey] forState:UIControlStateNormal];
                        [FavButton setBackgroundColor:[UIColor clearColor]];
                        [FavButton setSelected:NO];
                        imgOutOfStock.hidden=NO;
                    }
                    
                }
            }
            else
            {
          
            if([notloggedIn intValue]==1)
            {
                [FavButton setImage:[UIImage imageNamed:kImageHeartIconGrey] forState:UIControlStateNormal];
                [FavButton setBackgroundColor:[UIColor clearColor]];
                [FavButton setSelected:NO];
                imgOutOfStock.hidden=NO;
            }
           else
           {
            if ([productObj.productFavStatus intValue] == 0) {
                [FavButton setImage:[UIImage imageNamed:kImageHeartIconGrey] forState:UIControlStateNormal];
                [FavButton setBackgroundColor:[UIColor clearColor]];
                [FavButton setSelected:NO];
                imgOutOfStock.hidden=NO;
            }
            else{
                [FavButton setImage:[UIImage imageNamed:kImageHeartIconwhite] forState:UIControlStateNormal];
                [FavButton setBackgroundColor:[self getColorForRedText]];
                [FavButton setSelected:YES];
            }
           }
            }
             if ([productObj.productStatus intValue]==0) {
                    [cell setBackgroundColor: [UIColor colorWithRed:0.9373 green:0.9373 blue:0.9373 alpha:1.0]];
                    [bgv setBackgroundColor:[UIColor whiteColor]];
                    imgOutOfStock.hidden=NO;
                    if(imgOutOfStock.tag==101){
                        imgOutOfStock.image = [UIImage imageNamed:@"out-of-stock"];
                    }
                }
                else if ([productObj.productStatus intValue]==1){
                    imgOutOfStock.hidden=YES;
                    [bgv setBackgroundColor:[UIColor whiteColor]];
                    [cell setBackgroundColor:[UIColor clearColor]];
                }
        
            
          
            [[leftSeperatorV layer] setBorderColor:[[UIColor colorWithRed:0.8278 green:0.8278 blue:0.8278 alpha:1.0] CGColor]];
            [[seperatorV layer] setBorderColor:[[UIColor colorWithRed:0.8278 green:0.8278 blue:0.8278 alpha:1.0] CGColor]];
           
            [cell.layer setMasksToBounds:YES];
            return cell;

            
        }
    }            /*************************************************Add Cart and Like*****************************************************/
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag==k_expandableTableViewTag) {
        /*************** Close the section, once the data is selected ***********************************/
        [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:NO]];
        [_expandableTableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else{
        NSUInteger row = [indexPath row];
        NSUInteger count = [ItemArray count];
        if (row == count) {
            pagenumber++;
            if (count<totalCount) {
                if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTBeer]) {
                    [self CallApiSearchBeerWithSearchItem:self.SearchTextField.text :FilteriTemsArray:pagenumber];
                }
                else if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine]){
                    [self CallApiSearchWineWithSearchItem:self.SearchTextField.text :FilteriTemsArray:pagenumber];
                }
                else{
                    [self CallApiSearchLiquorWithSearchItem:self.SearchTextField.text :FilteriTemsArray:pagenumber];
                }
                DLog(@"pagenumber--%d",pagenumber);
            }
        }
        else{
            
            Product *productObj = [ItemArray objectAtIndex:indexPath.row];
            NSMutableArray *temparray=[NSMutableArray new];
            [temparray addObject:productObj];
            ProductDetailsViewController *vc=[self viewControllerWithIdentifier:kProductDetailsViewControllerStoryBoardID ];
            vc.productIDSelected=productObj.productId;
            vc.isfav=productObj.productFavStatus;
            vc.getProductarray=temparray;
            [self.navigationController pushViewController:vc animated:YES ];
        }
    }
}


-(BOOL)getRegion{
    DLog(@"FilteriTemsArray---%@",FilteriTemsArray);
    if ([[FilteriTemsArray objectAtIndex:2] isEqualToString:@"Country"])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Please Choose Country" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }
    else
    {
        NSString *countryString = [FilteriTemsArray objectAtIndex:2];
        NSString *filePath;
        if ([countryString isEqualToString:@"Australia/New Zealand"])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_State1" ofType:@"txt"];
        }
        
        if ([countryString isEqualToString:@"France"])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_State2" ofType:@"txt"];
        }
        if ([countryString isEqualToString:@"Germany"])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_State3" ofType:@"txt"];
        }
        if ([countryString isEqualToString:@"Italy"])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_State4" ofType:@"txt"];
        }
        if ([countryString isEqualToString:@"Other"])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_State5" ofType:@"txt"];
        }
        if ([countryString isEqualToString:@"Spain"])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_State6" ofType:@"txt"];
        }
        
        if ([countryString isEqualToString:@"United States"])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_State7" ofType:@"txt"];
        }
        
        if ([countryString isEqualToString:kStringSelectAll])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_State0" ofType:@"txt"];
        }
        
        //création d'un string avec le contenu du JSON
        NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
       // NSLog(@"json string is %@",myJSON);
        NSDictionary *JSON =
        [NSJSONSerialization JSONObjectWithData: [myJSON dataUsingEncoding:NSUTF8StringEncoding]
                                        options: NSJSONReadingMutableContainers
                                          error: nil];
     //   NSLog(@"dictionary is %@",JSON);
        
        regionResultArray = [[NSMutableArray alloc]initWithArray:[JSON valueForKey:@"Regions"]];
        
        if (regionNameArray == nil)
        {
            regionNameArray = [[NSMutableArray alloc]init];
            regionIdArray = [[NSMutableArray alloc]init];
        }
        else
        {
            [regionNameArray removeAllObjects];
            [regionIdArray removeAllObjects];
        }
        for (int i  =0; i< [regionResultArray count]; i++)
        {
            [regionNameArray addObject:[regionResultArray[i] valueForKey:@"RegionName"]];
            [regionIdArray addObject:[regionResultArray[i] valueForKey:@"RegionId"]];
        }
        
        NSLog(@"region Array is %@", regionNameArray);
        [regionNameArray addObject:kStringSelectAll];
        [regionIdArray addObject:kEmptyString];
        
        
        
        return YES;
    }
}
-(BOOL)getCountry{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"wine_Country" ofType:@"txt"];
    
    //création d'un string avec le contenu du JSON
    NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
   // NSLog(@"json string is %@",myJSON);
    NSDictionary *JSON =
    [NSJSONSerialization JSONObjectWithData: [myJSON dataUsingEncoding:NSUTF8StringEncoding]
                                    options: NSJSONReadingMutableContainers
                                      error: nil];
   // NSLog(@"dictionary is %@",JSON);
    countryResultArray = [[NSMutableArray alloc]initWithArray:[JSON valueForKey:@"GetCountryResult"]];
    countryArray = [[NSMutableArray alloc]init];
    countryIdArray = [[NSMutableArray alloc]init];
    
    [countryArray addObjectsFromArray:[countryResultArray valueForKey:@"CountryName"]];
    [countryIdArray addObjectsFromArray:[countryResultArray valueForKey:@"CountryID"]];
    [countryArray addObject:kStringSelectAll];
    [countryIdArray addObject:kEmptyString];
    NSLog(@"country array is %@" , countryArray);
    
    return YES;
}

-(BOOL)getSize{
    NSString *filePath;
    if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTBeer ]) {
        filePath = [[NSBundle mainBundle] pathForResource:@"beer_unitSizes" ofType:@"txt"];
    }
    else if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine ]){
        filePath = [[NSBundle mainBundle] pathForResource:@"wine_unitSize" ofType:@"txt"];
    }
    else{
        filePath = [[NSBundle mainBundle] pathForResource:@"liq_unitSize" ofType:@"txt"];
        
    }
    
    //création d'un string avec le contenu du JSON
    NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    NSLog(@"json string is %@",myJSON);
    NSDictionary *JSON =
    
    [NSJSONSerialization JSONObjectWithData: [myJSON dataUsingEncoding:NSUTF8StringEncoding]
                                    options: NSJSONReadingMutableContainers
                                      error: nil];
    NSLog(@"dictionary is %@",JSON);
    NSMutableArray *unitResultArray = [[NSMutableArray alloc]initWithArray:[JSON valueForKey:@"GetUnitSizeResult"]];
    sizeArray = [[NSMutableArray alloc]initWithCapacity:[unitResultArray count]];
    for (int i  =0; i< [unitResultArray count]; i++)
    {
        [sizeArray addObject:[unitResultArray[i] valueForKey:@"UnitDisplayName"]];
    }
    NSLog(@"sizeArray array is %@", sizeArray);
    [sizeArray addObject:kStringSelectAll];
    
    
    return YES;
}

-(BOOL)getType{
    NSString *filePath;
    if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTBeer ]) {
        filePath = [[NSBundle mainBundle] pathForResource:@"beer_getSubCategory" ofType:@"txt"];
    }
    else if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine ]){
        filePath = [[NSBundle mainBundle] pathForResource:@"wine_subCategory" ofType:@"txt"];
    }
    else{
        filePath = [[NSBundle mainBundle] pathForResource:@"liq_subCat" ofType:@"txt"];
        
    }
    //création d'un string avec le contenu du JSON
    NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    NSLog(@"json string is %@",myJSON);
    NSDictionary *JSON =
    [NSJSONSerialization JSONObjectWithData: [myJSON dataUsingEncoding:NSUTF8StringEncoding]
                                    options: NSJSONReadingMutableContainers
                                      error: nil];
    NSLog(@"dictionary is %@",JSON);
    subCategoryResultArray = [[NSMutableArray alloc]initWithObjects:[JSON valueForKey:@"GetSubCategoryResult"], nil];
    NSLog(@"subcategory array is %@", subCategoryResultArray);
    NSMutableArray *subCategoryNameArray  =[[NSMutableArray alloc]init];
    NSMutableArray *subCategoryIdArray = [[NSMutableArray alloc]init];
    
    if ([subCategoryResultArray count]>0){
        [subCategoryNameArray addObjectsFromArray:[subCategoryResultArray[0] valueForKey:@"SubCatName"]];
        [subCategoryIdArray addObjectsFromArray:[subCategoryResultArray[0] valueForKey:@"SubCatId"]];
    }
    
    NSLog(@"subCategoryNameArray is %@",subCategoryNameArray);
    NSLog(@"subCatId is %@",subCategoryIdArray);
    
    typeArray = [[NSMutableArray alloc]init];
    subTypeIDArray  = [[NSMutableArray alloc]init];
    for(int i=0;i < [subCategoryNameArray count]; i++)
    {
        [typeArray addObject:subCategoryNameArray[i]];
        [subTypeIDArray addObject:subCategoryIdArray[i]];
    }
    [typeArray addObject:kStringSelectAll];
    [subTypeIDArray addObject:@"0"];
    NSLog(@"subCategoryNameArray is %@",typeArray);
    NSLog(@"subCatId is %@",subTypeIDArray);
    
    return YES;
    
}


-(BOOL)getSubType{
    //    [self.view addSubview:activity];
    //    [activity startAnimating];
    //    FetchSubTypeOperation *operation = [[FetchSubTypeOperation alloc] init];
    //    operation.delegate = self;
    //    operation.catId = @"3";
    //
    if ([SUBTypeVal isEqualToString:@""])
    {
        SUBTypeVal=@"0";
    }
    
    if ([[FilteriTemsArray objectAtIndex:0] isEqualToString:@"Type"])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Please Choose Type" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
        return NO;
    }
    else
    {
        NSString *filePath;
        
        if ([[FilteriTemsArray objectAtIndex:0] isEqualToString:kStringSelectAll])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_subCategory2" ofType:@"txt"];
        }
        if ([[FilteriTemsArray objectAtIndex:0] isEqualToString:@"Dessert"])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_subCatDessert4" ofType:@"txt"];
        }
        if ([[FilteriTemsArray objectAtIndex:0] isEqualToString:@"Non-alcoholic"])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_subCatNonAlcoholic7" ofType:@"txt"];
        }
        if ([[FilteriTemsArray objectAtIndex:0] isEqualToString:@"Port / Sherry"])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_subCatPortShhery6" ofType:@"txt"];
        }
        if ([[FilteriTemsArray objectAtIndex:0] isEqualToString:@"Red"])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_subCatRed2" ofType:@"txt"];
        }
        if ([[FilteriTemsArray objectAtIndex:0] isEqualToString:@"Rose"])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_subCatRose5" ofType:@"txt"];
        }
        if ([[FilteriTemsArray objectAtIndex:0] isEqualToString:@"Sparkling"])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_subCatSparkling3" ofType:@"txt"];
        }
        if ([[FilteriTemsArray objectAtIndex:0] isEqualToString:@"White"])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_subCatWhite1" ofType:@"txt"];
        }
        
        if ([[FilteriTemsArray objectAtIndex:0] isEqualToString:@"Others"])
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"wine_Other" ofType:@"txt"];
        }
        
        if ([[FilteriTemsArray objectAtIndex:0] isEqualToString:kStringSelectAll])
        {
            NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
            NSLog(@"json string is %@",myJSON);
            NSMutableDictionary *jsonArray =  [NSJSONSerialization JSONObjectWithData:[myJSON dataUsingEncoding:NSUTF8StringEncoding]
                                                                              options: NSJSONReadingMutableContainers
                                                                                error: nil];
            NSLog(@"array  is %@",jsonArray);
            varietalArray = [[NSMutableArray alloc]initWithObjects:[jsonArray valueForKey:@"GetSubCategory2Result"], nil];
            NSLog(@"varietalArray array is %@", varietalArray);
            
            
            if (subTypeArray == nil)
            {
                subTypeArray = [[NSMutableArray alloc]init];
            }
            else
            {
                [subTypeArray removeAllObjects];
            }
            
            if (subTypeArray == nil)
            {
                subTypeArray = [[NSMutableArray alloc]init];
            }
            else
            {
                [subTypeArray removeAllObjects];
            }
            
            for (int i=0; i<[[varietalArray objectAtIndex:0] count]; i++)
            {
                [subTypeArray addObject:[[[varietalArray objectAtIndex:0] objectAtIndex:i]valueForKey:@"SubCatName2"]];
                [subTypeIDArray addObject:[[[varietalArray objectAtIndex:0] objectAtIndex:i] valueForKey:@"SubCatId2"]];
            }
            
            [subTypeArray addObject:kStringSelectAll];
            [subTypeIDArray addObject:@"0"];
            
        }
        else
        {
            
            NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
            NSLog(@"json string is %@",myJSON);
            NSMutableDictionary *jsonDict =  [NSJSONSerialization JSONObjectWithData:[myJSON dataUsingEncoding:NSUTF8StringEncoding]
                                                                             options: NSJSONReadingMutableContainers
                                                                               error: nil];
            NSLog(@"json dict is %@",jsonDict);
            varietalArray = [[NSMutableArray alloc]initWithArray:[jsonDict objectForKey:@"GetSubCategory2ByCatidSubcatidResult"]];
            NSLog(@"varietalArray array is %@", varietalArray);
            
            if (subTypeArray == nil)
            {
                subTypeArray = [[NSMutableArray alloc]init];
            }
            else
            {
                [subTypeArray removeAllObjects];
            }
            
            if (subTypeArray == nil)
            {
                subTypeArray = [[NSMutableArray alloc]init];
            }
            else
            {
                [subTypeArray removeAllObjects];
            }
            
            for (int i=0; i<[varietalArray count]; i++)
            {
                [subTypeArray addObject:[[varietalArray objectAtIndex:i]valueForKey:@"SubCatName2"]];
                [subTypeIDArray addObject:[[varietalArray objectAtIndex:i] valueForKey:@"SubCatId2"]];
            }
            
            [subTypeArray addObject:kStringSelectAll];
            [subTypeIDArray addObject:@"0"];
            
            
            //
            ////            NSLog(@"subType array is %@", subTypeArray);
            //          [subTypeArray insertObject:kStringSelectAll atIndex:0];
            //            [subTypeIDArray insertObject:@"" atIndex:0];
            ////
            ////            DLog(@"subTypeArray%@",subTypeArray);
            //
        }
        
        //création d'un string avec le contenu du JSON
        return YES;
    }
    //    operation.SUBcatId=SUBTypeVal;
    //    [operationQueue addOperation:operation];
    //    [operation release];
    
}

#pragma mark-BUTTON ACTION
-(void)refreshAllCell{
    for (UIView *view in _expandableTableView.subviews) {
        for (UITableViewCell *cell in view.subviews) {
            //do
            if ([cell isKindOfClass:[UITableViewCell class]]) {
                for (UIButton *btn in [cell subviews] ) {
                    if ([btn isKindOfClass:[UIButton class]]) {
                        
                        [btn setSelected:NO];
                        [btn setBackgroundColor:[self getColorForLightestGrey]];
                    }
                }
            }
        }
    }
}

- (void)bottomButton:(UIActionButton*)sender {
    [self refreshAllCell];
    DLog(@"SelectedSection--%d",SelectedSection);
    DLog(@"rowNumber-%d sectionNumber%d",sender.rowNumber,sender.sectionNumber);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.rowNumber inSection:sender.sectionNumber];
    UIView *thisView = (UIView*)[headerViewStore objectAtIndex:indexPath.section];
    DLog(@"%@",[thisView subviews])
    DLog(@"subviews%@",[[sender superview ] superview]);
    if ([sender isSelected]) {
        [sender setSelected:NO];
        [sender setBackgroundColor:[self getColorForLightestGrey]];
        for (UILabel *lbl in [thisView subviews] ) {
            if ([lbl isKindOfClass:[UILabel class]]) {
                lbl.text=[FilteriTemsArray objectAtIndex:indexPath.section];
                lbl.textColor=[self getColorForBlackText];
                
            }
        }
    }
    else{
        [sender setSelected:YES];
        [sender setBackgroundColor:[self getColorForRedText]];
        for (UILabel *lbl in [thisView subviews] ) {
            if ([lbl isKindOfClass:[UILabel class]]) {
                [FilteriTemsArray replaceObjectAtIndex:indexPath.section withObject:[sender titleLabel].text];
                lbl.text=[FilteriTemsArray objectAtIndex:indexPath.section];
                lbl.textColor=[self getColorForRedText];
                
                [selectedProductArray addObject:lbl.text];
                DLog(@"FilterItems--%@",selectedProductArray);
                
            }
        }
    }
}

-(void)rightButton:(UIActionButton*)sender{
    DLog(@"SelectedSection--%d",SelectedSection);
    DLog(@"rowNumber-%d sectionNumber%d",sender.rowNumber,sender.sectionNumber);
    
    [self refreshAllCell];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.rowNumber inSection:sender.sectionNumber];
    UIView *thisView = (UIView*)[headerViewStore objectAtIndex:indexPath.section];
    
    
    DLog(@"%@",[thisView subviews])
    DLog(@"subviews%@",[[sender superview ] superview]);
    if ([sender isSelected]) {
        [sender setSelected:NO];
        [sender setBackgroundColor:[self getColorForLightestGrey]];
        for (UILabel *lbl in [thisView subviews] ) {
            if ([lbl isKindOfClass:[UILabel class]]) {
                lbl.text=[FilteriTemsArray objectAtIndex:indexPath.section];
                lbl.textColor=[self getColorForBlackText];
                
                
            }
        }
    }
    else{
        [sender setSelected:YES];
        [sender setBackgroundColor:[self getColorForRedText]];
        for (UILabel *lbl in [thisView subviews] ) {
            if ([lbl isKindOfClass:[UILabel class]]) {
                [FilteriTemsArray replaceObjectAtIndex:indexPath.section withObject:[sender titleLabel].text];
                lbl.text=[FilteriTemsArray objectAtIndex:indexPath.section];
                DLog(@"selectedProductArray--%@",selectedProductArray);
                [selectedProductArray addObject:lbl.text];
                lbl.textColor=[self getColorForRedText];
            }
        }
    }
    DLog(@"titleLabel--%@",sender.titleLabel.text);
}
-(void)leftButton:(UIActionButton*)sender{
    [self refreshAllCell];
    DLog(@"rowNumber-%d sectionNumber%d",sender.rowNumber,sender.sectionNumber);
    DLog(@"SelectedSection--%d",SelectedSection);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.rowNumber inSection:sender.sectionNumber];
    UIView *thisView = (UIView*)[headerViewStore objectAtIndex:indexPath.section];
    for (UIView *view in _expandableTableView.subviews) {
        for (UITableViewCell *cell in view.subviews) {
            //do
            if ([cell isKindOfClass:[UITableViewCell class]]) {
                for (UIButton *btn in [cell subviews] ) {
                    if ([btn isKindOfClass:[UIButton class]]) {
                        [btn setSelected:NO];
                        [btn setBackgroundColor:[self getColorForLightestGrey]];
                    }
                }
            }
        }
    }
    DLog(@"%@",[thisView subviews])
    DLog(@"subviews%@",[[sender superview ] superview]);
    if ([sender isSelected]) {
        [sender setSelected:NO];
        [sender setBackgroundColor:[self getColorForLightestGrey]];
        for (UILabel *lbl in [thisView subviews] ) {
            if ([lbl isKindOfClass:[UILabel class]]) {
                lbl.text=[FilteriTemsArray objectAtIndex:indexPath.section];
                lbl.textColor=[self getColorForBlackText];
            }
        }
    }
    else{
        [sender setSelected:YES];
        [sender setBackgroundColor:[self getColorForRedText]];
        for (UILabel *lbl in [thisView subviews] ) {
            if ([lbl isKindOfClass:[UILabel class]]) {
                [FilteriTemsArray replaceObjectAtIndex:indexPath.section withObject:[sender titleLabel].text];
                lbl.text=[FilteriTemsArray objectAtIndex:indexPath.section];
                DLog(@"selectedProductArray--%@",selectedProductArray);
                [selectedProductArray addObject:lbl.text];
                lbl.textColor=[self getColorForRedText];
            }
        }
    }
    DLog(@"titleLabel--%@",sender.titleLabel.text);
}
/*******This is called when the user click GO Button*****/
- (void)GoButtonClicked:(UIButton*)sender {
    DLog(@"searchItemString --- %@",FilteriTemsArray);
    // We will perform search only  if the search textfield is not empty ,
    if ([self.SearchTextField.text  length]) {
        [FilteriTemsArray removeAllObjects];
        [ItemArray removeAllObjects];
        
        if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTBeer ]) {
            [self CallApiSearchBeerWithSearchItem:self.SearchTextField.text :FilteriTemsArray :1];
        }
        else if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine ]){
            [self CallApiSearchWineWithSearchItem:self.SearchTextField.text :FilteriTemsArray :1];
        }
        else{
            [self CallApiSearchLiquorWithSearchItem:self.SearchTextField.text :FilteriTemsArray :1];
        }
    }
  }
-(void)TapToDetailButtonClicked:(UIButton*)sender{
    ProductDetailsViewController *vc=[self viewControllerWithIdentifier:kProductDetailsViewControllerStoryBoardID ];
    vc.productIDSelected=[[ItemArray objectAtIndex:sender.tag]valueForKey:@"ProductID" ];
    [self.navigationController pushViewController:vc animated:YES ];
    
}
/**********This is called when the user click Filter button*********/
- (void)FilterButtonClicked:(id)sender {
    isfilterClicked=YES;
    [self addTransParentView];
}

/***This is called when the user add any particular item to Favourite ****/
-(void)FavButtonClicked:(UIActionButton*)button{
    if (![button isSelected]) {
        [button setSelected:YES];
        button.productObj.productFavStatus=@"1";
        [button setImage:[UIImage imageNamed:kImageHeartIconwhite] forState:UIControlStateNormal];
        [button setBackgroundColor:[self getColorForRedText]];
        
        if([notloggedIn intValue]==1)
        {
            
            //If Browse option, get the products from local storage if any and add clicked product to locakl storage.
            NSMutableArray *favoriteOfflineArray = [[NSMutableArray alloc]initWithObjects:button.productObj.productImageURL,button.productObj.productName,button.productObj.productSize,button.productObj.productPrice,button.productObj.productId,button.productObj.productStatus,button.productObj.productSalesPrice,nil];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            NSArray *defaultArray = [userDefaults objectForKey:@"offlineFavoriteArray"];
            if([defaultArray count]==0)
               [appDelegate.globalFavoriteOfflineArray addObject:favoriteOfflineArray];
            else
            {
              appDelegate.globalFavoriteOfflineArray = [[userDefaults objectForKey:@"offlineFavoriteArray"]mutableCopy];
              [appDelegate.globalFavoriteOfflineArray addObject:favoriteOfflineArray];
            }
            
            //Saving Favorites global Array in NSUserDefaults
            [self saveFavoriteofflineArray:appDelegate.globalFavoriteOfflineArray:@"offlineFavoriteArray"];
            
            // Add cart object to local storage when click on browse option
            [productBrowseArray addObject:button.productObj];
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:productBrowseArray];
            [currentDefaults setObject:data forKey:@"productObjectFavorites"];
        }
        else
        [self CallApiAddFavoriteWithProductID:button.productObj.productId];
    }
    else{
             
        [button setSelected:NO];
        button.productObj.productFavStatus=@"0";
        [button setImage:[UIImage imageNamed:kImageHeartIconGrey] forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor clearColor]];
        if([notloggedIn intValue]==1)
        {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            appDelegate.globalFavoriteOfflineArray = [[userDefaults objectForKey:@"offlineFavoriteArray"]mutableCopy];
            int productID = [button.productObj.productId intValue];
            for(int i=0;i<[appDelegate.globalFavoriteOfflineArray count];i++)
            {
                NSArray *individualArray = [appDelegate.globalFavoriteOfflineArray objectAtIndex:i];
                if([[individualArray objectAtIndex:4]intValue] == productID)
                {
                    [appDelegate.globalFavoriteOfflineArray removeObjectAtIndex:i];
                    [self saveFavoriteofflineArray:appDelegate.globalFavoriteOfflineArray :@"offlineFavoriteArray"];
                    break;
                }
                else
                {}
  
            }
            [self saveFavoriteofflineArray:appDelegate.globalFavoriteOfflineArray:@"offlineFavoriteArray"];
        }
        else
           [self CallApiRemoveFavoriteWithProductID:button.productObj.productId];
    }
    
}
-(void)saveFavoriteofflineArray:(NSArray *)offArray :(NSString *)saveKey
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:offArray forKey:saveKey];
    [defaults synchronize];
}
/***This is called when the user add any particular item to Cart ****/

-(void)cartButtonClicked:(UIActionButton*)button{
    
    NSArray *temparray=[NSArray arrayWithObjects:button.productObj, nil];
    NSLog(@"Temp Array = %@",temparray);
    if (![button isSelected]) {
        NSString *msg = [[ShoppingCart sharedInstace] addProductToMyShoppingCart:temparray];
        if ([msg isEqualToString:kAlertShoppingCart]) {
            [button setSelected:YES];
            [button setImage:[UIImage imageNamed:kImageCartIconwhite] forState:UIControlStateNormal];
            [button setBackgroundColor:[self getColorForYellowText]];
          /*  if([notloggedIn intValue]== 1)
            {
                [cartLocalArray addObject:button.productObj.productId];
                [self saveFavoriteofflineArray:cartLocalArray:@"cartlocal"];
                
                [cartBrowseArray addObject:button.productObj];
                NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:cartBrowseArray];
                [currentDefaults setObject:data forKey:@"productObject"];
              //  [self saveFavoriteofflineArray:temparray:@"productObject"];
                
            } */
            // [arrayForBoolCart replaceObjectAtIndex:button.tag withObject:[NSNumber numberWithBool:YES]];
        }
        NSMutableArray   *searchResult=[[NSMutableArray alloc] initWithArray:[[ShoppingCart sharedInstace] fetchProductFromShoppingCart]] ;
        if(searchResult.count>0){
            [self showAlertViewWithTitle:nil message:msg];
        }
        else{
            [self showAlertViewWithTitle:nil message:msg];
        }
    }
    else{
        [button setSelected:NO];
        [button setImage:[UIImage imageNamed:kImagecarticongrey] forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor clearColor]];
       /* if([notloggedIn intValue]==1)
        {
            [cartLocalArray removeObject:button.productObj.productId];
            [self saveFavoriteofflineArray:cartLocalArray :@"cartlocal"];
        } */
        // [arrayForBoolCart replaceObjectAtIndex:button.tag withObject:[NSNumber numberWithBool:NO]];
        [[ShoppingCart sharedInstace] removeProductFromMyShoppingCart:temparray];
    }
}

#pragma mark-_transparentView
/***This is called when the user select any particular Price range item on filter pop up ****/

-(void)priceRangeButtonClicked:(UIButton*)sender{
    
    UIView *thisView = (UIView*)sender.superview;
    DLog(@"%@",thisView.subviews);
    for (UIButton *btn in [thisView subviews] ) {
        if ([btn isKindOfClass:[UIButton class]]) {
            [btn setSelected:NO];
            [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [btn setBackgroundColor:[UIColor clearColor]];
        }
    }
    [sender setSelected:YES];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sender setBackgroundColor:[UIColor colorWithRed:0.9098 green:0.1294 blue:0.2078 alpha:1.0]];
    
    
    
    if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTBeer]) {
        if ([sender tag]==0) {
            PriceRange =@"$0.00-$9.99";
        }
        else if ([sender tag]==1){
            PriceRange =@"$10.00-$99.99";
            
        }
        else{
            PriceRange =@"$20.00-";
        }
        
    }
    else if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine]){
        if ([sender tag]==0) {
            PriceRange =@"$0.00-$19.99";
        }
        else if ([sender tag]==1){
            PriceRange =@"$20.00-$49.99";
            
        }
        else{
            PriceRange =@"$50.00-";
        }
        
    }
    else{
        if ([sender tag]==0) {
            PriceRange =@"$0.00-$19.99";
        }
        else if ([sender tag]==1){
            PriceRange =@"$20.00-$249.99";
            
        }
        else{
            PriceRange =@"$250.00-";
        }
        
    }
}
/***This is called when the user click search button on filter pop up ****/

-(void)FilterItems:(id)sender{
    [self.transparentView removeFromSuperview];
    DLog(@"searchItemString --- %@",searchItemString);
    DLog(@"FilteriTemsArray --- %@",FilteriTemsArray);
    [ItemArray removeAllObjects];
    
    if ([self.self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTBeer]) {
        [self CallApiSearchBeerWithSearchItem:_SearchTextField.text :FilteriTemsArray :1];
    }
    else if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine]){
        [self CallApiSearchWineWithSearchItem:_SearchTextField.text :FilteriTemsArray :1];
    }
    else{
        [self CallApiSearchLiquorWithSearchItem:_SearchTextField.text :FilteriTemsArray :1];
    }
}
-(void)refreshFilterScreen{
    arrayForBool=[[NSMutableArray alloc]init];
    if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTBeer]) {
        FilteriTemsArray=[[NSMutableArray alloc]initWithObjects:
                          @"Type",
                          @"Size",
                          nil];
    }
    else if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine]){
        FilteriTemsArray=[[NSMutableArray alloc]initWithObjects:
                          @"Type",
                          @"Size",
                          @"Country",
                          @"Region",
                          @"Varietal",
                          nil];
    }
    else{
        FilteriTemsArray=[[NSMutableArray alloc]initWithObjects:
                          @"Type",
                          @"Size",
                          nil];
    }
    for (int i=0; i<[FilteriTemsArray count]; i++) {
        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    }
    
}
- (void)closeFilterTapRecognizer:(UITapGestureRecognizer*)sender {
    UIView *view = sender.view;
    NSLog(@"%ld", (long)view.tag);//By tag, you can find out where you had tapped.
    [self.transparentView removeFromSuperview];
}
-(void)addTransParentView{
    [self refreshFilterScreen];
    _transparentView=[self createContentViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    UITapGestureRecognizer *closeFilterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeFilterTapRecognizer:)];
    [_transparentView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_transparentView];
    [_transparentView addGestureRecognizer:closeFilterTapRecognizer];
    UIImageView *transparentImageView=[self createImageViewWithRect:CGRectMake(0, 0, SCREEN_WIDTH
                                                                               , SCREEN_HEIGHT) image:nil];
    [_transparentView addSubview:transparentImageView];
    [transparentImageView setBackgroundColor:[UIColor colorWithRed:0.05 green:0.05 blue:0.05 alpha:0.5]];
    CGRect expandableTableViewFrame;
    
    if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine]){
        expandableTableViewFrame=CGRectMake(kPaddingSmall, kNavigationheight+kBottomViewHeight+kTopPadding,SCREEN_WIDTH-(kPaddingSmall*2),kContainerviewHeightWine);
    }
    else{
        expandableTableViewFrame=CGRectMake(kPaddingSmall, kNavigationheight+kBottomViewHeight+kTopPadding,SCREEN_WIDTH-(kPaddingSmall*2),kContainerviewHeight);
    }
    self.expandableTableView = [[UITableView alloc] initWithFrame:expandableTableViewFrame
                                
                                                            style:UITableViewStylePlain];
    self.expandableTableView .separatorStyle=UITableViewCellSeparatorStyleNone;
    [_expandableTableView setBackgroundColor:[UIColor clearColor]];
    _expandableTableView.dataSource=self;
    _expandableTableView.delegate=self;
    [_expandableTableView setTag:k_expandableTableViewTag];
    [self.transparentView addSubview:self.expandableTableView];
    
    [self.expandableTableView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.expandableTableView.layer setShadowOpacity:0.8];
    [self.expandableTableView.layer setShadowRadius:2.0];
    [self.expandableTableView.layer setShadowOffset:CGSizeMake(2.0, 1.0)];
}
#pragma mark-textfield delegate method
- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:
(NSString *)string {
    if (textField==self.SearchTextField) {
        if (  range.location==0 && [string isEqualToString: @" "] ) {
            return NO;//This restrict user from entering blank at the beggining of text field.
        }
        DLog(@"FilteriTemsArray========%@",FilteriTemsArray);
        if (range.location==0 && range.length==1 && [string isEqualToString:kEmptyString]) {
            [ItemArray removeAllObjects];  // this reset the datasource.
            if ([self.self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTBeer]) {
                [self CallApiSearchBeerWithSearchItem:kEmptyString :FilteriTemsArray :1];
            }
            else if ([self.searchItemString isEqualToString:kNavigationTitleFINDAPRODUCTWine]){
                [self CallApiSearchWineWithSearchItem:kEmptyString :FilteriTemsArray :1];
            }
            else{
                [self CallApiSearchLiquorWithSearchItem:kEmptyString :FilteriTemsArray :1];
            }
        }
    }

    return YES;
}
@end