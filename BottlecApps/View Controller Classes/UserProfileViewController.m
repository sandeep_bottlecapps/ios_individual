//
//  UserProfileViewController.m
//  BottlecApps
//
//  Created by Kuldeep Tyagi on 8/4/15.
//  Copyright (c) 2015 Kuldeep Tyagi. All rights reserved.
//

#import "UserProfileViewController.h"
#import "EditAccountViewController.h"
#import "Constants.h"

#define klabelWidth_userProfile  (SCREEN_WIDTH-(kLeftPadding+kLabelWidthSmall+10))
CGRect textRect1;
int numberofLines;
CGSize labelSize;
@implementation UserProfileViewController
@synthesize streetLabel;
-(void)UpdateUIWithAddress{
    __weak UserProfile *userProfileObj = kgetUserProfileModel;
    
    if (![userProfileObj.UserImage isEqualToString:kEmptyString]) {
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:userProfileObj.UserImage]];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            UIImage  *userImage = [UIImage imageWithData:imageData];
            [self.addAPhotoButton setImage: userImage forState:UIControlStateNormal];
        });
    }
    else{
        [self.addAPhotoButton setImage: [UIImage imageNamed:kImage_PlaceholderProfile] forState:UIControlStateNormal];
        
    }
    /************************add button for profile imageview***************/
    
    /**************adding label for user name********************/
    self.UserNameLabel.text=[NSString stringWithFormat:@"%@ %@",[userProfileObj FirstName],[userProfileObj LastName]];
    /**************adding label for user name********************/
    
    /**************adding label for user DOB********************/
    NSString *stringToFormat = [userProfileObj DateOfBirth];
    //
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    NSDate *date = [dateFormatter dateFromString:stringToFormat];
    
    [dateFormatter setDateFormat:@"MMM dd,yyyy"];
    
    [dateFormatter stringFromDate:date];
    
    self.DateOfBirthLabel.text=[dateFormatter stringFromDate:date];
    [self.CustomScrollView addSubview:self.DateOfBirthLabel];
    /**************adding label for user DOB********************/
    
    /**************adding label for card number********************/
    if([loyaltyEnable intValue]== 1)
    {
    if (![[userProfileObj UserLoyalityCardNo]isEqualToString:kEmptyString]) {
        self.CardNumberLabel.text=[NSString stringWithFormat:@"Card No. %@",[userProfileObj UserLoyalityCardNo]];
    }
    else {
        self.CardNumberLabel.text=kEmptyCardNumber;
    }
    self.CardNumberLabel.backgroundColor=[self getColorForCardNo];
    [self.CustomScrollView addSubview:self.CardNumberLabel];
    }
    else
    {}
    /**************adding label for card number********************/
    
    
    /**************adding label for address********************/
    [self.CustomScrollView addSubview:self.addressLabel];
    /**************adding label for address********************/
    /*****Add Street Name Textfield*******/
    
    
    if (![[userProfileObj Address]isEqualToString:kEmptyString]) {
         if (![[userProfileObj Address2]isEqualToString:kEmptyString]){
        NSString *address=[NSString stringWithFormat:@"%@, %@ ",[userProfileObj Address],[userProfileObj Address2]];
               self.streetLabel.text=[NSString stringWithFormat:@"%@",address];
             self.streetLabel.numberOfLines=0;
              numberofLines = [self lineCountForLabel:self.streetLabel];
             NSStringDrawingContext *ctx = [NSStringDrawingContext new];
             NSAttributedString *aString = [[NSAttributedString alloc] initWithString:self.streetLabel.text];
             UILabel *calculationView = [[UILabel alloc] init];
             [calculationView setAttributedText:aString];
             textRect1 = [calculationView.text boundingRectWithSize:self.view.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:calculationView.font} context:ctx];
             
      
        }
         else if (([[userProfileObj Address2]isEqualToString:kEmptyString])){
             
            NSString  *address=[NSString stringWithFormat:@"%@ %@ ",[userProfileObj Address],@""];
             self.streetLabel.text=[NSString stringWithFormat:@"%@",address];

              self.streetLabel.numberOfLines=0;
              numberofLines = [self lineCountForLabel:self.streetLabel];
            // NSStringDrawingContext *ctx = [NSStringDrawingContext new];
             NSAttributedString *aString = [[NSAttributedString alloc] initWithString:self.streetLabel.text];
             UILabel *calculationView = [[UILabel alloc] init];
             [calculationView setAttributedText:aString];
            textRect1 = [self sizeCountForLabel:self.streetLabel];
            
        }
    }
    else{
        self.streetLabel.text = kEmptyAddress;
    }
    [self.CustomScrollView addSubview:self.streetLabel];
    [self.streetLabel sizeToFit];
    
    /*****Add state Name Textfield*******/
    if (![[userProfileObj City]isEqualToString:kEmptyString]) {
        
        NSString *address=[NSString stringWithFormat:@"%@, %@ ",[userProfileObj City],[userProfileObj State]];
        self.stateLabel.text=[NSString stringWithFormat:@"%@ %@",address,[userProfileObj ZipCode]];
       // self.apartment.text=[NSString stringWithFormat:@"%@",[userProfileObj Address2]];
    }
    else{
        self.stateLabel.text= kEmptyString;
    }
    
    [self.CustomScrollView addSubview:self.stateLabel];
    [self.stateLabel sizeToFit];
    
    self.phoneLabel.text=[self getformattedPhoneNumberstringFrom:[userProfileObj ContactNo]];
    [self.CustomScrollView addSubview:self.phoneLabel];
    
    /*****Add email Textfield*******/
    self.emailLabel.text=[userProfileObj EmailID];
    [self.CustomScrollView addSubview:self.emailLabel];
    /*****Add email Textfield*******/
}
-(void)UpdateUI{
       __weak UserProfile *userProfileObj = kgetUserProfileModel;
    /**************adding label for user DOB********************/
    NSString *stringToFormat = [userProfileObj DateOfBirth];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *date = [dateFormatter dateFromString:stringToFormat];
    [dateFormatter setDateFormat:@"MMM dd,yyyy"];
    [dateFormatter stringFromDate:date];
    self.DateOfBirthLabel.text=[dateFormatter stringFromDate:date];
    self.emailLabel.text=[userProfileObj EmailID];
}

- (CGRect)sizeCountForLabel:(UILabel *)label {
    
    CGSize constrain = CGSizeMake(label.bounds.size.width, FLT_MAX);
    // CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constrain lineBreakMode:NSLineBreakByWordWrapping];
   /* CGSize size = [label.text sizeWithFont:label.font
                         constrainedToSize:constrain  // - 40 For cell padding
                             lineBreakMode:NSLineBreakByWordWrapping]; */
    
    CGRect labelRect =  [label.text boundingRectWithSize:constrain
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:label.font}
                                                 context:nil];
    CGSize size = labelRect.size;
    return CGRectMake(0, 0, size.width, size.height);
   
}

- (int)lineCountForLabel:(UILabel *)label {
    
    CGSize constrain = CGSizeMake(label.bounds.size.width, FLT_MAX);
   // CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constrain lineBreakMode:NSLineBreakByWordWrapping];
  /*  CGSize size = [label.text sizeWithFont:label.font
                  constrainedToSize:constrain  // - 40 For cell padding
                      lineBreakMode:NSLineBreakByWordWrapping]; */
    
    CGRect labelRect =  [label.text boundingRectWithSize:constrain
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:label.font}
                                                 context:nil];
    CGSize size = labelRect.size;

    return ceil(size.height / label.font.lineHeight);
}

-(void)createUIWithAddress{
    [self addScrollView];
    [self.CustomScrollView setDelegate:self];
    
    if (IS_IPHONE_4_OR_LESS) {
        [self.CustomScrollView setContentSize:CGSizeMake(kCustomScrollViewWidth, kCustomScrollViewHeight+100)];
    }
    else{
        [self.CustomScrollView setContentSize:CGSizeMake(kCustomScrollViewWidth, kCustomScrollViewHeight)];
    }

    /**************adding imageview for coverphoto********************/
    self.CoverPhotoImageView=[self createImageViewWithRect:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT/3) image:[UIImage imageNamed:kImageProfileImageBackground]];
    [self.CustomScrollView addSubview:self.CoverPhotoImageView];
    /************************add button for profile imageview***************/
    
    [self CreateAddAphotoButton];
    [self.CustomScrollView addSubview:self.addAPhotoButton];
    [self.addAPhotoButton setTitle:kEmptyString forState:UIControlStateNormal];
    [self.addAPhotoButton setBackgroundColor:[UIColor clearColor]];
    /************************add button for profile imageview***************/
    
    /**************adding label for user name********************/
    self.UserNameLabel=[self createLblWithRect:CGRectMake(0, (self.addAPhotoButton.frame.origin.y+self.addAPhotoButton.frame.size.height)+kPaddingSmall, SCREEN_WIDTH, kLabelHeight + 3) font:[UIFont fontWithName:kFontBold size:kFontUserName] text:kEmptytextfield textAlignment:NSTextAlignmentCenter textColor:[self getColorForRedText]];
    [self.CustomScrollView addSubview:self.UserNameLabel];
    /**************adding label for user name********************/
    
    /**************adding label for user DOB********************/
    self.DateOfBirthLabel=[self createLblWithRect:CGRectMake(0, (self.UserNameLabel.frame.origin.y+self.UserNameLabel.frame.size.height), SCREEN_WIDTH, kLabelHeight*2) font:[UIFont fontWithName:kFontSemiBold size:kFontDOB] text:kEmptytextfield textAlignment:NSTextAlignmentCenter textColor:[self getColorForBlackText]];
    [self.CustomScrollView addSubview:self.DateOfBirthLabel];
    /**************adding label for user DOB********************/

    self.CardNumberLabel=[self createLblWithRect:CGRectMake(0, (self.CoverPhotoImageView.frame.origin.y+self.CoverPhotoImageView.frame.size.height), SCREEN_WIDTH, kTEXTfieldHeight) font:[self addFontSemiBold] text:kEmptyCardNumber textAlignment:NSTextAlignmentCenter textColor:[UIColor whiteColor]];
    /**************adding label for card number********************/
    self.CardNumberLabel.backgroundColor=[self getColorForCardNo];
    [self.CustomScrollView addSubview:self.CardNumberLabel];
  
    if([loyaltyEnable intValue] == 0)
    {
        self.CardNumberLabel.hidden = YES;
    }
    /**************adding label for card number********************/
    
    /**************adding label for address********************/
        if (IS_IPHONE_4_OR_LESS) {
        self.addressLabel=[self createLblWithRect:CGRectMake(kLeftPadding, (self.CardNumberLabel.frame.origin.y+self.CardNumberLabel.frame.size.height)+kPaddingSmall, SCREEN_WIDTH-100, kLabelHeight) font:[self addFontSemiBold] text:kLabelAddress textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
            
        [self.CustomScrollView addSubview:self.addressLabel];
        }
    else{
        self.addressLabel=[self createLblWithRect:CGRectMake(kLeftPadding, (self.CardNumberLabel.frame.origin.y+self.CardNumberLabel.frame.size.height)+kTopPadding, SCREEN_WIDTH-100, kLabelHeight) font:[self addFontSemiBold] text:kLabelAddress textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
      
        [self.CustomScrollView addSubview:self.addressLabel];
       
    }
    /**************adding label for address********************/
    /*****Add Street Name Textfield*******/
   
     self.streetLabel=[self createLblWithRect:CGRectMake(kLeftPadding, (self.addressLabel.frame.origin.y+self.addressLabel.frame.size.height), SCREEN_WIDTH-58, kLabelHeight) font:[self addFontSemiBold] text:kEmptyAddress textAlignment:NSTextAlignmentLeft
                                   textColor:[UIColor colorWithRed:0.5020 green:0.5020 blue:0.5098 alpha:1.0]];
    
    

    [self.CustomScrollView addSubview:self.streetLabel];
     NSLog(@" previous height%f",(self.streetLabel.frame.size.height));
    self.stateLabel=[self createLblWithRect:CGRectMake(kLeftPadding, (self.streetLabel.frame.origin.y+self.streetLabel.frame.size.height), SCREEN_WIDTH-58, kLabelHeight) font:[self addFontSemiBold] text:kEmptyString textAlignment:NSTextAlignmentLeft
                                  textColor:[UIColor colorWithRed:0.5020 green:0.5020 blue:0.5098 alpha:1.0]];
    /*****Add state Name Textfield*******/
    [self.CustomScrollView addSubview:self.stateLabel];
    
    /*****Add state Name Textfield*******/
    /**************adding label contact address title********************/
    self.contactLabel=[self createLblWithRect:CGRectMake(kLeftPadding, (self.stateLabel.frame.origin.y+self.stateLabel.frame.size.height)+kPaddingSmall, SCREEN_WIDTH-100, kLabelHeight) font:[self addFontSemiBold] text:kLabelContact textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
    [self.CustomScrollView addSubview:self.contactLabel];
    /*****Add phone number Textfield*******/
    self.phoneLabel=[self createLblWithRect:CGRectMake(kLeftPadding, (self.contactLabel.frame.origin.y+self.contactLabel.frame.size.height)+kPaddingSmall, SCREEN_WIDTH-100, kLabelHeight) font:[self addFontSemiBold] text:kEmptytextfield textAlignment:NSTextAlignmentLeft textColor:[UIColor colorWithRed:0.5020 green:0.5020 blue:0.5098 alpha:1.0]];
    [self.CustomScrollView addSubview:self.phoneLabel];
    /*****Add phone number Textfield*******/
    
    /*****Add email Textfield*******/
    self.emailLabel=[self createLblWithRect:CGRectMake(kLeftPadding, (self.phoneLabel.frame.origin.y+self.phoneLabel.frame.size.height)+kPaddingSmall, SCREEN_WIDTH-58, kLabelHeight) font:[self addFontSemiBold] text:kEmptytextfield textAlignment:NSTextAlignmentLeft textColor:[UIColor colorWithRed:0.5020 green:0.5020 blue:0.5098 alpha:1.0]];
    [self.CustomScrollView addSubview:self.emailLabel];
    /*****Add email Textfield*******/
    
    /**************adding button for  edit********************/
    CGRect ContentainerViewFrame ;
    ContentainerViewFrame.origin.x=0;
    ContentainerViewFrame.origin.y=SCREEN_HEIGHT-kBottomViewHeight-kCustomButtonContainerHeight;
    ContentainerViewFrame.size.width=SCREEN_WIDTH;
    ContentainerViewFrame.size.height=kCustomButtonContainerHeight;
    self.EditButton=  [self AddCustomButtonInsideContentViewWithFrame:ContentainerViewFrame];
    [self.EditButton setTitle:kButtonTittleEDIT forState:UIControlStateNormal];
    [self.EditButton addTarget:self action:@selector(EditButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self UpdateUIWithAddress];
 if(numberofLines>1){
     CGFloat labelHeight = textRect1.size.height;
     
     self.streetLabel.frame=CGRectMake(kLeftPadding, (self.addressLabel.frame.origin.y+self.addressLabel.frame.size.height), SCREEN_WIDTH-58, labelHeight);
          NSLog(@"%f",(self.streetLabel.frame.size.height));
     
     self.stateLabel.frame=CGRectMake(kLeftPadding, (self.streetLabel.frame.origin.y+self.streetLabel.frame.size.height), SCREEN_WIDTH-58, kLabelHeight);
    // self.stateLabel.text=kEmptyString;
     
    self.contactLabel .frame=CGRectMake(kLeftPadding, (self.stateLabel.frame.origin.y+self.stateLabel.frame.size.height)+kPaddingSmall, SCREEN_WIDTH-100, kLabelHeight);
    //self.contactLabel.text=kLabelContact;
     
     
     self.phoneLabel .frame=CGRectMake(kLeftPadding, (self.contactLabel.frame.origin.y+self.contactLabel.frame.size.height)+kPaddingSmall, SCREEN_WIDTH-100, kLabelHeight);
    // self.phoneLabel.text=kEmptytextfield;
     
     self.emailLabel .frame=CGRectMake(kLeftPadding, (self.phoneLabel.frame.origin.y+self.phoneLabel.frame.size.height)+kPaddingSmall, SCREEN_WIDTH-100, kLabelHeight);
   //  self.emailLabel.text=kEmptytextfield;

//     
//              /**************adding button for  edit********************/
//        CGRect ContentainerViewFrame ;
//        ContentainerViewFrame.origin.x=0;
//        ContentainerViewFrame.origin.y=SCREEN_HEIGHT-kBottomViewHeight-kCustomButtonContainerHeight;
//        ContentainerViewFrame.size.width=SCREEN_WIDTH;
//        ContentainerViewFrame.size.height=kCustomButtonContainerHeight;
//        self.EditButton=  [self AddCustomButtonInsideContentViewWithFrame:ContentainerViewFrame];
//        [self.EditButton setTitle:kButtonTittleEDIT forState:UIControlStateNormal];
//        [self.EditButton addTarget:self action:@selector(EditButton:) forControlEvents:UIControlEventTouchUpInside];
     
        
       
    }
}





-(void)createUI{
    [self addScrollView];
    [self.CustomScrollView setDelegate:self];
    if (IS_IPHONE_4_OR_LESS) {
        [self.CustomScrollView setContentSize:CGSizeMake(kCustomScrollViewWidth, kCustomScrollViewHeight+100)];
    }
    else{
        [self.CustomScrollView setContentSize:CGSizeMake(kCustomScrollViewWidth, kCustomScrollViewHeight)];
    }

    /*****Add email Textfield*******/
    UILabel *emailTitleLabel=[self createLblWithRect:CGRectMake(kLeftPadding, kTopPadding,kLabelWidthSmall, kLabelHeight) font:[self addFontBold] text:@"EMAIL:" textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
     [self.CustomScrollView addSubview:emailTitleLabel];
    
    self.emailLabel=[self createLblWithRect:CGRectMake(emailTitleLabel.frame.size.width+emailTitleLabel.frame.origin.x, kTopPadding, klabelWidth_userProfile, kLabelHeight) font:[self addFontSemiBold] text:kEmptytextfield textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
    [self.CustomScrollView addSubview:self.emailLabel];
    /*****Add email Textfield*******/
    
    /**************adding label for user DOB********************/
    UILabel *dobTitleLabel=[self createLblWithRect:CGRectMake(kLeftPadding, (emailTitleLabel.frame.size.height+emailTitleLabel.frame.origin.y),kLabelWidthSmall, kLabelHeight) font:[self addFontBold] text:@"DOB:" textAlignment:NSTextAlignmentLeft textColor:[self getColorForRedText]];
    [self.CustomScrollView addSubview:dobTitleLabel];
    self.DateOfBirthLabel=[self createLblWithRect:CGRectMake(dobTitleLabel.frame.size.width+dobTitleLabel.frame.origin.x, (emailTitleLabel.frame.size.height+emailTitleLabel.frame.origin.y), klabelWidth_userProfile, kLabelHeight) font:[self addFontSemiBold] text:kEmptytextfield textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
    [self.CustomScrollView addSubview:self.DateOfBirthLabel];
    //    /**************adding label for user DOB********************/
    
    
    /**************adding button for  edit********************/
    self.EditButton=[self createButtonWithFrame:CGRectMake(kLeftPadding, dobTitleLabel.frame.size.height+dobTitleLabel.frame.origin.y+kTopPadding,SCREEN_WIDTH-(kLeftPadding*2), kButtonHeight) withTitle:@"UPDATE YOUR PROFILE"];
    [self.EditButton setTitleColor:[self getColorForRedText] forState:UIControlStateNormal];
    [self.EditButton.titleLabel setFont:[UIFont fontWithName:kFontBold size:kFontBoldSize]];
    [[self.EditButton layer] setBorderWidth:1.0f];
    [[self.EditButton layer] setBorderColor:[self getColorForRedText].CGColor];
    [self.EditButton addTarget:self action:@selector(EditButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.CustomScrollView addSubview:self.EditButton];

     [self UpdateUI];
  
}

#pragma mark-viewlifeCycle

-(void)CallApiMyaccount{
    
    __weak User *userObj = [[ModelManager modelManager] getuserModel];
    NSMutableDictionary* paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [paramDict setObject:userObj.userId forKey: kParamUserId];
    [self showSpinnerToSuperView:self.view]; // To show spinner
    [[ModelManager modelManager] ViewProfileWithParam:paramDict successStatus:^(BOOL status) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hideSpinner];
            UserProfile *userProfile = kgetUserProfileModel;
            DLog(@"IsProfileUpdated=====%@",userProfile.IsProfileUpdated)

            if ([userProfile.IsProfileUpdated isEqualToString:@"1"]) {
                 [self createUIWithAddress];
            }
            
            else{
                EditAccountViewController *editaccountVC=[self viewControllerWithIdentifier:kEditAccountViewControllerStoryboardId];
                [self.navigationController pushViewController:editaccountVC animated:NO];

            
            }
           
        });
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        //[self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
    }];
}

-(void)refreshPage:(id)sender{
    
//   if([notloggedIn isEqualToString:@"1"])
//    [self performSegueWithIdentifier:kLoginViewControllerStoryboardId sender:nil];
//    else
    [self CallApiMyaccount];
   
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.view=nil;
    [self refreshPage:nil];

}


-(void)viewDidLoad{
    [super viewDidLoad];
    
    //  [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshPage:) name:kUserProfileViewControllerStoryboardId object:nil];
}


#pragma mark - SlideNavigationController Methods -
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu  {
    return YES;
}
#pragma mark-
-(void)EditButton:(UIButton*)button
{
    [self performSegueWithIdentifier:kGoToEditAccountViewControllerSegueId sender:nil];
    
}

@end
