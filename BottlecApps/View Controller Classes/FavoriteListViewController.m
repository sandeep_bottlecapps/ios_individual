//
//  FavoriteListViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 8/25/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "FavoriteListViewController.h"
#import "ProductDetailsViewController.h"
#import "QALabel.h"
#import "SearchResultViewController.h"

@interface FavoriteListViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSURL               *productImageURL;
    int                 pagenumber;
    int                 totalCount;
    NSMutableArray             *ItemArray;
    UILabel *NorecordFoundLabel;
}
@end

@implementation FavoriteListViewController
-(void)UpdateUI{
    DLog(@"ItemArray----%@",ItemArray);
    if (![ItemArray count]) {
        
        [self.defaultTableView reloadData];
        
    }
    else{
        for (Product *productObj in ItemArray) {
            if ([[productObj productStatus] intValue]==0) {
                [self.arrayForBoolProductStatus addObject:@"0"];
            }
            else{
                [self.arrayForBoolProductStatus addObject:@"1"];
            }
        }
        
        [self.defaultTableView reloadData];
        
        DLog(@"self.arrayForBoolProductStatus-----------%@",self.arrayForBoolProductStatus);
        
    }
    if([ItemArray count] == 0)
        [self addNorecordFoundLAbelOnTableView:self.defaultTableView];
    else{
        [NorecordFoundLabel removeFromSuperview];
    }
}
-(void)addNorecordFoundLAbelOnTableView:(UITableView*)tableView{
    NorecordFoundLabel = [[UILabel alloc] init];
    [NorecordFoundLabel setBackgroundColor:[UIColor clearColor]];
    [NorecordFoundLabel setTextColor:[self getColorForBlackText]];
    [NorecordFoundLabel setText:kAlertNoRecordFound];
    [NorecordFoundLabel sizeToFit];
    NorecordFoundLabel.frame = CGRectMake((tableView.bounds.size.width - NorecordFoundLabel.bounds.size.width) / 2.0f,
                                          50,
                                          NorecordFoundLabel.bounds.size.width,
                                          NorecordFoundLabel.bounds.size.height);
    [tableView insertSubview:NorecordFoundLabel atIndex:0];
    
}

-(void)createUI{
    DLog(@"ItemArray----%@",ItemArray);
    
    // Do any additional setup after loading the view.
    CGRect ContentainerViewFrame ;
    ContentainerViewFrame.origin.x=0;
    ContentainerViewFrame.origin.y=SCREEN_HEIGHT-kBottomViewHeight-kCustomButtonContainerHeight-kPaddingSmall;
    ContentainerViewFrame.size.width=SCREEN_WIDTH;
    ContentainerViewFrame.size.height=kCustomButtonContainerHeight;
    UIButton *AddMoreButton;
    AddMoreButton=  [self AddCustomButtonInsideContentViewWithFrame:ContentainerViewFrame];
    
    [AddMoreButton setTitle:kButtonTitleADDMOREITEMS forState:UIControlStateNormal];
    [AddMoreButton addTarget:self action:@selector(AddMoreButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self AddDefaultTableView];
    [self.defaultTableView setDelegate:self];
    [self.defaultTableView setDataSource:self];
    
}

-(void)callApiViewFavoriteWithPagenumber:(int)page{
    User *userObj=kGetUserModel;
    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
    [paramDict setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
    [paramDict setObject:[NSString stringWithFormat:@"%d",page] forKey: kParamPageNumber];
    [paramDict setObject:@"10" forKey: kParamPageSize];
    [paramDict setObject:userObj.userId forKey: kParamUserId];
    [paramDict setObject:@"Product" forKey: kParamTabName];
    
    [self showSpinner];
    [[ModelManager modelManager] ViewFavoriteWithParam:paramDict  successStatus:^(APIResponseStatusCode statusCode, id response) {
      //  NSLog(@"response=====%@", response);
        [self hideSpinner];
        @try {
            for (NSDictionary *dict in [response valueForKey:@"productCount"]) {
                Product *productObj=[[Product alloc] initWithDictionary:dict];
                [ItemArray addObject:productObj];
            }
            totalCount=[[response valueForKey:@"totalNoOfRecords"] intValue];
            [self UpdateUI];
        }
        @catch (NSException *exception) {
            [self showAlertViewWithTitle:kError message:kInternalInconsistencyErrorMessage];
            
        }
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
        
    }];
}

#pragma mark-viewcontrollerlifecycle.
-(void)viewWillAppear:(BOOL)animated{
    [self refreshPage:nil];
}

-(void)refreshPage:(id)sender{
    self.arrayForBoolProductStatus=[NSMutableArray new];
    if(ItemArray)
        [ItemArray removeAllObjects];
    
    ItemArray       = [NSMutableArray new];
    
    pagenumber=1;
    totalCount=0;
    [self callApiViewFavoriteWithPagenumber:pagenumber];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshPage:) name:kFavoritesViewControllerStoryboardId object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)CallApiRemoveFavoriteWithProductID:(NSString*)productId{
    __weak User *userObj=kGetUserModel;
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setValue:kSTOREIDbottlecappsString forKey:kParamStoreId];
    [dict setValue:userObj.userId forKey:kParamAuthenticationID];
    [dict setValue:@"Product" forKey:kParamTabName];
    [dict setValue:productId forKey:kParamProductID];
    //kParamTabName
    [self showSpinner];
    [[ModelManager modelManager] RemoveFavoriteWithParam:dict  successStatus:^(APIResponseStatusCode statusCode, id response) {
      //  NSLog(@"%@", response);
        [self hideSpinner];
        @try {
          //  NSLog(@"%@", response);
            if ([[response valueForKey:@"Message"] boolValue]==TRUE) {
                Product *product = nil;
                
                for (Product *productObj in ItemArray) {
                    NSString *productIDForCheck=[productObj productId];
                    DLog(@"productIDForCheck=%@",productIDForCheck);
                    if ([productIDForCheck isEqualToString:productId]) {
                        product = productObj;
                        break;
                    }
                }
                if (product)    {
                    [ItemArray removeObject:product];
                    [self showAlertViewWithTitle:nil message:kAlertRemoveFavorite];
                    totalCount--;
                    [self.defaultTableView reloadData];
                    if(totalCount == 0)
                        [self addNorecordFoundLAbelOnTableView:self.defaultTableView];
                }
                else    {
                    [self showAlertViewWithTitle:nil message:kAlertNotRemoveFavorite];
                }
            }
            else {
                [self showAlertViewWithTitle:nil message:kAlertNotRemoveFavorite];
            }
        }
        @catch (NSException *exception) {
            [self showAlertViewWithTitle:kError message:kInternalInconsistencyErrorMessage];
        }
    } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
        [self hideSpinner];
        [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
        DLog(@"%@",[NSString stringWithFormat:@"%@\n", errorMessage]);
    }];
}


#pragma mark-BUTTON ACTION
-(void)FavButton:(UIActionButton*)button{
    DLog(@"ProductID==%@",button.productObj.productId);
    [button setSelected:NO];
    [button setImage:[UIImage imageNamed:kImageHeartIconGrey] forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor clearColor]];
    [self CallApiRemoveFavoriteWithProductID:button.productObj.productId];
}

-(void)cartButton:(UIActionButton*)button{
    NSArray *temparray=[NSArray arrayWithObjects:button.productObj, nil];
    if (![button isSelected]) {
        NSString *msg = [[ShoppingCart sharedInstace] addProductToMyShoppingCart:temparray];
        if ([msg isEqualToString:kAlertShoppingCart]) {
            [button setSelected:YES];
            [button setImage:[UIImage imageNamed:kImageCartIconwhite] forState:UIControlStateNormal];
            [button setBackgroundColor:[self getColorForYellowText]];
        }
        NSMutableArray   *searchResult=[[NSMutableArray alloc] initWithArray:[[ShoppingCart sharedInstace] fetchProductFromShoppingCart]] ;
        if(searchResult.count>0){
            [self showAlertViewWithTitle:nil message:msg];
        }
        else{
            [self showAlertViewWithTitle:nil message:msg];
        }
    }
    else{
        [button setSelected:NO];
        [button setImage:[UIImage imageNamed:kImagecarticongrey] forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor clearColor]];
        [[ShoppingCart sharedInstace] removeProductFromMyShoppingCart:temparray];
    }
}

-(void)AddMoreButton:(UIButton*)button{
    //    //[self performSegueWithIdentifier:kGoToSearchResultViewControllerSegueId sender:nil];
    //    SearchResultViewController *vc;
    //    vc=[self viewControllerWithIdentifier:@"GoToSearchResultViewControllerSegueId"];
    //   // vc.searchItemString=kNavigationTitleFINDAPRODUCTBeer;
    //    [self.navigationController pushViewController:vc animated:YES];
    UIViewController *vc = nil;
    [self performSegueWithIdentifier:kGoToSearchResultViewControllerSegueId sender:self];
    [self.navigationController pushViewController:vc animated:YES];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton*)button {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of ections.
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==[ItemArray count]) {
        return kButtonHeight;
    }
    else
        return krowHeightFavorite;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([ItemArray count]<totalCount && [ItemArray count]!=0) {
        return [ItemArray count]+1;
    }
    
    else{
        return [ItemArray count];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    static NSString *postCellId = @"postCell";
    static NSString *moreCellId = @"moreCell";
    NSUInteger row = [indexPath row];
    NSUInteger count = [ItemArray count];
    UITableViewCell *cell =nil;
    
    if (row == count && count != 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:moreCellId];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:moreCellId];
            cell.textLabel.text = kTapToloadMore;
            cell.textLabel.textColor = [self getColorForRedText];
            cell.textLabel.font = [self addFontRegular];
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
        }
        return cell;
    }
    
    else    {
        AsyncImageView *ProductImageView;
        QALabel*productNameLabel;
        UILabel*productSizeLabel;
        UILabel*productPriceLabel;
        UIView    *CartAndLikeView;
        UIActionButton  *cartButton;
        UIActionButton  * FavButton;
        UIView *seperatorV;
        UIView *bgv;
        UIView*  leftSeperatorV;
        UIImageView *separatorEnd;
        UIImageView* imgOutOfStock;
        cell = [tableView dequeueReusableCellWithIdentifier:postCellId];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:postCellId];
            bgv=[self createContentViewWithFrame:CGRectMake(0, 0, krowHeightFavorite-1, krowHeightFavorite-1)];
            [bgv setBackgroundColor:[UIColor whiteColor]];
            
            ProductImageView=[self createAsynImageViewWithRect:CGRectMake(3, 0, 73, 77) imageUrl:[NSURL URLWithString:kEmptyString] isThumnail:YES];
            ProductImageView.tag = kTagProductImageV;
            
            productNameLabel= [[QALabel alloc] initWithFrame:CGRectMake((bgv.frame.size.width)+(bgv.frame.origin.x)+5,3, kLabelWidth+24, kLabelHeight*2 - 6)];
            productNameLabel.font = [UIFont fontWithName:kFontSemiBold size:kFontSemiBoldSize-2];
            productNameLabel.textAlignment = NSTextAlignmentLeft;
            productNameLabel.textColor = [self getColorForRedText];
            productNameLabel.tag=kTagProductName;
            productNameLabel.numberOfLines=2;
            productNameLabel.backgroundColor = [UIColor clearColor];
            
            productSizeLabel=[self createLblWithRect:CGRectMake((bgv.frame.size.width)+(bgv.frame.origin.x)+5,(productNameLabel.frame.origin.y)+(productNameLabel.frame.size.height), kLabelWidth+24, kLabelHeight- 5) font:[self addFontRegular] text:kEmptyString textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
            productSizeLabel.tag=kTagProductSize;
            productSizeLabel.backgroundColor = [UIColor clearColor];
            
            productPriceLabel=[self createLblWithRect:CGRectMake((bgv.frame.size.width)+(bgv.frame.origin.x)+5, (productSizeLabel.frame.size.height)+(productSizeLabel.frame.origin.y), kLabelWidth/2 +24, kLabelHeight -5) font:[UIFont fontWithName:kFontBold size:kFontBoldSize-2] text:kEmptyString textAlignment:NSTextAlignmentLeft textColor:[self getColorForBlackText]];
            [productPriceLabel setTag:kTagProductPrice];
            [productPriceLabel setBackgroundColor:[UIColor clearColor]];
            
            CartAndLikeView=[self createContentViewWithFrame:CGRectMake(SCREEN_WIDTH-kCartAndLikeViewWidth, 0, kCartAndLikeViewWidth,krowHeightFavorite-kPaddingcartAndFav)];
            [CartAndLikeView setBackgroundColor:[UIColor clearColor]];
            [CartAndLikeView setTag:kTagCartAndLikeView];
            CartAndLikeView.layer.masksToBounds = YES;
            
            cartButton=[self createActionButtonWithFrame:CGRectMake(kCartAndLikeViewWidth/2,0, kCartAndLikeViewWidth/2,kCartLikeViewHeight ) withTitle:kEmptyString];
            cartButton.tag = kTagCartButton;
            
            [cartButton addTarget:self action:@selector(cartButton:) forControlEvents:UIControlEventTouchUpInside];
            leftSeperatorV=[self createContentViewWithFrame:CGRectMake(0,0, 1,kCartLikeViewHeight )];
            [leftSeperatorV setBackgroundColor:[UIColor clearColor]];
            [[leftSeperatorV layer] setCornerRadius:0];
            [[leftSeperatorV layer] setBorderWidth:1.0];
            seperatorV=[self createContentViewWithFrame:CGRectMake(kCartAndLikeViewWidth/2,0, 1,kCartLikeViewHeight )];
            [seperatorV setBackgroundColor:[UIColor clearColor]];
            [[seperatorV layer] setCornerRadius:0];
            [[seperatorV layer] setBorderWidth:1.0];
            
            FavButton=[self createActionButtonWithFrame:CGRectMake(0,0, kCartAndLikeViewWidth/2,kCartLikeViewHeight) withTitle:kEmptyString];
            FavButton.tag = kTagFavButton;
            [cell.contentView addSubview:bgv];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            [cell addSubview:bgv];
            
            [cell addSubview:ProductImageView];
            
            [cell addSubview:productNameLabel];
            [cell addSubview:productSizeLabel];
            [cell addSubview:productPriceLabel];
            [cell addSubview:CartAndLikeView];
            [CartAndLikeView addSubview:leftSeperatorV];
            [CartAndLikeView addSubview:FavButton];
            [CartAndLikeView addSubview:seperatorV];
            [CartAndLikeView addSubview:cartButton];
            
            separatorEnd = [[UIImageView alloc] initWithFrame:CGRectMake(0, krowHeightFavorite-1, SCREEN_WIDTH, 1)];
            [separatorEnd setBackgroundColor:[UIColor colorWithRed:0.8278 green:0.8278 blue:0.8278 alpha:1.0]];
            [cell.contentView addSubview:separatorEnd];
        }
        imgOutOfStock.hidden=YES;
        imgOutOfStock= [[UIImageView alloc] initWithFrame:CGRectMake((imgOutOfStock.frame.size.width)+182, 39, 38,38)];
        [imgOutOfStock setTag:101];
        [cell addSubview:imgOutOfStock];
        imgOutOfStock=(UIImageView *)[cell viewWithTag:101];
    
        Product *productObj = [ItemArray objectAtIndex:indexPath.row];
        
        productImageURL = [NSURL URLWithString:productObj.productImageURL];
        
        ProductImageView = (AsyncImageView *)[cell viewWithTag: kTagProductImageV];
        [ProductImageView setImageURL:productImageURL];
        [ProductImageView setContentMode:UIViewContentModeScaleAspectFit];
        ProductImageView.layer.masksToBounds = YES;
        
        productNameLabel = (QALabel *)[cell viewWithTag: kTagProductName];
        [productNameLabel setText:productObj.productName];
        productNameLabel.verticalAlignment = UIControlContentVerticalAlignmentBottom;
        
        productSizeLabel = (UILabel *)[cell viewWithTag: kTagProductSize];
        [productSizeLabel setText:productObj.productSize];
        
        productPriceLabel = (UILabel *)[cell viewWithTag: kTagProductPrice];
        [productPriceLabel setText:[self addDollarSignToString:kEmptytextfield]];
        
        
        if ([productObj.productSalesPrice isEqualToString:kEmptyString]||[productObj.productSalesPrice isEqualToString:kEmptySalePrice]) {
            [productPriceLabel setText:[self addDollarSignToString:productObj.productPrice]];
        }
        else{
            [productPriceLabel setText:[self addDollarSignToString:productObj.productSalesPrice]];
        }
        
        cartButton = (UIActionButton *)[[cell viewWithTag:kTagCartAndLikeView] viewWithTag:kTagCartButton];
        FavButton = (UIActionButton *)[[cell viewWithTag:kTagCartAndLikeView] viewWithTag:kTagFavButton];
        FavButton.rowNumber = (int)indexPath.row;
        cartButton.rowNumber = (int)indexPath.row;
        FavButton.productObj=productObj;
        cartButton.productObj=productObj;
        
        if ([[ShoppingCart sharedInstace ] shoppingCartContains:productObj]==NO) {
            [cartButton setImage:[UIImage imageNamed:kImagecarticongrey] forState:UIControlStateNormal];
            [cartButton setBackgroundColor:[UIColor clearColor]];
            [cartButton setSelected:NO];
        }
        else{
            [cartButton setImage:[UIImage imageNamed:kImageCartIconwhite] forState:UIControlStateNormal];
            [cartButton setBackgroundColor:[self getColorForYellowText]];
            [cartButton setSelected:YES];
        }
        
        [FavButton addTarget:self action:@selector(FavButton:) forControlEvents:UIControlEventTouchUpInside];
        productObj.productFavStatus=@"1";
        if ([productObj.productFavStatus intValue]==0) {
            [FavButton setImage:[UIImage imageNamed:kImageHeartIconGrey] forState:UIControlStateNormal];
            [FavButton setBackgroundColor:[UIColor clearColor]];
            [FavButton setSelected:NO];
        }
        else{
            [FavButton setImage:[UIImage imageNamed:kImageHeartIconwhite] forState:UIControlStateNormal];
            [FavButton setBackgroundColor:[self getColorForRedText]];
            [FavButton setSelected:YES];
            
        }
        
        if ([productObj.productStatus intValue]==0) {
            [productNameLabel setTextColor:[self getColorForBlackText]];
            [cell setBackgroundColor: [UIColor colorWithRed:0.9373 green:0.9373 blue:0.9373 alpha:1.0]];
            [bgv setBackgroundColor:[UIColor whiteColor]];
            imgOutOfStock.hidden=NO;
            if(imgOutOfStock.tag==101){
                imgOutOfStock.image = [UIImage imageNamed:@"out-of-stock"];
            }
        }
        else{
            imgOutOfStock.hidden=YES;
            [productNameLabel setTextColor:[self getColorForRedText]];
            [bgv setBackgroundColor:[UIColor whiteColor]];
            [cell setBackgroundColor:[UIColor clearColor]];
        }
        
        [[leftSeperatorV layer] setBorderColor:[[UIColor colorWithRed:0.8278 green:0.8278 blue:0.8278 alpha:1.0] CGColor]];
        [[seperatorV layer] setBorderColor:[[UIColor colorWithRed:0.8278 green:0.8278 blue:0.8278 alpha:1.0] CGColor]];
        return cell;
        
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row];
    NSUInteger count = [ItemArray count];
    
    if (row == count) {
        pagenumber++;
        
        if (count<totalCount) {
            [self callApiViewFavoriteWithPagenumber:pagenumber];
        }
    }
    else    {
        Product *productObj = [ItemArray objectAtIndex:indexPath.row];
        NSMutableArray *temparray=[NSMutableArray new];
        [temparray addObject:productObj];
        ProductDetailsViewController *vc=[self viewControllerWithIdentifier:kProductDetailsViewControllerStoryBoardID ];
        vc.productIDSelected=productObj.productId;
        vc.getProductarray=temparray;
        vc.isfav=@"1";
        [self.navigationController pushViewController:vc animated:YES ];
    }
}
@end
