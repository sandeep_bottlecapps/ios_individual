//
//  ForgotPasswordViewController.m
//  BOTTLECAPPS
//
//  Created by imac9 on 8/24/15.
//  Copyright (c) 2015 Monika Kumari. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#define kLogoImageFrame        (IS_IPHONE_4_OR_LESS? CGRectMake((SCREEN_WIDTH-189)-70, kTopPadding_logo, 100, 80):IS_IPHONE_6?CGRectMake((SCREEN_WIDTH-189)-70, kTopPadding_logo, 100, 80):CGRectMake((SCREEN_WIDTH-189)-70, kTopPadding_logo, 100, 80))

@interface ForgotPasswordViewController ()<UITextFieldDelegate>
{
    BOOL check;

}
@end

@implementation ForgotPasswordViewController
-(BOOL)formValidate
{
    check = FALSE;
    NSString *errMsg = @"Error";
    if ([self.UserEmailTextField.text length]==0)
    {
        check = TRUE;
        errMsg = kAlertEnterEmail;
    }
    
    if (check)
    {
        [self showAlertViewWithTitle:nil message:errMsg];
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}


-(void)addAppLogoImageInCenterOFLoginScreen{
 
        /***********create image view with bottlecapps logo*******************/
        UIImage *logoimage=[UIImage imageNamed:kImageLogo];
       self.LogoImageView= [self createImageViewWithRect:CGRectMake((SCREEN_WIDTH-logoimage.size.width)/2, kTopPadding2, logoimage.size.width, logoimage.size.height)  image:logoimage];
    
         if(kStoreImage == 1)
            self.LogoImageView= [self createImageViewWithRect:CGRectMake((SCREEN_WIDTH-logoimage.size.width)/2, kTopPadding2, logoimage.size.width, logoimage.size.height)  image:logoimage];
         else
            self.LogoImageView= [self createImageViewWithRect:CGRectMake((SCREEN_WIDTH-logoimage.size.width)/2-26, kTopPadding2, logoimage.size.width+50.0, logoimage.size.height+10.0)  image:logoimage];
        [self.CustomScrollView addSubview:self.LogoImageView];
        /***********create image view with bottlecapps logo*******************/

    /***********create image view with bottlecapps logo*******************/
   /* self.LogoImageView= [self createImageViewWithRect:kLogoImageFrame  image:[UIImage imageNamed:kImageLogo]];
    [self.LogoImageView sizeToFit];
    [self.CustomScrollView addSubview:self.LogoImageView]; */
      /***********create image view with bottlecapps logo*******************/
    
}
-(void)CallApiForgotPassword{
    NSMutableDictionary* userInfo = [NSMutableDictionary dictionary];
    
   
        [userInfo setObject:self.UserEmailTextField.text forKey: kParamUserName];
        [userInfo setObject:kSTOREIDbottlecappsString forKey: kParamStoreId];
        [self showSpinner]; // To show spinner
        [[ModelManager modelManager] ForgotPasswordWithUserCredentials:userInfo  successStatus:^(BOOL status,NSString*msg) {
       //     NSLog(@"%@", msg);
            [self hideSpinner];
            if ([msg isEqualToString:@"Sucess"]) {
                [self showAlertViewWithTitle:nil message:kAlertForgotPasswordSucess];
                [self popViewControllerWithDelay:2 complition:nil];


            }
            else{
                [self showAlertViewWithTitle:nil message:msg];
            }
        } failure:^(APIResponseStatusCode statusCode, NSString *errorMessage) {
            [self hideSpinner];
            [self showAlertViewWithTitle:kError message:[NSString stringWithFormat:@"%@\n", errorMessage]];
            DLog(@"%@",[NSString stringWithFormat:@"%@\n Status Code:%u", errorMessage, statusCode]);
            ///just for test
            
            
        }];
}

-(void)initScrollViewContent    {
    
       /*****************set content size to scrollview******************/
    self.CustomScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, self.CustomScrollView.frame.size.height);
    /*****************set content size to scrollview******************/
    [self addAppLogoImageInCenterOFLoginScreen];
    /***********create text field with username *******************/
    self.UserEmailTextField= [self createTxtfieldWithRect:CGRectMake(kRightPadding,(self.LogoImageView.frame.origin.y+self.LogoImageView.frame.size.height)+(kTopPadding), SCREEN_WIDTH-(kRightPadding*2),kTEXTfieldHeight) text:kEmptyString withLeftImage:[UIImage imageNamed:kImage_emailicon] withPlaceholder:kTextfieldPlaceholderemail withTag:kTextfieldTagSigninUserEmail ] ;
    self.UserEmailTextField.autocorrectionType = UITextAutocorrectionTypeNo;

    [self.UserEmailTextField setDelegate:self];
    [self.UserEmailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    self.UserEmailTextField.font= [UIFont fontWithName:kFontRegular size:kFontRegularSize];
    [self.CustomScrollView addSubview:self.UserEmailTextField];
    /***********create text field with username *******************/
    UIImage *submitImage=[UIImage imageNamed:kImagesubmitbutton];
    self.SubmitButton=[self createButtonWithFrame:CGRectMake((SCREEN_WIDTH-submitImage.size.width)/2,self.UserEmailTextField.frame.origin.y+self.UserEmailTextField.frame.size.height+kTopPadding, submitImage.size.width,submitImage.size.height) withTitle:kEmptyString];
    [_SubmitButton setImage:submitImage forState:UIControlStateNormal];
    [_SubmitButton.titleLabel setFont:[UIFont fontWithName:kFontBold size:kFontBoldSize]];
    [[_SubmitButton layer] setBorderWidth:1.0f];
    [[_SubmitButton layer] setBorderColor:[self getColorForRedText].CGColor];
    [_SubmitButton addTarget:self action:@selector(SubmitButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.CustomScrollView addSubview:_SubmitButton];

    
}


#pragma mark-VIEWCONTROLLER LIFE CYCLE
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addScrollView];
    [self initScrollViewContent];
    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.hidesBackButton = YES;
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [SlideNavigationController sharedInstance].enableSwipeGesture = NO;
}

-(void)viewDidDisappear:(BOOL)animated  {
    [SlideNavigationController sharedInstance].enableSwipeGesture = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark-BUTTON CLICKED
-(void)SubmitButton:(id)sender{
    
    
    [self.UserEmailTextField resignFirstResponder];
    check = FALSE;
    if ([self.UserEmailTextField.text length]==0)
    {
        check =  TRUE;
        [self showAlertViewWithTitle:nil message:kAlertEnterEmail];
    }
    else
    {
        if (![self validateEmail:self.UserEmailTextField.text])
        {
            check = TRUE;
            [self showAlertViewWithTitle:nil message:kAlertValidEmail];
        }
    }
    if (!check)
    {
        [self CallApiForgotPassword];
    }

    
}

@end
