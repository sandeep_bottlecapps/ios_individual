//
//  LocationManager.m
//  @lacarte
//
//  Created by Kuldeep Tyagi on 12/9/14.
//  Copyright (c) 2014 classic. All rights reserved.
//

#import "LocationManager.h"

@interface LocationManager  ()  {
 CLLocationManager *_CLLocationManager;
    
}

@end

@implementation LocationManager

static LocationManager *_locationManager;

+ (LocationManager*)locationManager {
    @synchronized([LocationManager class])   {
        if (_locationManager == nil) {
            _locationManager = [[LocationManager alloc] init];
        }
        return _locationManager;
    }
    return nil;
}

- (id)init {
    if ((self = [super init])) {
        // initialize the singleton object
        _CLLocationManager = [[CLLocationManager alloc] init];
        _CLLocationManager.delegate = self;
        _CLLocationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
        
        [_CLLocationManager startUpdatingLocation];
    }
    return self;
}

-(void) dealloc {
    _locationManager = nil;
    _CLLocationManager.delegate = nil;
    _CLLocationManager = nil;
}

#pragma mark - stop/Start update location to save battery

-(void)stopUpdatingLocation {
    [_CLLocationManager stopUpdatingLocation];
}

-(void)startUpdatingLocation    {
    [_CLLocationManager startUpdatingLocation];
}

#pragma mark - Check for location authorizationStatus

-(BOOL)getAuthorizationStatus   {
    if([CLLocationManager locationServicesEnabled]) {
        switch (CLLocationManager.authorizationStatus) {
            case kCLAuthorizationStatusNotDetermined:
                return FALSE;
                break;
            case kCLAuthorizationStatusRestricted:
                return FALSE;
                break;
            case kCLAuthorizationStatusDenied:
                return FALSE;
                break;
            case kCLAuthorizationStatusAuthorizedAlways:
                return TRUE;
                break;
            case kCLAuthorizationStatusAuthorizedWhenInUse:
                return TRUE;
                break;
            default:
                return FALSE;
                break;
        }
    }
    else    {
        return FALSE;
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error  {
    NSLog(@"didFailWithError: %@", error);
    // send notification to all classes to get update
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation   {
    NSLog(@"didUpdateToLocation: %@", newLocation);
}

@end
