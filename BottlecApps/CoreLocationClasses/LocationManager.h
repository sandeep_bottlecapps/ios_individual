//
//  LocationManager.h
//  @lacarte
//
//  Created by Kuldeep Tyagi on 12/9/14.
//  Copyright (c) 2014 classic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationManager : NSObject <CLLocationManagerDelegate>

+(LocationManager*)locationManager;
-(void)stopUpdatingLocation;
-(void)startUpdatingLocation;
-(BOOL)getAuthorizationStatus;

@end
